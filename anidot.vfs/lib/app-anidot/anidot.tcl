package provide app-anidot 1.0

#!/bin/sh
# next line is a comment in tcl \
exec tclkit $0 ${1+"$@"}

set topdir $starkit::topdir

catch {wm withdraw .}

#! @EXPECT@ --
#! /Utils/bin/tclsh
#! /usr/local/bin/expect --
#
# $Id: anifsm.sh,v 1.1.2.52 2005/11/03 14:38:51 belinfan Exp $
#
# (c) 2004-2005 Axel.Belinfante@cs.utwente.nl
# Feel free to use, update or redistribute as long as no monies are exchanged.
# Please report fixes/improvements back to me.
#
# To use, make one change above:
#   - replace @EXPECT@ by the path to your tclsh or expect interpreter
#   - make sure that the companion script anifsmsrv can be found
#      (is in your PATH)
#   - make sure that the utility script expnohup can be found
#      (is in your PATH)
#
# NOTE: this script only needs tcl(sh); it does not use expect
# features. The only reason that we use the expect interpreter
# here is that the configure script of TorX finds the location of
# expect already for us, and does not look for tclsh.

set debug 0
set G(prog) anidot
set G(srvprog) anidotsrv
set G(fnamepart)  anidot

proc cleanup_exit {{code {0}}} {
	global state
	if {[info exists state(stderr)]} {
		eval fconfigure stderr $state(stderr)
	}
	if {[info exists state(stdout)]} {
		eval fconfigure stdout $state(stdout)
	}
	if {[info exists state(stdin)]} {
		eval fconfigure stdin $state(stdin)
	}
	exit $code
}

proc configure_fids {socket} {
	fconfigure $socket -buffering line  -translation {binary binary}
	fconfigure stdin -buffering line
# not for windows: ?
# \
#		                   -translation {binary binary}
}

proc filevent_fids {socket remoteHost remotePort waitRemoteClose} {
	fileevent $socket readable \
	    	[list getRemote $remoteHost $remotePort $socket]
	fileevent stdin readable \
	 	[list sendRemote $remoteHost $remotePort $socket $waitRemoteClose]
	vwait forever
}

proc setUp {remoteHost remotePort timeout waitRemoteClose} {
	global connected
	global portNr conf
	global state

	warning "trying to connect ($waitRemoteClose) to tcp!$remoteHost!$remotePort"
	after $timeout {set connected timeout}
	if {[catch {socket $remoteHost $remotePort} socket]} {
		fatal "while connecting to tcp!$remoteHost!$remotePort: $socket" 1
	}
	set portNr [lindex [fconfigure $socket -sockname] 2]
	fileevent $socket w {set connected ok}
	vwait connected
	fileevent $socket w {}
	if {$connected == "timeout"} {
		return -code error timeout
	} else {
		warning "connected to tcp!$remoteHost!$remotePort at $portNr"
		configure_fids $socket
		filevent_fids $socket $remoteHost $remotePort $waitRemoteClose
	}
}

proc getRemote {remoteHost remotePort sock} {
	fileevent $sock readable {}
	set read [gets $sock line]
	debug 5 "getRemote tcp!$remoteHost!$remotePort read: $read"
	if {$read < 0} {
		if {[eof $sock]} {
			warning "Connection closed by server tcp!$remoteHost!$remotePort."
			cleanup_exit
		} elseif {[fblocked $sock]} {
			warning "getRemote tcp!$remoteHost!$remotePort blocked: $line"
		} else {
			warning "getRemote tcp!$remoteHost!$remotePort error?: $line"
			cleanup_exit
		}
	} else {
		output stdout $line
		debug 5 "getRemote tcp!$remoteHost!$remotePort: $line"
	}
	fileevent $sock readable [list getRemote $remoteHost $remotePort $sock]
}
proc sendRemote {remoteHost remotePort sock waitRemoteClose} {
	fileevent stdin readable {}
	set read [gets stdin line]
	debug 5 "sendRemote tcp!$remoteHost!$remotePort read: $read"
	if {$read < 0} {
		if {[eof stdin]} {
			# set line "A_QUIT"
			# catch {output $sock $line}
			output $sock -eof
			debug 0 "eof stdin"
			if {! $waitRemoteClose} {
				catch {close $sock}
				debug 5 "sendRemote closed tcp!$remoteHost!$remotePort"
				cleanup_exit
			} else {
				return
			}
		} elseif {[fblocked stdin]} {
			warning "sendRemote tcp!$remoteHost!$remotePort blocked: $line"
		} else {
			warning "sendRemote tcp!$remoteHost!$remotePort error?: $line"
			cleanup_exit
		}
	} else {
		debug 1 "sendRemote sending tcp!$remoteHost!$remotePort: $line"
		output $sock $line
		debug 1 "sendRemote sent tcp!$remoteHost!$remotePort: $line"
	}
	fileevent stdin readable [list sendRemote $remoteHost $remotePort $sock $waitRemoteClose]
}

proc main_loop {sock waitRemoteClose argstring} {
	configure_fids $sock
	set remote [fconfigure $sock -peername]
	set remoteHost [lindex $remote 0]
	set remotePort [lindex $remote 2]
	puts $sock $argstring
	filevent_fids $sock $remoteHost $remotePort $waitRemoteClose
}

proc usage {} {
	warning "usage: $G(prog) \[-r\] \[-m mcastid\] \[-t title\] \[-k key\] \[-s srvaddr \] \[file.dot\]"
		exit 1
}

proc main {} {
	global argv
	global env
	global pid
	global G

	set waitRemoteClose 1

	set reuse ""
	set mcast ""
	set title ""
	set key ""
	set srvaddr ""
	set files {}
	set exit 0
	set errors 0
	set pid [pid]

	set argstring ""
	
	if {[info exists env(TORXMCASTID)]} {
		set mcast "-m $env(TORXMCASTID)"
	}

	set args $argv
	
#	set joinedargs [join $argv ")(" ]
#	warning "command lin args: ($joinedargs)"

	while {[llength $args] > 0} {
		set a [lindex $args 0]
		set args [lrange $args 1 end]
		switch -- $a {
		    -r {
				set reuse "-r"
		  } -m {
				if {[llength $args] <= 0} {
		  			warning "missing argument for flag: $a"
					set errors 1
				} else {
					set mcast "-m [lindex $args 0]"
					set args [lrange $args 1 end]
				}
		  } -t {
				if {[llength $args] <= 0} {
		  			warning "missing argument for flag: $a"
					set errors 1
				} else {
					set title "-t {[lindex $args 0]}"
					set args [lrange $args 1 end]
				}
		  } -k {
				if {[llength $args] <= 0} {
		  			warning "missing argument for flag: $a"
					set errors 1
				} else {
					set key "-k [lindex $args 0]"
					set args [lrange $args 1 end]
				}
		  } -s {
				if {[llength $args] <= 0} {
		  			warning "missing argument for flag: $a"
					set errors 1
				} else {
					set srvaddr [lindex $args 0]
					set args [lrange $args 1 end]
				}
		  } -exit {
		  		set exit 1
		  } -* {
		  		warning "unknown flag: $a"
				set errors 1
		  } default {
		  		lappend files $a
		  }
		}
	}
#puts  "bla"

	if {$errors} {
		usage
	}
	set host [info hostname]
	set display [find_display $host]
	set user [find_user]
	set tmp [find_tmp]
	debug 0 "tmp=$tmp"
	debug 0 "host=$host"
	debug 0 "display=$display"
	set fname [mk_filename $tmp $G(fnamepart) $user $display]
	debug 0 "fname=$fname"
	set B(interface,name) $fname
	set B(pidsfile,name) "$fname.pid"
	set B(statusfile,name) "$fname.stat"

#puts  "bla2"

	if {[string compare $srvaddr ""] == 0} {
		set srvaddr [get_address $fname 0]
	}
	set sock [try_connect $srvaddr 0]
#puts  "bla3 $sock"
	if {$exit} {
		if {[string compare $sock 0] != 0} {
			puts $sock -exit
			exit
		}
	} else {
		set ff {}
		foreach file $files {
			if {[string compare $file "-"] != 0 && [string compare [file pathtype $file] absolute] != 0} {
				set file [file join [pwd] $file]
			}
			lappend ff $file
		}
# 		set argstring "-args $reuse $mcast $title $key [list $file]"
		set argstring "-args $reuse $mcast $title $key $ff"
		# puts stderr "sending $argstring"

		if {[string compare $sock 0] != 0} {
#puts  "bla4 $sock"
			main_loop $sock $waitRemoteClose $argstring
		} else {
#puts  "bla4 $sock"
			if {[catch {open $B(statusfile,name) a} fid]} {
				puts  "cannot open (append) $B(statusfile,name): $fid"
				exit 1
			} else {
				puts $fid "starting $host $pid"
				close $fid
			}
			if {[catch {open $B(statusfile,name) r} fid]} {
				puts  "cannot open (read) $B(statusfile,name)"
				exit 1
			} elseif {[gets $fid line] < 0} {
				puts  "cannot read $B(statusfile,name)"
				exit 1
			} else {
				close $fid
				#puts  "read $B(statusfile,name): $line"
				set words [split $line]
				set stat [lindex $words 0]
				set h [lindex $words 1]
				set p [lindex $words 2]
				if {[string compare $stat starting] == 0 &&
				    [string compare $host $h] == 0 &&
				    ( [string compare $pid $p] == 0 ||
				      ![findprocess $pid $p])} {
					# puts  "starting  $G(srvprog)"
					start  [list $G(srvprog) $pid]
				} else {
					puts  "stat $stat (starting) h $h ($host) p $p ($pid)"
				}
				set i 0
				set maxtries 150
				while {$i < $maxtries} {
					# puts  "trying to connect $i)"
					if {$i == ($maxtries - 1)} {
						set sock [try_connect [get_address $fname 1] 1]
					} else {
						set sock [try_connect [get_address $fname 0] 0]
					}
					if {[string compare $sock 0] != 0} {
						# puts "connected $sock"
						main_loop $sock $waitRemoteClose $argstring
					}
					after 100
					incr i
				}
			}
		}
	}
}


main
exit
