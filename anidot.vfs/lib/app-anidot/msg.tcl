package provide app-anidot 1.0

proc warn {chan msg} {
	global G
	output $chan "$G(prog): $msg"
}

# we use output to make sure that everything is flushed!
# we do this to avoid deadlocks due to buffers being full...
proc output {chan msg} {
	puts $chan $msg
	flush $chan
}

proc fatal {s {code {0}}} {
	global G
	set s [string trim $s]
	output stdout "$G(prog): $s"
	cleanup_exit $code
}
proc message {s} {
	global G
	set s [string trim $s]
	output stdout "$G(prog): $s"
}
proc warning {s} {
	global G
	set s [string trim $s]
	output stdout "$G(prog): $s"
}

proc debug {nr s} {
	global G
	global debug
	if {$debug >= $nr} {
		set s [string trim $s]
		output stderr "$G(prog): $s"
	}
}
