package provide app-anidot 1.0

# initially we tried to run 'exec -- tclkit bla -s &' but somehow
# on windows # the '-s' did not arrive with the program that
# we exec-ed
# That's why we tried using '-server' intead of '-s', which does work.

proc start { argv } {
	global tcl_platform starkit topdir argv0 pid
	set pid [pid]
	set tp $tcl_platform(platform)

	package require Tclkit
#	puts "process:$pid: info script: [info script]"
#	puts "process:$pid: starkit::topdir  $starkit::topdir"
#	puts "process:$pid: topdir  $topdir"

#	puts "process:$pid: argv0 $argv0"
#	puts "process:$pid: where we are, as script: driname argv0 : [file dirname $argv0]"
#	puts "process:$pid: where we are, as starpack 1:  dirname of dirname of argv0: [file dirname [file dirname $argv0]]"
#	puts "process:$pid: where we are, as starpack 2: info nameofexecutable:  [info nameofexecutable]"

	set runner ""
	set kit ""
	set cmd {}
	set nohupcmd {}
	set exe_tmp_script {}
	if {[lsearch [info commands] gettclkitpath] >= 0} {
		set exe_tmp_script [gettclkitpath]
	}
	if {[string first [info nameofexecutable] $argv0] == 0} {
#		puts "process:$pid: we are starpack"
		# we are a starpack
		# for the unix server: run [info nameofexecutable] -s
		# for the windows server: run tclkit on server...
		if {[string compare [lindex $exe_tmp_script 0] ""] != 0} {
#			puts "process:$pid: windows: using gettclkitpath"
			set exe [lindex $exe_tmp_script 0]
			set kit [info nameofexecutable]
			set runner [lindex $exe_tmp_script 2]
		} else {
			set exe [info nameofexecutable]
		}
	} else {
		switch -- [file extension $starkit::topdir] {
			.kit {
#				puts "process:$pid: we are ... topdir ~ .kit"
				if {[string compare [lindex $exe_tmp_script 0] ""] != 0} {
					set exe [lindex $exe_tmp_script 0]
				} else {
					set exe [info nameofexecutable]
				}
				set kit $starkit::topdir
			}
			.vfs {
#				puts "process:$pid: we are ... topdir ~ .vfs"
				if {[string compare [lindex $exe_tmp_script 0] ""] != 0} {
					set exe [lindex $exe_tmp_script 0]
				} else {
					set exe [info nameofexecutable]
				}
				set kit [file join $starkit::topdir main.tcl]
			}
			default {
#				puts "process:$pid: we are ... ???"
#				puts "process:$pid: don't know how to start server, just guesing..."
				if {[string compare [lindex $exe_tmp_script 0] ""] != 0} {
					set exe [lindex $exe_tmp_script 0]
				} else {
					set exe [info nameofexecutable]
				}
				set kit $starkit::topdir
				set runner [lindex $exe_tmp_script 2]
			}
		}
	}

	lappend nohupcmd exec --
	lappend cmd exec --
	lappend nohupcmd nohup
	foreach a [list $exe $runner $kit] {
		if {[string compare $a ""] != 0} {
			lappend nohupcmd $a
			lappend cmd $a
		}
	}
	lappend nohupcmd -server $pid &
	lappend cmd -server $pid &



#	set exe_lib [gettclkitpath]
#	set kit [file join [lindex $exe_lib 1] $argv.kit]
#	set exe [lindex $exe_lib 0]
#	set nohupcmd [concat  exec -- nohup $exe $runner $kit -server &]
#	set cmd [concat  exec  -- $exe $runner $kit -server &]
#	puts  "process:$pid: trying to start server: $nohupcmd"
	if {0 && ![catch $nohupcmd msg]} {
#		puts  "process:$pid: trying to start server: result: $msg"
		return
	} elseif {![catch $cmd msg]} {
		puts  "process:$pid: trying to start server: result: $msg"
#		puts  "process:$pid: succeeded starting server: $cmd"
		return
	} else {
		catch {puts stdout "cannot run: $cmd: $msg"}
	}
	return
	switch -- $tp {
	    unix {
		exec sh -c "expnohup $argv &" &
	  } windows {
		exec $argv &
	  }  macintosh {
		warn stderr "don't know how to start $argv (on platform $tp)"
	  } default {
		warn stderr "don't know how to start $argv (unknown platform $tp)"
	  }
	}
}

proc kill {p} {
	global tcl_platform

	msg stderr "cleaning up: kill $p"
	set tp $tcl_platform(platform)
	switch -- $tp {
	    unix {
		catch {exec kill $p}
		msg stderr "cleaning up: kill -9 $p"
		catch {exec kill -9 $p}
	  } windows -
	    macintosh {
		msg stderr "don't know how to kill $p (on platform $tp)"
	  } default {
		msg stderr "don't know how to kill $p (unknown platform $tp)"
	  }
	}
}

proc findprocess {pid p} {
	global tcl_platform

	set found 0
	set tp $tcl_platform(platform)
	warn stdout "findprocess $pid $p  $tp"
	switch -- $tp {
	    unix {
		if {[catch {open "|ps -ef"} pidsfd]} {
			warn stdout "could not run: ps -ef"
			exit 1
		} else {
			while {[gets $pidsfd line] >= 0} {
#				warn stdout "findprocess $pid $p  $tp line $line"
#				debug 25 "$pid: looking for $p in: $line"
				if {[lindex $line 1] == $p}  {
#					warn stdout "found running server pid: $p"
					set found 1
					break
				}
			}
			close $pidsfd
		}
	  } windows {
#		warn stdout "findprocess windows"
		if {![catch {open "|tasklist"} pidsfd]} {
#			warn stdout "findprocess windows in tasklist"
			while {[gets $pidsfd line] >= 0} {
				debug 1 "$pid: looking for $p in: $line"
				if {[lindex $line 2] == $p}  {
#					warn stdout "tasklist found running server pid: $p"
					set found 1
					break
				}
			}
			close $pidsfd
		} elseif {![catch {open "|tlist"} pidsfd]} {
#			warn stdout "findprocess windows in tlist"
			while {[gets $pidsfd line] >= 0} {
				debug 1 "$pid: looking for $p in: $line"
				set nr [lindex $line 0]
				set nr2 [expr int($nr - pow(2,32)]
				if {$nr == $p || $nr2 == $p}  {
#					warn stdout "tlist found running server pid: $p"
					set found 1
					break
				}
			}
			close $pidsfd
		} else {
			# should try tlist on other windows?
			warn stdout "could not run: tasklist"
			exit 1
		}
	  } macintosh {
		warn stdout "don't know how to kill $p (on platform $tp)"
	  } default {
		warn stdout "don't know how to kill $p (unknown platform $tp)"
	  }
	}
	warn stdout "findprocess $pid $p: $found"
	return $found
}

