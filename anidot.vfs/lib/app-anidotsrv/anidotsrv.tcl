package provide app-anidotsrv 1.0

#!/bin/sh
# next line is a comment in tcl \
exec tclkit $0 ${1+"$@"}

set topdir $starkit::topdir

package require Tk

source [file join $topdir lib app-anidotsrv msg.tcl]
source [file join $topdir lib app-anidotsrv process.tcl]
source [file join $topdir lib app-anidotsrv clientsrv.tcl]
source [file join $topdir lib app-anidotsrv util.tcl]
source [file join $topdir lib app-anidotsrv edit.tcl]
source [file join $topdir lib app-anidotsrv graph.tcl]
source [file join $topdir lib app-anidotsrv toaut.tcl]
source [file join $topdir lib app-anidotsrv todot.tcl]
source [file join $topdir lib app-anidotsrv tops.tcl]
source [file join $topdir lib app-anidotsrv brighten.tcl]

package require windowlist
package require mactoolbar

#! /bin/sh
#-*-tcl-*-
#
# $Id: anifsmsrv.sh,v 1.1.2.20 2006/02/15 19:37:43 belinfan Exp $
#
# (c) 2004-2005 Axel.Belinfante@cs.utwente.nl
#
# To give credit where it's due:
# Parts of this code (in particular the zooming code) have been lifted from:
#     doted - dot graph editor - John Ellson (ellson@graphviz.org)
# 
# Feel free to use, update or redistribute as long as no monies are exchanged.
# Please report fixes/improvements back to me.
# 
#
# To use, make two changes below:
#   - replace @WISH@ by the path to your wish interpreter
#   - replace @GRAPHVIZLIB@ by the path to the directory that contains tcldot.so
#
# the next line restarts using wish \
exec @WISH@ "$0" -- ${1+"$@"}

wm withdraw .
windowlist::omitDefaultWindow

if {[string compare [tk windowingsystem] aqua] == 0} {
	if {[catch {package require tkdock} msg ]} {
		 catch {puts stderr "anidot: cannot load tkdock: $msg"}
	} else {
		 if {[catch {open [file join $topdir lib dock-img.icns] r} fp]} {
			  catch {puts stderr "anidot: cannot open tkdock icon file: $fp"}
		 } elseif {[catch {
				   fconfigure $fp -translation binary
				   set data [read $fp]
				   close $fp
			  } msg]} {
				   catch {puts stderr "anidot: cannot read tkdock icon: $msg"}
		 } elseif {[catch {
				   ::tkdock::switchIcon -data $data
			  } msg]} {
				   catch {puts stderr "anidot: cannot read or set tkdock icon: $msg"}
		 }
	}
}

set PID [pid]:

set G(cprog) anidot
set G(sprog) anidotsrv
set G(fnamepart)  anidot

set addr 127.0.0.1 ;# loopback
# set addr 0.0.0.0 ;# any (all) address

# lappend auto_path /Tios/Utils/GraphViz1.8.5/lib/graphviz
# set graphvizlibdir "@GRAPHVIZLIB@"
# if {[string compare $graphvizlibdir ""] != 0} {
# 	lappend auto_path $graphvizlibdir
# }
set auto_path [concat /tmp/tcldotlib $auto_path]
catch {puts stderr "ap $auto_path"}
catch {package require Tkspline}
if {[catch {package require Tcldot} msg]} {
	catch {puts stderr "$G(sprog) : could not require package Tcldot: $msg"}
	catch {puts stderr "$G(sprog) : giving up"}
}

set color(background) white
set color(highlight) red

set zfact 1.1

set debug 0

set id 0
set ncanvas 0
set windows {}
set reuseQ {}
set keys {}
set DX 10c
set DY 7c
set SCROLLBARWIDTH 10

# in the code below, we deal with
# - multiple windows (identified by 'nr')
# - multiple animation runs (identifier by 'id')
# each window (nr) contains one animation run (id)
# when the animation run is finished
# (eof on filedescriptor from which we read run data),
# the window can become reuseable, it can then be
# reused for a new animation run (with a new id).

proc update_reuse {nr} {
	upvar #0 W_[set nr](id) id
	upvar #0 W_[set nr](doReuse) doReuse
	upvar #0 G_[set id](ieof) ieof
	upvar #0 S_[set id](nr) w_nrs
	global reuseQ

	debug 1 "update_reuse $nr { doReuse $doReuse ieof $ieof reuseQ $reuseQ"
	if {$ieof} {
		set state normal
	} else {
		set state disabled
	}
	foreach w_nr $w_nrs {
		upvar #0 W_[set w_nr](doReuse) w_doReuse
		set w_doReuse $doReuse
		debug 1 "update_reuse $w_nr : doReuse $doReuse ieof $ieof reuseQ $reuseQ"
		if {$ieof && $doReuse && [lsearch -exact  $reuseQ $w_nr] < 0} {
		 	lappend reuseQ $w_nr
		} elseif {!$doReuse} {
		 	set reuseQ [ldelete $reuseQ $w_nr]
		}
		.f$w_nr.br configure -state $state
	}
	debug 1 "update_reuse $nr } reuseQ $reuseQ"
}

proc do_render {id} {
	upvar #0 S_[set id](nr) w_nrs
	upvar #0 G_[set id](ofid) ofid
	upvar #0 G_[set id](g) g

	debug 1 "do_render $id {"
	hidecanvas $w_nrs
	update idletasks
	rendergraph $w_nrs $g $ofid
	showcanvas $w_nrs
	update idletasks
	showcur $w_nrs
	debug 1 "do_render $id }"
}

proc update_render {nr} {
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](doRender) doRender

	debug 1 "update_render $nr {"
	if {$doRender} {
		do_render $id
	}
	debug 1 "update_render $nr }"
}

proc update_keymenu {nr} {
	upvar #0 W_[set nr](id) id
	upvar #0 W_[set nr](key) key
	upvar #0 G_[set id](ieof) ieof
	upvar #0 S_[set id](nr) w_nrs

	debug 1 "update_keymenu $nr { key $key ieof $ieof"
	if {$ieof} {
		set state normal
	} else {
		set state disabled
	}
	foreach w_nr $w_nrs {
		upvar #0 W_[set w_nr](key) w_key
		set w_key $key
		debug 1 "update_keymenu $w_nr : key $key ieof $ieof"
		.f$w_nr.mbk configure -state $state
		.f$w_nr.mbk configure -state $state
		# nex line depends on "" being our first menu entry
		# there may be an `earlier' menu entry, for tear-off
		set i [.f$w_nr.mbk.m index ""]
		set end [.f$w_nr.mbk.m index end]
		while {$i <= $end} {
			.f$w_nr.mbk.m entryconfigure $i -state $state
			incr i
		}
	}
	debug 1 "update_keymenu $nr }"
}

proc extend_menu {nr m var val} {
	upvar #0 W_[set nr](t) t

	debug 1 "extend_menu $nr $var $val"
	$t.$m.m add radiobutton \
		-variable W_[set nr]([set var]) \
		-label $val \
		-value $val \
		-command "update_[set var]menu $nr"
}

proc populate_keymenu {nr} {
	global keys

	debug 1 "populate_keymenu $nr"
	foreach k $keys {
		extend_menu $nr mbk key $k
	}
}

proc add_key {key} {
	global keys
	global windows
	global winnr

	debug 1 "update_keys $key"
	if {[lsearch -exact $keys $key] < 0} {
		lappend keys $key
		foreach t $windows {
			extend_menu $winnr($t) mbk key $key
		}
	}
}

proc populate_vieweditmenu {nr} {
	upvar #0 W_[set nr](t) t

	foreach k {View Edit} {
		extend_menu $nr mbve viewedit $k
	}
}

proc update_vieweditmenu {nr} {
}

proc step_inc {nr inc} {
	upvar #0 W_[set nr](id) id
	upvar #0 W_[set nr](c) c
	upvar #0 G_[set id](scur) scur
	upvar #0 G_[set id](Q) Q
	
	if {([string compare $scur ""] != 0) &&
	    ([isinteger $scur]) &&
	    ([expr $scur $inc] >= 0) && ([expr $scur $inc] <= [llength $Q])} {
		set idx $scur
	    	incr idx $inc
	    	showq $nr 1 $idx
	}
}

proc find_reusable {id ofid key} {
	global reuseQ

	debug 1 "find_reusable $id : ($reuseQ) {"
	set res ""
	set haskey {}
	set nokey {}
	foreach w_nr $reuseQ {
		upvar #0 W_[set w_nr](id) w_id
		upvar #0 W_[set w_nr](key) w_key
		upvar #0 G_[set w_id](ieof) w_ieof
		if {[string compare $id $w_id] == 0} {
			debug 1 "find_reusable $id: skipping $w_nr: id $id == w_id $w_id"
		} elseif {$w_ieof} {
			if {[string compare $key $w_key] == 0} {
				debug 1 "find_reusable $id: found $w_nr (containing $w_id) key=$key w_key=$w_key)"
				set res $w_nr
				break
			} elseif {[string compare $w_key ""] == 0} {
				lappend nokey $w_nr
			} else {
				lappend haskey $w_nr
			}
		} else {
			debug 1 "find_reusable $id: skipping $w_nr: no eof seen"
		}
	}
	if {[string compare $res ""] == 0} {
		if {[string compare $key ""] == 0 && [llength $haskey] > 0} {
			set res [lindex $haskey 0]
		} elseif {[string compare $key ""] != 0 && [llength $nokey] > 0} {
			set res [lindex $nokey 0]
		}
	}
	if {[string compare $res ""] != 0} {
		set reuseQ [ldelete $reuseQ $res]
	}
	debug 1 "find_reusable $id: ($reuseQ) } ($res)"
	return $res
}

proc getcanvas {id reuse ofid key} {
	debug 1 "getcanvas ($id) ($reuse)($ofid) {"
	set nr [find_reusable $id $ofid $key]
	if {[string compare $nr ""] != 0} {
		upvar #0 W_[set nr](id) w_id
		upvar #0 S_[set w_id](nr) w_nrs
		set res_nr [reusecanvas $w_nrs $id $reuse $key]
		# assert($res_nr == $nr)
	} else {
		set res_nr [mkcanvas $id $reuse $key]
	}
	debug 1 "getcanvas ($id) ($reuse)($ofid)($res_nr) }"
	return $res_nr
}

proc hidecanvas {nrs} {
	debug 1 "hidecanvas ($nrs) {"
	foreach nr $nrs {
		upvar #0 W_[set nr]_packinfo packinfo
		upvar #0 W_[set nr](c) c

		set packinfo [pack info $c]
		pack forget $c

		# alternative:
		# lower $c
	}
	debug 1 "hidecanvas ($nrs) }"
}
proc showcanvas {nrs} {
	debug 1 "showcanvas ($nrs) {"
	foreach nr $nrs {
		upvar #0 W_[set nr]_packinfo packinfo
		upvar #0 W_[set nr](c) c

		eval [concat pack configure $c $packinfo]

		# alternative:
		# raise $c
	}
	debug 1 "showcanvas ($nrs) }"
}

proc reusecanvas {nrs id reuse akey} {

	debug 1  "reusecanvas ($nrs) ($id) ($reuse) {"
	foreach nr $nrs {
		upvar #0 W_[set nr](doReuse) doReuse
		upvar #0 W_[set nr](id) w_id
		upvar #0 W_[set nr](autoFit) autoFit
		upvar #0 W_[set nr](c) c
		upvar #0 W_[set nr](t) t
		upvar #0 W_[set nr](step) step
		upvar #0 W_[set nr](key) key
		upvar #0 W_[set nr](viewedit) viewedit
		upvar #0 S_[set id](nr) w_nrs
		upvar #0 G_[set id](title) title
		upvar #0 G_[set id](doRender) doRender
		upvar #0 G_[set id](delayRender) delayRender
		upvar #0 G_[set id](isChanged) isChanged
		upvar #0 G_[set id](doUpdate) doUpdate
		global zfact

		debug 1  "reusecanvas ($nrs) ($id) ($reuse): $nr"
		lappend w_nrs $nr

		destroy_contents $nr
		$c delete all
		bind $t <Configure> {}
		# reset zoom
		debug 1 "zoomdepth=[zoomdepth $nr]"
		zoom $nr [expr 1 / [zoomdepth $nr]]
		debug 1 "zoomdepth=[zoomdepth $nr]"

		set w_id $id
		set doReuse $reuse
		set key $akey
		set autoFit 1
		set step ""

		set viewedit View
		set doRender 1
		set delayRender 0
		set isChanged 0
		set doUpdate 1

		wm title $t $title
		wm deiconify $t
		raise $t
	}
	update_reuse [lindex $nrs 0]
	update_keymenu [lindex $nrs 0]
	debug 1  "reusecanvas ($nrs) ($id) ($reuse) }"

	return $nrs
}

proc setToolbarMenuItem {menu w} {
	if [winfo ismapped $w] {
		set label "Hide Toolbar"
	} else {
		set label "Show Toolbar"
	}
	$menu entryconfigure {*Toolbar} -label $label
}
proc toggleToolbar {menu w} {
	event generate  $w <<ToolbarButton>>
	setToolbarMenuItem $menu $w
}

proc mkcanvas {id reuse akey} {
	global color
	global ncanvas
	global windows
	global winnr
	global zfact
	global DX
	global DY
	global SCROLLBARWIDTH
	global advanced
	upvar #0 S_[set id](nr) w_nr
	upvar #0 G_[set id](scur) scur
	upvar #0 G_[set id](title) title
	upvar #0 G_[set id](doRender) doRender
	upvar #0 G_[set id](delayRender) delayRender
	upvar #0 G_[set id](isChanged) isChanged
	upvar #0 W_[set ncanvas](doReuse) doReuse
	upvar #0 G_[set id](doUpdate) doUpdate
	upvar #0 W_[set ncanvas](busy) busy
	upvar #0 W_[set ncanvas](destroying) destroying
	upvar #0 W_[set ncanvas](autoFit) autoFit
	upvar #0 W_[set ncanvas]_zoomdata zoomdata
	upvar #0 W_[set ncanvas](id) w_id
	upvar #0 W_[set ncanvas](c) c
	upvar #0 W_[set ncanvas](t) t
	upvar #0 W_[set ncanvas](step) step
	upvar #0 W_[set ncanvas](key) key
	upvar #0 W_[set ncanvas](viewedit) viewedit

	# we use busy and destroying to synchronise with the destroy
	# to avoid trying to write to a window that is just destroyed
	set busy 0
	set destroying 0

	set nr $ncanvas
	incr ncanvas

	lappend w_nr $nr
	set w_id $id
	set doReuse $reuse
	set key $akey

	# initialize zoom state
	set zoomdata(zdepth) 1.0
	set zoomdata(idle) {}
	set autoFit 1

	set step ""

	set viewedit View
	set doRender 1
	set delayRender 0
	set isChanged 0
	set doUpdate 1

	set t [toplevel .f$nr -background $color(background)]
	lappend windows $t
	set winnr($t) $nr
	
	set menubar [menu .m$nr]
	# catch {puts stderr "mkcanvas menu nr $nr m=$menubar"}
	$t configure -menu $menubar
	set am [menu $menubar.apple]
	$menubar add cascade -label "AniDot" -menu $am
	$am add command -label "About AniDot" -command {}
	set vm [menu $menubar.view]
	$menubar add cascade -label "View" -menu $vm
	$vm add command -label "Hide Toolbar" -command [list toggleToolbar $vm $t.tbf.toolbar]
	$vm configure -postcommand [list setToolbarMenuItem $vm $t.tbf.toolbar]
	windowlist::windowMenu $menubar
	
	set i 0
	while {$i <= [$menubar index last]} {
	 	if {! [catch {$menubar entrycget $i -menu} subMenu]} {
		 	$subMenu configure -tearoff 0
		 }
		 incr i
	}
	
	frame $t.tbf
	frame $t.rest
	mactoolbar::toolbar $t.tbf.toolbar

	# make frames first, below everything else
	frame $t.f
	frame $t.fc \
		-background  $color(background) \
		-borderwidth 0 \
		-highlightthickness 0 \
		-height $DY \
		-width $DX
	frame $t.fz -borderwidth 2 -relief ridge
	frame $t.fq -borderwidth 2 -relief ridge
	frame $t.fr -borderwidth 2 -relief ridge
	frame $t.fn -borderwidth 2 -relief ridge
	frame $t.frd -borderwidth 2 -relief ridge
	frame $t.bs
	frame $t.bz

	set c [canvas $t.c  -relief raised \
		-background  $color(background) \
		-borderwidth 0 \
		-highlightthickness 0 \
		-height $DY \
		-width $DX \
		-xscrollcommand "$t.hscroll set" \
		-yscrollcommand "$t.vscroll set"]
	scrollbar $t.vscroll -relief sunken \
		-width $SCROLLBARWIDTH \
		-command "$c yview"
	scrollbar $t.hscroll -relief sunken -orient horiz \
		-width $SCROLLBARWIDTH \
		-command "$c xview"

	button $t.bq -text "Quit" \
		-command "quit $nr"
		
	button $t.bc -text "Close" \
		-command "mydestroy $nr"

	button $t.bx -text "Clone" \
		-command "clone $nr"

	menubutton $t.mbk \
		 -width 5 \
		 -textvariable W_[set nr](key) \
		 -direction flush \
		 -menu $t.mbk.m
	menu $t.mbk.m
	populate_keymenu $nr
	update_keymenu $nr

	checkbutton $t.br -text "Reuse" \
		 -variable W_[set nr](doReuse) \
		 -command "update_reuse $nr"
	update_reuse $nr

	checkbutton $t.brd -text "Render" \
		 -variable G_[set id](doRender) \
		 -command "update_render $nr"
		 
	menubutton $t.mbve \
		 -width 5 \
		 -textvariable W_[set nr](viewedit) \
		 -direction flush \
		 -menu $t.mbve.m
	menu $t.mbve.m
	populate_vieweditmenu $nr
	update_vieweditmenu $nr

	label $t.l -text "Step:"
	entry $t.t \
		 -width 5 \
		 -textvariable W_[set nr](step)
	bind $t.t <Return> "showq $nr 1 \$W_[set nr](step)"
	button $t.bsp -image up \
		-command "step_inc $nr +1"
	button $t.bsm -image down \
		-command "step_inc $nr -1"

	label $t.lz -text "Zoom:"
	button $t.bzp -image up \
		-command "zoom $nr $zfact"
	button $t.bzm -image down \
		-command "zoom $nr [expr {1.0/$zfact}]"

	button $t.bzf -text "Fit" \
		-command "scaletofit $nr"
	checkbutton $t.bza -text "Auto" \
		 -variable W_[set nr](autoFit)


	pack append $t.bz \
		$t.bzp {top  fillx filly expand } \
		$t.bzm {bottom  fillx filly expand }

	pack append $t.fz \
		$t.bza {right  fillx filly } \
		$t.bzf {right  } \
		$t.bz {right  fillx filly } \
		$t.lz {left  fillx filly }

#if {$advanced} {
	pack append $t.fq \
		$t.bq {right } \
		$t.bc {right }
#}

	pack append $t.fr \
		$t.bx {right } \
		$t.mbk {right fillx expand } \
		$t.br {right fillx filly expand }

#if {$advanced } {
	pack append $t.frd \
		$t.brd {left fillx filly expand } \
		$t.mbve {left fillx expand }
#}

	pack append $t.bs \
		$t.bsp {top  fillx filly expand } \
		$t.bsm {bottom  fillx filly expand }

	pack append $t.fn \
		$t.bs {right  fillx filly } \
		$t.t {right fillx expand } \
		$t.l {left  fillx filly }

if {$advanced} {
	pack append $t.f \
		$t.fq {right fillx filly } \
		$t.fr {right fillx filly } \
		$t.fn {right  fillx filly expand} \
		$t.fz {left  fillx filly}\
		$t.frd {left fillx filly} 
} else {
	pack append $t.f \
		$t.fr {right fillx filly } \
		$t.fn {right  fillx filly expand} \
		$t.fz {left  fillx filly}\
}

	pack append $t.fc \
		$c {top expand fill}
		
	pack append [mactoolbar::getframe] $t.f {bottom fillx}

	pack append $t.rest \
		 \
		$t.vscroll {right filly} \
		$t.hscroll {bottom fillx} \
		$t.fc {top expand fill}
		
	pack $t.tbf -side top -fill x
	pack $t.rest -side bottom -expand 1 -fill both

	bind $t "=" "zoom $nr $zfact"
	bind $t "-" "zoom $nr [expr {1.0/$zfact}]"

	# scan/drag code originally added to xspin by Geert Janssen, TUE
	if {[string compare [tk windowingsystem] aqua] == 0} {
		bind $c <3> "$c scan mark %x %y ; panclones $nr %x %y"
		bind $c <B3-Motion> "$c scan dragto %x %y"
		bind $c <Control-B3-Motion> "panclones $nr %x %y"
	} else {
		bind $c <2> "$c scan mark %x %y ; panclones $nr %x %y"
		bind $c <B2-Motion> "$c scan dragto %x %y"
		bind $c <Control-B2-Motion> "panclones $nr %x %y"
	}
	
	wm protocol $t WM_DELETE_WINDOW "mydestroy $nr"
	wm protocol $t WM_SAVE_YOURSELF "mydestroy $nr"
	wm title $t $title
	bind $t <q>  "quit $nr"
	bind $t <Q>  "quit $nr"

	# instead of navigation
	# use button-1 to edit
	# use button-3 for context menu:
	#   on object (node/graph) have object properties
	#   on background have graph properties + save etc options
#	bind $c <1> "next $nr"
#	bind $c <3> "prev $nr"

	bind $c <ButtonPress-1> "mouse_b1_press $nr %x %y"
	bind $c <B1-Motion> "mouse_b1_motion $nr %x %y"
	bind $c <ButtonRelease-1> "mouse_b1_release $nr %x %y"

	set m [menu $c.menu -tearoff 1]
	global mlast
	set mlast ""
	$m add command -label "New" \
		-command "set mlast [expr [$m index end] + 1]; winnew $nr"
	$m add command -label "Open" \
		-command "set mlast [expr [$m index end] + 1]; winopen $nr"
	$m add separator
	$m add checkbutton -label "Advanced" \
		-command "set mlast [expr [$m index end] + 1]; setadvanced $nr"
	$m add separator
	$m add command -label "Info" \
		-command "set mlast [expr [$m index end] + 1]; showinfo $nr"
	$m add command -label "Connect mcast" \
		-command "set mlast [expr [$m index end] + 1]; try_connect_mcast $nr"
	$m add separator
	$m add command -label "Save As .dot" \
		-command "set mlast [expr [$m index end] + 1]; savedot $nr"
	$m add command -label "Save As .aut" \
		-command "set mlast [expr [$m index end] + 1]; saveaut $nr"
	$m add command -label "Save As .ps" \
		-command "set mlast [expr [$m index end] + 1]; saveps $nr"
	$m add separator
	$m add command -label "Graph Attributes" -underline 0 \
		-command "set mlast [expr [$m index end] + 1]; setAttributes $nr graph $m %X %Y \$mlast"
	$m add command -label "Node Attributes" -underline 0 \
		-command "set mlast [expr [$m index end] + 1]; setAttributes $nr node $m %X %Y \$mlast"
	$m add command -label "Edge Attributes" -underline 0 \
		-command "set mlast [expr [$m index end] + 1]; setAttributes $nr edge $m %X %Y \$mlast"
	$m add separator
	$m add command -label "Print Setup ..." -underline 0 \
		-command "set mlast [expr [$m index end] + 1]; printSetup $nr"
	$m add command -label "Print" -underline 0 \
		-command "set mlast [expr [$m index end] + 1]; print $nr"

	# bind $c <3> "tk_popup $m %X %Y \$mlast"
	if {[string compare [tk windowingsystem] aqua] == 0} {
		bind $c <2> "setAttributes $nr {} $m %X %Y \$mlast"
	} else {
		bind $c <3> "setAttributes $nr {} $m %X %Y \$mlast"
	}

	tkwait visibility $c
	return $nr
}
proc setadvanced {nr} {
	global advanced
	global windows

	if {$advanced} {
		set advanced 0
	} else {
		set advanced 1
	}

	foreach t $windows {
		if {$advanced } {
			# puts stderr "packing $t..."
			pack configure $t.fq \
				-side right -fill both \
				-in $t.f \
				-before $t.fr
			pack configure $t.frd \
				-side left -fill both \
				-in $t.f \
				-after $t.fz
		} else {
			# puts stderr "forgetting $t..."
			pack forget  $t.fq
			pack forget  $t.frd
		}
	}
}
proc winnew {nr} {
	continue_reading_after_process_args "" ""  stderr
}
proc winopen {nr} {
	upvar #0 W_[set nr](id) id

	set fname [askfilename $id Dotfile .dot open]
	if {[string compare $fname ""] != 0} {
		continue_reading_after_process_args $fname ""  stderr
	}
}

proc showinfo {nr} {
	upvar #0 W_[set nr](c) c
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](cfid) cfid
	upvar #0 G_[set id](mcast) mcast
	upvar #0 S_[set id](nr) w_nrs
	global B

	set msg ""
	append msg "Listening on host $B(host) at $B(listener)\n"
	if {[string compare $cfid ""] != 0} {
		append msg "Connected to mcast server at $mcast\n"
	} else {
		append msg "No mcast\n"
	}

	tk_messageBox -parent $c -message $msg
}
proc do_try_mcast {nr w} {
	upvar #0 W_[set nr](c) c
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](cfid) cfid
	upvar #0 G_[set id](ofid) ofid
	upvar #0 G_[set id](mcast) mcast
	upvar #0 G_[set id](g) g

	set mcast [string trim [$w.e get]]
	destroy $w
	if {[string compare $mcast ""] == 0} {
		# tk_messageBox -parent $c -message "mcast must be nonempty string"
		warning  "mcast must be nonempty string"
		set mcast ""
		return
	}
	set cfid [connect_mcast $mcast $ofid]
	if {[string compare $cfid ""] != 0} {
		message "connected to mcast server at $mcast"
		fileevent $cfid readable [list handle $cfid "<mcast>" mcastinterp mcastcleanup $id $g ]
	} else {
		# tk_messageBox -parent $c -message "failed to connect to mcast $mcast"
		warning "failed to connect to mcast $mcast"
		set mcast ""
	}
}

proc try_connect_mcast {nr} {
	upvar #0 W_[set nr](c) c
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](cfid) cfid
	upvar #0 G_[set id](mcast) mcast
	global B

	if {[string compare $cfid ""] != 0} {
		tk_messageBox -parent $c -message "Already connected to mcast server at $mcast"
		return
	}

	toplevel .mc
	entry .mc.e -textvariable v
	label .mc.l -text mcast:
	pack .mc.l
	pack .mc.e
	bind .mc.e <Return> "do_try_mcast $nr .mc"

}

proc panclones {nr mx my} {
	upvar #0 W_[set nr](c) c
	upvar #0 W_[set nr](id) id
	upvar #0 S_[set id](nr) w_nrs

	set canvas $c
	set offsety [$canvas canvasy 0]
	set offsetx [$canvas canvasx 0]
	set offsetx [expr $offsetx + 5] ;# margin
	set cx [$c canvasx $mx]
	set cy [$c canvasy $my]
	set bbox [$c bbox all]
	# puts "panclones $nr $c mx $mx my $my cx $cx cy $cy bb ($bbox)"
	if {[llength $bbox] != 4} {
		return
	}
	set bboxtlx [lindex $bbox 0]
	set bboxtly [lindex $bbox 1]
	set bboxbrx [lindex $bbox 2]
	set bboxbry [lindex $bbox 3]
	set bboxw [expr $bboxbrx - $bboxtlx]
	set bboxh [expr $bboxbry - $bboxtly]
	set nx [expr  $cx - $bboxtlx]
	set fx [expr $nx / $bboxw]
	set ny [expr $cy - $bboxtly]
	set fy [expr $ny / $bboxh]

	# puts "panclones $nr $c mx $mx my $my cx $cx cy $cy bb ($bbox) bbw $bboxw bbh $bboxh nx $nx ny $ny fx $fx fy $fy"
	# puts stderr "offset ox=lb $offsetx oy=tb $offsety"
	# puts stderr "bbox=($bbox)"
	foreach w_nr $w_nrs {
		upvar #0 W_[set w_nr](c) w_c
		# puts "$w_nr $w_c [$w_c configure]"
		# puts "$w_nr $w_c [$w_c xview]"
		# puts "$w_nr $w_c [$w_c yview]"
		if {$w_nr != $nr} {
			set canvas $w_c

			set bbox [$canvas bbox all]
			set bboxtlx [lindex $bbox 0]
			set bboxtly [lindex $bbox 1]
			set bboxbrx [lindex $bbox 2]
			set bboxbry [lindex $bbox 3]
			set bboxw [expr $bboxbrx - $bboxtlx]
			set bboxh [expr $bboxbry - $bboxtly]
			# puts stderr "bbox=($bbox)"

			set cnx [expr ($bboxw * $fx) + $bboxtlx]
			set cny [expr ($bboxh * $fy) + $bboxtly]

			if {0} {
				set items [$canvas find overlapping [expr $cnx - 5] [expr $cny - 5] [expr $cnx + 5] [expr $cny + 5]]
				puts "items $items"
				set bbox [$canvas bbox $items]
				# puts "cnx $cnx cny $cny bbox $bbox"
				if {[llength $bbox] != 4 } {
					set bbox [list [expr $cnx - 20] [expr $cny - 20] [expr $cnx + 20] [expr $cny + 20]]
				}
			}
			set zd [zoomdepth $w_nr]
			set sz [expr $zd * 20]
			set bbox [list [expr $cnx - $sz] [expr $cny - $sz] [expr $cnx + $sz] [expr $cny + $sz]]
			# puts "zd $zd cnx $cnx cny $cny bbox $bbox"

			set bboxtlx [lindex $bbox 0]
			set bboxtly [lindex $bbox 1]
			set bboxbrx [lindex $bbox 2]
			set bboxbry [lindex $bbox 3]
			set bboxw [expr $bboxbrx - $bboxtlx]
			set bboxh [expr $bboxbry - $bboxtly]
			# puts stderr "bbox=($bbox)"

			set offsety [$canvas canvasy 0]
			set offsetx [$canvas canvasx 0]
			set offsetx [expr $offsetx + 5] ;# margin
			set width [winfo width $canvas]
			set topboundary $offsety
			set botboundary [expr $offsety + [winfo height $canvas]]
			set leftboundary $offsetx
			set rightboundary [expr $offsetx + $width]
			# puts stderr "offset ox=lb $offsetx oy=tb $offsety rb $rightboundary bb $botboundary cnx $cnx cny $cny"
			set dragx 0
			set dragy 0
			if {$bboxtly < $topboundary} {
				set newtopboundary [expr $bboxtly - $bboxh]
				set dragy [expr int($topboundary - $newtopboundary)]
			} elseif {$bboxbry > $botboundary} {
				set newbotboundary [expr $bboxbry + $bboxh ]
				set dragy [expr int($botboundary - $newbotboundary)]
			}
			# puts stderr "offset ox=lb $offsetx oy=tb $offsety rb $rightboundary bb $botboundary cnx $cnx cny $cny"
			set leftmiss [expr $bboxtlx < $leftboundary ]
			set rightmiss [expr $bboxbrx > $rightboundary ]
			if {$bboxw > $width} {
				# puts -nonewline stderr "bwt"
				set newleftboundary [lindex $bbox 0]
				set dragx [expr int($leftboundary - $newleftboundary)]
			} elseif {($bboxw <= $width) && ($leftmiss || $rightmiss)} {
				# puts -nonewline stderr "lor\t"
				set newleftboundary [expr $bboxtlx - (($width - $bboxw) / 2)]
				set dragx [expr int($leftboundary - $newleftboundary)]
				# set newrightboundary [expr $bboxbrx + $bboxw ]
				# set dragx [expr int($rightboundary - $newrightboundary)]
			} else {
			}
			if {($dragx != 0) || ($dragy != 0)} {
				$canvas scan mark 0 0
				# puts stderr "canvas $canvas drag $dragx $dragy"
				# Older version of wish (wish8.1 and older?) do not allow
				# a third parameter to scan dragto , so there the following
				# scan  dragto command does not work.
				# $canvas scan dragto $dragx $dragy 1
				# The code below has the same effect, but uses scroll commands.
				set region [lindex [$canvas configure -scrollregion] 4]
				# puts stderr "canvas $canvas region $region"
				if {$dragy != 0} {
					set regiontly [lindex $region 1]
					set regionbry [lindex $region 3]
					set regionh [expr $regionbry - $regiontly]
					set curofffracy [lindex [$canvas yview] 0]
					set curoffy [expr $curofffracy * $regionh]
					set newoffy [expr $curoffy - $dragy]
					set newofffracy [expr $newoffy / $regionh.0]
					# puts stderr "canvas yview [$canvas yview]"
					# puts stderr "canvas yview moveto $newofffracy"
					$canvas yview moveto $newofffracy
				}
				if {$dragx != 0} {
					set regiontlx [lindex $region 0]
					set regionbrx [lindex $region 2]
					set regionw [expr $regionbrx - $regiontlx]
					set curofffracx [lindex [$canvas xview] 0]
					set curoffx [expr $curofffracx * $regionw]
					set newoffx [expr $curoffx - $dragx]
					set newofffracx [expr $newoffx / $regionw.0]
					# puts stderr "canvas xview [$canvas xview]"
					# puts stderr "canvas xview moveto $newofffracx"
					$canvas xview moveto $newofffracx
				}
			}
		}
	}
}

proc scaletofittrigger {nr} {
	upvar #0 W_[set nr](autoFit) autoFit
	upvar #0 W_[set nr](destroying) destroying

	debug 1 "scaletofittrigger ($nr)($destroying) {"
	if {$destroying} {
		return
	}
	if {$autoFit} {
		scaletofit $nr
	}
	debug 1 "scaletofittrigger ($nr)($destroying) }"
}

proc scaletofit {nr} {
	upvar #0 W_[set nr](c) c

	debug 1 "scaletofit ($nr) {"
	set bbox [$c bbox all]
	set cdx [expr [winfo width $c]-4]
	set cdy [expr [winfo height $c]-4]
#	puts stderr "bbox=($bbox) dx=$cdx dy=$cdy"

	if {$cdx <= 0 || $cdy <= 0} {
		return
	}

	if {[llength $bbox]} {
		$c configure -scrollregion $bbox
		set gdx [expr [lindex $bbox 2] - [lindex $bbox 0]]
		set gdy [expr [lindex $bbox 3] - [lindex $bbox 1]]
		set xscale [expr $cdx / $gdx.0]
		set yscale [expr $cdy / $gdy.0]
#		if {$xscale < 1.0 || $yscale < 1.0} {
			if {$xscale < $yscale} {
				zoom $nr $xscale
			} else {
				zoom $nr $yscale
			}
#		}
	} {
		$c configure -scrollregion [list -4 -4 \
			[expr {[winfo width $c]-4}] \
			[expr {[winfo height $c]-4}]]
	}

	debug 1 "scaletofit ($nr) }"
}

proc getattrval {al n} {
	set r ""
	set i 0
	set ll [llength $al]
	while {$i < $ll} {
		if {[string compare $n [lindex $al $i]] == 0} {
			set r [lindex $al [expr $i + 1]]
		}
		incr i
		incr i
	}
	return $r
}

proc colorparse {id g words} {
	upvar #0 G_[set id](ofid) ofid
	global color
	global G

	set col $color(highlight)
	set mode kind
	set kind node
	set literal 0
	set resStep {}
	set resAdd {}
	set additive 0
	debug 1 "colorparse ($id)($g)($words)"
	foreach w $words {
		debug 1 "  colorparse ($id)($g)($words): mode=$mode w=$w"
		if {[string compare $w ""] == 0} {
			# skip
		} elseif {(!$literal) && [regexp {^-} $w]} {
			switch -exact -- $w {
				  -c {
					set prevmode $mode
					set mode color
				} -e {
					set mode kind
					set kind edge
					set type step
				} -n {
					set mode kind
					set kind node
					set type step
				} -E {
					set mode kind
					set kind edge
					set type add
				} -N {
					set mode kind
					set kind node
					set type add
				} -- {
					set literal 1
				} default {
					catch {puts $ofid "$cprog: unknown command: $w"}
					return {}
				}
			}
		} else {
			set literal 0
			switch -exact -- $mode {
				  color {
					if {![catch {winfo rgb . $w} msg]} {
						set col $w
					} else {
						catch {puts $ofid "$cprog: unknown color name, using $col instead: $w"}
					}
					set mode $prevmode
				} kind {
					set n [find $id $w $kind]
					debug 1 "	$n (== find $id $w $kind)"
					if {[llength $n] == 0} {
						catch {puts $ofid "$cprog: cannot find $kind: $w"}
					} else {
						foreach ne $n {
							set modes($kind,$ne) $kind
							set names($kind,$ne) $ne
							lappend types($kind,$ne)  $type
							set colors($kind,$ne,$type) $col
						}
					}
				} default {
					catch {puts $ofid "$cprog: internal error: unknown mode: $mode"}
					return {}
				}
			}
		}
	}
	foreach k [array names names] {
		debug 1 "	$k ($types($k) [llength $types($k)] $modes($k) $names($k) )"
		if {[lsearch $types($k) add] >= 0} {
			lappend resAdd [list $modes($k) $names($k) $colors($k,add)]
		}
		if {[lsearch  $types($k) step] >= 0} {
			lappend resStep [list $modes($k) $names($k) $colors($k,step)]
		}
	}
	set res [list $resStep $resAdd]
	debug 1 "colorparse ($id)($g)($words): ($res)"
	return $res
}

# with mode == set : store current, and set given color
# with mode == reset : restore stored, and ignore given color
# type==step  for normal dot updates
# type==add   for 'sticky' additive updates
# idea: we have three layers of colors, from bottom to top:
#  - base layer: color that is already there
#  - middle layer: additive 'sticky' colors
#  - top layer: colors for the current step
proc changecolor {mode id c type tag attrs color} {
	upvar #0 G_[set id]_col col
	foreach i [$c find withtag $tag] {
		foreach a $attrs {
		debug 1 "changecolor ($id)($c)($i)($mode)($type)($tag)($a): ($color)"
			if {![catch {$c itemcget $i $a} val]} {
				if {[string compare $mode "set"] == 0} {
					if {[string compare $type "add"] == 0} {
						if {![info exists col($c,$i,$type,count,$a)]} {
							set col($c,$i,$type,count,$a) 1
						} else {
							incr col($c,$i,$type,count,$a) 1
						}
						debug 1 "changecolor after add ($id)($c)($mode)($type)($tag)($a) count: $col($c,$i,$type,count,$a)"
						if {![info exists col($c,$i,add,$a)]} {
							set col($c,$i,add,$a) $color
							debug 1 "changecolor set($c,$i,add,$a) : $color"
						}
					}
					if {![info exists col($c,$i,base,$a)]} {
						debug 1 "changecolor set($c,$i,base,$a) : $val"
						set col($c,$i,base,$a) $val
					}
					# puts stderr "$c itemconfigure $i $a $color"
					$c itemconfigure $i $a $color 
				} else  {
					# reset
					if {[string compare $type "add"] == 0} {
						if {[info exists col($c,$i,$type,count,$a)]} {
							debug 1 "changecolor reset sub ($id)($c)($mode)($type)($tag)($a) count: $col($c,$i,$type,count,$a)"
							incr col($c,$i,$type,count,$a) -1
							if {$col($c,$i,$type,count,$a) < 1} {
								unset col($c,$i,$type,count,$a)
								if {[info exists col($c,$i,add,$a)]} {
									unset col($c,$i,add,$a)
								}
							}
						}
						if {![info exists col($c,$i,$type,count,$a)]} {
							if {[info exists col($c,$i,base,$a)]} {
								set color $col($c,$i,base,$a)
								unset col($c,$i,base,$a)
								# puts stderr "$c itemconfigure $i $a $color"
								debug 1 "changecolor add reset now  ($id)($c)($mode)($type)($tag)($a) : $color"
								$c itemconfigure $i $a $color
							} else {
								debug 1 "changecolor add reset count 0 no col($c,$i,base,$a)"
							}
						} else {
							debug 1 "changecolor add reset later col($c,$i,base,$a): $col($c,$i,base,$a)  count: $col($c,$i,$type,count,$a)"
						}
					} else {
						#step
						if {[info exists col($c,$i,add,$a)]} {
							set color $col($c,$i,add,$a)
							# puts stderr "$c itemconfigure $i $a $color"
							debug 1 "changecolor step reset to add ($id)($c)($mode)($type)($tag)($a) : $color"
							$c itemconfigure $i $a $color
						} elseif {[info exists col($c,$i,base,$a)]} {
							set color $col($c,$i,base,$a)
							unset col($c,$i,base,$a)
							# puts stderr "$c itemconfigure $i $a $color"
							debug 1 "changecolor step reset to base  ($id)($c)($mode)($type)($tag)($a) : $color"
							$c itemconfigure $i $a $color
						} else {
							debug 1 "changecolor step reset  no col($c,$i,add/base,$a)"
						}
					}
					set color ""
				}
			}
		}
	}
}

# type==step  for normal dot updates
# type==add   for 'sticky' additive updates
proc colors {mode id c type pfx l} {
	foreach t $l {
		debug 1 "setcolors ($id)($c)($mode)($type)($pfx): ($l)"
		switch -exact -- [lindex $t 0] {
			  node {
				changecolor $mode $id $c $type 1[lindex $t 1] -fill [tkBright [lindex $t 2]]
			} edge {
				changecolor $mode $id $c $type 0[lindex $t 1] {-fill -outline} [tkDark [lindex $t 2]]
				changecolor $mode $id $c $type 1[lindex $t 1] {-fill -outline} [lindex $t 2]
			}
		}
	}
}

proc findJoinIdx {I cur oldI old} {
	debug 1 "findJoinIdx ($I)($cur)($oldI)($old)"
	set nI [llength $I]
	set noldI [llength $oldI]
	set i 0
	set ok 1
	while {$ok && $i < $nI && $i < $noldI} {
		if {[string compare [lindex $I $i] [lindex $oldI $i]] == 0} {
			set ok 1
			incr i
		} else {
			set ok 0
		}
	}
	set res [expr ($i - 1)]		
	debug 1 "findJoinIdx ($I)($cur)($oldI)($old) : $res"
	return $res
}

# TODO: when difference  |scur - oldscur| > 1
# we have to visit intermediate steps to add/remove colors
# we should optimize this somehow - cache intermediate situations?
proc showcur {nrs} {
	global color
	foreach nr $nrs {
		upvar #0 W_[set nr](id) id
		upvar #0 W_[set nr](step) step
		upvar #0 W_[set nr](c) c
		upvar #0 G_[set id](scur) scur
		upvar #0 G_[set id](oldscur) oldscur
		upvar #0 G_[set id](Q) Q
		upvar #0 G_[set id](oldQ) oldQ
		upvar #0 G_[set id](I) I
		upvar #0 G_[set id](oldI) oldI
		upvar #0 G_[set id](g) g

		debug 1 "showcur ($nr)($id)($step)($c)($scur)($oldscur) {"
		set old $oldscur
		if {[string compare $old ""] == 0} {
			set old -1
		}
		set cur $scur
		if {[string compare $cur ""] == 0} {
			set cur -1
		}
		
		set joinIdx [findJoinIdx $I $cur $oldI $old]
		
		if {$old >=  0} {
			set lsa [colorparse $id $g [lindex $oldQ $old]]
			set l [lindex $lsa 0]
			set a [lindex $lsa 1]
			colors reset $id $c step "$old->$cur" $l
			set p $old
			while {$p > $joinIdx} {
				colors reset $id $c add "$p->$cur" $a
				incr p -1
				set lsa [colorparse $id $g [lindex $oldQ $p]]
				set a [lindex $lsa 1]
			}
			while {$p > $cur} {
				colors reset $id $c add "$p->$cur" $a
				incr p -1
				set lsa [colorparse $id $g [lindex $oldQ $p]]
				set a [lindex $lsa 1]
			}
		}
		if {$cur >= 0} {
			if {$joinIdx < $old} {
				set p $joinIdx
			} else {
				set p $old
			}
			while {$cur > $p} {
				set lsa [colorparse $id $g [lindex $Q $p]]
				set a [lindex $lsa 1]
				colors set $id $c add "$p->$cur" $a
				incr p 1
			}
			set lsa [colorparse $id $g [lindex $Q $cur]]
			set l [lindex $lsa 0]
			colors set $id $c step "$old->$cur" $l
		}
		set step $scur
		debug 1 "showcur ($nr)($id)($step)($c)($scur)($oldscur) }"
	}
}

proc showq {nr tell cur} {
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](Q) Q
	upvar #0 G_[set id](scur) scur
	upvar #0 G_[set id](oldscur) oldscur
	upvar #0 S_[set id](nr) w_nr

	debug 1 "showq ($nr)($tell)($cur)"
	set last [llength $Q]
	if {$cur >= 0  && $cur < $last} {
		set oldscur $scur
		set scur $cur
		showcur $w_nr
		if {$tell} {
			mcastsend $id $cur
		}
	}
}

proc prev {nr} {
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](scur) scur
	set idx $scur
	showq $nr 1 [expr $idx -1]
}

proc next {nr} {
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](scur) scur
	set idx $scur
	showq $nr 1 [expr $idx +1]
}

proc parseedges {espec} {
	set i [string first -> $espec]
	while {$i >= 0} { 
		set e [string trim [string range $espec 0 [expr $i - 1]]]
		# puts "e=$e"
		if {[string compare $e ""] != 0} {
			lappend nlist $e
		}
		set espec [string range $espec [expr $i + 2] end]
		set i [string first -> $espec]
	}
	set e [string trim $espec]
	# puts "e=$e"
	if {[string compare $e ""] != 0} {
		lappend nlist $e
	}
	return $nlist
}

proc ctlparse { id g words ofid } {
       upvar #0 G_[set id]_name2graph name2graph
       upvar #0 G_[set id](doRender) doRender
       upvar #0 G_[set id](delayRender) delayRender
       upvar #0 G_[set id](doUpdate) doUpdate

       set w0 [lindex $words 0]
       if {[string compare $w0 render] == 0} {
               set n  [lindex $words 1]
               switch -exact [string tolower $n] {
                       0 -
                       off {
                               set doRender 0
                               set delayRender 0
                       }
                      delay {
                               set doRender 0
                               set delayRender 1
                       }
                        1 -
                       on {
                               set doRender 1
                               set delayRender 0
                               do_render $id
                       }
               }
       } elseif {[string compare $w0 update] == 0} {
               set n  [lindex $words 1]
               switch -exact [string tolower $n] {
                       0 -
                       off {
                               set doUpdate 0
                       }
                        1 -
                       on {
                               set doUpdate 1
                               update idletasks
                       }
               }
       }
}


# node name [attrname attrval [attrname attrval [...]]]
# edge fromname toname [attrname attrval [attrname attrval [...]]]
#   if fromname or toname does not yet exist, create on demand
proc dotparse { id g words ofid } {
	upvar #0 G_[set id]_name2graph name2graph
	global G

	set w0 [lindex $words 0]
	if {[string compare $w0 node] == 0} {
		set n [lindex $words 1]
		if {[llength [findnode $id $n]] > 0} {
			# already there; skip for now
			# (we may want to use its attributes later,
			# to update existing node)
		} else {
			set attrs [lrange $words 2 end]
			set lname [getattrval $attrs label]
			set sgname [getattrval $attrs subgraph]
			if {[string compare $sgname ""] != 0} {
				if {[info exists name2graph($g,$sgname)]} {
					set sg $name2graph($g,$sgname)
					set cmd [concat $sg addnode $n $attrs]
					set dummyvar name2graph(dummy,$sg)
				} else {
					catch {puts $ofid "$G(cprog): no subgraph named: $sgname"}
					set cmd [concat $g addnode $n $attrs]
					set dummyvar name2graph(dummy,$g)
				}
			} else {
				set cmd [concat $g addnode $n $attrs]
			}
			if {[catch $cmd nn]} {
				catch {puts $ofid "$G(cprog): cannot create node $n: $nn"}
			} else {
				# puts "created node ($n): $nn"
				if {[string compare $lname ""] != 0 && [string compare $lname "{}"] != 0} {
					addalias $id node $nn $lname
				}
				updatenodepfx $id $nn
				if {[info exists dummyvar] && [info exists $dummyvar]} {
					set dn [set $dummyvar]
					$dn delete
					# puts "deleteted dummy node ($dn)"
					catch {unset $dummyvar}
				}
			}
		}
	} elseif {[string compare $w0 delnode] == 0} {
		foreach n [findnode $id [lindex $words 1]] {
			delobj $id $n
		}
	} elseif {[string compare $w0 deledge] == 0} {
		foreach n [findedge $id [lindex $words 1]] {
			delobj $id $n
		}
	} elseif {[string compare $w0 edge] == 0} {
		set f {}
		set t {}
		foreach n [parseedges [lindex $words 1]] {
			set nn [findnode $id $n]
			if {[llength $nn] <= 0} {
				if {[catch {$g addnode $n} nn]} {
					catch {puts $ofid "$G(cprog): cannot create node $n: $nn"}
					set nn {}
				} else {
					# puts "created node ($n): $nn"
					updatenodepfx $id $nn
				}
			}
			set t $nn
			if {[llength $f] > 0 && [llength $t] > 0 } {
				foreach nse $f {
					foreach nte $t {
						set cmd [concat $g addedge $nse $nte [lrange $words 2 end]]
						# puts "cmd: $cmd"
						if {[catch $cmd e]} {
							catch {puts $ofid "$G(cprog): cannot create edge $nse-> $nte: $e"}
						}
						if {![catch {$e queryattributes name} n]} {
							if {[string compare $n ""] != 0 && [string compare $n "{}"] != 0} {
								addalias $id edge $e $n
							}
						}
					}
				}
			}
			set f $t
		}
	} elseif {[string compare $w0 subgraph] == 0} {
		set n [lindex $words 1]
		if {[info exists name2graph($g,$n)]} {
			# already there; skip for now
			# (we may want to use its attributes later,
			#  to update existing subgraph)
		} else {
			set attrs [lrange $words 2 end]
			set sgname [getattrval $attrs subgraph]
			if {[string compare $sgname ""] != 0} {
				if {[info exists name2graph($g,$sgname)]} {
					set sg $name2graph($g,$sgname)
					set cmd [concat $sg addsubgraph $n $attrs]
					set dummyvar name2graph(dummy,$sg)
				} else {
					catch {puts $ofid "$G(cprog): no subgraph named: $sgname"}
					set cmd [concat $g addsubgraph $n $attrs]
					set dummyvar name2graph(dummy,$g)
				}
			} else {
				set cmd [concat $g addsubgraph $n $attrs]
			}
			# puts "cmd: $cmd"
			if {[catch $cmd e]} {
				catch {puts $ofid "$G(cprog): cannot create subgraph $n: $e"}
			} else {
				set name2graph($g,$n) $e
				set name2graph(dummy,$e) [$e addnode _dummy_$e label "" style invis]
				# puts "created subgraph \"$n\": $e ; dummynode ($n): $name2graph(dummy,$e)"
				# $e setattributes name [$e showname]
				if {[info exists dummyvar] && [info exists $dummyvar]} {
					set dn [set $dummyvar]
					$dn delete
				}
			}
			debug 1 "name2graph($g $n) == $e"
		}
	} elseif {[string compare $w0 setgraphattr] == 0} {
		set attrs [lrange $words 1 end]
		set cmd [concat $g setattributes $attrs]
		if {[catch $cmd nn]} {
			catch {puts $ofid "$G(cprog): cannot set graph attributes: $nn"}
		}
	}
}

proc cmdinterp { id line chan argl } {
	upvar #0 G_[set id](Q) Q
	upvar #0 G_[set id](oldQ) oldQ
	upvar #0 G_[set id](I) I
	upvar #0 G_[set id](oldI) oldI
	upvar #0 S_[set id](nr) w_nrs
	upvar #0 G_[set id](doRender) doRender
	upvar #0 G_[set id](delayRender) delayRender
	upvar #0 G_[set id](isChanged) isChanged
	upvar #0 G_[set id](doUpdate) doUpdate
	upvar #0 G_[set id](ofid) ofid
	upvar #0 G_[set id](g) g
	upvar #0 G_[set id]_treeNodeName2Q treeNodeName2Q
	upvar #0 G_[set id]_treeNodeName2I treeNodeName2I

	debug 1 "cmdinterp ($line) ($argl)"
	set g [lindex $argl 0]
	set ofid [lindex $argl 1]
	set words [mysplit $line]
	set w0 [lindex $words 0]
	if {[string compare $w0 dot] == 0} {
		set isChanged 1
		dotparse $id $g [lrange $words 1 end] $ofid
		if {$doUpdate} {
			update
			update idletasks
		}
		if {$doRender} {
			do_render $id
		}
	} elseif {[string compare $w0 ctl] == 0} {
		ctlparse $id $g [lrange $words 1 end] $ofid
	} else {
		debug 1 "cmdinterp else branch ($words)"
		if {$delayRender && $isChanged} {
			do_render $id
			set isChanged 0
		}
		if {[string compare $w0 tree] == 0} {
			set treeNodeSrc [lindex $words 1]
			set treeNodeDst [lindex $words 2]
			if {[info exists treeNodeName2Q($treeNodeSrc)]} {
				set newQ $treeNodeName2Q($treeNodeSrc)
				set newI $treeNodeName2I($treeNodeSrc)
			} else {
				set newQ {}
				set newI {}
			}
			# regsub -all red [lindex $Q end] grey pfx
			debug 1 "cmdinterp init $treeNodeSrc Q: $Q"
			# set words [concat $pfx  [lrange $words 3 end]]
			set words [lrange $words 3 end]
			set treeId $treeNodeDst
			debug 1 "cmdinterp wordsext $words"
			set treeNodeName2I($treeNodeDst) $newI
		} else {
			set newQ $Q
			set newI $I
			set treeId ""
		}

#		lappend newQ [colorparse $id $g $words]
		lappend newQ $words
		lappend newI $treeId
		if {[info exists treeNodeDst]} {
			set treeNodeName2Q($treeNodeDst) $newQ
			set treeNodeName2I($treeNodeDst) $newI
			debug 1 "cmdinterp update $treeNodeDst newQ: $newQ"
		}
		if {$doUpdate} {
			if {[info exists treeNodeDst]} {
				set oldQ $Q
				set oldI $I
			} else {
				set oldQ $newQ
				set oldI $newI
			}
			set Q $newQ
			set I $newI
			set idx [expr [llength $Q] - 1]
			showq [lindex $w_nrs 0] 0 $idx
			set oldQ $Q
			set oldI $I
			update idletasks
		}
	}
	return 1
}

proc cmdcleanup { id chan argl } {
	upvar #0 G_[set id](ifid) ifid
	upvar #0 G_[set id](ieof) ieof
	upvar #0 S_[set id](nr) w_nr

debug 0 "cmdcleanup $id $chan $argl"
	if {[string compare $ifid $chan] == 0} {
		set ieof 1
		foreach nr $w_nr {
			update_reuse $nr
			update_keymenu $nr
		}
		if {[info exists ifid] && ([string compare $ifid ""] != 0)} {
			fileevent $ifid readable {}
			close $ifid
			set ifid ""
		}
	}
}


proc mcastinterp { id line chan argl } {
	upvar #0 G_[set id](Q) Q
	upvar #0 G_[set id](oldQ) oldQ
	upvar #0 G_[set id](I) I
	upvar #0 G_[set id](oldI) oldI
	upvar #0 S_[set id](nr) w_nr
	upvar #0 G_[set id]_treeNodeName2Q treeNodeName2Q
	upvar #0 G_[set id]_treeNodeName2I treeNodeName2I

	debug 1 "mcastinterp ($line) ($argl)"
	set g [lindex $argl 0]
	set w0 [lindex $line 0]
	set w1 [lindex $line 1]
	set w2 [lindex $line 2]
	if {[isinteger  $w0]} {
		showq [lindex $w_nr 0] 0 $w0
	} elseif {[string compare $w0 tree] == 0} {
		set treeRef $w1
		if {[info exists treeNodeName2Q($treeRef)]} {
			set newQ $treeNodeName2Q($treeRef)
			set newI $treeNodeName2I($treeRef)
			debug 1 "mcastinterp before $treeRef Q: $Q"
			set Q $newQ
			set I $newI
			debug 1 "mcastinterp after $treeRef Q: $Q"
			if {[isinteger  $w2]} {
				set idx $w2
			} else {
				set idx [expr [llength $Q] - 1]
			}
			showq [lindex $w_nr 0] 0 $idx
			set oldQ $Q
			set oldI $I
			update idletasks
		}
	}
	return 1
}

proc mcastcleanup { id chan argl } {
	upvar #0 G_[set id](cfid) cfid
	upvar #0 G_[set id](mcast) mcast
	upvar #0 G_[set id](ieof) ieof
	upvar #0 S_[set id](nr) nr

	if {[string compare $cfid $chan] == 0} {
		message "mcast server at $mcast closed connection"
		set cfid ""
		set mcast ""
	}
}

proc mcastsend {id nr} {
	upvar #0 G_[set id](cfid) cfid

# puts stderr "mcastsend $id $nr $cfid"
	if {[string compare $cfid ""] != 0} {
		if {[catch {output $cfid $nr} msg]} {
			debug 1 "mcastsend $nr -> $msg"
		} else {
			debug 1 "mcastsend $nr OK?"
		}
	}
}

proc connect_mcast {mcast ofid } {
	set cfid {}
	set fields [split $mcast "!"]
#puts stderr "connect_mcast $fields"
	if {([llength $fields] == 3) &&
		([string compare [lindex $fields 0] tcp] == 0)} {
		set host [lindex $fields 1]
		set port [lindex $fields 2]
		if {[catch {socket $host $port} foo]} {
			warn $ofid "cannot connect to mcast $mcast: $foo"
		} else {
			warn $ofid "connected to mcast $mcast"
			set cfid $foo
			fconfigure $cfid -blocking 0
		}
	}
	return $cfid
}

proc handle { chan info hproc cproc id args } {
	debug 1 "handle $chan $info $hproc $cproc"
	fileevent $chan readable ""
	set read [gets $chan line]
	debug 1 "handle $chan $info $hproc $cproc read: $read"
	if {$read < 0} {
		if {[eof $chan]} {
			debug 0 "Connection closed by client $info."
			 $cproc $id $chan $args
		} elseif {[fblocked $chan]} {
			warn stderr "handle $info $hproc $cproc blocked: $line"
			after 1 [list catch [list fileevent $chan readable \
				[concat handle $chan $info $hproc $cproc $id $args ]]]
		} else {
			warn stderr "handle $info error?: $line"
			$cproc $id $chan $args
		}
	} else {
		debug 1 "handle $info: $line"
		set res [$hproc $id $line $chan $args]
		if {$res > 0} {
			after 10 [list catch [list fileevent $chan readable \
				[concat handle $chan $info $hproc $cproc $id $args]]]
		} elseif {$res < 0} {
			catch {close $chan}
			$cproc $id $chan $args
		}
	}
}

proc newhandle { chan info hproc cproc id args } {
		set n [gets $chan line]
		if {$n >= 0} {
			debug 1 "handle $chan $info $hproc $cproc read $n: $line"
			# disable event handler here, $hproc may start its own (and return 0)
			fileevent $chan readable {}
			if {[string compare $line -eof] == 0} {
				debug 1 "handle $chan $info $hproc $cproc: eof from client $info."
				debug 1 "handle: fileevent $chan readable {}"
				fileevent $chan readable {}
				# close $chan
				$cproc $id $chan $args
			} else {
				set res [$hproc $id $line $chan $args]
				debug 1 "handled $chan $info: res $res : line: $line"
				if {$res > 0} {
					fileevent $chan readable \
						[concat handle $chan $info $hproc $cproc $id $args]
				} elseif {$res < 0} {
					debug 1 "handled $chan $info: res $res : closing $chan"
					close $chan
					$cproc $id $chan $args
				}
			}
		} else {
			if {[eof $chan]} {
				debug 0 "handle $chan $info $hproc $cproc: Connection closed by client $info."
				debug 1 "handle: fileevent $chan readable {}"
				fileevent $chan readable {}
				# close $chan
				$cproc $id $chan $args
			} else {
				debug 0 "handle $chan $info $hproc $cproc read $n"
			}
		}

}

proc chancleanup { id chan } {
	upvar #0 G_[set id](ifid) ifid
	upvar #0 G_[set id](ieof) ieof
	upvar #0 S_[set id](nr) nr

	debug 1  "chancleanup $id $chan {"
	if {[string compare $ifid $chan] == 0} {
		set ieof 1
		update_reuse $nr
		update_keymenu $nr
	}
	debug 1  "chancleanup $id $chan }"
}

proc quit {nr} {
	global B

	msg stderr "exiting"
	if {([string compare $B(interface,name) ""] != 0) &&
	    $B(interface,cleanup) &&
	    [file readable $B(interface,name)]} {
		catch {file delete -force $B(interface,name)}
	}
	if {([string compare $B(pidsfile,name) ""] != 0) &&
	    $B(pidsfile,cleanup)} {
		catch {file delete -force $B(pidsfile,name)}
	}
	exit
}
proc mydestroy {nr} {
	upvar #0 W_[set nr](busy) busy
	upvar #0 W_[set nr](destroying) destroying
	upvar #0 W_[set nr](t) t

	debug 1  "mydestroy $nr {"

	set destroying 1
	bind $t <Configure> {}
	if {$busy} {
		debug 1  "mydestroy $nr waiting for $busy"
		trace variable busy wu "destroy_when_not_busy $nr"
	} else {
		destroy_window_and_contents $nr
	}
	debug 1  "mydestroy $nr }"
}
proc destroy_when_not_busy {nr args} {
	debug 1  "destroy_when_not_busy $nr $args {"
	destroy_window_and_contents $nr
	debug 1  "destroy_when_not_busy $nr $args }"
}
proc destroy_window_and_contents {nr} {
	global windows
	global surviveWindowDestroy
	upvar #0 W_[set nr](t) t

	debug 1  "destroy_window_and_contents $nr {"
	destroy $t
	set windows [ldelete $windows $t]
	destroy_contents $nr
	cleanupnr $nr
	if {[llength $windows] <= 0 && !$surviveWindowDestroy} {
		quit $nr
	}
	debug 1  "destroy_window_and_contents $nr }"
}
proc destroy_contents {nr} {
	debug 1  "destroy_contents $nr {"
	cleanupid $nr
	debug 1  "destroy_contents $nr }"
}
proc cleanup_fids {nr} {
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](cfid) cfid
	upvar #0 G_[set id](ifid) ifid

	debug 1 "cleanup_fids $nr : id $id"
	if {[info exists cfid] && ([string compare $cfid ""] != 0)} {
		fileevent $cfid readable {}
		close $cfid
		set cfid ""
	}
	if {[info exists ifid] && ([string compare $ifid ""] != 0)} {
		fileevent $ifid readable {}
		close $ifid
		set ifid ""
	}
	debug 1 "cleanup_fids $nr : id $id"
}
proc cleanupid {nr} {
	upvar #0 W_[set nr](id) id
	upvar #0 S_[set id](nr) w_nr

	debug 1 "cleanup $nr {"
	set w_nr [ldelete $w_nr $nr]
	if {[llength $w_nr] == 0} {
		upvar #0 G_[set id](g) g
		global reuseQ

		debug 1 "cleanup $nr : id $id"
		cleanup_fids $nr
		if {[info exists g] && ([string compare $g ""] != 0)} {
			debug 1 "cleanup $nr : id $id g $g"
			# for now, we do not delete the graph:
			# with tcldot 1.8.5 we have a problem when we have
			# two windows with two (different graphs), that both
			# are reusable, (after some steps) and we clone one of
			# the windows which reuses the other window:
			# deletes the graph, then we get a segv when
			# we render the cloned graph in the reused window:
			# DEBUG: anifsmsrv: clone (0) {
			# DEBUG: anifsmsrv: getcanvas (1) (1)(sock6) {
			# DEBUG: anifsmsrv: find_reusable 1 : (1 0) {
			# DEBUG: anifsmsrv: find_reusable 1: found 1 (containing 2) key= w_key=)
			# DEBUG: anifsmsrv: find_reusable 1: (0) } (1)
			# DEBUG: anifsmsrv: reusecanvas (1) (1) (1) {
			# DEBUG: anifsmsrv: reusecanvas (1) (1) (1): 1
			# DEBUG: anifsmsrv: destroy_contents 1 {
			# DEBUG: anifsmsrv: cleanup 1 {
			# DEBUG: anifsmsrv: cleanup 1 : id 2 cfid sock12 ifid sock11
			# DEBUG: anifsmsrv: cleanup 1 : id 2 cfid  ifid  g graph1
			# DEBUG: anifsmsrv: cleanup 1 }
			# DEBUG: anifsmsrv: destroy_contents 1 }
			# DEBUG: anifsmsrv: zoomdepth=0.356725146199
			# DEBUG: anifsmsrv: zoom .f1.c 2.80327868852 0.356725146199 : 
			# DEBUG: anifsmsrv: zoom .f1.c 2.80327868852 -> 1.0
			# DEBUG: anifsmsrv: zoomdepth=1.0
			# DEBUG: anifsmsrv: update_reuse 1 { doReuse 1 ieof 1 reuseQ 0
			# DEBUG: anifsmsrv: update_reuse 0 : doReuse 1 ieof 1 reuseQ 0
			# DEBUG: anifsmsrv: update_reuse 1 : doReuse 1 ieof 1 reuseQ 0
			# DEBUG: anifsmsrv: update_reuse 1 } reuseQ 0 1
			# DEBUG: anifsmsrv: update_keymenu 1 { key  ieof 1
			# DEBUG: anifsmsrv: update_keymenu 0 : key  ieof 1
			# DEBUG: anifsmsrv: update_keymenu 1 : key  ieof 1
			# DEBUG: anifsmsrv: update_keymenu 1 }
			# DEBUG: anifsmsrv: reusecanvas (1) (1) (1) }
			# DEBUG: anifsmsrv: getcanvas (1) (1)(sock6)(1) }
			# DEBUG: anifsmsrv: hidecanvas (1) {
			# DEBUG: anifsmsrv: hidecanvas (1) }
			# DEBUG: anifsmsrv: zoomupdate 1 .f1.c {
			# DEBUG: anifsmsrv: 	zoomupdate 1 .f1.c  find all {
			# DEBUG: anifsmsrv: 	zoomupdate 1 .f1.c  find all } 
			# DEBUG: anifsmsrv: 	zoomupdate 1 .f1.c after foreach: bbox all
			# DEBUG: anifsmsrv: 	zoomupdate 1 .f1.c after bbox all: llength
			# DEBUG: anifsmsrv: 	zoomupdate 1 .f1.c after llength: configure list
			# DEBUG: anifsmsrv: zoomupdate 1 .f1.c }
			# DEBUG: anifsmsrv: rendergraph (1) (graph0) {
			# DEBUG: anifsmsrv: 	rendergraph (1) (graph0) render 1 {
			# t@1 (l@1) signal SEGV (no mapping at the fault address) in agxget at 0xfe37624c
			# 0xfe37624c: agxget+0x0018:	ld      [%g3 + %g2], %o0
			# (/opt/SUNWspro/bin/../WS6/bin/sparcv9/dbx) where
			# current thread: t@1
			# =>[1] agxget(0x1d5a60, 0x6d616e00, 0x3, 0xfe3d3dd4, 0xfe31f3a0, 0x1e7390), at 0xfe37624c
			#   [2] late_string(0xfe390e60, 0x1eb528, 0xfe390e60, 0x1b51b8, 0xfe376e10, 0x8), at 0xfe33fd68
			#   [3] emit_edge(0x1d5a60, 0xfffff000, 0x0, 0x1d4020, 0xffbe9f94, 0x1), at 0xfe32472c
			#   [4] emit_graph(0x1d4020, 0x1, 0xfe3d45f4, 0xfe3d45ec, 0xfe3d3dc4, 0x1), at 0xfe3239d0
			#   [5] graphcmd(0x0, 0x38660, 0x2, 0xffbe9f70, 0xe79a8, 0x38660), at 0xfe31cabc
			#   [6] TclInvokeStringCommand(0xffbe9f78, 0xffbe9f70, 0x2, 0x38660, 0x41168, 0xd09d8), at 0xff1a973c
			#   [7] TclEvalObjvInternal(0xd09d8, 0x2, 0x0, 0xffbea53c, 0xffbea53c, 0x1), at 0xff1aa9b0
			#   [8] TclExecuteByteCode(0xff253108, 0x1d742e, 0x11, 0x4111c, 0x382a0, 0x12), at 0xff1d77e8
			#   [9] TclCompEvalObj(0xff2521f4, 0x1e5c, 0xff24a868, 0x1d7350, 0xe7bb8, 0x38660), at 0xff1d6e8c
			#   [10] TclObjInterpProc(0xff239458, 0xc, 0x0, 0xffbea2bc, 0xff24a868, 0x158e40), at 0xff20f278
			#   [11] TclEvalObjvInternal(0xcd130, 0x3, 0x0, 0xffbeaae4, 0xffbeaae4, 0x1), at 0xff1aa9b0
			#   [12] TclExecuteByteCode(0xff253108, 0x23ad23, 0x1, 0x4111c, 0x382a0, 0x7), at 0xff1d77e8
			#   [13] TclCompEvalObj(0xff2521f4, 0x1d54, 0xff24a868, 0x23ac08, 0xe7690, 0x38660), at 0xff1d6e8c
			#   [14] TclObjInterpProc(0xff239458, 0x8, 0x0, 0xffbea864, 0xff24a868, 0x15b540), at 0xff20f278
			#   [15] TclEvalObjvInternal(0x14f108, 0x2, 0x0, 0x0, 0x0, 0x1), at 0xff1aa9b0
			#   [16] TclExecuteByteCode(0xff253108, 0x207934, 0x0, 0x4111c, 0x382a0, 0x4), at 0xff1d77e8
			#   [17] TclCompEvalObj(0x0, 0x1d53, 0xff24a868, 0x2078c8, 0x1a5100, 0x38660), at 0xff1d6e8c
			#   [18] Tcl_EvalObjEx(0x0, 0x0, 0x20000, 0xff24a868, 0x38660, 0x1a5100), at 0xff1ab97c
			#   [19] ButtonWidgetObjCmd(0x98828, 0x38660, 0x2, 0xff384b70, 0xff37534c, 0x98828), at 0xff2ef1c8
			#   [20] TclEvalObjvInternal(0x1ac538, 0x2, 0x0, 0x0, 0x0, 0x0), at 0xff1aa9b0
			#   [21] Tcl_EvalObjv(0x38660, 0x0, 0xff258a44, 0x0, 0x453d1, 0x2), at 0xff1aad04
			#   [22] Tcl_EvalObjEx(0x38660, 0x1a7230, 0x40000, 0xff24a868, 0x38660, 0x1a7230), at 0xff1aba78
			#   [23] Tcl_UplevelObjCmd(0xff24a868, 0xffbeb694, 0x3, 0x4111c, 0x38660, 0xc58e0), at 0xff20eba0
			#   [24] TclEvalObjvInternal(0x35330, 0x3, 0x0, 0xffbeb694, 0xffbeb694, 0x1), at 0xff1aa9b0
			#   [25] TclExecuteByteCode(0xff253108, 0x1d9082, 0x6, 0x4111c, 0x382a0, 0x2), at 0xff1d77e8
			#   [26] TclCompEvalObj(0xff2521f4, 0x1d49, 0xff24a868, 0x1d8f50, 0xe4cd8, 0x38660), at 0xff1d6e8c
			#   [27] TclObjInterpProc(0xff239458, 0x8, 0x0, 0xffbeb414, 0xff24a868, 0x118b08), at 0xff20f278
			#   [28] TclEvalObjvInternal(0xc4a60, 0x2, 0x0, 0x0, 0x0, 0x0), at 0xff1aa9b0
			#   [29] Tcl_EvalEx(0x19, 0xffbeb908, 0xffbebb99, 0x1, 0x0, 0xffbebb80), at 0xff1ab4c4
			#   [30] Tcl_Eval(0x38660, 0xffbebb80, 0x38660, 0x66302e, 0x81010100, 0x0), at 0xff1ab89c
			#   [31] Tcl_GlobalEval(0x38660, 0x0, 0x38660, 0xffbebc68, 0x0, 0x0), at 0xff1ad0d0
			#   [32] Tk_BindEvent(0xff384b70, 0x1f4188, 0x539a8, 0xffbebb9a, 0x0, 0xffbebc48), at 0xff2a43f8
			#   [33] TkBindEventProc(0x4, 0xffbebcd8, 0xff37534c, 0x0, 0x1f4188, 0x1adf60), at 0xff2aad74
			#   [34] Tk_HandleEvent(0x6800029, 0x0, 0xff384b70, 0xff37534c, 0x38660, 0x1f4188), at 0xff2b3af0
			#   [35] WindowEventProc(0x1f4188, 0xfffffffd, 0x1793b8, 0x25a3b0, 0x2fe48, 0x38164), at 0xff2b4168
			#   [36] Tcl_ServiceEvent(0x2fe48, 0x2fe48, 0x2fe54, 0xff2b40d8, 0x1f4180, 0xfffffffd), at 0xff202a60
			#   [37] Tcl_DoOneEvent(0x0, 0xc4060, 0x2fe48, 0x0, 0x2fe60, 0x1), at 0xff202dcc
			#   [38] Tcl_VwaitObjCmd(0x1, 0x38660, 0xc4060, 0xffbec188, 0xff24a868, 0x38660), at 0xff1d6104
			#   [39] TclEvalObjvInternal(0x35f30, 0x2, 0x0, 0x0, 0x0, 0x0), at 0xff1aa9b0
			#   [40] Tcl_EvalEx(0x90a5, 0xffbec188, 0x164ccd, 0x1, 0x0, 0x164cbc), at 0xff1ab4c4
			#   [41] Tcl_FSEvalFile(0x90160, 0x402e0, 0xff24a868, 0x0, 0x1, 0x3eb28), at 0xff1f62bc
			#   [42] Tcl_EvalFile(0x38660, 0x3eb28, 0x0, 0xff386780, 0x1, 0xff358c04), at 0xff1f4b90
			#   [43] Tk_MainEx(0xff384b78, 0x47cc0, 0xff384b70, 0x38660, 0xff37534c, 0x11318), at 0xff2c2564
			#   [44] main(0x2, 0xffbec62c, 0x2, 0x2e26c, 0x0, 0x0), at 0x112f8
			# (/opt/SUNWspro/bin/../WS6/bin/sparcv9/dbx) 
			#  } } }

			# $g delete
			set g ""
		}
		catch {unset G_[set id]}
		catch {unset G_[set id]_name2edge}
		catch {unset G_[set id]_name2graph}
		catch {unset G_[set id]_col}
	}
	debug 1 "cleanup $nr }"
}
proc cleanupnr {nr} {
	global reuseQ
	upvar #0 W_[set nr](id) id
	upvar #0 S_[set id](nr) w_nrs

	debug 1 "cleanupnr $nr: reuseQ ($reuseQ) {"
	set w_nrs [ldelete $w_nrs $nr]
	set reuseQ [ldelete $reuseQ $nr]
	catch {unset W_[set nr]}
	catch {unset W_[set nr]_packinfo packinfo}
	catch {unset W_[set nr]_zoomdata zoomdata}
	if {[llength $w_nrs] <= 0} {
		catch {unset S_[set id]}
	}
	
	debug 1 "cleanup $nr -> reuseQ ($reuseQ) }"
}

proc zoom {nr fact} {
	upvar #0 W_[set nr]_zoomdata zoomdata
	upvar #0 W_[set nr](c) c

	debug 1  "zoom $c $fact $zoomdata(zdepth) : "
	set x [$c canvasx [expr {[winfo pointerx $c] - [winfo rootx $c]}]]
	set y [$c canvasy [expr {[winfo pointery $c] - [winfo rooty $c]}]]
	if {![catch {expr {$zoomdata(zdepth) * $fact}} res]} {
		$c scale all $x $y $fact $fact
		set zoomdata(zdepth) $res
		after cancel $zoomdata(idle)
		set zoomdata(idle) [after idle "zoomupdate $nr"]
		debug 1  "zoom $c $fact -> $zoomdata(zdepth)"
	}
}

proc zoomdepth {nr} {
	upvar #0 W_[set nr]_zoomdata zoomdata

	return $zoomdata(zdepth)
}

proc zoomupdate {nr} {
	upvar #0 W_[set nr]_zoomdata zoomdata
	upvar #0 W_[set nr](c) c
	upvar #0 W_[set nr](destroying) destroying

	debug 1  "zoomupdate $nr $c {"

	if {$destroying} {
		debug 1  "	zoomupdate $nr $c  find all  destroying"
		return
	}
	# adjust fonts
	debug 1  "	zoomupdate $nr $c  find all {"
	if {[catch {set all [$c find all]} msg]} {
		debug 1  "	zoomupdate $nr $c  find all caught"
		return
	}
	debug 1  "	zoomupdate $nr $c  find all } $all"
	foreach {i} $all {
#		debug 1  "	zoomupdate $nr $c  foreach $i { type [$c type $i]"
		if {[string compare [$c type $i] text] != 0} {continue}
		set fontsize 0
		# get original fontsize and text from tags
		#   if they were previously recorded
		foreach {tag} [$c gettags $i] {
			scan $tag {_f%d} fontsize
			# do not use the next scan line because it
			# looses trailing '0' characters in names
			# like eg 'S40'; use the regexp instead
			# scan $tag {_t%[^\0]} text
			regexp {^_t(.*)$} $tag match text
		}
		debug 1  "	zoomupdate $nr $c   $i type [$c type $i] fontsize=$fontsize"
		# if not, then record current fontsize and text
		#   and use them
		set font [$c itemcget $i -font]
		if {!$fontsize} {
			set text [$c itemcget $i -text]
			set fontsize [lindex $font 1]
			set platform [lindex $::tcl_platform(os) 0]
			if {[string compare -nocase $platform Windows] != 0} {
				# cheat by increasing font size, and changing font to 'system'
				# note: increment of 5 might be one too much, esthetically
				set fontsize [expr {int(ceil($fontsize + 5)) }]
				set font [lreplace $font 0 0 system]
			}
			$c addtag _f$fontsize withtag $i
			$c addtag _t$text withtag $i
		}
		debug 1  "	zoomupdate $nr $c   $i type [$c type $i] fontsize=$fontsize zdepth=$zoomdata(zdepth)"
		# scale font
		if {![catch {expr {int(ceil($fontsize * $zoomdata(zdepth)))}} newsize]} {
			debug 1  "	zoomupdate $nr $c   $i type [$c type $i] fontsize=$fontsize text=$text zdepth=$zoomdata(zdepth) newsize=$newsize font=$font"
			if {abs($newsize) >= 4} {
				$c itemconfigure $i \
					-font [lreplace $font 1 1 $newsize] \
					-text $text
			} {
				# suppress text if too small
				$c itemconfigure $i -text {}
			}
		} else {
			debug 1  "	caught-zoomupdate $nr $c   $i type [$c type $i] fontsize=$fontsize text=$text zdepth=$zoomdata(zdepth) newsize=$newsize"
		}
#		debug 1  "	zoomupdate $nr $c  foreach $i }"
	}
	debug 1  "	zoomupdate $nr $c after foreach: bbox all"
	set bbox [$c bbox all]
	debug 1  "	zoomupdate $nr $c after bbox all: llength"
	if {[llength $bbox]} {
		debug 1  "	zoomupdate $nr $c after llength: configure $bbox"
		$c configure -scrollregion $bbox
	} {
		debug 1  "	zoomupdate $nr $c after llength: configure list"
		$c configure -scrollregion [list -4 -4 \
			[expr {[winfo width $c]-4}] \
			[expr {[winfo height $c]-4}]]
	}
	debug 1  "zoomupdate $nr $c }"
}

# =================================

proc main { argv addr } {
	global G
	global printCommand
	global advanced
	global surviveWindowDestroy

	set printCommand {lpr}
	set advanced 0
	set private 0
	set surviveWindowDestroy 0
	
#puts stderr "in main"
#flush stderr
	set pid [pid]
	if {[string compare [lindex $argv 0] -private] == 0} {
		set private 1
		set argv [lrange $argv 1 end]
	}
	if {[string compare [lindex $argv 0] -surviveWindowDestroy] == 0} {
		set surviveWindowDestroy 1
		fileevent stdin readable [list suicide_on_eof stdin]
		set argv [lrange $argv 1 end]
	}
	if {[llength $argv] > 1} {
		set s [join $argv " "]
		warn stderr "$G(sprog): ignoring arguments: $s"
		warn stderr "usage: $G(sprog)"
	} elseif {[llength $argv] == 1} {
		set pid [lindex $argv 0]
	}

	set host [info hostname]
	set display [find_display $host]
	set user [find_user]
	set tmp [find_tmp]
	set fname [mk_filename $tmp $G(fnamepart) $user $display]

#puts stderr "about to setup B"
#flush stderr
	if {![setup_B $addr 0 $fname $host $pid $private]} {
		exit 1
	}

	add_key "" ;# we depend on this key value in update_keymenu!!!
	
	image create bitmap up -data {
#define up_width 5
#define up_height 3
static unsigned char up_bits[] = {
   0x04, 0x0e, 0x1f};
	}
	image create bitmap down -data {
#define down_width 5
#define down_height 3
static unsigned char down_bits[] = {
   0x1f, 0x0e, 0x04};
	}

#	if {[continue_reading_after_process_args $argv stdin stderr] < 0} {
#		# hmmm... is this what we want,
#		# or should server remain running?
#		quit ignored
#	}

}

proc suicide_on_eof {fid} {
	if {[eof $fid]} {
		quit ignored
	} else {
		catch {read $fid}
		fileevent stdin readable [list suicide_on_eof $fid]
	}
}

proc setup_B {defAddr defPort fname host pid private} {
	global B

	set B(interface,name) $fname
	set B(pidsfile,name) "$fname.pid"
	set B(statusfile,name) "$fname.stat"
	set B(interface,cleanup) 0
	set B(pidsfile,cleanup) 0

    	warn stderr "starting server: private=$private"
        if {! $private} {

	if {[string compare [try_connect [get_address $B(interface,name) 0] 0] 0] != 0} {
		warn stderr "not replacing running server (aborting)"
		exit 0
	} else {
		# warn stderr "could not connect to running server"
	}
	if {[catch {open $B(statusfile,name) a} fid]} {
		warn stderr "cannot open (append) $B(statusfile,name): $fid"
		exit 1
	} else {
		puts $fid "starting $host $pid"
		close $fid
	}
	if {[catch {open $B(statusfile,name) r} fid]} {
		warn stderr "cannot open (read) $B(statusfile,name)"
		exit 1
	} elseif {[gets $fid line] < 0} {
		warn stderr "cannot read $B(statusfile,name)"
		exit 1
	} else {
		close $fid
		set words [split $line]
		set stat [lindex $words 0]
		set h [lindex $words 1]
		set p [lindex $words 2]
		if {[string compare $stat starting] == 0 &&
		    [string compare $host $h] == 0 &&
		    [string compare $pid $p] != 0} {
			warn stderr "pid $pid != p $p"
			if {[findprocess $pid $p]} {
				set i 0
				set maxtries 10
				while {$i < $maxtries} {
					if {$i == ($maxtries - 1)} {
						set sock [try_connect [get_address $B(interface,name) 1] 1]
					} else {
						set sock [try_connect [get_address $B(interface,name) 1] 0]
					}
					if {[string compare $sock 0] != 0} {
						warn stderr "attempt $i: not replacing running server (aborting)"
						exit 0
					} elseif {$i == ($maxtries - 1)} {
						warn stderr "attempt $i: could not connect to running server"
					}
					incr i
					after 2
				}
			} else {
				msg stderr "did not find running server pid: $p"
			}
		}
	}
	if {[string compare [try_connect [get_address $B(interface,name) 0] 0] 0] != 0} {
		warn stderr "not replacing running server (aborting)"
		exit 0
	} else {
		# warn stderr "could not connect to running server"
	}
	if [catch {open $B(interface,name) w} fid] {
		warn stderr "cannot create $B(interface,name): $fid"
		return 0
	}
	set B(interface,cleanup) 1
     }

	# msg stderr "trying to listen on tcp!$defAddr!$defPort"
	if {$defAddr == {}} {
		set c [socket -server acceptIt $defPort]
	} else {
		set c [socket -server acceptIt -myaddr $defAddr $defPort]
	}
	set cfg [fconfigure $c -sockname]
	set ipNr [lindex $cfg 0]
	set hostName [lindex $cfg 1]
	set portNr [lindex $cfg 2]
	set B(listener) "tcp!$ipNr!$portNr"
	set B(host) [info hostname]
	msg stderr "listening on tcp!$ipNr!$portNr"
	
	catch {puts stdout "$ipNr $hostName $portNr"} ;# should commit suicide?
	
    if {! $private} {
	puts $fid "$ipNr $hostName $portNr"
	close $fid

	if {![catch {open "$B(pidsfile,name)" r} pfid]} {
		while {[gets $pfid line] >= 0} {
			set p [string trim $line]
			kill $p
		}
		catch {close $pfid}
	}
	catch {file delete -force $B(pidsfile,name)}
	if {[catch {open "$B(pidsfile,name)" w} pfid]} {
		warn stderr "cannot open $B(pidsfile,name) for writing: $pfid"
	} else {
		set B(pidsfile,cleanup) 1
		catch {puts $pfid $pid}
		catch {close $pfid}
	}
	catch {file delete -force $B(statusfile,name)}
    }
	return 1
}

proc acceptIt {socket remoteHost remotePort} {
	global B
	
	set nSock $socket
 	set B(busy,$socket) 0
	fconfigure $socket -blocking 0
debug 1 "acceptIt: fileevent $socket readable [list handle $socket <B> Binterp Bcleanup ign_id]"
	msg stderr "Connection accepted from client tcp!$remoteHost!$remotePort: $socket"
	fileevent $socket readable [list handle $socket <B> Binterp Bcleanup ign_id]
}  


proc Binterp { ign_id line chan argl } {
	debug 1 "Binterp $ign_id $line $chan $argl"
	return [continue_reading_after_process_B_line $line $chan]
}

proc Bcleanup { ign_id chan argl } {
	global B

	debug 0 "Bcleanup $ign_id $chan $argl"
	if {![catch {fconfigure $chan -peername} cfg]} {
		set remoteHost [lindex $cfg 0]
		set remotePort [lindex $cfg 2]
		msg stderr "Connection closed by client tcp!$remoteHost!$remotePort"
	}
	close $chan
	unset B(busy,$chan)
}

proc continue_reading_after_process_B_line { line fid } {
	global B
debug 1 "continue_reading_after_process_B_line ($line) ($fid)"
	set name [string trim $line]
	set result 1
	if {([string compare $name -exit] == 0) ||
	    (([string compare [lindex $name 0] -args] == 0) &&
	     ([string compare [lindex $name 1] -exit] == 0))} {
		# we let quit clean up for us # chancleanup
		quit igored
	} elseif {[string compare [lindex $name 0] -eof] == 0} {
		debug 0 "B eof"
		trace variable B(busy,$fid) w "close_B_when_ready $fid"
		return 0
	} elseif {[string compare [lindex $name 0] -debug] == 0} {
		debug 1 "B debug: [lrange $name 1 end]"
	} elseif {[string compare [lindex $name 0] -args] == 0} {
		debug 1 "B debug: [lrange $name 1 end]"
		set result [continue_reading_after_process_args [lrange $name 1 end] $fid $fid]
	} else {
		debug 1 "B debug: unknown line: $line"
	}
	return $result
}

proc close_B_when_ready {fid varName index op} {
	global B
	debug 0 "close_B_when_ready ($fid)($varName)($index)($op)"
	switch $op {
		w {
			if {[string compare "busy,$fid" $index] == 0} {
				if {$B(busy,$fid) == 0} {
					debug 0 "close_B_when_ready: closing fid (B(busy,$fid) == 0)"
					if {![catch {fconfigure $fid -peername} cfg]} {
						set remoteHost [lindex $cfg 0]
						set remotePort [lindex $cfg 2]
						msg stderr "Closing connection to client tcp!$remoteHost!$remotePort"
					}
					close $fid
					unset B(busy,$fid)
				} else {
					debug 0 "close_B_when_ready: not yet closing fid (B(busy,$fid) == $B(busy,$fid))"
				}
			} else {
					debug 0 "close_B_when_ready: unrelated update (index=$index)"
				
			}
		}
	}
}

proc continue_reading_after_process_args {argv ifid ofid {name stdin} } {
	global id
	global G
	
	set reuse 0
	set files {}
	set result 1
	set mcast ""
	set title "AniDOT"
	set key ""
	
	set args $argv
	
#	set joinedargs [join $argv ")(" ]
#	warn $ofid "command lin args: ($joinedargs)"

	while {[llength $args] > 0} {
		set a [lindex $args 0]
		set args [lrange $args 1 end]
		switch -- $a {
		    -r {
				set reuse 1
		  } -m {
				if {[llength $args] <= 0} {
		  			warn $ofid "missing argument for flag: $a"
				} else {
					set mcast [lindex $args 0]
					set args [lrange $args 1 end]
				}
		  } -t {
				if {[llength $args] <= 0} {
		  			warn $ofid "missing argument for flag: $a"
				} else {
					set title "AniDOT"
					append title " - "
					append title [lindex $args 0]
					set args [lrange $args 1 end]
				}
		  } -k {
				if {[llength $args] <= 0} {
		  			warn $ofid "missing argument for flag: $a"
				} else {
					set key [lindex $args 0]
					set args [lrange $args 1 end]
				}
		  } -* {
		  		warn $ofid "unknown flag: $a"
		  } default {
		  		lappend files $a
		  }
		}
	}
	if {$ifid == {}} {
		set ifid ""
	}
	if {[llength $files] == 0} {
		lappend files ""
	}

	if {($result == -1)} {
#		warn $ofid "result=$result ifid=$ifid llength-files=[llength $files]"
		warn $ofid "usage: $G(cprog) \[-r\] \[-m mcastid\] \[-t title\] \[-k key\] \[file.dot\]"
		set result -1
	} else {
		add_key $key
		set cfid [connect_mcast $mcast $ofid]
		set lastfile [lindex $files end]
		set files [lreplace $files end end]
		foreach f $files {
			incr id
			start $id $f $reuse "" $ofid $cfid $mcast $name $title $key
		}
		incr id
		start $id $lastfile $reuse $ifid $ofid $cfid $mcast $name $title $key
		set result 0
	}
	return $result
}

proc clone {nr} {
	upvar #0 W_[set nr](id) id
	upvar #0 W_[set nr](doReuse) doReuse
	upvar #0 W_[set nr](key) key
	upvar #0 G_[set id](g) g
	upvar #0 G_[set id](ofid) ofid
	
	debug 1 "clone ($nr) {"
	set w_nr [getcanvas $id $doReuse $ofid $key]

	if {[string compare $g ""] != 0} {
		do_render $id
	}
	debug 1 "clone ($nr) }"
}

proc updatefilename {id fname} {
	upvar #0 G_[set id](filedir) filedir
	upvar #0 G_[set id](filetailpfx) filetailpfx
	upvar #0 G_[set id](modified) modified

	set filedir [file dirname $fname]
	set filetail [file tail $fname]
	set filetailpfx [file rootname $filetail]
}

proc getfiletail {id {ext ""}} {
	upvar #0 G_[set id](filetailpfx) filetailpfx

	set file $filetailpfx
	if {[string compare $filetailpfx ""] != 0} {
		append file $ext
	}
	return $file
}

proc getfiledir {id} {
	upvar #0 G_[set id](filedir) filedir

	return $filedir
}

proc getfiletypes {ext} {
	set filetypes {
		{{Aldebaran Files}       {.aut}   }
		{{Dot Files}       {.dot}   }
		{{Postscript Files}       {.ps}   }
		{{Text Files}      {.txt}  }
		{{All Files}        *             }
	}
	set ix [lsearch -glob $filetypes [subst *[set ext]*]]
	if {$ix >= 0} {
		lappend all [lindex $filetypes $ix]
		set rest [lreplace $filetypes $ix $ix]
		set all [concat $all $rest]
	} else {
		set all $filetypes
	}
	return $all
}

proc askfilename {id typename ext {mode save}} {
	upvar #0 S_[set id](nr) w_nrs
	upvar #0 W_[lindex $w_nrs 0](c) c

	set file [getfiletail $id $ext]
	set dir [getfiledir $id]
	set types [getfiletypes $ext]

	switch -- $mode {
	   save {
		set fname [tk_getSaveFile \
				-parent $c \
				-initialdir $dir \
				-initialfile $file \
				-filetypes $types  \
				-defaultextension $ext \
				-title "Save $typename As" ]
	  } open {
		set fname [tk_getOpenFile \
				-parent $c \
				-initialdir $dir \
				-initialfile $file \
				-filetypes $types  \
				-defaultextension $ext \
				-title "Open $typename" ]
	  }
	}
	return $fname
}

proc getwritefd {id typename ext} {
	upvar #0 S_[set id](nr) w_nrs
	upvar #0 W_[lindex $w_nrs 0](c) c

	set fname [askfilename $id $typename $ext]

	set fd ""
	if {[string compare $fname ""] != 0} {
		if {[catch {open $fname w} fd]} {
			tk_messageBox -parent $c \
				-message "can not open $fname for writing: $fd"
			set fd ""
		} else {
			updatefilename $id $fname
		}
	}

	return $fd
}

proc start {id dotfname rse aifid aofid acfid amcast name atitle key} {
	global idinfo
	global reuseQ
	upvar #0 G_[set id](scur) scur
	upvar #0 G_[set id](oldscur) oldscur
	upvar #0 G_[set id](Q) Q
	upvar #0 G_[set id](oldQ) oldQ
	upvar #0 G_[set id](I) I
	upvar #0 G_[set id](oldI) oldI
	upvar #0 G_[set id](ifid) ifid
	upvar #0 G_[set id](ieof) ieof
	upvar #0 G_[set id](iname) iname
	upvar #0 G_[set id](ofid) ofid
	upvar #0 G_[set id](cfid) cfid
	upvar #0 G_[set id](mcast) mcast
	upvar #0 G_[set id](reuse) reuse
	upvar #0 G_[set id](g) g
	upvar #0 G_[set id](title) title
	upvar #0 G_[set id](filedir) filedir
	upvar #0 G_[set id](filetailpfx) filetailpfx
	upvar #0 G_[set id](modified) modified

	debug 1 "start $id $dotfname $rse $aifid $aofid $acfid $amcast $name $atitle $key"

	set reuse $rse
	set ifid $aifid
	set ieof 0
	set iname $name
	set ofid $aofid
	set cfid $acfid
	set mcast $amcast
	set title $atitle

	set idinfo($id,started) 1
	set idinfo($id,closed) 0
	
	if {[info exists B(busy,$ofid)]} {
		incr B(busy,$ofid)
	}
	
	# if {[string compare $ifid stdin ] != 0} {
	if {[string compare $ifid ""] != 0} {
		fconfigure $ifid -buffering line
	}
		# -blocking 0
	# }

	set Q {}
	set oldQ {}
	set I {}
	set oldI {}
	set scur ""
	set oldscur ""

	set nr [getcanvas $id $reuse $ofid $key]
	if {[string compare $dotfname "-"] == 0 || [string compare $dotfname ""] == 0} {
		set g [newgraph $id $ofid]
		set filetailpfx ""
	} else {
		set g [opengraph $id $dotfname $ofid]
		set filetail [file tail $dotfname]
		set filetailpfx [file rootname $filetail]
	}
	set filedir [pwd]
	set modified 0
	if {[string compare $g ""] != 0} {
		do_render $id

		if {[string compare $ifid ""] != 0} {
debug 0 "start: fileevent $ifid readable [list handle $ifid $name cmdinterp cmdcleanup $id $g $ofid]"

			if {[catch {fileevent $ifid readable [list handle $ifid $name cmdinterp cmdcleanup $id $g $ofid]} msg]} {
				msg stderr "oops.. setting up fileevent $ifid $name cmdinterp cmdcleanup $id $g $ofid: $msg"
				cmdcleanup $id "" {}
				
			}
			debug 1 "start: started cmdinterp $id $ifid $name"
		} else {
			cmdcleanup $id "" {}
		}
		if {[string compare $cfid ""] != 0} {
			fileevent $cfid readable [list handle $cfid "<mcast>" mcastinterp mcastcleanup $id $g ]
		}
	} else {
		set ieof 1
		update_reuse $nr
		update_keymenu $nr
		# NOTE: no need to cleanupid here
		# (if we do, things break)
		cleanup_fids $nr
		lappend reuseQ $nr
	}
}

# start $id [lindex $argv 0]
main $argv $addr

vwait quit
exit
