# taken from:
# https://frostie.cs.ucl.ac.uk/repos/mmedia/tk-8.0/trunk/library/palette.tcl.

# Arguments:

# color - Name of starting color.
# perecent - Integer telling how much to brighten or darken as a
#  percent: 50 means darken by 50%, 110 means brighten
#  by 10%. 



proc tkBrighten {color amount} {
    set l [winfo rgb . $color]
    set red [expr {[lindex $l 0]/256}]
    set green [expr {[lindex $l 1]/256}]
    set blue [expr {[lindex $l 2]/256}]
    
    set red [expr {($red+$amount)}]
    if {$red > 255} {
      set red 255
    }
    set green [expr {($green+$amount)}]
    if {$green > 255} {
      set green 255
    }
    set blue [expr {($blue+$amount)}]
    if {$blue > 255} {
      set blue 255
    }
    format #%02x%02x%02x $red $green $blue
} 


proc tkDarken {color percent} {
    set l [winfo rgb . $color]
    set red [expr {[lindex $l 0]/256}]
    set green [expr {[lindex $l 1]/256}]
    set blue [expr {[lindex $l 2]/256}]
    set red [expr {($red*$percent)/100}]
    if {$red > 255} {
      set red 255
    }
    set green [expr {($green*$percent)/100}]
    if {$green > 255} {
      set green 255
    }
    set blue [expr {($blue*$percent)/100}]
    if {$blue > 255} {
      set blue 255
    }
    format #%02x%02x%02x $red $green $blue
} 


# rgb2hsv --
 #
 #       Convert a color value from the RGB model to HSV model.
 #
 # Arguments:
 #       r g b  the red, green, and blue components of the color
 #               value.  The procedure expects, but does not
 #               ascertain, them to be in the range 0 to 1.
 #
 # Results:
 #       The result is a list of three real number values.  The
 #       first value is the Hue component, which is in the range
 #       0.0 to 360.0, or -1 if the Saturation component is 0.
 #       The following to values are Saturation and Value,
 #       respectively.  They are in the range 0.0 to 1.0.
 #
 # Credits:
 #       This routine is based on the Pascal source code for an
 #       RGB/HSV converter in the book "Computer Graphics", by
 #       Baker, Hearn, 1986, ISBN 0-13-165598-1, page 304.
 #

 proc rgb2hsv {r g b} {
     set h [set s [set v 0.0]]]
     set sorted [lsort -real [list $r $g $b]]
     set v [expr {double([lindex $sorted end])}]
     set m [lindex $sorted 0]

     set dist [expr {double($v-$m)}]
     if {$v} {
         set s [expr {$dist/$v}]
     }
     if {$s} {
         set r' [expr {($v-$r)/$dist}] ;# distance of color from red
         set g' [expr {($v-$g)/$dist}] ;# distance of color from green
         set b' [expr {($v-$b)/$dist}] ;# distance of color from blue
         if {$v==$r} {
             if {$m==$g} {
                 set h [expr {5+${b'}}]
             } else {
                 set h [expr {1-${g'}}]
             }
         } elseif {$v==$g} {
             if {$m==$b} {
                 set h [expr {1+${r'}}]
             } else {
                 set h [expr {3-${b'}}]
             }
         } else {
             if {$m==$r} {
                 set h [expr {3+${g'}}]
             } else {
                 set h [expr {5-${r'}}]
             }
         }
         set h [expr {$h*60}]          ;# convert to degrees
     } else {
         # hue is undefined if s == 0
         set h -1
     }
     return [list $h $s $v]
 }

 # hsv2rgb --
 #
 #       Convert a color value from the HSV model to RGB model.
 #
 # Arguments:
 #       h s v  the hue, saturation, and value components of
 #               the color value.  The procedure expects, but
 #               does not ascertain, h to be in the range 0.0 to
 #               360.0 and s, v to be in the range 0.0 to 1.0.
 #
 # Results:
 #       The result is a list of three real number values,
 #       corresponding to the red, green, and blue components
 #       of a color value.  They are in the range 0.0 to 1.0.
 #
 # Credits:
 #       This routine is based on the Pascal source code for an
 #       HSV/RGB converter in the book "Computer Graphics", by
 #       Baker, Hearn, 1986, ISBN 0-13-165598-1, page 304.
 #

 proc hsv2rgb {h s v} {
     set v [expr {double($v)}]
     set r [set g [set b 0.0]]
     if {$h == 360} { set h 0 }
     # if you feed the output of rgb2hsv back into this
     # converter, h could have the value -1 for
     # grayscale colors.  Set it to any value in the
     # valid range.
     if {$h == -1} { set h 0 }
     set h [expr {$h/60}]
     set i [expr {int(floor($h))}]
     set f [expr {$h - $i}]
     set p1 [expr {$v*(1-$s)}]
     set p2 [expr {$v*(1-($s*$f))}]
     set p3 [expr {$v*(1-($s*(1-$f)))}]
     switch -- $i {
         0 { set r $v  ; set g $p3 ; set b $p1 }
         1 { set r $p2 ; set g $v  ; set b $p1 }
         2 { set r $p1 ; set g $v  ; set b $p3 }
         3 { set r $p1 ; set g $p2 ; set b $v  }
         4 { set r $p3 ; set g $p1 ; set b $v  }
         5 { set r $v  ; set g $p1 ; set b $p2 }
     }
     return [list $r $g $b]
 }
 
proc tkBright {color {max 0.5}} {
    set l [winfo rgb . $color]
    # puts stderr "rgb: $l"
    
    set red [expr {[lindex $l 0]/(65535.0)}]
    set green [expr {[lindex $l 1]/(65535.0)}]
    set blue [expr {[lindex $l 2]/(65535.0)}]
    # puts stderr "rgb: $red $green $blue"
       
    set hsv [rgb2hsv $red $green $blue]
    # puts stderr "hsv: $hsv"
    
    set h [lindex $hsv 0]
    set s [lindex $hsv 1]
    set v [lindex $hsv 2]
    
    if {$s > $max} {
    	set s $max
    }
    # puts stderr "hsv': $h $s $v"
    
    set rgb [hsv2rgb $h $s $v]
    # puts stderr "rgb': $rgb"
    
    set red [expr {int([lindex $rgb 0]*256)}]
    if {$red > 255} {
	set red 255
    }
    set green [expr {int([lindex $rgb 1]*256)}]
    if {$green > 255} {
	set green 255
    }
    set blue [expr {int([lindex $rgb 2]*256)}]
    if {$blue > 255} {
	set blue 255
    }
    format #%02x%02x%02x $red $green $blue
} 

proc tkDark {color} {
    set l [winfo rgb . $color]
    # puts stderr "rgb: $l"
    
    set red [expr {[lindex $l 0]/(65535.0)}]
    set green [expr {[lindex $l 1]/(65535.0)}]
    set blue [expr {[lindex $l 2]/(65535.0)}]
    # puts stderr "rgb: $red $green $blue"
       
    set hsv [rgb2hsv $red $green $blue]
    # puts stderr "hsv: $hsv"
    
    set h [lindex $hsv 0]
    set s [lindex $hsv 1]
    set v [lindex $hsv 2]
   
    if {$s < 0.25 && $v > 0.4} {
        set v 0.4
    }
    # puts stderr "hsv': $h $s $v"
    
    set rgb [hsv2rgb $h $s $v]
    # puts stderr "rgb': $rgb"
    
    set red [expr {int([lindex $rgb 0]*256)}]
    if {$red > 255} {
	set red 255
    }
    set green [expr {int([lindex $rgb 1]*256)}]
    if {$green > 255} {
	set green 255
    }
    set blue [expr {int([lindex $rgb 2]*256)}]
    if {$blue > 255} {
	set blue 255
    }
    format #%02x%02x%02x $red $green $blue
} 
