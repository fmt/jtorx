package provide app-anidotsrv 1.0

# The code here has been lifted from doted:
# "doted - dot graph editor - John Ellson (ellson@lucent.com)"
# and adapted for the anidot framework


# if b1 is pressed over the brackground then start a node,
# if b1 is pressed over a node then start an edge
proc mouse_b1_press {nr x y} {
	upvar #0 W_[set nr](c) c
	upvar #0 W_[set nr](id) id
	upvar #0 W_[set nr](viewedit) viewedit
	upvar #0 G_[set id](g) g
	upvar #0 G_[set id](graphtype) graphtype
	global startObj
	if {[string compare $viewedit View] == 0} {
		return
	}
	set x [$c canvasx $x]
	set y [$c canvasy $y]
	foreach item [$c find overlapping $x $y $x $y] {
		foreach tag [$c gettags $item] {
			if {[string first "node" $tag] == 1} {
				set item [string range $tag 1 end]
				if {[string equal $graphtype digraph]} {
					set startObj [$c create line $x $y $x $y \
						 -tag $item -fill red -arrow last]
				} {
					set startObj [$c create line $x $y $x $y \
						 -tag $item -fill red]
				}
				return
			}
		}
	}
	set startObj [$c create oval [expr $x - 10] [expr $y - 10] \
		[expr $x + 10] [expr $y + 10] -fill red -outline black]
}

# if node started by b1_press then move it,
# else extend edge
proc mouse_b1_motion {nr x y} {
	upvar #0 W_[set nr](c) c
	upvar #0 W_[set nr](id) id
	upvar #0 W_[set nr](viewedit) viewedit
	upvar #0 G_[set id](g) g
	global startObj
	if {[string compare $viewedit View] == 0} {
		return
	}
	set pos [$c coords $startObj]
	if {[$c type $startObj] == "line"} {
		$c coords $startObj [lindex $pos 0] [lindex $pos 1] \
			[$c canvasx $x] [$c canvasy $y]
	} {
		$c move $startObj [expr [$c canvasx $x] - [lindex $pos 0] - 10] \
			[expr [$c canvasy $y] - [lindex $pos 1] - 10]
	}
}

# complete node or edge construction.
proc mouse_b1_release {nr x y} {
	upvar #0 W_[set nr](c) c
	upvar #0 W_[set nr](id) id
	upvar #0 W_[set nr](viewedit) viewedit
	upvar #0 G_[set id](g) g
	upvar #0 G_[set id](modified) modified
	upvar #0 G_[set id](ofid) ofid
	global startObj
	if {[string compare $viewedit View] == 0} {
		return
	}
	set x [$c canvasx $x]
	set y [$c canvasy $y]
	set t [$c type $startObj]
	debugcl $id 1 "mouse_b1_release $t"
	if {$t == "line"} {
		set pos [$c coords $startObj]
		if {[lindex $pos 0] != [lindex $pos 2] || [lindex $pos 1] != [lindex $pos 3]} {
			set tail [lindex [$c gettags $startObj] 0]
			foreach item [$c find overlapping $x $y $x $y] {
				foreach tag [$c gettags $item] {
					set head [string range $tag 1 end]
					if {[string first "node" $head] == 0} {
						set e [$tail addedge $head]
						$c dtag $startObj $tail
						$c addtag 1$e withtag $startObj
						$c itemconfigure $startObj -fill black
						set modified 1
						update_render $nr
						set startObj {}
						return
					}
				}
			}
		}
		# if we get here then edge isn't terminating on a node
		# or we did not move the mouse at all
		$c delete $startObj
	} {
# The following works with old tcldot but in newer we get
# canvas objects tagged as 1graph1 without corresponding graph1
# command, or so it seems
		set sg $g
		foreach item [$c find overlapping $x $y $x $y] {
			# puts  "item $item"
			foreach tag [$c gettags $item] {
				# puts  "item $item tag $tag"
				if {[string first "graph" $tag] == 1} {
					set sg [string range $tag 1 end]
					if {[lsearch [info commands] $sg] >= 0} {
						set sg $g
					}
				}
			}
		}
		# set n [$sg addnode]
		set nn [autnextnode $id]
		set cmd [concat $sg addnode $nn]
		debugcl $id 1 "cmd $cmd"
		# puts stderr "cmd $cmd"

		if {[catch $cmd n]} {
			catch {puts $ofid "$G(cprog): cannot create node $nn: $n"}
			$c delete $startObj
		} else {
			$c addtag 1$n withtag $startObj
			$c itemconfigure $startObj -fill white
			set modified 1
			updatenodepfx $id $n
			autstart $id $n 1
			update_render $nr
		}
	}
	set startObj {}
}

proc setoneattribute {id w d a s o t} {
	upvar #0 S_[set id](nr) w_nrs
	set aa [$w.e$a.a get]
	if {$aa == {}} {
		error "no attribute name set"
	} {
		set v [$w.e$a.v get]
		if {[string compare $aa label] == 0 && [string compare $t node] == 0} {
			set nn [findnode $id $v]
			if {[string compare $nn ""] != 0 && [string compare $nn $o] != 0} {
				set doit [tk_messageBox -parent $w -message "node $v already exists. Are you sure?" \
						-type yesno]
				if {[string compare $doit yes] != 0} {
					return
				}
			}
			updatenodepfx $id $o del
		} elseif {[string compare $aa autstart] == 0 && [string compare $t node] == 0 && [string compare $v 1] == 0} {
			if {![catch {$o queryattributes autexclude} exc] && $exc == 1} {
				set doit [tk_messageBox -parent $w -message "node $v is excluded from aut. Are you sure?" \
						-type yesno]
				if {[string compare $doit yes] != 0} {
					return
				}
			}
		} elseif {[string compare $aa autstart] == 0 && [string compare $t node] == 0 && [string compare [getautstart $id] $o] == 0 && [string compare $v 1] != 0} {
			set doit [tk_messageBox -parent $w -message "the automaton will have no start state. Are you sure?" \
					-type yesno]
			if {[string compare $doit yes] != 0} {
				return
			}
		}
		eval $s $aa $v
		puts stderr "($s) ($aa) ($v) ($t) ($a)"
		if {[string compare $t edge] == 0} {
			if {[string compare $aa name] == 0} {
				delalias $id edge $o
				addalias $id edge $o $v
			}
		} elseif {[string compare $t node] == 0} {
			if {[string compare $aa label] == 0} {
				delalias $id node $o
				addalias $id node $o $v
				updatenodepfx $id $o
			} elseif {[string compare $aa autstart] == 0} {
				if {$v == 1} {
					autstart $id $o
				} else {
					delautstart $id $o
				}
			}
		}
		update_render [lindex $w_nrs 0]
	}
	if {$a == {}} {
		destroy $w.e
		addEntryPair $id $w $d $aa $v $s $o $t
		addEntryPair $id $w d {} {} $s $o $t
	}
}

proc addEntryPair {id w d a v s o t} {
	pack [frame $w.e$a] -side top -fill x
	pack [entry $w.e$a.a] -side left -fill x
	pack [entry $w.e$a.v] -side left -fill x -expand 1
	if {$a != {}} {
		$w.e$a.a insert end $a
		$w.e$a.a configure -state disabled -relief flat
		$w.e$a.v insert end $v
		if {$d != "d"} {
			$w.e$a.v configure -state disabled -relief flat
		}
	}
	bind $w.e$a.a <Return> "focus $w.e$a.v"
	bind $w.e$a.v <Return> [list setoneattribute $id $w $d $a $s $o $t]
	focus $w.e$a.a
}

proc delobj {id o} {
	upvar #0 S_[set id](nr) w_nrs
	upvar #0 W_[lindex $w_nrs 0](c) c

	deleteobj $id $c $o
}
# resetattr was meant to work around the case
# where deleted node/edge items seemed to be
# reincarnated with some of the attributs of their
# previous life.
# it seems this is not happening, though.
proc resetattr {o} {
	foreach a [$o listattributes] {
		# puts stderr "resetattr $o : $a"
		$o setattributes $a {}
	}
}
proc deleteobj {id c o} {
	debug 0 "deleteobj $id $c $o"
	if {[string first "node" $o] == 0} {
		foreach e [$o listedges] {
			$c delete 1$e
			$c delete 0$e
			# resetattr $e
			$e delete
			delalias $id edge $e
		}
		delalias $id node $o
		delautstart $id $o
		updatenodepfx $id $o del
	}
	$c delete 1$o
	$c delete 0$o
	# resetattr $o
	$o delete
	if {[string first "edge" $o] == 0} {
		delalias $id edge $o
	}
}

proc setAttributesWidget {id c o t d l q s} {
	upvar #0 S_[set id](nr) w_nrs
	set w .attributes
	catch {destroy $w}
	toplevel $w
	wm withdraw $w
	wm title $w "$o Attributes"
	wm iconname $w "Attributes"
	foreach a [eval $l] {
		if {[catch {eval $q $a} v]} {set v {}}
		addEntryPair $id $w $d $a $v $s $o $t
	}
	addEntryPair $id $w d {} {} $s $o $t
	frame $w.spacer -height 3m -width 20
	frame $w.buttons
	if {$d == "d"} {
		 button $w.buttons.delete -text Delete -command "deleteobj $id $c $o; destroy $w; update_render [lindex $w_nrs 0]"
		 pack $w.buttons.delete -side left -expand 1
	}
	button $w.buttons.dismiss -text Dismiss -command "destroy $w"
	pack $w.buttons.dismiss -side left -expand 1
	pack $w.buttons -side bottom  -fill x -pady 2m
	pack $w.spacer -fill y -expand 1
	positionWindow $w $c
}

proc setAttributes {nr obj m X Y mlast} {
	upvar #0 W_[set nr](c) c
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](g) g
	upvar #0 G_[set id](modified) modified
	global bmlast
	if {$obj == {}} {
		# puts stderr "setAttributes [$c gettags current]"
		set obj [string range [lindex [$c gettags current] 0] 1 end]
	}
	set type [string range $obj 0 3]
	set typeg [string range $obj 0 4]
	if {$type == "node" || $type == "edge"} {
		if {[string length $obj] > 4} {
			setAttributesWidget $id $c $obj $type d \
				"$obj listattributes" \
				"$obj queryattributes" \
				"$obj setattributes"
		} {
			setAttributesWidget $id $c $obj $type {} \
				"$g list[set type]attributes" \
				"$g query[set type]attributes" \
				"$g set[set type]attributes"
		}
	} elseif {$typeg == "graph" && [string length $obj] > 5 && [lsearch [info commands] $obj] >= 0} {
		# seems with recent tcldot we do get canvas objects
		# tagged something like 1graph1 without corresponding
		# graph1 command in the tcl/tk interpreter
		# work around by seccond part in condition above

		# with older tcldot versions,
		# it seems things break when
		#  - we try to delete a subgraph
		#     (we also loose the main graph?)
		# - we try to set subgraph label of clusterg1
		#    (it appears not in but below the cluster box)
		setAttributesWidget $id $c $obj $typeg {} \
			"$obj listattributes" \
			"$obj queryattributes" \
			"$obj setattributes"
	} elseif {$typeg == "graph" && [string length $obj] == 5} {
			setAttributesWidget $id $c $obj $typeg {} \
				"$g listattributes" \
				"$g queryattributes" \
				"$g setattributes"
	} else {
		tk_popup $m $X $Y $mlast
	}
}

proc positionWindow {w {c {}}} {
	eval wm transient $w $c
#	wm sizefrom $w user
#	wm positionfrom $w user 
	set px [winfo pointerx $w]
	set py [winfo pointery $w]
#	wm geometry $w +$px+$py
	wm deiconify $w
	if {[catch {tkwait visibility $w} msg]} {
		debug 0 "AnidotSrv: positionWindow: tkwait visibility failed: $msg"
		return
	}
	set wh [winfo height $w]
	set ww [winfo width $w]
	set sw [winfo screenwidth $w]
	set sh [winfo screenheight $w]
	set x [expr $px - ($ww / 2)]
	set y [expr $py - ($wh / 2)]
	set xmin 0
	set xmax [expr $sw - $ww]
	set ymin 0
	set ymax [expr $sh - $wh]
	if {$x < $xmin} {
		set x $xmin
	} elseif {$x > $xmax} {
		set x  $xmax
	}
	if {$y < $ymin} {
		set y $ymin
	} elseif {$y > $ymax} {
		set y $ymax
	}
	debug 3 "positionWindow $w w=[set ww]x[set wh] px=[set px]x[set py] s=[set sw]x[set sh] min=[set xmin]x[set ymin] max=[set xmax]x[set ymax] g=[set x]x[set y]"
	wm geometry $w +$x+$y
	raise $w
}

proc message {m} {
	set w .message
	catch {destroy $w}
	toplevel $w
	wm withdraw $w
	wm title $w "Message"
	wm iconname $w "Message"
	label $w.message -text "\n$m\n"
	pack $w.message -side top -anchor w
	positionWindow $w
	update
	after 2000 "destroy $w"
}

proc warning {m} {
	set w .warning
	catch {destroy $w}
	toplevel $w
	wm withdraw $w
	wm title $w "Warning"
	wm iconname $w "Warning"
	label $w.message -text "\nWarning:\n\n$m"
	pack $w.message -side top -anchor w
	positionWindow $w
	update
	after 2000 "destroy $w"
}

proc print {nr} {
	upvar #0 W_[set nr](c) c
	upvar #0 G_[set id](g) g
	global printCommand

	if {[catch {open "| $printCommand" w} f]} {
		warning "Unable to open pipe to printer command:\n$printCommand; return"
	}
	$g write $f ps
	close $f
	message $c "Graph printed to:\n$printCommand"
}

proc setPrinterCommand {w} {
	global printCommand
	set printCommand [$w.printCommand get]
	message "Printer command changed to:\n$printCommand"
	destroy $w
}

proc printSetup {nr} {
	upvar #0 W_[set nr](c) c
	global printCommand
	set w .printer
	catch {destroy $w}
	toplevel $w
	wm withdraw $w
	wm title $w "Printer"
	wm iconname $w "Printer"
	label $w.message -text "Printer command:"
	frame $w.spacer -height 3m -width 20
	entry $w.printCommand 
	$w.printCommand insert end $printCommand
	bind $w.printCommand <Return> "setPrinterCommand $w"
	frame $w.buttons
	button $w.buttons.confirm -text OK -command "setPrinterCommand $w"
	button $w.buttons.cancel -text Cancel -command "destroy $w"
	pack $w.buttons.confirm $w.buttons.cancel -side left -expand 1
	pack $w.message $w.spacer $w.printCommand -side top -anchor w
	pack $w.buttons -side bottom -expand y -fill x -pady 2m
	positionWindow $w $c
}

