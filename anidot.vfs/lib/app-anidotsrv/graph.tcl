package provide app-anidotsrv 1.0

proc newgraph {id ofid} {
	upvar #0 G_[set id]_mode mode
	upvar #0 G_[set id](graphtype) graphtype

	set mode stdin
	set graphtype digraph
	set g [dotnew $graphtype]
	resetpfx $id S

	return $g
}

proc opengraph {id dotfile ofid} {
	upvar #0 G_[set id]_mode mode
	upvar #0 G_[set id](graphtype) graphtype
	global G

	set mode file
	set graphtype digraph
	resetpfx $id
	if {[catch {open $dotfile r} dotfd]} {
		catch {puts $ofid "$cprog: cannot open $dotfile: $dotfd"}
		return ""
	}

	if {[catch {dotread $dotfd} g]} {
		catch {puts $ofid "$cprog: parse error in $dotfile: $g"}
		return ""
	}

	foreach n [$g listnodes] {
		if {![catch {$n queryattributes label} l]} {
			if {[string compare $l {{\N}}] != 0} {
				addalias $id node $n $l
			}
		}
		autscannode $id $g $n
	}
	foreach e [$g listedges] {
		if {![catch {$e queryattributes name} n]} {
			if {[string compare $n ""] != 0 && [string compare $n "{}"] != 0} {
				addalias $id edge $e $n
			}
		}
		autscanedge $id $g $e
	}

	return $g
}

proc rendergraph {nrs g ofid} {
	debug 1 "rendergraph ($nrs) ($g) {"
	foreach nr $nrs {
		upvar #0 W_[set nr](c) c
		upvar #0 W_[set nr](t) t

#		# reset zoom (alternative to code in reusecanvas)
#		set zd [zoomdepth $nr]
#		zoom $nr [expr 1 / $zd]

		$c addtag all all
		$c delete all
		bind $t <Configure> {}
		# reset zoom
		debug 1 "zoomdepth=[zoomdepth $nr]"
		zoom $nr [expr 1 / [zoomdepth $nr]]
		debug 1 "zoomdepth=[zoomdepth $nr]"
	
		debug 1 "	rendergraph ($nrs) ($g) render $nr {"
		# note: next line depends on canvas variable name: 'c'
		# debug 1 "	rendergraph ($nrs) ($g) render $nr [$g render]"
		# eval [$g render $c neato]
		if {![catch {$g queryattributes layout} layout] && [string compare $layout neato] == 0} {
			set cmd [concat $g render $c neato]
		} else {
			set cmd [concat $g render]
		}
		if {[catch $cmd code]} {
			warn $ofid "could not render: $code"
		} else {
			regsub -all stderr $code $ofid code
			# for old wish, where canvas text items have no -state
			debug 2 "	rendergraph ($nrs) ($g) render $nr {$code}"
			regsub -all -- "-state\[ \t\]+\[a-z\]+" $code "" code
			debug 2 "	rendergraph edited {$code}"
			if {[catch {eval $code} msg]} {
				warn $ofid "could not eval render: $msg"
			}
			# $c scale all 0 0 .01 .01 
		}
		debug 1 "	rendergraph ($nrs) ($g) render $nr }"

#		zoom $nr $zd

		debug 1 "	rendergraph ($nrs) ($g) scaletofittrigger $nr {"
		scaletofittrigger $nr
		debug 1 "	rendergraph ($nrs) ($g) scaletofittrigger $nr }"
		bind $t <Configure>  "scaletofittrigger $nr"
	}
	debug 1 "rendergraph ($nrs) ($g) }"
}

proc debuggraph { g } {
	foreach i [list "" node edge] {
		set s "\"$i\""
		foreach a [$g list[set i]attributes] {
			append s " $a=[$g query[set i]attributes $a]"
		}
		puts $s
	}
	foreach i [concat [$g listnodes] [$g listedges]] {
		set s "$i showname=[$i showname]"
		foreach a [$i listattributes] {
			append s " $a=[$i queryattributes $a]"
		}
		puts $s
	}
}

proc addalias {id t v n} {
	upvar #0 G_[set id]_name2edge name2edge
	upvar #0 G_[set id]_label2node label2node
	debug 3 "addalias $id $t $v $n"
	if {[string compare $t edge] == 0} {
		if {[info exists name2edge($n)] &&  [lsearch $name2edge($n) $v] >= 0} {
			debug 3 "duplicate addalias $id $t $v $n"
		} else {
			lappend name2edge($n) $v
		}
	} elseif {[string compare $t node] == 0} {
		if {[info exists label2node($n)] &&  [lsearch $label2node($n) $v] >= 0} {
			debug 3 "duplicate addalias $id $t $v $n"
		} else {
			lappend label2node($n) $v
		}
	}
}

proc delalias {id t v} {
	upvar #0 G_[set id]_name2edge name2edge
	upvar #0 G_[set id]_label2node label2node
	debug 3 "delalias $id $t $v"
	if {[string compare $t edge] == 0 && [info exists name2edge]} {
		foreach k [array names name2edge] {
			set name2edge($k) [ldelete $name2edge($k) $v]
			if {[llength $name2edge($k)] == 0} {
				catch {array unset  name2edge $k}
			}
		}
	} elseif {[string compare $t node] == 0 && [info exists label2node]} {
		foreach k [array names label2node] {
			set label2node($k) [ldelete $label2node($k) $v]
			if {[llength $label2node($k)] == 0} {
				catch {array unset  label2node $k}
			}
		}
	}
}

proc findnode {id n} {
	upvar #0 G_[set id]_label2node label2node
	upvar #0 G_[set id](g) g
	debug 3 "findnode $id $g $n ([array names label2node ])"

	set res {}

	if {[info exists label2node($n)]} {
		set res [concat $res $label2node($n)]
	}
	if {![catch {$g findnode $n} node]} {
		set res [concat $res $node]
	}
	debug 3 "findnode $id $g $n ([array names label2node ]) -> $res"
	return $res
}

proc findedge {id name} {
	upvar #0 G_[set id](g) g
	upvar #0 G_[set id]_name2edge name2edge
	debug 3 "findedge $id $g $name ([array names name2edge ])"
	set res {}
	if {[info exists name2edge($name)]} {
		set res [concat $res $name2edge($name)]
	}
	set src {}
	set dst {}
	foreach n [parseedges $name] {
		set dst $n
		if {[llength $src] > 0 && [llength $dst] > 0} {
			foreach i [findnode $id $src] {
				foreach j [findnode $id $dst] {
					if {![catch {$g findedge [$i showname] [$j showname]} edge]} {
						set res [concat $res $edge]
					}
				}
			}
		}
		set src $n
	}
	debug 1 "findedge $id $g $name ([array names name2edge ]) -> ($res)"
	return $res
}

proc find {id n k} {
	return [find$k $id $n]
}

proc isknownsubgraph {id g} {
	upvar #0 G_[set id]_name2graph name2graph
	global name2graph
	return 0
}
