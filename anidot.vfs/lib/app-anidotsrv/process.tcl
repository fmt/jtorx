package provide app-anidotsrv 1.0

proc start { argv } {
	global tcl_platform
	set tp $tcl_platform(platform)
	switch -- $tp {
	    unix {
		exec sh -c "expnohup $argv &" &
	  } windows {
		exec $argv &
	  }  macintosh {
		msg stderr "don't know how to start $argv (on platform $tp)"
	  } default {
		msg stderr "don't know how to start $argv (unknown platform $tp)"
	  }
	}
}

proc kill {p} {
	global tcl_platform

	msg stderr "cleaning up: kill $p"
	set tp $tcl_platform(platform)
	switch -- $tp {
	    unix {
		catch {exec kill $p}
		msg stderr "cleaning up: kill -9 $p"
		catch {exec kill -9 $p}
	  } windows -
	    macintosh {
		msg stderr "don't know how to kill $p (on platform $tp)"
	  } default {
		msg stderr "don't know how to kill $p (unknown platform $tp)"
	  }
	}
}

proc findprocess {pid p} {
	global tcl_platform

	set found 0
	set tp $tcl_platform(platform)
	switch -- $tp {
	    unix {
		if {[catch {open "|ps -ef"} pidsfd]} {
			warn stderr "could not run: ps -ef"
			exit 1
		} else {
			debug 0 "$pid: looking for $p"
			while {[gets $pidsfd line] >= 0} {
				debug 25 "$pid: looking for $p in: $line"
				if {[lindex $line 1] == $p}  {
					msg stderr "found running server pid: $p"
					set found 1
					break
				}
			}
			close $pidsfd
		}
	  } windows {
		warn stderr "findprocess windows"
		if {![catch {open "|tasklist"} pidsfd]} {
			warn stderr "findprocess windows in tasklist"
			debug 0 "$pid: looking for $p"
			while {[gets $pidsfd line] >= 0} {
				debug 25 "$pid: looking for $p in: $line"
				if {[lindex $line 2] == $p}  {
					msg stderr "tasklist found running server pid: $p"
					set found 1
					break
				}
			}
			close $pidsfd
		} elseif {![catch {open "|tlist"} pidsfd]} {
			warn stderr "findprocess windows in tlist"
			while {[gets $pidsfd line] >= 0} {
				debug 0 "$pid: looking for $p in: $line"
				set nr [lindex $line 0]
				set nr2 [expr $nr - pow(2,32)]
				if {$nr == $p || $nr2 == $p}  {
					msg stderr "tlist found running server pid: $p"
					set found 1
					break
				}
			}
			close $pidsfd
		} else {
			# should try tlist on other windows?
			warn stderr "could not run: tasklist"
			exit 1
		}
	  } macintosh {
		msg stderr "don't know how to kill $p (on platform $tp)"
	  } default {
		msg stderr "don't know how to kill $p (unknown platform $tp)"
	  }
	}
	return $found
}

