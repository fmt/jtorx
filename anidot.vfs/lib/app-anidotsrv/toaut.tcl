package provide app-anidotsrv 1.0

proc autstart {id n {unsetonly 0}} {
	upvar #0 G_[set id](autstart) autstart

	# puts stderr "$id $n $unsetonly"
	if {[info exists autstart] && [string compare $autstart "{}"] != 0} {
		if {$unsetonly} {
			return
		}
		catch {$autstart setattributes autstart 0}
	}
	set autstart $n
	$n setattributes autstart 1
}
proc getautstart {id} {
	upvar #0 G_[set id](autstart) autstart

	if {[info exists autstart] && [string compare $autstart "{}"] != 0} {
		return $autstart
	} else {
		return ""
	}
}
proc delautstart {id n} {
	upvar #0 G_[set id](autstart) autstart
	upvar #0 G_[set id](pfxprob) pfxprob

	if {[info exists autstart] && [string compare $autstart $n] == 0} {
		catch {unset autstart}
	}
}

proc resetpfx {id {p ""}} {
	upvar #0 G_[set id](pfx) pfx
	upvar #0 G_[set id](pfxok) pfxok
	upvar #0 G_[set id](pfxnr) pfxnr
	upvar #0 G_[set id](pfxall) pfxall
	upvar #0 G_[set id](pfxprob) pfxprob
	set pfx $p
	set pfxok 1
	set pfxnr 0
	set pfxall {}
	set pfxprob {}
}

proc updatenodepfx {id i {mode add}} {
	upvar #0 G_[set id](pfx) pfx
	upvar #0 G_[set id](pfxok) pfxok
	upvar #0 G_[set id](pfxnr) pfxnr
	upvar #0 G_[set id](pfxall) pfxall
	upvar #0 G_[set id](pfxprob) pfxprob

	set ok 0
	set nn [$i showname]
	if {![catch {$i queryattributes label} nl] && [string compare $nl {{\N}}] != 0} {
		set nn $nl
	}
	if {[regexp {^(.*[^0-9][^0-9]*)?0*([0-9][0-9]*)$} $nn match npfx nr]} {
		debug 2 "updatenodepfx ($id) ($i) ($nn) ($pfx) ($npfx) ($nr)"
		# if {[regexp {^00*([1-9][0-9]*)$} $nr match nnr]} {
		#	set nr $nnr
		# }
		if {[string compare $mode add] == 0 && $nr == 0} {
			autstart $id $i 1
		}
		if {[string compare $pfx $npfx] == 0} {
			set ok 1
			if {[string compare $mode add] == 0} {
				lappend pfxall $nr
				if {$nr >= $pfxnr} {
					set pfxnr [expr $nr + 1]
				}
			} elseif {[string compare $mode del] == 0} {
				set pfxall [ldelete $pfxall $nr]
				if {[llength $pfxall] > 0} {
					set last [lindex $pfxall end]
					set pfxnr [expr $last + 1]
				} else {
					set pfxnr 0
				}
#				if {[expr $nr +1] == $pfxnr} {
#					set  pfxnr $nr
#				}
			}
		} elseif {$pfxnr == 0 && [string compare $mode add] == 0} {
			set ok 1
			set pfxnr [expr $nr + 1]
			lappend pfxall $nr
			set pfx $npfx
		}
	}
	if {[string compare $mode add] == 0 && ! $ok} {
		if {[lsearch $pfxprob $i] < 0} {
			lappend pfxprob $i
		}
	} else {
		set pfxprob [ldelete $pfxprob $i]
	}
	if {[llength $pfxprob] > 0} {
		set pfxok 0
	} else {
		set pfxok 1
	}
	set pfxall [lsort -integer $pfxall]
	debug 2 "updatenodepfx ($id) ($i) ($nn) ($ok) ($pfxok) ($pfx) ($pfxnr) ($pfxall) ($pfxprob)"
}
proc autnode {id i} {
	upvar #0 G_[set id](pfx) pfx
	upvar #0 G_[set id](pfxok) pfxok
	upvar #0 G_[set id](pfxnr) pfxnr

	set nn [$i showname]
	if {![catch {$i queryattributes label} nl] && [string compare $nl {{\N}}] != 0} {
		set nn $nl
	}
	if {$pfxok} {
		set l [string length $pfx]
		set n [string range $nn $l end]
		if {[string compare [getautstart $id] $i] == 0 && $n != 0} {
			return 0
		} elseif {[string compare [getautstart $id] $i] != 0 && $n == 0} {
			return $pfxnr
		} else {
			return $n
		}
	} else {
		return $nn
	}
}

proc autnextnode {id} {
	upvar #0 G_[set id](pfx) pfx
	upvar #0 G_[set id](pfxok) pfxok
	upvar #0 G_[set id](pfxnr) pfxnr

	if {$pfxok} {
		set n [set pfx][set pfxnr]
		incr pfxnr
	} else {
		set n ""
	}
	debug 2 "autnextnode $id ($pfx) ($pfxok) ($pfxnr) -> ($n)"
	return $n
}

proc autscannode { id g i } {
	set n [$i showname]
	if {![catch {$i queryattributes autstart} as] && $as == 1} {
		autstart $id $i
	}
	if {[regexp {^_} $n]} {
		$i setattributes autexclude 1
	} elseif {![catch {$i queryattributes label} nl] && [regexp {^_} $nl]} {
		$i setattributes autexclude 1
	} else {
		# we do not explicitly set it to zero, to not pollute the dot file
		# $i setattributes autexclude 0
		updatenodepfx $id $i
	}
}
proc autscanedge { id g i } {
	set n [$i showname]
	if {[regexp {(.*)->(.*)} $n match src dst]} {
		set s [$g findnode $src]
		set d [$g findnode $dst]
		if {[catch {$s queryattributes autexclude} xs] || $xs != 1} {
			set xs 0
		}
		if {[catch {$d queryattributes autexclude} xd] || $xd != 1} {
			set xd 0
		}
		if {$xs && ! $xd} {
			set nn [$g findnode $dst]
			autstart $id $nn
		}
	}
}

proc graph2aut { id g } {
	upvar #0 S_[set id](nr) w_nrs
	upvar #0 W_[lindex $w_nrs 0](c) c

	set nodes {}
	set edges {}

	set start [getautstart $id]
	if {[string compare $start ""] == 0} {
		tk_messageBox -parent $c \
			-message "no start state. please set attribute 'autstart' to 1 for start state node"
		return
	}

	foreach i [$g listnodes] {
		set n [$i showname]
		if {[catch {$i queryattributes autexclude} excl] || $excl != 1} {
			lappend nodes $i
			set autexclude($n) 0
		} else {
			set autexclude($n) 1
		}
		debug 3 "node=$i name=$n excl=$autexclude($n)"
	}
	foreach i [$g listedges] {
		set n [$i showname]
		if {[regexp {(.*)->(.*)} $n match src dst]} {
			debug 3 "edge=$i name=$n autexclude($src)=$autexclude($src) autexclude($dst)=$autexclude($dst)"
			if {(! $autexclude($src)) && (! $autexclude($dst))} {
				set l ""
				catch {set l [$i queryattributes label]}
				set s [$g findnode $src]
				set d [$g findnode $dst]
				lappend edges [list $s $l $d]
			}
		}
	}

	set res {}
	lappend res "des ([autnode $id $start], [llength $edges], [llength $nodes])"
	foreach e $edges {
		set s [lindex $e 0]
		set l [lindex $e 1]
		set d [lindex $e 2]
		if {[string compare $l "{}"] == 0} {
			set l ""
		}
		lappend res "([autnode $id $s], \"$l\", [autnode $id $d])"
	}
	return $res
}
proc saveaut {nr} {
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](g) g

	# debuggraph $g

	set autlines [graph2aut $id $g]
	if {[llength $autlines] <= 0} {
		return
	}

	set fd [getwritefd $id Automaton .aut]

	if {[string compare $fd ""] != 0} {
		foreach l $autlines {
			puts $fd $l
		}
		close $fd
	}
}
