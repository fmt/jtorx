package provide app-anidotsrv 1.0

proc savedot {nr} {
	upvar #0 W_[set nr](id) id
	upvar #0 G_[set id](g) g

	# debuggraph $g
			
	set fd [getwritefd $id Dotfile .dot]

	if {[string compare $fd ""] != 0} {
		$g write $fd canon
		close $fd
	}
}
