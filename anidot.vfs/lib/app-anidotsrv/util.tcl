package provide app-anidotsrv 1.0

proc ldelete {l e} {
	 set ix [lsearch -exact $l $e]
	 if {$ix >= 0} {
	 	return [lreplace $l $ix $ix]
	 } else {
	 	return $l
	 }
}

proc isinteger { s } {
	# ideal implementation: return [string is integer -strict $s]
	# but that does not work in older wish versions
	if {[regexp "^\[-+\]?\[0-9\]\[0-9\]*$" $s]} {
		return 1
	} else {
		return 0
	}
}

# split on space or tab,
# but keep groups enclosed within
# single or double quotes together
proc mysplit { s } {
	set words {}
	set s [string trim $s]
	set i 0
	set l [string length $s]
	set inString 0
	set inWord 0
	set escaping 0
	set quoteChar ""
	while {$i < $l} {
		set c [string index $s $i]
		if {$inString} {
			if {$escaping} {
				set escaping 0
			} else {
				if {[string compare $c "\\"] == 0} {
					set escaping 1
				} elseif {[string compare $c $quoteChar] == 0} {
					set inString 0
					lappend words [string range $s $start [expr $i - 1]]
				}
			}
		} elseif {$inWord} {
			if {[string compare $c " "] == 0 ||
			     [string compare $c "\t"] == 0} {
			     	set inWord 0
			     	lappend words [string  range $s $start [expr $i - 1]]
			}
		} else {
			# look if word is quoted
			foreach q {"\"" "'"} {
				if {[string compare $c $q] == 0} {
					set inString 1
					set start [expr $i + 1]
					set quoteChar $q
					break
				}
			}
			if {! $inString} {
				if {[string compare $c " "] != 0 &&
				      [string compare $c "\t"] != 0} {
				      set inWord 1
				      set start $i
				}
			}
		}
		incr i
	}
	if {$inWord} {
		lappend words [string  range $s $start [expr $i - 1]]
	} elseif {$inString} {
		puts stderr "Error: mysplit: string ends in quoted string"
		lappend words [string  range $s $start [expr $i - 1]]
	}
	return $words
}	
	
#	while {[string compare "" $s] != 0} {
#		if {[regexp {^"([^"]*)"[ 	]*(.*)$} $s match word s]} {
#			lappend words $word
#		} elseif {[regexp {^'([^']*)'[ 	]*(.*)$} $s match word s]} {
#			lappend words $word
#		} elseif {[regexp {^([^ 	]*)[ 	]*(.*)$} $s match word s]} {
#			lappend words $word
#		} else {
#			warning "oops: could not match: ($s)"
#			set s ""
#		}
#	}

