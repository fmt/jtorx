set platform [lindex $::tcl_platform(os) 0]
set machine [lindex $::tcl_platform(machine) 0]
set lib tcldot[info sharedlibextension]
set libnew tcldot3[info sharedlibextension]
lappend tcl_libPath  [file join $dir $platform $lib]

proc winloadlib {dir platform lib} {
	global tcl_libPath
	global env
	# puts stderr "platform $platform"
	# Stupid temp directory
	set S(tmp) [pwd]
	if {[file isdirectory "c:/temp"]} { set S(tmp) "c:/temp"}
	if {[file isdirectory "/tmp"]} { set S(tmp) "/tmp"}
	catch {set S(tmp) $env(TRASH_FOLDER)}           ;# Macintosh(?)
	catch {set S(tmp) $env(TMPDIR)}
	catch {set S(tmp) $env(TMP)}
	catch {set S(tmp) $env(TEMP)}
	set tdir [file join $S(tmp) tcldotlib]
	# puts stderr "tdir $tdir"
	if {[catch {file mkdir $tdir} err]} {
		error "failed to create new temp directory $tdir: $err"
	}
	foreach f [glob [file join $dir $platform *]] {
		if {![file exists [file join $tdir [file tail $f]]]} {
			if {[catch {file copy -- $f $tdir} err]} {
				error "failed to copy $f to new temp directory $tdir: $err"
			}
		}
	}
	if {[string compare -nocase $platform Linux]==0} {
		# somehow this does not work
		lappend tcl_libPath $tdir
		set env(LD_LIBRARY_PATH) ".:$tdir"
		set env(LD_RUN_PATH) ".:$tdir"
		puts stderr "loading ($tcl_libPath) ($tdir) ($lib) ([pwd]) ($env(LD_LIBRARY_PATH)) ($env(LD_RUN_PATH))..."
		load [file join $tdir $lib]
	} else {
		set pwd [pwd]
		cd $tdir
		puts stderr "loading ($tcl_libPath) ($tdir) ($lib) ([pwd]) ..."
		load $lib Tcldot
		cd $pwd
	}
}

if {[string compare -nocase $platform Windows] == 0} {
  package ifneeded Tcldot 1.7.7 [list winloadlib $dir $platform $lib]
} elseif {[string compare -nocase $platform Darwin] == 0} {
  package ifneeded Tcldot 2.20.2 [list winloadlib $dir $platform libtcldot_builtin.dylib]
#    if {[catch {file delete -force -- $tdir} err]} {
#      error "failed to remove temp directory $tdir: $err"
#    }
 } elseif {[string compare -nocase $platform Linux] == 0 &&
           [file exists /usr/lib/libpng.so.3] &&
 	   ![file exists  /usr/lib/libpng.so]} {
  package ifneeded Tcldot 1.7.7 [list load [file join $dir $platform $libnew] Tcldot]
 } else {
  package ifneeded Tcldot 1.7.7 [list load [file join $dir $platform $lib] Tcldot]
 }


# } elseif {[string compare -nocase $platform Linux] == 0 && [string compare -nocase $machine x86_64]==0} {
#  package ifneeded Tcldot 1.7.7 [list load [file join $dir [set platform]_64 $lib] Tcldot]
