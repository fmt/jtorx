#windowlist.tcl: provides routines for managing windows from menu, i.e. minimize, raise, bring all to front; standard menu item on Mac OS X. 

#(c) 2008 WordTech Communications LLC. License: standard Tcl license, http://www.tcl.tk/software/tcltk/license.html

#includes code from http://wiki.tcl.tk/1461

package provide windowlist 1.0

namespace eval windowlist {

    variable omitDefault
    set omitDefault 0
    
#    variable diamond
#     set diamond [ image create bitmap diamond -data {
##define diamond_width 16
##define diamond_height 16
#static unsigned char diamond_bits[] = {
#   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0xc0, 0x01,
#   0xe0, 0x03, 0xf0, 0x07, 0xe0, 0x03, 0xc0, 0x01, 0x80, 0x00, 0x00, 0x00,
#   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
#        } ]
    variable diamond
    set diamond [ image create bitmap diamond -data {
#define diamond_width 16
#define diamond_height 16
static unsigned char diamond_bits[] = {
   0x80, 0x00, 0xc0, 0x01, 0xc0, 0x01, 0xe0, 0x03, 0xe0, 0x03, 0xf0, 0x07,
   0xf0, 0x07, 0xf8, 0x0f, 0xf8, 0x0f, 0xf0, 0x07, 0xf0, 0x07, 0xe0, 0x03,
   0xe0, 0x03, 0xc0, 0x01, 0xc0, 0x01, 0x80, 0x00};
       } ]

	variable checkmark
        set checkmark [ image create bitmap checkmark -data {
#define checkmark_width 16
#define checkmark_height 16
static unsigned char checkmark_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x02,
   0x00, 0x01, 0x00, 0x01, 0x80, 0x00, 0x80, 0x00, 0x44, 0x00, 0x44, 0x00,
   0x28, 0x00, 0x28, 0x00, 0x10, 0x00, 0x10, 0x00};
        } ]
        


    #setttings
    proc omitDefaultWindow {} {
    	variable omitDefault
    	
    	set omitDefault 1
    }
    
    #make the window menu
    proc windowMenu {mainmenu} {
	# puts stderr "windowMenu $mainmenu"

	set m [menu $mainmenu.window]
	
	$m add command -compound left -image {} -label "Minimize" -command [namespace current]::minimizeFrontWindow
	$m add separator
	$m add command -compound left -image {} -label "Bring All to Front" -command [namespace current]::raiseAllWindows
	$m add separator
	$m add separator
	
	$mainmenu add cascade -label "Window" -menu $m
	
	#configure the window menu to update whenever the menu is posted
	$m configure -postcommand [list [namespace current]::updateWindowMenu $m]
	$m configure -tearoffcommand [list [namespace current]::bindTearOffUpdate $m]
    }

    proc toplist2 {} {
	variable omitDefault
	
	set list {}
	
	#use [winfo children .] here to get windows that are minimized
	foreach item [winfo children .] {
	    #get all toplevel windows, exclude menubar windows
	    if { [string equal [winfo toplevel $item] $item] && [catch {$item cget -tearoff}]} {
		lappend list $item
	    }
	}
	if {! $omitDefault} {
		#be sure to deiconify ., since the above command only gets the child toplevels
		lappend list .
	}
	return $list;
   }

    
    #update the window menu with windows
    proc updateWindowMenu {windowmenu} {
    	variable diamond
    	variable checkmark

	set stackwindowlist [wm stackorder .]
	set windowlist [toplist2]
	# puts stderr "updateWindowMenu $windowmenu ($windowlist)"
	if {$windowlist == {}} {
	    return
	} else {
	    $windowmenu delete 4 end	    
	    foreach item $stackwindowlist {
	    	if {[lsearch -exact $windowlist $item] >= 0} {
	    		set m [lindex [$item configure -menu] 4]
			$windowmenu add command -compound left  -image {} -label "[wm title $item]" -command [list [namespace current]::raiseWindow $item]
			if {[string compare $m.window $windowmenu] == 0} {
			 	$windowmenu entryconfigure last -image $checkmark
			}
		}
	    }
	    foreach item $windowlist {
	    	if {[lsearch -exact $stackwindowlist $item] < 0} {
			$windowmenu add command -compound left  -image $diamond -label "[wm title $item]"  -command [list [namespace current]::raiseWindow $item]
		}
	    }
	}
    }

    proc bindTearOffUpdate {windowmenu menu tearoff} {
    	bind $tearoff <<MenuSelect>> [list [namespace current]::updateWindowMenu $windowmenu]
    }

    proc doRaiseWindow {w} {
    	puts stderr "doRaiseWindow $w"
    	wm deiconify $w
	raise $w
    }
     #bring selected window to the front
    proc raiseWindow {w} {
    	puts stderr "raiseWindow $w"
	[namespace current]::doRaiseWindow $w
    }
    

    #make all windows visible
    proc raiseAllWindows {} {
	foreach item [toplist2] {
		# wm deiconify $item
		[namespace current]::raiseWindow $item
	}
    }

    #minimize the selected window
    proc minimizeFrontWindow {} {

	#get list of mapped windows
	set windowlist [wm stackorder .]

	#do nothing if all windows are minimized
	if {$windowlist == {}} {
	    return
	} else {

	    #minimize topmost window
	    set topwindow [lindex $windowlist end]
	    wm iconify $topwindow

	}
    }

    namespace export *


}
