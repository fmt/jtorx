package require starkit
starkit::startup
	set pid [pid]
#	puts "main:$pid: argv: $argv"

if {[string compare [lindex $argv 0] -server] == 0} {
	set argv [lrange $argv 1 end]
	package require app-anidotsrv
} elseif {[lindex $argv 0] == {} &&
     [string compare [lindex $argv 1] -server] == 0} {
	set argv [lrange $argv 2 end]
	package require app-anidotsrv
} else {
	package require app-anidot
}
