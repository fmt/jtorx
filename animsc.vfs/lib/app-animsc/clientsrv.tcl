package provide app-animsc 1.0

proc find_user {} {
	global env
	global tcl_platform

	if {[info exists tcl_platform(user)]} {
		set user $tcl_platform(user)
	} elseif {[info exists env(USER)]} {
		set user $env(USER)
	} elseif {[info exists env(LOGNAME)]} {
		set user $env(LOGNAME)
	} elseif {[info exists env(USERNAME)]} {
		set user $env(USERNAME)
	} else {
		set user unknown
	}
	return $user
}

proc find_tmp {} {
	global env
	# Stupid temp directory
	set S(tmp) [pwd]
	if {[file isdirectory "c:/temp"]} { set S(tmp) "c:/temp"}
	if {[file isdirectory "/tmp"]} { set S(tmp) "/tmp"}
	catch {set S(tmp) $env(TRASH_FOLDER)}           ;# Macintosh(?)
	catch {set S(tmp) $env(TMPDIR)}
	catch {set S(tmp) $env(TMP)}
	catch {set S(tmp) $env(TEMP)}
	return $S(tmp)
}

proc find_display {host} {
	global env

	if {[info exists env(DISPLAY)]} {
		set display $env(DISPLAY)
	} else {
		set display ":0"
	}
	regsub {^:} $display "$host:" display
	regsub {^unix:} $display "$host:" display
	regsub {^/[^:]*:} $display "$host:" display
	regsub {\.[0-9]*$} $display "" display
	return $display
}

proc mk_filename {dir pfx user display} {
	set dname "$pfx-$user-$display"
	regsub -all : $dname _ dname
	set fname [file join $dir $dname]
	return $fname
}

proc get_address { fname do_warn} {
	if {![catch {open $fname r} fid]} {
		if {[gets $fid line] > 0} {
			set words [split $line]
			set ipNr [lindex $words 0]
			set hostName [lindex $words 1]
			set portNr [lindex $words 2]
			return "tcp!$ipNr!$portNr"
		}
		close $fid
	} elseif {$do_warn} {
		warn stderr "$fid"
		return ""
	}
}

proc try_connect { address do_warn} {
	set words [split $address !]
	if {[llength $words] == 3 && [string compare [lindex $words 0] tcp] == 0} {
		set host [lindex $words 1]
		set port [lindex $words 2]
		if {[string compare $host 0.0.0.0] == 0} {
			set host 127.0.0.1
		}
		if {![catch {socket $host $port} sock]} {
			return $sock
		} else {
			warn stderr "could not connect tcp!$host!$port: $sock"
		}
	} elseif {$do_warn} {
		warn stderr "could not parse address $address"
	}
	return 0
}
