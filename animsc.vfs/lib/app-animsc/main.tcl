package provide app-animsc 1.0

set topdir $starkit::topdir

catch {wm withdraw .}

#! @EXPECT@ --
#! /Utils/bin/tclsh
#! /usr/local/bin/expect --

# NOTE: this script only needs tcl(sh); it does not use expect
# features. The only reason that we use the expect interpreter
# here is that the configure script of TorX finds the location of
# expect already for us, and (AFAIK) does not (yet?) look for tclsh.


# exp_internal 1
set debug 0
set G(prog) animsc
set G(srvprog) animscsrv
set G(fnamepart)  animsc

set prog $G(prog)

proc cleanup_exit {{code {0}}} {
	global state
	if {[info exists state(stderr)]} {
		eval fconfigure stderr $state(stderr)
	}
	if {[info exists state(stdout)]} {
		eval fconfigure stdout $state(stdout)
	}
	if {[info exists state(stdin)]} {
		eval fconfigure stdin $state(stdin)
	}
	exit $code
}

proc configure_fids {socket} {
	fconfigure $socket -buffering line  -translation {binary binary}
	fconfigure stdin -buffering line \
		                   -translation {binary binary}
}

proc filevent_fids {socket remoteHost remotePort waitRemoteClose} {
	fileevent $socket readable \
	    	[list getRemote $remoteHost $remotePort $socket]
	fileevent stdin readable \
	 	[list sendRemote $remoteHost $remotePort $socket $waitRemoteClose]
	vwait forever
}

proc setUp {remoteHost remotePort timeout waitRemoteClose} {
	global connected
    global portNr conf
    global state

	warning "trying to connect ($waitRemoteClose) to tcp!$remoteHost!$remotePort"
	after $timeout {set connected timeout}
    if {[catch {socket $remoteHost $remotePort} socket]} {
    	fatal "while connecting to tcp!$remoteHost!$remotePort: $socket" 1
    }
    set portNr [lindex [fconfigure $socket -sockname] 2]
    fileevent $socket w {set connected ok}
    vwait connected
    fileevent $socket w {}
    if {$connected == "timeout"} {
    	return -code error timeout
    } else {
	warning "connected to tcp!$remoteHost!$remotePort at $portNr"
	configure_fids $socket
	filevent_fids $socket $remoteHost $remotePort $waitRemoteClose
    }
}

proc getRemote {remoteHost remotePort sock} {
	fileevent $sock readable {}
	set read [gets $sock line]
	debug "getRemote tcp!$remoteHost!$remotePort read: $read"
	if {$read < 0} {
		if {[eof $sock]} {
			warning "Connection closed by client tcp!$remoteHost!$remotePort."
			cleanup_exit
		} elseif {[fblocked $sock]} {
			warning "getRemote tcp!$remoteHost!$remotePort blocked: $line"
		} else {
			warning "getRemote tcp!$remoteHost!$remotePort error?: $line"
			cleanup_exit
		}
	} else {
		output stdout $line
		debug "getRemote tcp!$remoteHost!$remotePort: $line"
	}
	fileevent $sock readable [list getRemote $remoteHost $remotePort $sock]
}
proc sendRemote {remoteHost remotePort sock waitRemoteClose} {
	fileevent stdin readable {}
	set read [gets stdin line]
	debug "sendRemote tcp!$remoteHost!$remotePort read: $read"
	if {$read < 0} {
		if {[eof stdin]} {
			# set line "A_QUIT"
			# catch {output $sock $line}
			debug "eof stdin"
			if {! $waitRemoteClose} {
				catch {close $sock}
				debug "sendRemote closed tcp!$remoteHost!$remotePort"
				cleanup_exit
			} else {
				return
			}
		} elseif {[fblocked stdin]} {
			warning "sendRemote tcp!$remoteHost!$remotePort blocked: $line"
		} else {
			warning "sendRemote tcp!$remoteHost!$remotePort error?: $line"
			cleanup_exit
		}
	} else {
		catch {output $sock $line}
		debug "sendRemote tcp!$remoteHost!$remotePort: $line"
	}
	fileevent stdin readable [list sendRemote $remoteHost $remotePort $sock $waitRemoteClose]
}

proc main_loop {sock waitRemoteClose argstring eofstring} {
	configure_fids $sock
	set remote [fconfigure $sock -peername]
	set remoteHost [lindex $remote 0]
	set remotePort [lindex $remote 2]
	puts $sock $argstring
	if {[string compare $eofstring ""] != 0} {
		puts $sock $eofstring
	}
	filevent_fids $sock $remoteHost $remotePort $waitRemoteClose
}

proc usage {} {
	warning "usage: $G(prog) \[-r\] \[-m mcastid\] \[-t title\] \[-k key\] \[-s srvaddr \] \[file.msc\]"
		exit 1
}

proc main {} {
	global argv
	global env
	global G

	set waitRemoteClose 0

	set reuse ""
	set mcast ""
	set title ""
	set key ""
	set srvaddr ""
	set files {}
	set exit 0
	set errors 0

	set argstring ""
	
	if {[info exists env(TORXMCASTID)]} {
		set mcast "-m $env(TORXMCASTID)"
	}

	set args $argv
	
#	set joinedargs [join $argv ")(" ]
#	warning "command lin args: ($joinedargs)"

	while {[llength $args] > 0} {
		set a [lindex $args 0]
		set args [lrange $args 1 end]
		switch -- $a {
		    -r {
				set reuse "-r"
		  } -m {
				if {[llength $args] <= 0} {
		  			warning "missing argument for flag: $a"
					set errors 1
				} else {
					set mcast "-m [lindex $args 0]"
					set args [lrange $args 1 end]
				}
		  } -t {
				if {[llength $args] <= 0} {
		  			warning "missing argument for flag: $a"
					set errors 1
				} else {
					set title "-t {[lindex $args 0]]}"
					set args [lrange $args 1 end]
				}
		  } -k {
				if {[llength $args] <= 0} {
		  			warning "missing argument for flag: $a"
					set errors 1
				} else {
					set key "-k [lindex $args 0]"
					set args [lrange $args 1 end]
				}
		  } -s {
				if {[llength $args] <= 0} {
		  			warning "missing argument for flag: $a"
					set errors 1
				} else {
					set srvaddr [lindex $args 0]
					set args [lrange $args 1 end]
				}
		  } -* {
		  		warning "unknown flag: $a"
				set errors 1
		  } default {
		  		lappend files $a
		  }
		}
	}

	if {$errors} {
		usage
	}

#	warning "usage: Bmsc \[-r\] \[-m mcastid\] \[-t title\] \[-k key\] file.dot"

	set argstring "-args $reuse $mcast $title $key"
	set ff {}
	foreach file $files {
		if {[string compare [file pathtype $file] absolute] != 0} {
			set file [file join [pwd] $file]
		}
		lappend ff $file
	}
	append argstring " $ff"

	# puts stderr "sending $argstring"

	if {[llength $files] == 0} {
		set eofstring ""
	} else {
		set eofstring "-eof"
		set waitRemoteClose 1
	}

	set host [info hostname]
	set display [find_display $host]
	set user [find_user]
	set tmp [find_tmp]
	set fname [mk_filename $tmp $G(fnamepart) $user $display]
	# puts stderr "fname=$fname"
	if {[string compare $srvaddr ""] == 0} {
		set srvaddr [get_address $fname 0]
	}
	set sock [try_connect $srvaddr 0]
	if {[string compare $sock 0] != 0} {
		main_loop $sock $waitRemoteClose $argstring $eofstring
	} else {
		start  $G(srvprog)
		set i 0
		set maxtries 10
		while {$i < $maxtries} {
			if {$i == ($maxtries - 1)} {
				set sock [try_connect [get_address $fname 1] 1]
			} else {
				set sock [try_connect [get_address $fname 0] 0]
			}
			if {[string compare $sock 0] != 0} {
				main_loop $sock $waitRemoteClose $argstring $eofstring
			}
			after 1000
			incr i
		}
	}
}

main
