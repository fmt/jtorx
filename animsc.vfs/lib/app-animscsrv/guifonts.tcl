package provide app-animscsrv 1.0

proc wcHookFontSetup {} {
	global font
	set font(value,normal,font)   [option get . normalFont       NormalFont]
	set font(value,normal,small)  [option get . normalSmallFont  NormalSmallFont]
	set font(value,normal,big)    [option get . normalBigFont    NormalBigFont]
	set font(value,demo,font)     [option get . demoFont         DemoFont]
	set font(value,demo,small)    [option get . demoSmallFont    DemoSmallFont]
	set font(value,demo,big)      [option get . demoBigFont      DemoBigFont]

	# remember the font the user may have set in xrdb or .Xdefaults
	set font(value,default,font)  [option get . font             Font]
	set font(value,default,small) [option get . smallFont        SmallFont]
	set font(value,default,big)   [option get . bigFont          BigFont]

	# our font names:
	set font(name,font)           torxFont
	set font(name,small)          torxSmallFont
	set font(name,big)            torxBigFont

	# create the fonts:
	font create $font(name,font)
	font create $font(name,small)
	font create $font(name,big)
	
	# override the font (name) given by the user by our own name
	# such that all widgets take our font as default
	# (unless overridden in our source code, e.g. for msc)
	option add *font $font(name,font) 100
	option add *smallFont $font(name,small) 100
	option add *bigFont $font(name,big) 100
	
	# if the user had set fonts, use it as default
	foreach fn {font small big} {
		if {[string compare $font(value,default,$fn) ""] != 0} {
			set font(value,normal,$fn) $font(value,default,$fn)
		}
	}
	
	fontNormal
}
proc fontNormal {} {
	global font
	Debug_Msg fontChange "fontNormal $font(name,font) $font(name,small) $font(name,big)"
	eval [concat font configure $font(name,font)  [font actual $font(value,normal,font)]]
	eval [concat font configure $font(name,small) [font actual $font(value,normal,small)]]
	eval [concat font configure $font(name,big)   [font actual $font(value,normal,big)]]
}
proc fontDemo {} {
	global font
	Debug_Msg fontChange "fontDemo $font(name,font) $font(name,small) $font(name,big)"
	eval [concat font configure $font(name,font)  [font actual $font(value,demo,font)]]
	eval [concat font configure $font(name,small) [font actual $font(value,demo,small)]]
	eval [concat font configure $font(name,big)   [font actual $font(value,demo,big)]]
}

