package provide app-animscsrv 1.0
#! /bin/sh
# the next line restarts using wish \
exec @WISH@ "$0" -- ${1+"$@"}

set topdir $starkit::topdir
catch {puts stderr "topdir: $topdir"}
set default(scriptdir) [file join $topdir lib app-animscsrv]

package require Tk

package require windowlist
package require mactoolbar


wm withdraw .
windowlist::omitDefaultWindow

if {[string compare [tk windowingsystem] aqua] == 0} {
	if {[catch {package require tkdock} msg ]} {
		catch {puts stderr "anidot: cannot load tkdock: $msg"}
	} else {
		if {[catch {open [file join $topdir lib dock-img.icns] r} fp]} {
			catch {puts stderr "anidot: cannot open tkdock icon file: $fp"}
		} elseif {[catch {
				fconfigure $fp -translation binary
				set data [read $fp]
				close $fp
      			} msg]} {
				catch {puts stderr "anidot: cannot read tkdock icon: $msg"}
		} elseif {[catch {
				::tkdock::switchIcon -data $data
			} msg]} {
				catch {puts stderr "anidot: cannot read or set tkdock icon: $msg"}
		}
	}
}

proc Debug_Msg {name s} {
}

set addr 127.0.0.1 ;# loopback
# set addr 0.0.0.0 ;# any (all) address

namespace import msc::msc_start msc::msc_end msc::msc_init msc::msc_destroy msc::msc_append 
	
set debug 0
set G(prog) animsc
set G(srvprog) animscsrv
set G(sprog) animscsrv
set G(fnamepart)  animsc
set PID [pid]

# define macros
set WS "\[ \t\r\n]"  ;# def white space
set INST "\[^,;: \t\r\n]+" ;# instance identifier
# set LABEL "\[^\"; \t\r\n]+|\"\[^;\t\r\n]+\"" ;# label identifier
set LABEL "\[^\"; \t\r\n]+|\"\[^\"\r\n]+\"" ;# label identifier



proc main {addr} {
	global default
	global idcnt
	global argv
	global G
	global surviveWindowDestroy
	
	set private 0

	if {[string compare [lindex $argv 0] -private] == 0} {
		set private 1
		set argv [lrange $argv 1 end]
	}
	if {[string compare [lindex $argv 0] -surviveWindowDestroy] == 0} {
		set surviveWindowDestroy 1
		fileevent stdin readable [list suicide_on_eof stdin]
		set argv [lrange $argv 1 end]
	}
	if {[llength $argv] > 0} {
		set s [join $argv " "]
		warning stderr "ignoring arguments: $s"
		warning stderr "usage: $G(prog)"
	}
	
	set resourceFile [file join $default(scriptdir) MscViewerCfg]
	if {[file readable $resourceFile]} {
		if {[catch {option read $resourceFile startup} errMsg]} {
			msg stderr "error reading resource file $resourceFile $errMsg"
		}
	}
	wcHookFontSetup

	msc_init \
		"cleanup; exit" \
		filecleanup \
	    [option get . smallFont SmallFont] \
	    [option get . bigFont BigFont] \
	    fontInc \
	    fontDec \
	    get_msc_text \
	    msgfrommsc

	set idcnt 0
	set surviveWindowDestroy 0

	set host [info hostname]
	set display [find_display $host]
	set user [find_user]
	set tmp [find_tmp]
	set fname [mk_filename $tmp $G(fnamepart) $user $display]

	if {![setup_B $addr 0 $fname $host $private ]} {
		exit 1
	}
	
	# continue_reading_after_process_args $argv stdin stderr
	
}

proc suicide_on_eof {fid} {
	if {[eof $fid]} {
		cleanup
		exit
	} else {
		catch {read $fid}
		fileevent stdin readable [list suicide_on_eof $fid]
	}
}

proc fontInc {{inc 1}} {
	global font
	Debug_Msg fontChange "fontNormal $font(name,font) $font(name,small) $font(name,big)"
	foreach n [list $font(name,small) $font(name,big)] {
		set factor 1
		# puts stderr "$n old: [font actual $n]"
		while {$factor < 10} {
			set osz [font actual $n -size]
			set wsz [expr $osz + ($inc * $factor)]
			font configure $n -size $wsz
			set nsz [font actual $n -size]
			# puts stderr "osz $osz wsz $wsz nsz $nsz"
			if {$osz != $nsz} {
				break
			}
			incr factor
		}
		# puts stderr "$n new: [font actual $n]"
	}
}
proc fontDec {} {
	fontInc -1
}

proc get_msc_text {id} {
	# TODO do somehting with id
	upvar #0 MS_[set id](msc_lines) msc_lines

	return [join $msc_lines "\n"]
}

proc continue_reading_after_process_args {argv ifid ofid {name stdin} } {
	
	set reuse 0
	set files {}
	set result 1
	set mcast ""
	
	set args $argv
	
	while {[llength $args] > 0} {
		set a [lindex $args 0]
		set args [lrange $args 1 end]
		switch -- $a {
		    -r {
				set reuse 1
		  } -m {
				if {[llength $args] <= 0} {
		  			warning $ofid "missing argument for flag: $a"
				} else {
					set mcast [lindex $args 0]
					set args [lrange $args 1 end]
				}
		  } -* {
		  		warning $ofid "unknown flag: $a"
		  } default {
		  		lappend files $a
		  }
		}
	}

	set cfid [connect_mcast $mcast $ofid]
	
	if {($ifid != {}) && ([llength $files] == 0)} {
		process_fid $reuse $ifid $ofid $cfid $name
		set result 0
	} else {
		foreach n $files {
			process_fname $n $reuse $ofid $cfid
		}
	}
	return $result
}

proc connect_mcast { mcast ofid } {
	set cfid {}
	set fields [split $mcast "!"]
	if {([llength $fields] == 3) &&
		([string compare [lindex $fields 0] tcp] == 0)} {
		set host [lindex $fields 1]
		set port [lindex $fields 2]
		if {[catch {socket $host $port} foo]} {
			msg $ofid "warning: cannot connect to mcast $mcast: $foo"
		} else {
			set cfid $foo
		}
	}
	return $cfid
}

proc process_fid { reuse fid ofid cfid {name {}} } {
	global idcnt
	global B
	
	incr idcnt
	setup_Msc $idcnt $reuse $fid $ofid $cfid $name
}

proc process_fname { name reuse ofid cfid } {
	global idcnt
	
	if {[regexp "\\|" $name]} {
		msg $ofid "error opening file $name"
	} elseif {[catch {open $name r} fid]} {
		msg $ofid "error opening file $name: $fid"
	} else {
		process_fid $reuse $fid $ofid $cfid $name
	}
}

proc setup_B {defAddr defPort fname host private} {
	global B

	set B(interface,name) $fname
	set B(pidsfile,name) "$fname.pid"
	set B(statusfile,name) "$fname.stat"
	set B(interface,cleanup) 0
	set B(pidsfile,cleanup) 0
	set pid [pid]

	if {! $private} {
		if {[string compare [try_connect [get_address $B(interface,name) 0] 0] 0] != 0} {
			warning stderr "not replacing running server (aborting)"
			exit 0
		} else {
			# warning stderr "could not connect to running server"
		}
		if {[catch {open $B(statusfile,name) a} fid]} {
			warning stderr "cannot open (append) $B(statusfile,name)"
			exit 1
		} else {
			puts $fid "starting $host $pid"
			close $fid
		}
		if {[catch {open $B(statusfile,name) r} fid]} {
			warning stderr "cannot open (read) $B(statusfile,name)"
			exit 1
		} elseif {[gets $fid line] < 0} {
			warning stderr "cannot read $B(statusfile,name)"
			exit 1
		} else {
			close $fid
			set words [split $line]
			set stat [lindex $words 0]
			set h [lindex $words 1]
			set p [lindex $words 2]
			if {[string compare $stat starting] == 0 &&
			    [string compare $host $h] == 0 &&
			    [string compare $pid $p] != 0} {
				if {[findprocess $pid $p]} {
					set i 0
					set maxtries 10
					while {$i < $maxtries} {
						if {$i == ($maxtries - 1)} {
							set sock [try_connect [get_address $B(interface,name) 1] 1]
						} else {
							set sock [try_connect [get_address $B(interface,name) 1] 0]
						}
						if {[string compare $sock 0] != 0} {
							warn stderr "attempt $i: not replacing running server (aborting)"
							exit 0
						} elseif {$i == ($maxtries - 1)} {
							warn stderr "attempt $i: could not connect to running server"
						}
						incr i
					}
				} else {
					msg stderr "did not find running server pid: $p"
				}
			}
		}
		if {[string compare [try_connect [get_address $B(interface,name) 0] 0] 0] != 0} {
			warning stderr "not replacing running server (aborting)"
			exit 0
		} else {
			# warning stderr "could not connect to running server"
		}
		if [catch {open $B(interface,name) w} fid] {
			warning stderr "cannot create $B(interface,name): $fid"
			return 0
		}
		set B(interface,cleanup) 1
	}

	# msg stderr "trying to listen on tcp!$defAddr!$defPort"
	if {$defAddr == {}} {
		set c [socket -server acceptIt $defPort]
	} else {
		set c [socket -server acceptIt -myaddr $defAddr $defPort]
	}
	set cfg [fconfigure $c -sockname]
	set ipNr [lindex $cfg 0]
	set hostName [lindex $cfg 1]
	set portNr [lindex $cfg 2]
	set B(listener) "tcp!$ipNr!$portNr"
	set B(host) [info hostname]
	msg stderr "listening on tcp!$ipNr!$portNr"
	
	if {! $private} {
		puts $fid "$ipNr $hostName $portNr"
		close $fid
	}
	catch {puts stdout "$ipNr $hostName $portNr"} ;# should commit suicide?

	if {! $private} {
		if {![catch {open "$B(pidsfile,name)" r} pfid]} {
			while {[gets $pfid line] >= 0} {
				set p [string trim $line]
				kill $p
			}
			catch {close $pfid}
		}
		catch {file delete -force $B(pidsfile,name)}
		if {[catch {open "$B(pidsfile,name)" w} pfid]} {
			warning stderr "cannot open $B(pidsfile,name) for writing: $pfid"
		} else {
			set B(pidsfile,cleanup) 1
			catch {puts $pfid $pid}
			catch {close $pfid}
		}
		catch {file delete -force $B(statusfile,name)}
	}

	return 1
}

proc acceptIt {socket remoteHost remotePort} {
	global B
	
    set nSock      $socket
 	set B(busy,$socket) 0
	fconfigure $socket -buffering line -blocking 1
	fileevent $socket readable [list BReader $socket]
	msg stderr "Connection accepted from client tcp!$remoteHost!$remotePort"
}  


proc setup_Msc {id rse ifid ofid mcastfid {name ""} } {
	global idinfo
	global B
	
	upvar #0 MS_[set id](linenr) linenr
	upvar #0 MS_[set id](eomsc_seen) eomsc_seen
	upvar #0 MS_[set id](msc_stepnr) msc_stepnr
	upvar #0 MS_[set id](msc_lines) msc_lines
	upvar #0 S_[set id](busy) busy
	upvar #0 S_[set id](destroying) destroying
	upvar #0 MS_[set id](reuse) reuse
	upvar #0 S_[set id](cfid) cfid
	
	set linenr	0
	set eomsc_seen 0
	set msc_stepnr	1
	set msc_lines	{}
	set reuse $rse
	set cfid $mcastfid
	
	# we use busy and destroying to synchronise with the destroy
	# to avoid trying to write to a window that is just destroyed
	set busy 0
	set destroying 0

	set idinfo($id,ifid) $ifid
	set idinfo($id,ofid) $ofid
	set idinfo($id,name) $name
	set idinfo($id,started) 0
	set idinfo($id,closed) 0
	
	if {[info exists B(busy,$ofid)]} {
		incr B(busy,$ofid)
	}
	
	if {[string compare $ifid stdin ] != 0} {
		fconfigure $ifid -buffering line -blocking 0
	}
	fileevent $ifid readable [list FileReader $id $ifid]
	dbgmsg "setup_Msc: started FileReader $id $ifid"

	if {[string compare $cfid ""] != 0} {
		fileevent $cfid readable [list ControlReader $id $cfid]
	}
}

proc ControlReader { id fid } {
	fileevent $fid readable {}
	upvar #0 S_[set id](cfid) cfid
	
	# update; update idletasks
	
	if {[eof $fid]} {
		dbgmsg "ControlReader end of file reached"
		if {![catch {fconfigure $fid -peername} cfg]} {
			set remoteHost [lindex $cfg 0]
			set remotePort [lindex $cfg 2]
			msg stderr "Connection closed by client tcp!$remoteHost!$remotePort"
		}
		close $fid
		set cfid ""
		return
	}
	if {[gets $fid line] < 0} {
		# blocked anyway because no full line available
		dbgmsg "ControlReader blocked"
		fileevent $fid readable [list ControlReader $id $fid]
	} else {
		msc::boxit $id $line 0
		fileevent $fid readable [list ControlReader $id $fid]
	}
}

proc BReader { fid } {
	global B
	fileevent $fid readable {}
	
	# update; update idletasks
	
	if {[eof $fid]} {
		dbgmsg "BReader end of file reached"
		if {![catch {fconfigure $fid -peername} cfg]} {
			set remoteHost [lindex $cfg 0]
			set remotePort [lindex $cfg 2]
			msg stderr "Connection closed by client tcp!$remoteHost!$remotePort"
		}
		close $fid
		unset B(busy,$fid)
		return
	}
	if {[gets $fid line] < 0} {
		# blocked anyway because no full line available
		dbgmsg "BReader blocked"
		fileevent $fid readable [list BReader $fid]
	} else {
		if {[continue_reading_after_process_B_line $line $fid]} {
			fileevent $fid readable [list BReader $fid]
		}
	}
}

proc continue_reading_after_process_B_line { line fid } {
	global B
	set name [string trim $line]
	set result 1
	if {([string compare $name -exit] == 0) ||
	    (([string compare [lindex $name 0] -args] == 0) &&
	     ([string compare [lindex $name 1] -exit] == 0))} {
		cleanup
		exit 0
	} elseif {[string compare [lindex $name 0] -eof] == 0} {
		dbgmsg "B eof"
		trace variable B(busy,$fid) w "close_B_when_ready $fid"
		return 0
	} elseif {[string compare [lindex $name 0] -debug] == 0} {
		dbgmsg "B debug: [lrange $name 1 end]"
	} elseif {[string compare [lindex $name 0] -args] == 0} {
		dbgmsg "B debug: [lrange $name 1 end]"
		set result [continue_reading_after_process_args [lrange $name 1 end] $fid $fid]
	} else {
		dbgmsg "B debug: unknown line: $line"
	}
	return $result
}

proc close_B_when_ready {fid varName index op} {
	global B
	dbgmsg "close_B_when_ready ($fid)($varName)($index)($op)"
	switch $op {
		w {
			if {[string compare "busy,$fid" $index] == 0} {
				if {$B(busy,$fid) == 0} {
					dbgmsg "close_B_when_ready: closing fid (B(busy,$fid) == 0)"
					if {![catch {fconfigure $fid -peername} cfg]} {
						set remoteHost [lindex $cfg 0]
						set remotePort [lindex $cfg 2]
						msg stderr "Closing connection to client tcp!$remoteHost!$remotePort"
					}
					close $fid
					unset B(busy,$fid)
				} else {
					dbgmsg "close_B_when_ready: not yet closing fid (B(busy,$fid) == $B(busy,$fid))"
				}
			} else {
					dbgmsg "close_B_when_ready: unrelated update (index=$index)"
				
			}
		}
	}
}

proc FileReader { id fid } {
	global idinfo
	upvar #0 S_[set id](busy) busy
	upvar #0 S_[set id](destroying) destroying

	fileevent $fid readable {}
	set busy 1
	
	# update; update idletasks
	
	# now, we will set busy to 0 before returning and
	# before resetting the fileevent handler, to create
	# a ``time window'' in which the destroy_via_busy
	# can do its work
	if {$destroying} {
		# puts stderr "FileReader $id destroying window, flushing $fid"
		close $fid
		if {[info exists idinfo($id,ofid)] &&
		    [info exists B(busy,$idinfo($id,ofid))]} {
			incr B(busy,$idinfo($id,ofid)) -1
		}
		set busy 0
		return
	}
	
	if {[eof $fid]} {
		dbgmsg "FileReader $id end of file reached"
		if {![catch {fconfigure $fid -peername} cfg]} {
			set remoteHost [lindex $cfg 0]
			set remotePort [lindex $cfg 2]
			msg stderr "Connection closed by client tcp!$remoteHost!$remotePort"
		}
		close $fid
		do_process_eof $id
		set busy 0
	} elseif {[gets $fid line] < 0} {
		# blocked anyway because no full line available
		dbgmsg "FileReader $id blocked"
		set busy 0
		fileevent $fid readable [list FileReader $id $fid]
	} else {
		# dbgmsg "FileReader $id line: $line"
		if {[do_process_line $id $line]} {
			dbgmsg  "FileReader $id restart for $fid"
			set busy 0
			fileevent $fid readable [list FileReader $id $fid]
		} else {
			dbgmsg "FileReader $id not restarted for $fid"
			set busy 0
		}
	}
}

proc do_process_eof {id} {
	global idinfo
	global B
	upvar #0 MS_[set id](eomsc_seen) eomsc_seen

	if {$idinfo($id,started) && (!$eomsc_seen)} {
		msc_end $id 1
	}
	if {[info exists idinfo($id,ofid)] &&
	     [info exists B(busy,$idinfo($id,ofid))]} {
		incr B(busy,$idinfo($id,ofid)) -1
	}
}

proc do_process_line { id line } {
	global WS INST LABEL
	global idinfo
	
	upvar #0 MS_[set id](linenr) linenr
	upvar #0 MS_[set id](eomsc_seen) eomsc_seen
	upvar #0 MS_[set id](msc_stepnr) msc_stepnr
	upvar #0 MS_[set id](msc_lines) msc_lines
	upvar #0 MS_[set id](reuse) reuse
	upvar #0 S_[set id](cfid) cfid

	foreach vName {msc_instances msc_process_titles} {
		upvar #0 [set vName]_[set id] $vName
	}

	incr linenr
	set line [string trim $line]

	lappend msc_lines $line
	if {[regexp "^msc$WS+($LABEL)$WS*;\$" $line match title ]} {
		catch {unset msc_instances}
		catch {unset msc_process_titles}
		set msc_stepnr	1
		set idinfo($id,started) 1
		set title [strip_quotes $title]
		set tail_root [file rootname [file tail $title]]
		msc_start $id $reuse $cfid $title $tail_root
	} elseif {[regexp "^inst$WS+(($INST)((($WS*,$WS*)|($WS+))$INST)*)$WS*;\$" $line match names ]} {
		regsub -all {,} $names "" names
		foreach n [split $names] {
			set msc_instances($n) 1
		}
	} elseif {[regexp "^(\[^:]+):$WS+(.*);\$" $line match inst remains ]} {
		if {![info exists msc_instances($inst)]} {
			warning $idinfo($id,ofid) "msc $id: $idinfo($id,name):$linenr: skipping line: undefined msc instance name: $inst"
		} elseif {[regexp "^instancehead$WS+(process$WS+)?($LABEL)$WS*\$" $remains match proc_opt label ]} {
			set msc_process_titles($inst) [strip_quotes $label ]
		} elseif {[regexp "^in$WS+(.*)$WS+from$WS+($INST)$WS*\$" $remains match label from ]} {
			# "^in$WS+($LABEL)$WS+from$WS+($INST)$WS*\$" $remains match label from 
			if {![info exists msc_instances($from)]} {
				msg $idinfo($id,ofid) "msc $id: $idinfo($id,name):$linenr: skipping line: undefined source (from) msc instance name: $from"
			} else {
				set msc_label_in [strip_label_quotes $label ]
				set msc_remote_instance_label $from
				msc_append $id "msc $id: $idinfo($id,name):$linenr" $msc_stepnr $msc_process_titles($inst) 2 $msc_label_in
				incr msc_stepnr
			}
		} elseif {[regexp "^out$WS+(.*)$WS+to$WS+($INST)$WS*\$" $remains match label to ]} {
			if {![info exists msc_instances($to)]} {
				msg $idinfo($id,ofid) "msc $id: $idinfo($id,name):$linenr: skipping line: undefined target (to) msc instance name: $to"
			} else {
				set msc_label_out [strip_label_quotes $label ]
				set msc_remote_instance_label $to
				msc_append $id "msc $id: $idinfo($id,name):$linenr" $msc_stepnr $msc_process_titles($inst) 1 $msc_label_out
				incr msc_stepnr
			}
		} elseif {[regexp "^endinstance$WS*\$" $remains match ]} {
			# skip
		} else {
			filefatal $id "msc $id: $idinfo($id,name):$linenr: syntax error in: $remains"
		}
	} elseif {[regexp "^endmsc$WS*;\$" $line match ]} {
		if {$idinfo($id,started)} {
			msc_end $id
		}
		set eomsc_seen 1
	} else {
		filefatal $id "msc $id: $idinfo($id,name):$linenr: syntax error in: $line"
		return 0
	}
	return 1
}

proc strip_quotes { s } {
	if {[regexp {^"([^"]*)"$} $s match qs ]} {
		return $qs
	} else {
		return $s
	}
}

proc strip_label_quotes { s } {
	# puts stderr "strip_label_quotes: ($s)"
	if {[regexp {^"(.*)"[ \t]*$} $s match qs ]} {
		return [strip_label_escapes $qs]
	} else {
		return $s
	}
}
proc strip_label_escapes { s } {
	# puts stderr "strip_label_escapes: ($s)"
	set i 0
	set l [string length $s]
	set escaping 0
	set res ""
	while {$i < $l} {
		set c [string index $s $i]
		if {$escaping} {
			# puts stderr "escaping: ($c)"
			append res $c
			set escaping 0
		} else {
			# puts stderr "not escaping: ($c)"
			if {[string compare $c "\\"] == 0} {
				set escaping 1
			} else {
				append res $c
			}
		}
		incr i
	}
	return $res
}

proc warning { fid s } {
	msg $fid "warning: $s"
}

proc filefatal { id s } {
	upvar #0 S_[set id](cfid) cfid
	global idinfo
	global B
	
	msg $idinfo($id,ofid) "fatal: $s"
	if {$idinfo($id,started)} {
		msc_end $id 1
	}
	close $idinfo($id,ifid)
	if {[info exists B(busy,$idinfo($id,ofid))]} {
		incr B(busy,$idinfo($id,ofid)) -1
	}
	if {[info exists cfid] && ([string compare $cfid ""] != 0)} {
		fileevent $cfid readable {}
		close $cfid
		set cfid ""
	}
}

proc fatal { s } {
	msg $fid "fatal: $s"
	cleanup
	exit 1
}

proc cleanup {} {
	global B
	
	if {([string compare $B(interface,name) ""] != 0) &&
	    $B(interface,cleanup) &&
	    [file readable $B(interface,name)]} {
		catch {file delete -force $B(interface,name)}
	}
	if {([string compare $B(pidsfile,name) ""] != 0) &&
	    $B(pidsfile,cleanup)} {
		catch {file delete -force $B(pidsfile,name)}
	}
}

proc filecleanup { id } {
	global idinfo
	global idcnt
	global surviveWindowDestroy
	upvar #0 MS_[set id](linenr) linenr
	upvar #0 MS_[set id](eomsc_seen) eomsc_seen
	upvar #0 MS_[set id](msc_stepnr) msc_stepnr
	upvar #0 MS_[set id](msc_lines) msc_lines
	upvar #0 MS_[set id](reuse) reuse
	upvar #0 S_[set id](cfid) cfid

	# puts stderr "filecleanup $id"
	
	if {[info exists cfid] && ([string compare $cfid ""] != 0)} {
		fileevent $cfid readable {}
		close $cfid
		set cfid ""
	}

	foreach v {linenr eomsc_seen msc_stepnr msc_lines reuse cfid} {
		catch {unset $v}
	}
	set idinfo($id,closed) 1
	
	set all_closed 1
	for {set i 1} \
	    {$i <= $idcnt} \
	    {incr i} {
		if {$idinfo($i,started) && !$idinfo($i,closed)} {
			# at least one window open
			# puts stderr "still open: $i"
			set all_closed 0
			break
		} else {
			# puts stderr "closed or not started: $i"
		}
	}
	if {$all_closed && !$surviveWindowDestroy} {
		cleanup
		exit
	}
}

proc dbgmsg { s } {
	global debug
	if {$debug} {
		msg stderr "DEBUG: showmsc: $s"
	}
}

proc msgfrommsc { id s } {
	global idinfo
	
	if {$id != {}} {
		msg $idinfo($id,ofid) $s
	} else {
		msg stderr $s
	}
}

main $addr
