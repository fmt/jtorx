package provide app-animscsrv 1.0

namespace eval msc {

	# Make following procedures visible to namespace import
	namespace export msc_start msc_end msc_init msc_destroy msc_append 
	
	# For msc-dependent state information that is not stored in arrays
	# we use global array variables of the form:
	#      S_<id>
	# where <id> is a particular id, and we index those with the name
	# of the variable.
	# This should make it easier to remove such variables.
	# Within the code, we as much as possible use upvar to map a global variable
	# ``S_<id>(<varname>)'' to just ``<varname>'', to make the code more readable.
	# Only when we refer to widget's (text)variable's we have to use the long
	# global name.
	#
	# For msc-dependent state information that is stored in arrays, we use
	# global variables of the form:
	#      <varname>_<id>
	# where <varname> is the variable name, and <id> is a particular id.
	# Within the code, we as much as possible use upvar to map a global variable
	# ``<varname>_<id>'' to just ``<varname>'', to make the code more readable.
	#
	# We only use global declarations for global variables that contain
	# configuration information that is shared for all msc's.
	
		proc ldelete {l e} {
			 set ix [lsearch -exact $l $e]
			 if {$ix >= 0} {
			 	return [lreplace $l $ix $ix]
			 } else {
			 	return $l
			 }
		}
		
		proc isinteger { s } {
			# ideal implementation: return [string is integer -strict $s]
			# but that does not work in older wish versions
			if {[regexp "^\[-+\]?\[0-9\]\[0-9\]*$" $s]} {
				return 1
			} else {
				return 0
			}
		}
	
		proc update_prefs {id kind {w ""}} {
			upvar #0 S_[set id](doHighlight) doHighlight
			upvar #0 S_[set id](selStep) selStep
			upvar #0 S_[set id](doReuse) doReuse
			# upvar #0 S_[set id](decfontproc) decfontproc
			# upvar #0 S_[set id](incfontproc) incfontproc
			global decfontproc
			global incfontproc
			global msgproc
			global reuseQ
			
			# puts stderr "update_prefs $id $kind $w"
			
			switch -- $kind {
			  dfonts {
				boxit $id
				catch {eval $decfontproc}
				update idletasks
				move_canvas_items $id
				boxit $id $selStep 0
			 } ifonts {
				boxit $id
				catch {eval $incfontproc}
				update idletasks
				move_canvas_items $id
				boxit $id $selStep 0
			 } highlight {
			 	if {$doHighlight} {
					boxit $id $selStep 0
			 		foreach i [list l t bsp bsm ] {
			 			$w.$i configure -state normal
			 		}
			 	} else {
			 		boxit $id
			 		foreach i [list l t bsp bsm ] {
			 			$w.$i configure -state disabled
			 		}
			 	}
			 } reuse {
			 	if {$doReuse} {
			 		lappend reuseQ [list $w $id]
			 	} else {
			 		set reuseQ [ldelete $reuseQ [list $w $id]]
			 	}
			 } default {
			 	$msgproc $id "internal error: update_prefs: unknown kind: $kind"
			 }
			}
		}
		
		proc msc_save_as {id name} {
			upvar #0 S_[set id](savefile) savefile

			set filetypes {
				{{Postscript Files}       {.ps}   }
				{{Textual MSC Files}      {.msc}  }
				{{All Files}        *             }
			}
			
			set tail [file tail    $savefile]
			set dir  [file dirname $savefile]
			# puts stderr "msc_save_as t($tail)   d($dir)"

			set fname \
				[tk_getSaveFile \
					-filetypes $filetypes  \
					-defaultextension .ps \
					-initialdir $dir \
					-initialfile $tail \
					-title "Save MSC as postscript (.ps) or as text (.msc) as" \
				]
			if {[string compare $fname ""] != 0} {
				set savefile $fname
				# puts stderr "about to save as $fname"
				switch -re -- [file extension $fname] {
				   ^\.ps$ -
				   ^\.eps$ {
				 	msc_save $fname [get_postscript $id $name]
				 } ^\.msc$ -
				  default {
				 	msc_save $fname [get_text $id]
				}}
			}
		}
		
		proc get_postscript {id name} {
			global Ptype Pargs
			upvar #0 S_[set id](dbox2) dbox2
			
			return [canvas2postscript .f$dbox2.c $name -colormode $Ptype $Pargs]
		}
		proc get_text {id} {
			# upvar #0 S_[set id](gettextproc) gettextproc
			global gettextproc
			
			return [$gettextproc $id]
		}

		proc msc_save {fname data} {
			if {[catch {open $fname w} fid]} {
				tk_messageBox -message "Could not open $fname for writing: $fid"
			} else {
				puts $fid $data
				close $fid
			}
		}
		
		proc xview {canvas top c args} {
			foreach w [list $canvas $top] {
				eval "$w $c $args"
			}
		}

		proc yview {canvas top c args} {
			global msgproc
			
			# puts stderr "yview $canvas $top $c $args"
			if {$canvas != {}} {
				# puts stderr "	eval \"$canvas $c $args\""
				eval "$canvas $c $args"
			}
#			if {[string compare [lindex $args 0] scroll] == 0} {
				if {$canvas != {}} {
					set args [list moveto [lindex [$canvas yview] 0]]
				} else {
					# $msgproc {} "Warning: msc::yview: when canvas=={} don't know how to $args"
				}
#			}
			if {[string compare [lindex $args 0] moveto] == 0} {
				set movfrac [lindex $args 1]
				set regionconf [$top configure -scrollregion]
				set region [lindex [$top configure -scrollregion] 4]
				# puts stderr "	region $region"
				if {$canvas != {}} {
					set cregionconf [$canvas configure -scrollregion]
					set cregion [lindex [$canvas configure -scrollregion] 4]
					# puts stderr "	cregion $cregion"
				}
				set regiontly [lindex $region 1]
				set regionbry [lindex $region 3]
				set regionh [expr $regionbry - $regiontly]
				set headbbox [$top bbox header]
				set headtly [lindex $headbbox 1]
				set maxoffscreen [expr $headtly - $regiontly]
				set maxheadfrac [expr $maxoffscreen / $regionh.0]
				# puts stderr "	region $region headbbox $headbbox maxoffscreen $maxoffscreen movfrac $movfrac maxheadfrac $maxheadfrac"
				if {$movfrac > $maxheadfrac} {
					# puts stderr "	>  eval \"$top $c moveto $maxheadfrac\""
					eval "$top $c moveto $maxheadfrac"
				} else {
					# puts stderr "	<= eval \"$top $c $args\""
					eval "$top $c $args"
				}
			} else {
				$msgproc {} "Warning: msc::yview: don't know how to $args"
			}
		}

		proc reusecanvas {nr id reuse n m sn geox geoy placed} {
			global cnr tk_version
#			global Sticky
			global FG BG Ptype Pargs
			global TOPH

			upvar #0 S_[set id](savefile) savefile
			upvar #0 S_[set id](doHighlight) doHighlight
			upvar #0 S_[set id](doReuse) doReuse
			upvar #0 S_[set id](selStep) selStep
			upvar #0 S_[set id](dbox2) dbox2
					
			set dbox2 $nr
			# puts stderr "reusecanvas $id  .f$nr ($nr)($id)($reuse)($n)($m)($sn)($geox)($geoy)($placed)"
			wm title .f$nr "$n"
			wm iconname .f$nr $m
			# if {$placed} { wm geometry .f$nr +$geox+$geoy }
			wm protocol .f$nr WM_DELETE_WINDOW "msc_destroy $id"
			wm protocol .f$nr WM_SAVE_YOURSELF "msc_destroy $id"
			wm deiconify .f$nr
			raise .f$nr
			
			.f$nr.c delete all
			.f$nr.ct delete all
			
			.f$nr.b1 configure -command "msc_destroy $id"

			set doReuse $reuse
			.f$nr.b6 configure \
				 -variable S_[set id](doReuse) \
				 -command "msc::update_prefs $id reuse $nr"
			msc::update_prefs $id reuse $nr

			set savefile $m.ps
			# puts stderr "setting savefile ($savefile)"
			.f$nr.b2 configure \
				-state disabled \
				-command "msc::msc_save_as $id $sn"

			.f$nr.bfp configure \
				-command "msc::update_prefs $id ifonts"
			.f$nr.bfm configure \
				-command "msc::update_prefs $id dfonts"

			set doHighlight 1
			.f$nr.b5 configure \
				 -variable S_[set id](doHighlight) \
				 -command "msc::update_prefs $id highlight .f$nr"
			.f$nr.b6 configure -state disabled

			set selStep ""
			.f$nr.t configure \
				 -textvariable S_[set id](selStep)
			bind .f$nr.t <Return> "msc::boxit $id \$S_[set id](selStep)"
			.f$nr.bsp configure \
				-command "msc::step_inc $id +1"
			.f$nr.bsm configure \
				-command "msc::step_inc $id -1"

			return $nr
		}
		
		proc setToolbarMenuItem {menu w} {
			if [winfo ismapped $w] {
				set label "Hide Toolbar"
			} else {
				set label "Show Toolbar"
			}
			$menu entryconfigure {*Toolbar} -label $label
		}
		proc toggleToolbar {menu w} {
			event generate  $w <<ToolbarButton>>
			setToolbarMenuItem $menu $w
		}

		proc mkcanvas {id reuse n m sn geox geoy placed} {
			global cnr tk_version
#			global Sticky
			global FG BG Ptype Pargs
			global TOPH
			global DX
			global DY
			global SCROLLBARWIDTH

			upvar #0 S_[set id](savefile) savefile
			upvar #0 S_[set id](doHighlight) doHighlight
			upvar #0 S_[set id](doReuse) doReuse
			upvar #0 S_[set id](selStep) selStep
			upvar #0 S_[set id](dbox2) dbox2
					
			incr cnr
			set dbox2 $cnr
			# puts stderr "mkcanvas $id  .f$cnr ($id)($reuse)($n)($m)($sn)($geox)($geoy)($placed)"
			set t [toplevel .f$cnr]
			wm title .f$cnr "$n"
			wm iconname .f$cnr $m
			if {$placed} { wm geometry .f$cnr +$geox+$geoy }
			wm minsize .f$cnr 100 100
			wm protocol .f$cnr WM_DELETE_WINDOW "msc_destroy $id"
			wm protocol .f$cnr WM_SAVE_YOURSELF "msc_destroy $id"
			
			set menubar [menu .m$cnr]
			puts stderr "mkcanvas menu nr $cnr m=$menubar"
			$t configure -menu $menubar
			if {[string compare [tk windowingsystem] aqua]==0} {
				set am [menu $menubar.apple]
				$menubar add cascade -label "AniMSC" -menu $am
				$am add command -label "About AniMSC" -command {}
			} else {
				set hm [menu $menubar.help]
				$menubar add cascade -label "Help" -menu $hm
				$hm add command -label "About AniMSC" -command {}
			}
			set vm [menu $menubar.view]
			$menubar add cascade -label "View" -menu $vm
			$vm add command -label "Hide Toolbar" -command [list msc::toggleToolbar $vm $t.tbf.toolbar]
			$vm configure -postcommand [list msc::setToolbarMenuItem $vm $t.tbf.toolbar]
			windowlist::windowMenu $menubar

			set i 0
			while {$i <= [$menubar index last]} {
			 	if {! [catch {$menubar entrycget $i -menu} subMenu]} {
				 	$subMenu configure -tearoff 0
				 }
				 incr i
			}
	
			frame $t.rest
			frame $t.tbf
			mactoolbar::toolbar $t.tbf.toolbar

			# make frames first, below everything else
			frame .f$cnr.f
			frame .f$cnr.fs -borderwidth 2 -relief ridge
			frame .f$cnr.fh -borderwidth 2 -relief ridge
			frame .f$cnr.ft -borderwidth 2 -relief ridge
			frame .f$cnr.fq -borderwidth 2 -relief ridge
			frame .f$cnr.fn -borderwidth 2 -relief ridge
			frame .f$cnr.bf
			frame .f$cnr.bs


			# we make TWO canvases:
			# one for the real work, and
			# one to contain just the instance headers.
			#
			# Both have the same scrollregion, but the
			# visible size of the ``instance headers'' (top)
			# one is $TOPH, which should be just enough to
			# contain the header boxes, plus a bit of dotted
			# blue line. These header boxes etc. are created
			# in the ``instance headers'' window at an offset
			# from the top of $TOPH, such that they are ordinarily
			# off-screen, but can be scrolled on-screen.
			# We scroll both canvases in a ``synchronized way'',
			# with one constraint: in the top (instance headers)
			# window we cannot scroll down below the instance headers.
			#
			# We set the scrollregion of both canvases such that
			# we have at the bottom always $BOTRSV space left,
			# which should be enough to draw the end-of-msc
			# stuff in.
			
			if {[string first "4." $tk_version] == 0 \
			||  [string first "8." $tk_version] == 0} {
				set cv [canvas .f$cnr.c  -relief raised\
					-scrollregion {-15c -5c 30c 40c} \
					-background $BG \
					-borderwidth 0 \
					-highlightthickness 0 \
					-height $DY \
					-width $DX \
					-xscrollcommand ".f$cnr.hscroll set" \
					-yscrollcommand ".f$cnr.vscroll set"]
				set cvt [canvas .f$cnr.ct  -relief raised\
					-scrollregion {-15c -5c 30c 40c} \
					-background $BG \
					-borderwidth 0 \
					-highlightthickness 0 \
					-height $TOPH \
					-width $DX \
					-xscrollcommand ".f$cnr.hscroll set"]
				$cv configure -height [expr [$cv cget -height] - $TOPH]
				scrollbar .f$cnr.vscroll -relief sunken \
					-width $SCROLLBARWIDTH \
					-command "msc::yview .f$cnr.c .f$cnr.ct yview"
				scrollbar .f$cnr.hscroll -relief sunken -orient horiz \
					-width $SCROLLBARWIDTH \
					-command "msc::xview .f$cnr.c .f$cnr.ct xview"
			} else {
				set cv [canvas .f$cnr.c  -relief raised\
					-scrollregion {-15c -5c 30c 40c} \
					-background $BG \
					-borderwidth 0 \
					-highlightthickness 0 \
					-height $DY \
					-width $DX \
					-xscroll ".f$cnr.hscroll set" \
					-yscroll ".f$cnr.vscroll set"]
				set cvt [canvas .f$cnr.ct  -relief raised\
					-scrollregion {-15c -5c 30c 40c} \
					-background $BG \
					-borderwidth 0 \
					-highlightthickness 0 \
					-height $TOPH \
					-width $DX \
					-xscroll ".f$cnr.hscroll set"]
				$cv configure -height [expr [$cv cget -height] - $TOPH]
				scrollbar .f$cnr.vscroll -relief sunken \
					-width $SCROLLBARWIDTH \
					-command "msc::yview .f$cnr.c .f$cnr.ct yview"
				scrollbar .f$cnr.hscroll -relief sunken -orient horiz \
					-width $SCROLLBARWIDTH \
					-command "msc::xview .f$cnr.c .f$cnr.ct xview"
			}
			
			button .f$cnr.b0 -text "Quit" \
				-command "msc::msc_quit $id"

			button .f$cnr.b1 -text "Close" \
				-command "msc_destroy $id"


			set doReuse $reuse
			checkbutton .f$cnr.b6 -text "Reuse" \
				 -variable S_[set id](doReuse) \
				 -command "msc::update_prefs $id reuse $cnr"
			msc::update_prefs $id reuse $cnr
	
			.f$cnr.b6 configure -state disabled

			set savefile $m.ps
			button .f$cnr.b2 -text "Save as" \
				-state disabled \
				-command "msc::msc_save_as $id $sn"

			label .f$cnr.fl -text "Font"
			button .f$cnr.bfp -image up \
				-command "msc::update_prefs $id ifonts"
			button .f$cnr.bfm -image down \
				-command "msc::update_prefs $id dfonts"

			set doHighlight 1
			checkbutton .f$cnr.b5 -text "Highlight" \
				 -variable S_[set id](doHighlight) \
				 -command "msc::update_prefs $id highlight .f$cnr"

			set selStep ""
			label .f$cnr.l -text "Step:"
			entry .f$cnr.t \
				 -width 5 \
				 -textvariable S_[set id](selStep)
			bind .f$cnr.t <Return> "msc::boxit $id \$S_[set id](selStep)"
			button .f$cnr.bsp -image up \
				-command "msc::step_inc $id +1"
			button .f$cnr.bsm -image down \
				-command "msc::step_inc $id -1"


			pack append .f$cnr.fs \
				.f$cnr.b2 {left } 

			pack append .f$cnr.bf \
				.f$cnr.bfp {top fillx filly expand } \
				.f$cnr.bfm {bottom fillx filly expand }

			pack append .f$cnr.ft \
				.f$cnr.bf {right fillx filly } \
				.f$cnr.fl {left  fillx filly }

			pack append .f$cnr.fq \
				.f$cnr.b0 {right } \
				.f$cnr.b1 {right } \
				.f$cnr.b6 {right fillx filly }

			pack append .f$cnr.bs \
				.f$cnr.bsp {top fillx filly expand } \
				.f$cnr.bsm {bottom fillx filly expand } \

			pack append .f$cnr.fn \
				.f$cnr.bs {right fillx filly } \
				.f$cnr.t {right fillx expand } \
				.f$cnr.l {left  fillx filly }

			pack append .f$cnr.fh \
				.f$cnr.b5 {left  fillx filly }

			pack append .f$cnr.f \
				.f$cnr.fq {right fillx filly } \
				.f$cnr.fn {right  fillx filly expand} \
				.f$cnr.fh {right  fillx filly } \
				.f$cnr.fs {left  fillx filly} \
				.f$cnr.ft {left  fillx filly }

			pack append [mactoolbar::getframe] $t.f {bottom fillx}

		
			pack append $t.rest \
				\
				.f$cnr.vscroll {right filly} \
				.f$cnr.hscroll {bottom fillx} \
				.f$cnr.ct {top fillx} \
				.f$cnr.c {top expand fill}
				
			pack $t.tbf -side top -fill x
			pack $t.rest -side bottom -expand 1 -fill both

		
#			if {$m == "msc"} {
#				set Sticky($cnr) 0
#				checkbutton .f$cnr.b6 -text "Preserve" \
#					-variable Sticky($cnr) \
#					-relief flat
#				pack append .f$cnr \
#					.f$cnr.b6 { right padx 5}
#			}
			
			# scan/drag code originally added to xspin by Geert Janssen, TUE
			bind $cv <2> "$cv scan mark %x %y"
			bind $cv <B2-Motion> "
				$cv scan dragto %x %y
				$cvt xview moveto \[lindex \[$cv xview] 0]
				msc::yview {} $cvt yview moveto \[lindex \[$cv yview] 0]
			"
			
			bind $cvt <2> "$cv scan mark %x %y"
			bind $cvt <B2-Motion> "
				$cv scan dragto %x %y
				$cvt xview moveto \[lindex \[$cv xview] 0]
				msc::yview {} $cvt yview moveto \[lindex \[$cv yview] 0]
			"
		
			.f$cnr.c bind node <B1-Motion> "
				moveNode $cnr \[.f$cnr.c find withtag current] %x %y 1
			"
		
		#	.f$cnr.c bind node <B2-Motion> "
		#		moveNode $cnr \[.f$cnr.c find withtag current] %x %y 1
		#	"
		
			# introduce a tmporary variable to avoid a race:
			# while we tkwait someone else may invoke mkcanvas as well
			wm deiconify .f$cnr
			raise .f$cnr
			tkwait visibility .f$cnr
			return $dbox2
		}
		
		proc move_canvas_items {id} {
			upvar #0 S_[set id](Remarks) Remarks
			upvar #0 S_[set id](RCoords) RCoords
			upvar #0 S_[set id](dbox2) dbox2

			set i 0
			foreach r $Remarks {
				# puts stderr "DEBUG: move_canvas_items $r : [.f$dbox2.c bbox $r]"
				set c [lindex $RCoords $i]
				eval .f$dbox2.c coords $r $c
				set bbox_x [lindex [.f$dbox2.c bbox $r] 0]
				if {$bbox_x < 0} {
					# puts stderr "DEBUG: move because: $bbox_x"
					.f$dbox2.c move $r [expr - $bbox_x] 0
				}
				incr i
			}
		}
		
		proc canvas2postscript {cv name args} {
			set bbox [$cv bbox all]
			set x [lindex $bbox 0]
			set y [lindex $bbox 1]
			set x1 [lindex $bbox 2]
			set y1 [lindex $bbox 3]
			set w [expr $x1 - $x]
			set h [expr $y1 - $y]
			# must be a better way to do this, but I don't know how
			set cmd [list $cv postscript -x $x -y $y -width $w -height $h ]
			foreach a $args {
				if {[string compare $a ""] != 0} {
					lappend cmd $a
				}
			}
			set ps [eval $cmd]
			regsub -line -- {^%%Title:.*$} $ps "%%Title: $name" ps
			return $ps
		}
		
		proc boxit { id {stepnr ""} {report 1} } {
			global ArrowWidth
			
			upvar #0 S_[set id](dbox2) dbox2
			upvar #0 S_[set id](selBox) selBox
			upvar #0 S_[set id](selArrow) selArrow
			upvar #0 S_[set id](doHighlight) doHighlight
			upvar #0 S_[set id](selStep) selStep
			upvar #0 S_[set id](Remarks) Remarks
			upvar #0 S_[set id](Arrows) Arrows
			upvar #0 S_[set id](cfid) cfid

			if {![info exists dbox2]} {
				return
			}
			set canvas .f$dbox2.c
			set canvastop .f$dbox2.ct
			
			# puts stderr "boxit $id stepnr $stepnr"
			if {[info exists selBox] && [string compare $selBox ""] != 0} {
				$canvas delete $selBox
			}
			if {[info exists selArrow] && [string compare $selArrow ""] != 0} {
				$canvas itemconfigure $selArrow -width $ArrowWidth
			}
			if {! $doHighlight} {
				return
			}
			set stepnr [string trim $stepnr]
			if {([string compare $stepnr ""] != 0) &&
			    ([isinteger $stepnr]) &&
			    ($stepnr >= 0) && ($stepnr <= [llength $Remarks])} {
				# stepnr has origin 1; Remarks has origin 0
				# set item [lindex $Remarks [expr $stepnr - 1]]
				# stepnr has origin 1; Remarks has now also origin 1 (fake 0 entry)

				if {$report && [info exists cfid] &&
					([string compare $cfid ""] != 0)} {
					puts $cfid $stepnr
					flush $cfid
				}
				
				if {[string compare $stepnr [string trim $selStep]] != 0} {
					set selStep $stepnr
				}
				set item [lindex $Remarks $stepnr]
				set aitem [lindex $Arrows $stepnr]
				if {$aitem != {}} {
					set selArrow $aitem
					$canvas itemconfigure $selArrow -width [expr $ArrowWidth + 1]
				}
				if {[string compare $item ""] == 0} {
					return
				}
				set bbox [$canvas bbox $item ]
				# puts stderr "stepnr $stepnr item $item type [$canvas type $item]"
				if {[string compare [$canvas type $item] text] == 0} {
					set selBox [eval $canvas create rectangle $bbox -outline orange -width 2]
					$canvas lower $selBox
				}
				# the remainder is just to scroll the canvas to make the box visible
				set bboxtlx [lindex $bbox 0]
				set bboxtly [lindex $bbox 1]
				set bboxbrx [lindex $bbox 2]
				set bboxbry [lindex $bbox 3]
				set bboxh [expr $bboxbry - $bboxtly]
				set bboxw [expr $bboxbrx - $bboxtlx]
				set offsety [$canvas canvasy 0]
				set offsetx [$canvas canvasx 0]
				set offsetx [expr $offsetx + 5] ;# margin
				set width [winfo width $canvas]
				set topboundary $offsety
				set botboundary [expr $offsety + [winfo height $canvas]]
				set leftboundary $offsetx
				set rightboundary [expr $offsetx + $width]
				# puts stderr "offset ox=lb $offsetx oy=tb $offsety rb $rightboundary bb $botboundary bbox $bbox bboxw $bboxw bboxh $bboxh"
				set dragx 0
				set dragy 0
				if {$bboxtly < $topboundary} {
					set newtopboundary [expr $bboxtly - $bboxh]
					set dragy [expr int($topboundary - $newtopboundary)]
				} elseif {$bboxbry > $botboundary} {
					set newbotboundary [expr $bboxbry + $bboxh ]
					set dragy [expr int($botboundary - $newbotboundary)]
				}
				set leftmiss [expr $bboxtlx < $leftboundary ]
				set rightmiss [expr $bboxbrx > $rightboundary ]
				if {$bboxw > $width} {
					# puts -nonewline stderr "bwt"
					set newleftboundary [lindex $bbox 0]
					set dragx [expr int($leftboundary - $newleftboundary)]
				} elseif {($bboxw <= $width) && ($leftmiss || $rightmiss)} {
					# puts -nonewline stderr "lor\t"
					set newleftboundary [expr $bboxtlx - (($width - $bboxw) / 2)]
					set dragx [expr int($leftboundary - $newleftboundary)]
					# set newrightboundary [expr $bboxbrx + $bboxw ]
					# set dragx [expr int($rightboundary - $newrightboundary)]
				} else {
				}
				if {($dragx != 0) || ($dragy != 0)} {
					$canvas scan mark 0 0
					$canvastop scan mark 0 0
					# puts stderr "canvas drag $dragx $dragy"
					# Older version of wish (wish8.1 and older?) do not allow
					# a third parameter to scan dragto , so there the following
					# scan  dragto command does not work.
					# $canvas scan dragto $dragx $dragy 1
					# The code below has the same effect, but uses scroll commands.
					set region [lindex [$canvas configure -scrollregion] 4]
					if {$dragy != 0} {
						set regiontly [lindex $region 1]
						set regionbry [lindex $region 3]
						set regionh [expr $regionbry - $regiontly]
						set curofffracy [lindex [$canvas yview] 0]
						set curoffy [expr $curofffracy * $regionh]
						set newoffy [expr $curoffy - $dragy]
						set newofffracy [expr $newoffy / $regionh.0]
						# puts stderr "canvas yview moveto $newofffracy"
						$canvas yview moveto $newofffracy
					}
					if {$dragx != 0} {
						set regiontlx [lindex $region 0]
						set regionbrx [lindex $region 2]
						set regionw [expr $regionbrx - $regiontlx]
						set curofffracx [lindex [$canvas xview] 0]
						set curoffx [expr $curofffracx * $regionw]
						set newoffx [expr $curoffx - $dragx]
						set newofffracx [expr $newoffx / $regionw.0]
						# puts stderr "canvas xview moveto $newofffracx"
						$canvas xview moveto $newofffracx
					}
					$canvastop xview moveto [lindex [$canvas xview] 0]
					msc::yview {} $canvastop yview moveto [lindex [$canvas yview] 0]
				}
			}
		}
		
		proc step_inc {id inc} {
			upvar #0 S_[set id](selStep) selStep
			upvar #0 S_[set id](Remarks) Remarks
			
			# selStep has origin 1; Remarks has also origin 1 with fake 0 entry
			if {([string compare $selStep ""] != 0) &&
			    ([isinteger $selStep]) &&
			    ([expr $selStep $inc] >= 0) && ([expr $selStep $inc] <= [llength $Remarks])} {
			    incr selStep $inc
			    boxit $id $selStep
			}
		}

		proc msc_init { qproc cproc sfont bfont ifontproc dfontproc gettxtproc mproc } {
			# read-only parameters
			global YSZ
			global XSZ
			global YLOCLABEL
			global YNR
			global NPR
			global SMX
			global Easy
			global BG
			global TOPH
			global BOTRSV
			global DX
			global DY
			global SCROLLBARWIDTH

			global BG FG Ptype Pargs SparseMsc MySparseMsc NoBoxes ArrowWidth VERBOSE SYMBOLIC
			global Blue Yellow White Red Green SmallFont BigFont
			
			# state information
			global cnr
			# upvar #0 S_[set id](decfontproc) decfontproc
			# upvar #0 S_[set id](incfontproc) incfontproc
			# upvar #0 S_[set id](quitproc) quitproc
			# upvar #0 S_[set id](closeproc) closeproc
			# upvar #0 S_[set id](gettextproc) gettextproc
			global decfontproc
			global incfontproc
			global quitproc
			global closeproc
			global gettextproc
			global msgproc
			global reuseQ
		
			# puts stderr "msc_init"

			image create bitmap up -data {
#define up_width 5
#define up_height 3
static unsigned char up_bits[] = {
   0x04, 0x0e, 0x1f};
			}
			image create bitmap down -data {
#define down_width 5
#define down_height 3
static unsigned char down_bits[] = {
   0x1f, 0x0e, 0x04};
			}
	

			# 'read-only' parameters below, these should not be changed
			set YSZ 12
			set XSZ 84
			set YLOCLABEL 0
			set YNR 60
			set NPR 10
			set SMX 250
			set Easy 1
			set TOPH [expr 2*$YSZ]
			set BOTRSV [expr 5*$YSZ] ;# free space reserved at bottom of scrollregion
			                         ;# should be large enough to draw end-of-msc in
		
			set BG   "white"	;# default background color for text
			set FG   "black"	;# default foreground color for text
			set Ptype "color"	;# printer-type: mono, color, or gray
			# set Pargs "-pageheight 29c -pagewidth 21c"
			set Pargs ""
			
			set SparseMsc 0
			set MySparseMsc 1
			set NoBoxes 1
			set ArrowWidth 1 ;# originally 2
			set VERBOSE 0
			set SYMBOLIC 0
		
			set Blue	"blue";		#"yellow"
			set Yellow	"yellow";	#"red"
			set White	"white";	#"yellow"
			set Red		"red";		#"yellow"
			set Green	"green";	#"green"

			set DX 10c
			set DY 7c
			set SCROLLBARWIDTH 10

			# state information below
			set SmallFont	$sfont
			if {[string compare $SmallFont ""] == 0} {
				set SmallFont	"-*-Courier-Bold-R-Normal--*-60-*"
			}
			set BigFont	$bfont
			if {[string compare $BigFont ""] == 0} {
				set BigFont	"-*-Courier-Bold-R-Normal--*-60-*"
			}

			set cnr 0			;# canvas number, incremented for each new window
			set reuseQ {}
			
			set quitproc $qproc
			set closeproc $cproc
			set incfontproc $ifontproc
			set decfontproc $dfontproc
			set gettextproc $gettxtproc
			set msgproc $mproc
			
		}
		
		proc find_reusable {} {
			global reuseQ
			global Eofseen
			
			foreach oldcnr_oldid $reuseQ {
				set oldcnr [lindex $oldcnr_oldid 0]
				set oldid  [lindex $oldcnr_oldid 1]
				if {$Eofseen($oldid)} {
					# puts stderr "find_reusable: found $oldid (in $oldcnr)"
					set reuseQ [ldelete $reuseQ $oldcnr_oldid]
					return $oldcnr_oldid
				} else {
					# puts stderr "find_reusable: skipping $oldid (in $oldcnr): no eof seen"
				}
			}
			return {}
		}

		proc msc_start {id reuse mcastfid name nr} {
			global YSZ
			global XSZ
			global NPR
			global SMX
			global BG
			
			upvar #0 S_[set id](dbox2) dbox2
			upvar #0 S_[set id](Seenino) Seenino
			upvar #0 S_[set id](Pstp) Pstp
			upvar #0 S_[set id](Remarks) Remarks
			upvar #0 S_[set id](RCoords) RCoords
			upvar #0 S_[set id](Arrows) Arrows
			upvar #0 S_[set id](cfid) cfid
			upvar #0 S_[set id](ctfakeheader) ctfakeheader
			global Eofseen
		
			set cfid $mcastfid
			set Remarks {}
			set Arrows {}
			
			# puts stderr "msc_start $id $name $nr"

			set Pstp 3 ;# keep two line free after labels - we depend on YLOCLABEL 0
			           ;# we changed from 2 to 3, to make it work with a -scrollincrement
			           ;# of 2*$YSZ
			set Seenino -1
			set Eofseen($id) 0
		
			set maxx [expr [winfo screenwidth .]  - 500]	;# button widths
#			set maxh [expr [winfo screenheight .] - (350+120)]	;# borders+buttons
			set maxh 7c
			
			set oldcnr_oldid [find_reusable]
			if {$oldcnr_oldid != {}} {
				set oldcnr [lindex $oldcnr_oldid 0]
				set oldid  [lindex $oldcnr_oldid 1]
				fake_destroy $oldid
				set dbox2 [reusecanvas $oldcnr \
				    $id $reuse "Message Sequence Chart $id: $name" "msc-$id-$nr" $name $maxx 350 1]
			} else {
				set dbox2 [mkcanvas \
				    $id $reuse "Message Sequence Chart $id: $name" "msc-$id-$nr" $name $maxx 350 1]
			}
# -height $maxh
			.f$dbox2.c configure \
				-scrollregion "[expr -$XSZ/2] 0 \
					[expr $NPR*$XSZ] [expr $SMX*$YSZ]"
				# \
				# -yscrollincrement [expr 2*$YSZ]
				# no longer use xscrollincrement
				# \
				# -xscrollincrement $XSZ
			.f$dbox2.ct configure \
				-scrollregion [lindex [.f$dbox2.c configure -scrollregion] 4] \
				-yscrollincrement [lindex [.f$dbox2.c configure -yscrollincrement] 4] \
				-xscrollincrement  [lindex [.f$dbox2.c configure -xscrollincrement] 4]
						
			
			# create ``fake'' rectangle for the header;
			# we will update its bbox (coords) whenever a
			# new instance header box is created.
			lappend Remarks  [.f$dbox2.c create rectangle \
					0 0 1 1  -width 0 -fill "" -outline "" ]
			.f$dbox2.c bind [lindex $Remarks end] <Any-Enter> "
				set S_[set id](selStep) [expr [llength $Remarks] - 1]
				msc::boxit $id [expr [llength $Remarks] - 1]
			"
			.f$dbox2.c bind [lindex $Remarks end] <Any-Leave> "
				after 0 {}
			"

			set ctfakeheader [.f$dbox2.ct create rectangle \
					0 0 1 1  -width 0 -fill "" -outline "" ]
			.f$dbox2.ct bind $ctfakeheader <Any-Enter> "
				set S_[set id](selStep) [expr [llength $Remarks] - 1]
				msc::boxit $id [expr [llength $Remarks] - 1]
			"
			.f$dbox2.ct bind $ctfakeheader <Any-Leave> "
				after 0 {}
			"

			lappend RCoords  {}
			lappend Arrows  {}
			update
			update idletasks
		}
		
		proc msc_quit {id} {
			upvar #0 S_[set id](dbox2) dbox2
			# upvar #0 S_[set id](quitproc) quitproc
			global quitproc
			global closeproc

			# puts stderr "msc_quit $id : eval $quitproc"
			catch {eval $quitproc}
		}
		proc msc_destroy {id} {
			upvar #0 S_[set id](busy) busy
			upvar #0 S_[set id](destroying) destroying

			# puts stderr "msc_destroy $id"

			set destroying 1
			if {$busy} {
				# puts stderr "msc_destroy $id waiting for $busy"
				trace variable busy wu "msc::destroy_when_not_busy $id"
			} else {
				msc_really_destroy $id
			}
		}
		proc destroy_when_not_busy {id args} {
			# puts stderr "destroy_when_not_busy $id $args"
			msc_really_destroy $id
		}
		proc msc_really_destroy {id} {
			upvar #0 S_[set id](dbox2) dbox2
			# upvar #0 S_[set id](quitproc) quitproc
			global quitproc
			global closeproc

			# puts stderr "msc_really_destroy $id"
			destroy .f$dbox2
			msc_cleanup $id $dbox2
			# catch {eval $quitproc}
			catch {eval $closeproc $id}
		}
		proc fake_destroy {id} {
			# upvar #0 S_[set id](quitproc) quitproc
			upvar #0 S_[set id](dbox2) dbox2
			global quitproc
			global closeproc

			# puts stderr "fake_destroy $id"
			msc_cleanup $id $dbox2
			# catch {eval $quitproc}
			catch {eval $closeproc $id}
		}
		proc msc_cleanup {id dbox2} {
			upvar #0 S_[set id](Remarks) Remarks
			upvar #0 S_[set id](Arrows) Arrows
			upvar #0 S_[set id](RCoords) RCoords
			upvar #0 S_[set id](ctfakeheader) ctfakeheader
			upvar #0 S_[set id](Pstp) Pstp

			global reuseQ

			foreach vName {R T Plabel HasBox Rem Mesg_x Mesg_y Q_add Q_del Q_val imap A_present} {
				upvar #0 [set vName]_[set id] $vName
				catch {unset $vName}
			}
			catch {unset S_[set id]}
			catch {unset Remarks}
			catch {unset RCoords}
			catch {unset Arrows}
			catch {unset ctfakeheader}
			catch {unset Pstp}
			set reuseQ [ldelete $reuseQ [list $dbox2 $id]]
		}

		proc msc_append {id errprefix pstp interface {HAS 1} stmnt} {
			global YSZ
			global XSZ
			global YLOCLABEL
			global YNR
			global NPR
			global SMX
			global Easy
			global SparseMsc MySparseMsc NoBoxes ArrowWidth
			global VERBOSE SYMBOLIC
			global Blue
			global Yellow
			global White
			global Red
			global Green
			global SmallFont
			global BigFont
			global TOPH
			global BOTRSV
			global msgproc
			
			upvar #0 S_[set id](dbox2) dbox2
			upvar #0 S_[set id](Seenino) Seenino
			upvar #0 S_[set id](Pstp) Pstp
			upvar #0 S_[set id](Remarks) Remarks
			upvar #0 S_[set id](RCoords) RCoords
			upvar #0 S_[set id](Arrows) Arrows
			upvar #0 S_[set id](ctfakeheader) ctfakeheader

			foreach vName {R T Plabel HasBox Rem Mesg_x Mesg_y Q_add Q_del Q_val imap A_present} {
				upvar #0 [set vName]_[set id] $vName
			}

			# puts stderr "msc_append $id $errprefix $pstp $interface $HAS ($dbox2) : $stmnt"
			
			if {![winfo exists .f$dbox2]} {
				return
			}
			
			set Fnm ""
			set pln 0
			
			# parse line
			if {![info exists imap($interface)]} {
				set imap($interface) [llength [array names imap]]
			}
			set ino $imap($interface)
			set inq $stmnt
			set Mcont "--"
			# update seen process numbers:
			if {$ino+1 > $Seenino} { set Seenino [expr $ino+1] }
			# update horizontal position:
			set XLOC [expr (1+$ino)*$XSZ]
			# update vertical postions: number-of-steps*size
			set YLOC [expr $Pstp*$YSZ]
			# do the real work, which we catch
		#	catch {
				if {$VERBOSE \
				||  $HAS != 0 \
				||  [info exists R($pstp,$ino)]} {
		
			# update the vertical line segments, if necessary
					if { $SparseMsc == 1 \
					||   [info exists Plabel($ino)] == 0 \
					||   ([info exists R($pstp,$ino)] == 0 \
					&&   ($HAS != 1 \
					||   [info exists HasBox($YLOC,[expr 1+$ino])])) } {
						if {$HAS == 2} {
							incr Pstp
							for	{set i 1} \
								{$Pstp > 1 && $i <= $Seenino} \
								{incr i} {
								if {[info exists HasBox($YLOC,$i)]} {
									continue
								}
								set TMP1 [expr $i*$XSZ]
								set lncol $Blue
								set lnwdt 1
								catch {
									if {$nwcol($i)} {
										set lncol "gray"
										set lnwdt 15
								}	}
								.f$dbox2.c create line \
								$TMP1 $YLOC $TMP1 \
								[expr $YLOC+$YSZ] \
								-width $lnwdt \
								-fill $lncol
							}
							if {[info exists HasBox($YLOC,[expr 1+$ino])]} {
								set YLOC [expr $Pstp*$YSZ]
							}
						}
					}
		
			# create first process box (label)
					if { [info exists Plabel($ino)] == 0} {
						set Plabel($ino) 0
						if {$SparseMsc == 0} {
							set HasBox($YLOCLABEL,[expr 1+$ino]) 1
						}
						set rh [.f$dbox2.c create rectangle \
							[expr $XLOC-20] $YLOCLABEL \
							[expr $XLOC+20] \
							[expr $YLOCLABEL+$YSZ] \
							-outline $Red -fill $Yellow]
						.f$dbox2.c addtag header withtag $rh
						# update coords (bbox) of ``fake'' header rectangle
						eval .f$dbox2.c coords [lindex $Remarks 0] \
							[.f$dbox2.c bbox header]
						set rht [.f$dbox2.ct create rectangle \
							[expr $XLOC-20] [expr $TOPH+$YLOCLABEL]\
							[expr $XLOC+20] \
							[expr $TOPH+$YLOCLABEL+$YSZ] \
							-outline $Red -fill $Yellow]
						.f$dbox2.ct addtag header withtag $rht
						# update coords (bbox) of ``fake'' header rectangle
						eval .f$dbox2.ct coords $ctfakeheader \
							[.f$dbox2.ct bbox header]
						if {$interface != "TRACK"} {
							.f$dbox2.c create text $XLOC \
								[expr $YLOCLABEL+$YSZ/2] \
								-font $SmallFont \
								-text "$interface"
							.f$dbox2.ct create text $XLOC \
								[expr $TOPH+$YLOCLABEL+$YSZ/2] \
								-font $SmallFont \
								-text "$interface"
						} else {
							.f$dbox2.c create text $XLOC \
								[expr $YLOCLABEL+$YSZ/2] \
								-font $SmallFont \
								-text "<show>"
							.f$dbox2.ct create text $XLOC \
								[expr $TOPH+$YLOCLABEL+$YSZ/2] \
								-font $SmallFont \
								-text "<show>"
						}
						eval .f$dbox2.c raise [lindex $Remarks 0]
						eval .f$dbox2.ct raise $ctfakeheader
		
						# make sure the top of the labels is visible too
						msc::yview .f$dbox2.c .f$dbox2.ct yview moveto 0
		
			# put blue line from newly created label box to current YLOC
						for	{set i 1} \
							{$YLOC > 1 && $i < $Pstp} \
							{incr i} {
							set TMP1 [expr $i*$YSZ]
							set lncol $Blue
							set lnwdt 1
							catch {
								if {$nwcol($i)} {
									set lncol "gray"
									set lnwdt 15
							}	}
							.f$dbox2.c create line \
								$XLOC $TMP1 \
								$XLOC [expr $TMP1+$YSZ] \
								-width $lnwdt \
								-fill $lncol
						}
						set lncol $Blue
						set lnwdt 1
						.f$dbox2.ct create line \
							$XLOC [expr $TOPH+$YSZ] \
							$XLOC [expr $TOPH+$YSZ+$YSZ] \
							-width $lnwdt \
							-fill $lncol \
							-stipple gray50
		
			# put blue line next to newly created label box
						if {$YLOCLABEL != 0} {
							set YLOC [expr $Pstp*$YSZ]
							incr Pstp
							for	{set i 1} \
								{$Pstp > 1 && $i <= $Seenino} \
								{incr i} {
								set TMP1 [expr $i*$XSZ]
								set lncol $Blue
								set lnwdt 1
								catch {
									if {$nwcol($i)} {
										set lncol "gray"
										set lnwdt 15
								}	}
								.f$dbox2.c create line \
									$TMP1 $YLOC $TMP1 \
									[expr $YLOC+$YSZ] \
									-width $lnwdt \
									-fill $lncol
							}
						}

					}
		
			# update scrolling
					.f$dbox2.c configure \
						 -scrollregion \
						 "[expr -$XSZ/2] 0 \
						  [expr $NPR*$XSZ] [expr $YLOC + $BOTRSV]"
					.f$dbox2.ct configure \
						 -scrollregion [lindex [.f$dbox2.c configure -scrollregion] 4]
		
			# create node
					if { (! $NoBoxes) && ([info exists R($pstp,$ino)] == 0) } {
						if {$VERBOSE == 1} {
						#	set BoxFil $Yellow
						#	Michael Griffioen:
							if {[string first "~W " $stmnt] == 0} {
								set BoxFil $White
								set stmnt [string range $stmnt 3 end] 
							} elseif {[string first "~G " $stmnt] == 0} {
								set BoxFil $Green
								set stmnt [string range $stmnt 3 end]
							} elseif {[string first "~R " $stmnt] == 0} {
								set BoxFil $Red
								set stmnt [string range $stmnt 3 end]
							} elseif {[string first "~B " $stmnt] == 0} {
								set BoxFil $Blue
								set stmnt [string range $stmnt 3 end]
							} else {
								set BoxFil $Yellow
							}
							set BoxLab $stmnt
						} else {
							set BoxLab $pstp
							set BoxFil $White
						}
						if {$SparseMsc == 0} {
							set HasBox($YLOC,[expr 1+$ino]) 1
						}
						set R($pstp,$ino) \
							[.f$dbox2.c create rectangle \
							 [expr $XLOC-20] $YLOC \
							 [expr $XLOC+20] \
							 [expr $YLOC+$YSZ] \
							 -outline $Blue -fill $BoxFil]
						set T($pstp,$ino) \
							[.f$dbox2.c create text \
							 $XLOC \
							 [expr $YLOC+$YSZ/2] \
							-font $SmallFont \
							-text $BoxLab]
						#if {$Pstp > $YNR-2} {
						#	.f$dbox2.c yview \
						#	 [expr ($Pstp-$YNR)]
						#}
					}
					if { (!$NoBoxes) && ($HAS == 3) } {
						.f$dbox2.c itemconfigure \
							 $R($pstp,$ino) \
							 -outline $Red -fill $Yellow
					}
		
					if { (!$NoBoxes) && $SYMBOLIC} {
						.f$dbox2.c itemconfigure $T($pstp,$ino) \
							-font $SmallFont -text "$stmnt"
					} else {
						if {0} {
						if {$VERBOSE == 0 } {
							.f$dbox2.c bind $T($pstp,$ino) <Any-Enter> "
							.f$dbox2.c itemconfigure $T($pstp,$ino) \
								-font $BigFont -text {$stmnt}
							.inp.t tag remove hilite 0.0 end
							if {[string first "pan_in" $Fnm] >= 0} {
								src_line {[format "%d" $pln]}
							}
							"
							.f$dbox2.c bind $T($pstp,$ino) <Any-Leave> "
							.f$dbox2.c itemconfigure $T($pstp,$ino) \
								-font $SmallFont -text {$pstp}
							"
						} else {
							.f$dbox2.c bind $T($pstp,$ino) <Any-Enter> "
							.inp.t tag remove hilite 0.0 end
							if {[string first "pan_in" $Fnm] >= 0} {
								src_line {[format "%d" $pln]}
							}
							"
						}
						}
					}
				}
		
				set YLOC [expr $YLOC+$YSZ/2]
				if {$HAS == 1} {
					if { [info exists Q_add($inq)] == 0 } {
						set Q_add($inq) 0
						set Q_del($inq) 0
					}
					set Slot $Q_add($inq)
					incr Q_add($inq) 1
		
					set Mesg_y($inq,$Slot) $YLOC
					set Mesg_x($inq,$Slot) $XLOC
					set Q_val($inq,$Slot) $Mcont
					
					set A_present($XLOC,$YLOC) 1
					
					set Rem($inq,$Slot) \
						[.f$dbox2.c create text \
						[expr $XLOC-40] $YLOC \
						-font $SmallFont -text $stmnt]
#					.f$dbox2.c itemconfigure $Rem($inq,$Slot) \
#						-font $SmallFont -text \"$stmnt\"
				} elseif { $HAS == 2 } {
					if {$Easy} {
						set Slot $Q_del($inq)
						incr Q_del($inq) 1
					} else {
						for {set Slot $Q_del($inq)} \
							{$Slot < $Q_add($inq)} \
							{incr Slot} {
							if {$Q_val($inq,$Slot) == "_X_"} {
								incr Q_del($inq) 1
							} else {
								break
						}	}
		
						for {set Slot $Q_del($inq)} \
							{$Slot < $Q_add($inq)} \
							{incr Slot} {
							if {$Mcont == $Q_val($inq,$Slot)} {
								set Q_val($inq,$Slot) "_X_"
								break
						}	}
					}
					if {$Slot >= $Q_add($inq)} {
						$msgproc $id "$errprefix: error: cannot match $stmnt"
					} else {
						set TMP1 $Mesg_x($inq,$Slot)
						set TMP2 $Mesg_y($inq,$Slot)
						if ($NoBoxes) {
							set Delta 0
						} elseif {$XLOC < $TMP1} {
							set Delta -20
						} else {
							set Delta 20
						}
						# see if we have crossing arrows, if so:
						# use diagonal arrows instead of horizontal
						if {[info exists A_present($XLOC,$YLOC)]} {
							set TMP4 [expr $YLOC+$YSZ]
						} else {
							set TMP4 $YLOC
						}
						lappend Arrows  \
							[.f$dbox2.c create line \
								[expr $TMP1+$Delta] $TMP2 \
								[expr $XLOC-$Delta] $TMP4 \
								-fill $Red -width $ArrowWidth \
								-arrow last -arrowshape {5 5 5} ]
		
						if {$NoBoxes} {
							set TMP3 5
						} elseif {$MySparseMsc == 1} {
							set TMP3 8
						} elseif {$SparseMsc == 0} {
							set TMP3 5
						} else {
							set TMP3 0
						}
		
						.f$dbox2.c coords $Rem($inq,$Slot) \
							[expr ($TMP1 + $XLOC)/2] \
							[expr ($TMP2 + $YLOC)/2 - $TMP3]
						# puts stderr "DEBUG: bbox: [.f$dbox2.c bbox $Rem($inq,$Slot)]"
						set bbox_x [lindex [.f$dbox2.c bbox $Rem($inq,$Slot)] 0]
						if {$bbox_x < 0} {
							# puts stderr "DEBUG: move because: $bbox_x"
							.f$dbox2.c move $Rem($inq,$Slot) [expr - $bbox_x] 0
						}
						if {0} {
							# move to the left, and anchor west;
							# this is not satisfactory for the numeric
							# labels when we move the mouse over it :-(
							set bbox [.f$dbox2.c bbox $Rem($inq,$Slot)]
							set bbox_x1 [lindex $bbox 0]
							set bbox_x2 [lindex $bbox 2]
							set halflength [expr ($bbox_x2 - $bbox_x1) / 2]
							.f$dbox2.c move $Rem($inq,$Slot) [expr - $halflength] 0
							.f$dbox2.c itemconfigure  $Rem($inq,$Slot) -anchor w
						}
							
						# puts stderr "DEBUG: bbox: [.f$dbox2.c bbox $Rem($inq,$Slot)]"
						.f$dbox2.c raise $Rem($inq,$Slot)

						lappend Remarks $Rem($inq,$Slot)
						lappend RCoords [.f$dbox2.c coords $Rem($inq,$Slot)]
						.f$dbox2.c bind $Rem($inq,$Slot) <Any-Enter> "
							set S_[set id](selStep) [expr [llength $Remarks] - 1]
							msc::boxit $id [expr [llength $Remarks] - 1]
						"
						.f$dbox2.c bind $Rem($inq,$Slot) <Any-Leave> "
							after 0 {}
						"
				
						# scroll the msc window                  
 						msc::boxit $id [expr [llength $Remarks] - 1] 0
					}
				}
		
			# put blue line next to newly created label box
				if {($HAS == 2) && ($MySparseMsc != 0)} {
					set YLOC [expr $Pstp*$YSZ]
					incr Pstp
					for	{set i 1} \
						{$Pstp > 1 && $i <= $Seenino} \
						{incr i} {
						set TMP1 [expr $i*$XSZ]
						set lncol $Blue
						set lnwdt 1
						catch {
							if {$nwcol($i)} {
								set lncol "gray"
								set lnwdt 15
						}	}
						.f$dbox2.c create line \
							$TMP1 $YLOC $TMP1 \
							[expr $YLOC+$YSZ] \
							-width $lnwdt \
							-fill $lncol
					}
				}
                                      
				# enable save-as button            
				.f$dbox2.b2  configure -state normal 
		
		
		#	}
			update
			update idletasks
		}

		proc msc_end {id {abnormal 0} } {
			global YSZ
			global XSZ
			global YLOCLABEL
			global YNR
			global NPR
			global SMX
			global Easy
			global SparseMsc MySparseMsc NoBoxes ArrowWidth
			global VERBOSE SYMBOLIC
			global Blue
			global Yellow
			global White
			global Red
			global Green
			global SmallFont
			global BigFont
			global BG

			upvar #0 S_[set id](dbox2) dbox2
			upvar #0 S_[set id](Seenino) Seenino
			upvar #0 S_[set id](Pstp) Pstp
			upvar #0 S_[set id](Remarks) Remarks
			upvar #0 S_[set id](RCoords) RCoords
			upvar #0 S_[set id](Arrows) Arrows
			global Eofseen
			
			set Eofseen($id) 1
			# seen process numbers: $Seenino
			# if {$ino+1 > $Seenino} { set Seenino [expr $ino+1] }
			# update vertical postions: number-of-steps*size
			set YLOC [expr $Pstp*$YSZ]
			# foreach horizontal position:
			# set XLOC [expr (1+$ino)*$XSZ]
			set xbeg ""
			set xend ""
			if {$abnormal} {
				for	{set i 1} {$i <= $Seenino} {incr i} {
					set TMP1 [expr $i*$XSZ]
					set lncol $Red
					set lnwdt 1
					catch {
						if {$nwcol($i)} {
							set lncol "gray"
							set lnwdt 15
					}	}
					.f$dbox2.c create line \
						$TMP1 \
						$YLOC \
						$TMP1 \
						[expr $YLOC + $YSZ + $YSZ] \
						-width $lnwdt \
						-fill $lncol \
						-stipple gray50
					.f$dbox2.c create line \
						[expr $TMP1 - 10] \
						[expr $YLOC + $YSZ + $YSZ] \
						[expr $TMP1 + 10] \
						[expr $YLOC + $YSZ + $YSZ] \
						-width [expr 2 * $lnwdt ] \
						-fill $lncol \
						-stipple gray50
					if {[string compare $xbeg ""] == 0} {
						set xbeg [expr $TMP1 - 10]
					}
					set xend [expr $TMP1 + 10]
				}
				if {([string compare $xbeg ""] != 0) &&
				    ([string compare $xend ""] != 0)} {
					lappend Remarks [.f$dbox2.c create rectangle \
						$xbeg [expr $YLOC + $YSZ + $YSZ] \
						$xend [expr $YLOC + $YSZ + $YSZ + $YSZ] \
						-width 0 -fill "" -outline "" ]
					.f$dbox2.c bind [lindex $Remarks end] <Any-Enter> "
						set S_[set id](selStep) [expr [llength $Remarks] - 1]
						msc::boxit $id [expr [llength $Remarks] - 1]
					"
					.f$dbox2.c bind [lindex $Remarks end] <Any-Leave> "
						after 0 {}
					"
					.f$dbox2.c raise [lindex $Remarks end]
					lappend RCoords  {}
					lappend Arrows {}
				}
			} else {
				for	{set i 1} {$i <= $Seenino} {incr i} {
					set TMP1 [expr $i*$XSZ]
					set lncol $Red
					set lnwdt 1
					catch {
						if {$nwcol($i)} {
							set lncol "gray"
							set lnwdt 15
					}	}
					.f$dbox2.c create line \
						[expr $TMP1 - 10] \
						$YLOC \
						[expr $TMP1 + 10] \
						$YLOC \
						-width [expr 2 * $lnwdt ] \
						-fill $lncol
					if {[string compare $xbeg ""] == 0} {
						set xbeg [expr $TMP1 - 10]
					}
					set xend [expr $TMP1 + 10]
				}
				if {([string compare $xbeg ""] != 0) &&
				    ([string compare $xend ""] != 0)} {
					lappend Remarks [.f$dbox2.c create rectangle \
						$xbeg [expr $YLOC] \
						$xend [expr $YLOC + $YSZ] \
						-width 0 -fill "" -outline "" ]
					.f$dbox2.c bind [lindex $Remarks end] <Any-Enter> "
						set S_[set id](selStep) [expr [llength $Remarks] - 1]
						msc::boxit $id [expr [llength $Remarks] - 1]
					"
					.f$dbox2.c bind [lindex $Remarks end] <Any-Leave> "
						after 0 {}
					"
					.f$dbox2.c raise [lindex $Remarks end]
					lappend RCoords  {}
					lappend Arrows {}
				}
			}
			msc::boxit $id [expr [llength $Remarks] - 1] 0
			.f$dbox2.b6 configure -state normal

		}
}
