proc debugcl { id lvl s } {
	global debug
	upvar #0 G_[set id](ofid) ofid
	global G
	if {$debug >= $lvl } {
		catch {
			catch {puts $ofid "DEBUG: $G(sprog): $s"}
			flush $ofid
		}
	}
}
proc debug { lvl s } {
	global debug
	global G
	if {$debug >= $lvl } {
		catch {
			catch {puts stderr "DEBUG: $G(sprog): $s"}
			flush stderr
		}
	}
}
proc warn { fid s } {
	global PID
	global G
	catch {
		puts $fid "$G(sprog):$PID warning: $s"
		flush $fid
	}
}
proc output { c s } {
	global G
	if {[catch {
		puts $c $s
		flush $c
	} msg]} {
		catch {puts stderr "$G(sprog): output: $msg"}
	}
}

proc msg { fid s } {
	global PID
	global G
	catch {puts $fid "$G(sprog):$PID $s" }
}
