#mactoolbar: provides Mac-style toolbar. (c) 2007 WordTech Communications LLC. License: standard Tcl license, http://www.tcl.tk/software/tcltk/license.html


#Mac-style search field by Schelete Bron: http://wiki.tcl.tk/18188.


package provide mactoolbar 1.1

if {[info patchlevel] < 8.5} {
    package require tile
}


namespace eval mactoolbar {

    variable mytoolbar
    variable search
    variable glass


 #initialize the toolbar
    proc create {w} {

	variable mytoolbar
	variable toolwindow

	pack [ttk::frame $w] -side top -fill both -expand no

	set toolwindow [winfo toplevel $w]
	
	if {[string equal [tk windowingsystem] aqua]}  {
		tk::unsupported::MacWindowStyle style $toolwindow document {toolbarButton standardDocument}
	}

    }


#main procedure: create toolbar frame, search field
    proc toolbar {w} {

	variable mytoolbar
	variable search
	variable toolwindow


	set mytoolbar $w

	mactoolbar::create $mytoolbar

	bind [winfo toplevel $mytoolbar] <<ToolbarButton>> [list mactoolbar::toggle [winfo parent $mytoolbar]]

	ttk::frame $mytoolbar.frame 
	pack $w.frame -fill both -expand yes

	ttk::frame $mytoolbar.frame.top
	pack $mytoolbar.frame.top -side top -fill both -expand yes

	ttk::separator $w.frame.bottom -orient vertical

	pack $w.frame.bottom -fill both -expand no -side bottom

    }
    
    proc getframe {} {
    
    	variable mytoolbar

	return $mytoolbar.frame.top
    }


    #add buttons to the toolbar: widget name, image, command, help tag
    proc add {child img cmd help} {

	variable mytoolbar

	set mywidget $mytoolbar.frame.top.$child 

	ttk::label $mywidget  -image $img 
	pack $mywidget -side left -fill both -expand no


	bind $mywidget <Button-1>  "$mywidget configure -image $img-dark"
	bind  $mywidget <ButtonRelease-1>  "$mywidget configure -image $img; $cmd" 

	mactoolbar::balloon $mywidget $help

    }


    #hide/show toolbar if toolbutton window decoration is pressed: <<ToolbarButton>> event

    proc toggle {w} {
    
 	if [winfo ismapped $w] {
	    pack forget $w
	} else {
	    pack $w -before [pack slaves [winfo parent $w]] \
	    	-side top -fill both -expand no
	}
    }
	   

#add the search field

    proc add_searchfield {} {

	variable mytoolbar
	variable searchterm

	set searchterm ""


    set glass {

 R0lGODlhKgAaAOfnAFdZVllbWFpcWVtdWlxeW11fXF9hXmBiX2ZnZWhpZ2lraGxua25wbXJ0
 cXR2c3V3dHZ4dXh6d3x+e31/fH6AfYSGg4eJhoiKh4qMiYuNio2PjHmUqnqVq3yXrZGTkJKU
 kX+asJSWk32cuJWXlIGcs5aYlX6euZeZloOetZial4SftpqbmIWgt4GhvYahuIKivpudmYei
 uYOjv5yem4ijuoSkwIWlwYmlu56gnYamwp+hnoenw4unvaCin4ioxJCnuZykrImpxZmlsoaq
 zI2pv6KkoZGouoqqxpqms4erzaOloo6qwYurx5Kqu5untIiszqSmo5CrwoysyJeqtpOrvJyo
 tZGsw42typSsvaaopZKtxJWtvp6qt4+uy6epppOuxZCvzKiqp5quuZSvxoyx06mrqJWwx42y
 1JKxzpmwwaqsqZaxyI6z1ZqxwqutqpOzz4+01qyuq56yvpizypS00Jm0y5W10Zq1zJa20rCy
 rpu3zqizwbGzr6C3yZy4z7K0saG4yp250LO1sqK5y5660Z+70qO7zKy4xaC806S8zba4taG9
 1KW9zq66x6+7yLi6t6S/1rC8yrm7uLO8xLG9y7q8ubS9xabB2anB07K+zLW+xrO/za7CzrTA
 zrjAyLXBz77BvbbC0K/G2LjD0bnE0rLK28TGw8bIxcLL07vP28HN28rMycvOyr/T38DU4cnR
 2s/RztHT0NLU0cTY5MrW5MvX5dHX2c3Z59bY1dPb5Nbb3dLe7Nvd2t3f3NXh797g3d3j5dnl
 9OPl4eTm4+Ln6tzo9uXn5Obo5eDp8efp5uHq8uXq7ejq5+nr6OPs9Ovu6unu8O3v6+vw8+7w
 7ezx9O/x7vDy7/Hz8O/19/P18vT38/L3+fb49Pf59vX6/fj69/b7/vn7+Pr8+ff9//v9+vz/
 +/7//P//////////////////////////////////////////////////////////////////
 /////////////////////////////////yH/C05FVFNDQVBFMi4wAwEAAAAh+QQJZAD/ACwC
 AAIAKAAWAAAI/gD/CRz4bwUGCg8eQFjIsGHDBw4iTLAQgqBFgisuePCiyJOpUyBDihRpypMi
 Lx8qaLhIMIyGFZ5sAUsmjZrNmzhzWpO2DJgtTysqfGDpxoMbW8ekeQsXzty4p1CjRjUXrps3
 asJsuclQ4uKKSbamMR3n1JzZs2jRkh1HzuxVXX8y4CDYAwqua+DInVrRwMGJU2kDp31KThy1
 XGWGDlxhi1rTPAUICBBAoEAesoIzn6Vm68MKgVAUHftmzhOCBCtQwQKSoABgzZnJdSMmyIPA
 FbCotdUQAIhNa9B6DPCAGbZac+SowVIMRVe4pwkA4GpqDlwuAAmMZx4nTtfnf1mO5JEDNy46
 MHJkxQEDgKC49rPjwC0bqGaZuOoZAKjBPE4NgAzUvYcWOc0QZF91imAnCDHJ5JFAAJN0I2Ba
 4iRDUC/gOEVNDwIUcEABCAgAAATUTIgWOMBYRFp80ghiAQIIVAAEAwJIYI2JZnUji0XSYAYO
 NcsQA8wy0hCTwAASXGOiONFcxAtpTokTHznfiLMNMAkcAMuE43jDC0vLeGOWe2R5o4sn1LgH
 GzkWsvTPMgEOaA433Ag4TjjMuDkQMNi0tZ12sqWoJ0HATMPNffAZZ6U0wLAyqJ62RGoLLrhI
 aqmlpzwaEAAh+QQJZAD/ACwAAAAAKgAaAAAI/gD/CRw40JEhQoEC+fGjcOHCMRAjRkxDsKLF
 f5YcAcID582ZjyBDJhmZZIjJIUySEDHiBMhFghrtdNnRAgSHmzhz6sTZQcSLITx+CHn5bxSk
 Nz5MCMGy55CjTVCjbuJEtSrVQ3uwqDBRQwrFi476SHHxow8qXcemVbPGtm21t3CnTaP27Jgu
 VHtuiIjBsuImQkRiiEEFTNo2cOTMKV7MuLE5cN68QUOGSgwKG1EqJqJDY8+rZt8UjxtNunTj
 cY3DgZOWS46KIFgGjiI0ZIsqaqNNjWjgYMUpx8Adc3v2aosNMAI1DbqyI9WycOb4IAggQEAB
 A3lQBxet/TG4cMpI/tHwYeSfIzxM0uTKNs7UgAQrYL1akaDA7+3bueVqY4NJlUhIcQLNYx8E
 AIQ01mwjTQ8DeNAdfouNA8440GBCQxJY3MEGD6p4Y844CQCAizcSgpMLAAlAuJ03qOyQRBR3
 nEHEK+BMGKIui4kDDAAIPKiiYuSYSMQQRCDCxhiziPMYBgDkEaEaAGQA3Y+MjUPOLFoMoUUh
 cKxRC4ngeILiH8Qkk0cCAUzSDZWpzbLEE1EwggcYqWCj2DNADFDAAQUgIAAAEFDDJmPYqNJF
 F1s4cscTmCDjDTjdSPOHBQggUAEQDAgggTWDPoYMJkFoUdRmddyyjWLeULMMMcAsIw0x4wkM
 IME1g25zyxpHxFYUHmyIggw4H4ojITnfiLMNMAkcAAub4BQjihRdDGTJHmvc4Qo1wD6Imje6
 eILbj+BQ4wqu5Q3ECSJ0FOKKMtv4mBg33Pw4zjbKuBIIE1xYpIkhdQQiyi7OtAucj6dt48wu
 otQhBRa6VvSJIRwhIkotvgRTzMUYZ6xxMcj4QkspeKDxxRhEmUfIHWjAgQcijEDissuXvCyz
 zH7Q8YQURxDhUsn/bCInR3AELfTQZBRt9BBJkCGFFVhMwTNBlnBCSCGEIJQQIAklZMXWRBAR

 RRRWENHwRQEBADs=

}

	image create photo search1 -data $glass -format "gif -index 0"
	image create photo search2 -data $glass -format "gif -index 1"

    
    if {[info patchlevel] < 8.5} {
	ttk::style element create Search.field image search1 \
	    -border {22 7 14} -sticky ew -map {focus search2}
    } else {
	ttk::style element create Search.field image {search1  focus search2} \
	    -border {22 7 14} -sticky ew
    }

	ttk::style layout Search.entry {
	    Search.field -sticky nswe -border 1 -children {
		Entry.padding -sticky nswe -children {
		    Entry.textarea -sticky nswe
		}
	    }
	}


	ttk::entry  $mytoolbar.frame.top.entry -style Search.entry -width 30 -textvariable mactoolbar::searchterm

	pack $mytoolbar.frame.top.entry -side right -fill x -expand no -padx 4 -pady 4

    }

    #obtain the value of the textvariable
    proc getsearchterm {} {

	variable searchterm

	if {$searchterm == ""} {
	    return
	}

	puts $searchterm
	return $searchterm

    }

    
    #bind command to search field
    proc bindentry {cmd} {
	variable mytoolbar

	bind $mytoolbar.frame.top.entry <Return> [list $cmd]
    }
    

    #balloon help
    proc balloon {w help} {
	bind $w <Any-Enter> "after 1000 [list mactoolbar::balloon_show %W [list $help]]"
	bind $w <Any-Leave> "destroy %W.balloon"
    }


    #show balloon help
    proc balloon_show {w arg} {
	if {[eval winfo containing  [winfo pointerxy .]]!=$w} {return}
	set top $w.balloon
	catch {destroy $top}
	toplevel $top -bd 0 -bg black
	#wm overrideredirect $top 1
	if {[string equal [tk windowingsystem] aqua]}  {
	    ::tk::unsupported::MacWindowStyle style $top help none
	}
	
	pack [message $top.txt -aspect 10000 -bg lightyellow \
		  -font {size 11} -text $arg]
	set wmx [winfo rootx $w]
	set wmy [expr [winfo rooty $w]+[winfo height $w]]
	wm geometry $top \
	    [winfo reqwidth $top.txt]x[winfo reqheight $top.txt]+$wmx+$wmy
	raise $top
    }



    #let's see how it works: demo
    proc demo {} {

	variable toolwindow


	#first, create the images--light and dark

	image create photo demo_folder-dark -data {
R0lGODlhEAANAMIFAAAAAEJCQmNjY36AAICAgP///////////yH+EUNyZWF0\
ZWQgd2l0aCBHSU1QACH5BAEAAAcALAAAAAAQAA0AAAM1eBrM+vAEMagI8c3K\
b3MSIY6kGABhp1JnaK1VGxDwKxNvHqNzzd263M3H4n2OH9QBwGw6nQkAOw==
	}

	image create photo demo_folder -data {
R0lGODlhEAANALMAAAAAAISEhMbGxv//AP///wD/////////////////////\
/////////////////////yH5BAEAAAUALAAAAAAQAA0AAAQ4sMhAqby4BDG4\
CNm1deRXWRqhrqwaAGkpc2/qzV0dEPitE7dgDrbrkX7C4M9II56eJ1gBQK1a\
rREAOw==
	}

	image create photo demo_file-dark -data {

R0lGODlhEAAQAKECAAAAAICAgP///////yH+EUNyZWF0ZWQgd2l0aCBHSU1Q\
ACH5BAEAAAIALAAAAAAQABAAAAIrlI95wM1qQJj0KUmnBDfrlmCeFo5VaQYc\
Io7rkqqo+UZxLbQe7vQWBAwGCwA7

}

	image create photo demo_file -data {

R0lGODlhEAAQALMAAAAAAMbGxv//////////////////////////////////\
/////////////////////yH5BAEAAAEALAAAAAAQABAAAAQwMMhJ6wQ4YyuB\
+OBmeeDnAWNpZhWpmu0bxrKAUu57X7VNy7tOLxjIqYiapIjDbDYjADs=
}

	#draw the parent frame

        ttk::frame .s 
	pack .s -side top -fill both -expand yes


	#add the toolbar
	mactoolbar::toolbar .s.top


	#add the toolbar buttons
	mactoolbar::add demo_folder demo_folder {puts "clicked"} "Folder"

	mactoolbar::add demo_file demo_file {puts "clicked"} "File"


	#add the searchfield
	mactoolbar::add_searchfield

	#bind the search field to a command
	mactoolbar::bindentry mactoolbar::getsearchterm


	#add an extra widget to show what the window looks like when the toolbar is hidden
	pack [ttk::label .y -text "This is empty screen real estate"] -side bottom -fill both -expand yes


    }


    namespace export *

}

