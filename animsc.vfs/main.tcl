package require starkit
starkit::startup
	set pid [pid]
#	puts "main:$pid: argv: $argv"
#	puts "main:$pid: info script: [info script]"
#	puts "main:$pid: starkit::topdir  $starkit::topdir"

#	puts "main:$pid: argv0 $argv0"
#	puts "main:$pid: where we are, as script: driname argv0 : [file dirname $argv0]"
#	puts "main:$pid: where we are, as starpack 1:  dirname of dirname of argv0: [file dirname [file dirname $argv0]]"
#	puts "main:$pid: where we are, as starpack 2: info nameofexecutable:  [info nameofexecutable]"
if {[string compare [lindex $argv 0] -server] == 0} {
	set argv [lrange $argv 1 end]
#	puts "main:$pid: requiring package app-animscsrv"
	package require app-animscsrv
} elseif {[lindex $argv 0] == {} &&
     [string compare [lindex $argv 1] -server] == 0} {
	set argv [lrange $argv 2 end]
#	puts "main:$pid: requiring package app-animscsrv"
	package require app-animscsrv
} else {
#	puts "main:$pid: requiring package app-animsc"
	package require app-animsc
}
