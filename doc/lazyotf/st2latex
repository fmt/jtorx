#!/bin/sh 
################################################################################
# Simple texi to LaTeX converter                                               #
#                                                                              #
# Usage: st2latex FILE                                                         #
# The converter outputs the LaTeX code to FILE.tex, where FILE is your input   #
# file. The LaTeX code does *not* contain the document headers, i.e. is *not*  #
# directly compileable by, say, pdflatex. This was intentionally left out.     #
# Instead, you need to write your own preamble LaTeX file and include the      #
# generated one within a "document" environment.                               #
# The converter assumes the inclusion of the following LaTeX packages:         #
# utf8 inputenc, amsmath, color                                                #
#                                                                              #
# Notes:                                                                       #
#   - The converter is somewhat tailored to the subset of features being used  #
#     to write the LazyOTF manual. It is not complete.                         #
#   - Things needing improvement:                                              #
#      * Image handling. Probably a job for awk.                               #
#      * Math handling. Currently works fine as long as you put @math          #
#        environments in their own lines.                                      #
#      * Having linked references in LaTeX would be quite nice.                #
#      * Another nice extra would be a way to specify where \[ should be used  #
#        instead of $.                                                         #
################################################################################

FILE=`basename $1`.tex
cp $1 $FILE
echo Output goes to $FILE.

echo Debug: Cleaning header.
sed -i '/\\input/d' $FILE
sed -i '/\@setfilename/d' $FILE
sed -i '/\@settitle/d' $FILE

echo Debug: Cleaning footer.
sed -i '/\@bye/d' $FILE

echo Debug: Removing copyright section.
sed -i '/\@copying/,/\@end\ copying/d' $FILE
sed -i '/\@insertcopying/d' $FILE

echo Debug: Fixing title.
sed -i '/\@titlepage/d' $FILE 
sed -i 's/\@title\ .*/\\title{\\textbf{&}}/g' $FILE
sed -i 's/\@title\ //g' $FILE
sed -i 's/\@end\ titlepage/\\maketitle \\titlepage/g' $FILE

echo Debug: Fixing table of contents.
sed -i 's/\@contents/\\tableofcontents/g' $FILE

echo Debug: Removing @ifnottex sections.
sed -i '/\@ifnottex/,/\@end\ ifnottex/d' $FILE

echo Debug: Transforming comments.
sed -i 's/@c\ /\%\ /g' $FILE

echo Debug: Transforming chapters, sections and subsections.
sed -i 's/\@chapter\ .*/\\chapter{&}/g' $FILE
sed -i 's/\@section\ .*/\\section{&}/g' $FILE
sed -i 's/\@subsection\ .*/\\subsection{&}/g' $FILE
sed -i 's/\@unnumbered\ .*/\\chapter{&}/g' $FILE

sed -i 's/\@chapter\ //g' $FILE
sed -i 's/\@section\ //g' $FILE
sed -i 's/\@subsection\ //g' $FILE
sed -i 's/\@unnumbered\ //g' $FILE

echo Debug: Removing nodes.
sed -i '/\@node/d' $FILE

echo Debug: Removing menus.
sed -i '/\@menu/,/\@end\ menu/d' $FILE

echo Debug: Removing @ignore blocks.
sed -i '/\@ignore/,/@end ignore/d' $FILE

echo Debug: Removing @iftex tags.
sed -i '/\@iftex/d' $FILE
sed -i '/\@end\ iftex/d' $FILE

echo Debug: Transforming italics, strong, var, dfn and code.
sed -i 's/\@i{/\\textit{/g' $FILE
sed -i 's/\@code{/\\texttt{/g' $FILE
sed -i 's/\@var{/\\textit{/g' $FILE
sed -i 's/\@dfn{/\\textit{/g' $FILE
sed -i 's/\@strong{/\\textbf{/g' $FILE

echo Debug: Transforming enumerations.
sed -i 's/\@enumerate/\\begin{enumerate}/g' $FILE
sed -i 's/\@end\ enumerate/\\end{enumerate}/g' $FILE

echo Debug: Transforming itemizes.
sed -i 's/\@itemize\ \@bullet/\\begin{itemize}/g' $FILE
sed -i 's/\@itemize/\\begin{itemize}/g' $FILE
sed -i 's/\@end\ itemize/\\end{itemize}/g' $FILE

sed -i 's/\@item/\\item/g' $FILE

#echo Debug: Transforming \@page
#sed -i 's/\@page/\\newpage/g' $FILE

echo Debug: Whitespace fix
sed -i 's/\@\ /\\ /g' $FILE

echo Debug: Newline fix
sed -i 's/\@\*/\\newline/g' $FILE

echo Debug: Transforming \@center
sed -i 's/\@center\ .*/\\centerline{\ &}/g' $FILE
sed -i 's/\@center\ //g' $FILE

echo Debug: Fixing underscores in lines not containing @math.
sed -i '/^\@math/! s/_/\\_/g' $FILE

echo Debug: Removing vskips and page breaks.
sed -i '/\@vskip/d' $FILE
sed -i '/\@page/d' $FILE

echo Debug: Transforming \@math.
sed -i 's/\@math{.*/\$&\$/g' $FILE
sed -i 's/\@math//g' $FILE

echo TODO: References.
sed -i 's/\@xref{/See\ \\textit{/g' $FILE
sed -i 's/\@ref{/\\textit{/g' $FILE

echo TODO: Images.
sed -i 's/\@image/\(image\)/g' $FILE

echo TODO: Index.
sed -i '/\@printindex/d' $FILE

echo Debug: Transforming \@command.
sed -i 's/\@command{/\\texttt{/g' $FILE

