BEG_G {
   graph_t sg = subg ($, "reachable");
   $tvtype=TV_bfs;
}
N { if (isSubnode(sg,$)) { color="green"; } }
E { 
	printf("%s -> %s\n", head.name, tail.name);
 }
