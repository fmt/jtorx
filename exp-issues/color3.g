BEG_G {
   graph_t sg = readG( "reachable.dot");
   $tvtype=TV_bfs;
}
N { if (isNode(sg,$.name)) { color="green"; style="filled"; } }
E {
    string hn = head.name;
    string tn = tail.name;
    if (isNode(sg,hn)) {
      // printf("head %s\n", hn);
      node_t h=node(sg,hn);
      if(isNode(sg,tn)) {
	// printf("%s => %s\n", hn, tn);
        node_t t = node(sg,tn);
	if(isEdge_sg(sg,t,h,"")) {
		// printf("%s -> %s\n", hn, tn);
		color="green";
	}
     }
    }
 }
