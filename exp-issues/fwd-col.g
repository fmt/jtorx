BEG_G {
   graph_t sg = subg ($, "reachable");
   $tvtype = TV_fwd;
   $tvroot = node($,ARGV[0]);
}

N {$tvroot = NULL; color="green"; subnode (sg, $); }

E { color="green"; }

END_G {
   induce (sg);
   write (sg);
}
