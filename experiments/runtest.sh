#!/bin/sh




simpletest() {
    ./evaluate Gen3_Bare_LazyOTF.jtorx licenseGenerator3Skel.sax 5 100 1 "liccov-lazyotfto;0;1" lazyotf
}

longertest() {
    ./evaluate Gen3_Bare_LazyOTF.jtorx licenseGenerator3Skel.sax 5 1000000 1 "liccov-lazyotfto;0;2" lazyotf 7 7 10
}

longertestwithboundadj() {
    ./evaluate Gen3_Bare_LazyOTF.jtorx licenseGenerator3Skel.sax 5 1000000 1 "liccov-lazyotfto;0;2" lazyotf 1 7 10
}


longertestwithboundadj




echo -- Tar files:
ls *.tar
echo -- -\> surgeonsrepo
mkdir -p surgeonsrepo
mv *.tar surgeonsrepo
