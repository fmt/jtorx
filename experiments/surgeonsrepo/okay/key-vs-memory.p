set xrange [-1:3]
set yrange [0:354302]
set xtics 1.0
set xlabel 'Maximum Model Traversal Depth'
set ylabel 'VmHWM (kB)'
set key box
set style data histogram
set style histogram rowstacked
set style fill solid
set boxwidth 0.4 absolute
set grid
set style fill solid border -1
set style fill pattern 2
set key invert
set key left
plot '< sort -g key-vs-memory.dat' using 2 title 'Model_SymToSim',\
'' using 3 title 'Model_treeSolver',\
'' using 4 title 'Adapter_SymToSim',\
'' using 5 title 'Adapter_treeSolver',\
'' using 6:xtic(1) title 'JTorX'
