/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iocochecker;

import iocochecker.Main.SeveralInitialStatesException;
import iocochecker.Main.TransitionException;
import iolts.IOLTS;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Lars Frantzen
 */
public class AutImporter {

    protected static IOLTS generateIOLTSFromAut(String filename) throws AutImportException {
        IOLTS iolts = new IOLTS();
        Scanner s = null;
        int firstState;
        int numTransitions;
        int numStates;

        try {
            s = new Scanner(new BufferedReader(new FileReader(new File(filename))));
            s.useDelimiter("\\s*,\\s*|\\s*\\(\\s*|\\s*\\)\\s*");

            s.next();
            firstState = s.nextInt();
            numTransitions = s.nextInt();
            numStates = s.nextInt();
            System.out.println("First state: " + firstState);
            System.out.println("Number of transitions: " + numTransitions);
            System.out.println("Number of states: " + numStates);

            addStates(iolts, numStates, firstState);
            
            while (s.hasNext()) {
                s.next();
                String fromID = s.next();
                String label = s.next();
                if (label.equals("i"))
                    label = "tau";
                String toID = s.next();
                Main.addTransition(iolts, fromID, toID, label);
                numTransitions--;
            }
        } catch (SeveralInitialStatesException ex) {
            throw new AutImportException(ex.getMessage());
        } catch (TransitionException ex) {
            throw new AutImportException(ex.getMessage());
        } catch (Exception ex) {
            throw new AutImportException(ex.getClass().toString());
        } finally {
            if (s != null) {
                s.close();
            }
        }

        if (numTransitions != 0) {
            throw new AutImportException("Wrong number of transitions in .aut file!");
        }

        return iolts;
    }

    private static void addStates(IOLTS iolts, int numStates, int initialState) throws SeveralInitialStatesException {
        for (int i = 0; i < numStates; i++) {
            Main.addState(iolts, Integer.toString(i), Integer.toString(i), i == initialState ? true : false);
        }
    }

    public static class AutImportException extends Exception {

        AutImportException(String text) {
            super(text);
        }
    }
}
