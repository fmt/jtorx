/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iocochecker;

import iocochecker.AutImporter.AutImportException;
import iocochecker.YEdImporter.YEdImportException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import iolts.Action;
import iolts.IOLTS;
import iolts.IOLTSAlgorithms;
import iolts.InputAction;
import iolts.OutputAction;
import iolts.State;
import iolts.Transition;
import iolts.UnobservableAction;
import java.awt.Component;
import javax.swing.JOptionPane;
import org.xml.sax.SAXException;

/**
 *
 * @author Lars Frantzen
 */
public class Main {

    static UnobservableAction tau;
    static Set<Action> actions;

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new MainFrame().setVisible(true);
            }
        });

        tau = new UnobservableAction();
        actions = new HashSet<Action>();
    }

    protected static Set<FailureSituation> startCheck(Component component, String specPath, String implPath, int traces)
            throws NoInitialStateException, AutImportException, YEdImportException {

        System.out.println("Assembling specification:");
        IOLTS spec = generateIOLTS(specPath);
        System.out.println("\nAssembling implementation:");
        IOLTS impl = generateIOLTS(implPath);;
        try {
            checkInputEnabled(spec.getInputActions(), impl);
        } catch (NotInputEnabledException ex) {
            JOptionPane.showMessageDialog(component, ex.getMessage(), "Not Input-Enabled Exception", JOptionPane.WARNING_MESSAGE);
        }
        
        System.out.println();
        IocoChecker ic = new IocoChecker(spec, impl, traces);
        return ic.doCheck();
    }

    private static void checkInputEnabled(Set<InputAction> inputs, IOLTS iolts) throws NotInputEnabledException {
        Set<State> bg = IOLTSAlgorithms.getNonInputEnabledStates(inputs, iolts.getStates());
        if (bg.size() != 0) {

            IOLTSAlgorithms.makeStatesInputEnabled(inputs, bg);

            throw new NotInputEnabledException("The implementation states " +
                    bg + " are not input enabled!\niocoChecker added" +
                    " self-loop transitions to achieve input-enabledness.");
        }
    }

    private static void checkInitialStateSet(IOLTS iolts) throws NoInitialStateException {
        if (iolts.getInitialState() == null) {
            throw new NoInitialStateException("No initial state defined!");
        }
    }

    protected static void addState(IOLTS iolts, String id, String label, boolean initial)
            throws SeveralInitialStatesException {
        State s = new State(id, label);
        iolts.addState(s);
        System.out.println("Added state " + s);

        if (initial) {
            if (iolts.getInitialState() == null) {
                iolts.setInitialState(s);
                System.out.println("(initial)");
            } else {
                throw new SeveralInitialStatesException("More than one initial state defined!");
            }
        }
    }

    protected static void addTransition(IOLTS iolts, String from, String to, String label)
            throws TransitionException {
        State target = null;
        State source = null;

        try {
            target = iolts.getStateByID(to);
            source = iolts.getStateByID(from);
        } catch (Exception ex) {
            throw new TransitionException(ex.getMessage());
        }

        Action act = null;
        if (label.equals("tau")) {
            act = tau;
        } else {
            char kind = label.charAt(0);
            String rLabel = label.substring(1);
            if (kind == '?') {
                act = new InputAction(rLabel);
            } else if (kind == '!') {
                act = new OutputAction(rLabel);
            } else {
                throw new TransitionException("Transition label must be \"?labelname\" (input)," +
                        " \"!labelname\" (output), or \"tau\" (unobservable)\n" +
                        "Check transition from state " + source + " to state " + target +
                        " with label " + label);
            }
        }

        if (!(act instanceof UnobservableAction)) {
            act = actionKnown(act);
            iolts.addAction(act);
        }
        Transition t = new Transition(act, target);
        source.addOutgoingTransition(t);

        System.out.println("Added transition from state " + source + " with " + t.toString());
    }

    private static Action actionKnown(Action act) {
        for (Iterator it = actions.iterator(); it.hasNext();) {
            Action a = (Action) it.next();
            if (a.equals(act)) {
                return a;
            }
        }
        actions.add(act);
        return act;
    }

    private static IOLTS generateIOLTS(String path) throws NoInitialStateException, AutImportException, YEdImportException {
        String extension = path.substring(path.lastIndexOf('.') + 1);
        IOLTS iolts = null;
        if (extension.toLowerCase().equals("graphml")) {
            iolts = YEdImporter.generateIOLTSFromyEDGraphML(path);
        } else if (extension.toLowerCase().equals("aut")) {
            iolts = AutImporter.generateIOLTSFromAut(path);
        } else {
            System.err.println("Unknown file extension " + extension);
            System.exit(1);
        }
        checkInitialStateSet(iolts);

        return iolts;
    }

    public static class SeveralInitialStatesException extends SAXException {

        SeveralInitialStatesException(String text) {
            super(text);
        }
    }

    public static class TransitionException extends SAXException {

        TransitionException(String text) {
            super(text);
        }
    }

    public static class NoInitialStateException extends Exception {

        NoInitialStateException(String text) {
            super(text);
        }
    }

    public static class NotInputEnabledException extends Exception {

        NotInputEnabledException(String text) {
            super(text);
        }
    }
}
