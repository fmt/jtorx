/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iocochecker;

import iolts.IOLTS;
import java.io.File;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Lars Frantzen
 */
public class YEdImporter {

    protected static IOLTS generateIOLTSFromyEDGraphML(String filename) throws YEdImportException {
        final IOLTS iolts = new IOLTS();

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser;

        DefaultHandler handler = new DefaultHandler() {

            boolean nodeLabel = false;
            boolean edgeLabel = false;
            String nodeID;
            String from;
            String to;

            @Override
            public void startElement(String uri, String localName,
                    String qName, Attributes attributes)
                    throws SAXException {
                if (qName.equals("node")) {
                    nodeID = attributes.getValue("id");
                }
                if (qName.equals("y:NodeLabel")) {
                    nodeLabel = true;
                }
                if (qName.equals("edge")) {
                    from = attributes.getValue("source");
                    to = attributes.getValue("target");
                }
                if (qName.equals("y:EdgeLabel")) {
                    edgeLabel = true;
                }
            }

            @Override
            public void characters(char ch[], int start, int length)
                    throws SAXException {
                if (nodeLabel) {
                    String label = new String(ch, start, length);
                    Main.addState(iolts, nodeID, label, label.equals("1") ? true : false);
                    nodeLabel = false;
                }
                if (edgeLabel) {
                    Main.addTransition(iolts, from, to, new String(ch, start, length));
                    edgeLabel = false;
                }
            }
        };
        try {
            saxParser = factory.newSAXParser();
            saxParser.parse(new File(filename), handler);
        } catch (Exception ex) {
            throw new YEdImportException(ex.getMessage());
        }

        return iolts;
    }

    public static class YEdImportException extends Exception {

        YEdImportException(String text) {
            super(text);
        }
    }
}
