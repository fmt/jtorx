NOTE: this is a MODIFIED version of Kevin Walzer's mactoolbar-1.1 package.

changes:
 - portability: protect accesses to
      tk::unsupported::MacWindowStyle
   with a test for windowingsystem aqua
 - robustness fix: pack toolbar frame before other items,
   to avoid resizing issue (make sure other items are shrunk
    before the toolbar is shrunk, when window size is reduced)
 - added proc getframe that gives access to toolbar frame
   (because we misuse the toolbar to pack additonal stuff in it)

Axel.Belinfante@cs.utwente.nl


Kevin's text follows below unchanged, below the line:
==========================================================================================


The Mactoolbar Package 

The mactoolbar package is designed to emulate the standard Cocoa toolbar provided on OS X. On the Mac, toolbars behave a bit differently than they do on Windows or X11; rather than having the entire button darken when pressed, only the image and/or text darkens, and the toolbar itself can be hidden or made visible with a button-click. As such, the usual Tk approach to toolbars (arranging Tk or Tile buttons in a frame) requires a bit of customization. The mactoolbar package uses a Tile frame, labels, and bindings to implement a Cocoa-style toolbar. 

Commands

The mactoolbar package is called by the invocation package require mactoolbar, and defines several commands: 

* mactoolbar::toolbar {w}: This command creates the basic toolbar frame, and takes a widget name as an argument.

* mactoolbar::add {child img cmd help}: This command adds buttons to the toolbar, and takes four arguments: the name of the button, the name of the button's image, the name of the command bound to the button, and a help string. 

* mactoolbar::add_searchfield: This command adds a Mac-style search field to the toolbar.

* mactoolbar::demo: This command loads a basic demo of the toolbar. 

Using the Mactoolbar Package

The toolbar should be a child widget of another frame. For instance, set up a container frame .f and then call mactoolbar::toolbar .f.toolbar. The toolbar will not toggle its visibility correctly otherwise.

The toolbar and its parent frame should be packed at the very top of the window. All other widgets in the window should be packed -side bottom. This will ensure that the toolbar will remain on top when its visibility is toggled.

Toolbutton images should come in two forms using this naming scheme: image and image-dark. The mactoolbar package displays the dark image when the button is pushed, and the standard image when the button is raised. Any valid Tk image format (.gif, .png, .jpeg, etc.) can be displayed. Using a photo editor to adjust the color of the original image is a good way to create the darker image.

The mactoolbar package requires Tile to be installed. 

License 

The mactoolbar package is licensed under the same license as Tcl/Tk itself. See http://www.tcl.tk/software/tcltk/license.html for the current license. 
