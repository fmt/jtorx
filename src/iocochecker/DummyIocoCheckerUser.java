package iocochecker;

public class DummyIocoCheckerUser implements IocoCheckerUser {

	public boolean hasStopRequest() {
		return false;
	}

	public void progressReportSituationAdded() {}

	public void progressReportSituationProcessed() {}

	public void reportFailure(FailureSituation f) {}

	public void progressReportCheckingFinished() {}
}
