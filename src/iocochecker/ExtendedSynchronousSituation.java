/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iocochecker;

import iolts.Action;
import iolts.State;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Lars Frantzen
 */
public class ExtendedSynchronousSituation extends SynchronousSituation {

    private List<Action> suspensionTrace;

    public ExtendedSynchronousSituation(Set<State> specSituation, Set<State> implSituation, List<Action> suspensionTrace) {
        super(specSituation, implSituation);
        this.suspensionTrace = suspensionTrace;
    }

    public ExtendedSynchronousSituation(SynchronousSituation syn, List<Action> suspensionTrace) {
        super(syn.getSpecSituation(), syn.getImplSituation());
        this.suspensionTrace = suspensionTrace;
    }

    public List<Action> getSuspensionTrace() {
        return suspensionTrace;
    }

    @Override
    public String toString() {
        return super.toString() + suspensionTrace;
    }
}
