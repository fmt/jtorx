/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iocochecker;

import iolts.Action;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Lars Frantzen
 */
public class FailureSituation {
	
    private Set<? extends Action> implOutputs;
    private Set<? extends Action> specOutputs;
    private List<Action> suspensionTrace;

    public FailureSituation(Set<? extends Action> implActions,
            Set<? extends Action> specActions,
            List<Action> suspensionTrace) {

        this.implOutputs = implActions;
        this.specOutputs = specActions;
        this.suspensionTrace = suspensionTrace;
    }

    public Set<? extends Action> getImplActions() {
        return implOutputs;
    }

    public Set<? extends Action> getSpecActions() {
        return specOutputs;
    }

    public List<Action> getSuspensionTrace() {
        return suspensionTrace;
    }

    @Override
    public String toString() {
        return '(' + getImplActions().toString() + ", " +
                getSpecActions().toString() + ')';
    }
}
