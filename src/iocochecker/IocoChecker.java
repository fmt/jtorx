/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iocochecker;

import iolts.Action;
import iolts.IOLTS;
import iolts.IOLTSAlgorithms;
import iolts.InputAction;
import iolts.OutputAction;
import iolts.State;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Lars Frantzen
 */
public class IocoChecker {

    private IOLTS spec;
    private IOLTS impl;
    private int traces;
    
	// refines is more general than ioco: IOLTS M1 refines M2
	// iff for all traces, in(M1) is a superset of in(M2) and
	// out(M1) is a subset of out(M2).
    private final boolean checkRefines;

    public IocoChecker(IOLTS spec, IOLTS impl, int traces, boolean checkRefines) {
        this.spec = spec;
        this.impl = impl;
        this.traces = traces;
        this.checkRefines = checkRefines;
    }
    
    protected Set<FailureSituation> doCheck() {
    	return doCheck(null);
    }

    protected Set<FailureSituation> doCheck(IocoCheckerUser user) {
    	if (user==null)
    		user = new DummyIocoCheckerUser();
        Set<State> specInitialStates = IOLTSAlgorithms.doInternalSteps(spec.getInitialState());
        Set<State> implInitialStates = IOLTSAlgorithms.doInternalSteps(impl.getInitialState());
        SynchronousSituation syn = new SynchronousSituation(specInitialStates, implInitialStates);
        user.progressReportSituationAdded();
        return walkTheStraightLine(syn, user);
    }

    private Set<FailureSituation> walkTheStraightLine(SynchronousSituation s, IocoCheckerUser user) {
        Set<FailureSituation> failures = new HashSet<FailureSituation>();
        Set<SynchronousSituation> visited = new HashSet<SynchronousSituation>();
        Set<ExtendedSynchronousSituation> currentSyns = new HashSet<ExtendedSynchronousSituation>();
        currentSyns.add(new ExtendedSynchronousSituation(s, new ArrayList<Action>()));
        Set<ExtendedSynchronousSituation> newCurrentSyns;

        do {
            System.out.println("Current situations: " + currentSyns);
            visited.addAll(currentSyns);
            newCurrentSyns = new HashSet<ExtendedSynchronousSituation>();

            for (Iterator<ExtendedSynchronousSituation> it = currentSyns.iterator(); it.hasNext();) {
            	if (user.hasStopRequest())
            		break;
            	
                ExtendedSynchronousSituation syn = it.next();
                System.out.println("Proceeding with: " + syn);

                // check the outputs
                Set<OutputAction> implOutputs = IOLTSAlgorithms.out(syn.getImplSituation());
                Set<OutputAction> specOutputs = IOLTSAlgorithms.out(syn.getSpecSituation());
                if (!specOutputs.containsAll(implOutputs)) {
                	FailureSituation f = new FailureSituation(implOutputs, specOutputs, syn.getSuspensionTrace());
                    failures.add(f);
                    user.reportFailure(f);
                }

                // compute relevant input actions
                Set<InputAction> relevantInputActions = new HashSet<InputAction>();
                if (traces == 1) {
                    relevantInputActions.addAll(IOLTSAlgorithms.in(syn.getSpecSituation()));
                } else {
                    relevantInputActions.addAll(IOLTSAlgorithms.incap(syn.getSpecSituation()));
                }
                Set<Action> relevantActions = new HashSet<Action>(relevantInputActions);
                // compute relevant output actions
                Set<OutputAction> specOutputsCopy = new HashSet<OutputAction>(specOutputs);
                specOutputsCopy.retainAll(implOutputs);
                relevantActions.addAll(specOutputsCopy);
                System.out.println("Possible actions: " + relevantActions);

                if(checkRefines) {
                	// check the inputs
                	Set<InputAction> implInputs = IOLTSAlgorithms.in(syn.getImplSituation());
                	if (!implInputs.containsAll(relevantInputActions)) {
                		FailureSituation f = new FailureSituation(implInputs, relevantInputActions, syn.getSuspensionTrace());
                		failures.add(f);
                		user.reportFailure(f);
                	}
                }
                
                // process each relevant action and compute new synchronous sitations
                for (Iterator it2 = relevantActions.iterator(); it2.hasNext();) {
                    Action act = (Action) it2.next();
                    System.out.println("I do action " + act);
                    Set<State> newSpecStates = IOLTSAlgorithms.after(syn.getSpecSituation(), act);
                    Set<State> newImplStates = IOLTSAlgorithms.after(syn.getImplSituation(), act);
                    List<Action> newSuspensionTrace = new ArrayList<Action>(syn.getSuspensionTrace());
                    newSuspensionTrace.add(act);
                    ExtendedSynchronousSituation nextSyn =
                            new ExtendedSynchronousSituation(newSpecStates, newImplStates, newSuspensionTrace);
                    if (!alreadySeen(visited, nextSyn)) {
                        newCurrentSyns.add(nextSyn);
                        user.progressReportSituationAdded();
                        System.out.println("This leads to the new sitation: " + nextSyn);
                    } else {
                        System.out.println("This leads to the already seen sitation: " + nextSyn);
                    }
                }
                user.progressReportSituationProcessed();
            }
            currentSyns = newCurrentSyns;

        } while (!currentSyns.isEmpty() && !user.hasStopRequest());

        return failures;
    }

    private boolean alreadySeen(Set<SynchronousSituation> visited, SynchronousSituation nextSyn) {
        for (Iterator it = visited.iterator(); it.hasNext();) {
            SynchronousSituation syn = (SynchronousSituation) it.next();
            if (syn.equals(nextSyn)) {
                return true;
            }
        }
        return false;
    }
}// This is the original, recursive algorithm which needs a global 'visited',
// 'failures', and 'suspensionTrace':
//    private void walkTheLine(SynchronousSituation syn) {
//        System.out.println("I am here: " + syn + ", suspension trace: " + suspensionTrace);
//
//        // check the outputs
//        Set<OutputAction> implOutputs = IOLTSAlgorithms.out(syn.getImplSituation());
//        Set<OutputAction> specOutputs = IOLTSAlgorithms.out(syn.getSpecSituation());
//        if (!specOutputs.containsAll(implOutputs)) {
//            failures.add(new FailureSituation(implOutputs, specOutputs,
//                    ((List) ((ArrayList) suspensionTrace).clone())));
//        }
//
//        visited.add(syn);
//        Set<Action> relevantActions = new HashSet<Action>();
//        if (traces == 1) {
//            relevantActions.addAll(IOLTSAlgorithms.in(syn.getSpecSituation()));
//        } else {
//            relevantActions.addAll(IOLTSAlgorithms.incap(syn.getSpecSituation()));
//        }
//
//        /* We need a copy here since the original specOutputs are possibly stored
//         * in the FailureSituation */
//        Set<OutputAction> specOutputsCopy = new HashSet<OutputAction>(specOutputs);
//        specOutputsCopy.retainAll(implOutputs);
//        relevantActions.addAll(specOutputsCopy);
//        System.out.println("Possible actions: " + relevantActions);
//
//        for (Iterator it = relevantActions.iterator(); it.hasNext();) {
//            Action act = (Action) it.next();
//            Set<State> newSpecStates = IOLTSAlgorithms.after(syn.getSpecSituation(), act);
//            Set<State> newImplStates = IOLTSAlgorithms.after(syn.getImplSituation(), act);
//            SynchronousSituation nextSyn = new SynchronousSituation(newSpecStates, newImplStates);
//            if (!alreadySeen(nextSyn)) {
//                System.out.println("I do a " + act);
//                suspensionTrace.add(act);
//                walkTheLine(nextSyn);
//                System.out.println("Stepped back to situation " + syn);
//                suspensionTrace.remove(suspensionTrace.size() - 1);
//            } else {
//                System.out.println("Action " + act + " leads to no new situation.");
//            }
//        }
//    }
