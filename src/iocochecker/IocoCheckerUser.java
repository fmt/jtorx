package iocochecker;

/**
*
* @author Axel Belinfante
*/
public interface IocoCheckerUser {
	void reportFailure(FailureSituation f);
	boolean hasStopRequest();
	void progressReportSituationProcessed();
	void progressReportSituationAdded();
	void progressReportCheckingFinished();
}
