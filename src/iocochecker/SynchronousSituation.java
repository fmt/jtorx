/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iocochecker;

import java.util.Set;
import iolts.State;

/**
 *
 * @author Lars Frantzen
 */
public class SynchronousSituation {

    private Set<State> specSituation;
    private Set<State> implSituation;

    public SynchronousSituation(Set<State> specSituation, Set<State> implSituation) {
        this.specSituation = specSituation;
        this.implSituation = implSituation;
    }

    public Set<State> getSpecSituation() {
        return specSituation;
    }

    public Set<State> getImplSituation() {
        return implSituation;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof SynchronousSituation) {
         SynchronousSituation syn = (SynchronousSituation) o;
         return (syn.specSituation.equals(specSituation) && syn.implSituation.equals(implSituation));
        }
        return false;
    }
    
    @Override
    public String toString() {
        return '(' + specSituation.toString() + ',' + implSituation.toString() + ')'; 
    }
}
