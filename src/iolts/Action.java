/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iolts;

/**
 *
 * @author Lars Frantzen
 */
public abstract class Action {

    private String name;
    
    public Action() {       
    }
    
    public Action(String name) {
        this.name = name;
    }
    
    String getName() {
        return name;
    }
    
    @Override
    public String toString() {
        return "Action: " + name;
    }
}
