/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iolts;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Lars Frantzen
 */
public class IOLTS {

    private Set<State> states;
    private State initialState;
    private Set<InputAction> inputs;
    private Set<OutputAction> outputs;

    public IOLTS() {
        states = new HashSet<State>();
        inputs = new HashSet<InputAction>();
        outputs = new HashSet<OutputAction>();
        initialState = null;
    }

    public void addAction(Action act) {
        if (act instanceof InputAction)
            addInput((InputAction) act);
        else
            addOutput((OutputAction) act);
    }

    public void addInput(InputAction i) {
        inputs.add(i);
    }

    public void addOutput(OutputAction o) {
        outputs.add(o);
    }

    public void addState(State s) {
        states.add(s);
    }

    public State getStateByID(String id) throws Exception {
        
        for (Iterator it = states.iterator(); it.hasNext();) {
            State s = (State) it.next();
            if (s.getID().equals(id)) {
                return s;
            }
        }
        throw new Exception("State " + id +" not existent!");
    }

    public void setInitialState(State s) {
        initialState = s;
    }
    
    public State getInitialState() {
        return initialState;
    }

    public Set<State> getStates() {
        return states;
    }
    
    public Set<InputAction> getInputActions() {
        return inputs;
    }
}
