/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iolts;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Lars Frantzen
 * 
 * This is an utility class with the algorithms necessary to deal
 * with IOLTS.
 * 
 */
public class IOLTSAlgorithms {

    private static Quiescence quiescence = new Quiescence();

    public static Set<State> after(Set<State> states, Action act) {
        Set<State> result = new HashSet<State>();

        if (act == quiescence) {
            result = processQuiescence(states);
        } else {
            for (Iterator it = states.iterator(); it.hasNext();) {
                State s = (State) it.next();
                result.addAll(after(s, act));
            }
        }
        return result;
    }

    public static Set<State> after(State s, Action act) {
        Set<State> result = new HashSet<State>();

        for (Iterator i = s.getOutgoingTransitions().iterator(); i.hasNext();) {
            Transition next = (Transition) i.next();

            if (next.getAction() == act) {
                result.addAll(doInternalSteps(next.getTarget()));
            }
        }
        return result;
    }

    public static Set<OutputAction> out(Set<State> states) {
        Set<OutputAction> result = new HashSet<OutputAction>();

        for (Iterator it = states.iterator(); it.hasNext();) {
            State next = (State) it.next();
            if (next.quiescent()) {
                result.add(quiescence);
            } else {
                result.addAll(next.getOutputs());
            }
        }
        return result;
    }

    public static Set<InputAction> in(Set<State> states) {
        Set<InputAction> result = new HashSet<InputAction>();

        for (Iterator it = states.iterator(); it.hasNext();) {
            State next = (State) it.next();
            result.addAll(next.getInputs());
        }
        return result;
    }

    public static Set<InputAction> incap(Set<State> states) {
        Set<InputAction> result = new HashSet<InputAction>();

        boolean firstState = true;
        for (Iterator it = states.iterator(); it.hasNext();) {
            State next = (State) it.next();

            /* If a state has an outgoing tau-transition, we can ignore
             * this state for the computation of the inputs which are
             * enabled at all the given states.
             */
            if (!next.hasTauTransition()) {
                if (firstState) {
                    result.addAll(next.getInputs());
                    firstState = false;
                } else {
                    result.retainAll(next.getInputs());
                }
                if (result.isEmpty()) {
                    break;
                }
            }
        }
        return result;
    }

    public static Set<State> doInternalSteps(State s) {
        Set<State> result = new HashSet<State>();
        boolean onlyTaus = true;

        for (Iterator i = s.getOutgoingTransitions().iterator(); i.hasNext();) {
            Transition next = (Transition) i.next();
            if (next.getAction() instanceof UnobservableAction) {
                State nextS = next.getTarget();
                result.addAll(doInternalSteps(nextS));
            } else {
                onlyTaus = false;
            }
        }

        if (!onlyTaus || s.getOutgoingTransitions().size() == 0) {
            result.add(s);
        }

        return result;
    }

    private static Set<State> processQuiescence(Set<State> states) {

        Set<State> quiescentStates = new HashSet<State>();

        for (Iterator i = states.iterator(); i.hasNext();) {
            State s = (State) i.next();
            if (s.quiescent()) {
                quiescentStates.add(s);
            }
        }

        return quiescentStates;
    }

    public static Set<State> getNonInputEnabledStates(Set<InputAction> inputs, Set<State> states) {
        HashSet<State> badGuys = new HashSet<State>();

        for (Iterator<State> it = states.iterator(); it.hasNext();) {
            State state = it.next();
            if (!state.hasTauTransition()) {
                if (!inputs.equals(state.getInputs())) {
                    badGuys.add(state);
                }
            }
        }

        return badGuys;
    }

    public static void makeStatesInputEnabled(Set<InputAction> inputs, Set<State> states) {
        HashSet<InputAction> missingInputs;
        
        for (Iterator<State> it = states.iterator(); it.hasNext();) {
            State state = it.next();

            missingInputs = new HashSet<InputAction>(inputs);
            missingInputs.removeAll(state.getInputs());

            for (Iterator<InputAction> it2 = missingInputs.iterator(); it2.hasNext();) {
                InputAction inputAction = it2.next();
                Transition t = new Transition(inputAction, state);
                state.addOutgoingTransition(t);
                System.out.println("Added self-loop transition with " + t);
            }
        }
    }
}
