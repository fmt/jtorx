/*
 * Copyright (C) 2008 Lars Frantzen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
package iolts;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Lars Frantzen
 */
public class State {

    private String id;
    private String label;
    private Set<Transition> outgoingTransitions;

    public State(String id, String label) {
        this.id = id;
        this.label = label;
        outgoingTransitions = new HashSet<Transition>();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return label;
    }

    public void addOutgoingTransition(Transition t) {
        outgoingTransitions.add(t);
    }

    public Set<Transition> getOutgoingTransitions() {
        return outgoingTransitions;
    }

    public boolean quiescent() {
        for (Iterator it = outgoingTransitions.iterator(); it.hasNext();) {
            Transition t = (Transition) it.next();
            Action act = t.getAction();
            if (act instanceof OutputAction || act instanceof UnobservableAction) {
                return false;
            }
        }
        return true;
    }

    public boolean hasTauTransition() {
        for (Iterator it = outgoingTransitions.iterator(); it.hasNext();) {
            Transition t = (Transition) it.next();
            Action act = t.getAction();
            if (act instanceof UnobservableAction) {
                return true;
            }
        }
        return false;
    }

    public Set<OutputAction> getOutputs() {
        Set<OutputAction> result = new HashSet<OutputAction>();

        for (Iterator it = outgoingTransitions.iterator(); it.hasNext();) {
            Transition t = (Transition) it.next();
            Action act = t.getAction();
            if (act instanceof OutputAction) {
                result.add((OutputAction) act);
            }
        }
        return result;
    }

    public Set<InputAction> getInputs() {
        Set<InputAction> result = new HashSet<InputAction>();

        for (Iterator it = outgoingTransitions.iterator(); it.hasNext();) {
            Transition t = (Transition) it.next();
            Action act = t.getAction();
            if (act instanceof InputAction) {
                result.add((InputAction) act);
            }
        }
        return result;
    }
}
