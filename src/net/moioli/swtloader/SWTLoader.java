/***************************************************************************
 * SWT Autoloader - everything in a jar                                    *
 * Copyright (C) 2005 by Silvio Moioli                                     *
 * silvio@moioli.net                                                       * 
 *                                                                         *
 * A brief tutorial on how to use this class is provided in the readme     *
 * file included in the distribution package.                              *
 * Use of this software is subject to the terms in the LICENSE.txt file    *
 ***************************************************************************/
package net.moioli.swtloader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.*;
import java.io.InputStream;
import java.net.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.*;

import org.eclipse.swt.widgets.Display;

/**
 * 
 * Enables SWT auto-loading from the jar file. A brief tutorial on how to use
 * this class is provided in the readme file included in the distribution
 * package.
 * 
 * @author Silvio Moioli
 * @version 1.0
 */
public class SWTLoader {

	private File SWTDir = null;

	private File jarFile = null;

	/**
	 * Constructs a new loader.
	 * 
	 */
	public SWTLoader() {
		try {
			URL u = this.getClass().getProtectionDomain().getCodeSource()
					.getLocation();
			try{
			    this.jarFile = new File(URLDecoder.decode(u.getFile(), "UTF-8")).getAbsoluteFile();
			}
			catch(UnsupportedEncodingException cantHappen){
				//Never gets here
			}
			System.out.println(this.jarFile);
			this.load();
		} catch (LoadingException e) {
			System.out.println("Error while loading SWT. Traceback: ");
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Loads SWT (if it's not loaded already).
	 */
	private void load() throws LoadingException {
		try {
			if (this.isInstalled() == false) {
				this.extract();
				String[] command = {"java", "-Djava.library.path=" + this.getSWTDir(), "-jar", this.getJarFile().getName()};
				String[] environment = null;
				File workDir = this.getJarFile().getParentFile();
				System.out.println(workDir);
				Process p = Runtime.getRuntime().exec(command, environment, workDir);

				// Implementation to get the standard output, requires two VMs
				// running
				/*
				 * BufferedInputStream i = new
				 * BufferedInputStream(p.getInputStream()); while(true){ try{
				 * System.exit(p.exitValue()); }
				 * catch(IllegalThreadStateException e){ //Spawned VM is still
				 * running, wait some time and continue } if (i.available() !=
				 * 0){ System.out.write(i.read()); System.out.flush(); } }
				 */
				Runtime.getRuntime().exit(0);
			}
		} catch (IOException e) {
			throw new LoadingException(e);
		}
	}

	/**
	 * @return Returns the jarFile.
	 */
	private File getJarFile() {
		return this.jarFile;
	}

	/**
	 * Creates a temporary directory for SWT unpacking.
	 * 
	 * @return the created directory
	 */
	private File getSWTDir() throws LoadingException {
		try {
			if (this.SWTDir == null) {
				// Create a temp dir if needed
				File result = File.createTempFile("swtLibs", "");
				if (!result.delete())
					throw new IOException();
				if (!result.mkdir())
					throw new IOException();
				this.SWTDir = result.getAbsoluteFile();
			}
			return this.SWTDir;
		} catch (IOException e) {
			throw new LoadingException(e);
		}
	}

	/**
	 * Extracts the SWT native files in a temporary directory.
	 */
	private void extract() throws LoadingException {
		try {
			File currentArchive = getJarFile();
			File outputDir = getSWTDir();

			ZipFile zf = new ZipFile(currentArchive);
			Enumeration<?extends ZipEntry> entries = zf.entries();

			while (entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();
				String pathname = entry.getName();
				if ((pathname.endsWith(".so") || pathname.endsWith(".dll") || pathname
						.endsWith(".jnilib"))) {
					InputStream in = zf.getInputStream(entry);
					File outFile = new File(outputDir, pathname);
					FileOutputStream out = new FileOutputStream(outFile);
					byte[] buf = new byte[1024];
					while (true) {
						int nRead = in.read(buf, 0, buf.length);
						if (nRead <= 0)
							break;
						out.write(buf, 0, nRead);
					}
					in.close();
					out.close();
				}
			}
			zf.close();
		} catch (ZipException e) {
			throw new LoadingException(e);
		} catch (IOException e) {
			throw new LoadingException(e);
		} catch (IllegalStateException e) {
			throw new LoadingException(e);
		}
	}

	/**
	 * Checks if SWT isn't already loaded.
	 * 
	 * @return true if SWT is loaded.
	 */
	private boolean isInstalled() {
		try {
			Display.getDefault().dispose();
		} catch (UnsatisfiedLinkError e) {
			return false;
		}
		return true;
	}
}
