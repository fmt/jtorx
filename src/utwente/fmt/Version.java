package utwente.fmt;

public class Version {

	static public String getVersionAndDate() {
		return theVersion+" ("+theDate+")";
	}
	static public String getVersion() {
		return theVersion+" ("+theDate+")";
	}
	static public String getDate() {
		return theDate;
	}
	static private String theVersion = "1.10.0-beta8";
	static private String theDate = "2012-09-24";

}
