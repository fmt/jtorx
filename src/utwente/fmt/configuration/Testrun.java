package utwente.fmt.configuration;

import java.util.concurrent.TimeUnit;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.InterpKind;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.testgui.RunItemData;
import utwente.fmt.lts.LTS;

public interface Testrun {
	public IOLTSInterpretation getInterpretation();
	public boolean useGuide();
	public boolean useCoverage();
	public boolean useModelInstantiator();
	public String getInstModelFilename();
	public boolean useImplInstantiator();
	public String getInstImplFilename();

	public String getModelFilename();
	public String getGuideFilename();
	public String getImplFilename();
	public RunItemData.LTSKind getGuideKind();
	
	public RunItemData.AdapterKind getImplKind();
	public TraceKind getTraceKind();

	public int getTimeoutValue();
	public TimeUnit getTimeoutUnit();
	
	public LTS getSelectedLTS();
	public LTS getCombinedLTS();

	public boolean useImplRealTime();
	public boolean getStripAddLabelAnno();
	public InterpKind getInterpKind();
	
	public boolean getReconDelta();
	public boolean getSynthDelta();
	public boolean getAngelicCompletion();
	public boolean getDivergence();
	
	public String getInterpInputs();
	public String getInterpOutputs();
	
	public String getSeed();
	public void setSeed(String s);
	public String getSimSeed();
	public void setSimSeed(String s);
	
	public boolean vizModel();
	public boolean vizMsc();
	public boolean vizRun();
	public boolean vizLogPane();
	public boolean vizSuspAutom();
	public boolean vizGuide();
	public boolean vizImpl();
	public boolean vizCoverage();

	public boolean isLazyOTFEnabled();
	public LazyOTFConfig getLazyOTFConfig();

}
