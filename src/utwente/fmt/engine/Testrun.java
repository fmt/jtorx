package utwente.fmt.engine;

import utwente.fmt.jtorx.logger.coverage.CoverageData;
import utwente.fmt.jtorx.testgui.RunSessionData;
import utwente.fmt.jtorx.torx.ExtendedModel;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.test.Verdict;


public interface Testrun {
	
	public class RunState {
        public utwente.fmt.jtorx.torx.OnLineTestingDriver myDriver;
    	public ExtendedModel myModel;
    	public Verdict verdict;
    	public Thread testStartThread = null;

	}
	
	public void addStatusMsg(String s);
	public void runSetup();
	public void runBegins();
	public void abortTestrun();
	public void stopTestrun();
	
	public void addLogger(boolean inWindow, String t);
	public void addCoverageLogger(CoverageData c);
	
	public void showMenu();
	public void showVerdict(Verdict v);
	public void prepareLogMuxForSession();
	public void startLogMuxSession(RunSessionData rd);
	
	public void showCoverage(CoverageData d);
	
	public ProgressReporter getStateSpaceProgressReporter();

	
	public long parseSeed(String s, String e);
	public long initSeed(String s);
	public long initSimSeed(String s, long l);
}
