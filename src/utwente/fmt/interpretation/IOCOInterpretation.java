package utwente.fmt.interpretation;

import utwente.fmt.lts.LabelConstraint;

public interface IOCOInterpretation extends IOLTSInterpretation {
	LabelConstraint isQuiescence();
}
