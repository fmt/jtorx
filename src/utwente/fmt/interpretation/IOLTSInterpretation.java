package utwente.fmt.interpretation;

import utwente.fmt.lts.LabelConstraint;

public interface IOLTSInterpretation extends LTSInterpretation {
	LabelConstraint isInput();
	LabelConstraint isOutput();
}
