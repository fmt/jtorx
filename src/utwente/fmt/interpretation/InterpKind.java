package utwente.fmt.interpretation;

public enum InterpKind {
	POSTFIX,
	PREFIX,
	ACTIONLIST,
	HARDCODED,
}
