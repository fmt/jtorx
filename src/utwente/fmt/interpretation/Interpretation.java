package utwente.fmt.interpretation;

import utwente.fmt.lts.LabelConstraint;

public interface Interpretation {
	LabelConstraint isAny();
}
