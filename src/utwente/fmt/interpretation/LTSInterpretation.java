package utwente.fmt.interpretation;

import utwente.fmt.lts.LabelConstraint;

public interface LTSInterpretation extends Interpretation {
	LabelConstraint isInternal();
}
