package utwente.fmt.jtorx.engine;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import java.util.logging.Logger;

import utwente.fmt.engine.Testrun.RunState;
import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.InterpKind;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfigurationHooks;
import utwente.fmt.jtorx.lazyotf.LazyOTFPolicyFactory;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriverFactory;
import utwente.fmt.jtorx.lazyotf.policy.LazyOTFPolicyFactoryImpl;
import utwente.fmt.jtorx.lazyotf.remoteimpl.RemoteLazyOTF;
import utwente.fmt.jtorx.logger.coverage.CoverageData;
import utwente.fmt.jtorx.logger.coverage.CoverageFactory;
import utwente.fmt.jtorx.testgui.QuiescenceInterpretation;
import utwente.fmt.jtorx.testgui.RollCall;
import utwente.fmt.jtorx.testgui.RunItemData;
import utwente.fmt.jtorx.testgui.RunItemData.AdapterKind;
import utwente.fmt.jtorx.testgui.RunSessionData;
import utwente.fmt.jtorx.testgui.StateStatHandler;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.SimOnLineTestingDriver;
import utwente.fmt.jtorx.torx.adapter.iocolts.IocoAdapterSimImpl;
import utwente.fmt.jtorx.torx.adapter.log.LogAdapter;
import utwente.fmt.jtorx.torx.adapter.lts.AdapterSimImpl;
import utwente.fmt.jtorx.torx.adapter.stdinout.PerformanceMeasuringStdInOutAdapterWrapper;
import utwente.fmt.jtorx.torx.adapter.stdinout.TimedStdInOutAdapter;
import utwente.fmt.jtorx.torx.adapter.tcplabels.TcpClientLabelAdapter;
import utwente.fmt.jtorx.torx.adapter.tcplabels.TcpServerLabelAdapter;
import utwente.fmt.jtorx.torx.adapter.torx.AdapterImpl;
import utwente.fmt.jtorx.torx.coverage.CoverageGuide;
import utwente.fmt.jtorx.torx.driver.ExtendedCompoundSimOnLineTestingDriver;
import utwente.fmt.jtorx.torx.explorer.guide.GuidanceLTS;
import utwente.fmt.jtorx.torx.explorer.torx.TorxExplorer;
import utwente.fmt.jtorx.torx.guided.GuidedLTS;
import utwente.fmt.jtorx.torx.model.ExtendedModelImpl;
import utwente.fmt.jtorx.torx.primer.Primer;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.ui.StepHighlighter;
import utwente.fmt.jtorx.writers.AniDotExtendedLTSWriter;
import utwente.fmt.jtorx.writers.AniDotLTSWriter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.LTS.LTSException;

public class Engine {
	private static final Logger LOGGER = Logger.getLogger(Engine.class.getName());
	
	private RollCall missing = new RollCall();
	
	private IOLTSInterpretation quiescenceInterp =  null;


	
	public static String titleOf(File f) {
		return f.getName().split("\\.",2)[0];
	}


	
	public void startTestRun(final utwente.fmt.configuration.Testrun cfg,
			                 final utwente.fmt.engine.Testrun run,
			                 final RunState runState,
			                 final RunSessionData runData,
			                 final RunItemData modelData,
			                 final RunItemData modelPrimerData,
			                 final RunItemData guideData,
			                 final RunItemData guidePrimerData,
			                 final RunItemData combinatorData,
			                 final RunItemData implData,
			                 final ErrorReporter err,
			                 final ProgressReporter progress,
			                 final StateStatHandler stateStats,
			                 final StepHighlighter highlighter ) {

		run.addStatusMsg("-- new test run --");
		run.addStatusMsg("");
		run.prepareLogMuxForSession();
		err.log(System.currentTimeMillis(), "START", "engine started");
		
		missing.reset();
		
		run.runSetup();

		final IOLTSInterpretation interp = cfg.getInterpretation();
		final boolean useGuide = cfg.useGuide();
		final boolean useCoverage = cfg.useCoverage();
		final boolean useModelInstantiator = cfg.useModelInstantiator();
		final String cfgInstModelFileName = cfg.getInstModelFilename();
		final boolean useImplInstantiator = cfg.useImplInstantiator();
		final String cfgInstImplFileName = cfg.getInstImplFilename();

		final String cfgModelFilename = cfg.getModelFilename();
		final String cfgGuideFilename = cfg.getGuideFilename();
		final String cfgImplFilename = cfg.getImplFilename();
		final RunItemData.LTSKind guideKind = cfg.getGuideKind();
		LTS tmpIocoCheckerTestPurpose = null;
		final RunItemData.AdapterKind implKind = cfg.getImplKind();
		final TraceKind traceKind = cfg.getTraceKind();

		final int timeoutValue = cfg.getTimeoutValue();
		final TimeUnit unitValue = cfg.getTimeoutUnit();
		//if (implKind == RunItemData.AdapterKind.STDINOUTADAPTER ||
		//		implKind == RunItemData.AdapterKind.TCPCLIENTLABELADAPTER ||
		//		implKind == RunItemData.AdapterKind.TCPSERVERLABELADAPTER) {


		//}

		missing.check(cfgModelFilename.equals(""), "model");
		if (cfg.useGuide() && (guideKind==RunItemData.LTSKind.MODEL || guideKind==RunItemData.LTSKind.TRACE))
			missing.check(cfgGuideFilename.equals(""), "guide");
		if (cfg.useGuide() && guideKind == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE) {
			tmpIocoCheckerTestPurpose = cfg.getSelectedLTS();
			missing.check(tmpIocoCheckerTestPurpose==null, "iocoChecker-trace-as-guide");
		}
		if (cfg.useGuide() && guideKind == RunItemData.LTSKind.IOCOCHECKERMULTITRACE) {
			tmpIocoCheckerTestPurpose = cfg.getCombinedLTS();
			missing.check(tmpIocoCheckerTestPurpose==null, "iocoChecker-traces-as-guide");
		}
		final LTS iocoCheckerTestPurpose = tmpIocoCheckerTestPurpose;

		missing.check(cfgImplFilename.equals(""), "implementation");
		missing.check(implKind == null, "implementation kind");
		missing.check(interp == null, "interpretation");
		missing.check(traceKind == null, "trace kind");
		if (implKind == RunItemData.AdapterKind.STDINOUTADAPTER ||
				implKind == RunItemData.AdapterKind.PERFMEASURINGSTDINOUTADAPTER ||
				implKind == RunItemData.AdapterKind.TCPCLIENTLABELADAPTER ||
				implKind == RunItemData.AdapterKind.TCPSERVERLABELADAPTER) {
			missing.check(timeoutValue < 0, "timeout-value");
			missing.check(unitValue == null, "timeout-unit");
		}
		int tmpImplPort = -1;
		String tmpImplHost = "";
		if ((implKind == RunItemData.AdapterKind.TCPCLIENTLABELADAPTER ||
				implKind == RunItemData.AdapterKind.TCPSERVERLABELADAPTER)
				&& !cfgImplFilename.equals("")) {
			// be liberal: map crazy (sequence of) separator(s) onto !
			String tmp = cfgImplFilename.replace(' ', '!');
			tmp = tmp.replace(':', '!');
			tmp = tmp.replaceFirst("!!*","!");
			String hostPlusPort[] = tmp.split("!",2);
			if (hostPlusPort.length == 2) {
				tmpImplHost = hostPlusPort[0];
				try {
					tmpImplPort = Integer.parseInt(hostPlusPort[1]);
				}
				catch(NumberFormatException nfe) {
					run.addStatusMsg("impl port number: value is not a number: "+hostPlusPort[1]);
				}
			}
			missing.check(tmpImplHost.equals(""), "implementation-host-name");
			missing.check(tmpImplPort < 0, "implementation-port-number");
		}
		final int implPort = tmpImplPort;
		final String implHost = tmpImplHost;

System.err.println("about to status missing");

		if (!missing.getMissing().equals("")) {
			run.addStatusMsg("specify"+missing.getMissing());
			run.abortTestrun();
			return;
		}

System.err.println("beyond status missing");

		final boolean implUsesModel = (implKind == RunItemData.AdapterKind.SIMADAPTER ||
		        implKind == RunItemData.AdapterKind.IOCOSIMADAPTER ||
				implKind == RunItemData.AdapterKind.LOGADAPTER);
		final boolean useImplRealTime = (implKind==RunItemData.AdapterKind.STDINOUTADAPTER && cfg.useImplRealTime());
		final boolean addRmLabelAnnoPfx = cfg.getStripAddLabelAnno() &&
		cfg.getInterpKind() == InterpKind.PREFIX;
		final boolean addRmLabelAnnoSfx = cfg.getStripAddLabelAnno() &&
		cfg.getInterpKind() == InterpKind.POSTFIX;
		final boolean vizCoverage = cfg.vizCoverage();
		final boolean vizGuide = cfg.vizGuide();

		run.addStatusMsg("");

		runData.reset();
		runData.setUseGuide(useGuide);
		runData.setUseModelInstantiator(useModelInstantiator);
		runData.setUseImplInstantiator(useImplInstantiator);
		runData.setUseImplRealTime(useImplRealTime);
		runData.setModelData(modelData);
		runData.setModelPrimerData(modelPrimerData);
		runData.setGuideData(guideData);
		runData.setGuidePrimerData(guidePrimerData);
		runData.setCombinatorData(combinatorData);
		runData.setImplData(implData);

		runData.setTimeoutValue(timeoutValue);
		runData.setTimeoutUnit(unitValue);

		runData.setAddRmLabelAnnoPfx(addRmLabelAnnoPfx);
		runData.setAddRmLabelAnnoSfx(addRmLabelAnnoSfx);

		runData.setInterpKind(cfg.getInterpKind());
		runData.setReconDelta(cfg.getReconDelta());
		runData.setSynthDelta(cfg.getSynthDelta());
		runData.setAngelicCompletion(cfg.getAngelicCompletion());
		runData.setDivergence(cfg.getDivergence());
		runData.setInterpInputs(cfg.getInterpInputs());
		runData.setInterpOutputs(cfg.getInterpOutputs());
		runData.setTraceKind(traceKind);
		
		runData.setSeed(run.parseSeed(cfg.getSeed(), "seed is not a number"));
		runData.setSimSeed(run.parseSeed(cfg.getSimSeed(), "sim seed is not a number"));

		runData.setVizModel(cfg.vizModel());
		runData.setVizMsc(cfg.vizMsc());
		runData.setVizRun(cfg.vizRun());
		runData.setVizLogPane(cfg.vizLogPane());
		runData.setVizSuspAut(cfg.vizSuspAutom());
		runData.setVizGuide(cfg.vizGuide());
		runData.setVizImpl(cfg.vizImpl());
		runData.setVizCoverage(cfg.vizCoverage());





		// How to deal with special case where particular TorX-explorer has Instantiator built-in?
		// always allow user to override and user external Instantiator
		// TODO extend torx-explorer protocol to allow 'user' to ask for properties
		//  - finite/infinite ?
		//  - known-to-be-symbolic ?
		//  - has built-in instantiator ?
		// e.g. by having comamand: p
		// and answer  P list of property names that apply

		String instModelFileName = "";
		if (useModelInstantiator && cfgInstModelFileName!=null)
			instModelFileName = cfgInstModelFileName;
		runData.setModelInstantiator(instModelFileName);

		String instImplFileName = "";
		if (useImplInstantiator && cfgInstImplFileName!=null)
			instImplFileName = cfgInstImplFileName;
		runData.setImplInstantiator(instImplFileName);

		modelData.endPreviousIfDifferent(cfgModelFilename, instModelFileName);
		modelData.setFilename(cfgModelFilename, instModelFileName);
		modelData.setLTSKind(RunItemData.LTSKind.MODEL);

		// TODO this needs more thought: should endPreviousIfDifferent
		// look not only at filename but also at kind???

		guideData.setLTSKind(guideKind);
		if (useGuide && (guideKind==RunItemData.LTSKind.MODEL || guideKind==RunItemData.LTSKind.TRACE)) {
			guideData.endPreviousIfDifferent(cfgGuideFilename, "");
			guideData.setFilename(cfgGuideFilename, "");
		} else if (useGuide && 
				(guideKind == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE ||
						guideKind == RunItemData.LTSKind.IOCOCHECKERMULTITRACE)) {
			guideData.setFilename("", "");
		} else {
			guideData.setFilename("", "");
		}
		implData.endPreviousIfDifferent(cfgImplFilename, instImplFileName);
		implData.setFilename(cfgImplFilename, instImplFileName);
		implData.setAdapterKind(implKind);
		implData.setLTSKind(RunItemData.LTSKind.MODEL);

		// TODO this explicit setting of env vars should not be necessary:
		// it should be implicitly done by the runData.setTimeoutValue/Unit calls
		String[] tmpEnvVars = null;
		if (runData.getTimeoutValue()!= -1 && runData.getTimeoutUnit()!=null){
			tmpEnvVars = new String[2];
			tmpEnvVars[0] = "TORXTIMEOUT="+runData.getTimeoutValue();
			tmpEnvVars[1] = "TORXTIMEUNIT="+runData.getTimeoutUnit().name();
			modelData.setEnvVars(tmpEnvVars);
			guideData.setEnvVars(tmpEnvVars);
			implData.setEnvVars(tmpEnvVars);
		};
		final String[] envVars = tmpEnvVars;

		if (! modelData.startInstantiatorIfNeeded(err, progress)) {
			run.abortTestrun();
			return;
		}
		
		LOGGER.fine("[progress] Reading the model LTS file if it has not been loaded yet.");
		if (! modelData.readLTSFileIfNeeded(err, progress)) {
			run.abortTestrun();
			return;
		}
		if (useGuide && (guideKind==RunItemData.LTSKind.MODEL) &&
				! guideData.readLTSFileIfNeeded(err, progress)) {
			run.abortTestrun();
			return;
		}
		if (useGuide && (guideKind==RunItemData.LTSKind.TRACE) &&
				! guideData.readLTSFileIfNeeded(err, progress)) {
			run.abortTestrun();
			return;
		}
		if (useGuide &&
				(guideKind == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE ||
						guideKind == RunItemData.LTSKind.IOCOCHECKERMULTITRACE))
			guideData.setLTS(iocoCheckerTestPurpose);
		if (useGuide && !guideData.containsEpsilon())
			guideData.setLTS(new GuidanceLTS(guideData.getLTS()));

		
		if (implUsesModel &&  ! implData.startInstantiatorIfNeeded(err, progress)) {
			run.abortTestrun();
			return;
		}
		LOGGER.fine("[progress] Reading the implementation LTS file if it has not been loaded yet.");
		if (implUsesModel && ! implData.readLTSFileIfNeeded(err, progress)){
			run.abortTestrun();
			return;
		}

System.err.println("about to init seed");

		runData.setSeed(run.initSeed(""+runData.getSeed()));
		cfg.setSeed(""+runData.getSeed());
		
		if (implData.getAdapterKind() == RunItemData.AdapterKind.SIMADAPTER) {
			runData.setSimSeed(run.initSimSeed(""+runData.getSimSeed(), runData.getSeed()));
			cfg.setSimSeed(""+runData.getSimSeed());
		}
		
		System.err.println("about to set highlighter");

		// menuPane.getGroup().setText("Current tester state gives:");


		runData.setHighlighter(highlighter);
		File modelFile = new File(modelData.getModelFilename());
		final File implFile = new File(implData.getModelFilename());
		runData.setTitle(titleOf(modelFile)+"-"+titleOf(implFile));
		runData.setModelTitle(titleOf(modelFile));
		runData.setImplTitle(titleOf(implFile));
		
		final boolean cacheAllModelStates = runData.getVizSuspAut();
		final boolean synthDelta = runData.getSynthDelta();
		final boolean angelicCompletion = runData.getAngelicCompletion();
		final boolean divergence = runData.getDivergence();
		final long simSeed = runData.getSimSeed();
		final long seed = runData.getSeed();
		final boolean doUseCoverage = vizCoverage || useCoverage;

		// do not access cfg.getWhatever() methods below in the new thread,
		// because it will result in:
		// Exception in thread "Testgui-startTestRun" org.eclipse.swt.SWTException: Invalid thread access
		runState.testStartThread = new Thread("Testgui-startTestRun") {
			
			public void run() {
				LOGGER.fine("[progress] Beginning to configure the test run.");
				CompoundLTS myPrimer = new Primer(modelData.getLTS(), interp, !synthDelta /*tracesOnly*/, cacheAllModelStates, traceKind, angelicCompletion, divergence, err, progress);
				runData.setModelLTS(myPrimer);
				runData.setModelWriter(new AniDotLTSWriter(modelData.getLTS(),  interp, run.getStateSpaceProgressReporter()));

                quiescenceInterp = new QuiescenceInterpretation(interp);
                CoverageData coverage = null;
                if(doUseCoverage) {
                        coverage = CoverageFactory.getCoverageData(modelData.getModelFilename(), myPrimer, cfgImplFilename);
                        run.showCoverage(coverage);
                        coverage.setErrorReporter(err);
                        try {
							coverage.setModel(modelData.getLTS(), myPrimer, quiescenceInterp);
						} catch (LTSException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
                } else {
                        // TODO put message in coverage pane that tells that it contains old data
                }

              Set<CompoundLTS> guides = new HashSet<CompoundLTS>();

                if (useCoverage) {
                        // create coverage guide based on primer
                        // add coverage guide to guides
                        CoverageGuide cg = new CoverageGuide(myPrimer, coverage, quiescenceInterp, err);
                        guides.add(cg);
                        runData.setVizGuide(false);
                }
                
    			if (useGuide) {
					// TODO make sure choice for always STRACES here is correct
					CompoundLTS myGuide = new Primer(guideData.getLTS(), quiescenceInterp, true /*tracesOnly*/, cacheAllModelStates, TraceKind.STRACES, false  /*no AngelicCompletion*/, false/*no divergence*/, err, progress);
					Set<CompoundLTS> specs = new HashSet<CompoundLTS>();
					specs.add(myPrimer);
					guides.add(myGuide);
					if (guideKind==RunItemData.LTSKind.MODEL || guideKind==RunItemData.LTSKind.TRACE) {
						File guideFile = new File(guideData.getModelFilename());
						runData.setGuideTitle(titleOf(guideFile));
					} else
						runData.setGuideTitle("IocoCheckerTrace");
					runData.setGuideLTS(myGuide);
					runData.setGuideWriter(new AniDotLTSWriter(guideData.getLTS(),  quiescenceInterp, run.getStateSpaceProgressReporter()));
					runData.setVizGuide(vizGuide);
				}

				if (guides.size() > 0) {
					Set<CompoundLTS> specs = new HashSet<CompoundLTS>();
					specs.add(myPrimer);
					CompoundLTS myGuidedPrimer = new GuidedLTS(specs, guides, interp, /*enableCache:*/ !useCoverage, err);
					runState.myModel = new ExtendedModelImpl(myGuidedPrimer, interp, err);
					runData.setSuspAutLTS(myGuidedPrimer);
					runData.setSuspAutWriter(new AniDotExtendedLTSWriter(myGuidedPrimer,  interp, run.getStateSpaceProgressReporter()));
				} else {
					runState.myModel = new ExtendedModelImpl(myPrimer, interp, err);
					runData.setSuspAutLTS(myPrimer);
					runData.setSuspAutWriter(new AniDotExtendedLTSWriter(myPrimer,  interp, run.getStateSpaceProgressReporter()));
					runData.setVizGuide(false);
				}
				
				runData.setSuspAutTitle(runData.getModelTitle());

				LazyOTFPolicyFactory lazyOTFPolicy = null;
				if(cfg.isLazyOTFEnabled() && modelData.getLTS() instanceof TorxExplorer) {
				    TorxExplorer te = (TorxExplorer)modelData.getLTS();
				    LazyOTFConfig lcfg = cfg.getLazyOTFConfig();
				    
				    LazyOTFConfig postHookCfg = LazyOTFConfigurationHooks.getInstance().applyHooks(lcfg);
					lazyOTFPolicy = new LazyOTFPolicyFactoryImpl(postHookCfg);				    
				    runData.setLazyOTFInstance(new RemoteLazyOTF(te.getIn(), te.getOut(),
										     postHookCfg,
										     lazyOTFPolicy));
				}

				//TODO: fix adapter hierarchy, such that we can use Adapter here
				SimAdapter myAdapter = null;
				if (implData.getAdapterKind() == RunItemData.AdapterKind.IOCOSIMADAPTER) {
					myAdapter = new IocoAdapterSimImpl(implData.getLTS(), interp, simSeed, !implData.containsDelta(), err, run.getStateSpaceProgressReporter());
					System.err.println("testgui: created Adapter");
					runState.myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, runState.myModel, myAdapter, seed);
					System.err.println("testgui: created Driver");
					runData.setImplTitle(titleOf(implFile));
					runData.setImplLTS(implData.getLTS());
					System.err.println("testgui: about to setImplWriter");
					runData.setImplWriter(new AniDotLTSWriter(implData.getLTS(),  interp, run.getStateSpaceProgressReporter()));
				} else if (implData.getAdapterKind() == RunItemData.AdapterKind.SIMADAPTER) {
                    myAdapter = new AdapterSimImpl(implData.getLTS(), interp, simSeed, ! implData.containsDelta());
                    System.err.println("testgui: created Adapter");
                    runState.myDriver = new ExtendedCompoundSimOnLineTestingDriver(err,runState.myModel, myAdapter, seed);
                    System.err.println("testgui: created Driver");
                    runData.setImplTitle(titleOf(implFile));
                    runData.setImplLTS(implData.getLTS());
                    System.err.println("testgui: about to setImplWriter");
                    runData.setImplWriter(new AniDotLTSWriter(implData.getLTS(),  interp, run.getStateSpaceProgressReporter()));
                }
				else if (implData.getAdapterKind() == RunItemData.AdapterKind.LOGADAPTER) {
					myAdapter = new LogAdapter(implData.getLTS(), interp, seed, true);
					runState.myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, runState.myModel, myAdapter, seed);
					runData.setImplTitle(titleOf(implFile));
					runData.setImplLTS(implData.getLTS());
					System.err.println("testgui: about to setImplWriter");
					runData.setImplWriter(new AniDotLTSWriter(implData.getLTS(),  interp, run.getStateSpaceProgressReporter()));
				} else if (implData.getAdapterKind() == RunItemData.AdapterKind.TORXADAPTER) {
					//String cmd[] = new String[1];
					//cmd[0] = implFile.getAbsoluteFile().getPath();
					//String dir = implFile.getAbsoluteFile().getParent();
					myAdapter = new AdapterImpl(err, implData.getModelFilename(), null, envVars, progress);
					runState.myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, runState.myModel, myAdapter, seed);
					runData.setVizImpl(false);
				} else if (implData.getAdapterKind() == RunItemData.AdapterKind.STDINOUTADAPTER
						|| implData.getAdapterKind() == RunItemData.AdapterKind.PERFMEASURINGSTDINOUTADAPTER) {
					TimedStdInOutAdapter stdioAdapter = new TimedStdInOutAdapter(err, implData.getModelFilename(), null, envVars, useImplRealTime, addRmLabelAnnoPfx, addRmLabelAnnoSfx, progress, timeoutValue, unitValue);

					if (implData.getAdapterKind() == AdapterKind.PERFMEASURINGSTDINOUTADAPTER) {
						myAdapter = new PerformanceMeasuringStdInOutAdapterWrapper(stdioAdapter);
					}
					else {
						myAdapter = stdioAdapter;
					}
					
					System.err.println("testgui: created Adapter");
					runState.myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, runState.myModel, myAdapter, seed);
					System.err.println("testgui: created Driver");
					runData.setVizImpl(false);
				} else if (implData.getAdapterKind() == RunItemData.AdapterKind.TCPCLIENTLABELADAPTER) {
					myAdapter = new TcpClientLabelAdapter(err, implHost, implPort, addRmLabelAnnoPfx, addRmLabelAnnoSfx,  progress, timeoutValue, unitValue);
					System.err.println("testgui: created Adapter");
					runState.myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, runState.myModel, myAdapter, seed);
					System.err.println("testgui: created Driver");
					runData.setVizImpl(false);
				} else if (implData.getAdapterKind() == RunItemData.AdapterKind.TCPSERVERLABELADAPTER) {
					myAdapter = new TcpServerLabelAdapter(err, implHost, implPort, addRmLabelAnnoPfx, addRmLabelAnnoSfx,  progress, timeoutValue, unitValue);
					System.err.println("testgui: created Adapter");
					runState.myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, runState.myModel, myAdapter, seed);
					System.err.println("testgui: created Driver");
					runData.setVizImpl(false);
				} else {
					System.err.println("testgui: adapter not set");
					run.abortTestrun();
					return;
				}

				if(cfg.isLazyOTFEnabled() && runData.getLazyOTFInstance() != null) {
				    runState.myDriver = LazyOTFAwareSimOnlineTestingDriverFactory.makeDriver((SimOnLineTestingDriver)runState.myDriver, 
				    		runData.getLazyOTFInstance(), 
				    		cfg.getLazyOTFConfig(),
				    		lazyOTFPolicy,
				    		interp);
				}

				System.err.println("testgui: about to execute myDriver.init");
				final DriverInitializationResult initResult = runState.myDriver.init();
				System.err.println("testgui: executed myDriver.init");
				runData.setInitResult(initResult);
				System.err.println("testgui: runData.setInitResult");

				stateStats.runBegins(initResult);
				System.err.println("testgui: stateStats.runBegins");
				
				run.addLogger(runData.getVizLogPane(), runData.getTitle());
				if (vizCoverage || useCoverage) {
					run.addCoverageLogger(coverage);
				} else {
					// TODO update text or so in pane (also suggested in comment above)
				}

				run.startLogMuxSession(runData);

				//display.syncExec (new Runnable () {
				//	public void run () {
						run.runBegins();

						System.err.println("testgui: testPanes runBegins");

						// early stop of test run in case of error verdict
						if (initResult.hasVerdict() && initResult.getVerdict().getString().equals("error")) {
							System.err.println("testgui: have initial error verdict; stopping test run");
							runState.verdict = initResult.getVerdict();
							run.showVerdict(runState.verdict);
							System.err.println("testgui: stopping test run verdict="+runState.verdict.getString());
							run.stopTestrun();
							return;
						}
						System.err.println("testgui: no init verdict");

						runState.verdict = null;
						run.showMenu();

						// now that we have initialized gui and visualiations,
						// check if we have another verdict, because now we can
						// visualize the reason for the verdict.
						if (initResult.hasVerdict()) {
							System.err.println("testgui: have initial verdict");
							runState.verdict = initResult.getVerdict();
							run.showVerdict(runState.verdict);
							System.err.println("testgui: stopping test run verdict="+runState.verdict.getString());
							run.stopTestrun();
						}
					//}
				//});
						LOGGER.fine("[progress] Done configuring the test run.");
			}
		};
		System.err.println("starting thread");
		runState.testStartThread.start();
		System.err.println("started thread");
	}
}
