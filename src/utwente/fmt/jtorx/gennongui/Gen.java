package utwente.fmt.jtorx.gennongui;


import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;


import utwente.fmt.jtorx.interpretation.HardcodedInterpretation;
import utwente.fmt.jtorx.reporting.StderrErrorReporter;
import utwente.fmt.jtorx.torx.explorer.aut.AutLTS;
import utwente.fmt.jtorx.torx.primer.Primer;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;




public class Gen {

	private static void printState(State s) {
		//System.out.println("state: " + s.getID()+" "+s.toString());
		System.out.println("state: " + s.getID());

	}
	private static void printTransition(Transition<? extends State> t) {
		String parOpen, parClose;
		if (!t.getLabel().isObservable()) {
			parOpen = "(";
			parClose = ")";
		} else {
			parOpen = "[";
			parClose = "]";
		}
		String s = "";
		s += "\t";
		s += parOpen;
		s += t.getLabel().getAction().getString();
		s += parClose;
		s += "\t";
		s += t.getLabel().getString();
		s += "\t";
		s += t.getDestination().getID();
		System.out.println(s);	
	}
	private static void unfold (State s, LabelConstraint c, int rec) {
		//if (rec > 3)
			//return;
		if (!stateSet.contains(s)) {
			stateSet.add(s);
		
			Vector<State> states = new Vector<State>();
			printState(s);
			Iterator<?extends Transition<?extends State>> tit = s.menu(c);
			while (tit.hasNext()) {
				Transition<?extends State> t = tit.next();
				states.add(t.getDestination());
				printTransition(t);
			}
			printState(s);
			Iterator<State> sit = states.iterator();
			int i = 0;
			while (sit.hasNext()) {
				i++;
				//System.out.println("{ statessss: "+i);
				State nxt = sit.next();
				//printState(nxt);
				//System.out.println("} statessss: "+i);
				unfold(nxt, c, rec+1);
			}
		}	
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//String fname = "/Users/axel/myaut.aut";
		
		if (args.length != 1) {
			System.err.println("usage: ltsim/sim file.aut");
			return;
		}
		
		String fname = args[0];
		
		// here we might want something that looks at the file
		// type and does the right thing:
		// .aut -> use AutLTS
		LTS myLTS;
		try {
			myLTS = new AutLTS(fname);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			String msg = "cannot read "+fname;
			if (e.getMessage() != null) {
				msg += ": "+e.getMessage();
			}
			System.err.println(msg);
			return;
			//e.printStackTrace();
		}
		
		
		HardcodedInterpretation interp = new HardcodedInterpretation();	
		Boolean tracesOnly = false;
		ErrorReporter err = new StderrErrorReporter();
		LTS myPrimer = new Primer(myLTS, interp, tracesOnly, err);
		LTS doit;
		doit = myPrimer;
		//doit = myLTS;
		//AutLTS myAutLTS = myLTS.getAutLTS();
		// System.out.println("nrStates: "+myAutLTS.getNrOfStates()+"; nrTrans: "+myAutLTS.getNrOfTrans()+"; start: "+myAutLTS.getStartNr());
		//StateIterator sit = myLTS.init(new MyInterpretation());
		
		Iterator<?extends State> sit = null;
		try {
			sit  = doit.init();
			//labelConstraint.isInternal();
			//labelConstraint.hasAction(new AutAction("REQUEST"));
			
			while (sit.hasNext()) {
				State s = sit.next();
				unfold(s, interp.isAny(), 0);
			}
		} catch (LTSException e) {
			System.err.println("caught LTSException: "+e.getMessage());
		}



	}
		
	private static HashSet<State> stateSet = new HashSet<State>();

}
