package utwente.fmt.jtorx.interpretation;

import utwente.fmt.lts.Action;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;

public class ActionListInterpretation implements utwente.fmt.interpretation.IOLTSInterpretation {

	public class InputLabelConstraint implements utwente.fmt.lts.LabelConstraint {

		public Boolean isSatisfiedBy(Label l) {
			Action a = l.getAction();
	    	for (int i = 0; i < inputs.length ; i++)
	    		if (a.getString().equalsIgnoreCase(inputs[i]))
	    			return true;
			return false;

		}

	}
	public class OutputLabelConstraint implements LabelConstraint {

		public Boolean isSatisfiedBy(Label l) {
			Action a = l.getAction();

	    	for (int i = 0; i < outputs.length ; i++)
	    		if (a.getString().equalsIgnoreCase(outputs[i]))
	    			return true;
			return false;
		}

	}

	public LabelConstraint isAny() {
		return any;
	}
	public LabelConstraint isInternal() {
		return obs;
	}
	public LabelConstraint isInput() {
		return in;
	}

	public LabelConstraint isOutput() {
		return out;
	}
	
	public void updateInputs(String inputActions) {
		inputs = inputActions.split("[, \\t]+");
		if (debug)
			for(int i =0; i < inputs.length; i++) {
				System.err.println("action-input:"+inputs[i]);
			}
	}
	public void updateOutputs(String outputActions) {
		outputs = outputActions.split("[, \\t]+");
		if (debug)
			for(int i =0; i < outputs.length; i++) {
				System.err.println("action-output:"+outputs[i]);
			}
	}
	
	public ActionListInterpretation(String inputActions, String outputActions) {
		any = new AnyLabelConstraint();
		obs = new InternalLabelConstraint();
		in = new InputLabelConstraint();
		out = new OutputLabelConstraint();
		updateInputs(inputActions);
		updateOutputs(outputActions);
	}
	
	private String[] inputs;
	private String [] outputs;
	private LabelConstraint any, obs, in, out;
	private boolean debug = false;
}
