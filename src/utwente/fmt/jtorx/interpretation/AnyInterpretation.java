package utwente.fmt.jtorx.interpretation;

import utwente.fmt.lts.LabelConstraint;

public class AnyInterpretation implements utwente.fmt.interpretation.Interpretation {

	public LabelConstraint isAny() {
		return any;
	}

	public AnyInterpretation() {
		any = new AnyLabelConstraint();

	}
	
	private LabelConstraint any;

}
