package utwente.fmt.jtorx.interpretation;

import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;

public class AnyLabelConstraint implements LabelConstraint {

	public Boolean isSatisfiedBy(Label l) {
		return true;
	}

}
