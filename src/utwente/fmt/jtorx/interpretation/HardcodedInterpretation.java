package utwente.fmt.jtorx.interpretation;

import utwente.fmt.lts.Action;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;

public class HardcodedInterpretation implements utwente.fmt.interpretation.IOLTSInterpretation {

	public class InputLabelConstraint implements utwente.fmt.lts.LabelConstraint {

		public Boolean isSatisfiedBy(Label l) {
			Action a = l.getAction();
			return (a.getString().equalsIgnoreCase("button") );
		}

	}
	public class OutputLabelConstraint implements LabelConstraint {

		public Boolean isSatisfiedBy(Label l) {
			Action a = l.getAction();
	    	String [] out = new String[] {
	    		"coffee",
	    		"espresso",
	    		"cappuccino",
	    	//	"delta",
	    	};

	    	for (int i = 0; i < out.length ; i++)
	    		if (a.getString().equalsIgnoreCase(out[i]))
	    			return true;
			return false;	}

	}

	public LabelConstraint isAny() {
		return any;
	}
	public LabelConstraint isInternal() {
		return obs;
	}
	public LabelConstraint isInput() {
		return in;
	}

	public LabelConstraint isOutput() {
		return out;
	}
	
	public HardcodedInterpretation() {
		any = new AnyLabelConstraint();
		obs = new InternalLabelConstraint();
		in = new InputLabelConstraint();
		out = new OutputLabelConstraint();
	}
	
	private LabelConstraint any, obs, in, out;

}
