package utwente.fmt.jtorx.interpretation;

import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;

public class InternalLabelConstraint implements LabelConstraint {

	public Boolean isSatisfiedBy(Label l) {
		return !l.isObservable();
	}

}
