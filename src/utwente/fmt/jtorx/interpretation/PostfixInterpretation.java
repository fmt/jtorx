package utwente.fmt.jtorx.interpretation;

import utwente.fmt.lts.Action;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;

public class PostfixInterpretation implements utwente.fmt.interpretation.IOLTSInterpretation {

	public class InputLabelConstraint implements utwente.fmt.lts.LabelConstraint {
		public Boolean isSatisfiedBy(Label l) {
			Action a = l.getAction();
			return (a.getString().endsWith("?") );
		}
	}
	public class OutputLabelConstraint implements utwente.fmt.lts.LabelConstraint {
		public Boolean isSatisfiedBy(Label l) {
			Action a = l.getAction();
			return (a.getString().endsWith("!") /*|| a.getString().equalsIgnoreCase("delta")*/);
		}
	}

	public LabelConstraint isAny() {
		return any;
	}

	public LabelConstraint isInternal() {
		return obs;
	}

	public LabelConstraint isInput() {
		return in;
	}

	public LabelConstraint isOutput() {
		return out;
	}
	
	public PostfixInterpretation() {
		any = new AnyLabelConstraint();
		obs = new InternalLabelConstraint();
		in = new InputLabelConstraint();
		out = new OutputLabelConstraint();
	}
	
	private LabelConstraint any, obs, in, out;

}
