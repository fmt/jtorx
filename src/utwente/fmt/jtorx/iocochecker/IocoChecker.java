package utwente.fmt.jtorx.iocochecker;

import java.util.Set;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.writers.IOLTSgenerator;
import utwente.fmt.lts.LTS;

public class IocoChecker {
	
	public IocoCheckerResult check() {
		if (w!=null) {
			w.startCheck();
			return w.getFailures();
		} else
			return null;
	}
	
	public IocoChecker(ErrorReporter err, LTS specLTS, LTS implLTS, String specName, String implName, TraceKind traceKind, IOLTSInterpretation i, iocochecker.IocoCheckerUser u, boolean checkRefines) {

		iolts.IOLTS spec = new IOLTSgenerator(err, "specification", specLTS, i).generateIOLTS();
		iolts.IOLTS impl = new IOLTSgenerator(err, "implementation", implLTS, i).generateIOLTS();
		
		int traceKindMap[] = new int[TraceKind.values().length];
		traceKindMap[TraceKind.STRACES.ordinal()] = 1;
		traceKindMap[TraceKind.UTRACES.ordinal()] = 2;
		
		String relationNameMap[] = new String[TraceKind.values().length];
		relationNameMap[TraceKind.STRACES.ordinal()] = "ioco";
		relationNameMap[TraceKind.UTRACES.ordinal()] = "uioco";

		int traces = traceKindMap[traceKind.ordinal()];
		String rel = relationNameMap[traceKind.ordinal()];

		if (spec==null && impl==null)
			err.report("cannot check: could not generate IOLTS spec and impl");
		else if (spec==null)
			err.report("cannot check: could not generate IOLTS spec");
		else if (impl==null)
			err.report("cannot check: could not generate IOLTS impl");

		if (spec!=null && impl!=null) {
			Set<iolts.InputAction> inputs = spec.getInputActions();

			Set<iolts.State> bg = iolts.IOLTSAlgorithms.getNonInputEnabledStates(inputs, impl.getStates());
			if (bg.size() != 0) {
				if(!checkRefines) {
					// refines is more general than ioco: IOLTS M1 refines M2
					// iff for all traces, in(M1) is a superset of in(M2) and
					// out(M1) is a subset of out(M2).
					iolts.IOLTSAlgorithms.makeStatesInputEnabled(inputs, bg);
				
					err.report("The implementation states " +
							bg + " are not input enabled!\niocoChecker added" +
							" self-loop transitions to achieve input-enabledness.");
				}
			}

			w = new IocoCheckerWrapper(specLTS, implLTS, specName, implName, spec, impl, traces, rel, u, checkRefines);
		}
	}
	
	private IocoCheckerWrapper w = null;

}
