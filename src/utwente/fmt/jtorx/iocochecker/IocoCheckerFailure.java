package utwente.fmt.jtorx.iocochecker;

import utwente.fmt.jtorx.torx.explorer.lib.LibLTS;
import utwente.fmt.jtorx.torx.explorer.lib.LibState;
import utwente.fmt.lts.LTS;

public class IocoCheckerFailure {
	
	public String[] getTabelRowItems() {
		return tableRowItems;
	}
	public int getTraceLength() {
		return trace.length;
	}
	public LTS getLTS() {
		return constructLTS(trace, diff);
	}
	
	public IocoCheckerFailure(String[] tri, String[] t, String[] d) {
		tableRowItems = tri;
		trace = t;
		diff = d;
	}
	
	private LTS constructLTS(String[] t, String[] d) {
		LibLTS lts = new LibLTS();
		LibState init = lts.mkInitialState();
		
		LibState src = init;
		int i = 0;
		for(String l: t) {
			LibState dst = lts.mkState();
			src.addSucc(lts.mkLabel(l), dst, null, null, ""+i++);
			src = dst;
		}
			
		for (String a: d) {
			LibState dst = lts.mkState();
			src.addSucc(lts.mkLabel(a), dst, null, null, ""+i++);
		}
		
		return lts;
	}


	private String[] tableRowItems; // trace, 
	private String[] trace;
	private String[] diff;

}
