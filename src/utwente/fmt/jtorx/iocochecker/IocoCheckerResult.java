package utwente.fmt.jtorx.iocochecker;

import java.util.List;

import utwente.fmt.lts.LTS;

public class IocoCheckerResult {
	
	public List<IocoCheckerFailure> getResults() {
		return results;
	}
	
	public LTS getLTSCombinedResult() {
		return ltsCombinedResult;
	}
	
	public String getRelationName() {
		return relationName;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public LTS getLTS(int idx) {
		if (idx < 0 || idx >= results.size())
			return null;
		return results.get(idx).getLTS();
	}
	
	public LTS getModelLTS() {
		return specLTS;
	}
	
	public LTS getImplementationLTS() {
		return implLTS;
	}
	
	public String getModelFilename() {
		return specName;
	}
	
	public String getImplementationFilename() {
		return implName;
	}

	
	public IocoCheckerResult(LTS specLTS, LTS implLTS, String specName, String implName, List<IocoCheckerFailure> fl, LTS c, String rel) {
		results = fl;
		ltsCombinedResult = c;
		relationName = rel;
		errorMessage = null;
		this.specLTS = specLTS;
		this.implLTS = implLTS;
		this.specName = specName;
		this.implName = implName;
	}
	public IocoCheckerResult(String e) {
		results = null;
		ltsCombinedResult = null;
		relationName = null;
		errorMessage = e;
		specLTS = null;
		implLTS = null;
		specName = null;
		implName = null;
	}


	private List<IocoCheckerFailure> results;
	private LTS specLTS;
	private LTS implLTS;
	private String specName;
	private String implName;
	private LTS ltsCombinedResult;
	private String relationName;
	private String errorMessage;
}

