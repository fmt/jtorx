package utwente.fmt.jtorx.iocochecker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.torx.explorer.lib.LibLTS;
import utwente.fmt.jtorx.torx.explorer.lib.LibState;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;


public class IocoCheckerWrapper extends iocochecker.IocoChecker {

	public IocoCheckerWrapper(LTS specLTS, LTS implLTS, String specName, String implName, iolts.IOLTS spec, iolts.IOLTS impl, int traces, String rel, iocochecker.IocoCheckerUser u, boolean checkRefines) {
		super(spec, impl, traces, checkRefines);
		relationName = rel;
		user = u;
		this.specLTS = specLTS;
		this.implLTS =implLTS;
		this.specName = specName;
		this.implName = implName;
	}

	public void startCheck() {
		Set<iocochecker.FailureSituation> errSet = doCheck(user);
		user.progressReportCheckingFinished();
		errors = new Vector<iocochecker.FailureSituation>(errSet);
		if (false) {
		Collections.sort(errors, new Comparator<iocochecker.FailureSituation>() {
			public int compare(iocochecker.FailureSituation o1, iocochecker.FailureSituation o2) {
				List<iolts.Action> o1acts = o1.getSuspensionTrace();
				List<iolts.Action> o2acts = o2.getSuspensionTrace();
				int diff = o1acts.size() - o2acts.size();
				if (diff!=0)
					return diff;
				Iterator<iolts.Action> o1it = o1acts.iterator();
				Iterator<iolts.Action> o2it = o2acts.iterator();
				int lexCmpRes = 0;
				while(o1it.hasNext() && o2it.hasNext()) {
					iolts.Action o1act = o1it.next();
					iolts.Action o2act = o2it.next();
					lexCmpRes = o1act.toString().compareTo(o2act.toString());
					if (lexCmpRes != 0)
						return lexCmpRes;
				}
				return lexCmpRes;
			}});
		}
	}
	
	public IocoCheckerResult getFailures() {
		if (errors==null)
			return null;
		if (failures==null) {
			List<IocoCheckerFailure> ffl = new Vector<IocoCheckerFailure>();
			for (iocochecker.FailureSituation f: errors) {
				ffl.add(wrapFailureSituation(f));
			}

			LTS c =  null; // combineTestPurposes(tp);
			failures = new IocoCheckerResult(specLTS, implLTS, specName, implName, ffl, c, relationName);
		}
		return failures;
	}
	
	static public IocoCheckerFailure wrapFailureSituation(iocochecker.FailureSituation f) {
		String[] arr = new String[4];
		List<String> l;
		String[] trace = new String[f.getSuspensionTrace().size()];
		int i = 0;
		String as;

		String s = "";
		Iterator<iolts.Action> ait = f.getSuspensionTrace().iterator();
		if (ait.hasNext()) {
			as = getString(ait.next());
			s += as;
			trace[i++] = as;
		}
		while(ait.hasNext()) {
			as = getString(ait.next());
			s += " . "+as;
			trace[i++] = as;
		}
		arr[0] = s;

		l = new Vector<String>();
		for(iolts.Action o: f.getImplActions())
			l.add(getString(o));
		arr[1] = makeSetString(l);

		l = new Vector<String>();
		for(iolts.Action o: f.getSpecActions())
			l.add(getString(o));
		arr[2] = makeSetString(l);
		
		l = new Vector<String>();
		
		if(f.getSpecActions().size() > f.getImplActions().size()) {
			for(iolts.Action o: f.getSpecActions())
				l.add(getString(o));
			for(iolts.Action o: f.getImplActions())
				l.remove(getString(o));			
		}
		else {
			for(iolts.Action o: f.getImplActions())
				l.add(getString(o));
			for(iolts.Action o: f.getSpecActions())
				l.remove(getString(o));
		}
		
		arr[3] = makeSetString(l);
	

		String[] diff = l.toArray(new String[0]);
		
		return new IocoCheckerFailure(arr, trace, diff);
	}
	
	private LTS combineTestPurposes(List<LTS> tp) {
		if (errors==null)
			return null;
		
		AnyInterpretation interp = new AnyInterpretation();
		LibLTS lts = new LibLTS();
		LibState init = lts.mkInitialState();
		Map<State,LibState> stateMap = new HashMap<State,LibState>();
		ArrayList<State> work = new ArrayList<State>();
		LibState src = init;
		for (LTS t: tp) {
			Iterator<? extends State> sit = null;
			try {
				sit = t.init();
			} catch (LTSException e) {
				System.err.println("combineTestPurposes: caught LTSException: "+e.getMessage());
				return null;
			}
			while(sit.hasNext()) {
				State s = sit.next();
				stateMap.put(s, src);
				work.add(s);
			}
		}
		Iterator<? extends Transition<? extends State>> tit;
		for(int idx = 0; idx < work.size(); idx++) {
			State s = work.get(idx);
			src = stateMap.get(s);
			tit = s.menu(interp.isAny());
			if (tit != null)
				while(tit.hasNext()) {
					Transition<? extends State> t = tit.next();
					Label l = t.getLabel();
					State d = t.getDestination();
					LibState dst = stateMap.get(d);
					if (dst==null) {
						dst = lts.mkState(); // this is OK because we do not merge states across LTSes
						stateMap.put(d, dst);
						work.add(d);
					}
					// TODO add more informative transition identifier (last argument) in next line
					src.addSucc(lts.mkLabel(l.getString()), dst, null, null, ""); 
				}
		}
		return lts;
	}
	
	static private String stripPrefix(String s) {
		if (s.charAt(0) == '?' || s.charAt(0) == '!')
			return s.substring(1);
		else
			return s;
	}
	static private String getString(iolts.Action a) {
		if (a instanceof iolts.Quiescence)
			return "delta";
		else
			return stripPrefix(a.toString());
	}
	
	static private String makeSetString(List<String> l) {
		String s = "{ ";
		Collections.sort(l);
		Iterator<String> it = l.iterator();
		if (it.hasNext())
			s += it.next();
		while(it.hasNext())
			s += ", "+it.next();
		s += " }";
		return s;
	}
	
	private List<iocochecker.FailureSituation> errors = null;
	private IocoCheckerResult failures = null;
	private String relationName;
	private iocochecker.IocoCheckerUser user = null;
	private LTS specLTS = null;
	private LTS implLTS = null;
	private String specName = null;
	private String implName = null;
}
