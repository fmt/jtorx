package utwente.fmt.jtorx.lazyotf;

/**
 * A validator for (Dumont) expressions and conditions.
 */
public interface ExpressionValidator {
    /**
     * Checks the validity of an integer-value expression. If and only if the
     * expression is valid, no {@link ValidationException} is thrown.
     * 
     * @param expression
     *            The expression to be checked.
     * @throws ValidationException
     *             when the given expression is invalid.
     */
    void checkUpdateExpression(String expression) throws ValidationException;

    /**
     * Checks the validity of a boolean condition expression. If and only if the
     * expression is valid, no {@link ValidationException} is thrown.
     * 
     * @param condition
     *            The condition expression to be checked.
     * @throws ValidationException
     *             when the given expression is invalid.
     */
    void checkCondition(String condition) throws ValidationException;

    /**
     * Checks the validity of a boolean condition expression and an
     * integer-value expression. If and only if both expressions are valid, no
     * {@link ValidationException} is thrown.
     * 
     * @param condition
     *            The boolean condition expression to be checked.
     * @param expression
     *            The expression to be checked.
     * @throws ValidationException
     *             when at least one of the given expressions is invalid.
     */
    void check(String condition, String expression) throws ValidationException;

    /**
     * An exception intended to be thrown when an expression is invalid.
     */
    @SuppressWarnings("serial")
    public static class ValidationException extends Exception {
        /**
         * Constructs a new {@link ValidationException}.
         * @param text The exception's message text.
         */
        public ValidationException(String text) {
            super(text);
        }
    };
}
