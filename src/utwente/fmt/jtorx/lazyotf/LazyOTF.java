package utwente.fmt.jtorx.lazyotf;

import java.util.Set;

import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.remoteimpl.Message;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.torx.DriverSimInterActionResult;
import utwente.fmt.lts.Label;

/**
 * Interface for LazyOTF implementations.
 */
public interface LazyOTF {

	/**
	 * Get the current LazyOTF proposal.
	 * 
	 * @return A set of Label objects representing the current proposal.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 * @throws NotReadyException
	 *             The LazyOTF instance currently does not have a test tree.
	 */
	Set<Label> propose() throws InactiveException, NotReadyException;

	/**
	 * Inform the LazyOTF instance about a taken transition represented by the
	 * given transition label.
	 * 
	 * @param transitionLabel
	 *            The label associated with the transition.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 * @throws NotReadyException
	 *             The LazyOTF instance currently does not have a test tree.
	 */
	void tookTransition(Label transitionLabel) throws InactiveException, NotReadyException;

	/**
	 * Inform the LazyOTF instance about a taken transition represented by the
	 * given driver interaction result.
	 * 
	 * @param drvResult
	 *            The driver interaction result associated with the transition.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 * @throws NotReadyException
	 *             The LazyOTF instance currently does not have a test tree.
	 */
	void tookTransition(DriverSimInterActionResult drvResult) throws InactiveException,
	NotReadyException;

	/**
	 * Instruct the LazyOTF instance to recompute its test tree.
	 * 
	 * @param stateIDs
	 *            The ID of the compound state which should be the
	 *            recomputation's base.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 */
	void computeNewTestTree(Set<String> stateIDs) throws InactiveException;

	/**
	 * Instruct the LazyOTF instance to recompute its test tree "on the spot",
	 * i.e. without altering the set of instantiated states which are the base
	 * of the test tree computation. Use this only if you know that your driver
	 * and your LazyOTF instance have the same set of currently instantiated
	 * states.
	 * 
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 */
	void computeNewTestTree() throws InactiveException;

	/**
	 * Reset the LazyOTF instance. Clears the current test tree and sets the
	 * instance's current STS state to the initial state. Also, clears any
	 * caches kept by the LazyOTF instance.
	 */
	void reset();

	/**
	 * Sets the maximum depth of the test trees computed by this LazyOTF
	 * instance.
	 * 
	 * @param depth
	 *            The maximum test tree depth. Must be non-negative.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 */
	void setMaxRecursionDepth(int depth) throws InactiveException;

	/**
	 * Instructs the LazyOTF instance to construct the test tree using the given
	 * STS visiting method.
	 * 
	 * @param it
	 *            The STS visiting method.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 */
	void setTraversalMethod(TraversalMethod it) throws InactiveException;

	/**
	 * Instructs the LazyOTF instance to use the {@link Path2Weight} associated
	 * with the given name in addition to all previously selected
	 * {@link Path2Weight}s.
	 * 
	 * @param m
	 *            The {@link Path2Weight}.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 */
	void addPath2Weight(Path2Weight m) throws InactiveException;

	/**
	 * Install the given {@link NodeValue2Weight} instance.
	 * 
	 * @param it
	 *            The {@link NodeValue2Weight} instance to be added.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 */
	void addNodeValue2Weight(NodeValue2Weight it) throws InactiveException;

	/**
	 * Removes the given {@link NodeValue2Weight} instance.
	 * 
	 * @param it
	 *            The {@link NodeValue2Weight} instance which will be removed.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 */
	void removeNodeValue2Weight(NodeValue2Weight it) throws InactiveException;

	/**
	 * Use the given test tree weigher.
	 * 
	 * @param it
	 *            The test tree weigher.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 */
	void setTreeWeigher(TestTree2Weight it) throws InactiveException;

	/**
	 * Getter for the enabledness of additional precondition checks during the
	 * test tree construction.
	 * 
	 * @return true iff additional precondition checks will be considered.
	 */
	boolean isCheckedPREDfsVisitorEnabled();

	/**
	 * Getter for the maximum depth of the test trees computed by this LazyOTF
	 * instance.
	 * 
	 * @return The maximum depth of the test trees computed by this LazyOTF
	 *         instance.
	 */
	int getMaxRecursionDepth();

	/**
	 * Getter for the used STS traversal method's name.
	 * 
	 * @return the used STS traversal method's name.
	 */
	TraversalMethod getTraversalMethod();

	/**
	 * Getter for the {@link Path2Weight}s used for STS traversals.
	 * 
	 * @return A set of {@link Path2Weight}s.
	 */
	Set<Path2Weight> getPath2Weights();

	/**
	 * Getter for the selected test tree weigher's name.
	 * 
	 * @return the selected test tree weigher's name.
	 */
	TestTree2Weight getTreeWeigher();

	/**
	 * Getter for the readyness attribute of the LazyOTF instance.
	 * 
	 * @return true iff the instance is ready to compute test trees.
	 */
	boolean isReady();

	/**
	 * Gets an LazyOTFConfigurationOptions object representing the admissible
	 * values for this instance's configuration.
	 * 
	 * @return A LazyOTFConfigurationOptions object as specified above.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 */
	LazyOTFConfigurationOptions getConfigurationOptions() throws InactiveException;

	/**
	 * @return The LazyOTF instance's Location Manager.
	 */
	LocationMgr getLocationManager();

	/**
	 * Initializer for the LazyOTF instance.
	 * 
	 * @throws InactiveException when the I/O channel is not working,
	 *         e.g. SymToSim is not running anymore.
	 */
	void init() throws InactiveException;

	/**
	 * Deactivate the LazyOTF instance.
	 * 
	 * @param activeness
	 */
	void setInactive();

	/**
	 * @return true iff the LazyOTF instance is active.
	 */
	boolean isActive();

	/**
	 * getTestTreeNode reveals information about the nodes within the currently
	 * used LazyOTF test tree. The current node can be obtained by passing -1 as
	 * the nodeID.
	 * 
	 * @param nodeID
	 *            The desired node's ID.
	 * @return A TestTreeNode object representing the node identified by nodeID
	 *         or null if no such node exists.
	 * @throws InactiveException
	 *             The LazyOTF instance has become inactive, i.e. it has been
	 *             deactivated or an internal error occurred.
	 */
	TestTreeNode getTestTreeNode(Integer nodeID) throws InactiveException;

	/**
	 * Adds an observer to the LazyOTF instance. The notification objects have
	 * types directly subclassing {@link LazyOTFEvents}.
	 * 
	 * @param observer
	 *            The observer.
	 */
	void addObserver(Observer observer/* , EventType eventCategory */);

	/**
	 * @return This LazyOTF instance's backend port (or null if it has no
	 *         backend).
	 */
	LazyOTFInstancePort getInstancePort();

	/**
	 * Removes a {@link Path2Weight} from the LazyOTF instance.
	 * 
	 * @param it
	 *            The {@link Path2Weight} which should be removed.
	 */
	void removePath2Weight(Path2Weight it);

	/**
	 * @return The object responsible for computing the current model traversal
	 *         depth.
	 */
	TraversalDepthComputer getTraversalDepthComputer();

	/**
	 * @return <tt>true</tt> iff a test tree has been computed, but no
	 *         transition has been taken yet.
	 */
	boolean hasFreshTestTree();

	/**
	 * "Destructor" to clean up the LazyOTF instance.
	 */
	void cleanUp();

	/**
	 * Enables rsp. disables the recognition of nondeterministic superstates as
	 * test goals.
	 * @param inhibit Iff <tt>true</tt>, only deterministic superstates may be
	 *   recognized as test goals. 
	 */
	public void setInhibitNondeterministicTestGoals(boolean inhibit);

	/**
	 * Signals the LazyOTF implementation that a test objective has been discharged.
	 */
	public void onDischargedTestObjective();
	
	/**
	 * Queries the LazyOTF instance for cycle warnings.
	 */
	public boolean hasDetectedACycle();

	/**
	 * The LazyOTF exception supertype.
	 */
	@SuppressWarnings("serial")
	public abstract class LazyOTFException extends Exception {
		/**
		 * Constructs a {@link LazyOTFException} object using the given
		 * exception message.
		 * 
		 * @param msg
		 *            The exception message.
		 */
		public LazyOTFException(String msg) {
			super(msg);
		}

		/**
		 * Constructs a {@link LazyOTFException} without an exception message.
		 */
		public LazyOTFException() {
			super();
		}
	}

	/**
	 * A class for exceptions indicating that the operation could not be
	 * performed because LazyOTF has become inactive, i.e. deactivated on
	 * purpose or an internal, non-recoverable error occurred.
	 */
	@SuppressWarnings("serial")
	public class InactiveException extends LazyOTFException {
		/**
		 * Constructs an {@link InactiveException} without an exception message.
		 */
		public InactiveException() {
			super();
		}

		/**
		 * Constructs an {@link InactiveException} object using the given
		 * exception message.
		 * 
		 * @param msg
		 *            The exception message.
		 */
		public InactiveException(String msg) {
			super(msg);
		}
	}

	/**
	 * A class for exceptions indicating that the operation could not be
	 * performed because LazyOTF currently does not have a test tree, i.e. that
	 * a new test tree needs to be computed beforehand.
	 */
	@SuppressWarnings("serial")
	public class NotReadyException extends LazyOTFException {
		/**
		 * Constructs a {@link NotReadyException} object using the given
		 * exception message.
		 * 
		 * @param msg
		 *            The exception message.
		 */
		public NotReadyException(String msg) {
			super(msg);
		}
	}

	/**
	 * Small helper class enabling direct communication with the LazyOTF
	 * backend. TODO: Move to RemoteLazyOTF
	 */
	public interface LazyOTFInstancePort {
		/**
		 * Sends a message to the LazyOTF backend.
		 * 
		 * @param cmd
		 *            The message which should be sent to the backend.
		 */
		void send(String cmd);
	}

	/**
	 * An interface collecting LazyOTF event classes.
	 */
	public interface LazyOTFEvents {
		/**
		 * Observer notification class for message-exchange events.
		 */
		public class MessageExchangedEvent {
			/** The exchanged message. */
			public final Message message;

			/**
			 * Constructs a new {@link MessageExchangedEvent} instance.
			 * 
			 * @param message
			 *            The exchanged message.
			 */
			public MessageExchangedEvent(Message message) {
				super();
				this.message = message;
			}
		}

		/**
		 * Observer notification class for took-transition events.
		 */
		public class TookTransitionEvent {
			/** The new current test tree node. */
			public final TestTreeNode newTestTreeNode;

			/**
			 * Constructs a new {@link TookTransitionEvent} instance.
			 * 
			 * @param newTestTreeNode
			 *            The new current test tree node.
			 */
			public TookTransitionEvent(TestTreeNode newTestTreeNode) {
				super();
				this.newTestTreeNode = newTestTreeNode;
			}
		}
	}
}
