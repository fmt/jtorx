package utwente.fmt.jtorx.lazyotf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.distributed.DistributeLazyOTF;
import utwente.fmt.jtorx.lazyotf.distributed.DistributeLazyOTFFactory;
import utwente.fmt.jtorx.lazyotf.distributed.NullDistributeLazyOTFFactory;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.testnongui.casestudy.CaseStudyIO;

/**
 * A class representing the requested LazyOTF configuration.
 */
public class LazyOTFConfig {
	/** The default model traversal depth modification threshold. */
	private static final int DEFAULT_DEPTH_MOD_THRESH = 20;

	/** The default initial model traversal depth. */
	private static final int DEFAULT_INITIAL_DEPTH = 1;

	/** The default maximum model traversal depth. */
	private static final int DEFAULT_MAX_DEPTH = -1;

	/** The traversal method to be used. */
	private TraversalMethod traversalMethod = null;

	/** The test tree weigher to be used. */
	private TestTree2Weight testTreeWeigher = null;

	/** The set of {@link Path2Weight}s to be used. */
	private Set<Path2Weight> path2Weights = new HashSet<>();

	/**
	 * The set of all defined custom {@link Path2Weight}s (including disabled ones).
	 */
	private Set<Path2Weight> allPath2Weights = new HashSet<>();

	/** The maximum traversal depth to be used. */
	private int maxRecursionDepth = DEFAULT_MAX_DEPTH;

	/** The initial traversal depth. */
	private int initialRecursionDepth = DEFAULT_INITIAL_DEPTH;

	/** The recursion depth modification threshold. */
	private int recursionDepthModThreshold = DEFAULT_DEPTH_MOD_THRESH;

	/** true iff the CheckedPREWrappingDfsVisitor should be used. */
	private boolean doUseCP = false;

	/** true iff the auto-recompute mechanism should be activated. */
	private boolean doAutoRecompute = false;

	/** true iff the automatic stopping of autostep should be activated. */
	private boolean doAutoStop = false;

	/** A Map associating state names with location test types. */
	private Set<Location> locations = new HashSet<Location>();

	/** The set of test type groups. */
	private Set<TestTypeGroup> testTypeGroups = new HashSet<>();

	/** The set of globally available {@link SatStrategy} conditions. */
	private Set<SatStrategy.Condition> globalSatStrategyConditions = new HashSet<>();

	/** The set of decision strategies to be installed. */
	private List<DecisionStrategy> decisionStrategies = new ArrayList<>();

	/** The set of available node value weighers. */
	private List<NodeValue2Weight> nodeValue2Weights = new ArrayList<>();

	/** If true, only superstates with exactly one element count as test goals. */
	private boolean inhibitNondetTestGoals = false;

	/** If true, cycle detection is enabled. */
	private boolean doUseCycleDetection = false;

	/** The cycle warning threshold. */
	private int cycleDetectionThreshold = 0;

	/**
	 * The factory for {@link DistributeLazyOTF} instances. By default, this
	 * is a factory producing a null DistributeLazyOTF instance, i.e.
	 * distribution is disabled.
	 * TODO configure me! 
	 */
	private DistributeLazyOTFFactory distributeLazyOTFFactory = new NullDistributeLazyOTFFactory();

	/** If true, messages exchanged with the LazyOTF core are logged. */
	private boolean doLogCommMessages = true;

	/**
	 * Make a new LazyOTFConfig instance.
	 */
	public LazyOTFConfig() {
		// This is only included temporarily.
		String logProperty = System.getProperty("lazyotf.debug.logCommByDefault");
		if (logProperty != null) {
			CaseStudyIO.err.println("LazyOTF: Overriding default internal messsage logging "
					+ "parameter to " + logProperty);
			doLogCommMessages = Boolean.valueOf(logProperty);
		}
	}

	/**
	 * Copy-constructy a new LazyOTFConfig instance.
	 * @param source The copied LazyOTFConfig instance.
	 */
	public LazyOTFConfig(LazyOTFConfig source) {
		this.allPath2Weights = source.allPath2Weights;
		this.decisionStrategies = source.decisionStrategies;
		this.distributeLazyOTFFactory = source.distributeLazyOTFFactory;
		this.doAutoRecompute = source.doAutoRecompute;
		this.doAutoStop = source.doAutoStop;
		this.doUseCP = source.doUseCP;
		this.globalSatStrategyConditions = source.globalSatStrategyConditions;
		this.inhibitNondetTestGoals = source.inhibitNondetTestGoals;
		this.initialRecursionDepth = source.initialRecursionDepth;
		this.locations = source.locations;
		this.maxRecursionDepth = source.maxRecursionDepth;
		this.nodeValue2Weights = source.nodeValue2Weights;
		this.path2Weights = source.path2Weights;
		this.recursionDepthModThreshold = source.recursionDepthModThreshold;
		this.testTreeWeigher = source.testTreeWeigher;
		this.testTypeGroups = source.testTypeGroups;
		this.traversalMethod = source.traversalMethod;
		this.cycleDetectionThreshold = source.cycleDetectionThreshold;
		this.doUseCycleDetection = source.doUseCycleDetection;
		this.doLogCommMessages = source.doLogCommMessages;
	}

	/**
	 * @return The traversal method to be used.
	 */
	public TraversalMethod getTraversalMethod() {
		return traversalMethod;
	}

	/**
	 * @param name
	 *            The traversal method to be used.
	 */
	public void setTraversalMethod(TraversalMethod name) {
		traversalMethod = name;
	}

	/**
	 * @return The name test tree weigher to be used.
	 */
	public TestTree2Weight getTestTreeWeigher() {
		return testTreeWeigher;
	}

	/**
	 * @param name
	 *            The test tree weigher to be used.
	 */
	public void setTestTreeWeigher(TestTree2Weight name) {
		testTreeWeigher = name;
	}

	/**
	 * @return The set of {@link Path2Weight}s to be used.
	 */
	public Set<Path2Weight> getEnabledPath2Weights() {
		return path2Weights;
	}

	/**
	 * @param methods
	 *            The set of {@link Path2Weight}s to be used.
	 */
	public void setEnabledPath2Weights(Set<Path2Weight> methods) {
		path2Weights = methods;
	}

	/**
	 * @return The maximum traversal depth to be used.
	 */
	public int getMaxRecursionDepth() {
		return maxRecursionDepth;
	}

	/**
	 * @param depth
	 *            The maximum traversal depth to be used.
	 */
	public void setMaxRecursionDepth(int depth) {
		maxRecursionDepth = depth;
	}

	/**
	 * @return true iff the CheckedPREWrappingDfsVisitor should be used.
	 */
	public boolean doUseCP() {
		return doUseCP;
	}

	/**
	 * @return <tt>true</tt> iff the test should be stopped automatically as
	 *         soon as all test objectives have been discharged.
	 */
	public boolean doAutoStop() {
		return doAutoStop;
	}

	/**
	 * @param useCP
	 *            true iff the CheckedPREWrappingDfsVisitor should be used.
	 */
	public void setUseCP(boolean useCP) {
		doUseCP = useCP;
	}

	/**
	 * @return true iff the auto-recompute mechanism should be activated.
	 */
	public boolean doAutoRecompute() {
		return doAutoRecompute;
	}

	/**
	 * @param autoRecompute
	 *            true iff the auto-recompute mechanism should be activated.
	 */
	public void setAutoRecompute(boolean autoRecompute) {
		doAutoRecompute = autoRecompute;
	}

	/**
	 * @param autoStop
	 *            <tt>true</tt> the test should be stopped automatically as soon
	 *            as all test objectives have been discharged.
	 */
	public void setAutoStop(boolean autoStop) {
		doAutoStop = autoStop;
	}

	/**
	 * @return The set of locations used by the test type groups.
	 */
	public Set<Location> getLocations() {
		return locations;
	}

	/**
	 * @param locations
	 *            the set of locations used by the test type groups.
	 */
	public void setLocations(Set<Location> locations) {
		this.locations = locations;
	}

	/**
	 * @param m
	 *            The set of all custom {@link Path2Weight}s (including disabled
	 *            ones) to be stored.
	 */
	public void setAllPath2Weights(Set<Path2Weight> m) {
		allPath2Weights = m;
	}

	/**
	 * @return The set of all custom {@link Path2Weight}s (including disabled
	 *         ones).
	 */
	public Set<Path2Weight> getAllPath2Weights() {
		return allPath2Weights;
	}

	/**
	 * @param g
	 *            The set of test type groups to be stored.
	 */
	public void setTestTypeGroups(Set<TestTypeGroup> g) {
		testTypeGroups = g;
	}

	/**
	 * @return The set of test type groups to be loaded.
	 */
	public Set<TestTypeGroup> getTestTypeGroups() {
		return testTypeGroups;
	}

	/**
	 * @return The set of globally available {@link SatStrategy} conditions.
	 */
	public Set<SatStrategy.Condition> getGlobalSatStrategyConditions() {
		return globalSatStrategyConditions;
	}

	/**
	 * @param conditions
	 *            The set of globally available {@link SatStrategy} conditions.
	 */
	public void setGlobalSatStrategyConditions(Set<SatStrategy.Condition> conditions) {
		globalSatStrategyConditions = conditions;
	}

	/**
	 * @return The set of available {@link DecisionStrategy} instances.
	 */
	public List<DecisionStrategy> getDecisionStrategies() {
		return decisionStrategies;
	}

	/**
	 * @param strategies
	 *            The set of available {@link DecisionStrategy} instances.
	 */
	public void setDecisionStrategies(List<DecisionStrategy> strategies) {
		decisionStrategies = strategies;
	}

	/**
	 * @return The set of available node value weighers.
	 */
	public List<NodeValue2Weight> getNodeValue2Weights() {
		return nodeValue2Weights;
	}

	/**
	 * @param it
	 *            The set of available node value weighers.
	 */
	public void setNodeValue2Weights(List<NodeValue2Weight> it) {
		nodeValue2Weights = it;
	}

	/**
	 * @return The initial recursion depth.
	 */
	public int getInitialRecursionDepth() {
		return initialRecursionDepth;
	}

	/**
	 * Sets the initial recursion depth.
	 * 
	 * @param initialRecursionDepth
	 *            The initial recursion depth.
	 */
	public void setInitialRecursionDepth(int initialRecursionDepth) {
		this.initialRecursionDepth = initialRecursionDepth;
	}

	/**
	 * @return The recursion depth modification threshold.
	 */
	public int getRecursionDepthModThreshold() {
		return recursionDepthModThreshold;
	}

	/**
	 * Sets the recursion depth modification threshold.
	 * 
	 * @param recursionDepthModThreshold
	 *            The recursion depth modification threshold.
	 */
	public void setRecursionDepthModThreshold(int recursionDepthModThreshold) {
		this.recursionDepthModThreshold = recursionDepthModThreshold;
	}

	public DistributeLazyOTFFactory getDistributeLazyOTFFactory() {
		return distributeLazyOTFFactory;
	}

	public void setDistributeLazyOTFFactory(DistributeLazyOTFFactory it) {
		distributeLazyOTFFactory = it;
	}

	public void setInhibitNondetTestGoals(boolean inhibit) {
		this.inhibitNondetTestGoals = inhibit;
	}

	public boolean isInhibitingNondetTestGoals() {
		return this.inhibitNondetTestGoals;
	}

	public void setCycleDetectionEnabled(boolean enabled) {
		doUseCycleDetection = enabled;
	}

	public boolean isCycleDetectionEnabled() {
		return doUseCycleDetection;
	}

	public void setCycleDetectionThreshold(int threshold) {
		cycleDetectionThreshold = threshold;
	}

	public int getCycleDetectionThreshold() {
		return cycleDetectionThreshold;
	}

	public void setCommunicationLoggingEnabled(boolean enabled) {
		this.doLogCommMessages = enabled;
	}

	public boolean isCommunicationLoggingEnabled() {
		return this.doLogCommMessages;
	}
}
