package utwente.fmt.jtorx.lazyotf;

import java.util.HashSet;
import java.util.Set;

/**
 * A singleton class for managing LazyOTF configuration hooks, i.e.
 * hooks getting applied to LazyOTF configurations before the start of the
 * test run.
 */
public class LazyOTFConfigurationHooks {
	/** The set of hooks beind managed by this singleton. */
	private final Set<LazyOTFConfigurationHook> hooks;
	
	/** The class' singleton instance. */
	private static final LazyOTFConfigurationHooks instance = new LazyOTFConfigurationHooks();
	
	/**
	 * Adds a hook to the set of hooks managed by this singleton.
	 * @param hook The new hook.
	 */
	public void addHook(LazyOTFConfigurationHook hook) {
		hooks.add(hook);
	}
	
	/**
	 * Applies the hooks previously added to this singleton to the given
	 * LazyOTF configuration.
	 * @param initialConfig The initial LazyOTF configuration (does not get
	 *   changed by this procedure).
	 * @return The LazyOTF configuration after applying all hooks.
	 */
	public LazyOTFConfig applyHooks(LazyOTFConfig initialConfig) {
		LazyOTFConfig currentConfig = new LazyOTFConfig(initialConfig);
		for (LazyOTFConfigurationHook hook : hooks) {
			currentConfig = hook.applyHook(currentConfig);
		}
		return currentConfig;
	}
	
	/**
	 * Removes all previously added hooks from this singleton.
	 */
	public void reset() {
		hooks.clear();
	}
	
	/**
	 * @return The singleton instance of this class.
	 */
	public static LazyOTFConfigurationHooks getInstance() {
		return instance;
	}
	
	/**
	 * Constructs the {@link LazyOTFConfigurationHooks} singleton.
	 */
	private LazyOTFConfigurationHooks() {
		this.hooks = new HashSet<>();
	}
	
	/**
	 * An interface for LazyOTF configuration hooks.
	 */
	public interface LazyOTFConfigurationHook {
		/**
		 * Applies the hook to the given LazyOTF configuration. 
		 * @param configuration The target LazyOTF configuration.
		 * @return The LazyOTF configuration after the hook application.
		 */
		LazyOTFConfig applyHook(LazyOTFConfig configuration);
	}	
}
