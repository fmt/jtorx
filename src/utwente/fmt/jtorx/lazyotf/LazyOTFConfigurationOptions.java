package utwente.fmt.jtorx.lazyotf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;

/**
 * A container for LazyOTF configuration options.
 */
public class LazyOTFConfigurationOptions {
    public LazyOTFConfigurationOptions(List<TraversalMethod> traversalMethods,
            List<TestTree2Weight> testTree2Weights,
            List<Path2Weight> path2Weights,
            String defaultVisitorMethod,
            String defaultTestTreeWeigher,
            String defaultPath2Weight,
            List<Path2Weight> configurablePath2Weights,
            List<String> decisionStrategies) {
        this.traversalMethods = Collections.unmodifiableList(traversalMethods);
        testTreeWeighers = Collections.unmodifiableList(testTree2Weights);
        this.path2Weights = Collections.unmodifiableList(path2Weights);
        defaultTraversalMethod = defaultVisitorMethod;
        this.defaultTestTreeWeigher = defaultTestTreeWeigher;
        this.defaultPath2Weight = defaultPath2Weight;
        this.configurablePath2Weights = Collections.unmodifiableList(configurablePath2Weights);
        this.decisionStrategies = Collections.unmodifiableList(decisionStrategies);

        name2Tree2Weight = new HashMap<>();
        for (TestTree2Weight t : testTree2Weights) {
            name2Tree2Weight.put(t.getName(), t);
        }

        name2TraversalMethod = new HashMap<>();
        for (TraversalMethod t : traversalMethods) {
            name2TraversalMethod.put(t.getName(), t);
        }
    }
    
    public LazyOTFConfigurationOptions() {
        traversalMethods = new ArrayList<>();
        testTreeWeighers = new ArrayList<>();
        path2Weights = new ArrayList<>();
        defaultTraversalMethod = null;
        defaultTestTreeWeigher = null;
        defaultPath2Weight = null;
        configurablePath2Weights = new ArrayList<>();
        decisionStrategies = new ArrayList<>();
        name2Tree2Weight = new HashMap<>();
        name2TraversalMethod = new HashMap<>();
    }

    /** A list of available graph traversal methods. */
    public final List<TraversalMethod> traversalMethods;

    /** A list of available test tree weighers. */
    public final List<TestTree2Weight> testTreeWeighers;

    /** A list of available {@link Path2Weight}s. */
    public final List<Path2Weight> path2Weights;

    /** The default visitor method's name. */
    public final String defaultTraversalMethod;

    /** The default test tree weigher's name. */
    public final String defaultTestTreeWeigher;

    /** The default {@link Path2Weight}'s name. */
    public final String defaultPath2Weight;

    /** A list of configurable {@link Path2Weight}s. */
    public final List<Path2Weight> configurablePath2Weights;

    /** The default strategy's name. */
    public final List<String> decisionStrategies;

    /**
     * Maps {@link TestTree2Weight} implementation names to the respective
     * {@link TestTree2Weight} instance.
     */
    private final Map<String, TestTree2Weight> name2Tree2Weight;

    /**
     * Maps {@link TraversalMethod} implementation names to the respective
     * {@link TraversalMethod} instance.
     */
    private final Map<String, TraversalMethod> name2TraversalMethod;

    /**
     * Gets the {@link TestTree2Weight} object associated with the given name.
     * 
     * @param name
     *            The name to be looked up in the {@link TestTree2Weight}
     *            dictionary.
     * @return The corresponding {@link TestTree2Weight} instance.
     * @throws NoSuchElementException
     *             when <tt>name</tt> is not associated with any
     *             {@link TestTree2Weight} object.
     */
    public TestTree2Weight tree2WeightByName(String name) {
        if (!name2Tree2Weight.containsKey(name)) {
            throw new NoSuchElementException(name);
        }
        return name2Tree2Weight.get(name);
    }

    /**
     * Gets the {@link TraversalMethod} object associated with the given name.
     * 
     * @param name
     *            The name to be looked up in the {@link TraversalMethod}
     *            dictionary.
     * @return The corresponding {@link TraversalMethod} instance.
     * @throws NoSuchElementException
     *             when <tt>name</tt> is not associated with any
     *             {@link TraversalMethod} object.
     */
    public TraversalMethod traversalMethodByName(String name) {
        if (!name2TraversalMethod.containsKey(name)) {
            throw new NoSuchElementException(name);
        }
        return name2TraversalMethod.get(name);
    }
}
