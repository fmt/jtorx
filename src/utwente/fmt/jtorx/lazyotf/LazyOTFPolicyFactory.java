package utwente.fmt.jtorx.lazyotf;

import utwente.fmt.jtorx.lazyotf.utilities.Observer;

/**
 * A factory interface for LazyOTF policy packages.
 */
public interface LazyOTFPolicyFactory {
    /**
     * Makes a new LazyOTF policy instance.
     * 
     * @return A new LazyOTF policy instance.
     */
    Observer newLazyOTFPolicyObserver();

    /**
     * Makes a new {@link TraversalDepthComputer} instance.
     * 
     * @return A new {@link TraversalDepthComputer} instance.
     */
    TraversalDepthComputer newTraversalDepthComputer();

    /**
     * Makes a new, unconfigured {@link LocationMgr} instance.
     * 
     * @return A new {@link LocationMgr} instance.
     */
    LocationMgr newLocationManager();

    /**
     * Sets the factory's {@link LazyOTF} instance.
     * 
     * @param inst
     *            The {@link LazyOTF} instance.
     */
    void setLazyOTFInstance(LazyOTF inst);
}
