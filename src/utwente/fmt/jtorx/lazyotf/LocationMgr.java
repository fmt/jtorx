package utwente.fmt.jtorx.lazyotf;

import java.util.Set;

import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightControlInterface;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.strategy.StrategyControlInterface;

/**
 * <p>
 * A class responsible for setting up and keeping track of location-test-type
 * related manager classes - {@link TestTypeAssigner}, {@link TestTypeGroupMgr}.
 * </p>
 */
public interface LocationMgr {

    /**
     * Add a location to the location manager.
     * 
     * @param l
     *            The location to be added.
     */
    void addLocation(Location l);

    /**
     * @return The set of locations managed by this location manager.
     */
    Set<Location> getLocations();

    /**
     * @param name
     *            The requested location's name
     * @return The requested location object.
     */
    Location getLocation(String name);

    /**
     * Sets the strategy control interface for this location manager.
     * 
     * @param it
     *            The new {@link StrategyControlInterface}. If <tt>null</tt>,
     *            the default ("null") strategy control interface is used.
     */
    void setStrategyControlInterface(StrategyControlInterface it);

    /**
     * Sets the {@link Path2Weight} control interface for this location manager.
     * 
     * @param it
     *            The new {@link Path2WeightControlInterface}. If
     *            <tt>null</tt>, the default ("null") {@link Path2Weight} control
     *            interface is used.
     */
    void setPath2WeightControlInterface(
            Path2WeightControlInterface it);

    /**
     * Initialize the location manager and the managers it created.
     */
    void init();

    /**
     * @return The location manager's {@link TestTypeAssigner}.
     */
    TestTypeAssigner getTestTypeAssigner();

    /**
     * @return The location manager's {@link TestTypeGroupMgr}.
     */
    TestTypeGroupMgr getTestTypeGroupMgr();
    
    /**
     * Cleaning up all objects managed by this (e.g. those of {@link utwente.fmt.jtorx.lazyotf.distributed}).
     */
    void cleanUp();
}
