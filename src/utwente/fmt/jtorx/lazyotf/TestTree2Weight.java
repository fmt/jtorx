package utwente.fmt.jtorx.lazyotf;

/**
 * An interface for representations of test tree weighers.
 */
public interface TestTree2Weight {
    /**
     * @return The weigher's name.
     */
    String getName();

    /**
     * Configures the weigher using the given string {@code s}.
     * 
     * @param s
     *            The weigher's configuration string.
     */
    void readFromString(String s);
}
