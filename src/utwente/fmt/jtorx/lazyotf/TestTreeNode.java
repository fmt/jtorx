package utwente.fmt.jtorx.lazyotf;

import java.util.List;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.LazyOTF.InactiveException;
import utwente.fmt.jtorx.lazyotf.location.InstantiatedLocation;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategyHandle;
import utwente.fmt.lts.Label;

/**
 * A class representing LazyOTF test tree nodes.
 */
public interface TestTreeNode {
    /**
     * @return This node's children.
     * @throws InactiveException The LazyOTF Instance is inactive, thus no test
     *         tree nodes can e obtained.
     */
    List<TestTreeNode> getChildren() throws InactiveException;

    /**
     * @return This node's parent node.
     */
    TestTreeNode getParent();

    /**
     * @return This node's weight.
     */
    int getWeight();

    /**
     * @return This node's label.
     */
    Label getLabel();

    /**
     * @return This node's ID.
     */
    int getNodeID();

    /**
     * @return This node's states.
     */
    Set<InstantiatedLocation> getLocations();

    /**
     * @return This node's instantiated locations belonging to the given
     *         uninstantiated location.
     * @param uninstantiatedLocation A location.
     */
    Set<InstantiatedLocation> getLocations(Location uninstantiatedLocation);

    /**
     * @return A set of all test goal states contained within this node.
     */
    Set<InstantiatedLocation> getTestGoals();

    /**
     * @return A set of all inducing non-test-goal states contained within this
     *         node.
     */
    Set<InstantiatedLocation> getInducingStates();

    /**
     * @param type A location test type.
     * @return A set of all locations having the given test type contained in
     *         this node.
     */
    Set<InstantiatedLocation> getLocationsHavingTestType(TestType type);

    /**
     * @return <tt>true</tt> iff the node has any child nodes.
     */
    boolean hasChildren();

    /**
     * Get the non-ordinary strategy evaluation results for a given Location.
     * 
     * @param loc
     *            A Location object belonging to this node.
     * @return A map associating decision strategy handles with the
     *         corresponding strategy evaluation results.
     */
    Map<DecisionStrategyHandle, TestType> getNonOrdinaryStrategyResults(
            InstantiatedLocation loc);
}
