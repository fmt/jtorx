package utwente.fmt.jtorx.lazyotf;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;

/**
 * <p>
 * The {@link TestTypeAssigner} is responsible for translating the location type
 * assignments made by the set of <i>active</i> test type groups to single,
 * static location test type assignments.
 * </p>
 * 
 * <p>
 * The <i>maximal constant</i> test type of a location is the highest- priority
 * location test type assigned by any active test type group to that location.
 * <tt>DECIDED_BY_STRATEGY</tt> assignments are excluded and must be handled
 * separately, as decision strategies are evaluated during the test tree
 * generation, while the assignments by the {@link TestTypeAssigner} are
 * computed beforehand.
 * </p>
 * 
 * <p>
 * The "final" test type of an instantiated(!) location used during the test
 * tree computation is the maximum out of the location's maximal constant test
 * type and the ones assigned by the decision strategies whose domains contain
 * the corresponding location.
 * </p>
 * 
 * <p>
 * <i>testTypeMightBeOverridenByStrategy()</i> evaluates to <tt>true</tt> if the
 * test type might be overridden by a strategy.
 * </p>
 */
public interface TestTypeAssigner {

    /**
     * Gets the maximum constant test type for the given location.
     * 
     * @param loc
     *            A {@link Location}.
     * @return The maximum constant {@link TestType} for the given location. For
     *         the sake of simplicity, <tt>ORDINARY</tt> is assigned to
     *         locations having no defined maximum constant test type (i.e. no
     *         assignment at all or all active test type groups assign
     *         <tt>DECIDED_BY_STRATEGY</tt> to it).
     */
    TestType getMaxConstantTestType(Location loc);

    /**
     * Determines whether the maximum constant test type of the given location
     * might be overridden by a strategy, i.e. if any active test type group
     * assigns <tt>DECIDED_BY_STRATEGY</tt> to the location.
     * 
     * @param loc
     *            A {@link Location}
     * @return <tt>true</tt> iff the location's test type might be overridden by
     *         a strategy.
     */
    boolean isDecidedByStrategy(Location loc);
}
