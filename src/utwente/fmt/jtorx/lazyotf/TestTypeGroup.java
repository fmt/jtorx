package utwente.fmt.jtorx.lazyotf;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategyHandle;

/**
 * TestTypeGroup objects manage the relation of Location objects and TestTypes.
 * A location can be associated with arbitrarily many TestTypeGroups and has a
 * TestType within each such group. A location L determines its test type by
 * selecting the highest-priority test type assigned to it by the active test
 * groups.
 */
public class TestTypeGroup {
    /** A Map associating test types with locations. */
    private final Map<TestType, Set<Location>> typeToStateMap = new HashMap<>();

    /** A Map associating locations with types. */
    private final Map<Location, TestType> stateToTypeMap = new HashMap<>();

    /** true iff this test type group is active (see the doc of isActive(). */
    private boolean isActive = true;

    /** The test type group's name. */
    private String name = "";

    /** The decision strategy associated with this test type group. */
    private DecisionStrategy groupStrategy = null;

    /**
     * A handle to the installed decision strategy within the underlying LazyOTF
     * instance.
     */
    private DecisionStrategyHandle groupStrategyHandle = null;

    /** false ==> isActive() always returns false. */
    private boolean isEnabled;

    /** The set of locations associated with this test type group. */
    private final Set<Location> locations = new HashSet<Location>();

    /** The set of {@link Path2Weight}s used by this test type group. */
    private Set<Path2Weight> path2Weights;

    /**
     * Make a new test type group instance.
     * 
     * @param types
     *            The test types known to this test type group.
     */
    public TestTypeGroup(Set<TestType> types) {
        for (TestType t : types) {
            addTestType(t);
        }
        name = "Default Group";
        isEnabled = true;

        path2Weights = new HashSet<>();
    }

    /**
     * Make a new test type group instance, copying the given test type group.
     * 
     * @param g
     *            The test type group to be copied.
     */
    public TestTypeGroup(TestTypeGroup g) {
        this(g.getTestTypes());

        for (TestType tt : getTestTypes()) {
            for (Location l : g.getStates(tt)) {
                associate(l, tt);
            }
        }

        setPath2Weights(g.getPath2Weights());
    }

    /**
     * @return true iff this test type group is active, i.e. its test type
     *         associations get considered during the test type redetermination
     *         of the locations within this group. Test type groups are active
     *         iff they are enabled and set to active (or enabled and set to
     *         permanently active).
     */
    public boolean isActive() {
        return isEnabled && isActive;
    }

    /**
     * Set the activeness of this test type group. (see the doc of isActive().)
     * 
     * @param activeness
     *            true iff this test type group is to be active.
     */
    public void setActive(boolean activeness) {
        isActive = activeness;
    }

    /**
     * Add a test type to this test type group.
     * 
     * @param type
     *            the test type to be added.
     */
    public void addTestType(TestType type) {
        typeToStateMap.put(type, new HashSet<Location>());
    }

    /**
     * Remove a test type from this test type group.
     * 
     * @param type
     *            the test type to be removed.
     */
    public void removeTestType(TestType type) {
        if (typeToStateMap.containsKey(type)) {
            typeToStateMap.remove(type);
            for (Location loc : stateToTypeMap.keySet()) {
                if (stateToTypeMap.get(loc).equals(type)) {
                    stateToTypeMap.remove(loc);
                }
            }
        }
        else {
            throw new IllegalArgumentException("Unknown test type " + type);
        }
    }

    /**
     * Associate a location with the given test type.
     * 
     * @param state
     *            the location.
     * @param type
     *            the test type.
     */
    public void associate(Location state, TestType type) {
        if (stateToTypeMap.containsKey(state)) {
            return;
        }

        Set<Location> stateSet = typeToStateMap.get(type);
        if (stateSet != null) {
            typeToStateMap.get(type).add(state);
        }
        else {
            throw new IllegalArgumentException("Unknown test type " + type);
        }
        stateToTypeMap.put(state, type);

        locations.add(state);
    }

    /**
     * Dissociate a location from a given test type.
     * 
     * @param state
     *            the location.
     */
    public void dissociate(Location state) {
        Set<Location> stateSet = typeToStateMap.get(getTestType(state));
        if (stateSet != null) {
            typeToStateMap.get(getTestType(state)).remove(state);
        }
        else {
            throw new IllegalArgumentException("Unknown test type " + getTestType(state));
        }
        stateToTypeMap.remove(state);

        locations.remove(state);
    }

    /**
     * Get the locations contained in this test type group which are associated
     * with the given test type.
     * 
     * @param type
     *            The test type.
     * @return The requested location if it exists; null otherwise.
     */
    public Set<Location> getStates(TestType type) {
        Set<Location> result = typeToStateMap.get(type);
        if (result == null) {
            return new HashSet<Location>();
        }
        else {
            Set<Location> resultClone = new HashSet<Location>();
            resultClone.addAll(result);
            return resultClone;
        }
    }

    /**
     * @return The set of {@link Location}s associated with this test type
     *         group.
     */
    public Set<Location> getStates() {
        Set<Location> clone = new HashSet<Location>();
        clone.addAll(locations);
        return clone;
    }

    /**
     * Get a given location's test type within this test type group.
     * 
     * @param l
     *            The location whose test type is requested.
     * @return The location's test type within this group; null if the given
     *         location is not associated with this test type group.
     */
    public TestType getTestType(Location l) {
        return stateToTypeMap.get(l);
    }

    /**
     * @return The set of test types used by this test type group.
     */
    public Set<TestType> getTestTypes() {
        return typeToStateMap.keySet();
    }

    /**
     * @return This test type group's name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            This test type group's new name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param location A location.
     * @return true iff this test type group contains the given location.
     */
    public boolean contains(Location location) {
        return !(getTestType(location) == null);
    }

    /**
     * Initialize the test type group by adding it to each of it's locations'
     * set of test type groups, thus preparing it for the test run.
     */
    public void init() {
        setActive(true);
    }

    /**
     * @param enabled
     *            true iff this test type group should be enabled.
     */
    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    /**
     * @return true iff this test type group is enabled.
     */
    public boolean isEnabled() {
        return isEnabled;
    }

    /**
     * Sets the test type groups {@link DecisionStrategy}. If the group should
     * not have a {@link DecisionStrategy}, set this to <tt>null</tt> (which is
     * the default).
     * 
     * @param it
     *            The new {@link DecisionStrategy} object.
     */
    public void setStrategy(DecisionStrategy it) {
        groupStrategy = it;
    }

    /**
     * @return The group's <tt>DecisionStrategy</tt> object (or <tt>null</tt> if
     *         no decision strategy is associated with the test type group).
     */
    public DecisionStrategy getStrategy() {
        return groupStrategy;
    }

    /**
     * Sets the handle to the group's installed decision strategy.
     * 
     * @param it
     *            The new {@link DecisionStrategyHandle}.
     */
    public void setStrategyHandle(DecisionStrategyHandle it) {
        groupStrategyHandle = it;
    }

    /**
     * @return The group's installed decision strategy handle.
     */
    public DecisionStrategyHandle getStrategyHandle() {
        return groupStrategyHandle;
    }

    /**
     * Sets the set of {@link Path2Weight}s used by the test type group.
     * 
     * @param methods
     *            The set of {@link Path2Weight}s.
     */
    public void setPath2Weights(Set<Path2Weight> methods) {
        path2Weights = methods;
    }

    /**
     * @return The set of {@link Path2Weight}s used by the test type group.
     */
    public Set<Path2Weight> getPath2Weights() {
        assert path2Weights != null;
        return path2Weights;
    }
}
