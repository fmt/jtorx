package utwente.fmt.jtorx.lazyotf;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.distributed.DistributeLazyOTF;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightControlInterface;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategyHandle;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;

/**
 * <p>
 * The {@link TestTypeGroupMgr} is responsible for executing the activation and
 * deactivation of test type groups as well as keeping track of the installed
 * ones.
 * </p>
 * 
 * <p>
 * Instances of this class maintain a map associating each {@link Location} with
 * a test-type->test-type-group map, which in turn associates test types with
 * the set of test type groups assigning the test type at hand to the location
 * at hand.
 * <p>
 * 
 * <p>
 * This class issues the deactivation of decision strategies and guidance
 * methods related to discharged test type groups. Location test type
 * invalidations are not carried out directly, but are observable events.
 * </p>
 */
public interface TestTypeGroupMgr {

	/**
	 * @return All test type groups managed by the test type group manager
	 *         instance.
	 */
	Set<TestTypeGroup> getTestTypeGroups();

	/**
	 * Sets the {@link Path2Weight} control interface for the location manager.
	 * 
	 * @param it
	 *            The new {@link Path2WeightControlInterface}.
	 */
	void setPath2WeightControlInterface(
			Path2WeightControlInterface it);

	/**
	 * Adds a test type group to this test type group manager.
	 * 
	 * @param g
	 *            The test type group to be added.
	 */
	void addTestTypeGroup(TestTypeGroup g);

	/**
	 * Initializes the test type group manager instance by initializing it for
	 * each test type group.
	 */
	void init();

	/**
	 * @param l
	 *            A {@link Location} object.
	 * @return The corresponding map associating all test types with the set of
	 *         test type groups assigning the test type at hand to the given
	 *         location. This map has a key for each test type.
	 */
	Map<TestType, Set<TestTypeGroup>> getTestTypeGroupsByTestType(
			Location l);

	/**
	 * Removes a test type group from the set of test type groups associated
	 * with the given location. This method is deprecated; However, a unit test
	 * still uses it, precluding deletion.
	 * 
	 * @param l
	 *            A {@link Location}.
	 * @param g
	 *            The {@link TestTypeGroup} which is to be removed from the set
	 *            of test type groups having a test type association for
	 *            <i>l</i>.
	 */
	@Deprecated
	void removeTestTypeGroupFromLocation(Location l,
			TestTypeGroup g);

	/**
	 * Registers the event that the test traversed a location for which the
	 * {@link TestTypeGroup} <i>g</i> assigned the test type <i>type</i>. The
	 * test type group's activeness is updated accordingly (i.e. if <i>type</i>
	 * is <tt>TEST_GOAL</tt>, the group is deactivated).
	 * 
	 * @param g
	 *            A {@link TestTypeGroup} previously added to the test type
	 *            group manager.
	 * @param type
	 *            A {@link TestType} as described above.
	 * @param deactivatedSet
	 *            A set of {@link TestTypeGroup}s. If <i>g</i> gets deactivated
	 *            by this call, it is added to this set. May be <tt>null</tt>.
	 */
	void dischargeTestTypeGroup(TestTypeGroup g, TestType type,
			Collection<TestTypeGroup> deactivatedSet);

	/**
	 * Registers the event that the test traversed a location for which the
	 * {@link TestTypeGroup} <i>g</i> assigned the test type <i>type</i>. The
	 * test type group's activeness is updated accordingly (i.e. if <i>type</i>
	 * is <tt>TEST_GOAL</tt>, the group is deactivated).
	 * 
	 * @param g
	 *            A {@link TestTypeGroup} previously added to the test type
	 *            group manager.
	 * @param type
	 *            A {@link TestType} as described above.
	 */
	void dischargeTestTypeGroup(TestTypeGroup g, TestType type);

	/**
	 * Sets the activeness of the given test type group, issuing the test type
	 * invalidation within the {@link TestTypeAssigner} belonging to the given
	 * location manager.
	 * 
	 * @param g
	 *            A {@link TestTypeGroup}.
	 * @param enabledness
	 *            The new activeness of <i>g</i>.
	 */
	void setActive(TestTypeGroup g, boolean enabledness);

	/**
	 * Sets the {@link DistributeLazyOTF} for distribution.
	 * @param it The {@link DistributeLazyOTF} to be used for distribution.
	 */
	void setDistributeLazyOTF(DistributeLazyOTF it);

	/**
	 * @return The test type group manager's {@link DistributeLazyOTF} instance.
	 */
	DistributeLazyOTF getDistributeLazyOTF();

	/**
	 * @return the number of TTGs that have been discharged only via {@link DistributeLazyOTF}.
	 */
	int getNumberOfTTGsDischargedFromExtern();

	/**
	 * @return The set of active test type groups having a test goal or strategy
	 *         assignment.
	 */
	Set<TestTypeGroup> getActiveTestTypeGroupsWithTestGoal();

	/**
	 * Processes the indication that a test goal was reached. Issues the
	 * deactivation of all test type groups assigning the test goal type to the
	 * given location.
	 * 
	 * @param testGoal
	 *            The test goal location.
	 * @param deactivatedTestTypeGroups
	 *            A set of {@link TestTypeGroup}s. Unless <tt>null</tt>, test
	 *            type groups which get deactivated during the course of the
	 *            method call are added to this set.
	 */
	void hitTestGoal(Location testGoal,
			Set<TestTypeGroup> deactivatedTestTypeGroups);

	/**
	 * Processes the indication that a test goal was reached due to a decision
	 * strategy test type assignment. Issues the deactivation of the
	 * corresponding test type group.
	 * 
	 * @param byStrategy
	 *            The decision strategy which assigned the test goal type.
	 * @param deactivatedTestTypeGroups
	 *            A set of {@link TestTypeGroup}s. Unless <tt>null</tt>, test
	 *            type groups which get deactivated during the course of the
	 *            method call are added to this set.
	 */
	void hitTestGoal(DecisionStrategyHandle byStrategy,
			Set<TestTypeGroup> deactivatedTestTypeGroups);

	/**
	 * Determines whether all test objectives have been discharged.
	 * 
	 * @return {@true} if all test objectives have been discharged.
	 */
	boolean isEveryTestObjectiveDischarged();

	void cleanUp();

	/**
	 * Adds an event observer to the object.
	 * 
	 * The events are of type {@link LocationTestTypeInvalidatedEvent}.
	 * 
	 * @param observer
	 *            The observer object, implementing {@link Observer}.
	 * @param category
	 *            The observer's event category.
	 */
	public void addObserver(Observer observer, EventCategory category);

	/** The event observer categories for this class. */
	public enum EventCategory {
		/**
		 * Event: A location test type has been invalidated and needs to be recomputed.
		 */
		LOCATION_TEST_TYPE_INVALIDATED,

		/**
		 * Event: A test type group has been discharged.
		 */
		TEST_TYPE_GROUP_DISCHARGED;
	}

	/**
	 * An event class describing the invalidation of a {@link TestTypeGroup}
	 * instance.
	 */
	public final class TestTypeGroupDischargedEvent {
		/** The discharged test type group. */
		public final TestTypeGroup dischargedGroup;

		/**
		 * Constructs a new {@link TestTypeGroupDischargedEvent} instance.
		 * @param dischargedGroup The discharged test type group.
		 */
		public TestTypeGroupDischargedEvent(TestTypeGroup dischargedGroup) {
			this.dischargedGroup = dischargedGroup;
		}
	}

	/**
	 * An event class describing the invalidation of {@link Location} test
	 * types.
	 */
	public final class LocationTestTypeInvalidatedEvent {
		/**
		 * Constructs a new {@link LocationTestTypeInvalidatedEvent} instance.
		 * @param invalidatedLocations The set of location objects whose test
		 *         have become invalid.
		 * @param isInvalidatingMaxConstantMap {@code true} iff the max. constant
		 *         mapping strategy needs to be updated for the locations in
		 *         {@code invalidatedLocations}.
		 */
		public LocationTestTypeInvalidatedEvent(Set<Location> invalidatedLocations,
				boolean isInvalidatingMaxConstantMap) {
			this.invalidatedLocations = invalidatedLocations;
			this.isInvalidatingMaxConstantMap = isInvalidatingMaxConstantMap;
		}

		/**
		 * @return The {@link Location} objects whose test types have become
		 *         invalid.
		 */
		public final Set<Location> invalidatedLocations;

		/**
		 * @return <tt>true</tt> iff the max. constant mapping strategy needs to
		 *         be updated for the locations in
		 *         <tt>getInvalidatedLocations()</tt>.
		 */
		public final boolean isInvalidatingMaxConstantMap;
	}
}
