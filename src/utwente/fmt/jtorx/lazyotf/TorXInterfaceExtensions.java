package utwente.fmt.jtorx.lazyotf;

import java.util.Map;

/**
 * An interface to TorX states.
 */
public interface TorXInterfaceExtensions {
    /**
     * Gets the valuation of the superlocation represented by the given string
     * stateID.
     * 
     * @param stateID
     *            The TorX state ID.
     * @return A map from variable names to their respective value.
     */
    Map<String, String> getValuation(String stateID);
}
