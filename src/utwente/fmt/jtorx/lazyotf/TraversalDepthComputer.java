package utwente.fmt.jtorx.lazyotf;

import java.util.Observer;

import utwente.fmt.jtorx.lazyotf.location.TestType;

/**
 * An interface for model traversal depth computation.
 * 
 * These classes are used to determine the model traversal depth dependent on
 * the sequence of test types sampled at the leafs of test trees.
 */
public interface TraversalDepthComputer {
    /**
     * Indicate that a test tree leaf was traversed.
     * 
     * @param type
     *            The leaf's location test type.
     */
    void hit(TestType type);

    /**
     * Gets the computed model traversal depth.
     * 
     * @return The computed model traversal depth.
     */
    int getTraversalDepth();

    /**
     * Adds an observer to the traversal depth computer.
     * 
     * @param obs
     *            An observer handling events
     */
    void addObserver(Observer obs);
}
