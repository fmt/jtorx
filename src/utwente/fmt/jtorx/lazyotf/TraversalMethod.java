package utwente.fmt.jtorx.lazyotf;

/**
 * An interface for representations of test tree traversal methods.
 */
public interface TraversalMethod {

    /**
     * @return The method's name.
     */
    String getName();

    /**
     * Configures the traversal method using the given string {@code s}.
     * 
     * @param s
     *            The traversal method's configuration string.
     */
    void readFromString(String s);
}
