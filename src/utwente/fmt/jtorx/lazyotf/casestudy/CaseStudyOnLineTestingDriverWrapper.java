package utwente.fmt.jtorx.lazyotf.casestudy;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver;
import utwente.fmt.jtorx.lazyotf.policy.LazyOTFPolicyFactoryImpl;
import utwente.fmt.jtorx.lazyotf.remoteimpl.RemoteLazyOTF;
import utwente.fmt.jtorx.testnongui.casestudy.CaseStudyIO;
import utwente.fmt.jtorx.testnongui.casestudy.CaseStudyMainWrapper;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.torx.DriverInterActionResultConsumer;
import utwente.fmt.jtorx.torx.LabelPlusVerdict;
import utwente.fmt.jtorx.torx.OnLineTestingDriver;
import utwente.fmt.jtorx.torx.explorer.torx.TorxExplorer;
import utwente.fmt.jtorx.torx.guided.GuidedLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;

/**
 * A wrapper for {@link OnLineTestingDriver}s measuring the CPU time spent
 * during {@link # autoStep(int, DriverInterActionResultConsumer) autostep}
 * calls. The wrapper does not influence testing behaviour.
 */
public class CaseStudyOnLineTestingDriverWrapper implements OnLineTestingDriver {
	/** The wrapped driver. */
	private final OnLineTestingDriver wrappedDriver;

	/** The simplified test objectives for JTorX tests. */
	private final Set<SimpleTO> pureJTorXTestObjectives;

	/** The set of discharged simplified test objectives for JTorX tests. */
	private final Set<SimpleTO> dischargedPureJTorXTestObjectives;

	/** Set to {@link true} in order to stop autostepping. */
	private boolean stop = false;

	/** Set to {@link true} in order to enable the simplified test objectives. */
	private boolean pureJTorXMode = false;

	/** Specifies a sleep period between test steps when using autostep. */ 
	private long delayAfterTeststepMs = 0l;

	/** The amount of executed test steps. */
	private long executedTestSteps;

	private static final Logger LOGGER = Logger.getLogger(CaseStudyOnLineTestingDriverWrapper.class.getName());

	/**
	 * Constructs a new {@link CaseStudyOnLineTestingDriverWrapper} instance.
	 * 
	 * @param wrappedDriver
	 *            The wrapped JTorX driver.
	 */
	public CaseStudyOnLineTestingDriverWrapper(OnLineTestingDriver wrappedDriver) {
		this.wrappedDriver = wrappedDriver;
		this.pureJTorXTestObjectives = new HashSet<>();
		this.dischargedPureJTorXTestObjectives = new HashSet<>();
		this.executedTestSteps = 0;

		/*
		 * Send bogus command to LazyOTF in order to get correct time measuring
		 * names
		 */
		TorxExplorer et;
		try {
			LTS modelLTS = wrappedDriver.getTesterState().getLTS();

			if (modelLTS instanceof GuidedLTS) {
				et = (TorxExplorer) ((GuidedLTS) modelLTS).getSpecs().get(0).getSubLTS();
			}
			else {
				et = (TorxExplorer) wrappedDriver.getTesterState().getLTS().getSubLTS();
			}
		}
		catch (NullPointerException e) {
			throw new IllegalStateException("Could not get model LTS, aborting the test run. This is likely caused by a previous error.");
		}
		synchronized (et.getIn()) {
			try {
				et.getOut().write("LazyOTF:get bogus\n".getBytes());
				et.getOut().flush();

				// Read LazyOTF complaint
				et.getIn().readLine();
			}
			catch (IOException e) {
				throw new IllegalStateException();
			}
		}

		InstantiationHooks.getInstance().applyHooks(this);
		if (pureJTorXMode) {
			// A modelless LazyOTF instance is set up to provide a TorX interface.
			RemoteLazyOTF lotfInst = new RemoteLazyOTF(et.getIn(),
					et.getOut(), new LazyOTFPolicyFactoryImpl(new LazyOTFConfig()), true);
			TorXInterfaceExtensions tif = lotfInst.getTorXInterfaceExtensions();

			for (SimpleTO to : pureJTorXTestObjectives) {
				to.setTorxInterface(tif);
				to.initialize(wrappedDriver.getTesterState().getSubStates());
			}
		}
	}

	public long getDelayAfterTeststepMs() {
		return delayAfterTeststepMs;
	}

	public void setDelayAfterTeststepMs(long delayAfterTeststepMs) {
		this.delayAfterTeststepMs = delayAfterTeststepMs;
		if (this.wrappedDriver instanceof LazyOTFAwareSimOnlineTestingDriver) {
			((LazyOTFAwareSimOnlineTestingDriver)this.wrappedDriver).setDelayAfterTeststepMs(delayAfterTeststepMs);
		}
	}

	public void setPureJTorXModeEnabled(boolean enableJTorxMode) {
		this.pureJTorXMode = enableJTorxMode;
	}

	public void addJTorXTestObjective(SimpleTO jtorxTO) {
		this.pureJTorXTestObjectives.add(jtorxTO);
	}

	@Override
	public DriverInitializationResult init() {
		return wrappedDriver.init();
	}

	@Override
	public DriverFinalizationResult finish() {
		DriverFinalizationResult result = wrappedDriver.finish();
		return result;
	}

	@Override
	public DriverInterActionResult stimulate(Label l) {
		executedTestSteps++;
		return wrappedDriver.stimulate(l);
	}

	@Override
	public DriverInterActionResult stimulate() {
		executedTestSteps++;
		return wrappedDriver.stimulate();
	}

	@Override
	public DriverInterActionResult observe() {
		return wrappedDriver.observe();
	}

	@Override
	public Vector<Label> getModelOutputs() {
		return wrappedDriver.getModelOutputs();
	}

	@Override
	public Vector<LabelPlusVerdict> getModelOutputVerdicts() {
		return wrappedDriver.getModelOutputVerdicts();
	}

	@Override
	public Vector<Label> getModelInputs() {
		return wrappedDriver.getModelInputs();
	}

	@Override
	public CompoundState getTesterState() {
		return wrappedDriver.getTesterState();
	}

	@Override
	public DriverInterActionResult randomStep() {
		return wrappedDriver.randomStep();
	}

	@Override
	public boolean autoStep(int n, DriverInterActionResultConsumer c) {
		executedTestSteps = 0;
		startAutostepTimeMeasuring();
		if (pureJTorXMode) {

			// In order to check the SimpleTOs, we have to do autostepping
			// ourselves.
			// The code is taken from CompoundSimOnLineTestingDriver.

			Boolean goOn = true;
			Boolean forever = (n <= 0);
			int i = n;
			// System.out.println("run starting");
			stop = false;
			while (goOn && !stop && (forever || i > 0)) {
				DriverInterActionResult result = randomStep();
				executedTestSteps++;
				goOn = c.consume(result);
				goOn &= checkTestObjectives(result);
				i--;

				if (this.delayAfterTeststepMs > 0l) {
					LOGGER.info("Sleeping for " + this.delayAfterTeststepMs + " ms "
							+ "after executing a test step.");
					try {
						Thread.sleep(this.delayAfterTeststepMs);
					} catch (InterruptedException e) {
						LOGGER.warning("Post-teststep sleep interrupted.");
					}
				}
			}
			c.end();
			// /code from CompoundSimOnLineTestingDriver

			stopAutostepTimeMeasuring();

			return goOn;
		}
		else {
			boolean result = wrappedDriver.autoStep(n, c);
			stopAutostepTimeMeasuring();
			return result;
		}
	}

	private boolean checkTestObjectives(DriverInterActionResult result) {
		boolean doContinue = true;

		Iterator<SimpleTO> simpleTOIterator = pureJTorXTestObjectives.iterator();
		while (simpleTOIterator.hasNext()) {
			SimpleTO to = simpleTOIterator.next();
			to.registerInteraction(result.getLabel(), wrappedDriver.getTesterState().getSubStates());
			if (to.isTODischarged()) {
				simpleTOIterator.remove();
				doContinue = !pureJTorXTestObjectives.isEmpty();
				dischargedPureJTorXTestObjectives.add(to);
			}            
		}

		return doContinue;
	}

	@Override
	public boolean autoStep(DriverInterActionResultConsumer i) {
		return autoStep(1, i);
	}

	/** The autostep CPU start time in milliseconds. */
	private long autostepStartTime = -1;

	/**
	 * Starts measuring the elapsed CPU time for the autostep mode.
	 */
	private void startAutostepTimeMeasuring() {
		CaseStudyMainWrapper.printTotalCPUTimeSetup();
		autostepStartTime = MeasureTime.getCurrentThreadCPUTime();
	}

	/**
	 * Stops measuring the elapsed CPU time for the autostep mode ands prints
	 * the results along with the process-wide total spent CPU time.
	 */
	private void stopAutostepTimeMeasuring() {
		if (autostepStartTime != -1) {
			long stopTime = MeasureTime.getCurrentThreadCPUTime();
			long elapsed = (stopTime - autostepStartTime);
			CaseStudyIO.out.print("[time] JTorX_Fin_ElapsedInJTorXAutostepMode_MXBean: " + elapsed + "\n");
			printTotalCPUTime();
			CaseStudyIO.out.print("[stats] ExecutedTestSteps: " + executedTestSteps + "\n");
			CaseStudyIO.out.print("[stats] DischargedTO: " + dischargedPureJTorXTestObjectives.size() + "\n");
			CaseStudyIO.out.print("[stats] MissedTO: " + pureJTorXTestObjectives.size() + "\n");
			CaseStudyIO.out.print("[stats] ElapsedMilliseconds: -1" + "\n");
			autostepStartTime = -1;
		}
		else {
			throw new IllegalStateException("Called stopAutostepTimeMeasuring() without measuring");
		}
	}

	/**
	 * Prints the process-wide total spent CPU time.
	 */
	private static void printTotalCPUTime() {
		long totalCPUTimeSigar = MeasureTime.getCPUTime();
		CaseStudyIO.out.print("[time] JTorX_Fin_TotalElapsedUntilStopAuto_Sigar: " + totalCPUTimeSigar + "\n");
		long totalCPUTimeLinux = MeasureTime.getLinuxCPUTime(MeasureTime.getPID());
		CaseStudyIO.out.print("[time] JTorX_Fin_TotalElapsedUntilStopAuto_Linux: " + totalCPUTimeLinux + "\n");
	}

	@Override
	public void stopAuto() {
		stop = true;
		wrappedDriver.stopAuto();
	}

	/**
	 * Singleton class for managing hooks being executed when a
	 * {@link CaseStudyOnLineTestingDriverWrapper} is created.
	 */
	public static class InstantiationHooks {
		/** The singleton's instance. */
		private final static InstantiationHooks instance = new InstantiationHooks();

		/** The set of hooks managed by the singleton. */
		private final Set<InstantiationHook> hooks;

		/**
		 * Adds a hook to the set of {@link CaseStudyOnLineTestingDriverWrapper}
		 * instantiation hooks.
		 * @param hook The new hook.
		 */
		public void addHook(InstantiationHook hook) {
			hooks.add(hook);
		}

		/**
		 * Applies the hooks managed by this singleton to the <tt>target</tt>
		 * {@link CaseStudyOnLineTestingDriverWrapper} instance.
		 * @param target The target {@link CaseStudyOnLineTestingDriverWrapper}
		 *   instance.
		 */
		private void applyHooks(CaseStudyOnLineTestingDriverWrapper target) {
			for (InstantiationHook hook : hooks) {
				hook.applyHook(target);
			}
		}

		/**
		 * Removes all hooks from this singleton.
		 */
		public void reset() {
			hooks.clear();
		}

		/**
		 * @return The class' singleton instance.
		 */
		public static InstantiationHooks getInstance() {
			return instance;
		}

		/**
		 * Constructs the {@link InstantiationHooks} singleton.
		 */
		private InstantiationHooks() {
			this.hooks = new HashSet<>();
		}

		/**
		 * The {@link CaseStudyOnLineTestingDriverWrapper} creation-time hook
		 * interface. 
		 */
		public interface InstantiationHook {
			/**
			 * Applies the hook to the target {@link CaseStudyOnLineTestingDriverWrapper}.
			 * @param target The target {@link CaseStudyOnLineTestingDriverWrapper}.
			 */
			void applyHook(CaseStudyOnLineTestingDriverWrapper target);
		}
	}
}
