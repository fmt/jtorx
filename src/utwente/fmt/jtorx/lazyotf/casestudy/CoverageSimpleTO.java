package utwente.fmt.jtorx.lazyotf.casestudy;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;

public class CoverageSimpleTO implements SimpleTO {
    
    private final static int MAX_LOCATIONSET_SIZE = 19;
    private Set<String> visitedLocations = new HashSet<>();
    
    @Override
    public void initialize(Iterator<? extends State> initialStates) {
    }

    @Override
    public void registerInteraction(Label interaction, Iterator<? extends State> torxStates) {
        while (torxStates.hasNext()) {
            State s = torxStates.next();
            if (!s.getNodeName().equals("nshowMoreLicenses")) {
                visitedLocations.add(s.getNodeName());
            }
        }
        
        if (visitedLocations.size() == MAX_LOCATIONSET_SIZE) {
            System.err.println("Discharging CoverageSimpleTO");
        }
    }

    @Override
    public boolean isTODischarged() {
        assert visitedLocations.size() <= MAX_LOCATIONSET_SIZE;
        return visitedLocations.size() == MAX_LOCATIONSET_SIZE;
    }

	@Override
	public void setTorxInterface(TorXInterfaceExtensions tif) {
	}
}
