package utwente.fmt.jtorx.lazyotf.casestudy;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;

public class DecreaseLicenseCountSimpleTO implements SimpleTO {
    private final static String TARGET_EXIT_TRANSLABEL = "!removeLicenseOutput(\"ok\")";
    private final static Set<String> RESET_STATES;
    
    static {
        RESET_STATES = new HashSet<>();
        RESET_STATES.add("ngenerateLicenseRequested");
    }
    
    private final int target;
    
    private int targetCounter = 0;
    private boolean isDischarged = false;
    
    public DecreaseLicenseCountSimpleTO(int target) {
        this.target = target;
    }

    @Override
    public void initialize(Iterator<? extends State> initialStates) {
    }

    @Override
    public void registerInteraction(Label interaction, Iterator<? extends State> torxStates) {
        
        if (interaction.getString().equals(TARGET_EXIT_TRANSLABEL)) {
            targetCounter++;
        }
        
        while (torxStates.hasNext()) {
            State s = torxStates.next();            
            if (RESET_STATES.contains(s.getNodeName())) {
                targetCounter = 0;
            }
        }
       
        if (targetCounter >= target) {
            isDischarged = true;
            System.err.println("Discharging DecreaseLicenseCountSimpleTO.");
        }
    }

    @Override
    public boolean isTODischarged() {
        return isDischarged;
    }

	@Override
	public void setTorxInterface(TorXInterfaceExtensions tif) {
	}
}
