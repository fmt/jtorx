package utwente.fmt.jtorx.lazyotf.casestudy;

import java.util.Iterator;

import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;

public class FindShowMoreLicensesSimpleTO implements SimpleTO {
    private static final String LOCATION_NAME = "nshowMoreLicenses";
    
    boolean doContinue = true;
    
    public FindShowMoreLicensesSimpleTO() {
    }
    
    @Override
    public void registerInteraction(Label interaction, Iterator<? extends State> torxStates) {
        while(torxStates.hasNext()) {
            State s = torxStates.next();
            if (s.getNodeName().equals(LOCATION_NAME)) {
                doContinue = false;
                System.err.println("Discharging FindShowMoreLicensesSimpleTO.");
            }
        }
    }

    @Override
    public boolean isTODischarged() {
        return !doContinue;
    }

    @Override
    public void initialize(Iterator<? extends State> initialStates) {
        registerInteraction(null, initialStates);
    }

	@Override
	public void setTorxInterface(TorXInterfaceExtensions tif) {
	}
}
