package utwente.fmt.jtorx.lazyotf.casestudy;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.driver.AbstractDriverObserver;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverFinalizationEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverInitializedEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverSUTInteractionEvent;
import utwente.fmt.jtorx.lazyotf.driver.PolicyObserverFactoryRegistry;
import utwente.fmt.jtorx.lazyotf.driver.PolicyObserverFactoryRegistry.PolicyObserverFactory;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2WeightImpl;
import utwente.fmt.jtorx.lazyotf.guidance.Locations2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverMethod;

public class LicenseCountCoveragePassiveTO extends AbstractDriverObserver {

    private final LazyOTF lazyOTF;
    private final int minLicGenVal;
    private final int maxLicGenVal;
    
    public LicenseCountCoveragePassiveTO(LazyOTF lazyOTF, int min, int max) {
        this.lazyOTF = lazyOTF;
        this.minLicGenVal = min;
        this.maxLicGenVal = max;
    }
    
    private final String[] locations = {
            "generateLicenseRequested",
            "showLicensesUnknownException",
            "remoteBackupDiskFullLocation",
            "remoteBackupUnknownExceptionLocation",
            "generateLicenseUnknownException",
            "localBackupDiskFullLocation",
            "backupMenu",
            "remoteBackupRequest",
            //"showMoreLicenses",
            "initLocation",
            "localBackupUnknownExceptionLocation",
            "removeAllRequested",
            "removeLicenseRequested",
            "localBackupRequest",
            "removeAllUnknownException",
            "showLicensesRequest",
            "removeLicenseUnknownException",
            "removeLicenseLicenseNotFoundException",
    };
    
    private final String[] excludedFromZero = {
        "removeLicenseUnknownException",
        "removeLicenseLicenseNotFoundException",        
    };
    
    @Override
    @ObserverMethod
    public void onSUTInteraction(DriverSUTInteractionEvent eir) {
    }
    
    @Override
    @ObserverMethod
    public void onFinish(DriverFinalizationEvent dfr) {
    }
    
    private void addTO(String locationName, int licenseCount, CustomPath2Weight path2w) {
        Location loc = new Location(locationName);
        SatStrategy satStrat = new SatStrategy();
        satStrat.associate(loc, TestType.TEST_GOAL, new SatStrategy.DumontCondition("licenseCount == " + licenseCount));
        TestTypeGroup ttg = new TestTypeGroup(TestType.ALL_TEST_TYPES);
        ttg.setName("Cover " + locationName + "." + licenseCount);
        ttg.associate(loc, TestType.DECIDED_BY_STRATEGY);
        ttg.setStrategy(satStrat);
        ttg.getPath2Weights().add(path2w);
        lazyOTF.getLocationManager().getTestTypeGroupMgr().addTestTypeGroup(ttg);
    }

    private boolean isExcludedFromZero(String locationName) {
        for (String s : excludedFromZero) {
            if (s.equals(locationName)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    @ObserverMethod
    public void onInit(DriverInitializedEvent dsir) {
        System.out.println("Adding a LicenseGeneratorCoveragePassiveTO.");

        Locations2Weight loc2W = new Locations2Weight("TGLoc2W", 1, 100, 1000*1000*10);
        CustomPath2Weight tgPath2W = new CustomPath2WeightImpl("NondetFiningFinalLocationSetPath2W",
                "TGPath2W");
        tgPath2W.setNodeValue2Weight(loc2W);
                
        for (int i = minLicGenVal; i <= maxLicGenVal; i++) {
            for (String loc : locations) {
                if (!(i == 0 && isExcludedFromZero(loc))) {
                    addTO(loc, i, tgPath2W);
                }
            }
        }
    }
    
    public static void registerWithPrePolicyObserverFactory(final int min, final int max) {
    	PolicyObserverFactoryRegistry.getInstance().addPrePolicyObserverFactory(new PolicyObserverFactory() {
    		@Override
    		public Observer makeObserver(LazyOTF lazyOTFInstance) {
    			return new LicenseCountCoveragePassiveTO(lazyOTFInstance, min, max);
    		}
    	});
    }
}
