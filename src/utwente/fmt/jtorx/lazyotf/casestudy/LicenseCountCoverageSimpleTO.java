package utwente.fmt.jtorx.lazyotf.casestudy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;

public class LicenseCountCoverageSimpleTO implements SimpleTO  {

	private static final String LICENSECOUNT = "licenseCount";
	private final Map<String, Set<Integer>> remainingObligations;
	private int remainingObligationsTotalSize;
	private TorXInterfaceExtensions torxInterface;

	public LicenseCountCoverageSimpleTO(TorXInterfaceExtensions torxInterface,
			int minLicenseCount, int maxLicenseCount) {
		this(minLicenseCount, maxLicenseCount);
		this.torxInterface = torxInterface;
	}
	
	public LicenseCountCoverageSimpleTO(int minLicenseCount, int maxLicenseCount) {
		this.remainingObligations = new HashMap<>();
		setupObligations(minLicenseCount, maxLicenseCount);
		this.torxInterface = new TorXInterfaceExtensions() {
			@Override
			public Map<String, String> getValuation(String stateID) {
				throw new IllegalStateException("Need to set TorX interface before "
						+ "using the LicenseCountCoverageSimpleTO.");
			}
		};
	}

	private void setupObligations(int minLicenseCount, int maxLicenseCount) {
		remainingObligationsTotalSize = 0;
		for (int i = minLicenseCount; i <= maxLicenseCount; i++) {
			setupObligationsForLicenseCountValue(i);
		}
	}

	private boolean isLicenseGeneratorStateReachable(LicenseGeneratorState s, int lc) {
		if (s == LicenseGeneratorState.REMOVELICENSELICENSENOTFOUNDEXCEPTION
				|| s == LicenseGeneratorState.REMOVELICENSEUNKNOWNEXCEPTION) {
			return lc != 0;
		}
		else {
			return true;
		}
	}

	private void addObligation(LicenseGeneratorState s, int licenseCountValue) {
		if (!remainingObligations.containsKey(s.getName())) {
			remainingObligations.put(s.getName(), new HashSet<Integer>());
		}
		Set<Integer> obligationsForS = remainingObligations.get(s.getName());
		obligationsForS.add(licenseCountValue);
		remainingObligationsTotalSize++;
	}

	private void setupObligationsForLicenseCountValue(int licenseCountValue) {
		for (LicenseGeneratorState s : LicenseGeneratorState.values()) {
			if (s != LicenseGeneratorState.SHOWMORELICENSES 
					&& isLicenseGeneratorStateReachable(s, licenseCountValue)) {
				addObligation(s, licenseCountValue);
			}
		}
	}

	@Override
	public void initialize(Iterator<? extends State> initialStates) {
		registerInteraction(null, initialStates);
	}


	private Set<State> getTorxStates(Iterator<? extends State> states) {
		Set<State> result = new HashSet<>();
		while (states.hasNext()) {
			result.add(states.next());
		}
		return result;
	}

	private boolean isDeterministicSuperstate(Set<State> superState) {
		return superState.size() == 1;
	}

	private void registerLicenseCountValue(String nodeName, int value) {
		if (remainingObligations.containsKey(nodeName)) {
			Set<Integer> remainingLicenseCounts = remainingObligations.get(nodeName);
			if (remainingLicenseCounts.contains(value)) {
				remainingLicenseCounts.remove(value);
				remainingObligationsTotalSize--;
			}
		}
	}

	private void registerSuperstate(Set<State> superState) {
		// Precondition: superState.size() == 1
		State state = superState.iterator().next();
		Map<String, String> valuation = torxInterface.getValuation(state.getID());
		try {
			registerLicenseCountValue(state.getNodeName(), Integer.parseInt(valuation.get(LICENSECOUNT)));
		}
		catch (NumberFormatException e) {
			System.err.println("LicenseCountCoverageSimpleTO: Don't know how to handle licenseCount value "
					+ valuation.get(LICENSECOUNT) + "\n Further throwing a NumberFormatException.");
			throw e;
		}
	}

	@Override
	public void registerInteraction(Label interaction, Iterator<? extends State> torxStates) {
		Set<State> superState = getTorxStates(torxStates);
		if (isDeterministicSuperstate(superState)) {
			registerSuperstate(superState);
		}
	}

	@Override
	public boolean isTODischarged() {
		return remainingObligationsTotalSize == 0;
	}
	
	protected int getRemainingObligationCount() {
		return remainingObligationsTotalSize;
	}

	@Override
	public void setTorxInterface(TorXInterfaceExtensions tif) {
		this.torxInterface = tif;
	}

}
