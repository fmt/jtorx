package utwente.fmt.jtorx.lazyotf.casestudy;

public enum LicenseGeneratorState {
	GENERATELICENSEREQUESTED("ngenerateLicenseRequested"),
	SHOWLICENSESUNKNOWNEXCEPTION("nshowLicensesUnknownException"),
	REMOTEBACKUPDISKFULLLOCATION("nremoteBackupDiskFullLocation"),
	REMOTEBACKUPUNKNOWNEXCEPTIONLOCATION("nremoteBackupUnknownExceptionLocation"),
	GENERATELICENSEUNKNOWNEXCEPTION("ngenerateLicenseUnknownException"),
	LOCALBACKUPDISKFULLLOCATION("nlocalBackupDiskFullLocation"),
	BACKUPMENU("nbackupMenu"),
	REMOTEBACKUPREQUEST("nremoteBackupRequest"),
	INITLOCATION("ninitLocation"),
	LOCALBACKUPUNKNOWNEXCEPTIONLOCATION("nlocalBackupUnknownExceptionLocation"),
	REMOVEALLREQUESTED("nremoveAllRequested"),
	REMOVELICENSEREQUESTED("nremoveLicenseRequested"),
	LOCALBACKUPREQUEST("nlocalBackupRequest"),
	REMOVEALLUNKNOWNEXCEPTION("nremoveAllUnknownException"),
	SHOWLICENSESREQUEST("nshowLicensesRequest"),
	REMOVELICENSEUNKNOWNEXCEPTION("nremoveLicenseUnknownException"),
	REMOVELICENSELICENSENOTFOUNDEXCEPTION("nremoveLicenseLicenseNotFoundException"),
	SHOWMORELICENSES("nshowMoreLicenses");
	
	private final String name;
	
	public String getName() {
		return name;
	}
	
	private LicenseGeneratorState(String name) {
		this.name = name;
	}
}
