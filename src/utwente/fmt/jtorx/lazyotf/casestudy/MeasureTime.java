package utwente.fmt.jtorx.lazyotf.casestudy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hyperic.sigar.ProcTime;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

import utwente.fmt.jtorx.testnongui.casestudy.CaseStudyIO;

public final class MeasureTime {
	/* When failing to read CPU time measurements, an UnsupportedOperationException
	 * gets thrown if this flag is set to true; Otherwise, a CPU time value of -1
	 * gets returned.
	 */
	private static final boolean THROW_EXCEPTION_ON_FAILURE = false;
	
    private static final int LINUX_TICKS_PER_SEC = 100;
    private static final int LINUX_MILLISEC_PER_TICK = 1000/LINUX_TICKS_PER_SEC;
    private static final int NANOS_PER_MILLIS = 1000000;

    private static final Logger LOGGER = Logger.getLogger(MeasureTime.class.getName());

    /**
     * Gets the CPU time spent in the current thread.
     * 
     * @return The spent CPU time in milliseconds.
     */
    public static long getCurrentThreadCPUTime() {
        return getThreadCPUTime(Thread.currentThread().getId());
    }

    /**
     * Gets the CPU time spent in a given thread.
     * 
     * @param threadID The thread's ID.
     * @return The spent CPU time in milliseconds.
     */
    public static long getThreadCPUTime(long threadID) {
        ThreadMXBean bean = ManagementFactory.getThreadMXBean();
        if (bean.isCurrentThreadCpuTimeSupported()) {
            return bean.getThreadCpuTime(threadID)/NANOS_PER_MILLIS;        
        }
        else {
            return 0L;
        }
    }

    /**
     * Gets the CPU time spent in the garbage collectors.
     * 
     * @return The spent CPU time in milliseconds.
     */
    public static long getGarbageCollectorTime() {
        long time = 0L;
        for (GarbageCollectorMXBean bean : ManagementFactory.getGarbageCollectorMXBeans()) {
            time += bean.getCollectionTime();
        }
        return time;
    }

    public static long getCPUTime(long pid) {
        Sigar sigar = new Sigar();
        try {
            ProcTime pt = sigar.getProcTime(pid);
            return pt.getTotal();
        }
        catch (SigarException e) {
            LOGGER.log(Level.WARNING, "Unable to get CPU time from Sigar");
            return 0;
        }
        finally {
            sigar.close();
        }
    }

    public static long getCPUTime() {
        Sigar sigar = new Sigar();
        long pid = sigar.getPid();
        sigar.close();
        return getCPUTime(pid); 
    }

    public static long getPID() {
        Sigar sigar = new Sigar();
        long pid = sigar.getPid();
        sigar.close();
        return pid;    
    }
    
    public static void printLinuxStat(long pid, String prefix) {
        printProcFile("status", pid, prefix);
    }
    
    public static void printLinuxSmaps(long pid, String prefix) {
        printProcFile("smaps", pid, prefix);
    }
    
    public static void printProcFile(String subFilename, long pid, String prefix) {
        String filename = "/proc/" + pid + "/" + subFilename;

        File f = new File(filename);
        if(f.exists()) {
            InputStream is;
            try {
                is = new FileInputStream(filename);
                BufferedReader br = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                
                try {
                    String line;
                    String completeLine = "";
                    while ((line = br.readLine()) != null) {
                        completeLine += line + ";";
                    }
                    CaseStudyIO.out.print(prefix + completeLine + "\n");
                }
                catch (IOException e) {
                	LOGGER.severe("Could not read the file " + filename);
                	if (THROW_EXCEPTION_ON_FAILURE) {
                		throw new UnsupportedOperationException("Measurement not supported.");
                	}
                }
                finally {
                    try {
                        br.close();
                    }
                    catch (IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    try {
                        is.close();
                    }
                    catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    
                }
                
            }
            catch (FileNotFoundException e1) {
            	LOGGER.severe("Could not read the file " + filename);
            	if (THROW_EXCEPTION_ON_FAILURE) {
            		throw new UnsupportedOperationException("Measurement not supported.");
            	}
            }
        }
        else {
        	LOGGER.severe("Could not read the file " + filename);
        	if (THROW_EXCEPTION_ON_FAILURE) {
        		throw new UnsupportedOperationException("Measurement not supported.");
        	}
        }        
    }

    /**
     * Gets the CPU time spent by an external process.
     * 
     * @param pid The pid of the external process.
     * @return The spent CPU time in milliseconds or a negative number
     *   iff CPU time measurement is not implemented for the current operating
     *   system.
     */
    public static long getLinuxCPUTime(long pid) {
        String filename = "/proc/" + pid + "/stat";
        File f = new File(filename);
        if (f.exists()) {
            InputStream is;
            try {
                is = new FileInputStream(filename);
                BufferedReader br = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                String line;
                try {
                    line = br.readLine();
                    String[] lineSplit = line.split(" ");
                    if (lineSplit.length >= 15) {
                        long uticks = Long.parseLong(lineSplit[13]);
                        long sticks = Long.parseLong(lineSplit[14]);
                        br.close();
                        is.close();
                        return (uticks + sticks) * LINUX_MILLISEC_PER_TICK;
                    }
                    else {
                    	LOGGER.severe("proc/PID/stat output does not have the expected format.");
                    	if (THROW_EXCEPTION_ON_FAILURE) {
                    		throw new UnsupportedOperationException("proc/PID/stat output does not have the expected format.");
                    	}
                    	else {
                    		return -1;
                    	}
                    }
                }
                catch (IOException e) {
                	LOGGER.severe("Measurement not supported.");
                	if (THROW_EXCEPTION_ON_FAILURE) {
                		throw new UnsupportedOperationException("Measurement not supported.");
                	}
                	else {
                		return -1;
                	}
                }
                finally {
                    try {
                        br.close();
                    }
                    catch (IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    try {
                        is.close();
                    }
                    catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            }
            catch (FileNotFoundException e1) {
            	LOGGER.severe("Measurement not supported.");
            	if (THROW_EXCEPTION_ON_FAILURE) {
            		throw new UnsupportedOperationException("Measurement not supported.");
            	}
            	else {
            		return -1;
            	}
            }
        }
        else {
        	LOGGER.severe("Measurement not supported.");
        	if (THROW_EXCEPTION_ON_FAILURE) {
        		throw new UnsupportedOperationException("Measurement not supported.");
        	}
        	else {
        		return -1;
        	}
        }
    }

    /** Utility class - cannot be instantiated. */
    private MeasureTime() {

    }
}
