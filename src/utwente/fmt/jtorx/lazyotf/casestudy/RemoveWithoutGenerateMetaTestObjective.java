package utwente.fmt.jtorx.lazyotf.casestudy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.driver.AbstractMetaTestObjectiveObserver;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.ExtendedInteractionResult;
import utwente.fmt.jtorx.lazyotf.driver.PolicyObserverFactoryRegistry;
import utwente.fmt.jtorx.lazyotf.driver.PolicyObserverFactoryRegistry.PolicyObserverFactory;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2WeightImpl;
import utwente.fmt.jtorx.lazyotf.guidance.LocationValuation2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Locations2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.utils.Pair;
import utwente.fmt.lts.State;

/**
 * <p>
 * A policy observer implementing the test objective "visited
 * removeLicenseRequested N times without visiting generateLicenseRequested
 * in-between".
 * </p>
 * 
 * <p>
 * This meta test objective manages three distinct test objectives (i.e., test
 * type groups): The "IncreaseSequence" TO which seeks a high-enough
 * licenseCount, the "DecreaseSequence" TO rewarding lower licenseCount values,
 * and the "RemoveWithoutGenerateMetaTO" which defines the necessary inducing
 * states and basic weighers for this TO.
 * </p>
 * 
 * <p>
 * The MetaTO works as follows: The test is interpreted in terms of increase and
 * decrease phases. The "IncreaseSequence" TO is active exactly during the
 * increase phases, overriding the "DecreaseSequence" TO by assigning higher
 * weights. The MetaTO starts out in the increase phase, which automatically
 * ends when the licenseCount is high enough to start a subsequent decrease
 * phase. If the test run encounters one of the "forbidden" locations during a
 * decrease phase, the MetaTO switches back to the increase phase in order to
 * subsequently restart the decrease phase when enough licenses have been
 * generated.
 * </p>
 * 
 * <p>
 * During a decrease phase, the MetaTO counts the amounts of license removals.
 * This counter is reset to 0 when starting an increase phase. As soon as the
 * counter reaches the target amount of license removals, all three test type
 * groups are deactivated, thus completing the test objective.
 * </p>
 */
public class RemoveWithoutGenerateMetaTestObjective extends AbstractMetaTestObjectiveObserver {

    /** The logger for this class. */
    private static final Logger LOGGER = Logger
            .getLogger(RemoveWithoutGenerateMetaTestObjective.class.getName());

    /**
     * Iff {@code true}, this test objective automatically gets installed into
     * the driver.
     */
    private static boolean enabled = true;

    /**
     * Status of registration with
     * {@link PolicyObserverFactoryRegistry}.
     */
    private static boolean isRegistered = false;

    /** This test objective's name. */
    private static final String NAME = "IncreaseDecrease";

    /** The "increase" test type group's id. */
    private static final int INCREASE_TO = 0;

    /** The "decrease" test type group's id. */
    private static final int DECREASE_TO = 1;

    /** The "meta" test type group's id. */
    private static final int META_TO = 2;
    
    /** The amount of concrete test objectives used by this meta test
     * objective.
     */
    private static final int AMNT_SUBGOALS = 3;

    /** The target single-license removal sequence length. */
    private static final int TARGET = 8;

    /** The test tree trace weigher used by this TO. */
    private static final String PATH2WEIGHT = "FinalLocationSetDefaultPath2W";

    /**
     * Constructs a new {@link RemoveWithoutGenerateMetaTestObjective} instance.
     * 
     * @param lazyOTF
     *            The test run's LazyOTF instance.
     */
    public RemoveWithoutGenerateMetaTestObjective(LazyOTF lazyOTF) {
        super(lazyOTF, NAME, AMNT_SUBGOALS);
    }

    /**
     * This is set to {@code true} when the meta test objective detects that the
     * test is currently in the "removeLicenseRequested" state. A license
     * removal is possible now and is completed when returning to "initLocation"
     * from "removeLicenseRequested".
     */
    boolean removePossible = false;

    /**
     * After completing the MetaTO, this is set to {@code false}, inhibiting
     * further processing by this MetaTO.
     */
    boolean active = true;

    /**
     * The amount of times removeLicenseRequested has been visited without
     * visiting generateLicenseRequested, removeAllLicensesRequested and
     * removeExpiredLicensesRequested. This counter is only increased when the
     * MetaTO is not in the increasing phase.
     */
    int longestRemoveSequence = 0;

    @Override
    protected void processDriverInteraction(ExtendedInteractionResult eir) {
        if (!active) {
            return;
        }

        LOGGER.log(Level.INFO, "At step " + eir.getInteractionResult().getStepNr());
        Iterator<? extends State> substates = eir.getTesterState().getSubStates();
        while (substates.hasNext()) {
            State st = substates.next();
            LOGGER.log(Level.INFO, "Looking at: " + st.getNodeName());
            if (st.getNodeName().equals("nremoveLicenseRequested")) {
                LOGGER.log(Level.INFO, "Remove is possible now.");
                removePossible = true;
            }
            else if (st.getNodeName().equals("ninitLocation")) {
                if (removePossible && !isActive(INCREASE_TO)) {
                    // transition from removeLicenseRequested to initLocation
                    // in removal phase => licenseCount has decreased by one.
                    longestRemoveSequence++;
                    LOGGER.log(Level.INFO, "Increased sequence counter.");
                }
                removePossible = false;
            }
            else if (st.getNodeName().equals("ngenerateLicenseRequested")
                    || st.getNodeName().equals("nremoveAllRequested")
                    || st.getNodeName().equals("nremoveExpiredRequested")) {
                if (!isActive(INCREASE_TO)) {
                    LOGGER.log(Level.INFO,
                            "Bad state hit, resetting sequence counter and engaging increase TO.");
                    longestRemoveSequence = 0;
                    engage(INCREASE_TO);
                }
            }
            else {
                removePossible = false;
            }
        }

        if (longestRemoveSequence == TARGET) {
            if (isActive(INCREASE_TO)) {
                discharge(INCREASE_TO);
            }
            if (isActive(DECREASE_TO)) {
                discharge(DECREASE_TO);
            }
            active = false;
            discharge(META_TO);
        }
    }

    /**
     * Generate a simple valuation-dependent node value weihger.
     * 
     * @param name
     *            The weigher's display name.
     * @param condition
     *            The evaluation condition.
     * @param value
     *            The evaluation value expression.
     * @return Thhe new node value weigher.
     */
    private Path2Weight generatePath2Weight(String name, String condition,
            String value) {
        CustomPath2Weight result;
        result = CustomPath2WeightImpl.newInstance(PATH2WEIGHT, name);

        LocationValuation2Weight lv2w = LocationValuation2Weight.newInstance(
                "IncreaseLV2W", new ArrayList<Pair<String, String>>());

        lv2w.addExpression(condition, value);

        result.setNodeValue2Weight(lv2w);

        return result;
    }

    /**
     * Generate a location test type decision strategy setting the initLocation
     * to TEST_GOAL when {@code licenseCount >= TARGET}.
     * 
     * @return The new location test type decision strategy.
     */
    private DecisionStrategy generateIncreaseDecisionStrategy() {
        SatStrategy result = new SatStrategy();
        result.associate(new Location("initLocation"), TestType.TEST_GOAL,
                new SatStrategy.DumontCondition("licenseCount >= " + TARGET));
        return result;
    }

    /** The INDUCING location weight for the base test tree trace weigher. */
    private static final int INDFIN_IND_WEIGHT = 1;

    /** The ORDINARY location weight for the base test tree trace weigher. */
    private static final int INDFIN_ORD_WEIGHT = 100000;

    /** The TEST_GOAL location weight for the base test tree trace weigher. */
    private static final int INDFIN_TG_WEIGHT = 10000000;

    /**
     * <p>
     * Generates the base test tree trace weighers active as long as this meta
     * test objective is active.
     * </p>
     * 
     * <p>
     * Currently, the only generated weigher serves to fine inducing states
     * and to reward test goals.
     * </p>
     * 
     * 
     * @return The base test tree trace weigher.
     */
    private Set<Path2Weight> generateBasicPath2Weights() {
        Set<Path2Weight> result = new HashSet<>();

        /* Fine inducing states. */
        Locations2Weight inducingFiner = new Locations2Weight("InducingFiner",
                INDFIN_IND_WEIGHT, INDFIN_ORD_WEIGHT, INDFIN_TG_WEIGHT);
        CustomPath2Weight inducingFinerTT;
        inducingFinerTT = CustomPath2WeightImpl.newInstance(PATH2WEIGHT,
                "InducingFinerTT");
        inducingFinerTT.setNodeValue2Weight(inducingFiner);
        result.add(inducingFinerTT);

        return result;
    }

    @Override
    protected void setupTestObjective(TestTypeGroup objective, int id) {
        objective.associate(new Location("initLocation"), TestType.DECIDED_BY_STRATEGY);
        switch (id) {
        case 0:
            objective.setName("IncreaseSequence " + objective.hashCode());
            Set<Path2Weight> gm = new HashSet<>();
            gm.add(generatePath2Weight("IncreaseTT2W",
                    "true == true", "licenseCount * 1000000"));
            objective.setPath2Weights(gm);
            objective.setStrategy(generateIncreaseDecisionStrategy());
            objective.setActive(true);
            break;
        case 1:
            objective.setName("DecreaseSequence " + objective.hashCode());
            Set<Path2Weight> gm2 = new HashSet<>();
            gm2.add(generatePath2Weight("DecreaseTT2W",
                    "licenseCount <= " + TARGET, "(" + (1 + TARGET) + " - licenseCount) * 10000"));
            objective.setPath2Weights(gm2);
            objective.setActive(true);
            break;
        default:
            objective.setName("RemoveWithoutGenerateMetaTO");
            objective.setActive(true);
            objective.setPath2Weights(generateBasicPath2Weights());
            objective.associate(new Location("removeAllRequested"), TestType.INDUCING);
            objective.associate(new Location("removeExpiredRequested"), TestType.INDUCING);
            break;
        }
        LOGGER.log(Level.INFO, "Added objective " + objective.getName());
    }

    /**
     * Registers this class with the driver's pre-policy-phase observer
     * registry.
     */
    public static void registerWithDriverPrePolicyObserverRegistry() {
        if (!enabled || isRegistered) {
            return;
        }
        isRegistered = true;

        PolicyObserverFactory fact = new PolicyObserverFactory() {
            @Override
            public Observer makeObserver(LazyOTF lazyOTFInstance) {
                return new RemoveWithoutGenerateMetaTestObjective(lazyOTFInstance);
            }
        };
        PolicyObserverFactoryRegistry.getInstance().addPrePolicyObserverFactory(fact);
    }
}
