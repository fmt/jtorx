package utwente.fmt.jtorx.lazyotf.casestudy;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.driver.AbstractMetaTestObjectiveObserver;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.ExtendedInteractionResult;
import utwente.fmt.jtorx.lazyotf.driver.PolicyObserverFactoryRegistry;
import utwente.fmt.jtorx.lazyotf.driver.PolicyObserverFactoryRegistry.PolicyObserverFactory;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2WeightImpl;
import utwente.fmt.jtorx.lazyotf.guidance.LocationValuation2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Locations2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.lts.State;

public class RemoveWithoutGeneratePassiveTO  extends AbstractMetaTestObjectiveObserver {

    private final static String NAME = "RemoveWithoutPassiveTO";
    private final static String TARGET_STATE = "nremoveLicenseRequested";
    private final static String TARGET_EXIT_STATE = "ninitLocation";
    private final static Set<String> RESET_STATES;
    
    static {
        RESET_STATES = new HashSet<>();
        RESET_STATES.add("ngenerateLicenseRequested");
    }
    
    private final int target;
    
    private int testObjectiveID = -1;
    private boolean hasRegisteredLicenseCountBoosterTG = false;
    private int targetCounter = 0;
    private boolean isPrevStepInTargetState = false;
    
    public RemoveWithoutGeneratePassiveTO(LazyOTF lazyOTF, int target) {
        super(lazyOTF, NAME, 2);
        this.target = target;
    }

    @Override
    protected void processDriverInteraction(ExtendedInteractionResult eir) {
        Iterator<? extends State> currentStates = eir.getTesterState().getSubStates();
        while (currentStates.hasNext()) {
            State s = currentStates.next();
            if (s.getNodeName().equals(TARGET_STATE)) {
                isPrevStepInTargetState = true;
            }
            else if (s.getNodeName().equals(TARGET_EXIT_STATE)) {
                if (isPrevStepInTargetState) {
                    targetCounter++;
                }
                isPrevStepInTargetState = false;
            }
            else {
                isPrevStepInTargetState = false;
            }
            
            if (RESET_STATES.contains(s.getNodeName())) {
                targetCounter = 0;
            }
        }
       
        if (targetCounter == target) {
            discharge(testObjectiveID);
        }
    }

    @Override
    protected void setupTestObjective(TestTypeGroup objective, int id) {
        if (hasRegisteredLicenseCountBoosterTG) {
            //System.out.println("ADD second TO");
            // Add the license-count fining test objective. This needs to be
            // discharged explicitly.
            
            assert testObjectiveID < 0;
            testObjectiveID = id;
            targetCounter = 0;

            objective.associate(new Location("initLocation"), TestType.DECIDED_BY_STRATEGY);
            objective.associate(new Location("removeAllRequested"), TestType.INDUCING);
            objective.associate(new Location("removeExpiredRequested"), TestType.INDUCING);
            objective.associate(new Location("backupMenu"), TestType.INDUCING);

            LocationValuation2Weight node2W = new LocationValuation2Weight();
            node2W.addExpression("true == true", "2000 + 150000/(licenseCount+1)");
            CustomPath2Weight path2W = new CustomPath2WeightImpl("FinalLocationSetDefaultPath2W",
                    "DecreaseLicenseCountPath2W");
            path2W.setNodeValue2Weight(node2W);
            objective.getPath2Weights().add(path2W);
        }
        else {
            // Add the license-count increasing test objective (so that this test
            // objective can run standalone). This test objective gets discharged
            // as soon as the needed license count is reached.
            //System.out.println("ADD first TO");
            Location initLoc = new Location("initLocation");
            
            objective.associate(initLoc, TestType.DECIDED_BY_STRATEGY);
            SatStrategy satStrat = new SatStrategy();
            satStrat.associate(initLoc, TestType.TEST_GOAL, new SatStrategy.DumontCondition("licenseCount >= " + target));
            objective.setStrategy(satStrat);

            Locations2Weight loc2W = new Locations2Weight("TGLoc2W", 1, 100, 1000000000);
            CustomPath2Weight tgPath2W = new CustomPath2WeightImpl("FinalLocationSetDefaultPath2W",
                    "TGPath2W");
            tgPath2W.setNodeValue2Weight(loc2W);
            objective.getPath2Weights().add(tgPath2W);
            
            LocationValuation2Weight node2W = new LocationValuation2Weight();
            node2W.addExpression("true == true", "licenseCount * 200000");
            CustomPath2Weight path2W = new CustomPath2WeightImpl("FinalLocationSetDefaultPath2W",
                    "IncreaseLicenseCountPath2W");
            path2W.setNodeValue2Weight(node2W);
            objective.getPath2Weights().add(path2W); 
            
            hasRegisteredLicenseCountBoosterTG = true;
        }
    }
    
    
    /**
     * Registers this class with the driver's pre-policy-phase observer
     * registry.
     */
    public static void registerWithDriverPrePolicyObserverRegistry(final int target) {
        PolicyObserverFactory fact = new PolicyObserverFactory() {
            @Override
            public Observer makeObserver(LazyOTF lazyOTFInstance) {
                return new RemoveWithoutGeneratePassiveTO(lazyOTFInstance, target);
            }
        };
        PolicyObserverFactoryRegistry.getInstance().addPrePolicyObserverFactory(fact);
    }
}
