package utwente.fmt.jtorx.lazyotf.casestudy;

import java.util.Iterator;

import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;

public interface SimpleTO {
    void initialize(Iterator<? extends State> initialStates);
    void registerInteraction(Label interaction, Iterator<? extends State> torxStates);
    boolean isTODischarged();
    void setTorxInterface(TorXInterfaceExtensions tif);
}
