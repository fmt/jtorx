package utwente.fmt.jtorx.lazyotf.distributed;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.utilities.Observable;
import utwente.fmt.jtorx.lazyotf.utilities.ObservableImpl;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;

/**
 * Distributes LazyOTF via broadcast UDP (not multicast).
 *
 */
public class BUDPDistributeLazyOTFFactory implements DistributeLazyOTFFactory {

	
	@Override
	public DistributeLazyOTF newDistributeLazyOTF() {
		return new BUDPDistributeLazyOTF();
	}
	
	private static class BUDPDistributeLazyOTF implements DistributeLazyOTF {
		private final DatagramSocket bcSock;

		// TODO: Remove hardcoded networking parameters
		private static final int MCSOCK_TIMEOUT = 10000;
		private static final int PORT_NUMBER = 4445; //55555;
		
		private static final String BROADCAST_ADDRESS = "192.168.0.255";
		
		private final Logger LOGGER = Logger.getLogger(BUDPDistributeLazyOTF.class.getName());
		private final Observable eventMgr = new ObservableImpl();
		
		private final int INITIAL_SYNC_SLEEP = 1000;
		
		private final InetAddress address;

		// true iff cleanUp() has been called on this.
		private boolean isCleanedUp = false; 
		
		public BUDPDistributeLazyOTF() {
			try {
				bcSock = new DatagramSocket(PORT_NUMBER);
				bcSock.setSoTimeout(MCSOCK_TIMEOUT);
				LOGGER.log(Level.INFO, "Broadcast-Socket established with receive buffer size "
						+ bcSock.getReceiveBufferSize());
				System.out.println("debug: Broadcast-Socket established with receive buffer size "
						+ bcSock.getReceiveBufferSize());
				address = InetAddress.getByName(BROADCAST_ADDRESS);
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "UDP Socket setup failed");
				throw new RuntimeException(e.toString());
			}
			
			try {
                Thread.sleep(INITIAL_SYNC_SLEEP);
            }
            catch (InterruptedException e) {
                LOGGER.log(Level.WARNING, "Sync sleep interrupted");
            }
		}

		@Override
		public void pullExternalDischarges() {
			try {
				while (true){
					System.out.println("debug: before BUDP Read ");
					notifyExternalDischargeObservers(readExtrenalDischargedTTG());				
				} 
			} catch (java.net.SocketTimeoutException soe) {
				System.out.println("debug: timeout of MUDP Read ");
				LOGGER.log(Level.FINE, "All messages read and forwarded to observer.");
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "UDP Socket setup failed");
				throw new RuntimeException(e.toString());
			}
		}

		@Override
		public void pushInternalDischarge(TestTypeGroup dischargedGroup) {
			DatagramPacket out = new DatagramPacket(dischargedGroup.getName().getBytes(), 
					dischargedGroup.getName().length(), address, PORT_NUMBER);
			try {
				System.out.println("debug: sending packet "+dischargedGroup.getName());
				bcSock.send(out);
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "sending package" + out + " to " + address + " failed.");
				throw new RuntimeException(e.toString());
			}
		}

		@Override
		public void addExternalDischargeObserver(Observer obs) {
			eventMgr.addObserver(obs);
		}
		
		@Override
		public void cleanUp() {
			if (isCleanedUp) { 
				LOGGER.log(Level.WARNING, this + " has already been cleanedUp.");
			} else {
				try {
					bcSock.close();
				} finally {
					isCleanedUp = true;	
				}
			}
		}

		// Effective Java #7 (Avoid finalize() due to GC)
		@Override 
		protected void finalize() throws Throwable {
			try {
				if (!isCleanedUp) { 
					LOGGER.log(Level.WARNING, "cleanUp() has been forgotten on "+ this);
					cleanUp();
				} 
			} finally {
				super.finalize();
			}
		}

		private String readExtrenalDischargedTTG() throws IOException {
			byte[] buf = new byte[1000];
			DatagramPacket in = new DatagramPacket(buf, buf.length);
			bcSock.receive(in);
			System.out.println("debug: Read TTG "+new String(in.getData(),0,in.getLength()));
			return new String(in.getData(),0,in.getLength());
		}
		
		private void notifyExternalDischargeObservers(String testTypeGroupID) {
			eventMgr.notifyObservers(new ExternalDischargeEvent(testTypeGroupID));
		}		
	}
}