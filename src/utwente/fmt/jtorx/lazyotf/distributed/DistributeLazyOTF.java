package utwente.fmt.jtorx.lazyotf.distributed;

import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;

public interface DistributeLazyOTF {
	/**
	 * Handles all externally discharged dischargedGroups by discharging them internally 
	 * with the help of the observers registered via {@link #addExternalDischargeObserver(Observer)}. 
	 */
    void pullExternalDischarges();
    
    /**
     * Pushes dischargedGroup that was internally discharged to the other lazyOTF instances.
     * @param dischargedGroup
     */
    void pushInternalDischarge(TestTypeGroup dischargedGroup);
    
    /**
     * Register obs for {@link #pullExternalDischarges()}.
     * @param obs
     */
    void addExternalDischargeObserver(Observer obs);
    
    //TODO: extend this interface for synchronization: setUp()
    
    /**
     * "Destructor" to cleanUp() before termination: cleanUp() must be exactly the last method called on this.
     */
    void cleanUp();
    
    public static class ExternalDischargeEvent {
    	public final String dischargedTestTypeGroupName;
    	
    	public ExternalDischargeEvent(String dischargedTestTypeGroupName) {
    		this.dischargedTestTypeGroupName = dischargedTestTypeGroupName;
    	}
    }
}
