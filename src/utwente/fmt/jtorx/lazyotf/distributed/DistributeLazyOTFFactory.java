package utwente.fmt.jtorx.lazyotf.distributed;

public interface DistributeLazyOTFFactory {
	DistributeLazyOTF newDistributeLazyOTF();
}
