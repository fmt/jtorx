package utwente.fmt.jtorx.lazyotf.distributed;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.utilities.Observable;
import utwente.fmt.jtorx.lazyotf.utilities.ObservableImpl;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

/**
 * Distributes LazyOTF via Hazelcast.
 * 
 * This enables synchronization, 
 * multicast discovery as well as other discoveries and setups,
 * cloud computing (via hazelcast-cloud-3.1.2.jar), 
 * monitoring (via Hazelcast Management Center)
 * 
 */
public class HazelDistributeLazyOTFFactory implements DistributeLazyOTFFactory {
	// Specifies the default minimum duration of the time HazelDistributeLazyOTF sleeps before
	// shutting down HazelCast during the cleanup phase.
	private static final long DEFAULT_MIN_SLEEP_BEFORE_SHUTDOWN_MS = 0l;

	// Specifies the maximum amount of time actually before shutting down HazelCast during cleanup
	// as a ratio of the amount of time specified by the user.
	private static final double DEFAULT_MAX_SLEEP_BEFORE_SHUTDOWN_RATIO = 1.0d;


	private int initialMinCluster = 1; //minimal initial size of the cluster, beforehand newHazelcastInstance blocks
	private String clusterName = "default"; //group name for the hazelcast cluster, to build separate clusters

	// Specifies the minimum duration of the time HazelDistributeLazyOTF sleeps before
	// shutting down HazelCast during the cleanup phase.
	private long minSleepBeforeShutdownMs = DEFAULT_MIN_SLEEP_BEFORE_SHUTDOWN_MS;

	// Specifies the maximum amount of time actually before shutting down HazelCast during cleanup
	// as a ratio of minSleepBeforeShutdownMs.
	private double maxSleepBeforeShutdownRatio = DEFAULT_MAX_SLEEP_BEFORE_SHUTDOWN_RATIO;

	@Override
	public DistributeLazyOTF newDistributeLazyOTF() {
		return new HazelDistributeLazyOTF(initialMinCluster, clusterName,
				minSleepBeforeShutdownMs, maxSleepBeforeShutdownRatio);
	}

	public void setInitialMinCluster(int minimum){
		initialMinCluster = minimum;
	}

	public void setClusterName(String id){
		clusterName = id;
	}

	public long getMinSleepBeforeShutdownMs() {
		return minSleepBeforeShutdownMs;
	}

	public void setMinSleepBeforeShutdownMs(long minSleepBeforeShutdownMs) {
		this.minSleepBeforeShutdownMs = minSleepBeforeShutdownMs;
	}

	public double getMaxSleepBeforeShutdownRatio() {
		return maxSleepBeforeShutdownRatio;
	}

	public void setMaxSleepBeforeShutdownRatio(double maxSleepBeforeShutdownRatio) {
		this.maxSleepBeforeShutdownRatio = maxSleepBeforeShutdownRatio;
	}

	/**
	 * TODO: create abstract superclass NetworkingDistributeLazyOTFFactory 
	 */
	private static class HazelDistributeLazyOTF implements DistributeLazyOTF {

		private final HazelcastInstance hazelInst; 

		// a TTG can be present in the list multiple times if the TTG is discharged concurrently by multiple instances.
		private final List<String> distrDischargedTTGs;

		private int distrDischargedTTGsCursor = 0;

		//used as ID for the distributed list
		private final int PORT_NUMBER = 4446;

		//use TCPIP discovery instead of multicast to avoid problems if multiple instances run on one machine in different JVMs
		public static final boolean USE_TCPIP = true;		

		private final Logger LOGGER = Logger.getLogger(HazelDistributeLazyOTF.class.getName());
		private final Observable eventMgr = new ObservableImpl();

		// true iff cleanUp() has been called on this.
		private boolean isCleanedUp = false; 

		private final long minSleepBeforeShutdownMs;
		private final double maxSleepBeforeShutdownRatio;

		public HazelDistributeLazyOTF(int minimumInitialMembersInHazelCluster, String clusterName,
				long minSleepBeforeShutdownMs, double maxSleepBeforeShutdownRatio) {
			LOGGER.fine("[progress] Beginning to create a HazelCast instance.");
			this.minSleepBeforeShutdownMs = minSleepBeforeShutdownMs;
			this.maxSleepBeforeShutdownRatio = maxSleepBeforeShutdownRatio;

			if (minimumInitialMembersInHazelCluster == 1) {
				LOGGER.info("initialMinCluster == 1");
			}
			if (clusterName.equals("default")) {
				LOGGER.info("clusterName is default");
			}
			Config cfg = new Config();	
			//use the following to synchronize with other instances. Thus no timeout needed (as for, e.g., multicasts).
			cfg.setProperty("hazelcast.initial.min.cluster.size",Integer.toString(minimumInitialMembersInHazelCluster));
			cfg.getGroupConfig().setName(clusterName);


			//manual hazelcast discovery network configuration, for TCPIP or UDP-multicasts.
			NetworkConfig network = cfg.getNetworkConfig();
			//			network.setPort(PORT_NUMBER);
			//			
			//			//?necessary for both TCPIP and multicast?
			//			network.setPortAutoIncrement(false);
			network.setPortAutoIncrement(true);
			//
			JoinConfig join = network.getJoin();

			if (USE_TCPIP){
				join.getMulticastConfig().setEnabled(false);
				join.getTcpIpConfig().addMember("192.168.0.1").addMember("192.168.0.2").addMember("192.168.0.3").
				addMember("192.168.0.4").addMember("192.168.0.5").addMember("192.168.0.6").addMember("192.168.0.7").
				addMember("192.168.0.8").addMember("192.168.0.9").addMember("192.168.0.10").addMember("192.168.0.11").
				addMember("192.168.0.12").addMember("192.168.0.13").addMember("192.168.0.14").addMember("192.168.0.15").
				addMember("192.168.0.16").addMember("192.168.0.17").addMember("192.168.0.18").addMember("192.168.0.19").
				addMember("192.168.0.20").
				setRequiredMember(null).setEnabled(true);

				//this sets the allowed connections to the cluster? necessary for multicast, too?
				network.getInterfaces().setEnabled(true).addInterface("192.168.0.*");
			} else {

				//				join.getTcpIpConfig().setEnabled(false);
				//				join.getAwsConfig().setEnabled(false);
				//				join.getMulticastConfig().setEnabled(true);
				//
				//				join.getMulticastConfig().setMulticastGroup(MULTICAST_ADDRESS);
				//				join.getMulticastConfig().setMulticastPort(PORT_NUMBER);
				//				join.getMulticastConfig().setMulticastTimeoutSeconds(MCSOCK_TIMEOUT/100);
			}



			hazelInst = Hazelcast.newHazelcastInstance(cfg);
			LOGGER.log(Level.SEVERE, "debug: joined via "+cfg.getNetworkConfig().getJoin()+" with "+
					hazelInst.getCluster().getMembers().size()+" members.");
			//			distrDischargedTTGs = hazelInst.getList(new Integer(PORT_NUMBER).toString());
			distrDischargedTTGs = hazelInst.getList(clusterName);
			LOGGER.fine("[progress] Done setting up a HazelCast instance.");
		}

		@Override
		public void pullExternalDischarges() {
			System.out.println("debug: starting Hazel pullExternal from Hazelcluster with "+hazelInst.getCluster().getMembers().size()+" members.");

			/* since this is writing and reading from distrDischargedTTGs, 
			 * this also get TTGs this just wrote into distrDischargedTTGs.
			 */
			while (distrDischargedTTGsCursor < distrDischargedTTGs.size()){
				final String ttgName = distrDischargedTTGs.get(distrDischargedTTGsCursor);
				LOGGER.log(Level.SEVERE, "debug: pulled TTG "+ttgName);
				notifyExternalDischargeObservers(ttgName);
				distrDischargedTTGsCursor++;
			}
		}

		@Override
		public void pushInternalDischarge(TestTypeGroup dischargedGroup) {
			LOGGER.log(Level.SEVERE, "debug: starting Hazel pushInternal: "+dischargedGroup.getName());
			distrDischargedTTGs.add(dischargedGroup.getName());
		}

		@Override
		public void addExternalDischargeObserver(Observer obs) {
			eventMgr.addObserver(obs);
		}

		private long randomizeSleepDuration(long duration, double maxRatio) {
			double result = Math.random() * (maxRatio - 1.0d) * (double)duration;
			result = result + duration;
			return (long) result;
		}

		private void sleepBeforeShutdown() {
			if (this.minSleepBeforeShutdownMs > 0l) {
				long sleepDuration = randomizeSleepDuration(this.minSleepBeforeShutdownMs,
						this.maxSleepBeforeShutdownRatio);

				LOGGER.fine("Sleeping " + sleepDuration + " milliseconds before shutting down HazelCast.");
				try {
					Thread.sleep(sleepDuration);
				} catch (InterruptedException e) {
					LOGGER.warning("Sleep before HazelCast shutdown has been interrupted:\n" + e);
				}
				LOGGER.fine("Finished sleeping before shutting down HazelCast.");
			}
		}

		@Override
		public void cleanUp() {
			if (isCleanedUp) { 
				LOGGER.log(Level.WARNING, this + " has already been cleanedUp.");
			} else {
				sleepBeforeShutdown();
				try {
					hazelInst.shutdown(); 
				} finally {
					isCleanedUp = true;	
				}
			}
		}

		// Effective Java #7 (Avoid finalize() due to GC)
		@Override 
		protected void finalize() throws Throwable {
			try {
				if (!isCleanedUp) { 
					LOGGER.log(Level.WARNING, "cleanUp() has been forgotten on "+ this);
					cleanUp();
				} 
			} finally {
				super.finalize();
			}
		}

		private void notifyExternalDischargeObservers(String testTypeGroupID) {
			eventMgr.notifyObservers(new ExternalDischargeEvent(testTypeGroupID));
		}		
	}
}