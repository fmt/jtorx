package utwente.fmt.jtorx.lazyotf.distributed;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.utilities.Observable;
import utwente.fmt.jtorx.lazyotf.utilities.ObservableImpl;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;

/**
 * Distributes LazyOTF via multicast UDP (not broadcast).
 */
public class MUDPDistributeLazyOTFFactory implements DistributeLazyOTFFactory {
    //true to disable the LoopbackMode
    // TODO configurable per GUI
    private final boolean noLoopback;
    
    /**
     * Constructs a new {@link MUDPDistributeLazyOTFFactory} instance.
     * @param noLoopback <tt>true</tt> to disable multicast loopback in the
     *   {@link MUDPDistributeLazyOTF} instances this factory creates.
     */
    public MUDPDistributeLazyOTFFactory(boolean noLoopback) {
        this.noLoopback = noLoopback;
    }
	
	@Override
	public DistributeLazyOTF newDistributeLazyOTF() {
		return new MUDPDistributeLazyOTF(noLoopback);
	}
	
	/**
	 * TODO: create abstract superclass UDPDistributeLazyOTFFactory 
	 */
	private static class MUDPDistributeLazyOTF implements DistributeLazyOTF {

	    private final MulticastSocket mcSock;

		// TODO: Remove hardcoded networking parameters
		// TODO configurable per GUI
		private final int PORT_NUMBER = 4446; //55555;
			
		// should not influence timing experiments. thus low priority TODO (DF): lower by experiment 
		private final int INITIAL_SYNC_SLEEP = 1000; // (TODO configurable per GUI)
	
		// IP e.g. 224.0.0.183 (see http://en.wikipedia.org/wiki/Multicast_address)
		// TODO configurable per GUI
		private final String MULTICAST_ADDRESS = "230.0.0.1";
		// If cluster does not support MultiCasts, 
		// use BUDPDistributeLazyOTF with broadcast-address 192.168.0.255,
		// rest similar to this solution, but remove packet to yourself. 

		// TODO find largest string or use indices in sorted TO-set instead of TO-names
		private static final int PACKET_SIZE = 512;
		// (TODO configurable per GUI)
		private static final int MCSOCK_TIMEOUT = 200;

		private final Logger LOGGER = Logger.getLogger(MUDPDistributeLazyOTF.class.getName());
		private final Observable eventMgr = new ObservableImpl();
	
		private final InetAddress group;

		// true iff cleanUp() has been called on this.
		private boolean isCleanedUp = false; 
		
		/**
		 * Constructs a new {@link MUDPDistributeLazyOTF} instance.
		 * @param noLoopback <tt>true</tt> to disable multicast loopback.
		 */
		public MUDPDistributeLazyOTF(boolean noLoopback) {
			try {
				mcSock = new MulticastSocket(PORT_NUMBER);
				group = InetAddress.getByName(MULTICAST_ADDRESS);
				mcSock.joinGroup(group);
				mcSock.setSoTimeout(MCSOCK_TIMEOUT);       
				mcSock.setLoopbackMode(noLoopback); 
				
				LOGGER.log(Level.INFO, "Multicast-Socket established with receive buffer size "
						+ mcSock.getReceiveBufferSize());
				System.out.println("debug: Multicast-Socket established with receive buffer size "
						+ mcSock.getReceiveBufferSize());
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "UDP Socket setup failed");
				throw new RuntimeException(e.toString());
			}
			
			try {
                Thread.sleep(INITIAL_SYNC_SLEEP);
            }
            catch (InterruptedException e) {
                LOGGER.log(Level.WARNING, "Sync sleep interrupted");
                throw new RuntimeException(e.toString());
            }
		}

		@Override
		public void pullExternalDischarges() {
			System.out.println("debug: before MUDP Read-Loop");
			try {
				while (true){
					System.out.println("debug: before MUDP Read");
					notifyExternalDischargeObservers(readExtrenalDischargedTTG());				
				} 
			} catch (java.net.SocketTimeoutException soe) {
				System.out.println("debug: timeout of MUDP Read");
				LOGGER.log(Level.FINE, "All messages read and forwarded to observer.");
			} catch (IOException e) {
				System.out.println("debug: not SocketTimeoutException but "+e);
				LOGGER.log(Level.SEVERE, "UDP Socket setup failed");
				throw new RuntimeException(e.toString());
			}
		}

		@Override
		public void pushInternalDischarge(TestTypeGroup dischargedGroup) {
			DatagramPacket out = new DatagramPacket(dischargedGroup.getName().getBytes(), 
					dischargedGroup.getName().length(), group, PORT_NUMBER);
			try {
				System.out.println("debug: sending packet "+dischargedGroup.getName());
				mcSock.send(out);
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "sending package" + out + " to " + group + " failed.");
				throw new RuntimeException(e.toString());
			}
		}

		@Override
		public void addExternalDischargeObserver(Observer obs) {
			eventMgr.addObserver(obs);
		}
		
		@Override
		public void cleanUp() {
			if (isCleanedUp) { 
				LOGGER.log(Level.WARNING, this + " has already been cleanedUp.");
			} else {
				try {
					mcSock.leaveGroup(group);
					mcSock.close();
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, "leaving UDP group caused" + e);
					throw new RuntimeException(e.toString());
				} finally {
					isCleanedUp = true;	
				}
			}
		}

		// Effective Java #7 (Avoid finalize() due to GC)
		@Override 
		protected void finalize() throws Throwable {
			try {
				if (!isCleanedUp) { 
					LOGGER.log(Level.WARNING, "cleanUp() has been forgotten on "+ this);
					cleanUp();
				} 
			} finally {
				super.finalize();
			}
		}

		private String readExtrenalDischargedTTG() throws IOException {
			byte[] buf = new byte[PACKET_SIZE];
			DatagramPacket in = new DatagramPacket(buf, buf.length);
			mcSock.receive(in);
			System.out.println("debug: Read TTG "+new String(in.getData(),0,in.getLength()));
			return new String(in.getData(),0,in.getLength());
		}
		
		private void notifyExternalDischargeObservers(String testTypeGroupID) {
			eventMgr.notifyObservers(new ExternalDischargeEvent(testTypeGroupID));
		}		
	}
}