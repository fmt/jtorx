package utwente.fmt.jtorx.lazyotf.distributed;

import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;

/**
 * A factory for {@link NullDistributeLazyOTF}.
 * @author dball
 *
 */
public class NullDistributeLazyOTFFactory implements DistributeLazyOTFFactory {
	private static final DistributeLazyOTF NULL_DISTRIBUTELAZYOTF;
	
	static {
		NULL_DISTRIBUTELAZYOTF = new NullDistributeLazyOTF();
	}

	@Override
	public DistributeLazyOTF newDistributeLazyOTF() {
		return NULL_DISTRIBUTELAZYOTF;
	}
	
	/**
	 * A "null" {@link DistributeLazyOTF} implementation that causes no
	 * distribution at all.
	 * @author dball
	 *
	 */
	public static class NullDistributeLazyOTF implements DistributeLazyOTF {
		@Override
		public void pullExternalDischarges() {
		}

		@Override
		public void pushInternalDischarge(TestTypeGroup dischargedGroup) {
		}

		@Override
		public void addExternalDischargeObserver(Observer obs) {
		}

		@Override
		public void cleanUp() {
		}
	}
}
