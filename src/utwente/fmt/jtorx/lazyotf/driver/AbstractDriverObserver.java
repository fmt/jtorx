package utwente.fmt.jtorx.lazyotf.driver;

import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverFinalizationEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverInitializedEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverSUTInteractionEvent;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverMethod;

public abstract class AbstractDriverObserver implements Observer {
    /**
     * An observer method for SUT interactions.
     * 
     * @param eir
     *            The SUT interaction represented by a
     *            {@link DriverSUTInteractionEvent}.
     */
    @ObserverMethod
    public abstract void onSUTInteraction(DriverSUTInteractionEvent eir);

    /**
     * An observer method for driver initialization events.
     * 
     * @param dsir
     *            The initialization result represented by a
     *            {@link DriverInitializedEvent}.
     */
    @ObserverMethod
    public abstract void onInit(DriverInitializedEvent dsir);
    
    /**
     * An observer method for driver finalization events.
     * 
     * @param dfr
     *            The finalization result represented by a
     *            {@link DriverFinalizationEvent}.
     */
    @ObserverMethod
    public abstract void onFinish(DriverFinalizationEvent dfr);
}
