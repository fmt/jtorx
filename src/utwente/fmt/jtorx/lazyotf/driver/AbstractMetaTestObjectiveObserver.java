package utwente.fmt.jtorx.lazyotf.driver;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverInitializedEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverSUTInteractionEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.ExtendedInteractionResult;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverMethod;

/**
 * A Test Objective observer for directly implementing test objectives.
 */
public abstract class AbstractMetaTestObjectiveObserver implements Observer {
    /** The logger for this class. */
    private static final Logger LOGGER = Logger.getLogger(AbstractMetaTestObjectiveObserver.class
            .toString());

    /** The LazyOTF policy's test type group manager. */
    private final TestTypeGroupMgr testTypeGroupMgr;

    /** The target LazyOTF instance. */
    private final LazyOTF lazyOTF;

    /** The test objectives. */
    private final Map<Integer, TestTypeGroup> testObjectives;

    /** The test objectives registered with the test type group manager. */
    private final Set<TestTypeGroup> registeredObjectives;

    /** The amount of sub-testobjectives. */
    private final int testObjectiveAmount;

    /** The meta test objective's name. */
    private final String name;

    /**
     * Constructs a {@link AbstractMetaTestObjectiveObserver} instance.
     * 
     * @param lazyOTF
     *            The target LazyOTF instance.
     * @param testObjectiveName
     *            The test objective's name. Initially, the concrete test
     *            objectives (i.e. test type groups) will be named
     *            {@code testObjectiveName}.X (where X is an integer).
     * @param n
     *            The amount of concrete test objectives to create.
     */
    public AbstractMetaTestObjectiveObserver(LazyOTF lazyOTF, String testObjectiveName, int n) {
        testTypeGroupMgr = lazyOTF.getLocationManager().getTestTypeGroupMgr();
        testObjectives = new HashMap<>();
        registeredObjectives = new HashSet<>();
        this.lazyOTF = lazyOTF;
        testObjectiveAmount = n;
        name = testObjectiveName;
    }

    /**
     * Creates a concrete test objective.
     * 
     * @param id
     *            The test objective's ID.
     * @param defaultName
     *            The test objective's default name prefix.
     * @return The newly created test objective.
     */
    private TestTypeGroup createTestObjective(int id, String defaultName) {
        TestTypeGroup result = new TestTypeGroup(TestType.ALL_TEST_TYPES);
        result.setName(defaultName + "." + id);
        setupTestObjective(result, id);
        testObjectives.put(id, result);
        return result;
    }

    /**
     * Processes a SUT interaction.
     * 
     * @param eir
     *            The interaction's {@link ExtendedInteractionResult}.
     */
    protected abstract void processDriverInteraction(ExtendedInteractionResult eir);

    /**
     * Sets up the test objectives.
     * 
     * @param objective
     *            The concrete test objective.
     * @param id
     *            The test objective ID.
     */
    protected abstract void setupTestObjective(TestTypeGroup objective, int id);

    /**
     * Discharges the test objectives.
     */
    protected void discharge() {
        LOGGER.log(Level.INFO, "Discharging...");
        for (int i : testObjectives.keySet()) {
            discharge(i);
        }
    }

    /**
     * Engages the test objectives.
     */
    protected void engage() {
        LOGGER.log(Level.INFO, "Engaging...");
        for (int i : testObjectives.keySet()) {
            engage(i);
        }
    }

    /**
     * Registers the test objectives with the test type group manager.
     */
    protected void registerTestObjectives() {
        for (TestTypeGroup to : testObjectives.values()) {
            if (to.isActive()) {
                testTypeGroupMgr.addTestTypeGroup(to);
                registeredObjectives.add(to);
            }
        }
    }

    /**
     * Discharges a specific concrete test objective.
     * 
     * @param id
     *            The concrete test objective's id.
     */
    protected void discharge(int id) {
        LOGGER.log(Level.FINE, "Discharging id " + id);
        if (!testObjectives.containsKey(id)) {
            throw new IllegalArgumentException("Unknown test objective ID " + id);
        }
        else if (!registeredObjectives.contains(testObjectives.get(id))) {
            throw new IllegalArgumentException("Cannot discharge non-registered TO ID" + id);
        }
        else {
            TestTypeGroup to = testObjectives.get(id);
            if (to.isActive()) {
                LOGGER.log(Level.INFO, "Discharging TO " + to.getName());
                testTypeGroupMgr.dischargeTestTypeGroup(to, TestType.TEST_GOAL);
            }
            else {
                LOGGER.log(Level.WARNING, "Could not discharge TO id " + id + ": Was not active");
            }
        }
    }

    /**
     * Engages a specific concrete test objective.
     * 
     * @param id
     *            The concrete test objective's id.
     */
    protected void engage(int id) {
        LOGGER.log(Level.FINE, "Discharging id " + id);
        if (!testObjectives.containsKey(id)) {
            throw new IllegalArgumentException("Unknown test objective ID " + id);
        }
        else {
            TestTypeGroup to = testObjectives.get(id);
            LOGGER.log(Level.INFO, "Engaging TO " + to.getName());
            if (registeredObjectives.contains(to)) {
                testTypeGroupMgr.setActive(to, true);
            }
            else {
                to.setActive(true);
                testTypeGroupMgr.addTestTypeGroup(to);
                registeredObjectives.add(to);
            }
        }
    }

    /**
     * @param testObjectiveID
     *            The test objective ID.
     * @return {@code true} iff the test objective is active.
     */
    protected boolean isActive(int testObjectiveID) {
        if (!testObjectives.containsKey(testObjectiveID)) {
            throw new IllegalArgumentException("Unknown test objective ID " + testObjectiveID);
        }
        else {
            TestTypeGroup ttg = testObjectives.get(testObjectiveID);
            return testTypeGroupMgr.getActiveTestTypeGroupsWithTestGoal().contains(ttg);
        }
    }

    /**
     * @return The target LazyOTF instance.
     */
    protected LazyOTF getTargetLazyOTF() {
        return this.lazyOTF;
    }
    
    /**
     * @return The TO's name.
     */
    public String getName() {
        return name;
    }

    /**
     * An observer method for SUT interactions.
     * 
     * @param eir
     *            The SUT interaction represented by a
     *            {@link DriverSUTInteractionEvent}.
     */
    @ObserverMethod
    public void onSUTInteraction(DriverSUTInteractionEvent eir) {
        LOGGER.log(Level.INFO, "Got SUT interaction, fine!");
        processDriverInteraction(eir.interactionResult);
    }

    /**
     * An observer method for driver initialization events.
     * 
     * @param dsir
     *            The initialization result represented by a
     *            {@link DriverInitializedEvent}.
     */
    @ObserverMethod
    public void onInit(DriverInitializedEvent dsir) {
        LOGGER.log(Level.INFO, "Using the " + this.getClass().getName() + " TO policy.");
        for (int i = 0; i < testObjectiveAmount; i++) {
            createTestObjective(i, name);
        }
        registerTestObjectives();
    }
}
