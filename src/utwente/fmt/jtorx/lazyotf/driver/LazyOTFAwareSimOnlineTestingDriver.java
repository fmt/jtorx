package utwente.fmt.jtorx.lazyotf.driver;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTF.InactiveException;
import utwente.fmt.jtorx.lazyotf.LazyOTF.NotReadyException;
import utwente.fmt.jtorx.lazyotf.TestTreeNode;
import utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverFinalizationEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverInitializedEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverSUTInteractionEvent;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverCategorizingObservable;
import utwente.fmt.jtorx.lazyotf.utilities.Utils;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.torx.DriverInterActionResultConsumer;
import utwente.fmt.jtorx.torx.DriverSimInitializationResult;
import utwente.fmt.jtorx.torx.DriverSimInterActionResult;
import utwente.fmt.jtorx.torx.LabelPlusVerdict;
import utwente.fmt.jtorx.torx.SimOnLineTestingDriver;
import utwente.fmt.jtorx.utils.term.ConstantTerm;
import utwente.fmt.jtorx.utils.term.TermParser;
import utwente.fmt.jtorx.utils.term.VariableTerm;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Term;

/**
 * <p>
 * A wrapper for SimOnLineTestingDriver objects adding LazyOTF functionality.
 * </p>
 * 
 * <p>
 * This driver does not modify the configuration of the {@link LazyOTF}
 * instance. However, it can be linked to {@link LazyOTF} via a LazyOTF policy
 * observer to dynamically reconfigure the LazyOTF instance.
 * </p>
 * 
 */
public class LazyOTFAwareSimOnlineTestingDriver implements SimOnLineTestingDriver {
    /**
     * The driver's event categories.
     */
    public enum EventCategory {
        /**
         * Event: A SUT step has been executed and the policy observers are
         * about to be run.
         */
        PRE_POLICY_PHASE,

        /**
         * Event: A SUT step has been executed and the policy observers must be
         * run. Use this element only for policy observers.
         */
        POLICY_PHASE,

        /**
         * Event: A SUT step has been executed and the policy observers have
         * been run.
         */
        POST_POLICY_PHASE;
    }

    /**
     * The instance's event manager. Accepts notifications directly subclassing
     * {@link DriverEvents}.
     */
    private final ObserverCategorizingObservable<EventCategory> eventManager;

    /** The decorated driver. */
    private SimOnLineTestingDriver decoratedDriver;

    /** The LazyOTF instance. */
    private LazyOTF lazyOTFInstance;

    /**
     * Iff true, the auto mode is halted automatically as soon as all test
     * objectives have been discharged.
     */
    private final boolean doAutoStop;

    /**
     * Used to ensure that the auto mode gets stopped only once due to the
     * <i>doAutoStop</i> flag.
     */
    private boolean hasUsedAutoStop = false;

    /**
     * The message interpretation. LazyOTF currently only uses the prefix
     * notation.
     */
    private final IOLTSInterpretation interp;

    /** Controls the automatic testing process; if true, it aborts. */
    private boolean stopAuto = false;

    /** <tt>true</tt> iff the driver is currently in autostep mode. */
    private boolean isInAutoMode = false;

	/** Specifies a sleep period between test steps when using autostep. */
	private long delayAfterTeststepMs = 0l;

	/**
     * The class logger.
     */
    private static final Logger LOGGER = Logger.getLogger(LazyOTFAwareSimOnlineTestingDriver.class
            .getName());

	/**
     * The random number generator for this class.
     */
    private static final Random RANDOM = new Random();

    /**
     * Constructs a LazyOTFAwareSimOnlineTestingDriver.
     * 
     * @param drv
     *            The JTorX driver to be decorated.
     * @param lazyOTFInstance
     *            The LazyOTF instance to be used.
     * @param doAutoStop
     *            <tt>true</tt> if the autostep mode should be deactivated as
     *            soon as all test objectives have been discharged.
     * @param interp
     *            The JTorX message interpretation.
     */
    public LazyOTFAwareSimOnlineTestingDriver(SimOnLineTestingDriver drv, LazyOTF lazyOTFInstance,
            boolean doAutoStop, IOLTSInterpretation interp) {
        eventManager = new ObserverCategorizingObservable<>(EventCategory.POST_POLICY_PHASE,
                DriverEvents.class.getClasses());
        decoratedDriver = drv;
        this.lazyOTFInstance = lazyOTFInstance;
        this.doAutoStop = doAutoStop;
        this.interp = interp;
    }

    @Override
    public DriverSimInitializationResult init() {
        LOGGER.log(Level.FINE, "[progress] Beginning driver intialisation...");
        LOGGER.log(Level.FINE, "Initialising subdriver...");
        DriverSimInitializationResult result = decoratedDriver.init();
        LOGGER.log(Level.FINE, "Running observers for the init event.");
        onInit(result);
        LOGGER.log(Level.FINE, "[progress] Done initialising the driver instance.");
        return result;
    }

    @Override
    public DriverSimInterActionResult observe() {
        LOGGER.log(Level.FINE, "Observing...");
        DriverSimInterActionResult result = decoratedDriver.observe();
        LOGGER.log(Level.FINE, " Result: " + result.getLabel().getString());

        onStimulationOrObservation(result);

        return result;
    }

    @Override
    public DriverSimInterActionResult stimulate(Label l) {
        assert l != null;
        LOGGER.log(Level.FINE, "Stimulating SUT with " + l.getString());
        DriverSimInterActionResult result = decoratedDriver.stimulate(l);
        onStimulationOrObservation(result);
        return result;
    }

    @Override
    public DriverSimInterActionResult stimulate() {
        Vector<Label> inp = getModelInputs();
        assert (inp.size() > 0);

        if (inp.size() > 0) {
            if (lazyOTFInstance.isActive()) {
                Set<Label> lOTFprop = getCurrentLazyOTFProposal();

                Set<Label> inps = Utils.filterLabels(lOTFprop, interp.isInput());
                if (!inps.isEmpty()) {
                    /* Known: inps.size() == 1 */
                    return stimulate(inps.iterator().next());
                }
            }

            Label labelToTake = inp.get(RANDOM.nextInt(inp.size()));
            return stimulate(labelToTake);
        }
        else {
            return null;
        }

    }

    @Override
    public boolean autoStep(int n, DriverInterActionResultConsumer i) {
        boolean positiveVerdict = true;
        stopAuto = false;

        int nToUse;
        if (n == 0) {
            nToUse = -1;
        }
        else {
            nToUse = n;
        }

        isInAutoMode = true;

        for (int x = nToUse; x != 0 && positiveVerdict && !stopAuto; x--) {
            positiveVerdict = doAutomaticStep(i);
			if (this.delayAfterTeststepMs > 0l) {
				LOGGER.info("Sleeping for " + this.delayAfterTeststepMs + " ms "
						+ "after executing a test step.");
				try {
					Thread.sleep(this.delayAfterTeststepMs);
				} catch (InterruptedException e) {
					LOGGER.warning("Post-teststep sleep interrupted.");
				}
				LOGGER.info("Finished sleeping after executing a test step.");
			}
        }

        isInAutoMode = false;

        i.end();
        return positiveVerdict;
    }

    /**
     * Automatically stimulates or observes the SUT. If LazyOTF is active,
     * LazyOTF-proposed inputs are used. (See {@link # stimulate()})
     * 
     * @param i
     *            The JTorX driver interaction result consumer.
     * @return The verdict produced by {@code i} in response to the executed
     *         action.
     */
    private boolean doAutomaticStep(DriverInterActionResultConsumer i) {
        boolean verdict = true;

        boolean lazyOTFProposesDelta = false;
        
        Set<Label> proposal = null;
        try {
            proposal = lazyOTFInstance.propose();
            for (Label l : proposal) {
                if (l.getString().equals("delta")) {
                    lazyOTFProposesDelta = true;
                }
            }
        }
        catch (InactiveException | NotReadyException e) {
            lazyOTFProposesDelta = false;
        }
        
        if (getModelInputs().size() > 0 && !lazyOTFProposesDelta) {
            if (proposal != null) {
                /* LazyOTF proposes input => stimulate */
                verdict = i.consume(stimulate());
            }
            else {
                /*
                 * Both inputs and non-quiescence outputs => observe with a 50%
                 * chance
                 */
                int z = RANDOM.nextInt(2);
                if (z == 0) {
                    verdict = i.consume(observe());
                }
                else {
                    verdict = i.consume(stimulate());
                }
            }
        }
        else {
            /* No inputs available => observe */
            verdict = i.consume(observe());
        }

        return verdict;
    }

    @Override
    public boolean autoStep(DriverInterActionResultConsumer i) {
        return autoStep(-1, i);
    }

    @Override
    public DriverFinalizationResult finish() {
        LOGGER.log(Level.FINE, "Finishing...");
        DriverFinalizationResult result = decoratedDriver.finish();
        onFinish(result);
        return result;
    }

    @Override
    public Vector<Label> getModelInputs() {
        return getModelInputs(true, true);
    }

    /**
     * Computes the set of model inputs not belonging to the current LazyOTF
     * proposal.
     * 
     * @return A vector of input labels.
     */
    public Vector<Label> getModelInputsClean() {
        Label excludeLabel = null;
        if (lazyOTFInstance.isActive()) {
            Set<Label> prop = getCurrentLazyOTFProposal();
            Set<Label> inputs = Utils.filterLabels(prop, interp.isInput());
            if (inputs.size() != 0) {
                /* Only one input label can exist */
                excludeLabel = inputs.iterator().next();
            }
        }

        Vector<Label> ordInputs = getModelInputs(false, true);

        if (excludeLabel != null) {
            Vector<Label> result = new Vector<Label>();
            for (Label l : ordInputs) {
                if (!(l.getString() + "()").equals(excludeLabel.getString())) {
                    result.add(l);
                }
            }
            return result;
        }
        else {
            return ordInputs;
        }
    }

    /**
     * Computes the set of model inputs.
     * 
     * @param includeLazyOTFInputs
     *            Include the input proposed by LazyOTF.
     * @param includeModelInputs
     *            Include the ordinary model inputs.
     * @return A vector of input labels.
     */
    public Vector<Label> getModelInputs(boolean includeLazyOTFInputs, boolean includeModelInputs) {
        Vector<Label> result = new Vector<Label>();

        if (includeModelInputs) {
            result.addAll(decoratedDriver.getModelInputs());
        }

        if (lazyOTFInstance.isActive() && includeLazyOTFInputs) {
            Set<Label> inpProp = getCurrentLazyOTFProposal();
            if (inpProp == null) {
                LOGGER.log(Level.WARNING, "Can't get lOTF proposal!");
            }
            else {
                Set<Label> inputs = Utils.filterLabels(inpProp, interp.isInput());
                if (includeModelInputs) {
                    /* Only add labels not already present in result */
                    for (Label l : inputs) {
                        Term labelTerm = TermParser.parse(l);

                        /*
                         * Label variable-free --> already present in decorated
                         * drivers menu
                         */
                        boolean hasVariables = false;
                        LinkedList<Term> termStack = new LinkedList<Term>();
                        termStack.push(labelTerm);
                        /*
                         * Look through the term tree both for uninstantiated
                         * and instantiated variables
                         */
                        while (!termStack.isEmpty() && !hasVariables) {
                            Term t = termStack.pop();
                            if (t instanceof VariableTerm) {
                                hasVariables = true;
                            }
                            if (t.subTerms() != null) {
                                boolean mightHaveInstantiatedVar = false;
                                if (t.getOperator() != null && !t.getOperator().equals("")) {
                                    // constant term, not root => might have an
                                    // instantiated var
                                    mightHaveInstantiatedVar = true;
                                }
                                for (Term st : t.subTerms()) {
                                    if (st instanceof ConstantTerm && !st.getOperator().equals("")
                                            && mightHaveInstantiatedVar) {
                                        // constant term (not root) as parent,
                                        // contains operator => instantiated var
                                        // TODO: is this always the case?
                                        hasVariables = true;
                                    }
                                    termStack.push(st);
                                }
                            }
                        }
                        if (hasVariables) {
                            result.add(l);
                        }
                    }
                }
                else {
                    result.addAll(inputs);
                }

            }
        }
        return result;
    }

    @Override
    public Vector<LabelPlusVerdict> getModelOutputVerdicts() {
        return decoratedDriver.getModelOutputVerdicts();
    }

    @Override
    public Vector<Label> getModelOutputs() {
        return decoratedDriver.getModelOutputs();
    }

    @Override
    public CompoundState getTesterState() {
        return decoratedDriver.getTesterState();
    }

    @Override
    public DriverInterActionResult randomStep() {
        return decoratedDriver.randomStep();
    }
    
    public long getDelayAfterTeststepMs() {
		return delayAfterTeststepMs;
	}

	public void setDelayAfterTeststepMs(long delayAfterTeststepMs) {
		this.delayAfterTeststepMs = delayAfterTeststepMs;
	}

    @Override
    public void stopAuto() {
        decoratedDriver.stopAuto();
        stopAuto = true;
    }

    /**
     * Instructs the underlying LazyOTF instance to compute a new test tree.
     */
    public void recomputeTestTree() {
        try {
            if (getConcatenatedTorXStateID().isEmpty()) {
                throw new IllegalStateException("BAD: Got empty TorX state!");
            }

            lazyOTFInstance.computeNewTestTree(getConcatenatedTorXStateID());
        }
        catch (NumberFormatException e) {
            LOGGER.log(Level.SEVERE,
                    "recomputeTestTree: The current TorX state ID was not parseable as an integer");
            e.printStackTrace();
        }
        catch (InactiveException e) {
            LOGGER.log(Level.SEVERE,
                    "recomputeTestTree: Could not recompute the test tree; LazyOTF remains inactive");
            e.printStackTrace();
        }
    }

    /**
     * Determines the current TorX state ID.
     * 
     * @return The current TorX state ID.
     */
    private Set<String> getConcatenatedTorXStateID() {
        Set<String> result = new HashSet<String>();
        Iterator<? extends State> itx = getTesterState().getSubStates();
        while (itx.hasNext()) {
            result.add(itx.next().getID());
        }

        return result;
    }

    /**
     * A method to be called after executing an observation or a stimulation
     * which calls this class' observers, passing them an
     * ExtendedInteractionResult object.
     * 
     * @param result
     *            The decorated driver's observation/stimulation result.
     */
    private void onStimulationOrObservation(DriverSimInterActionResult result) {
        boolean wasFirstInteraction = lazyOTFInstance.hasFreshTestTree();
        if (result != null) {
            try {
                lazyOTFInstance.tookTransition(result);
            }
            catch (LazyOTF.LazyOTFException e) {
                // Ignore -- the policy deals with the LazyOTF state later.
            }
        }

        boolean testGoalHit = false;
        boolean inducingStateHit = false;

        try {
            TestTreeNode currentTTN = lazyOTFInstance.getTestTreeNode(-1);
            testGoalHit = !currentTTN.getTestGoals().isEmpty();
            inducingStateHit = !currentTTN.getInducingStates().isEmpty();
        }
        catch (InactiveException e) {
            // Instance was inactive => no test goal or inducing state hit.
        }
        
        boolean hasDetectedACycle = false;
        if (!testGoalHit) {
        	hasDetectedACycle = lazyOTFInstance.hasDetectedACycle();
        }

        onSUTInteraction(new ExtendedInteractionResult(result, testGoalHit, inducingStateHit,
                wasFirstInteraction, getTesterState(), interp, hasDetectedACycle));

        TestTypeGroupMgr ttgMgr = lazyOTFInstance.getLocationManager().getTestTypeGroupMgr();

        synchronized (this) {
            if (isInAutoMode && doAutoStop && !hasUsedAutoStop
                    && ttgMgr.isEveryTestObjectiveDischarged()) {
                stopAuto = true;
                hasUsedAutoStop = true;
            }
        }
    }

    /**
     * Produces LazyOTF's current proposal.
     * 
     * @return The labels contained in the current LazyOTF's proposal. If
     *         LazyOTF is inactive, an empty set is returned.
     */
    public Set<Label> getCurrentLazyOTFProposal() {
        try {
            return lazyOTFInstance.propose();
        }
        catch (InactiveException | NotReadyException e) {
            return new HashSet<Label>(); // LazyOTF inactive or has no test tree
            // => return empty set.
        }
    }

    /**
     * @return The current JTorX message interpretation.
     */
    public IOLTSInterpretation getInterpretation() {
        return interp;
    }

    /**
     * Notifies the observers about a SUT interaction. The observers are
     * executed in order of phases, i.e. first PRE_POLICY_PHASE, then
     * POLICY_PHASE and finally POST_POLICY_PHASE observers.
     * 
     * @param eir
     *            The {@link ExtendedInteractionResult} object which gets passed
     *            to the observers.
     */
    private void onSUTInteraction(ExtendedInteractionResult eir) {
        DriverSUTInteractionEvent event = new DriverSUTInteractionEvent(eir);
        eventManager.notifyObservers(event, EventCategory.PRE_POLICY_PHASE);
        eventManager.notifyObservers(event, EventCategory.POLICY_PHASE);
        eventManager.notifyObservers(event, EventCategory.POST_POLICY_PHASE);
    }

    /**
     * Notifies the observers about an initialization event.
     * 
     * @param dsir
     *            The JTorX {@link DriverSimInitializationResult} object
     *            produced by the decorated driver.
     */
    private void onInit(DriverSimInitializationResult dsir) {
        eventManager.notifyObservers(new DriverInitializedEvent(dsir));
    }

    /**
     * Notifies the observers about an finalization event.
     * 
     * @param dfr
     *            The JTorX {@link DriverFinalizationResult} object produced by
     *            the decorated driver.
     */
    private void onFinish(DriverFinalizationResult dfr) {
        eventManager.notifyObservers(new DriverFinalizationEvent(dfr));
    }

    /**
     * Adds an observer to the driver.
     * 
     * @param observer
     *            An {@link Observer} instance. This instance may have
     *            {@link utwente.fmt.jtorx.lazyotf.utilities.ObserverMethod
     *            ObserverMethods} for types directly subclassing
     *            {@link DriverEvents}.
     * @param category
     *            The observer's event category. See {@link EventCategory}.
     */
    public void addObserver(Observer observer, EventCategory category) {
        eventManager.addObserver(observer, category);
    }

    /**
     * An extended representation of driver interaction results.
     */
    public class ExtendedInteractionResult {
        /** The driver's original interaction result. */
        private final DriverSimInterActionResult interactionResult;

        /** true iff a test goal is hit by this interaction. */
        private boolean isTestGoalHit;

        /** true iff an inducing state is hit by this interaction. */
        private boolean isInducingStateHit;

        /**
         * true iff this interaction was the first transition in the current
         * test tree.
         */
        private boolean isFirstTransitionInTestTree;

        /**
         * true iff this interaction was the last transition in the current test
         * tree.
         */
        private boolean isLastTransitionInTestTree;

        /** The driver's state. */
        private final CompoundState testerState;

        /** The LTS' label interpretation. */
        private final IOLTSInterpretation interp;
        
        /** If true, a cycle has been detected at the current step. */
        private final boolean hasDetectedACycle;
        
        /**
         * Constructs an ExtendedInteractionResult data structure.
         * 
         * @param interactionResult
         *            The driver's original interaction result.
         * @param isTestGoalHit
         *            true iff a test goal is hit by this interaction.
         * @param isInducingStateHit
         *            true iff an inducing state is hit by this interaction.
         * @param isFirstTransitionInTestTree
         *            true iff this interaction was the first transition in the
         *            current test tree.
         * @param testerState
         *            The driver's state.
         * @param interp
         *            The LTS' label interpretation.
         */
        public ExtendedInteractionResult(DriverSimInterActionResult interactionResult,
                boolean isTestGoalHit, boolean isInducingStateHit,
                boolean isFirstTransitionInTestTree, CompoundState testerState,
                IOLTSInterpretation interp, boolean hasDetectedACycle) {
            this.interactionResult = interactionResult;
            this.isFirstTransitionInTestTree = isFirstTransitionInTestTree;
            this.isInducingStateHit = isInducingStateHit;
            isLastTransitionInTestTree = false;
            this.isTestGoalHit = isTestGoalHit;
            this.testerState = testerState;
            this.interp = interp;
            this.hasDetectedACycle = hasDetectedACycle;
        }

        /**
         * @return The driver's original interaction result.
         */
        public DriverSimInterActionResult getInteractionResult() {
            return interactionResult;
        }

        /**
         * @return true iff a test goal is hit by this interaction.
         */
        public boolean isTestGoalHit() {
            return isTestGoalHit;
        }

        /**
         * @return true iff an inducing state is hit by this interaction.
         */
        public boolean isInducingStateHit() {
            return isInducingStateHit;
        }

        /**
         * @return true iff this interaction was the last transition in the
         *         current test tree.
         */
        public boolean isLastTransitionInTestTree() {
            return isLastTransitionInTestTree;
        }

        /**
         * @return true iff this interaction was the first transition in the
         *         current test tree.
         */
        public boolean isFirstTransitionInTestTree() {
            return isFirstTransitionInTestTree;
        }

        /**
         * @return The driver's state.
         */
        public CompoundState getTesterState() {
            return testerState;
        }
        
        /**
         * @return The LTS' label interpretation.
         */
        public IOLTSInterpretation getInterpretation() {
            return interp;
        }
        
        /**
         * @return <tt>true</tt> iff a cycle has been detected at the current step.
         */
        public boolean hasDetectedACycle() {
        	return hasDetectedACycle;
        }
    }

    /**
     * An interface collecting the driver event classes.
     */
    public interface DriverEvents {
        /** The driver initialization event class. */
        public static final class DriverInitializedEvent {
            /**
             * Constructs the driver initialization event object.
             * 
             * @param initResult
             *            The driver initialization result.
             */
            public DriverInitializedEvent(DriverSimInitializationResult initResult) {
                initializationResult = initResult;
            }

            /** The JTorX driver initialization result. */
            public final DriverSimInitializationResult initializationResult;
        }

        /** The SUT interaction event class. */
        public static final class DriverSUTInteractionEvent {
            /**
             * Constructs the driver SUT interaction event object.
             * 
             * @param interactionResult
             *            The SUT interaction result.
             */
            public DriverSUTInteractionEvent(ExtendedInteractionResult interactionResult) {
                this.interactionResult = interactionResult;
            }

            /**
             * The extended interaction result (containing the JTorX driver
             * interaction result).
             */
            public final ExtendedInteractionResult interactionResult;
        }

        /** The driver finalization event class. */
        public static final class DriverFinalizationEvent {
            /**
             * Constructs he driver finalization event object.
             * 
             * @param finalizationResult
             *            The driver finalization result.
             */
            public DriverFinalizationEvent(DriverFinalizationResult finalizationResult) {
                super();
                this.finalizationResult = finalizationResult;
            }

            /** The JTorX driver finalization result. */
            public final DriverFinalizationResult finalizationResult;
        }
    }
}
