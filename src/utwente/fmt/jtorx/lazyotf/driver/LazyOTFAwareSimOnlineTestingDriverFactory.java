package utwente.fmt.jtorx.lazyotf.driver;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.LazyOTFPolicyFactory;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.EventCategory;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.torx.SimOnLineTestingDriver;

/**
 * A factory class for {@link LazyOTFAwareSimOnlineTestingDriver}.
 */
public final class LazyOTFAwareSimOnlineTestingDriverFactory {
        
    /**
     * Makes a new {@link LazyOTFAwareSimOnlineTestingDriver} instance using the
     * given JTorX driver and links it to the given {@link LazyOTF} instance
     * using a LazyOTF policy. Also, this method installs the observers provided
     * by the {@link PolicyObserverFactoryRegistry}.
     * 
     * @param drv
     *            The JTorX driver which should be decorated.
     * @param lazyOTFInstance
     *            The LazyOTF instance.
     * @param lazyOTFConfig
     *            The LazyOTF instance's configuration.
     * @param policyFactory
     *            The policy object factory.
     * @param interp
     *            The JTorX transition interpretation.
     * @return The LazyOTF-decorated JTorX driver.
     */
    public static LazyOTFAwareSimOnlineTestingDriver makeDriver(SimOnLineTestingDriver drv,
            LazyOTF lazyOTFInstance, LazyOTFConfig lazyOTFConfig,
            LazyOTFPolicyFactory policyFactory, IOLTSInterpretation interp) {

        //RemoveWithoutGeneratePassiveTO.registerWithDriverPrePolicyObserverRegistry(6);
        
        LazyOTFAwareSimOnlineTestingDriver lazyOTFDrv = new LazyOTFAwareSimOnlineTestingDriver(drv,
                lazyOTFInstance, lazyOTFConfig.doAutoStop(), interp);
        Observer policyObs = policyFactory.newLazyOTFPolicyObserver();

        for (Observer prePolObs : PolicyObserverFactoryRegistry.getInstance().createPrePolObservers(lazyOTFInstance)) {
            lazyOTFDrv.addObserver(prePolObs, EventCategory.PRE_POLICY_PHASE);
        }
        
        for (Observer postPolObs : PolicyObserverFactoryRegistry.getInstance().createPostPolObservers(lazyOTFInstance)) {
            lazyOTFDrv.addObserver(postPolObs, EventCategory.POST_POLICY_PHASE);
        }

        lazyOTFDrv.addObserver(policyObs, EventCategory.POLICY_PHASE);
        return lazyOTFDrv;
    }

    /**
     * Utility class -- cannot be instantiated.
     */
    private LazyOTFAwareSimOnlineTestingDriverFactory() {
    }
}
