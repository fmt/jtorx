package utwente.fmt.jtorx.lazyotf.driver;

import java.io.PrintStream;
import java.util.Calendar;
import java.util.Iterator;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverFinalizationEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverInitializedEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverSUTInteractionEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.EventCategory;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.ExtendedInteractionResult;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverMethod;
import utwente.fmt.jtorx.torx.DriverSimInterActionResult;
import utwente.fmt.lts.State;

/**
 * LazyOTFObservatory observes and protocols the test run, outputting its
 * findings to a stream.
 */
public class LazyOTFObservatory {
    /** The test run's LazyOTF instance. */
    private final LazyOTF lazyOTFInstance;

    /** The stream to which the observatory writes. */
    private final PrintStream outputStream;

    /** The test run's LazyOTF-enabled driver. */
    private final LazyOTFAwareSimOnlineTestingDriver driver;

    /**
     * Constructs a new {@link LazyOTFObservatory} instance.
     * 
     * @param driver
     *            The test run's LazyOTF-enabled driver.
     * @param lazyOTF
     *            The test run's LazyOTF instance.
     * @param output
     *            The stream to which the observatory writes.
     */
    public LazyOTFObservatory(LazyOTFAwareSimOnlineTestingDriver driver, LazyOTF lazyOTF,
            PrintStream output) {
        lazyOTFInstance = lazyOTF;
        outputStream = output;
        this.driver = driver;
        attachDriver();

        // TODO: get init event from driver.
        reset();
        startTime = System.currentTimeMillis();
    }

    /** A local test step counter; will eventually be removed. */
    private int stepCounter = 0;

    /** The test start time. */
    private long startTime = 0;

    /** The test stop time. */
    private long stopTime = 0;

    /**
     * Attachs the observatory to the LazyOTF driver.
     */
    private void attachDriver() {
        driver.addObserver(new Observer() {
            @ObserverMethod
            public void onSUTInteraction(DriverSUTInteractionEvent eir) {
                addInteractionResult(eir.interactionResult);
            }

            @ObserverMethod
            public void onInit(DriverInitializedEvent dsir) {
                reset();
            }

            @ObserverMethod
            public void onFinish(DriverFinalizationEvent dfr) {
                stopTime = dfr.finalizationResult.getStartTime();
                printStatistics();
            }
        }, EventCategory.POST_POLICY_PHASE);
    }

    /**
     * Resets the counters and prints the date to the output stream.
     */
    private void reset() {
        stepCounter = 0;
        startTime = -1;
        stopTime = -1;
        outputStream.println(Calendar.getInstance().getTime().toString());
    }

    /**
     * Makes a string representation of the given TorX states.
     * 
     * @param states
     *            A set of TorX states.
     * @return A string representation of the states given in {@code states}.
     */
    private String makeStateSetRepresentation(Iterator<? extends State> states) {
        String result = "";
        while (states.hasNext()) {
            result += states.next().getNodeName();
            if (states.hasNext()) {
                result += ", ";
            }
        }
        return result;
    }

    /**
     * Prints statistics about the test to the output stream.
     */
    private void printStatistics() {
        outputStream.println();
        outputStream.println("*** Statistics ********************************************");

        outputStream.println("ExecutedTestSteps: " + stepCounter);

        TestTypeGroupMgr ttgMgr = lazyOTFInstance.getLocationManager().getTestTypeGroupMgr();
        int testObjCount = ttgMgr.getTestTypeGroups().size();
        int activeObjCount = ttgMgr.getActiveTestTypeGroupsWithTestGoal().size();
        int externalDischargedCount = ttgMgr.getNumberOfTTGsDischargedFromExtern();
        
        outputStream.println("DischargedTO: " + (testObjCount - activeObjCount));
        outputStream.println("MissedTO: " +  activeObjCount);
        outputStream.println("ElapsedMilliseconds: " + (stopTime - startTime));
        outputStream.println("ExternallyDischargedTO: " + externalDischargedCount);
    }

    /**
     * @return The current computation-time test tree traversal depth.
     */
    private int getCurrentRecursionDepth() {
        return lazyOTFInstance.getTraversalDepthComputer().getTraversalDepth();
    }

    /**
     * Processes a SUT interaction result and prints the result to the output
     * stream.
     * 
     * @param result
     *            The SUT interaction result.
     */
    private void addInteractionResult(ExtendedInteractionResult result) {
        stepCounter++;

        DriverSimInterActionResult driverRes = result.getInteractionResult();
        outputStream.print(driverRes.getStepNr() + ";\t");
        outputStream.print(driverRes.getTransition().getLabel().getString() + ";\t");
        Iterator<? extends State> subStates = result.getTesterState().getSubStates();
        outputStream.print(makeStateSetRepresentation(subStates) + ";\t");
        outputStream.print(getCurrentRecursionDepth() + ";\t");

        if (result.isFirstTransitionInTestTree()) {
            outputStream.print("F");
        }
        if (result.isLastTransitionInTestTree()) {
            outputStream.print("L");
        }
        if (result.isTestGoalHit()) {
            outputStream.print("T");
        }
        if (result.isInducingStateHit()) {
            outputStream.print("I");
        }
        if (result.hasDetectedACycle()) {
        	outputStream.print("C");
        }
        outputStream.println();
    }

}
