package utwente.fmt.jtorx.lazyotf.driver;

import java.util.ArrayList;
import java.util.List;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;

/**
 * <p>
 * A registry for observers getting run by the
 * {@link LazyOTFAwareSimOnlineTestingDriver} between the execution of a test
 * step and the execution of the driver's policy phase.
 * </p>
 * 
 * <p>
 * This class is intended to ease the direct implementation of test objective
 * policies via an {@link Observer} observing the test run. Taking care of
 * enabling and discharging their own set of test type groups, they can
 * punctually extend the internal policies.
 * </p>
 */
public final class PolicyObserverFactoryRegistry {

    /**
     * An interface for observers getting run before executing the driver policy
     * phase.
     */
    public interface PolicyObserverFactory {
        /**
         * Make a new observer to be run before the policy phase of the driver.
         * 
         * @param lazyOTFInstance
         *            The target (uninitialized) LazyOTF instance.
         * @return The new observer.
         */
        Observer makeObserver(LazyOTF lazyOTFInstance);
    }

    /**
     * A list of policy observer factories for pre-policy execution.
     */
    private List<PolicyObserverFactory> prePolicyObserverFactories = new ArrayList<>();
    
    /**
     * A list of policy observer factories for post-policy execution.
     */
    private List<PolicyObserverFactory> postPolicyObserverFactories = new ArrayList<>();

    /**
     * The singleton instance of this class.
     */
    private static final  PolicyObserverFactoryRegistry instance = new PolicyObserverFactoryRegistry();
    
    /**
     * @return The singleton instance of this class.
     */
    public static PolicyObserverFactoryRegistry getInstance() {
    	return instance;
    }
    
    /**
     * Adds a PolicyObserverFactory to the list of pre-policy-phase observer
     * factories. The factories are invoked when the LazyOTF driver wrapper gets
     * created. This method is intended to register observers managing test
     * goals, for example extending {@link AbstractMetaTestObjectiveObserver},
     * without modifying the LazyOTF packages.
     * 
     * @param factory
     *            The factory which gets added.
     */
    public void addPrePolicyObserverFactory(PolicyObserverFactory factory) {
        prePolicyObserverFactories.add(factory);
    }
    
    /**
     * Adds a PolicyObserverFactory to the list of post-policy-phase observer
     * factories. The factories are invoked when the LazyOTF driver wrapper gets
     * created.
     * 
     * @param factory
     *            The factory which gets added.
     */
    public void addPostPolicyObserverFactory(PolicyObserverFactory factory) {
        postPolicyObserverFactories.add(factory);
    }

    /**
     * Invokes all registered pre-policy-phase observer factories and gathers
     * the results.
     * 
     * @param lazyOTFInstance
     *            The target LazyOTF instance.
     * @return The list of created observers.
     */
    public List<Observer> createPrePolObservers(LazyOTF lazyOTFInstance) {
        List<Observer> result = new ArrayList<>();
        for (PolicyObserverFactory factory : prePolicyObserverFactories) {
            result.add(factory.makeObserver(lazyOTFInstance));
        }

        return result;
    }
    
    /**
     * Invokes all registered post-policy-phase observer factories and gathers
     * the results.
     * 
     * @param lazyOTFInstance
     *            The target LazyOTF instance.
     * @return The list of created observers.
     */
    public List<Observer> createPostPolObservers(LazyOTF lazyOTFInstance) {
        List<Observer> result = new ArrayList<>();
        for (PolicyObserverFactory factory : postPolicyObserverFactories) {
            result.add(factory.makeObserver(lazyOTFInstance));
        }

        return result;        
    }
    
    /**
     * Removes all observer factories from the registry.
     */
    public void clear() {
        prePolicyObserverFactories.clear();
        postPolicyObserverFactories.clear();
    }

    /** Singleton class - cannot be instantiated. */
    private PolicyObserverFactoryRegistry() {
    }
}
