package utwente.fmt.jtorx.lazyotf.guidance;

/**
 * A representation for LazyOTF {@link Path2Weight} configured with a
 * NodeValue2Weight instance.
 */
public interface CustomPath2Weight extends Path2Weight {
    /**
     * Sets the NodeValue2Weight instance.
     * 
     * @param it
     *            The NodeValue2Weight instance to be set.
     */
    void setNodeValue2Weight(NodeValue2Weight it);

    /**
     * Gets the NodeValue2Weight instance.
     * 
     * @return The NodeValue2Weight instance associated with this guidance
     *         method.
     */
    NodeValue2Weight getNodeValue2Weight();
}
