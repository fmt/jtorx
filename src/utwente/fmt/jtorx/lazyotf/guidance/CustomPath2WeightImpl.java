package utwente.fmt.jtorx.lazyotf.guidance;

/**
 * A default implementation of {@link Path2Weight}.
 */
public class CustomPath2WeightImpl implements CustomPath2Weight {
    /**
     * A {@link SimplePath2Weight} instance used for storing the name and the
     * displayName.
     */
    private final SimplePath2Weight backingPath2Weight;

    /**
     * The {@link NodeValue2Weight} instance associated with this guidance
     * method.
     */
    private NodeValue2Weight nodeWeigher;

    /**
     * Construct a new {@link CustomPath2WeightImpl} instance.
     * 
     * @param name
     *            The instance's NodeValue2Weight name.
     * @param displayName
     *            The instance's display name.
     */
    public CustomPath2WeightImpl(String name, String displayName) {
        backingPath2Weight = SimplePath2Weight.newInstance(name, displayName);
    }

    /**
     * Make a new {@link CustomPath2WeightImpl} instance, initially not carrying a
     * node weigher.
     * 
     * @param name
     *            The instance's NodeValue2Weight name.
     * @param displayName
     *            The instance's display name.
     * @return The newly constructed {@link CustomPath2WeightImpl} instance.
     */
    public static CustomPath2WeightImpl newInstance(String name, String displayName) {
        return new CustomPath2WeightImpl(name, displayName);
    }

    /**
     * Make a new {@link CustomPath2WeightImpl} instance.
     * 
     * @param name
     *            The instance's NodeValue2Weight name.
     * @param displayName
     *            The instance's display name.
     * @param nodeWeigher
     *            The instance's node weigher.
     * @return The newly constructed {@link CustomPath2WeightImpl} instance.
     */
    public static CustomPath2WeightImpl newInstance(String name, String displayName,
            NodeValue2Weight nodeWeigher) {
        CustomPath2WeightImpl nc;
        nc = new CustomPath2WeightImpl(name, displayName);
        nc.setNodeValue2Weight(nodeWeigher);
        return nc;
    }

    /**
     * Make a new {@link CustomPath2WeightImpl} instance by copying the given one.
     * 
     * @param prefab
     *            The {@link CustomPath2WeightImpl} instance to be copied.
     * @return The copy.
     */
    public static CustomPath2WeightImpl newInstance(CustomPath2Weight prefab) {
        return CustomPath2WeightImpl.newInstance(prefab.getName(),
                prefab.getDisplayName(), prefab.getNodeValue2Weight());
    }

    @Override
    public String getDisplayName() {
        return backingPath2Weight.getDisplayName();
    }

    @Override
    public void setDisplayName(String name) {
        backingPath2Weight.setDisplayName(name);
    }

    @Override
    public void accept(Path2WeightVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String getName() {
        return backingPath2Weight.getName();
    }

    @Override
    public void setNodeValue2Weight(NodeValue2Weight it) {
        nodeWeigher = it;
    }

    @Override
    public NodeValue2Weight getNodeValue2Weight() {
        return nodeWeigher;
    }

    @Override
    public CustomPath2Weight clone() {
        CustomPath2Weight clone = CustomPath2WeightImpl.newInstance(getName(),
                getDisplayName());
        clone.setNodeValue2Weight(getNodeValue2Weight());
        return clone;
    }
}
