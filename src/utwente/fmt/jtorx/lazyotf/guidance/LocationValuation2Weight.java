package utwente.fmt.jtorx.lazyotf.guidance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import utwente.fmt.jtorx.utils.Pair;

/**
 * A class representing LazyOTF's LocationValuation2Weight node value weigher.
 */
public class LocationValuation2Weight implements NodeValue2Weight {
    /** The set of condition/evaluation expression pairs. */
    private final List<Pair<String, String>> expressions;

    /** The instance's display name. */
    private String displayName;

    /** The weigher's internal LazyOTF name. */
    private static final String NV2W_NAME = "LocationValuation2Weight";

    /**
     * Construct a new LocationValuation2Weight instance.
     * 
     * @param displayName
     *            The instance's display name.
     * @param expressions
     *            The instance's initial set of condition/evaluation expression
     *            pairs. May be null.
     */
    public LocationValuation2Weight(String displayName, List<Pair<String, String>> expressions) {
        this.displayName = displayName;
        if (expressions != null) {
            this.expressions = expressions;
        }
        else {
            this.expressions = new ArrayList<Pair<String, String>>();
        }
    }

    /**
     * Construct a new LocationValuation2Weight instance with an empty display
     * name and no initial expression pairs.
     */
    public LocationValuation2Weight() {
        this("", null);
    }

    /**
     * Add a condition/evaluation expression pair to the instance.
     * 
     * @param condition
     *            The condition to be added.
     * @param expression
     *            The expression to be evaluated when the condition holds.
     */
    public void addExpression(String condition, String expression) {
        expressions.add(new Pair<>(condition, expression));
    }

    /**
     * @return The set of condition/evaluation pairs managed by this instance.
     */
    public Collection<Pair<String, String>> getExpressions() {
        return expressions;
    }

    /**
     * Make a new LocationValuation2Weight instance.
     * 
     * @param displayName
     *            The instance's display name.
     * @param expressions
     *            The instance's initial set of condition/evaluation expression
     *            pairs. May be null.
     * @return The newly created LocationValuation2Weight instance.
     */
    public static LocationValuation2Weight newInstance(String displayName,
            List<Pair<String, String>> expressions) {
        return new LocationValuation2Weight(displayName, expressions);
    }

    @Override
    public String getName() {
        return NV2W_NAME;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public void setDisplayName(String name) {
        displayName = name;
    }

    @Override
    public void accept(NodeValue2WeightVisitor v) {
        v.visit(this);
    }
}
