package utwente.fmt.jtorx.lazyotf.guidance;

/**
 * A class representing LazyOTF's Locations2Weight node value weigher.
 */
public class Locations2Weight implements NodeValue2Weight {
    /** The weight assigned to ordinary locations. */
    private int ordinaryWeight;

    /** The weight assigned to inducing locations. */
    private int inducingWeight;

    /** The weight assigned to test goal locations. */
    private int testGoalWeight;

    /** The method's display name, intended for the user. */
    private String displayName;

    /** The method's internal LazyOTF name. */
    private static final String INTERNALNAME = "Locations2Weight";

    /**
     * Construct a new Locations2Weight instance.
     * 
     * @param displayName
     *            The instance's display name.
     * @param inducingWeight
     *            The weight to be assigned to inducing locations.
     * @param ordinaryWeight
     *            The weight to be assigned to ordinary locations.
     * @param testGoalWeight
     *            The weight to be assigned to test goal locations.
     */
    public Locations2Weight(String displayName, int inducingWeight, int ordinaryWeight,
            int testGoalWeight) {
        this.displayName = displayName;
        this.inducingWeight = inducingWeight;
        this.ordinaryWeight = ordinaryWeight;
        this.testGoalWeight = testGoalWeight;
    }

    /**
     * Construct a new Locations2Weight instance by copying the given one.
     * 
     * @param prefab
     *            The Locations2Weight instance to be copied.
     */
    public Locations2Weight(Locations2Weight prefab) {
        this(prefab.getDisplayName(), prefab.getInducingWeight(), prefab.getOrdinaryWeight(),
                prefab.getTestGoalWeight());
    }

    /**
     * @return The weight assigned to ordinary locations.
     */
    public int getOrdinaryWeight() {
        return ordinaryWeight;
    }

    /**
     * @return The weight assigned to inducing locations.
     */
    public int getInducingWeight() {
        return inducingWeight;
    }

    /**
     * @return The weight assigned to test goal locations.
     */
    public int getTestGoalWeight() {
        return testGoalWeight;
    }

    /**
     * Set the weight assigned to ordinary locations.
     * 
     * @param weight
     *            The weight of ordinary locations.
     */
    public void setOrdinaryWeight(int weight) {
        ordinaryWeight = weight;
    }

    /**
     * Set the weight assigned to inducing locations.
     * 
     * @param weight
     *            The weight of test-inducing locations.
     */
    public void setInducingWeight(int weight) {
        inducingWeight = weight;
    }

    /**
     * Set the weight assigned to test goal locations.
     * 
     * @param weight
     *            The weight of test goal locations.
     */
    public void setTestGoalWeight(int weight) {
        testGoalWeight = weight;
    }

    /**
     * Make a new Locations2Weight instance.
     * 
     * @param displayName
     *            The instance's display name.
     * @param inducingWeight
     *            The weight to be assigned to inducing locations.
     * @param ordinaryWeight
     *            The weight to be assigned to ordinary locations.
     * @param testGoalWeight
     *            The weight to be assigned to test goal locations.
     * @return The newly constructed Locations2Weight instance.
     */
    public static Locations2Weight newInstance(String displayName, int inducingWeight,
            int ordinaryWeight, int testGoalWeight) {
        return new Locations2Weight(displayName, inducingWeight, ordinaryWeight, testGoalWeight);
    }

    @Override
    public String getName() {
        return INTERNALNAME;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public void setDisplayName(String name) {
        displayName = name;
    }

    @Override
    public void accept(NodeValue2WeightVisitor v) {
        v.visit(this);
    }
}
