package utwente.fmt.jtorx.lazyotf.guidance;

/**
 * An interface for representations of LazyOTF's node value weighers.
 */
public interface NodeValue2Weight {
    /**
     * Get the weigher's internal (LazyOTF-implementation specific) name.
     * 
     * @return The weigher's internal name.
     */
    String getName();

    /**
     * Get the weigher's display name. This name is arbitrary and intended to be
     * set by the user.
     * 
     * @return The display name.
     */
    String getDisplayName();

    /**
     * Set the weigher's display name. This name is arbitrary and intended to be
     * set by the user.
     * 
     * @param name
     *            The new display name.
     */
    void setDisplayName(String name);

    /**
     * Acceptor method for Visitor pattern implementations.
     * 
     * @param v
     *            The visitor whose visit method is to be called.
     */
    void accept(NodeValue2WeightVisitor v);
}
