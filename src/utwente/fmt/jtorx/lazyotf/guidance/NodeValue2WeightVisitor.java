package utwente.fmt.jtorx.lazyotf.guidance;

/**
 * A visitor class for {@link NodeValue2Weight}.
 */
public interface NodeValue2WeightVisitor {
    /**
     * Visit the given NodeValue2Weight instance.
     * 
     * @param it
     *            The instance to be visited.
     */
    void visit(NodeValue2Weight it);

    /**
     * Visit the given Locations2Weight instance.
     * 
     * @param it
     *            The instance to be visited.
     */
    void visit(Locations2Weight it);

    /**
     * Visit the given LocationValuation2Weight instance.
     * 
     * @param it
     *            The instance to be visited.
     */
    void visit(LocationValuation2Weight it);

    /**
     * Visit the given SuperLocationSize2Weight instance.
     * 
     * @param it
     *            The instance to be visited.
     */
    void visit(SuperLocationSize2Weight it);

    /**
     * An "adapter" class implementing the enclosing interface. This class is
     * intended to be subclassed. In this implementation, the visit methods call
     * otherMethodCalled (unless you override them, of course).
     */
    public static class Adapter implements NodeValue2WeightVisitor {
        @Override
        public void visit(NodeValue2Weight it) {
            otherMethodCalled(it);
        }

        @Override
        public void visit(Locations2Weight it) {
            otherMethodCalled(it);
        }

        @Override
        public void visit(LocationValuation2Weight it) {
            otherMethodCalled(it);
        }

        @Override
        public void visit(SuperLocationSize2Weight it) {
            otherMethodCalled(it);
        }

        /**
         * Gets called in case a non-overridden visit method gets invoked.
         * @param it The argument to the respective visit method.
         */
        public void otherMethodCalled(NodeValue2Weight it) {
        }
    }
}
