package utwente.fmt.jtorx.lazyotf.guidance;

/**
 * A representation for LazyOTF test tree path weighers.
 */
public interface Path2Weight {
    /**
     * Gets the {@link Path2Weight}'s display name. This is an arbitrary string
     * presented to the user as the method's name.
     * 
     * @return The {@link Path2Weight}'s display name.
     */
    String getDisplayName();

    /**
     * Sets the {@link Path2Weight}'s display name. This may be an arbitrary string
     * intended to make the {@link Path2Weight} recognizable by the user.
     * 
     * @param name
     *            The new display name.
     */
    void setDisplayName(String name);

    /**
     * Acceptor for the visitor pattern.
     * 
     * @param visitor
     *            A {@link Path2WeightVisitor} visitor instance.
     */
    void accept(Path2WeightVisitor visitor);

    /**
     * Gets the {@link Path2Weight}'s internal name. This name describes a "basic"
     * {@link Path2Weight} provided by the underlying LazyOTF instance.
     * 
     * @return The {@link Path2Weight}'s internal name.
     */
    String getName();

    /**
     * Clones the {@link Path2Weight} instance.
     * 
     * @return The method's clone.
     */
    Path2Weight clone();
}
