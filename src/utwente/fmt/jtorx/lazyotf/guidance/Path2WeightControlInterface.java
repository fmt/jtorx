package utwente.fmt.jtorx.lazyotf.guidance;

import java.util.Collection;

/**
 * An interface for enabling and disabling {@link Path2Weight}s.
 */
public interface Path2WeightControlInterface {
    /** Modes of Path2Weight aggregation. */
    public enum AggregationMode {
        /** Aggregate by addition. */
        SUM,

        /** Aggregate by taking the maximum. */
        MAX;
    }

    /**
     * Sets the given {@link Path2Weight}'s enabledness.
     * 
     * @param it
     *            The {@link Path2Weight} which should be enabled/disabled.
     * @param enabledness
     *            <tt>true</tt> if the given {@link Path2Weight} should be
     *            enabled.
     */
    void setEnabled(Path2Weight it, boolean enabledness);

    /**
     * Determines whether the given {@link Path2Weight} is active.
     * 
     * @param it
     *            A {@link Path2Weight} instance.
     * @return {@code true} iff {@code it} is currently active.
     */
    boolean isEnabled(Path2Weight it);

    /**
     * Determines whether a given {@link Path2Weight} is installed.
     * 
     * @param it
     *            A {@link Path2Weight} instance.
     * @return {@code true} iff {@code it} is currently installed.
     */
    boolean isInstalled(Path2Weight it);

    /**
     * Aggregates the given {@link Path2Weight}s. This installs a new
     * {@link Path2Weight} aggregating over the arguments, which are removed.
     * 
     * @param path2Weights
     *            The {@link Path2Weight} objects which need to be aggregated.
     * @param aggMode
     *            The {@link AggregationMode}.
     */
    void aggregate(Collection<Path2Weight> path2Weights, AggregationMode aggMode);
}
