package utwente.fmt.jtorx.lazyotf.guidance;

/**
 * A visitor class for {@link Path2Weight} instances.
 */
public interface Path2WeightVisitor {
    /**
     * Visit the provided {@link Path2Weight} instance.
     * 
     * @param it
     *            The visited object.
     */
    void visit(Path2Weight it);

    /**
     * Visit the provided {@link SimplePath2Weight} instance.
     * 
     * @param it
     *            The visited object.
     */
    void visit(SimplePath2Weight it);

    /**
     * Visit the provided {@link CustomPath2Weight} instance.
     * 
     * @param it
     *            The visited object.
     */
    void visit(CustomPath2Weight it);

    /**
     * An "adapter" class implementing the enclosing interface. This class is
     * intended to be subclassed. In this implementation, the visit methods call
     * otherMethodCalled (unless you override them, of course).
     */
    public class Adapter implements Path2WeightVisitor {
        @Override
        public void visit(Path2Weight it) {
            otherMethodCalled(it);
        }

        @Override
        public void visit(CustomPath2Weight it) {
            otherMethodCalled(it);
        }

        @Override
        public void visit(SimplePath2Weight it) {
            otherMethodCalled(it);
        }

        /**
         * Gets called in case a non-overridden visit method gets invoked.
         * 
         * @param it
         *            The argument to the respective visit method.
         */
        public void otherMethodCalled(Path2Weight it) {
        }
    }

}
