package utwente.fmt.jtorx.lazyotf.guidance;

/**
 * A class representing a simple LazyOTF {@link Path2Weight}.
 */
public class SimplePath2Weight implements Path2Weight {
    /** The LazyOTF-internal name. */
    private final String name;

    /** The display name. */
    private String displayName;

    /**
     * Construct a new {@link SimplePath2Weight} instance.
     * 
     * @param name
     *            The LazyOTF-internal name.
     * @param displayName
     *            The display name.
     */
    public SimplePath2Weight(String name, String displayName) {
        this.name = name;
        this.displayName = displayName;
    }

    /**
     * Construct a new {@link SimplePath2Weight} instance.
     * 
     * @param name
     *            The LazyOTF-internal name.
     * @param displayName
     *            The display name.
     * @return The newly constructed SimplePath2Weight instance.
     */
    public static SimplePath2Weight newInstance(String name, String displayName) {
        return new SimplePath2Weight(name, displayName);
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public void setDisplayName(String name) {
        displayName = name;
    }

    @Override
    public void accept(Path2WeightVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public SimplePath2Weight clone() {
        return new SimplePath2Weight(name, displayName);
    }
}
