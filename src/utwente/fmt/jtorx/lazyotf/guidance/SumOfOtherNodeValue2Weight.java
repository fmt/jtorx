package utwente.fmt.jtorx.lazyotf.guidance;

import java.util.Collection;

/**
 * A class representing LazyOTF's SumOfOtherNodeValue2Weight node weigher,
 * assigning the aggregate weight of the node weighers known to this one.
 * 
 */
@Deprecated
abstract class SumOfOtherNodeValue2Weight implements NodeValue2Weight {
    /**
     * Add a summand to the weigher.
     * 
     * @param summand
     *            The new summand.
     */
    abstract void addSummand(NodeValue2Weight summand);

    /**
     * Remove all weighers from this instance.
     */
    abstract void clear();

    /**
     * Drop the currently used set of summands and use the provided one instead.
     * 
     * @param summands
     *            The new set of summands.
     */
    abstract void setSummands(Collection<NodeValue2Weight> summands);

    /**
     * Get the set of summands of this weigher.
     * 
     * @return The set of summands this weigher uses.
     */
    abstract Collection<NodeValue2Weight> getSummands();

    /**
     * Make a new {@link SumOfOtherNodeValue2Weight} instance, having an empty
     * initial set of summands.
     * 
     * @return The newly constructed {@link SumOfOtherNodeValue2Weight}
     *         instance.
     */
    public static SumOfOtherNodeValue2Weight newInstance() {
        return null;
    }
}
