package utwente.fmt.jtorx.lazyotf.guidance;

/**
 * A class representing LazyOTF's SUperLocationSize2Weight weigher.
 */
public class SuperLocationSize2Weight implements NodeValue2Weight {
    /** The weigher's internal LazyOTF name. */
    private static final String NV2W_NAME = "SuperLocationSize2Weight";

    /** The instance's display name. */
    private String displayName;

    /** The weigher's coefficient. */
    private int coefficient;

    /**
     * <tt>true</tt> iff the weigher should use the default weighing coefficient.
     */
    private boolean isUsingSystemDefaultCoefficient;

    /**
     * Constructs a new {@link SuperLocationSize2Weight} instance using
     * LazyOTF's default weight coefficient for this type of weigher.
     */
    public SuperLocationSize2Weight() {
        coefficient = 0;
        isUsingSystemDefaultCoefficient = true;
        displayName = "";
    }

    /**
     * Constructs a new {@link SuperLocationSize2Weight} instance using the
     * given weight coefficient.
     * 
     * @param displayName
     *            The initial display name.
     * @param coefficient
     *            The used weight coefficient.
     */
    public SuperLocationSize2Weight(String displayName, Integer coefficient) {
        this.coefficient = coefficient;
        isUsingSystemDefaultCoefficient = false;
        this.displayName = displayName;
    }

    /**
     * Sets the weigher's weight coefficient.
     * 
     * @param coefficient
     *            The new weight coefficient.
     */
    public void setCoefficient(int coefficient) {
        this.coefficient = coefficient;
    }

    /**
     * @return The weigher's weight coefficient.
     */
    public int getCoefficient() {
        return coefficient;
    }

    /**
     * Sets the usage of the default weight coefficient.
     * 
     * @param use
     *            Iff <tt>true</tt>, the weigher uses LazyOTF's system default
     *            weight coefficient for this type of weigher.
     */
    public void setUsingSystemDefaultCoefficient(boolean use) {
        isUsingSystemDefaultCoefficient = use;
    }

    /**
     * @return <tt>true</tt> iff the weigher uses LazyOTF's system default
     *         weight coefficient for this type of weigher.
     */
    public boolean isUsingSystemDefaultCoefficient() {
        return isUsingSystemDefaultCoefficient;
    }

    @Override
    public String getName() {
        return NV2W_NAME;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public void setDisplayName(String name) {
        displayName = name;
    }

    @Override
    public void accept(NodeValue2WeightVisitor v) {
        v.visit(this);
    }

    /**
     * Makes a new {@link SuperLocationSize2Weight} instance using the given
     * weight coefficient.
     * 
     * @param displayName
     *            The initial display name.
     * @param coefficient
     *            The used weight coefficient.
     * @return The new {@link SuperLocationSize2Weight} instance.
     */
    public static SuperLocationSize2Weight newInstance(String displayName,
            int coefficient) {
        return new SuperLocationSize2Weight(displayName, coefficient);
    }

}
