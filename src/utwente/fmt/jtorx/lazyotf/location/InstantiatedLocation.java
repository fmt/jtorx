package utwente.fmt.jtorx.lazyotf.location;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import utwente.fmt.lts.Term;
import utwente.fmt.lts.Variable;

/**
 * <p>
 * A representation of instantiated locations, containing the valuation as well
 * as the state's TorX ID, the concrete test type and a name-to-variable
 * mapping.
 * </p>
 * 
 * <p>
 * Instantiated locations can be obtained from
 * {@link utwente.fmt.jtorx.lazyotf.TestTreeNode TestTreeNode}s.
 * </p>
 */
public class InstantiatedLocation {
    /** The location object of which this is an instantiation. */
    private final Location location;

    /**
     * The test type assigned to this instantiated location during the test tree
     * computation.
     */
    private final TestType testType;

    /** A valuation mapping STS location variables to their respective value. */
    private final Map<Variable, Term> valuation;

    /** The instantiated location's TorX ID. */
    private final String locID;

    /** A mapping from variable names to the respective variables. */
    private final Map<String, Variable> variablesByName;

    /**
     * Constructs a new {@link InstantiatedLocation}.
     * 
     * @param location
     *            The location object of which this is an instantiation.
     * @param testType
     *            The test type assigned to this instantiated location during
     *            the test tree computation.
     * @param valuation
     *            A valuation mapping STS location variables to their respective
     *            value.
     * @param locID
     *            The instantiated location's TorX ID.
     */
    public InstantiatedLocation(Location location, TestType testType,
            Map<Variable, Term> valuation, String locID) {
        this.location = location;
        this.testType = testType;
        this.valuation = valuation;
        this.locID = locID;
        variablesByName = new HashMap<>();

        for (Variable v : valuation.keySet()) {
            variablesByName.put(v.getName(), v);
        }
    }

    /**
     * @return The instantiated location's location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @return The test type assigned to the instantiated location during the
     *         LazyOTF test tree computation.
     */
    public TestType getTestType() {
        return testType;
    }

    /**
     * Gets a variable value from the instantiated location's valuation.
     * 
     * @param var
     *            The variable object.
     * @return The associated value or <tt>null</tt> if no value is associated
     *         with the given variable.
     */
    public Term getValue(Variable var) {
        return valuation.get(var);
    }

    /**
     * @return The set of location variables associated with this instantiated
     *         location.
     */
    public Set<Variable> getLocationVariables() {
        return valuation.keySet();
    }

    /**
     * Gets a variable by its name.
     * 
     * @param name
     *            A variable name.
     * @return The corresponding variable or <tt>null</tt> if no such variable
     *         exists.
     */
    public Variable getLocationVariable(String name) {
        return variablesByName.get(name);
    }

    /**
     * @return The instantiated location's TorX ID.
     */
    public String getID() {
        return locID;
    }

    @Override
    public int hashCode() {
        return locID.hashCode();
    }
}
