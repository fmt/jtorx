package utwente.fmt.jtorx.lazyotf.location;

/**
 * <p>
 * Representation of STS states for LazyOTF.
 * </p>
 * 
 * <p>
 * Note: The location test types are determined by the TestTypeAssigner and (if
 * applicable) the decision strategies.
 * </p>
 */
public class Location implements Comparable<Location>, NamedEntity {
    /** The location's name. */
    private final String name;

    /**
     * Make a new Location object.
     * 
     * @param name
     *            The location's name.
     */
    public Location(String name) {
        this.name = name;
    }

    /**
     * @return The location's name.
     */
    @Override
    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public int compareTo(Location arg0) {
        return arg0.getName().compareTo(getName());
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Location) {
            Location otherLocation = (Location) o;
            String otherLocName = otherLocation.getName();
            if (otherLocName == null) {
                return getName() == null;
            }
            else {
                return getName() != null && getName().equals(otherLocName);
            }
        }
        else {
            return false;
        }
    }
}
