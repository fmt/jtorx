package utwente.fmt.jtorx.lazyotf.location;

/**
 * A minimal implementation of JTorX variables. These variables are used within
 * {@link InstantiatedLocation} objects generated by
 * {@link utwente.fmt.jtorx.lazyotf.TestTreeNode TestTreeNode}.
 */
public class LocationVariable implements utwente.fmt.lts.Variable {
    /** The variable name. */
    private final String name;

    /** The variable type. */
    private final String type;

    /**
     * Constructs a new {@link LocationVariable} instance.
     * 
     * @param name
     *            The variable name.
     * @param type
     *            The variable type.
     */
    public LocationVariable(String name, String type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getOriginalName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return (name.hashCode() >> 2) + (type.hashCode() >> 2);
    }
}
