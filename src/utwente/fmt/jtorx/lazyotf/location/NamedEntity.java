package utwente.fmt.jtorx.lazyotf.location;

/**
 * An interface used for LazyOTF-related entities having a name.
 */
public interface NamedEntity {
    /**
     * @return The entity's name.
     */
    String getName();
}
