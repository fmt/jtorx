package utwente.fmt.jtorx.lazyotf.location;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A representation of LazyOTF test types.
 */
public class TestType implements Comparable<TestType> {
    /** The ordinary location test type. */
    public static final TestType ORDINARY = new TestType("ORDINARY", "Ordinary", 1);

    /** The inducing location test type. */
    public static final TestType INDUCING = new TestType("INDUCING", "Inducing", 2);

    /** The test goal location test type. */
    public static final TestType TEST_GOAL = new TestType("TEST_GOAL", "Test Goal", 3);

    /** The decided-by-strategy location test type. */
    public static final TestType DECIDED_BY_STRATEGY = new TestType("DECIDED_BY_STRATEGY",
            "Decided By Strategy", -1, true);

    /** The set of all predefined test types. */
    public static final Set<TestType> ALL_TEST_TYPES;

    /**
     * A Map mapping test type names to the respective predefined test type.
     */
    private static Map<String, TestType> testTypeByName;

    static {
        ALL_TEST_TYPES = new HashSet<>();
        ALL_TEST_TYPES.add(ORDINARY);
        ALL_TEST_TYPES.add(INDUCING);
        ALL_TEST_TYPES.add(TEST_GOAL);
        ALL_TEST_TYPES.add(DECIDED_BY_STRATEGY);
    }

    /** The type's internal name. */
    private final String name;

    /** The type's display name. */
    private final String displayName;

    /** The type's priority. */
    private final int priority;

    /** <tt>true</tt> iff this test type is decided by strategy. */
    private final boolean isDecidedByStrategy;

    /**
     * Make a new TestType object.
     * 
     * @param name
     *            The test type's internal name.
     * @param displayName
     *            The test type's display (readable) name.
     * @param priority
     *            The test type's priority.
     */
    public TestType(String name, String displayName, int priority) {
        this(name, displayName, priority, false);
    }

    /**
     * Make a new TestType object.
     * 
     * @param name
     *            The test type's name.
     * @param priority
     *            The test type's priority.
     * @param displayName
     *            The test type's display (readable) name.
     * @param isDecidedByStrategy
     *            <tt>true</tt> iff this test type is decided by strategy.
     */
    public TestType(String name, String displayName, int priority, boolean isDecidedByStrategy) {
        this.name = name;
        this.priority = priority;
        this.isDecidedByStrategy = isDecidedByStrategy;
        this.displayName = displayName;

        if (testTypeByName == null) {
            testTypeByName = new HashMap<>();
        }
        testTypeByName.put(name, this);
    }

    /**
     * @param name
     *            The test type's name.
     * @return The associated test type.
     */
    public static TestType getTestTypeByName(String name) {
        return testTypeByName.get(name);
    }

    /**
     * @return The test type's internal name.
     */
    public String getInternalName() {
        return name;
    }

    /**
     * @return The test type's display name.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @return <tt>true</tt> iff this test type is decided by strategy.
     */
    public boolean isDecidedByStrategy() {
        return isDecidedByStrategy;
    }

    @Override
    public boolean equals(Object o2) {
        if (o2 instanceof TestType) {
            return getInternalName().equals(((TestType) o2).getInternalName());
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "TestType " + getInternalName();
    }

    @Override
    public int hashCode() {
        return getInternalName().hashCode();
    }

    @Override
    public int compareTo(TestType t) {
        return (t.priority > priority) ? -1 : ((t.priority < priority) ? 1 : 0);
    }
}
