package utwente.fmt.jtorx.lazyotf.policy;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategyHandle;
import utwente.fmt.jtorx.lazyotf.strategy.MappingStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.Condition;
import utwente.fmt.jtorx.lazyotf.strategy.StrategyControlInterface;

/**
 * The location test type decision strategy manager, taking care of installing
 * decision strategies, particularly the {@link MappingStrategy} taking care of
 * constant type assignments which might be overridden by other test objectives
 * assigning DECIDED_BY_STRATEGY to the location in question.
 */
public class DecisionStrategyMgr {
    /**
     * The class logger.
     */
    private static final Logger LOGGER = Logger.getLogger(DecisionStrategyMgr.class.getName());
    /**
     * An interface containing various methods used for the installation of
     * decision strategies.
     */
    private StrategyControlInterface strategyManipulator;

    /**
     * The default, "null" strategy control interface used when none other is
     * specified.
     */
    private StrategyControlInterface defaultStrategyManipulator;

    /**
     * A mapping strategy containing the maximum constant test type assignments.
     */
    private MappingStrategy maxConstTypeMapStrat;

    /** The handle to the installed {@link # maxConstTypeMapStrat}. */
    private DecisionStrategyHandle maxConstTypeMapStratHnd;

    /**
     * Constructs a new decision strategy manager, initially using a default
     * strategy control interface having no effects.
     */
    public DecisionStrategyMgr() {
        defaultStrategyManipulator = new StrategyControlInterface() {

            @Override
            public DecisionStrategyHandle installDecisionStrategy(DecisionStrategy ds,
                    Set<Location> domain) {
                LOGGER.log(Level.WARNING, "Warning: Missing strategy control interface!");
                return null;
            }

            @Override
            public void setEnabled(DecisionStrategyHandle ds, boolean enabledness) {
                LOGGER.log(Level.WARNING, "Warning: Missing strategy control interface!");
            }

            @Override
            public void simpleMap(DecisionStrategyHandle ds, Location loc, TestType type) {
                LOGGER.log(Level.WARNING, "Warning: Missing strategy control interface!");
            }

            @Override
            public void constraintMap(DecisionStrategyHandle ds, Location loc, TestType type,
                    Condition cond) {
                LOGGER.log(Level.WARNING, "Warning: Missing strategy control interface!");
            }

            @Override
            public DecisionStrategyHandle installDecisionStrategy(MappingStrategy ds,
                    Set<Location> domain) {
                LOGGER.log(Level.WARNING, "Warning: Missing strategy control interface!");
                return null;
            }

            @Override
            public DecisionStrategyHandle installDecisionStrategy(SatStrategy ds,
                    Set<Location> domain) {
                LOGGER.log(Level.WARNING, "Warning: Missing strategy control interface!");
                return null;
            }
        };

        strategyManipulator = defaultStrategyManipulator;
    }

    /**
     * Sets the maximum constant (i.e. non decided-by-strategy) test type of the
     * given {@link Location}. If called before
     * <tt>setupMaxConstantTypeMap()</tt>, this method has no effect.
     * 
     * @param loc
     *            The {@link Location} whose maximum constant test type should
     *            be set.
     * @param type
     *            The given location's maximum constant test type.
     */
    protected void mapMaxConstantType(Location loc, TestType type) {
        if (maxConstTypeMapStrat != null) {
            strategyManipulator.simpleMap(maxConstTypeMapStratHnd, loc, type);
        }
    }

    /**
     * Installs the given {@link DecisionStrategy}.
     * 
     * @param it
     *            The {@link DecisionStrategy} which should be installed.
     * @param domain
     *            The decision strategy's domain, i.e. the set of locations for
     *            which it acts as a test type decision strategy.
     * @return A handle to the installed decision strategy.
     */
    public DecisionStrategyHandle installDecisionStrategy(DecisionStrategy it,
            Set<Location> domain) {
        DecisionStrategyHandle strategyHandle = it.install(strategyManipulator, domain);
        it.update(strategyManipulator, strategyHandle);
        strategyManipulator.setEnabled(strategyHandle, true);
        return strategyHandle;
    }

    /**
     * Activates rsp. deactivates the installed {@link DecisionStrategy}
     * referenced by the given {@link DecisionStrategyHandle}.
     * 
     * @param it
     *            The {@link DecisionStrategyHandle} referencing the installed
     *            {@link DecisionStrategy}.
     * @param enabledness
     *            <tt>true</tt> if the referenced strategy should be enabled.
     */
    public void setStrategyEnabled(DecisionStrategyHandle it, boolean enabledness) {
        strategyManipulator.setEnabled(it, enabledness);
    }

    /**
     * Sets up the maximum constant test type mapping strategy.
     * 
     * @param maxConstantTypes
     *            The initial map of locations to their respective maximum
     *            constant test types.
     */
    public void setupMaxConstantTypeMap(Map<Location, TestType> maxConstantTypes) {
        maxConstTypeMapStrat = new MappingStrategy();
        LOGGER.log(Level.FINE, "Installing constant type mapping strategy.");
        Set<Location> dbsLocations = new HashSet<Location>();
        for (Location l : maxConstantTypes.keySet()) {
            dbsLocations.add(l);
        }
        maxConstTypeMapStratHnd = strategyManipulator.installDecisionStrategy(maxConstTypeMapStrat,
                dbsLocations);
        for (Location l : dbsLocations) {
            maxConstTypeMapStrat.associate(l, maxConstantTypes.get(l));
        }
        maxConstTypeMapStrat.update(strategyManipulator, maxConstTypeMapStratHnd);
    }

    /**
     * Sets the {@link StrategyControlInterface}.
     * 
     * @param it
     *            The new {@link StrategyControlInterface}.
     */
    public void setStrategyControlInterface(StrategyControlInterface it) {
        if (it != null) {
            strategyManipulator = it;
        }
        else {
            strategyManipulator = defaultStrategyManipulator;
        }
    }
}
