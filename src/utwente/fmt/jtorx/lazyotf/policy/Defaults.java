package utwente.fmt.jtorx.lazyotf.policy;

import utwente.fmt.jtorx.lazyotf.guidance.Locations2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;

/**
 * A class providing default objects for LazyOTF.
 */
public final class Defaults {
    /** The default node value weigher. */
    private static final NodeValue2Weight DEFAULT_NODEVALUE2WEIGHT;

    /** The default test goal weight. */
    private static final int DEFAULT_TESTGOAL_WEIGHT = 100 * 1000 * 1000;

    /** The default inducing state weight. */
    private static final int DEFAULT_INDUCING_WEIGHT = 1;

    /** The default ordinary state weight. */
    private static final int DEFAULT_ORDINARY_WEIGHT = 10;

    static {
        DEFAULT_NODEVALUE2WEIGHT = new Locations2Weight(
                "TestTypeDefaultNodeValue2Weight", DEFAULT_INDUCING_WEIGHT,
                DEFAULT_ORDINARY_WEIGHT, DEFAULT_TESTGOAL_WEIGHT);
    };

    /**
     * @return The default node value weigher.
     */
    public static NodeValue2Weight defaultNodeValue2Weight() {
        return DEFAULT_NODEVALUE2WEIGHT;
    }

    /** Utility class -- cannot instantiate. */
    private Defaults() {
    }
}
