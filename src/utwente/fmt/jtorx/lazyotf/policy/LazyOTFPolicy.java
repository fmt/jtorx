package utwente.fmt.jtorx.lazyotf.policy;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTF.InactiveException;
import utwente.fmt.jtorx.lazyotf.LazyOTF.NotReadyException;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.LocationMgr;
import utwente.fmt.jtorx.lazyotf.TestTreeNode;
import utwente.fmt.jtorx.lazyotf.TestTypeAssigner;
import utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr;
import utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr.TestTypeGroupDischargedEvent;
import utwente.fmt.jtorx.lazyotf.TraversalDepthComputer;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverFinalizationEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverInitializedEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverSUTInteractionEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.ExtendedInteractionResult;
import utwente.fmt.jtorx.lazyotf.location.InstantiatedLocation;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategyHandle;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverMethod;
import utwente.fmt.jtorx.torx.DriverSimInitializationResult;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.State;

/**
 * <p>
 * {@link LazyOTFPolicy} is intended to observe the
 * {@link utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver
 * LazyOTFAwareSimOnlineTestingDriver}, updating the LazyOTF policy systems
 * accordingly (e.g. the {@link TestTypeAssigner} and
 * {@link TraversalDepthComputer} objects associated with the LazyOTF instance)
 * and implementing simple policy tasks (e.g. issuing the test tree
 * recomputation).
 * </p>
 */
public class LazyOTFPolicy implements Observer {
	/** The class logger. */
	private static final Logger LOGGER = Logger.getLogger(LazyOTFPolicy.class.getName());

	/** The LazyOTF instance manipulated by this policy. */
	private final LazyOTF lazyOTFInst;

	/** The LazyOTF instance's configuration. */
	private final LazyOTFConfig cfg;

	/**
	 * The traversal depth computer used to recompute the test tree traversal
	 * depths.
	 */
	private final TraversalDepthComputer traversalDepthComputer;

	/**
	 * The location manager taking care of creating the policy sub-managers and
	 * keeping track of locations.
	 */
	private final LocationMgr locationMgr;

	/**
	 * The test type assigner, which assigns concrete location test types
	 * (including DECIDED_BY_STRATEGY) to locations.
	 */
	private final TestTypeAssigner testTypeAssigner;

	/**
	 * The test type group manager, responsible for keeping track of and
	 * discharging test type groups.
	 */
	private final TestTypeGroupMgr testTypeGroupMgr;

	/**
	 * If LazyOTF fails to recompute the test tree during automatic test tree
	 * computation, this predicate is set to <tt>true</tt> blocking further
	 * automatic test tree computations.
	 */
	private boolean isAutorecomputeBlocked = false;


	private boolean hasFullTestTree = true;

	/**
	 * Constructs a {@link LazyOTFPolicy} instance.
	 * 
	 * @param lazyOTFInst
	 *            The {@link LazyOTF} instance which should be manipulated.
	 * @param cfg
	 *            The configuration of {@code lazyOTFInst}.
	 */
	public LazyOTFPolicy(LazyOTF lazyOTFInst, LazyOTFConfig cfg) {
		this.lazyOTFInst = lazyOTFInst;
		traversalDepthComputer = lazyOTFInst.getTraversalDepthComputer();
		locationMgr = lazyOTFInst.getLocationManager();
		testTypeAssigner = locationMgr.getTestTypeAssigner();
		testTypeGroupMgr = locationMgr.getTestTypeGroupMgr();
		this.cfg = cfg;

		testTypeGroupMgr.addObserver(new Observer() {
			@ObserverMethod
			public void onTestTypeGroupDischarged(TestTypeGroupDischargedEvent event) {
				if (LazyOTFPolicy.this.cfg.isCycleDetectionEnabled()) {
					LazyOTFPolicy.this.lazyOTFInst.onDischargedTestObjective();
				}
			}
		}, TestTypeGroupMgr.EventCategory.TEST_TYPE_GROUP_DISCHARGED);
	}

	/**
	 * An observer method for SUT interactions.
	 * 
	 * @param eir
	 *            The SUT interaction represented by a
	 *            {@link DriverSUTInteractionEvent}.
	 */
	@ObserverMethod
	public void onSUTInteraction(DriverSUTInteractionEvent eir) {
		onDriverInteraction(eir.interactionResult);
	}

	/**
	 * An observer method for driver finalization events.
	 * @param dfe
	 *           The finalization result represented by a
	 *           {@link DriverFinalizationEvent}.
	 */
	@ObserverMethod
	public void onFinish(DriverFinalizationEvent dfe) {
		lazyOTFInst.cleanUp();
	}

	/**
	 * An observer method for driver initialization events.
	 * 
	 * @param dsir
	 *            The initialization result represented by a
	 *            {@link DriverInitializedEvent}.
	 */
	@ObserverMethod
	public void onInit(DriverInitializedEvent dsir) {
		onInitializedDriver(dsir.initializationResult);
	}

	/**
	 * Initializes the LazyOTF instance and issue the first test tree
	 * computation.
	 * 
	 * @param result
	 *            The JTorX driver's initialization result.
	 */
	private void onInitializedDriver(DriverSimInitializationResult result) {
		try {
			lazyOTFInst.init();
			TestTypeGroupMgr ttgMgr = lazyOTFInst.getLocationManager().getTestTypeGroupMgr();
			ttgMgr.getDistributeLazyOTF().pullExternalDischarges();
			LOGGER.log(Level.FINE, "Computing intial test tree...");
			lazyOTFInst.computeNewTestTree(getConcatenatedTorXStateID(result.getState()));
		}
		catch (InactiveException e) {
			LOGGER.severe("LazyOTF failed to initialize: " + e);
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Reacts to an interaction with the SUT. The reached test tree node is
	 * analyzed and, if applicable, a new test tree computation is issued.
	 * 
	 * @param result
	 *            The driver's interaction result.
	 */
	private void onDriverInteraction(ExtendedInteractionResult result) {
		LOGGER.info("Handling a driver interaction.");
		TestTreeNode currentNode;
		if (lazyOTFInst.isActive()) {
			try {
				currentNode = lazyOTFInst.getTestTreeNode(-1);
				receiveTestTreeNode(currentNode);
			}
			catch (InactiveException e) {
				throw new IllegalStateException(
						"Implementation error: Could not get the current test tree node from an active LazyOTF instance.");
			}
		}

		if (result.isTestGoalHit() && testTypeGroupMgr.getActiveTestTypeGroupsWithTestGoal().isEmpty()) {
			// Last TO discharged: Disable autorecomputing
			LOGGER.info("All test objectives have been discharged. Disabling automatic test tree recomputation.");
			lazyOTFInst.setInactive();
			isAutorecomputeBlocked = true;
		}

		if (cfg.doAutoRecompute() && !isAutorecomputeBlocked) {
			LOGGER.info("Checking whether to issue a test tree recomputation.");
			recomputeTestTreeIfNeeded(result.getTesterState(), result.getInterpretation());
		}
		else {
			LOGGER.info("Not checking whether to issue a test tree recomputation.");
		}
	}

	/**
	 * Processes a newly reached test tree node by updating the traversal depth
	 * computer and discharging any reached test objectives.
	 * 
	 * @param ttn
	 *            The newly reached test tree node.
	 */
	private void receiveTestTreeNode(TestTreeNode ttn) {
		dischargeTestObjectives(ttn);
	}

	/**
	 * Converts a JTorX tester state to a set of String-represented TorX state
	 * IDs digestible by LazyOTF.
	 * 
	 * @param testerState
	 *            The TorX state ID to be converted.
	 * @return The current TorX state, represented as a set of Strings.
	 */
	private Set<String> getConcatenatedTorXStateID(CompoundState testerState) {
		Set<String> result = new HashSet<String>();
		Iterator<? extends State> itx = testerState.getSubStates();
		while (itx.hasNext()) {
			result.add(itx.next().getID());
		}

		return result;
	}

	/**
	 * Unless automatic test tree recomputation is disabled due to errors
	 * encountered by LazyOTF, a new test tree is computed. If the computation
	 * fails, further automatic test tree computations are blocked.
	 * 
	 * Before issuing the recomputation, distributed discharges are registered
	 * with the local policy.
	 * 
	 * If the current tester state menu only has outputs, a depth-1 test tree
	 * is computed instead since LazyOTF cannot influence tester behaviour
	 * (but still needs to register discharged test goals).
	 * 
	 * @param currentTesterState
	 *            The driver's current tester state.
	 */
	private void recomputeTestTreeIfNeeded(CompoundState currentTesterState, IOLTSInterpretation interp) {
		//System.err.println("recomputeTestTree begin");
		if (isAutorecomputeBlocked) {
			LOGGER.warning("recomputeTestTreeIfNeeded called while auto-recomputing is blocked");
			return;
		}

		TestTreeNode currentTTN;
		try {
			currentTTN = lazyOTFInst.getTestTreeNode(-1);
		}
		catch (InactiveException e1) {
			isAutorecomputeBlocked = true;
			return;
		}

		boolean doRecompute = false;
		try {
			// Querying LazyOTF early does not hurt due to caching
			lazyOTFInst.propose();
		}
		catch (InactiveException e) {
			// This situation is handled by the driver, ignore for now
		}
		catch (NotReadyException e) {
			doRecompute = true;
		}

		if (doRecompute) {
			if (hasFullTestTree) {
				updateTraversalDepthComputer(currentTTN);
			}

			TestTypeGroupMgr ttgMgr = lazyOTFInst.getLocationManager().getTestTypeGroupMgr();
			ttgMgr.getDistributeLazyOTF().pullExternalDischarges();

			boolean isSuppressingFullComputation = false;
			int originalBound = lazyOTFInst.getTraversalDepthComputer().getTraversalDepth();
			if (isOutputOnlySituation(currentTesterState, interp)) {
				LOGGER.fine("Computing a new test tree in an output-only situation");
				isSuppressingFullComputation = true;
				try {
					lazyOTFInst.setMaxRecursionDepth(1);
					hasFullTestTree = false;
				}
				catch (InactiveException e) {
					LOGGER.log(Level.SEVERE, "Unable to decrease the LazyOTF recursion depth bound. Reason: \n" + e);
				}
			}
			else {
				LOGGER.fine("Computing a new test tree with depth = " + originalBound);
				hasFullTestTree = true;
			}

			try {
				lazyOTFInst.computeNewTestTree(getConcatenatedTorXStateID(currentTesterState));
				if (isSuppressingFullComputation) {
					lazyOTFInst.setMaxRecursionDepth(originalBound);
				}
			}
			catch (InactiveException e) {
				// Double-fault - do not compute any more test trees.
				LOGGER.log(Level.SEVERE, "Double-fault while trying to compute a new test tree, disabling autorecomputing");
				isAutorecomputeBlocked = true;
			}
		}
		//System.err.println("recomputeTestTree end");
	}

	private boolean isOutputOnlySituation(CompoundState currentTesterState, IOLTSInterpretation interp) {
		Iterator<? extends CompoundTransition<? extends CompoundState>> iter;
		iter = currentTesterState.menu(interp.isInput());
		assert iter != null;
		return !iter.hasNext();
	}

	/**
	 * Discharges the test objectives reached at the given test tree node.
	 * 
	 * @param ttn
	 *            The current test tree node.
	 */
	private void dischargeTestObjectives(TestTreeNode ttn) {
		if (!ttn.getTestGoals().isEmpty()) {        	
			for (InstantiatedLocation l : ttn.getLocations()) {
				Map<DecisionStrategyHandle, TestType> stratMap = ttn
						.getNonOrdinaryStrategyResults(l);
				for (DecisionStrategyHandle key : stratMap.keySet()) {
					if (stratMap.get(key).equals(TestType.TEST_GOAL)) {
						testTypeGroupMgr.hitTestGoal(key, null);
					}
				}
			}

			for (InstantiatedLocation l : ttn.getTestGoals()) {
				Location abstractLoc = l.getLocation();
				if (testTypeAssigner.getMaxConstantTestType(abstractLoc).equals(TestType.TEST_GOAL)) {
					testTypeGroupMgr.hitTestGoal(abstractLoc, null);
				}
			}
		}
	}

	/**
	 * Updates the traversal depth computer according to the given
	 * {@link TestTreeNode}.
	 * 
	 * @param ttn
	 *            The current {@link TestTreeNode}.
	 */
	private void updateTraversalDepthComputer(TestTreeNode ttn) {
		if (!ttn.getTestGoals().isEmpty()) {
			traversalDepthComputer.hit(TestType.TEST_GOAL);
		}
		else if (!ttn.getInducingStates().isEmpty()) {
			traversalDepthComputer.hit(TestType.INDUCING);
		}
		else {
			traversalDepthComputer.hit(TestType.ORDINARY);
		}
	}
}
