package utwente.fmt.jtorx.lazyotf.policy;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.LazyOTFPolicyFactory;
import utwente.fmt.jtorx.lazyotf.LocationMgr;
import utwente.fmt.jtorx.lazyotf.TraversalDepthComputer;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;

/**
 * An implementation of {@link LazyOTFPolicyFactory} making instances of the
 * policy subsystem contained in this package.
 */
public class LazyOTFPolicyFactoryImpl implements LazyOTFPolicyFactory {
    /** The LazyOTF configuration used by the policy. */
    private final LazyOTFConfig cfg;

    /** The instance of the {@link LazyOTF} system. */
    private LazyOTF inst;

    /**
     * Constructs a new {@link LazyOTFPolicyFactoryImpl} instance.
     * 
     * @param lazyOTFinst
     *            The "target" {@link LazyOTF} instance.
     * @param cfg
     *            The configuration of {@code lazyOTFinst}.
     */
    public LazyOTFPolicyFactoryImpl(LazyOTF lazyOTFinst, LazyOTFConfig cfg) {
        this.cfg = cfg;
        inst = lazyOTFinst;
    }

    /**
     * Constructs a new {@link LazyOTFPolicyFactoryImpl} instance. Before
     * calling {@link # newLazyOTFPolicyObserver()}, you must supply the
     * targetted {@link LazyOTF} instance via
     * {@link # setLazyOTFInstance(LazyOTF)}.
     * 
     * @param cfg
     *            The target {@link LazyOTF} instance's configuration.
     */
    public LazyOTFPolicyFactoryImpl(LazyOTFConfig cfg) {
        this.cfg = cfg;
    }

    /**
     * Makes a new {@link LazyOTFPolicy} observer for the
     * {@link utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver
     * LazyOTFAwareSimOnlineTestingDriver}.
     * 
     * @return A new {@link LazyOTFPolicy} object.
     */
    @Override
    public Observer newLazyOTFPolicyObserver() {
        if (inst == null || cfg == null) {
            throw new IllegalStateException(
                    "Must have configured both instance and config.");
        }
        LazyOTFPolicy result = new LazyOTFPolicy(inst, cfg);
        return result;
    }

    /**
     * Makes a new {@link SimpleTraversalDepthComputer} object.
     * 
     * @return A new {@link SimpleTraversalDepthComputer} object.
     */
    @Override
    public TraversalDepthComputer newTraversalDepthComputer() {
        return new SawtoothTraversalDepthComputer(cfg.getInitialRecursionDepth(),
                cfg.getMaxRecursionDepth(), cfg.getRecursionDepthModThreshold());
    }

    /**
     * Makes a new, unconfigured {@link LocationMgrImpl} object.
     * 
     * @return A new {@link LocationMgrImpl} object.
     */
    @Override
    public LocationMgr newLocationManager() {
        return new LocationMgrImpl();
    }

    @Override
    public void setLazyOTFInstance(LazyOTF inst) {
        this.inst = inst;
    }
}
