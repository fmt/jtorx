package utwente.fmt.jtorx.lazyotf.policy;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.LocationMgr;
import utwente.fmt.jtorx.lazyotf.TestTypeAssigner;
import utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightControlInterface;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.StrategyControlInterface;

/**
 * This policy package's implementation of {@link LocationMgr}.
 */
public class LocationMgrImpl implements LocationMgr {
    /** The logger. */
    private static final Logger LOGGER = Logger.getLogger(LocationMgr.class.getName());

    /** The locations managed by this location manager. */
    private final Set<Location> locations = new HashSet<>();

    /** A map from location names to locations. */
    private final Map<String, Location> locationsByName = new HashMap<>();

    /**
     * The default, "null" {@link Path2Weight} control interface used when none
     * other is specified.
     */
    private Path2WeightControlInterface defaultPath2WeightManipulator;

    /** The {@link TestTypeAssigner} used by this location manager. */
    private final TestTypeAssigner testTypeAssigner;

    /** The {@link TestTypeGroupMgr} used by this location manager. */
    private final TestTypeGroupMgr testTypeGroupMgr;

    /** The {@link DecisionStrategyMgr} used by this location manager. */
    private final DecisionStrategyMgr decisionStrategyMgr;

    /**
     * Makes a new LocationMgr instance.
     */
    public LocationMgrImpl() {
        defaultPath2WeightManipulator = new Path2WeightControlInterface() {
            @Override
            public void setEnabled(Path2Weight it, boolean enabledness) {
                LOGGER.log(Level.WARNING, "Warning: Missing {@link Path2Weight} control interface!");
            }

            @Override
            public boolean isEnabled(Path2Weight it) {
                LOGGER.log(Level.WARNING, "Warning: Missing {@link Path2Weight} control interface!");
                return false;
            }

            @Override
            public boolean isInstalled(Path2Weight it) {
                LOGGER.log(Level.WARNING, "Warning: Missing {@link Path2Weight} control interface!");
                return false;
            }
            
            @Override
            public void aggregate(Collection<Path2Weight> p2ws, AggregationMode aggMode) {
                LOGGER.log(Level.WARNING, "Warning: Missing {@link Path2Weight} control interface!");
            }
        };

        decisionStrategyMgr = new DecisionStrategyMgr();
        TestTypeGroupMgrImpl ttgMgrImpl = new TestTypeGroupMgrImpl(decisionStrategyMgr,
                defaultPath2WeightManipulator);
        testTypeGroupMgr = ttgMgrImpl;
        testTypeAssigner = new TestTypeAssignerImpl(decisionStrategyMgr, ttgMgrImpl);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * utwente.fmt.jtorx.lazyotf.policy.ILocationMgr#addLocation(utwente.fmt
     * .jtorx.lazyotf.location.Location)
     */
    @Override
    public void addLocation(Location l) {
        LOGGER.log(Level.FINE, "Adding location '" + l.getName() + "'");
        locations.add(l);
        locationsByName.put(l.getName(), l);
    }

    /*
     * (non-Javadoc)
     * 
     * @see utwente.fmt.jtorx.lazyotf.policy.ILocationMgr#getLocations()
     */
    @Override
    public Set<Location> getLocations() {
        return locations;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * utwente.fmt.jtorx.lazyotf.policy.ILocationMgr#getLocation(java.lang.String
     * )
     */
    @Override
    public Location getLocation(String name) {
        return locationsByName.get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * utwente.fmt.jtorx.lazyotf.policy.ILocationMgr#setStrategyControlInterface
     * (utwente.fmt.jtorx.lazyotf.strategy.StrategyControlInterface)
     */
    @Override
    public void setStrategyControlInterface(StrategyControlInterface it) {
        decisionStrategyMgr.setStrategyControlInterface(it);
    }

    /*
     * (non-Javadoc)
     * 
     * @see utwente.fmt.jtorx.lazyotf.policy.ILocationMgr#
     * set{@link Path2Weight}ControlInterface
     * (utwente.fmt.jtorx.lazyotf.guidance.Path2WeightControlInterface)
     */
    @Override
    public void setPath2WeightControlInterface(Path2WeightControlInterface it) {
        if (it != null) {
            testTypeGroupMgr.setPath2WeightControlInterface(it);
        }
        else {
            testTypeGroupMgr
                    .setPath2WeightControlInterface(defaultPath2WeightManipulator);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see utwente.fmt.jtorx.lazyotf.policy.ILocationMgr#init()
     */
    @Override
    public void init() {
        Map<Location, TestType> maxConstantTypes = new HashMap<>();
        for (Location l : getLocations()) {
            if (testTypeAssigner.isDecidedByStrategy(l)) {
                maxConstantTypes.put(l, testTypeAssigner.getMaxConstantTestType(l));
            }
        }
        decisionStrategyMgr.setupMaxConstantTypeMap(maxConstantTypes);
        testTypeGroupMgr.init();
    }

    /*
     * @see utwente.fmt.jtorx.lazyotf.LocationMgr#getTestTypeAssigner()
     */
    @Override
    public TestTypeAssigner getTestTypeAssigner() {
        return testTypeAssigner;
    }

    /*
     * (non-Javadoc)
     * 
     * @see utwente.fmt.jtorx.lazyotf.LocationMgr#getTestTypeGroupMgr()
     */
    @Override
    public TestTypeGroupMgr getTestTypeGroupMgr() {
        return testTypeGroupMgr;
    }
    
    @Override
    public void cleanUp() {
        testTypeGroupMgr.cleanUp();
    }
}
