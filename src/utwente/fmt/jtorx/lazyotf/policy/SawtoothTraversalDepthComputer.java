package utwente.fmt.jtorx.lazyotf.policy;

import java.util.Observable;

import utwente.fmt.jtorx.lazyotf.TraversalDepthComputer;
import utwente.fmt.jtorx.lazyotf.location.TestType;

public class SawtoothTraversalDepthComputer extends Observable implements TraversalDepthComputer {
    /**
     * Counts the amount of test trees traversed during test execution time
     * since last hitting a test goal.
     */
    private int noTGCounter = 0;

    /**
     * The threshold for modifying the test-tree computation time traversal
     * depth.
     */
    private final int threshold;

    /** The current test-tree computation time traversal depth. */
    private int currentTraversalDepth;

    /** The minimal test tree traversal depth. */
    private final int minTraversalDepth;

    /** The maximal test tree traversal depth. */
    private final int maxTraversalDepth;

    private final int INCREMENT_SIZE = 3;
    
    /**
     * Constructs a new {@link SimpleTraversalDepthComputer} instance.
     * 
     * @param minTraversalDepth
     *            The minimum (and initial) traversal depth.
     * @param maxTraversalDepth
     *            The maximum traversal depth.
     * @param threshold
     *            The threshold for depth increases/decreases, see
     *            {@link SimpleTraversalDepthComputer}.
     */
    public SawtoothTraversalDepthComputer(int minTraversalDepth, int maxTraversalDepth, int threshold) {
        currentTraversalDepth = minTraversalDepth;
        this.minTraversalDepth = minTraversalDepth;
        this.maxTraversalDepth = maxTraversalDepth;
        this.threshold = threshold;
    }

    @Override
    public void hit(TestType type) {
        if (type != TestType.TEST_GOAL) {
            noTGCounter++;
            if (noTGCounter >= threshold && currentTraversalDepth < maxTraversalDepth) {
                noTGCounter = 0;
                currentTraversalDepth = Math.min(currentTraversalDepth + INCREMENT_SIZE, maxTraversalDepth);
                System.err.println("Increasing bound to " + currentTraversalDepth);
                setChanged();
                notifyObservers(currentTraversalDepth);
            }
        }
        else {
            noTGCounter = 0;
            currentTraversalDepth = minTraversalDepth;
            setChanged();
            notifyObservers(currentTraversalDepth);
        }
    }

    @Override
    public int getTraversalDepth() {
        return currentTraversalDepth;
    }
}

