package utwente.fmt.jtorx.lazyotf.policy;

import java.util.Observable;

import utwente.fmt.jtorx.lazyotf.TraversalDepthComputer;
import utwente.fmt.jtorx.lazyotf.location.TestType;

/**
 * A simple model traversal depth computation class, used by LazyOTF. Initially,
 * the computed (=: current) depth equals the minimum traversal depth given in
 * the constructor parameters. The current depth gets increased by one after
 * hit() was called a (specified, constant) number of times in a row with a test
 * type apart from TEST_GOAL; Conversely, the current depth is decreased by one
 * after hit() was called a (specified, constant) number of times in a row with
 * TEST_GOAL as its parameter. However, the current depth is kept within the
 * given min/max bounds.
 */
public class SimpleTraversalDepthComputer extends Observable implements TraversalDepthComputer {
    /**
     * Counts the amount of test trees traversed during test execution time
     * since last hitting a test goal.
     */
    private int noTGCounter = 0;

    /**
     * Counts the amount of test trees traversed during test execution time
     * since the last test tree traversal where no test goal was hit.
     */
    private int tGCounter = 0;

    /**
     * The threshold for modifying the test-tree computation time traversal
     * depth.
     */
    private final int threshold;

    /** The current test-tree computation time traversal depth. */
    private int currentTraversalDepth;

    /** The minimal test tree traversal depth. */
    private final int minTraversalDepth;

    /** The maximal test tree traversal depth. */
    private final int maxTraversalDepth;

    /**
     * Constructs a new {@link SimpleTraversalDepthComputer} instance.
     * 
     * @param minTraversalDepth
     *            The minimum (and initial) traversal depth.
     * @param maxTraversalDepth
     *            The maximum traversal depth.
     * @param threshold
     *            The threshold for depth increases/decreases, see
     *            {@link SimpleTraversalDepthComputer}.
     */
    public SimpleTraversalDepthComputer(int minTraversalDepth, int maxTraversalDepth, int threshold) {
        currentTraversalDepth = minTraversalDepth;
        this.minTraversalDepth = minTraversalDepth;
        this.maxTraversalDepth = maxTraversalDepth;
        this.threshold = threshold;
    }

    @Override
    public void hit(TestType type) {
        if (type != TestType.TEST_GOAL) {
            noTGCounter++;
            tGCounter = 0;
            if (noTGCounter >= threshold && currentTraversalDepth < maxTraversalDepth) {
                noTGCounter = 0;
                currentTraversalDepth++;
                setChanged();
                notifyObservers(currentTraversalDepth);
            }
        }
        else {
            noTGCounter = 0;
            tGCounter++;
            if (currentTraversalDepth > minTraversalDepth && tGCounter >= threshold) {
                tGCounter = 0;
                currentTraversalDepth--;
                setChanged();
                notifyObservers(currentTraversalDepth);
            }
        }
    }

    @Override
    public int getTraversalDepth() {
        return currentTraversalDepth;
    }
}
