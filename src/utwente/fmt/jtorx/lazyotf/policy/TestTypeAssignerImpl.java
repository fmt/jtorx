package utwente.fmt.jtorx.lazyotf.policy;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.TestTypeAssigner;
import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr;
import utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr.EventCategory;
import utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr.LocationTestTypeInvalidatedEvent;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverMethod;

/**
 * This package's implementation of {@link TestTypeAssigner}.
 */
public class TestTypeAssignerImpl implements TestTypeAssigner {
    /** Test type computations are cached in this map. */
    private final Map<Location, TestType> cachedTestTypes;

    /**
     * The class logger.
     */
    private static final Logger LOGGER = Logger.getLogger(TestTypeAssignerImpl.class.getName());

    /**
     * If a location's test type may be overridden by a strategy (i.e. at least
     * one active test type group assigns <tt>DECIDED_BY_STRATEGY</tt>), the
     * location is associated with <tt>TRUE</tt> here.
     */
    private final Map<Location, Boolean> decisionStrategyOverride;

    /**
     * The LazyOTF subsystem's
     * {@link utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr TestTypeGroupMgr} used
     * to keep track of {@link TestTypeGroup}s.
     */
    private final TestTypeGroupMgr testTypeGroupMgr;

    /**
     * Constructs a new {@link TestTypeAssigner} instance.
     * 
     * @param decisionStrategyMgr
     *            The LazyOTF subsystem's {@link DecisionStrategyMgr}.
     * @param testTypeGroupMgr
     *            The LazyOTF subsystem's
     *            {@link utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr
     *            TestTypeGroupMgr}.
     */
    public TestTypeAssignerImpl(final DecisionStrategyMgr decisionStrategyMgr,
            final TestTypeGroupMgrImpl testTypeGroupMgr) {
        cachedTestTypes = new HashMap<>();
        this.testTypeGroupMgr = testTypeGroupMgr;
        decisionStrategyOverride = new HashMap<>();

        // Add test type invalidation listener to the test type group manager.
        testTypeGroupMgr.addObserver(new Observer() {
            @ObserverMethod
            public void onInvalidatedLocations(LocationTestTypeInvalidatedEvent event) {
                for (Location loc : event.invalidatedLocations) {
                    invalidateTestType(loc);
                    if (event.isInvalidatingMaxConstantMap && isDecidedByStrategy(loc)) {
                        if (isDecidedByStrategy(loc)) {
                            TestType maxConstType = getMaxConstantTestType(loc);
                            decisionStrategyMgr.mapMaxConstantType(loc, maxConstType);
                        }
                    }
                }
            }
        }, EventCategory.LOCATION_TEST_TYPE_INVALIDATED);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * utwente.fmt.jtorx.lazyotf.policy.ITestTypeAssigner#getMaxConstantTestType
     * (utwente.fmt.jtorx.lazyotf.location.Location)
     */
    @Override
    public TestType getMaxConstantTestType(Location loc) {
        if (!cachedTestTypes.containsKey(loc)) {
            recomputeTestType(loc);
        }
        return cachedTestTypes.get(loc);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * utwente.fmt.jtorx.lazyotf.policy.ITestTypeAssigner#isDecidedByStrategy
     * (utwente.fmt.jtorx.lazyotf.location.Location)
     */
    @Override
    public boolean isDecidedByStrategy(Location loc) {
        if (!decisionStrategyOverride.containsKey(loc)) {
            recomputeTestType(loc);
        }
        return decisionStrategyOverride.get(loc);
    }

    /**
     * Invalidate the current test type association made by this test type
     * assigner for a given location. This method needs to be called whenever
     * the set of active test type groups involving <i>loc</i> changes.
     * 
     * @param loc
     *            The {@link Location} whose test type needs to be recomputed.
     */
    protected void invalidateTestType(Location loc) {
        cachedTestTypes.remove(loc);
        decisionStrategyOverride.remove(loc);
    }

    /**
     * Recompute the test type of a given location.
     * 
     * @param loc
     *            The {@link Location} whose test type needs to be recomputed.
     */
    private void recomputeTestType(Location loc) {
        TestType currentType = null;

        Map<TestType, Set<TestTypeGroup>> ttgMapForLoc;
        ttgMapForLoc = testTypeGroupMgr.getTestTypeGroupsByTestType(loc);

        TestType[] orderedTestTypes = { TestType.TEST_GOAL, TestType.INDUCING };

        for (TestType tt : orderedTestTypes) {
            if (!ttgMapForLoc.get(tt).isEmpty()) {
                currentType = tt;
                break;
            }
        }
        decisionStrategyOverride
                .put(loc, !ttgMapForLoc.get(TestType.DECIDED_BY_STRATEGY).isEmpty());

        if (currentType == null) {
            LOGGER.log(Level.FINE, "No test type assigned for location " + loc.getName()
                    + ". => The test type group configuration is not well-defined."
                    + "This is non-fatal, assuming ORDINARY.");
            currentType = TestType.ORDINARY;
        }
        cachedTestTypes.put(loc, currentType);
    }
}
