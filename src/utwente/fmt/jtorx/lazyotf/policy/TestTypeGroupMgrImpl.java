package utwente.fmt.jtorx.lazyotf.policy;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr;
import utwente.fmt.jtorx.lazyotf.distributed.DistributeLazyOTF;
import utwente.fmt.jtorx.lazyotf.distributed.DistributeLazyOTF.ExternalDischargeEvent;
import utwente.fmt.jtorx.lazyotf.distributed.NullDistributeLazyOTFFactory;
import utwente.fmt.jtorx.lazyotf.distributed.NullDistributeLazyOTFFactory.NullDistributeLazyOTF;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightControlInterface;
import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightControlInterface.AggregationMode;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategyHandle;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverCategorizingObservable;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverMethod;

/**
 * This package's implementation of {@link TestTypeGroupMgr}.
 */
public class TestTypeGroupMgrImpl implements TestTypeGroupMgr {
	/** The class logger. */
	public static final Logger LOGGER = Logger.getLogger(TestTypeGroupMgr.class.getName());

	/** If set to {@code true}, {@link Path2Weight}s are aggregated within
	 * test objectives by summarizing.
	 */
	private static boolean sumPath2WeightWithinTestObjectives = false;

	/** The test type groups associated with this test type group manager. */
	private final Set<TestTypeGroup> typeGroups = new HashSet<>();

	/**
	 * A map associating each location with the test type groups in which it is
	 * it is present. The test type groups are divided up by the test type they
	 * assign for the corresponding location, i.e.
	 * <tt>testTypeGroupsByLocation.get(l).get(t)</tt> is the set of all test
	 * type groups assigning the type <i>t</i> to location <i>l</i>.
	 */
	private final Map<Location, Map<TestType, Set<TestTypeGroup>>> testTypeGroupsByLocation;

	/**
	 * The default test-type->test-type-group map for locations not present in
	 * the key set of <i>testTypeGroupsByLocation</i>. This map is not
	 * modifiable.
	 */
	private final Map<TestType, Set<TestTypeGroup>> defaultTestTypeTTGMap;

	/**
	 * The {@link DecisionStrategyMgr} used to modify decision strategies upon
	 * test type group discharges.
	 */
	private final DecisionStrategyMgr decisionStrategyMgr;

	/**
	 * The {@link DistributeLazyOTF} instance used for distribution. By default,
	 * this is a {@link NullDistributeLazyOTF} causing no distribution at all.
	 */
	private DistributeLazyOTF distributeLazyOTF; 

	private int countDischargedFromExtern = 0;

	/**
	 * An interface containing methods for the activation/deactivation of
	 * {@link Path2Weight}s.
	 */
	private Path2WeightControlInterface path2WeightManipulator;

	/**
	 * The set of test type groups which are active and have a
	 * <tt>TEST_GOAL</tt> or <tt>DECIDED_BY_STRATEGY</tt> assignment.
	 */
	private final Set<TestTypeGroup> activeTestTypeGroupsWithTestGoal;

	/** {@link Path2Weight} reference counts. */
	private final Map<Path2Weight, Integer> path2WeightRefCounts;

	/**
	 * A map associating decision strategy handles with their corresponding test
	 * type groups.
	 */
	private final Map<DecisionStrategyHandle, TestTypeGroup> ttgByHandle;

	/**
	 * A map associating test type group names with the corresponding test type
	 * groups.
	 */
	private final Map<String, TestTypeGroup> ttgByName;

	/**
	 * The event manager for this class. Responsible for issuing updates to the
	 * observers.
	 */
	private final ObserverCategorizingObservable<EventCategory> eventManager;

	/**
	 * {@code true} iff init() has been completed.
	 */
	private boolean isInitialised;

	/**
	 * Constructs a new {@link TestTypeGroupMgr} instance.
	 * 
	 * @param decisionStrategyMgr
	 *            The {@link DecisionStrategyMgr} used by the LazyOTF subsystem.
	 * @param gci
	 *            The {@link Path2WeightControlInterface} used to
	 *            manipulate {@link Path2Weight}s.
	 */
	public TestTypeGroupMgrImpl(DecisionStrategyMgr decisionStrategyMgr,
			Path2WeightControlInterface gci) {
		isInitialised = false;
		ttgByHandle = new HashMap<>();
		ttgByName = new HashMap<>();
		testTypeGroupsByLocation = new HashMap<>();
		activeTestTypeGroupsWithTestGoal = new HashSet<>();
		this.decisionStrategyMgr = decisionStrategyMgr;
		path2WeightManipulator = gci;
		eventManager = new ObserverCategorizingObservable<EventCategory>(
				EventCategory.LOCATION_TEST_TYPE_INVALIDATED,
				new Class<?>[] { LocationTestTypeInvalidatedEvent.class, TestTypeGroupDischargedEvent.class });

		HashMap<TestType, Set<TestTypeGroup>> defMap = new HashMap<>();
		for (TestType tt : TestType.ALL_TEST_TYPES) {
			defMap.put(tt, Collections.unmodifiableSet(new HashSet<TestTypeGroup>()));
		}
		defaultTestTypeTTGMap = Collections.unmodifiableMap(defMap);
		path2WeightRefCounts = new HashMap<>();

		NullDistributeLazyOTFFactory nullDistFact = new NullDistributeLazyOTFFactory();
		distributeLazyOTF = nullDistFact.newDistributeLazyOTF();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#getTestTypeGroups()
	 */
	@Override
	public Set<TestTypeGroup> getTestTypeGroups() {
		return typeGroups;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#
	 * set{@link Path2Weight}ControlInterface
	 * (utwente.fmt.jtorx.lazyotf.guidance.Path2WeightControlInterface)
	 */
	@Override
	public void setPath2WeightControlInterface(Path2WeightControlInterface it) {
		path2WeightManipulator = it;
	}

	@Override
	public void setDistributeLazyOTF(DistributeLazyOTF it) {
		distributeLazyOTF = it;
		it.addExternalDischargeObserver(new Observer() {
			@ObserverMethod
			public void onExternalDischarge(ExternalDischargeEvent ev) {
				onExternalDischargeTestTypeGroup(ev.dischargedTestTypeGroupName);
			}
		});
	}

	@Override
	public DistributeLazyOTF getDistributeLazyOTF() {
		return distributeLazyOTF;
	}

	/**
	 * Handles the external discharge of the test type group named <tt>dischargedTTGName</tt>,
	 * caused by <tt>distributeLazyOTF</tt>.
	 * @param dischargedTTGName The discharged test type group's name.
	 */
	private void onExternalDischargeTestTypeGroup(String dischargedTTGName) {
		TestTypeGroup dischargedTTG = ttgByName.get(dischargedTTGName);
		assert dischargedTTG != null;
		if (dischargedTTG.isActive()) {
			countDischargedFromExtern++;
			dischargeTestTypeGroup(dischargedTTG, TestType.TEST_GOAL, null, false);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#addTestTypeGroup(utwente
	 * .fmt.jtorx.lazyotf.policy.TestTypeGroup)
	 */
	@Override
	public void addTestTypeGroup(TestTypeGroup g) {
		if (!g.isEnabled()) {
			LOGGER.log(Level.FINE, "Not adding disabled test type group " + g.getName() + "'");
			return;
		}

		LOGGER.log(Level.FINE, "Adding test type group '" + g.getName() + "'");

		if (typeGroups.contains(g)) {
			return;
		}
		typeGroups.add(g);

		assert g.getName() != null;
		assert !ttgByName.containsKey(g.getName());
		ttgByName.put(g.getName(), g);

		if (isInitialised) {
			initializeTestTypeGroup(g);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#init()
	 */
	@Override
	public void init() {
		for (TestTypeGroup g : typeGroups) {
			initializeTestTypeGroup(g);
		}

		Set<Location> invalidateDueToStrategy = new HashSet<>();
		for (Location l : testTypeGroupsByLocation.keySet()) {
			Set<TestTypeGroup> dbsTTGs;
			dbsTTGs = testTypeGroupsByLocation.get(l).get(TestType.DECIDED_BY_STRATEGY);
			if (dbsTTGs != null && !dbsTTGs.isEmpty()) {
				invalidateDueToStrategy.add(l);
			}
		}
		if (!invalidateDueToStrategy.isEmpty()) {
			onLocationTestTypeInvalidated(invalidateDueToStrategy, true);
		}

		isInitialised = true;
	}

	/**
	 * Initializes the given test type group. Currently, this entails issuing
	 * the setup of its location test type decision strategy and its guidance
	 * method.
	 * 
	 * @param g
	 *            The {@link TestTypeGroup} which should be initialized.
	 */
	private void initializeTestTypeGroup(TestTypeGroup g) {
		if ((g.getTestTypes().contains(TestType.TEST_GOAL) && !g.getStates(TestType.TEST_GOAL)
				.isEmpty())
				|| (g.getTestTypes().contains(TestType.DECIDED_BY_STRATEGY) && !g.getStates(
						TestType.DECIDED_BY_STRATEGY).isEmpty())) {
			activeTestTypeGroupsWithTestGoal.add(g);
		}

		Set<Location> invalidatedTestTypeLocs = new HashSet<>();
		for (Location l : g.getStates()) {
			if (!testTypeGroupsByLocation.containsKey(l)) {
				Map<TestType, Set<TestTypeGroup>> testTypeGroups = new HashMap<>();
				testTypeGroupsByLocation.put(l, testTypeGroups);
				for (TestType t : TestType.ALL_TEST_TYPES) {
					testTypeGroups.put(t, new HashSet<TestTypeGroup>());
				}
			}
			testTypeGroupsByLocation.get(l).get(g.getTestType(l)).add(g);
			invalidatedTestTypeLocs.add(l);
		}
		LOGGER.log(Level.FINE, "Invalidating " + invalidatedTestTypeLocs);

		// If init() has not been executed yet, delay the update of the
		// max constant test type map.
		onLocationTestTypeInvalidated(invalidatedTestTypeLocs, isInitialised);

		for (Path2Weight gm : g.getPath2Weights()) {
			if (path2WeightRefCounts.containsKey(gm)) {
				int currentRefCount = path2WeightRefCounts.get(gm);
				path2WeightRefCounts.put(gm, currentRefCount + 1);
			}
			else {
				path2WeightRefCounts.put(gm, 1);
			}

			if (!path2WeightManipulator.isEnabled(gm)) {
				path2WeightManipulator.setEnabled(gm, true);
			}
		}

		if (sumPath2WeightWithinTestObjectives) {
			path2WeightManipulator.aggregate(g.getPath2Weights(), AggregationMode.SUM);
		}

		if (g.isActive() && !g.getStates(TestType.DECIDED_BY_STRATEGY).isEmpty()
				&& g.getStrategy() != null) {
			LOGGER.log(Level.FINE, "Installing strategy " + g.getStrategy().getImplementationName()
					+ " for ttg '" + g.getName() + "'.");

							DecisionStrategyHandle dsHandle;
							dsHandle = decisionStrategyMgr.installDecisionStrategy(g.getStrategy(),
									g.getStates(TestType.DECIDED_BY_STRATEGY));
							ttgByHandle.put(dsHandle, g);
							g.setStrategyHandle(dsHandle);
		}
		else {
			LOGGER.log(Level.FINE, "Not installing a strategy for ttg '" + g.getName() + "'.");
			g.setStrategyHandle(null);
		}
	}

	/**
	 * Event handler called when a location's test type has become invalid.
	 * 
	 * @param invalidatedLocs
	 *            The set of locations whose test type assignments have become
	 *            invalid.
	 * @param isInvMaxConstMap
	 *            If true, the maximum constant type map needs to be updated as
	 *            well.
	 */
	private void onLocationTestTypeInvalidated(final Set<Location> invalidatedLocs,
			final boolean isInvMaxConstMap) {
		Set<Location> unModIV = Collections.unmodifiableSet(invalidatedLocs);
		LocationTestTypeInvalidatedEvent ev = new LocationTestTypeInvalidatedEvent(unModIV,
				isInvMaxConstMap);

		eventManager.notifyObservers(ev, EventCategory.LOCATION_TEST_TYPE_INVALIDATED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#
	 * getTestTypeGroupsByTestType(utwente.fmt.jtorx.lazyotf.location.Location)
	 */
	@Override
	public Map<TestType, Set<TestTypeGroup>> getTestTypeGroupsByTestType(Location l) {
		if (!testTypeGroupsByLocation.containsKey(l)) {
			return defaultTestTypeTTGMap;
		}
		else {
			return testTypeGroupsByLocation.get(l);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#
	 * removeTestTypeGroupFromLocation
	 * (utwente.fmt.jtorx.lazyotf.location.Location,
	 * utwente.fmt.jtorx.lazyotf.policy.TestTypeGroup)
	 */
	@Override
	@Deprecated
	public void removeTestTypeGroupFromLocation(Location l, TestTypeGroup g) {
		if (!testTypeGroupsByLocation.containsKey(l)) {
			throw new IllegalArgumentException(l.toString()
					+ " not managed by this location manager.");
		}
		testTypeGroupsByLocation.get(l).get(g.getTestType(l)).remove(g);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#dischargeTestTypeGroup
	 * (utwente.fmt.jtorx.lazyotf.policy.TestTypeGroup,
	 * utwente.fmt.jtorx.lazyotf.location.TestType, java.util.Collection)
	 * 
	 * TODO: put parameter pushToDistributedInstances into interface TestTypeGroupMgr?
	 */
	@Override
	public void dischargeTestTypeGroup(TestTypeGroup g, TestType type,
			Collection<TestTypeGroup> deactivatedSet) {
		final boolean pushToDistributedInstances = true;
		dischargeTestTypeGroup(g,type,deactivatedSet,pushToDistributedInstances);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#dischargeTestTypeGroup
	 * (utwente.fmt.jtorx.lazyotf.policy.TestTypeGroup,
	 * utwente.fmt.jtorx.lazyotf.location.TestType, java.util.Collection)
	 * 
	 * TODO: put parameter pushToDistributedInstances into interface TestTypeGroupMgr? 
	 * Makes sense since other distributed concerns are in the interface, too, and then 
	 * dischargeTestTypeGroup without the boolean parameter can be omitted. 
	 */
	private void dischargeTestTypeGroup(TestTypeGroup g, TestType type,
			Collection<TestTypeGroup> deactivatedSet, boolean pushToDistributedInstances) {
		if (g != null) {
			LOGGER.log(Level.FINE, "Hitting test type group " + g.getName());
			if (type.getInternalName().equals("TEST_GOAL") && g.isActive()) {
				eventManager.notifyObservers(new TestTypeGroupDischargedEvent(g),
						EventCategory.TEST_TYPE_GROUP_DISCHARGED);
				setActive(g, false);
				if (pushToDistributedInstances) {
					distributeLazyOTF.pushInternalDischarge(g);
				}
				if (deactivatedSet != null) {
					deactivatedSet.add(g);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#dischargeTestTypeGroup
	 * (utwente.fmt.jtorx.lazyotf.policy.TestTypeGroup,
	 * utwente.fmt.jtorx.lazyotf.location.TestType)
	 */
	@Override
	public void dischargeTestTypeGroup(TestTypeGroup g, TestType type) {
		dischargeTestTypeGroup(g, type, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#setActive(utwente.
	 * fmt.jtorx.lazyotf.policy.TestTypeGroup, boolean)
	 */
	@Override
	public void setActive(TestTypeGroup g, boolean enabledness) {
		LOGGER.log(Level.FINE, 
				(enabledness ? "Activating" : "Deactivating") + " test type group '" + g.getName()
				+ "'.");
		if (g.isActive() == enabledness) {
			throw new IllegalStateException("Cannot "
					+ (enabledness ? "activate active" : "deactivate inactive")
					+ " test type groups.");
		}
		g.setActive(enabledness);
		Set<Location> invalidatedTestTypeLocs = new HashSet<Location>();

		/* Signal the invalidation of the location test types */
		for (Location loc : g.getStates()) {
			LOGGER.log(Level.FINE, "Invalidating test type for location " + loc.getName());

			TestType tt = g.getTestType(loc);

			if (!enabledness) {
				testTypeGroupsByLocation.get(loc).get(tt).remove(g);
			}
			else {
				testTypeGroupsByLocation.get(loc).get(tt).add(g);
			}

			invalidatedTestTypeLocs.add(loc);
		}
		onLocationTestTypeInvalidated(invalidatedTestTypeLocs, true);

		/* Update the decision strategies */
		if (g.getStrategy() != null) {
			if (g.getStrategyHandle() != null) {
				LOGGER.log(Level.FINE,
						(enabledness ? "Enabling" : "Disabling") + " Strategy " + g.getStrategy());
				decisionStrategyMgr.setStrategyEnabled(g.getStrategyHandle(), enabledness);
			}
		}

		/* Update the {@link Path2Weight}s */
		int refCountDelta = enabledness ? 1 : -1;
		for (Path2Weight gm : g.getPath2Weights()) {
			int currentRefCount = path2WeightRefCounts.get(gm);
			currentRefCount += refCountDelta;
			path2WeightRefCounts.put(gm, currentRefCount);

			if (!enabledness && currentRefCount == 0) {
				path2WeightManipulator.setEnabled(gm, false);
			}
			else if (enabledness && currentRefCount == 1) {
				path2WeightManipulator.setEnabled(gm, true);
			}
		}

		if (enabledness && sumPath2WeightWithinTestObjectives) {
			path2WeightManipulator.aggregate(g.getPath2Weights(), AggregationMode.SUM);
		}

		/* Update the set of active test type groups */
		boolean containsTG = g.getTestTypes().contains(TestType.TEST_GOAL);
		boolean containsDS = g.getTestTypes().contains(TestType.DECIDED_BY_STRATEGY);
		if ((containsTG  && !g.getStates(TestType.TEST_GOAL).isEmpty())
				|| (containsDS && !g.getStates(TestType.DECIDED_BY_STRATEGY).isEmpty())) {
			if (enabledness) {
				activeTestTypeGroupsWithTestGoal.add(g);
			}
			else {
				activeTestTypeGroupsWithTestGoal.remove(g);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#
	 * getActiveTestTypeGroupsWithTestGoal()
	 */
	@Override
	public Set<TestTypeGroup> getActiveTestTypeGroupsWithTestGoal() {
		return activeTestTypeGroupsWithTestGoal;
	}

	@Override
	public boolean isEveryTestObjectiveDischarged() {
		return getActiveTestTypeGroupsWithTestGoal().size() == 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#hitTestGoal(utwente
	 * .fmt.jtorx.lazyotf.location.Location, java.util.Set)
	 */
	@Override
	public void hitTestGoal(Location testGoal, Set<TestTypeGroup> deactivatedTestTypeGroups) {
		Map<TestType, Set<TestTypeGroup>> type2TTG = getTestTypeGroupsByTestType(testGoal);

		Collection<TestTypeGroup> testGoalGroups = new HashSet<>(type2TTG.get(TestType.TEST_GOAL));
		for (TestTypeGroup ttg : testGoalGroups) {
			dischargeTestTypeGroup(ttg, TestType.TEST_GOAL, deactivatedTestTypeGroups);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * utwente.fmt.jtorx.lazyotf.policy.ITestTypeGroupMgr#hitTestGoal(utwente
	 * .fmt.jtorx.lazyotf.strategy.DecisionStrategyHandle, java.util.Set)
	 */
	@Override
	public void hitTestGoal(DecisionStrategyHandle byStrategy,
			Set<TestTypeGroup> deactivatedTestTypeGroups) {
		TestTypeGroup matchingGroup = ttgByHandle.get(byStrategy);
		if (matchingGroup != null) {
			dischargeTestTypeGroup(matchingGroup, TestType.TEST_GOAL, deactivatedTestTypeGroups);
		}
	}

	/**
	 * Adds an event observer to the object.
	 * 
	 * The events are of type {@link LocationTestTypeInvalidatedEvent}.
	 * 
	 * @param observer
	 *            The observer object, implementing {@link Observer}.
	 * @param category
	 *            The observer's event category.
	 */
	@Override
	public void addObserver(Observer observer, EventCategory category) {
		eventManager.addObserver(observer, category);
	}

	/**
	 * Sets the weight aggregation mode within test objectives. The default
	 * is {@code true}.
	 * @param enabled If {@code false}, no further aggregation happens within
	 *   test objectives. If {@code true}, the Path2Weight results are summed
	 *   within test objectives.
	 */
	public static void setSumAggregationWithinTestObjectives(boolean enabled) {
		sumPath2WeightWithinTestObjectives = enabled;
	}

	/* (non-Javadoc)
	 * @see utwente.fmt.jtorx.lazyotf.TestTypeGroupMgr#getNumberOfTTGsDischargedFromExtern()
	 */
	@Override
	public int getNumberOfTTGsDischargedFromExtern() {
		return countDischargedFromExtern;
	}

	@Override
	public void cleanUp() {
		int tgCount = getTestTypeGroups().size();
		int dischargedCount = getActiveTestTypeGroupsWithTestGoal().size();

		LOGGER.log(Level.INFO, "Cleaning up a TestTypeGroupMgr with "
				+ tgCount + " of " + dischargedCount + " test goals discharged, "
				+ countDischargedFromExtern + " of which were discharged externally.");

		distributeLazyOTF.cleanUp();
	}
}
