/**
 * <p>This package contains LazyOTF policies managing the LazyOTF instance
 * during test execution time.</p>
 * 
 * <ul>
 *  <li>{@link LazyOTFPolicy} is an observer for the LazyOTF-enabled JTorX
 *      driver. It processes information about the executed test steps and
 *      updates the LazyOTF instance accordingly.</li>
 * 
 *  <li>{@link LocationMgrImpl} creates and initializes the
 *      {@link TraversalDepthMgr}, {@link TestTypeGroupMgr} and
 *      {@link DecisionStrategyMgr} objects for the LazyOTF instance.</li>
 * 
 *  <li>{@link DecisionStrategyMgr} is responsible for issuing the installation
 *      of test type decision strategies and for maintaining the maximum
 *      constant test type mapping strategy. Since strategies are evaluated at
 *      test tree computation time, their results are compared with the constant
 *      test type assignments for the respective locations using a
 *      {@link MappingStrategy}. (This plays a role e.g. when one test type
 *      group assigns the type DECIDED_BY_STRATEGY and another assigns any other
 *      test type. The effective test type is the higher-priority one, which is
 *      determined during the test tree computation. The latter test type is
 *      provided by the maximum constant mapping strategy.)</li>
 * 
 *  <li>{@link TestTypeAssignerImpl} assigns test types to locations using the
 *      respective current set of test type groups.</li>
 * 
 *  <li>{@link TestTypeGroupMgrImpl} is responsible for installing and discharging
 *      test type groups.</li>
 * </ul>
 */
package utwente.fmt.jtorx.lazyotf.policy;

