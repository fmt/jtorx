package utwente.fmt.jtorx.lazyotf.remoteimpl;

public enum AddRemoveSubcommandIdentifier {
    ADD,
    REMOVE;

    @Override
    public String toString() {
        return TokenNameTranslator.encode(super.toString());
    }
}
