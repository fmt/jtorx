package utwente.fmt.jtorx.lazyotf.remoteimpl;

/**
 * The "toplevel" commands.
 */
public enum CommandIdentifier {
    ORACLESAFETYTREE,
    TOOK_TRANSITION_EXT,
    PATH2WEIGHT,
    COMPUTE_NEW_TEST_TREE,
    DEBUG,
    GET,
    HELP,
    NODEVALUE2WEIGHT,
    PROPOSE,
    SELECT_VISITOR,
    SET_TEST_TREE_WEIGHER,
    SET_LOCATION_TYPE,
    SET_MAX_RECURSION_DEPTH,
    STRATEGY,
    TOOK_TRANSITION,
    TORX_GET,
    UNKNOWN_COMMAND,
    INHIBIT_NONDET_TESTGOALS,
	SET_CYCLE_WARNING_THRESHOLD,
	DISCHARGED_TEST_OBJECTIVE,
	GET_CYCLE_WARNING;

    @Override
    public String toString() {
        return TokenNameTranslator.encode(super.toString());
    }
}
