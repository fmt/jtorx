package utwente.fmt.jtorx.lazyotf.remoteimpl;

public enum EnableDisableCommandIdentifier {
    ENABLE,
    DISABLE;
    
    @Override
    public String toString() {
        return TokenNameTranslator.encode(super.toString());
    }
}
