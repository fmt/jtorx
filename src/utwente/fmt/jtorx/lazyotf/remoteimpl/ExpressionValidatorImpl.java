package utwente.fmt.jtorx.lazyotf.remoteimpl;

import utwente.fmt.jtorx.lazyotf.ExpressionValidator;
import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTF.InactiveException;
import utwente.fmt.jtorx.lazyotf.guidance.LocationValuation2Weight;

/**
 * <p>
 * A validator for (Dumont) expressions and conditions.
 * </p>
 * 
 * <p>
 * This validator uses {@link LazyOTF} to validate expressions. Thus, if the
 * LazyOTF/STSimulator combination no longer uses Dumont as its switch
 * restriction rsp. update mapping language, no changes are needed in this
 * validator implementation.
 * </p>
 */
public class ExpressionValidatorImpl implements ExpressionValidator {
    /** The LazyOTF instance used to perform the checks. */
    private final LazyOTF lazyOTF;

    /**
     * Constructs a new {@link ExpressionValidatorImpl} instance.
     * 
     * @param lazyOTF
     *            The {@link LazyOTF} instance used for validating expressions.
     */
    public ExpressionValidatorImpl(LazyOTF lazyOTF) {
        this.lazyOTF = lazyOTF;
    }

    @Override
    public void check(String condition, String expression) throws ValidationException {
        LocationValuation2Weight dummy = new LocationValuation2Weight();
        dummy.addExpression(condition, expression);
        try {
            lazyOTF.addNodeValue2Weight(dummy);
            try {
                lazyOTF.removeNodeValue2Weight(dummy);
            }
            catch (InactiveException | IllegalArgumentException e) {
                throw new IllegalStateException("Unexpected exception " + e);
            }
        }
        catch (InactiveException e) {
            throw new IllegalStateException("Need active instance to check expressions.");
        }
        catch (IllegalArgumentException e) {
            throw new ValidationException(e.toString());
        }
    }

    @Override
    public void checkUpdateExpression(String expression) throws ValidationException {
        check("true==true", expression);
    }

    @Override
    public void checkCondition(String condition) throws ValidationException {
        check(condition, "1");
    }

}
