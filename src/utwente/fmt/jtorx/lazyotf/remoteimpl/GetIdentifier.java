package utwente.fmt.jtorx.lazyotf.remoteimpl;

public enum GetIdentifier {
    CONFIGURABLE_PATH2WEIGHTS,
    DEFAULT_TEST_TREE_WEIGHER,
    DEFAULT_PATH2WEIGHT,
    DEFAULT_VISITOR,
    LIST_OF_TEST_TREE_WEIGHERS,
    LIST_OF_PATH2WEIGHTS,
    HANDLER_STATE,
    LIST_OF_LOCATION_TEST_TYPES,
    LIST_OF_LOCATIONS,
    LIST_OF_STRATEGIES,
    LOCATION_TEST_TYPE,
    NODE_DESCRIPTION,
    LIST_OF_VISITORS;

    @Override
    public String toString() {
        return TokenNameTranslator.encode(super.toString());
    }
}
