package utwente.fmt.jtorx.lazyotf.remoteimpl;

public enum HitIdentifier {
    HIT, MISS;

    @Override
    public String toString() {
        return TokenNameTranslator.encode(super.toString());
    }

    public static HitIdentifier get(String name) {
        return HitIdentifier.valueOf(TokenNameTranslator.decode(name));
    }
}
