package utwente.fmt.jtorx.lazyotf.remoteimpl;

import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.CommunicationItem;

/**
 * Representation for messages exchanged between JTorX and LazyOTF.
 */
public class Message {
    public enum Kind {
        SENT, RECEIVED;
    }

    /** The instance's message kind. */
    private final Kind kind;

    /** Object giving access to the originally exchanged data. */
    private final CommunicationItem<?> messageObject;

    /** true iff this is an error message. */
    private final boolean isError;

    /** This message's sequence number. */
    private int sequenceNumber = -1;

    /**
     * Construct a new Message instance.
     * 
     * @param messageObject
     *            A CommunicationItem instance representing the originally
     *            exchanged data.
     * @param kind
     *            The message kind.
     * @param isError
     *            true iff this should be an error message.
     */
    protected Message(CommunicationItem<?> messageObject, Kind kind, boolean isError) {
        this.kind = kind;
        this.messageObject = messageObject;
        this.isError = isError;
    }

    /**
     * @return The message kind.
     */
    public Kind getKind() {
        return kind;
    }

    @Override
    public String toString() {
        return messageObject.toString();
    }

    /**
     * @return A pretty-printed version of the message.
     */
    public String toPrettyString() {
        return messageObject.toPrettyString();
    }

    /**
     * @return true iff this is an error message.
     */
    public boolean isErrorMessage() {
        return isError;
    }

    /**
     * Set this message's sequence number.
     * 
     * @param i
     *            The new sequence number.
     */
    public void setSequenceNumber(int i) {
        sequenceNumber = i;
    }

    /**
     * @return This message's sequence number.
     */
    public int getSequenceNumber() {
        return sequenceNumber;
    }
}
