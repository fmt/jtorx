package utwente.fmt.jtorx.lazyotf.remoteimpl;

public enum Path2WeightSubcommandIdentifier {
    ADD,
    REMOVE,
    AGGREGATE
}
