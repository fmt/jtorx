package utwente.fmt.jtorx.lazyotf.remoteimpl;

public enum ReadyIdentifier {
    READY,
    NOT_READY;

    @Override
    public String toString() {
        return TokenNameTranslator.encode(super.toString());
    }

    public static ReadyIdentifier get(String name) {
        return ReadyIdentifier.valueOf(TokenNameTranslator.decode(name));
    }
}
