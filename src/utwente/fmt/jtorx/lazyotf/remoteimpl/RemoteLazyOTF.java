package utwente.fmt.jtorx.lazyotf.remoteimpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfigurationOptions;
import utwente.fmt.jtorx.lazyotf.LazyOTFPolicyFactory;
import utwente.fmt.jtorx.lazyotf.LocationMgr;
import utwente.fmt.jtorx.lazyotf.TestTree2Weight;
import utwente.fmt.jtorx.lazyotf.TestTreeNode;
import utwente.fmt.jtorx.lazyotf.TestTypeAssigner;
import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.jtorx.lazyotf.TraversalDepthComputer;
import utwente.fmt.jtorx.lazyotf.TraversalMethod;
import utwente.fmt.jtorx.lazyotf.distributed.DistributeLazyOTF;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2WeightImpl;
import utwente.fmt.jtorx.lazyotf.guidance.LocationValuation2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Locations2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2WeightVisitor;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightControlInterface;
import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightControlInterface.AggregationMode;
import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightVisitor;
import utwente.fmt.jtorx.lazyotf.guidance.SimplePath2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.SuperLocationSize2Weight;
import utwente.fmt.jtorx.lazyotf.location.InstantiatedLocation;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.LocationVariable;
import utwente.fmt.jtorx.lazyotf.location.LocationVariableTerm;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.CommunicationItem;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.InstantiatedLocationDescription;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.QueryCommunicationHandler;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.QueryMgr;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.ResponseCommunicationHandler;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.ResponseCommunicationHandler.UnexpectedResponseException;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.TestTreeNodeDescription;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.XMLQueryMgr;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategyHandle;
import utwente.fmt.jtorx.lazyotf.strategy.MappingStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.StrategyControlInterface;
import utwente.fmt.jtorx.lazyotf.utilities.ObservableImpl;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.testnongui.casestudy.CaseStudyIO;
import utwente.fmt.jtorx.torx.DriverSimInterActionResult;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.utils.Pair;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Term;
import utwente.fmt.lts.Variable;

/**
 * An interface for LazyOTF-JTorX.
 */
public class RemoteLazyOTF implements LazyOTF {
	/**
	 * The class logger.
	 */
	private static final Logger LOGGER = Logger.getLogger(RemoteLazyOTF.class.getName());

	/**
	 * If true, the took-transition-ext command is used in lieu of
	 * took-transition. This saves context switches by summarizing the
	 * 'took-transition', 'propose' and 'get node-id -1' commands.
	 */
	private static final Boolean USE_EXTENDED_TOOKTRANSITION_CMD = true;

	/**
	 * The remaining LazyOTF tree depth at which an oracle safety tree
	 * is computed.
	 */
	private static final int ORACLE_SAFETY_TREE_SAFETYBOUND = 1;

	/**
	 * The depth of the oracle safety trees.
	 */
	private static final int ORACLE_SAFETY_TREE_DEPTH = 1;

	/**
	 * The "null" handle for {@link Path2Weight}s.
	 */
	private static final String NULL_HANDLE = "";

	/**
	 * The instance's configuration object.
	 */
	private final LazyOTFConfig cfg;

	/**
	 * Indicates this instance's activeness. Instance is active ==> New test
	 * trees can be generated.
	 */
	private boolean isActive = true;

	/**
	 * Indicates if this instance can currently propose inputs rsp. process
	 * outputs.
	 */
	private boolean isReady = false;

	/**
	 * Indicates if this instance has been initialised.
	 */
	private boolean isInitialised = false;

	/**
	 * LazyOTFMessage log. Needed for correctly ordered recv/send notifications.
	 */
	private final Deque<Message> messageLog;

	/**
	 * This instance's location manager, used for keeping track of locations and
	 * their associations with test types and test type groups.
	 */
	private final LocationMgr locationMgr;

	/**
	 * The communication handler responsible for interpreting responses to
	 * queries.
	 */
	private final ResponseCommunicationHandler responseCommh;

	/** The communication handler responsible for sending queries. */
	private final QueryCommunicationHandler commandCommh;

	/** The manager of the communication handlers. */
	private final QueryMgr commMgr;

	/**
	 * This instance's event manager. LazyOTFViaStreams indicates "interesting"
	 * events to its EventManager, which in turn is Observable.
	 */
	private final ObservableImpl eventMgr;

	/**
	 * {@code true} iff a test tree has been computed and no transition has been
	 * taken since.
	 */
	private boolean hasFreshTestTree = false;

	/**
	 * A map associating decision strategy handle strings obtained from LazyOTF
	 * with their corresponding decision strategy handles.
	 */
	private final Map<String, DecisionStrategyHandle> installedStrategies = new HashMap<>();

	/**
	 * A map associating {@link Path2Weight}s with their corresponding handle
	 * strings obtained from LazyOTF.
	 */
	private final Map<Path2Weight, String> path2WeightIDs = new HashMap<>();

	/** The model traversal depth computer instance. */
	private final TraversalDepthComputer traversalDepthComp;

	/**
	 * A map associating the node value weighers with their corresponding handle
	 * strings obtained from LazyOTF.
	 */
	private final Map<NodeValue2Weight, String> nodeValue2WeightIDs = new HashMap<>();

	/**
	 * A backlog for actions which must be postponed until after
	 * {@link #init()}.
	 */
	private final SetupBacklog preInitBacklog = new SetupBacklog();

	/**
	 * If {@code true}, the pre-init backlog is used.
	 */
	private boolean isPreInitBackloggingEnabled = true;

	/**
	 * Holds the current oracle safety tree enabledness state of the
	 * underlying LazyOTF instance. Its value is set every time the
	 * ORACLESAFETYTREE command is sent down to the (remote) LazyOTF
	 * instance.
	 */
	private boolean isSafetyOracleTreeEnabled = false;

	/**
	 * Constructs a new RemoteLazyOTF instance.
	 * 
	 * @param input
	 *            The stream from which the interface reads LazyOTF responses.
	 * @param output
	 *            The stream to which the interface writes LazyOTF commands.
	 * @param config
	 *            A LazyOTFConfig instance containing the configuration for this
	 *            LazyOTF instance.
	 * @param polFact
	 *            A LazyOTF policy object factory.
	 */
	public RemoteLazyOTF(BufferedReader input,
			OutputStream output,
			LazyOTFConfig config,
			LazyOTFPolicyFactory polFact) {
		this(config, polFact, new XMLQueryMgr(input, output, input));
	}

	/**
	 * Constructs a new RemoteLazyOTF instance.
	 * 
	 * @param config
	 *            A LazyOTFConfig instance containing the configuration for this
	 *            LazyOTF instance.
	 * @param polFact
	 *            A LazyOTF policy object factory.
	 * @param commMgr
	 *            A QueryMgr instance providing access to the communication
	 *            layer.
	 */
	public RemoteLazyOTF(
			LazyOTFConfig config,
			LazyOTFPolicyFactory polFact,
			QueryMgr commMgr) {
		LOGGER.fine("[progress] Creating a LazyOTF instance.");
		polFact.setLazyOTFInstance(this);

		eventMgr = new ObservableImpl(LazyOTFEvents.class.getClasses());
		this.commMgr = commMgr;
		responseCommh = commMgr.getResponseHandler();
		commandCommh = commMgr.getQueryHandler();
		messageLog = new LinkedList<Message>();

		cfg = config;
		if (config.isCommunicationLoggingEnabled()) {
			CaseStudyIO.err.println("LazyOTF: Creating an instance with communication logging enabled.");
			CaseStudyIO.err.flush();
			setupMessageLogging(commMgr);
		}
		else {
			CaseStudyIO.err.println("LazyOTF: Creating an instance with communication logging disabled.");
			CaseStudyIO.err.flush();
		}

		locationMgr = polFact.newLocationManager();
		for (Location l : cfg.getLocations()) {
			locationMgr.addLocation(l);
		}
		for (TestTypeGroup ttg : cfg.getTestTypeGroups()) {
			locationMgr.getTestTypeGroupMgr().addTestTypeGroup(ttg);
		}

		traversalDepthComp = polFact.newTraversalDepthComputer();
		traversalDepthComp.addObserver(new java.util.Observer() {
			@Override
			public void update(java.util.Observable arg0, Object arg1) {
				assert arg1 instanceof Integer;
				try {
					RemoteLazyOTF.this.setMaxRecursionDepth((Integer) arg1);
				}
				catch (InactiveException e) {
					// not critical - ignore
				}
			}
		});

		DistributeLazyOTF distLazyOTF = cfg.getDistributeLazyOTFFactory().newDistributeLazyOTF();
		locationMgr.getTestTypeGroupMgr().setDistributeLazyOTF(distLazyOTF);
		LOGGER.fine("[progress] Done creating a LazyOTF instance.");
	}

	/**
	 * <p>
	 * Make a LazyOTF instance without configuring anything. This can be applied
	 * to a previously configured LazyOTF backend or to a backend not even
	 * having a model loaded - however, in the latter case the functionality of
	 * this handler is restricted to queries about the backend's capabilities.
	 * </p>
	 * 
	 * <p>
	 * Since there is no configuration to initialize, pre-initialisation
	 * add-method backlogging is disabled.
	 * </p>
	 * 
	 * @param input
	 *            The stream from which the interface reads LazyOTF responses.
	 * @param output
	 *            The stream to which the interface writes LazyOTF commands.
	 * @param polFact
	 *            A LazyOTF policy object factory.
	 * @param modelless
	 *            If true, don't try to load any model properties.
	 */
	public RemoteLazyOTF(BufferedReader input,
			OutputStream output,
			LazyOTFPolicyFactory polFact,
			boolean modelless) {
		this(input, output, new LazyOTFConfig(), polFact);
		LOGGER.log(Level.FINE, "Making a configurationless LazyOTF instance.");
		deactivateIfIONotWorking();
		if (!modelless) {
			try {
				initLocationManager();
			}
			catch (InactiveException e) {
				reportImplementationError("Implementation error: Unable to construct default Location Manager");
			}
		}
		isPreInitBackloggingEnabled = false;
	}

	/**
	 * Sets up logging of the messages exchanged with the LazyOTF core.
	 * @param commMgr The {@link QueryMgr} handling the communication.
	 */
	private void setupMessageLogging(QueryMgr commMgr) {
		ResponseCommunicationHandler responseCommh = commMgr.getResponseHandler();
		QueryCommunicationHandler commandCommh = commMgr.getQueryHandler();
		
		responseCommh.getEventManager().addObserver(new java.util.Observer() {
			@Override
			public void update(java.util.Observable eventMgr, Object msg) {
				LOGGER.log(Level.FINE, "Received message: " + msg.toString());
				CommunicationItem<?> ci = (CommunicationItem<?>) msg;
				messageLog.addLast(new Message(ci, Message.Kind.RECEIVED, ci.isError()));
			}
		});

		commandCommh.getEventManager().addObserver(new java.util.Observer() {
			@Override
			public void update(java.util.Observable eventMgr, Object msg) {
				LOGGER.log(Level.FINE, "Transmitted message: " + msg.toString());
				CommunicationItem<?> ci = (CommunicationItem<?>) msg;
				messageLog.addLast(new Message(ci, Message.Kind.SENT, ci.isError()));
			}
		});

		commMgr.getEventManager().addObserver(new java.util.Observer() {
			@Override
			public void update(java.util.Observable arg0, Object arg1) {
				flushMessageLog();
			}
		});
	}

	/**
	 * Test the I/O channel and set LazyOTF to inactive if it does not work.
	 */
	private void deactivateIfIONotWorking() {
		try {
			queryForString(CommandIdentifier.GET, GetIdentifier.DEFAULT_VISITOR.toString());
		}
		catch (IllegalStateException e) {
			LOGGER.log(Level.SEVERE, "RemoteLazyOTF: I/O not working, deactivating instance.");
			setActive(false);
		}
	}

	@Override
	public boolean isReady() {
		return isActive && isReady;
	}

	@Override
	public LocationMgr getLocationManager() {
		return locationMgr;
	}

	@Override
	public void setTreeWeigher(TestTree2Weight arg) throws InactiveException {
		if (arg == null) {
			throw new IllegalArgumentException("arg must not be null");
		}
		LOGGER.log(Level.FINE, "Setting edge weight aggregator to " + arg.toString());
		sendSimpleCommand(CommandIdentifier.SET_TEST_TREE_WEIGHER, arg.getName());
	}

	@Override
	public void addPath2Weight(final Path2Weight m)
			throws InactiveException {
		if (m == null) {
			throw new IllegalArgumentException("Argument 'it' must not be null");
		}

		if (!isInitialised && isPreInitBackloggingEnabled) {
			LOGGER.log(Level.FINE, "Backlogging Path2Weight " + m.getName() + ".");
			preInitBacklog.add(m);
			return;
		}

		if (path2WeightIDs.containsKey(m)
				&& path2WeightIDs.get(m) != NULL_HANDLE) {
			/* Is already installed and active - ignore. */
			return;
		}

		m.accept(new Path2WeightVisitor.Adapter() {
			@Override
			public void visit(SimplePath2Weight it) {
				if (it.getName() == null) {
					throw new IllegalArgumentException("Adding "
							+ it.getDisplayName() + "failed: name must not be null");
				}
				LOGGER.log(Level.FINE, "Attempting to to add constant guidance "
						+ "method " + it.toString());
				String id = queryForString(CommandIdentifier.PATH2WEIGHT,
						Path2WeightSubcommandIdentifier.ADD, it.getName());
				path2WeightIDs.put(m, id);
			}

			@Override
			public void visit(CustomPath2Weight it) {
				if (it.getName() == null) {
					throw new IllegalArgumentException("Adding "
							+ it.getDisplayName() + "failed: name must not be null");
				}

				String cmdArg = it.getName();

				if (it.getNodeValue2Weight() != null) {
					NodeValue2Weight nv2w = it.getNodeValue2Weight();
					if (!nodeValue2WeightIDs.containsKey(nv2w)) {
						addNodeValue2Weight(nv2w);
					}
					String nv2wID = nodeValue2WeightIDs.get(nv2w);
					assert nv2wID != null;
					cmdArg += " " + nv2wID;
				}

				String id = queryForString(CommandIdentifier.PATH2WEIGHT,
						Path2WeightSubcommandIdentifier.ADD, cmdArg);
				path2WeightIDs.put(m, id);
			}

			@Override
			public void otherMethodCalled(Path2Weight it) {
				throw new UnsupportedOperationException("Adding "
						+ it.getDisplayName() + "failed because underlying method "
						+ it.getClass() + " is not supported.");
			}
		});
	}

	@Override
	public void addNodeValue2Weight(NodeValue2Weight it) {
		if (it == null) {
			throw new IllegalArgumentException("Argument 'it' must not be null");
		}

		if (!isInitialised && isPreInitBackloggingEnabled) {
			LOGGER.log(Level.FINE, "Backlogging node value weigher " + it.getName() + ".");
			preInitBacklog.add(it);
			return;
		}

		LOGGER.log(Level.FINE, "Attempting to add node value weigher " + it.getName() + ".");

		it.accept(new NodeValue2WeightVisitor.Adapter() {
			@Override
			public void visit(Locations2Weight it) {
				String cmdArg = "Locations2Weight "
						+ it.getTestGoalWeight() + " "
						+ it.getOrdinaryWeight() + " "
						+ it.getInducingWeight();
				try {
					String result = queryForString(CommandIdentifier.NODEVALUE2WEIGHT,
							AddRemoveSubcommandIdentifier.ADD, cmdArg);
					nodeValue2WeightIDs.put(it, result);
				}
				catch (UnexpectedResponseException e) {
					throw new IllegalArgumentException(e.toString());
				}
			}

			@Override
			public void visit(LocationValuation2Weight it) {
				String cmdArg = "LocationValuation2Weight ";
				for (Pair<String, String> exprPair : it.getExpressions()) {
					cmdArg += escapeBackslash(exprPair.getLeft())
							+ "\\ß" + escapeBackslash(exprPair.getRight()) + "\\ß\\ß";
				}
				try {
					String result = queryForString(CommandIdentifier.NODEVALUE2WEIGHT,
							AddRemoveSubcommandIdentifier.ADD, cmdArg);
					nodeValue2WeightIDs.put(it, result);
				}
				catch (UnexpectedResponseException e) {
					throw new IllegalArgumentException(e.toString());
				}
			}

			@Override
			public void visit(SuperLocationSize2Weight it) {
				String cmdArg = "SuperLocationSize2Weight";
				if (!it.isUsingSystemDefaultCoefficient()) {
					cmdArg += (" " + it.getCoefficient());
				}
				try {
					String result = queryForString(CommandIdentifier.NODEVALUE2WEIGHT,
							AddRemoveSubcommandIdentifier.ADD, cmdArg);
					nodeValue2WeightIDs.put(it, result);
				}
				catch (UnexpectedResponseException e) {
					throw new IllegalArgumentException(e.toString());
				}
			}
		});
	}

	@Override
	public void removeNodeValue2Weight(NodeValue2Weight it) throws InactiveException {
		if (it == null) {
			throw new IllegalArgumentException("Argument 'it' must not be null");
		}
		if (!nodeValue2WeightIDs.containsKey(it)) {
			throw new IllegalArgumentException("Only installed node value weighers may be removed.");
		}

		LOGGER.log(Level.FINE, "Attempting to remove value weigher " + it.getName() + ":");

		sendSimpleCommand(CommandIdentifier.NODEVALUE2WEIGHT, AddRemoveSubcommandIdentifier.REMOVE,
				nodeValue2WeightIDs.get(it));
	}

	@Override
	public void removePath2Weight(Path2Weight it) {
		if (path2WeightIDs.containsKey(it)) {
			String id = path2WeightIDs.get(it);
			sendSimpleCommand(CommandIdentifier.PATH2WEIGHT,
					Path2WeightSubcommandIdentifier.REMOVE, id);
			path2WeightIDs.put(it, NULL_HANDLE);
		}
		else {
			throw new IllegalArgumentException("Cannot remove Path2Weight " + it.toString()
					+ " because it has not been added yet.");
		}
	}

	/**
	 * Aggregates the given {@link Path2Weight}s. The {@link Path2Weight}s are
	 * effectively replaced by their aggregation.
	 * 
	 * @param path2Weights
	 *            The (installed) {@link Path2Weight}s to be aggregated.
	 * @param aggMode
	 *            The {@link AggregationMode}, either
	 *            {@link AggregationMode # SUM SUM} or
	 *            {@link AggregationMode # ADD ADD}.
	 */
	private void aggregate(Collection<Path2Weight> path2Weights, AggregationMode aggMode) {
		if (path2Weights.isEmpty()) {
			return;
		}

		if (!isInitialised) {
			preInitBacklog.addAggregation(path2Weights, aggMode);
			return;
		}

		String cmdString = TokenNameTranslator.encode(aggMode.toString());
		for (Path2Weight p2w : path2Weights) {
			if (path2WeightIDs.containsKey(p2w)) {
				cmdString += " " + path2WeightIDs.get(p2w);
			}
			else {
				throw new IllegalArgumentException("Cannot aggregate Path2Weight " + p2w.toString()
						+ " - not installed");
			}
		}

		sendSimpleCommand(CommandIdentifier.PATH2WEIGHT,
				Path2WeightSubcommandIdentifier.AGGREGATE, cmdString);
	}

	/**
	 * Escape backslashes within the given string <i>it</i>.
	 * 
	 * @param it
	 *            The string to be escaped.
	 * @return The string <i>it</i> after replacing backslashes by
	 *         double-backslashes.
	 */
	private String escapeBackslash(String it) {
		return it.replaceAll("\\\\", "\\\\\\\\");
	}

	@Override
	public void setTraversalMethod(TraversalMethod arg) throws InactiveException {
		if (arg == null) {
			throw new IllegalArgumentException("name must not be null");
		}

		LOGGER.log(Level.FINE, "Attempting to to set graph traversal method " + arg.toString());

		sendSimpleCommand(CommandIdentifier.SELECT_VISITOR, arg.getName()
				+ (cfg.doUseCP() ? " -useCheckedPreWrappingVisitor" : ""));
	}

	/**
	 * Configure the test type for each STS location known to this instance.
	 * 
	 * @throws InactiveException
	 * @throws IOException
	 * @throws LazyOTFException
	 */
	private void sendTestTypeConfiguration() {
		for (Location l : locationMgr.getLocations()) {
			TestTypeAssigner testTypeAssgn = locationMgr.getTestTypeAssigner();
			if (!testTypeAssgn.isDecidedByStrategy(l)) {
				sendSimpleCommand(CommandIdentifier.SET_LOCATION_TYPE, l.getName() + " "
						+ testTypeAssgn.getMaxConstantTestType(l).getInternalName());
			}
			else {
				sendSimpleCommand(CommandIdentifier.SET_LOCATION_TYPE, l.getName() + " "
						+ "DECIDED_BY_STRATEGY");
			}
		}
	}

	@Override
	public void computeNewTestTree(Set<String> stateIDs) throws InactiveException {
		if (stateIDs == null || stateIDs.isEmpty()) {
			throw new IllegalArgumentException("stateIDs must be neither null nor empty.");
		}

		String concatenatedStateIDs = "";
		for (String s : stateIDs) {
			concatenatedStateIDs += " " + s;
		}
		concatenatedStateIDs = concatenatedStateIDs.trim();

		LOGGER.log(Level.FINE, "Computing new test tree for " + concatenatedStateIDs);

		reset();
		sendTestTypeConfiguration();
		sendSimpleCommand(CommandIdentifier.COMPUTE_NEW_TEST_TREE, concatenatedStateIDs);

		hasFreshTestTree = true;
		isReady = true;
	}

	@Override
	@Deprecated
	public void computeNewTestTree() throws InactiveException {
		throw new IllegalStateException("computeNewTestTree() is deprecated");
	}

	@Override
	public TestTree2Weight getTreeWeigher() {
		return cfg.getTestTreeWeigher();
	}

	@Override
	public Set<Path2Weight> getPath2Weights() {
		return cfg.getEnabledPath2Weights();
	}

	@Override
	public int getMaxRecursionDepth() {
		return cfg.getMaxRecursionDepth();
	}

	@Override
	public TraversalMethod getTraversalMethod() {
		return cfg.getTraversalMethod();
	}

	/**
	 * Stores LazyOTF proposals between {@link #isActive tookTransition} calls
	 * rsp. {@link # computeNewTestTree(Set)} calls.
	 */
	private Set<Label> proposalCache = null;

	@Override
	public Set<Label> propose() throws InactiveException, NotReadyException {
		if (!isActive) {
			throw new InactiveException();
		}

		if (isActive && proposalCache != null) {
			return proposalCache;
		}

		LOGGER.log(Level.FINE, "Attempting to to read LazyOTF proposal...");

		try {
			List<String> queryResult;
			queryResult = this.queryForStringList(CommandIdentifier.PROPOSE);
			HashSet<Label> result = new HashSet<Label>();

			for (String s : queryResult) {
				result.add(makeLabel(s));
			}
			proposalCache = result;
			return result;
		}
		catch (UnexpectedResponseException e) {
			throw new NotReadyException(CommandIdentifier.PROPOSE + " Exception: " + e.toString());
		}
	}

	@Override
	public void reset() {
		LOGGER.log(Level.FINE, "Resetting the LazyOTF instance");
		testTreeNodeCache.clear();
		rootNodeCache = null;
		proposalCache = null;
		isActive = true;
	}

	@Override
	public void setMaxRecursionDepth(int depth) throws InactiveException {
		if (depth < 0) {
			throw new IllegalArgumentException("depth must be >= 0");
		}

		/* Using the oracle safety tree is only sensible (and safe) when
		 * depth > OST safety bound.
		 */
		boolean useOST = (depth > ORACLE_SAFETY_TREE_SAFETYBOUND);
		setOracleSafetyTreeEnabled(useOST);

		LOGGER.log(Level.FINE, "Attempting to to set the max. traversal depth to " + depth);
		sendSimpleCommand(CommandIdentifier.SET_MAX_RECURSION_DEPTH, "" + depth);
	}

	@Override
	public boolean isCheckedPREDfsVisitorEnabled() {
		return cfg.doUseCP();
	}

	/**
	 * Initialise this instance's location manager by adding the test type
	 * groups' locations as well as any location not being governed by a test
	 * type group with the default test type.
	 * 
	 * @throws InactiveException
	 *             when the LazyOTF has been deactivated rsp. encountered an
	 *             internal fatal error.
	 */
	private void initLocationManager() throws InactiveException {
		LOGGER.log(Level.FINE, "Initializing the Location Manager...");
		locationMgr.setStrategyControlInterface(getStrategyControlInterface());
		locationMgr.setPath2WeightControlInterface(getPath2WeightControlInterface());

		List<String> newLocations = queryForStringList(CommandIdentifier.GET,
				GetIdentifier.LIST_OF_LOCATIONS);
		for (String locName : newLocations) {
			if (locationMgr.getLocation(locName) == null) {
				locationMgr.addLocation(new Location(locName));
			}
		}

		locationMgr.init();
	}

	/**
	 * Issues the extended took-transition command, which combines
	 * 'took-transition LABEL' , 'propose' and 'get node-id -1'.
	 * 
	 * @param transLabel
	 *            The label associated with the transition.
	 * @return The resulting {@link HitIdentifier}.
	 */
	private HitIdentifier extendedTookTransition(Label transLabel) {
		HitIdentifier hId;
		LOGGER.log(Level.FINE, "Starting extendedTookTransition...");
		try {
			synchronized (commMgr.getSyncObject()) {
				commandCommh.sendCommand(CommandIdentifier.TOOK_TRANSITION_EXT.toString(),
						transLabel.getString());
				String tookTransResult;
				try {
					tookTransResult = responseCommh.receiveSimpleString();
				}
				catch (UnexpectedResponseException e) {
					throw new IllegalStateException("Got unknown response to "
							+ CommandIdentifier.TOOK_TRANSITION_EXT + ": " + e);
				}

				hId = HitIdentifier.get(tookTransResult.trim());
				LOGGER.log(Level.FINE, "extendedTookTransition result: " + hId);
				if (hId == HitIdentifier.HIT) {
					/* Read further command results */

					/* Read propose result */
					try {
						List<String> proposalList = responseCommh.receiveListOfStrings();
						HashSet<Label> proposal = new HashSet<>();
						for (String s : proposalList) {
							proposal.add(makeLabel(s));
						}
						proposalCache = proposal;
						LOGGER.log(Level.FINE,
								"extendedTookTransition proposal read-ahead succeeded.");
					}
					catch (UnexpectedResponseException e) {
						/* Proposal read-ahead failed - do nothing. */
						LOGGER.log(Level.FINE, "extendedTookTransition proposal read-ahead "
								+ "failed. This is not fatal, continuing.");
						proposalCache = null;
					}

					/* Read get node-description -1 */
					try {
						TestTreeNodeDescription ndesc;
						ndesc = responseCommh.receiveTestTreeNodeDescription();
						rootNodeCache = new TestTreeNodeImpl(ndesc, null, -1);
						LOGGER.log(Level.FINE, "extendedTookTransition test tree node read-ahead "
								+ "succeeded.");
					}
					catch (UnexpectedResponseException e) {
						/* Proposal read-ahead failed - do nothing. */
						LOGGER.log(Level.FINE, "extendedTookTransition test tree node read-ahead "
								+ "failed. This is not fatal, continuing.");
						rootNodeCache = null;
					}
				}
			}
		}
		finally {
			commMgr.onMessageExchanged();
		}
		return hId;
	}

	@Override
	public void tookTransition(Label transLabel) throws InactiveException, NotReadyException {
		rootNodeCache = null;

		if (transLabel == null) {
			throw new IllegalArgumentException("transitionLabel must not be null");
		}

		LOGGER.log(Level.FINE, "Took transition " + transLabel.getString());

		if (!isActive) {
			throw new InactiveException("tookTransition");
		}
		if (!isReady()) {
			throw new NotReadyException("tookTransition");
		}

		/* Make the label LazyOTFHandler-digestible */
		Label usedTransLabel = transLabel;
		if (!transLabel.getString().trim().equals("delta")) {
			if (!transLabel.getString().trim().endsWith(")")) {
				usedTransLabel = new LibLabel(transLabel.getString().trim() + "()");
			}
			else {
				usedTransLabel = transLabel;
			}
		}

		/* Actual took-transition call */

		proposalCache = null;

		HitIdentifier hId;
		if (!USE_EXTENDED_TOOKTRANSITION_CMD) {
			String queryResult = queryForString(CommandIdentifier.TOOK_TRANSITION,
					usedTransLabel.getString());

			LOGGER.log(Level.FINE, "Result: " + queryResult);

			try {
				hId = HitIdentifier.get(queryResult.trim());
			}
			catch (IllegalArgumentException e) {
				throw new IllegalStateException("Got unknown response " + queryResult);
			}
		}
		else {
			hId = extendedTookTransition(usedTransLabel);
		}

		if (hId == HitIdentifier.MISS) {
			onTookTransition(null);
			throw new InactiveException(HitIdentifier.MISS.toString());
		}
		else {
			onTookTransition(getTestTreeNode(-1));
		}

		hasFreshTestTree = false;
	}

	/**
	 * Notify the observers about having moved down in the test tree.
	 * 
	 * @param newTestTreeNode
	 *            The new current test tree node.
	 */
	private void onTookTransition(TestTreeNode newTestTreeNode) {
		eventMgr.notifyObservers(new LazyOTFEvents.TookTransitionEvent(newTestTreeNode));
	}

	@Override
	public void tookTransition(DriverSimInterActionResult drvResult) throws InactiveException,
	NotReadyException {
		if (drvResult == null) {
			throw new IllegalArgumentException("drvResult must not be null");
		}

		tookTransition(drvResult.getLabel());
	}

	@Override
	public void onDischargedTestObjective() {
		sendSimpleCommand(CommandIdentifier.DISCHARGED_TEST_OBJECTIVE, "");
	}

	/**
	 * Controls the activeness of the oracle safety tree. Messages are
	 * sent down to the SymToSim part only if the activeness state
	 * is actually changed, i.e. subsequent calls with the same
	 * value for <i>enableOST</i> do not incur additional communication
	 * costs.
	 * 
	 * @param enableOST <code>true</code> iff the OST should
	 *   be enabled.
	 */
	private void setOracleSafetyTreeEnabled(boolean enableOST) {
		if (enableOST && (!isSafetyOracleTreeEnabled || !isInitialised)) {
			LOGGER.info("Enabling the oracle safety tree.");
			sendSimpleCommand(CommandIdentifier.ORACLESAFETYTREE, EnableDisableCommandIdentifier.ENABLE,
					ORACLE_SAFETY_TREE_SAFETYBOUND + " " + ORACLE_SAFETY_TREE_DEPTH);
		}
		else if (!enableOST && (isSafetyOracleTreeEnabled || !isInitialised)) {
			LOGGER.info("Disabling the oracle safety tree.");
			sendSimpleCommand(CommandIdentifier.ORACLESAFETYTREE, EnableDisableCommandIdentifier.DISABLE, "");
		}
		this.isSafetyOracleTreeEnabled = enableOST;
	}

	@Override
	public void init() throws InactiveException {
		LOGGER.log(Level.FINE, "[progress] Initializing a LazyOTF instance...");
		deactivateIfIONotWorking();
		if (!isActive()) {
			throw new InactiveException("I/O not working");
		}

		final String errorMsg =
				((cfg.getTraversalMethod() == null) ? "visitor" :
					((cfg.getMaxRecursionDepth() <= 0) ? "positive recursion depth" :
						null));
		if (errorMsg != null) {
			reportImplementationError("Before calling init(), you need to specify a " + errorMsg
					+ ".");
		}

		try {
			setTraversalMethod(cfg.getTraversalMethod());

			/* Gather and install Path2Weights */
			Set<Path2Weight> addedGMs = new HashSet<>();
			for (Path2Weight s : cfg.getEnabledPath2Weights()) {
				try {
					addPath2Weight(s);
					addedGMs.add(s);
				}
				catch (IllegalArgumentException e) {
					LOGGER.warning("Caught an error during the initialisation of LazyOTF:\n"
							+ e + "\n - presumed to be non-fatal.");
				}
			}

			for (TestTypeGroup t : cfg.getTestTypeGroups()) {
				if (!t.isEnabled()) {
					continue;
				}
				for (Path2Weight gm : t.getPath2Weights()) {
					if (!addedGMs.contains(gm)) {
						try {
							addPath2Weight(gm);
							addedGMs.add(gm);
						}
						catch (IllegalArgumentException e) {
							LOGGER.warning("Caught an error during the initialisation "
									+ "of LazyOTF:\n" + e
									+ "\n- presumed to be non-fatal.");
						}
					}
				}
			}

			if (cfg.getTestTreeWeigher() != null) {
				setTreeWeigher(cfg.getTestTreeWeigher());
			}
			setMaxRecursionDepth(cfg.getInitialRecursionDepth());

			if (cfg.isInhibitingNondetTestGoals()) {
				setInhibitNondeterministicTestGoals(true);
			}

			if (cfg.isCycleDetectionEnabled()) {
				setCycleWarningsEnabled(cfg.getCycleDetectionThreshold());
			}
			else {
				setCycleWarningsDisabled();
			}

			initLocationManager();
			LOGGER.fine("[progress] Done initializing a LazyOTF instance.");
		}
		catch (LazyOTF.LazyOTFException e) {
			reportImplementationError("Caught an error during the LazyOTF initialisation: "
					+ e.toString());
		}

		isInitialised = true;
		initReplayBacklog();
	}

	/**
	 * Replays the pre-init action backlog.
	 */
	private void initReplayBacklog() throws InactiveException {
		LOGGER.log(Level.FINE, "Replaying pre-init backlog.");
		for (Path2Weight item : preInitBacklog.getBackloggedPath2Weights()) {
			addPath2Weight(item);
		}
		for (NodeValue2Weight item : preInitBacklog.getBackloggedNodeValue2Weights()) {
			addNodeValue2Weight(item);
		}

		Map<Collection<Path2Weight>, AggregationMode> aggr = preInitBacklog.getBackloggedPath2WeightAggregations();
		for (Collection<Path2Weight> aggrArg : aggr.keySet()) {
			aggregate(aggrArg, aggr.get(aggrArg));
		}
	}

	/**
	 * @return a new {@link StrategyControlInterface} communicating with the
	 *         LazyOTF backend.
	 */
	private StrategyControlInterface getStrategyControlInterface() {
		return new StrategyControlInterface() {
			@Override
			public DecisionStrategyHandle installDecisionStrategy(
					DecisionStrategy ds, Set<Location> domain) {
				String domainRepr = "";
				for (Location loc : domain) {
					domainRepr += " " + loc.getName();
				}
				String handle = queryForString(CommandIdentifier.STRATEGY,
						AddRemoveSubcommandIdentifier.ADD, ds.getImplementationName() + " "
								+ domainRepr);
				DecisionStrategyHandle result = new DecisionStrategyHandle(handle, ds);
				installedStrategies.put(handle, result);
				return result;
			}

			@Override
			public void setEnabled(DecisionStrategyHandle ds, boolean enabledness) {
				StrategySubcommandIdentifier si = enabledness ? StrategySubcommandIdentifier.ENABLE
						: StrategySubcommandIdentifier.DISABLE;
				sendSimpleCommand(CommandIdentifier.STRATEGY, si, ds.getHandle().toString());
			}

			@Override
			public void simpleMap(DecisionStrategyHandle ds, Location loc, TestType type) {
				sendSimpleCommand(CommandIdentifier.STRATEGY,
						StrategySubcommandIdentifier.SIMPLE_MAP, ds.getHandle().toString() + " "
								+ loc.getName() + " " + type.getInternalName());
			}

			@Override
			public void constraintMap(DecisionStrategyHandle ds, Location loc, TestType type,
					SatStrategy.Condition cond) {
				String cmdArg = ds.getHandle().toString() + " ";
				cmdArg += loc.getName() + " ";
				cmdArg += type.getInternalName() + " ";
				cmdArg += cond.toString();
				try {
					sendSimpleCommand(CommandIdentifier.STRATEGY,
							StrategySubcommandIdentifier.CONSTRAINT_MAP, cmdArg);
				}
				catch (UnexpectedResponseException e) {
					LOGGER.warning("Caught an error during the initialisation "
							+ "of LazyOTF:\n" + e
							+ "\n- presumed to be non-fatal.");
				}
			}

			@Override
			public DecisionStrategyHandle installDecisionStrategy(
					MappingStrategy ds, Set<Location> domain) {
				return installDecisionStrategy((DecisionStrategy) ds, domain);
			}

			@Override
			public DecisionStrategyHandle installDecisionStrategy(
					SatStrategy ds, Set<Location> domain) {
				return installDecisionStrategy((DecisionStrategy) ds, domain);
			}
		};
	}

	/**
	 * @return a new {@link Path2WeightControlInterface} communicating with the
	 *         LazyOTF backend.
	 */
	private Path2WeightControlInterface getPath2WeightControlInterface() {
		return new Path2WeightControlInterface() {
			@Override
			public void setEnabled(Path2Weight it, boolean enabledness) {
				if (cfg.getEnabledPath2Weights().contains(it)) {
					// permanently active Path2Weight, ignore call
					return;
				}

				if (enabledness) {
					try {
						addPath2Weight(it);
					}
					catch (InactiveException | IllegalArgumentException e) {
						LOGGER.log(Level.WARNING, "Warning: unable to add Path2Weight: " + e);
					}
				}
				else {
					try {
						removePath2Weight(it);
					}
					catch (IllegalArgumentException e) {
						LOGGER.log(Level.WARNING, "Warning: unable to remove Path2Weight: "
								+ e);
					}
				}
			}

			@Override
			public boolean isEnabled(Path2Weight it) {
				return isInstalled(it) && path2WeightIDs.get(it) != NULL_HANDLE;
			}

			@Override
			public boolean isInstalled(Path2Weight it) {
				return path2WeightIDs.containsKey(it);
			}

			@Override
			public void aggregate(Collection<Path2Weight> path2Weights,
					AggregationMode aggMode) {
				RemoteLazyOTF.this.aggregate(path2Weights, aggMode);
			}
		};
	}

	/**
	 * @param s
	 *            The label, as a string.
	 * @return A Label represented by the given string.
	 */
	private static Label makeLabel(String s) {
		assert (s != null);

		return new LibLabel(s);
	}

	@Override
	public LazyOTFConfigurationOptions getConfigurationOptions() throws InactiveException {
		if (!isActive()) {
			throw new InactiveException();
		}

		List<String> visitorNames = queryForStringList(CommandIdentifier.GET,
				GetIdentifier.LIST_OF_VISITORS);
		List<String> path2WeightNames = queryForStringList(CommandIdentifier.GET,
				GetIdentifier.LIST_OF_PATH2WEIGHTS);
		List<String> edgeWeightAggregatorNames = queryForStringList(CommandIdentifier.GET,
				GetIdentifier.LIST_OF_TEST_TREE_WEIGHERS);
		List<String> confPath2WeightNames = queryForStringList(CommandIdentifier.GET,
				GetIdentifier.CONFIGURABLE_PATH2WEIGHTS);
		List<String> strategies = queryForStringList(CommandIdentifier.GET,
				GetIdentifier.LIST_OF_STRATEGIES);
		String defaultVisitor;
		String defaultTreeWeigher;
		String defaultPath2Weight;

		defaultVisitor = queryForString(CommandIdentifier.GET, GetIdentifier.DEFAULT_VISITOR);
		defaultTreeWeigher = queryForString(CommandIdentifier.GET,
				GetIdentifier.DEFAULT_TEST_TREE_WEIGHER);
		defaultPath2Weight = queryForString(CommandIdentifier.GET,
				GetIdentifier.DEFAULT_PATH2WEIGHT);

		List<Path2Weight> path2Weights = new LinkedList<Path2Weight>();
		for (String gm : path2WeightNames) {
			path2Weights.add(SimplePath2Weight.newInstance(gm, gm));
		}

		List<Path2Weight> confPath2Weights = new LinkedList<Path2Weight>();
		for (String cgm : confPath2WeightNames) {
			confPath2Weights.add(CustomPath2WeightImpl.newInstance(cgm, cgm));
		}

		List<TraversalMethod> traversalMethods = new LinkedList<>();
		for (String v : visitorNames) {
			traversalMethods.add(new StringIdentifiedEntity(v));
		}

		List<TestTree2Weight> tree2weights = new LinkedList<>();
		for (String v : edgeWeightAggregatorNames) {
			tree2weights.add(new StringIdentifiedEntity(v));
		}

		LazyOTFConfigurationOptions o = new LazyOTFConfigurationOptions(
				traversalMethods,
				tree2weights,
				path2Weights,
				defaultVisitor,
				defaultTreeWeigher,
				defaultPath2Weight,
				confPath2Weights,
				strategies);
		return o;
	}

	/**
	 * Set the "activeness" of this instance. The instance becomes inactive
	 * again when setting it inactive or when an irrecoverable internal error
	 * occurs.
	 * 
	 * @param activeness
	 *            If {@link true}, LazyOTF is activated.
	 * 
	 */
	 private void setActive(boolean activeness) {
		 isActive = activeness;
	 }

	 @Override
	 public void setInactive() {
		 setActive(false);
	 }

	 @Override
	 public boolean isActive() {
		 return isActive;
	 }

	 @Override
	 public void setInhibitNondeterministicTestGoals(boolean inhibit) {
		 LOGGER.log(Level.FINE, "Attempting to  set inhibit nondet testgoals to " + inhibit);

		 sendSimpleCommand(CommandIdentifier.INHIBIT_NONDET_TESTGOALS,
				 (inhibit ? EnableDisableCommandIdentifier.ENABLE : EnableDisableCommandIdentifier.DISABLE).toString().toLowerCase());        
	 }

	 private void setCycleWarningsEnabled(int threshold) {
		 LOGGER.log(Level.FINE, "Attempting to enable cycle warnings with a threshold of " + threshold);

		 sendSimpleCommand(CommandIdentifier.SET_CYCLE_WARNING_THRESHOLD, "" + threshold);
	 }

	 private void setCycleWarningsDisabled() {
		 LOGGER.log(Level.FINE, "Attempting to disable cycle warnings");
		 sendSimpleCommand(CommandIdentifier.SET_CYCLE_WARNING_THRESHOLD, "0");
	 }

	 @Override
	 public boolean hasDetectedACycle() {
		 if (cfg.isCycleDetectionEnabled()) {
			 LOGGER.log(Level.FINE, "Attempting to query cycle warnings");
			 return queryForBoolean(CommandIdentifier.GET_CYCLE_WARNING);
		 }
		 else {
			 return false;
		 }
	 }

	 /**
	  * @param errorMsg
	  *            The implementation error to be reported.
	  */
	 private void reportImplementationError(String errorMsg) {
		 setInactive();
		 LOGGER.severe("*** LazyOTF-JTorX implementation error");
		 LOGGER.severe(errorMsg);
		 throw new IllegalStateException(errorMsg);
	 }

	 /**
	  * Send a message to the LazyOTF instance. This method automatically
	  * attaches the "LazyOTF:" prefix to the message. This method currently only
	  * supports single-line messages.
	  * 
	  * @param msg
	  *            The message to be sent.
	  */
	 public void sendMessageToLazyOTFBackend(String msg) {
		 LOGGER.log(Level.FINE, "Sending message to backend: " + msg);

		 if (msg == null) {
			 throw new IllegalArgumentException("msg must not be null");
		 }

		 proposalCache = null;

		 String[] parts = msg.split(" ", 2);

		 LinkedList<String> queryArgs = new LinkedList<String>();
		 if (parts.length == 2) {
			 queryArgs.add(parts[1]);
		 }

		 this.sendSimpleCommand(parts[0], (parts.length == 2) ? parts[1] : "", true);
	 }

	 /** Caches test tree nodes until the tree is abandoned. */
	 private final Map<Integer, TestTreeNode> testTreeNodeCache = new HashMap<Integer, TestTreeNode>();

	 /** Caches the current test tree root node. */
	 private TestTreeNode rootNodeCache = null;

	 @Override
	 public TestTreeNode getTestTreeNode(Integer nodeID) throws InactiveException {
		 if (testTreeNodeCache.containsKey(nodeID)) {
			 return testTreeNodeCache.get(nodeID);
		 }
		 else if (nodeID == -1) {
			 if (rootNodeCache == null) {
				 rootNodeCache = loadTestTreeNode(nodeID, null);
			 }
			 return rootNodeCache;
		 }
		 else {
			 return null;
		 }
	 }

	 /**
	  * Retrieves information about a test tree node.
	  * 
	  * @param nodeID
	  *            The requested node's ID (-1 for current root)
	  * @param parent
	  *            The requested node's parent ID (may be null)
	  * @return A TestTreeNode instance containing information about the
	  *         requested test tree node.
	  * @throws InactiveException
	  */
	 private TestTreeNode loadTestTreeNode(Integer nodeID, TestTreeNode parent)
			 throws InactiveException {
		 if (testTreeNodeCache.containsKey(nodeID)) {
			 return testTreeNodeCache.get(nodeID);
		 }
		 else {
			 try {
				 TestTreeNode newTestTreeNode = new TestTreeNodeImpl(this, parent, nodeID);
				 if (nodeID != -1) {
					 testTreeNodeCache.put(nodeID, newTestTreeNode);
				 }
				 return newTestTreeNode;
			 }
			 catch (IOException e) {
				 isActive = false;
				 throw new InactiveException("Lost connection to the underlying LazyOTF instance.");
			 }
			 catch (LazyOTFException e) {
				 reportImplementationError("Unable to query LazyOTF about node " + nodeID + "\n"
						 + e.toString());
				 return null;
			 }
		 }
	 }

	 /*
	  * @Override public Observable getEventManager() { return this.eventMgr; }
	  */

	 @Override
	 public LazyOTFInstancePort getInstancePort() {
		 return new LazyOTFInstancePort() {
			 @Override
			 public void send(String cmd) {
				 try {
					 sendMessageToLazyOTFBackend(cmd.trim());
				 }
				 catch (UnexpectedResponseException e) {
					 /* ignore */
				 }
			 }
		 };
	 }

	 @Override
	 public boolean hasFreshTestTree() {
		 return hasFreshTestTree && isActive();
	 }

	 /**
	  * Send a command to the LazyOTF instance, expecting a boolean-typed
	  * response but dropping it.
	  * 
	  * @param cmd
	  *            The command identifier.
	  * @param arg
	  *            Argument to the command.
	  */
	 private void sendSimpleCommand(CommandIdentifier cmd, String arg) {
		 sendSimpleCommand(cmd.toString(), arg, false);
	 }

	 /**
	  * Send a command to the LazyOTF instance, expecting a boolean-typed
	  * response but dropping it.
	  * 
	  * @param cmd
	  *            The command identifier.
	  * @param subcmd
	  *            A subcommand identifier.
	  * @param arg
	  *            An argument to the command.
	  */
	 private void sendSimpleCommand(CommandIdentifier cmd, Enum<?> subcmd, String arg) {
		 sendSimpleCommand(cmd.toString(), subcmd.toString() + " " + arg, false);
	 }

	 /**
	  * Send a command to the LazyOTF instance, expecting a boolean-typed or any
	  * response but dropping it.
	  * 
	  * @param cmd
	  *            The name of the command to be sent.
	  * @param arg
	  *            Argument to the command.
	  * @param acceptAnyResponse
	  *            If false, only boolean-type responses are accepted; if true,
	  *            all answers are accepted.
	  */
	 private void sendSimpleCommand(String cmd, String arg, boolean acceptAnyResponse) {
		 try {
			 synchronized (commMgr.getSyncObject()) {
				 commandCommh.sendCommand(cmd, arg);
				 if (acceptAnyResponse) {
					 responseCommh.receiveAndDrop();
				 }
				 else {
					 responseCommh.receiveSimpleBoolean();
				 }
			 }
		 }
		 finally {
			 commMgr.onMessageExchanged();
		 }
	 }

	 /**
	  * Send a command to the LazyOTF instance, receiving a list of string
	  * afterwards.
	  * 
	  * @param cmd
	  *            The name of the command to be sent.
	  * @param arg
	  *            Argument to the command.
	  * @return A list of strings received from LazyOTF.
	  */
	 private List<String> queryForStringList(CommandIdentifier cmd, String arg) {
		 List<String> result;
		 try {
			 synchronized (commMgr.getSyncObject()) {
				 commandCommh.sendCommand(cmd.toString(), arg);
				 result = responseCommh.receiveListOfStrings();
			 }
		 }
		 finally {
			 commMgr.onMessageExchanged();
		 }
		 return result;
	 }

	 /**
	  * Send a command to the LazyOTF instance, expecting to receive a test tree
	  * node afterwards.
	  * 
	  * @param cmd
	  *            The name of the command to be sent.
	  * @param subcmd
	  *            A subcommand identifier.
	  * @param arg
	  *            Argument to the command.
	  * @return The received test tree node.
	  */
	 private TestTreeNodeDescription queryForTestTreeNodeDescription(CommandIdentifier cmd,
			 Enum<?> subcmd, String arg) {
		 TestTreeNodeDescription result;
		 try {
			 synchronized (commMgr.getSyncObject()) {
				 commandCommh.sendCommand(cmd.toString(), subcmd.toString() + " " + arg);
				 result = responseCommh.receiveTestTreeNodeDescription();
			 }
		 }
		 finally {
			 commMgr.onMessageExchanged();
		 }
		 return result;
	 }

	 /**
	  * Send a command to the LazyOTF instance, receiving a list of string
	  * afterwards.
	  * 
	  * @param cmd
	  *            The name of the command to be sent.
	  * @return A list of strings received from LazyOTF.
	  */
	 private List<String> queryForStringList(CommandIdentifier cmd) {
		 return queryForStringList(cmd, "");
	 }

	 /**
	  * Send a command to the LazyOTF instance, receiving a list of string
	  * afterwards.
	  * 
	  * @param cmd
	  *            The name of the command to be sent.
	  * @param subCmd
	  *            The subcommand.
	  * @return A list of strings received from LazyOTF.
	  */
	 private List<String> queryForStringList(CommandIdentifier cmd, Enum<?> subCmd) {
		 return queryForStringList(cmd, subCmd.toString());
	 }

	 /**
	  * Send a command to the LazyOTF instance, receiving string afterwards.
	  * 
	  * @param cmd
	  *            The name of the command to be sent.
	  * @param subcmd
	  *            The subcommand.
	  * @return The string received from LazyOTF.
	  */
	 private String queryForString(CommandIdentifier cmd, Enum<?> subcmd) {
		 return queryForString(cmd, subcmd.toString());
	 }

	 /**
	  * Send a command to the LazyOTF instance, receiving string afterwards.
	  * 
	  * @param cmd
	  *            The name of the command to be sent.
	  * @param subcmd
	  *            The subcommand.
	  * @param arg
	  *            The argument to the command.
	  * @return The string received from LazyOTF.
	  */
	 private String queryForString(CommandIdentifier cmd, Enum<?> subcmd, String arg) {
		 return queryForString(cmd, subcmd.toString() + " " + arg);
	 }

	 /**
	  * Send a command to the LazyOTF instance, receiving string afterwards.
	  * 
	  * @param cmd
	  *            The name of the command to be sent.
	  * @param arg
	  *            The argument to the command.
	  * @return The string received from LazyOTF.
	  */
	 private String queryForString(CommandIdentifier cmd, String arg) {
		 String result;
		 try {
			 synchronized (commMgr.getSyncObject()) {
				 commandCommh.sendCommand(cmd.toString(), arg);
				 result = responseCommh.receiveSimpleString();
			 }
		 }
		 finally {
			 commMgr.onMessageExchanged();
		 }

		 return result;
	 }

	 private boolean queryForBoolean(CommandIdentifier cmd) {
		 boolean result;
		 try {
			 synchronized (commMgr.getSyncObject()) {
				 commandCommh.sendCommand(cmd.toString());
				 result = responseCommh.receiveSimpleBoolean();
			 }
		 }
		 finally {
			 commMgr.onMessageExchanged();
		 }

		 return result;    	
	 }

	 /**
	  * The number of the next message expected to be received from LazyOTF. This
	  * counter serves to maintain the receive-order of messages.
	  */
	 private int nextMessageSequenceNumber = 0;

	 /**
	  * Empty the message log sending the appropriate messages along the way.
	  * This function does nothing if no listeners are attached to the event
	  * manager.
	  */
	 public void flushMessageLog() {
		 if (eventMgr.hasObservers()) {
			 LinkedList<Message> removedHere = new LinkedList<Message>();
			 synchronized (messageLog) {
				 while (!messageLog.isEmpty()) {
					 Message m = messageLog.pop();
					 m.setSequenceNumber(nextMessageSequenceNumber++);
					 removedHere.addFirst(m);
				 }
			 }
			 for (Message m : removedHere) {
				 onMessageExchanged(m);
			 }
		 }
	 }

	 /**
	  * Notifies the observers that a message has been exchanged.
	  * 
	  * @param m
	  *            The exchanged message.
	  */
	 private void onMessageExchanged(Message m) {
		 eventMgr.notifyObservers(new LazyOTFEvents.MessageExchangedEvent(m));
	 }

	 /**
	  * @return An accessor to various TorX inspection methods implemented via
	  *         LazyOTF.
	  */
	 public TorXInterfaceExtensions getTorXInterfaceExtensions() {
		 return new TorXInterfaceExtensionsImpl(commMgr);
	 }

	 /**
	  * LazyOTF exception superclass.
	  */
	 @SuppressWarnings("serial")
	 public class LazyOTFException extends Exception {
		 /** The LazyOTF message causing the error */
		 private final Message errorMessage;

		 public LazyOTFException(Message errorMessage) {
			 this.errorMessage = errorMessage;
		 }

		 public Message getErrorMessage() {
			 return errorMessage;
		 }
	 }

	 /**
	  * Exception indicating that the LazyOTF instance was not ready.
	  */
	 @SuppressWarnings("serial")
	 public class LazyOTFNotReadyException extends LazyOTFException {
		 public LazyOTFNotReadyException(Message errorMessage) {
			 super(errorMessage);
		 }

		 @Override
		 public String toString() {
			 return "LazyOTF/JtorX: Not ready";
		 }
	 }

	 /**
	  * Exception indicating that the test tree was left via a leaf node.
	  */
	 @SuppressWarnings("serial")
	 public class LazyOTFIteratorExhaustedException extends LazyOTFException {

		 public LazyOTFIteratorExhaustedException(Message errorMessage) {
			 super(errorMessage);
		 }

		 @Override
		 public String toString() {
			 return "LazyOTF/JtorX: Test tree exhausted";
		 }
	 }

	 /**
	  * A class representing LazyOTF test tree nodes.
	  */
	 public final class TestTreeNodeImpl implements TestTreeNode {
		 /** The LazyOTF instance where this node originates. */
		 private final RemoteLazyOTF lazyOTFInstance;

		 /** This node's ID. */
		 private final int nodeID;

		 /** IDs of this node's children. */
		 private final Set<Integer> childIDs;

		 /** The destination states' names. */
		 private final Set<InstantiatedLocation> destinations;

		 /** This node's label. */
		 private final Label label;

		 /** This node's parent node. */
		 private final TestTreeNode parent;

		 /** This node's weight assigned by LazyOTF. */
		 private final int weight;

		 /** This node's strategy evaluation results. */
		 private final Map<InstantiatedLocation, Map<DecisionStrategyHandle, TestType>> strategyEval;

		 /** Set of all test goal states contained within this node. */
		 private final Set<InstantiatedLocation> testGoalStates;

		 /**
		  * Set of all inducing (but not test goal) states contained within this
		  * node.
		  */
		 private final Set<InstantiatedLocation> inducingStates;

		 /**
		  * A map from uninstantiated locations to their instantiated
		  * counterparts.
		  */
		 private final Map<Location, Set<InstantiatedLocation>> locationMap;

		 /** List of instantiated children (serves as a lookup cache). */
		 private List<TestTreeNode> children = null;

		 /**
		  * Constructs a TestTreeNodeImpl instance.
		  * 
		  * @param lazyOTFInstance
		  *            The associated LazyOTF instance.
		  * @param parent
		  *            This node's parent node; may be null.
		  * @param nodeID
		  *            This node's ID.
		  * @throws InactiveException
		  * @throws IOException
		  * @throws LazyOTFException
		  */
		 private TestTreeNodeImpl(RemoteLazyOTF lazyOTFInstance, TestTreeNode parent, int nodeID)
				 throws InactiveException, IOException, LazyOTFException {
			 this(queryForTestTreeNodeDescription(
					 CommandIdentifier.GET, GetIdentifier.NODE_DESCRIPTION, nodeID + ""), parent,
					 nodeID);
		 }

		 public TestTreeNodeImpl(TestTreeNodeDescription nodeDescription, TestTreeNode parent,
				 int nodeID) {
			 LOGGER.log(Level.FINE, "Setup for node ID " + nodeID);

			 if (nodeID < -1) {
				 throw new IllegalArgumentException("nodeID must be >= -1");
			 }

			 lazyOTFInstance = RemoteLazyOTF.this;
			 this.nodeID = nodeID;
			 this.parent = parent;

			 childIDs = new HashSet<Integer>();
			 destinations = new HashSet<InstantiatedLocation>();
			 strategyEval = new HashMap<InstantiatedLocation, Map<DecisionStrategyHandle, TestType>>();

			 testGoalStates = new HashSet<InstantiatedLocation>();
			 inducingStates = new HashSet<InstantiatedLocation>();

			 locationMap = new HashMap<Location, Set<InstantiatedLocation>>();

			 label = new LibLabel(nodeDescription.getIncomingEdgeName());
			 LOGGER.log(Level.FINE, "Name: " + label.getString());

			 weight = nodeDescription.getWeight();
			 LOGGER.log(Level.FINE, "Weight: " + weight);

			 /* Read child node IDs line */

			 try {
				 for (String id : nodeDescription.getSubnodeIDs()) {
					 childIDs.add(Integer.parseInt(id));
					 LOGGER.log(Level.FINE, "Child ID: " + weight);
				 }
			 }
			 catch (NumberFormatException e) {
				 reportImplementationError("Invalid LazyOTF response to 'get node-description':"
						 + nodeDescription.toString());
			 }

			 // locationValuations = new HashMap<Location, Map<String,
			 // Map<String, String>>>();

			 int idCounter = 0;
			 for (InstantiatedLocationDescription ild : nodeDescription.getLocationDescriptions()) {
				 String currentID = "L" + nodeID + "." + (idCounter++);
				 Location loc = getLocationManager().getLocation(ild.getName());

				 LOGGER.log(Level.FINE, "Destination node: " + ild.getName());

				 /* Compute the valuation map */
				 Map<Variable, Term> valuationMap = new HashMap<Variable, Term>();
				 for (String variableName : ild.getValuation().getVariableNames()) {
					 String value = ild.getValuation().getValuation(variableName);
					 LOGGER.log(Level.FINE, "Val: " + ild.getName() + "." + variableName + "="
							 + value);
					 Variable v = new LocationVariable(variableName, "undefined");
					 Term t = new LocationVariableTerm(value, v);
					 valuationMap.put(v, t);
				 }

				 /* Get strategy results for this location */
				 Map<DecisionStrategyHandle, TestType> localStratResults;
				 localStratResults = new HashMap<DecisionStrategyHandle, TestType>();
				 TestType instLocTestType = TestType.ORDINARY;

				 for (String strategyID : ild.getStrategyResults().keySet()) {
					 TestType tt = TestType.getTestTypeByName(ild.getStrategyResults().get(
							 strategyID));
					 localStratResults.put(installedStrategies.get(strategyID), tt);
					 if (tt.compareTo(instLocTestType) > 0) {
						 instLocTestType = tt;
					 }
				 }

				 TestTypeAssigner tta = locationMgr.getTestTypeAssigner();
				 if (tta.getMaxConstantTestType(loc).compareTo(instLocTestType) > 0) {
					 instLocTestType = tta.getMaxConstantTestType(loc);
				 }

				 InstantiatedLocation instLoc = new InstantiatedLocation(loc, instLocTestType,
						 valuationMap, currentID);
				 strategyEval.put(instLoc, localStratResults);
				 destinations.add(instLoc);

				 if (locationMap.get(loc) == null) {
					 locationMap.put(loc, new HashSet<InstantiatedLocation>());
				 }

				 locationMap.get(loc).add(instLoc);
				 if (instLoc.getTestType().getInternalName().equals("TEST_GOAL")) {
					 if (!(nodeDescription.getLocationDescriptions().size() > 1
							 && cfg.isInhibitingNondetTestGoals())) {       
						 testGoalStates.add(instLoc);
					 }
				 }
				 else if (instLoc.getTestType().getInternalName().equals("INDUCING")) {
					 inducingStates.add(instLoc);
				 }
			 }

			 LOGGER.log(Level.FINE, "Done setting up node " + nodeID);
		 }

		 @Override
		 public List<TestTreeNode> getChildren() throws InactiveException {
			 if (children != null) {
				 return children;
			 }
			 else {
				 List<TestTreeNode> result = new ArrayList<TestTreeNode>();
				 try {
					 for (Integer s : childIDs) {
						 result.add(lazyOTFInstance.loadTestTreeNode(s, this));
					 }
				 }
				 catch (InactiveException e) {
					 return null;
				 }
				 children = result;
				 return result;
			 }
		 }

		 @Override
		 public boolean hasChildren() {
			 return !childIDs.isEmpty();
		 }

		 @Override
		 public Set<InstantiatedLocation> getLocations() {
			 return destinations;
		 }

		 @Override
		 public Set<InstantiatedLocation> getLocations(Location location) {
			 if (locationMap.get(location) == null) {
				 return new HashSet<InstantiatedLocation>();
			 }
			 return locationMap.get(location);
		 }

		 @Override
		 public Label getLabel() {
			 return label;
		 }

		 @Override
		 public TestTreeNode getParent() {
			 return parent;
		 }

		 @Override
		 public int getWeight() {
			 return weight;
		 }

		 @Override
		 public int getNodeID() {
			 return nodeID;
		 }

		 @Override
		 public Map<DecisionStrategyHandle, TestType> getNonOrdinaryStrategyResults(
				 InstantiatedLocation loc) {
			 return strategyEval.get(loc);
		 }

		 @Override
		 public Set<InstantiatedLocation> getLocationsHavingTestType(TestType type) {
			 if (type.getInternalName().equals("TEST_GOAL")) {
				 return testGoalStates;
			 }
			 else if (type.getInternalName().equals("INDUCING")) {
				 return inducingStates;
			 }
			 else {
				 Set<InstantiatedLocation> result = new HashSet<InstantiatedLocation>(destinations);
				 result.removeAll(testGoalStates);
				 result.removeAll(inducingStates);
				 return result;
			 }
		 }

		 @Override
		 public Set<InstantiatedLocation> getTestGoals() {
			 return testGoalStates;
		 }

		 @Override
		 public Set<InstantiatedLocation> getInducingStates() {
			 return inducingStates;
		 }

		 @Override
		 public String toString() {
			 String result = "Node ID: " + nodeID;
			 result += " Incoming edge name: " + label.getString() + "\n";

			 result += " Destination locations: ";
			 for (InstantiatedLocation l : destinations) {
				 result += l.getLocation().getName() + " ";
			 }
			 result += "\n";

			 result += " Test goals: ";
			 for (InstantiatedLocation l : testGoalStates) {
				 result += l.getLocation().getName() + " ";
			 }
			 result += "\n";

			 result += " Inducing states: ";
			 for (InstantiatedLocation l : inducingStates) {
				 result += l.getLocation().getName() + " ";
			 }
			 result += "\n";

			 result += " Valuation:\n";
			 for (InstantiatedLocation l : destinations) {
				 for (Variable var : l.getLocationVariables()) {
					 result += "  " + l.getLocation().getName() + ":" + l.getID() + "."
							 + var.getName() + "=";
					 result += l.getValue(var).toString() + "\n";
				 }
			 }

			 return result;
		 }
	 }

	 @Override
	 public TraversalDepthComputer getTraversalDepthComputer() {
		 return traversalDepthComp;
	 }

	 @Override
	 public void addObserver(Observer observer) {
		 eventMgr.addObserver(observer);
	 }

	 @Override
	 public void cleanUp() {
		 getLocationManager().cleanUp();
	 }

	 protected Deque<Message> getCommunicationLogBuffer() {
		 return this.messageLog;
	 }
}
