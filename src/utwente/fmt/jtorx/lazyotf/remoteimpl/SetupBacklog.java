package utwente.fmt.jtorx.lazyotf.remoteimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightControlInterface.AggregationMode;

/**
 * A backlog for add* actions.
 */
class SetupBacklog {
    /** The backlogged {@link Path2Weight}s. */
    private final List<Path2Weight> path2WeightBacklog;

    /** The backlogged node value weighers. */
    private final List<NodeValue2Weight> nodeValue2WeightBacklog;
    
    /** The backlogged aggregations. */
    private final Map<Collection<Path2Weight>, AggregationMode> aggregationBacklog;

    /**
     * Constructs a new {@link SetupBacklog} instance.
     */
    public SetupBacklog() {
        path2WeightBacklog = new ArrayList<>();
        nodeValue2WeightBacklog = new ArrayList<>();
        aggregationBacklog = new HashMap<>();
    }

    /**
     * Adds the given item to the backlog.
     * 
     * @param it
     *            The {@link Path2Weight} to be added.
     */
    public void add(Path2Weight it) {
        path2WeightBacklog.add(it);
    }

    /**
     * Adds the given item to the backlog.
     * 
     * @param it
     *            The {@link NodeValue2Weight} to be added.
     */
    public void add(NodeValue2Weight it) {
        nodeValue2WeightBacklog.add(it);
    }

    /**
     * Adds the given {@link Path2Weight}s and {@link AggregationMode} to the
     * Path2Weight aggregation backlog.
     * @param aggArgs The aggregation argument.
     * @param mode The aggregation mode.
     */
    public void addAggregation(Collection<Path2Weight> aggArgs, AggregationMode mode) {
        aggregationBacklog.put(aggArgs, mode);
    }

    /**
     * Gets the backlogged {@link Path2Weight}s.
     * 
     * @return A {@link List} of backlogged {@link Path2Weight}s.
     */
    public List<Path2Weight> getBackloggedPath2Weights() {
        return path2WeightBacklog;
    }

    /**
     * Gets the backlogged {@link NodeValue2Weight}s.
     * 
     * @return A {@link List} of backlogged {@link NodeValue2Weight}s.
     */
    public List<NodeValue2Weight> getBackloggedNodeValue2Weights() {
        return nodeValue2WeightBacklog;
    }

    /**
     * Gets the backlogged {@link Path2Weight} aggregations.
     * 
     * @return A {@link Map} associating collections of {@link Path2Weight}
     *      with the targed {@link AggregationMode}.
     */
    public Map<Collection<Path2Weight>, AggregationMode> getBackloggedPath2WeightAggregations() {
        return aggregationBacklog;
    }

    /**
     * Clears the backlog.
     */
    public void clear() {
        this.path2WeightBacklog.clear();
        this.nodeValue2WeightBacklog.clear();
    }
}
