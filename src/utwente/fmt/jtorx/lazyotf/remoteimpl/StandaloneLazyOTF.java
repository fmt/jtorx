package utwente.fmt.jtorx.lazyotf.remoteimpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTFPolicyFactory;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.utils.ChildStarter;

/**
 * An interface to the "bare" LazyOTF-JTorX instance usable for the
 * determination of admissible configuration values etc.
 */
public class StandaloneLazyOTF {
    /** The class logger. */
    private static final Logger LOGGER = Logger.getLogger(StandaloneLazyOTF.class.getName());
    
    /** A ChildStarter governing the console process. */
    private final ChildStarter lazyOTFConsole;

    /** The LazyOTF instance associated with the console. */
    private LazyOTF lazyOTFConsoleInstance;

    /** The command used to start the console. */
    private final String[] lazyOTFcmd = { "java", "-classpath", "SymToSim.jar:lib/sigar-bin/lib/sigar.jar",
            "symtosim.lazyotf.console.LazyOTFConsole" };

    /** The insance's input stream. */
    private final BufferedReader instanceReader;

    /** The LazyOTF policy factory for the backend. */
    private final LazyOTFPolicyFactory lotfPolicy;
    
    boolean isQuit = false;
    
    private static final int CHILDSTARTER_MAX_RETRIES = 10;
    private static final long CHILDSTARTER_RETRY_DELAY = 2000;

    /**
     * Constructs a new {@link StandaloneLazyOTF} instance using the given
     * LazyOTF policy factory.
     * 
     * @param lotfPolicy
     *            A {@link LazyOTFPolicyFactory} used to initialize the internal
     *            LazyOTF instance. This policy factory does not need to have a
     *            valid configuration.
     */
    public StandaloneLazyOTF(LazyOTFPolicyFactory lotfPolicy) {
        LOGGER.log(Level.FINE, "Setting up new standalone LazyOTF instance... ");
        this.lotfPolicy = lotfPolicy;
        boolean running = false;
        ChildStarter cs = null;
        for (int i = 0; i < CHILDSTARTER_MAX_RETRIES && !running; i++) {
            try {
                cs = new ChildStarter("LazyOTF console", new LazyOTFErrorReporter(),
                        lazyOTFcmd, ".", new LazyOTFProgressReporter(), new LazyOTFAppendable());
                cs.run();
                try {
                    Thread.sleep(2000);
                }
                catch (InterruptedException e) {
                    LOGGER.warning("Thread sleep interrupted");
                }
                running = cs.isRunning() && cs.getIn() != null && cs.getOut() != null;
            }
            catch (Exception e) {
                LOGGER.warning("Got childstarter exception " + e);
                e.printStackTrace();
                LOGGER.warning("Retries left: " + (CHILDSTARTER_MAX_RETRIES - i));
                try {
                    Thread.sleep(CHILDSTARTER_RETRY_DELAY);
                }
                catch (InterruptedException e1) {
                    LOGGER.warning("Thread sleep interrupted");
                }
            }
        }
        
        if (!running) {
            LOGGER.severe("Fatal: Aborting: Could not start child!");
            throw new RuntimeException("Fatal: Aborting: Could not start standalone LazyOTF!");
        }
        
        lazyOTFConsole = cs;
        if (lazyOTFConsole.isRunning()) {
            instanceReader = new BufferedReader(lazyOTFConsole.getOut());
            LOGGER.log(Level.FINE, "done.");
        }
        else {
            LOGGER.log(Level.SEVERE, "Setting up the standalone LazyOTF instance failed.");
            instanceReader = null;
        }
    }

    /**
     * @return the used LazyOTF instance.
     */
    public LazyOTF getLazyOTFInstance() {
        return lazyOTFConsoleInstance;
    }

    /**
     * Reset the LazyOTF console and read a STS file.
     * 
     * @param modelFileName
     *            The STS's .sax file.
     * @return true iff the reset/read operation succeeded.
     * @throws ModelNotFoundException
     *             when the model STS file could not be opened.
     */
    public boolean resetAndRead(String modelFileName) throws ModelNotFoundException {
        LOGGER.log(Level.FINE, "Attempting to reset and read " + modelFileName + "... ");

        if (isQuit) {
            LOGGER.log(Level.SEVERE, "Attemted to reset and read after quit()");
            throw new IllegalStateException();
        }
        
        File modelFile = new File(modelFileName);
        if (!modelFile.exists() || modelFile.isDirectory()) {
            throw new ModelNotFoundException();
        }

        if (lazyOTFConsole.isRunning()) {
            String cmd = "reset-and-read " + modelFileName + "\n";
            try {
                String s;
                synchronized (lazyOTFConsole.getIn()) {
                    lazyOTFConsole.getIn().write(cmd.getBytes());
                    lazyOTFConsole.getIn().flush();
                    s = "";
                    while (s != null && s.trim().equals("")) {
                        s = instanceReader.readLine();
                    }
                }
                if (s == null || !s.equals("okay")) {
                    lazyOTFConsoleInstance = null;
                    LOGGER.log(Level.WARNING, "Reset/Read: got response " + s);
                    throw new ModelNotFoundException();
                }
                LOGGER.log(Level.FINE, "Done.");
                lazyOTFConsoleInstance = new RemoteLazyOTF(instanceReader, lazyOTFConsole.getIn(),
                        lotfPolicy, false);
                return true;
            }
            catch (IOException e) {
                LOGGER.log(Level.FINE, "Failed.");
                lazyOTFConsoleInstance = null;
                return false;
            }
        }
        else {
            LOGGER.log(Level.SEVERE, "Failed: Backend not running.");
            return false;
        }
    }

    /**
     * @return A LazyOTF instance without a model tied to it. Only a small
     *         subset of commands is available in that instance; Use with
     *         caution.
     */
    public LazyOTF makeModellessInstance() {
        if (isQuit) {
            LOGGER.log(Level.SEVERE, "Attemted to reset and read after quit()");
            throw new IllegalStateException();
        }
        
        if (lazyOTFConsole.isRunning()) {
            lazyOTFConsoleInstance = new RemoteLazyOTF(instanceReader, lazyOTFConsole.getIn(),
                    lotfPolicy, true);
            return lazyOTFConsoleInstance;

        }
        else {
            lazyOTFConsoleInstance = null;
            return null;
        }
    }

    /**
     * Abandon the LazyOTF console instance.
     */
    public void quit() {
        LOGGER.log(Level.FINE, "Abandoning a standalone LazyOTF instance.");

        if (isQuit) {
            LOGGER.log(Level.WARNING, "quit() has already been called on this");
            return;
        }
        
        try {
            if (lazyOTFConsoleInstance != null) {
                lazyOTFConsoleInstance.cleanUp();
            }

            try {
                synchronized (lazyOTFConsole.getIn()) {
                    lazyOTFConsole.getIn().write("quit".getBytes());
                    lazyOTFConsole.getIn().flush();
                }
                lazyOTFConsole.destroy(0);
            }
            catch (IOException e) {

            }
        }
        finally {
            lazyOTFConsoleInstance = null;
            isQuit = true;
        }
    }

    /**
     * Exception class for the event that the model could not be loaded.
     */
    @SuppressWarnings("serial")
    public class ModelNotFoundException extends Exception {
    }

    /* Helper classes */
    private class LazyOTFErrorReporter implements ErrorReporter {
        @Override
        public void endOfTest(String msg) {
        }

        @Override
        public void log(long timeStamp, String src, String msg) {

        }

        @Override
        public void report(String s) {
        }
    }

    private class LazyOTFProgressReporter implements ProgressReporter {
        @Override
        public void reset() {
        }

        @Override
        public void ready() {
        }

        @Override
        public void stop() {
        }

        @Override
        public void setActivity(String s) {
        }

        @Override
        public void setMax(int m) {
        }

        @Override
        public void setLevel(int l) {
        }

        @Override
        public void startUndeterminate() {
        }

        @Override
        public void stopUndeterminate() {
        }
    }

    private class LazyOTFAppendable implements Appendable {
        @Override
        public java.lang.Appendable append(java.lang.CharSequence arg0) throws java.io.IOException {
            return this;
        }

        @Override
        public java.lang.Appendable append(java.lang.CharSequence arg0, int arg1, int arg2)
                throws java.io.IOException {
            return this;
        }

        @Override
        public java.lang.Appendable append(char arg0) throws java.io.IOException {
            return this;
        }
    }
}
