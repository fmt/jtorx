package utwente.fmt.jtorx.lazyotf.remoteimpl;

public enum StrategySubcommandIdentifier {
    ADD,
    CONSTRAINT_MAP,
    DISABLE,
    ENABLE,
    REMOVE,
    SIMPLE_MAP;

    @Override
    public String toString() {
        return TokenNameTranslator.encode(super.toString());
    }
}
