package utwente.fmt.jtorx.lazyotf.remoteimpl;

import utwente.fmt.jtorx.lazyotf.TestTree2Weight;
import utwente.fmt.jtorx.lazyotf.TraversalMethod;

/**
 * A class representing simple string representations of entities.
 */
public class StringIdentifiedEntity implements TraversalMethod, TestTree2Weight {
    /** The entity's name. */
    private String name;

    /**
     * Constructs a new {@link StringIdentifiedEntity} instance.
     * 
     * @param name
     *            The instance's name.
     */
    public StringIdentifiedEntity(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void readFromString(String s) {
        name = s;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Creates a new {@link StringIdentifiedEntity} instance.
     * 
     * @param name
     *            The instance's name.
     * @return The new instance.
     */
    public static StringIdentifiedEntity newInstance(String name) {
        return new StringIdentifiedEntity(name);
    }
}
