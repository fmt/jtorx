package utwente.fmt.jtorx.lazyotf.remoteimpl;

public class TokenNameTranslator {
	public static String decode(String token) {
		return token.replace('-', '_').toUpperCase();
	}

	public static String encode(String token) {
		return token.replace('_', '-').toLowerCase();
	}
}
