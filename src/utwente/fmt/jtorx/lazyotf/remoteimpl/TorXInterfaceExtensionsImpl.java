package utwente.fmt.jtorx.lazyotf.remoteimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.InstantiatedLocationDescription;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.QueryMgr;

/**
 * An implementation of {@link TorXInterfaceExtensions} for
 * {@link RemoteLazyOTF}.
 */
public class TorXInterfaceExtensionsImpl implements TorXInterfaceExtensions {
    /**
     * The class logger.
     */
    private static final Logger LOGGER = Logger.getLogger(TorXInterfaceExtensionsImpl.class
            .getName());

    /**
     * The query communication manager talking to the LazyOTF backend.
     */
    private final QueryMgr commMgr;

    /**
     * Constructs a new {@link TorXInterfaceExtensionsImpl} instance.
     * 
     * @param commMgr
     *            The query communication manager talking to the LazyOTF
     *            backend.
     */
    protected TorXInterfaceExtensionsImpl(QueryMgr commMgr) {
        this.commMgr = commMgr;
    }

    @Override
    public Map<String, String> getValuation(String stateID) {
        LOGGER.log(Level.FINE, "Getting valuation for " + stateID);

        List<InstantiatedLocationDescription> result;
        synchronized (commMgr.getSyncObject()) {
            commMgr.getQueryHandler().sendCommand("torx-get", "valuation-of " + stateID);
            result = commMgr.getResponseHandler().receiveListOfLocationDescriptions();
        }
        commMgr.onMessageExchanged();

        LOGGER.log(Level.FINE, "Got valuation for " + stateID);

        Map<String, String> resultMap = new HashMap<String, String>();

        int idCtr = 0;
        for (InstantiatedLocationDescription ild : result) {
            String name = "T" + ild.getName() + "." + (idCtr++);
            LOGGER.log(Level.FINE, name);
            for (String varname : ild.getValuation().getVariableNames()) {
                resultMap.put(varname, ild.getValuation().getValuation(varname));
            }
        }
        return resultMap;
    }
}
