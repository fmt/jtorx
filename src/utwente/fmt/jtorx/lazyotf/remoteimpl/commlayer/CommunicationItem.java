package utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer;

/**
 * A representation class for messages exchanged between JTorX and LazyOTF.
 * 
 * @param <T>
 *            The message type.
 */
public class CommunicationItem<T> {
    /** The exchanged message. */
    private final T exchangedData;

    /** The exchanged message's string representation. */
    private final String stringRepresentation;

    /** true iff this represents an error message. */
    private final boolean isError;

    /** A pretty printer for the exchanged message. */
    private final PrettyPrinter<T> prettyPrinter;

    /** Cached pretty-printed version of the exchanged message. */
    private String prettyPrinterCache;

    /**
     * Construct a new CommunicationItem instance (using a default pretty
     * printer).
     * 
     * @param exchangedData
     *            The exchanged message.
     * @param stringRepresentation
     *            The exchanged message's string representation.
     * @param isError
     *            true iff the exchanged message was an error message.
     */
    public CommunicationItem(T exchangedData, String stringRepresentation, boolean isError) {
        this(exchangedData, stringRepresentation, isError, null);
    }

    /**
     * Construct a new CommunicationItem instance.
     * 
     * @param exchangedData
     *            The exchanged message.
     * @param stringRepresentation
     *            The exchanged message's string representation.
     * @param isError
     *            true iff the exchanged message was an error message.
     * @param prettyPrinter
     *            The pretty printer to be used for the message.
     */
    public CommunicationItem(T exchangedData, String stringRepresentation, boolean isError,
            PrettyPrinter<T> prettyPrinter) {
        this.exchangedData = exchangedData;
        this.stringRepresentation = stringRepresentation;
        this.isError = isError;
        if (prettyPrinter != null) {
            this.prettyPrinter = prettyPrinter;
        }
        else {
            /* Make default (identity) pretty printer */
            this.prettyPrinter = new PrettyPrinter<T>() {
                @Override
                public String prettyPrint(T msg) {
                    return msg.toString();
                }
            };
        }
        this.prettyPrinterCache = null;
    }

    /**
     * @return The exchanged message.
     */
    public T getExchangedData() {
        return this.exchangedData;
    }

    /**
     * @return true iff this represents an error message.
     */
    public boolean isError() {
        return this.isError;
    }

    @Override
    public String toString() {
        return this.stringRepresentation;
    }

    /**
     * @return A pretty-printed string representation of the exchanged message.
     */
    public String toPrettyString() {
        if (prettyPrinterCache == null) {
            prettyPrinterCache = this.prettyPrinter.prettyPrint(exchangedData);
        }
        return prettyPrinterCache;
    }

    /**
     * Message Pretty-Printer interface for user display.
     * @param <T> The message type.
     */
    public interface PrettyPrinter<T> {
        /**
         * @param msg The raw message.
         * @return The pretty-printed message, as a String.
         */
        String prettyPrint(T msg);
    }
}
