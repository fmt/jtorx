package utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Observable;

/**
 * A communication handler responsible for the "old-fashioned" way of
 * formulating queries.
 */
public class DeprecatedQueryCommunicationHandler implements QueryCommunicationHandler {

    private final OutputStream sink;
    private final EventManager eventManager;
    private final boolean debug = false;

    public DeprecatedQueryCommunicationHandler(OutputStream out) {
        sink = out;
        eventManager = new EventManager();
    }

    @Override
    public void sendCommand(String cmd) {
        sendCommand(cmd, "");
    }

    @Override
    public void sendCommand(String cmd, String args) {
        try {
            String tbw = "LazyOTF:" + cmd + " " + args + "\n";
            if (debug) {
                System.err.print("Deprecated Query Comm Handler: Sending " + tbw);
            }
            sink.write(tbw.getBytes());
            sink.flush();
            eventManager.onSendMessage(new CommunicationItem<String>(tbw, tbw, false));
        }
        catch (IOException e) {
            throw new IllegalStateException("Lost connection to SymToSim.");
        }
    }

    @Override
    public Observable getEventManager() {
        return eventManager;
    }

    private static class EventManager extends Observable {
        public void onSendMessage(CommunicationItem<String> msg) {
            setChanged();
            this.notifyObservers(msg);
        }
    }
}
