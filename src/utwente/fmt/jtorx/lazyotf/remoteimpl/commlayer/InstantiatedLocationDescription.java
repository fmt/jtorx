package utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer;

import java.util.Map;

/**
 * A communication-layer representation for instantiated locations.
 */
public class InstantiatedLocationDescription {
    /** The location's name. */
    public final String name;

    /** The location's valuation. */
    public final LocationValuationDescription valuation;

    /** The location's decision strategy evaluation results. */
    public final Map<String, String> decisionStrategyResults;

    /**
     * Construct a new InstantiatedLocationDescription instance.
     * 
     * @param name
     *            The location's name.
     * @param strategyEval
     *            The strategy evaluation map (ID -> TestTypeName)
     * @param valuation
     *            The location's valuation.
     */
    public InstantiatedLocationDescription(String name, LocationValuationDescription valuation,
            Map<String, String> strategyEval) {
        this.name = name;
        this.valuation = valuation;
        decisionStrategyResults = strategyEval;
    }

    /**
     * @return The location's name.
     */
    public String getName() {
        return name;
    }

    /**
     * @return The location's valuation.
     */
    public LocationValuationDescription getValuation() {
        return valuation;
    }

    /**
     * @return The location's decision strategy evaluation results.
     */
    public Map<String, String> getStrategyResults() {
        return decisionStrategyResults;
    }
}
