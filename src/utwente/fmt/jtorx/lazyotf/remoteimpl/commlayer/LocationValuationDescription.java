package utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer;

import java.util.Map;
import java.util.Set;

/**
 * A communication-layer representation of a location valuation.
 */
public class LocationValuationDescription {
    /** A map from variable names to their variable's type. */
    private final Map<String, String> typeMap;

    /** A map from variable names to their variable's value. */
    private final Map<String, String> valuationMap;

    /**
     * Construct a new LocationValuationDescription instance.
     * 
     * @param typeMap
     *            A map from variable names to their variable's type.
     * @param valuationMap
     *            A map from variable names to their variable's value.
     */
    public LocationValuationDescription(Map<String, String> typeMap,
            Map<String, String> valuationMap) {
        this.typeMap = typeMap;
        this.valuationMap = valuationMap;
    }

    /**
     * @return The location's variable names.
     */
    public Set<String> getVariableNames() {
        return valuationMap.keySet();
    }

    /**
     * @param variableName
     *            The name of the variable whose value is requested.
     * @return The value of the variable associated with the given variable
     *         name.
     */
    public String getValuation(String variableName) {
        return valuationMap.get(variableName);
    }

    /**
     * @param variableName
     *            The name of the variable whose type is requested.
     * @return The type of the variable associated with the given variable name.
     */
    public String getType(String variableName) {
        return typeMap.get(variableName);
    }
}
