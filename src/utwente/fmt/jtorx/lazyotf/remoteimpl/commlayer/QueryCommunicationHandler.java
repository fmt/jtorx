package utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer;

import java.util.Observable;

/**
 * A communication handler suitable for sending commands to the LazyOTF backend.
 */
public interface QueryCommunicationHandler {
    /**
     * Send a command to the LazyOTF backend.
     * 
     * @param cmd
     *            The string representation of the (parameterless) command.
     */
    void sendCommand(String cmd);

    /**
     * Send a command to the LazyOTF backend, passing along a parameter.
     * 
     * @param cmd
     *            The string representation of the command.
     * @param args
     *            The string representation of the command's arguments.
     */
    void sendCommand(String cmd, String args);

    /**
     * @return An Observable notifying its observers about the communication
     *         which passes through this communication handler.
     */
    Observable getEventManager();
}
