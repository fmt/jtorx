package utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer;

import java.util.Observable;

/**
 * Manager class used for keeping track of communication handlers and channel
 * synchronization objects.
 */
public interface QueryMgr {
    /**
     * @return the response communication handler associated with this query
     *         manager.
     */
    ResponseCommunicationHandler getResponseHandler();

    /**
     * @return the query communication handler associated with this query
     *         manager.
     */
    QueryCommunicationHandler getQueryHandler();

    /**
     * @return the communication channel's synchronisation object. Use this
     *         object to lock the channel if you want to do queries.
     */
    Object getSyncObject();

    /**
     * @return an Observable notifying its observers about the usage of the
     *         query/response handlers.
     */
    Observable getEventManager();

    /**
     * Notify this class's event manager listeners about calls to the query/
     * response handlers.
     */
    void onMessageExchanged();
}
