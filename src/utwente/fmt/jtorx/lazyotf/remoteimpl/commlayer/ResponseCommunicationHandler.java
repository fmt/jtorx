package utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer;

import java.util.List;
import java.util.Observable;

/**
 * A communication handler responsible for the LazyOTF->JTorX direction of
 * communication.
 */
public interface ResponseCommunicationHandler {
    /**
     * Receive a data packet from LazyOTF and interpret it as a test tree node
     * description.
     * 
     * @return The test tree node description obtained from LazyOTF.
     */
    TestTreeNodeDescription receiveTestTreeNodeDescription();

    /**
     * Send a data packet to JTorX containing the given test tree node
     * description.
     * 
     * @param it
     *            The test tree node description to be sent to JTorX.
     */
    void sendTestTreeNodeDescription(TestTreeNodeDescription it);

    /**
     * Receive a data packet from LazyOTF and interpret it as a list of
     * instantiated location descriptions.
     * 
     * @return The list of instantiated location descriptions obtained from
     *         LazyOTF.
     */
    List<InstantiatedLocationDescription> receiveListOfLocationDescriptions();

    /**
     * Send a data packet to JTorX containing the given list of instantiated
     * location descriptions.
     * 
     * @param it
     *            The list of instantiated location descriptions to be sent.
     */
    void sendLocationDescriptions(List<InstantiatedLocationDescription> it);

    /**
     * Receive a data packet from LazyOTF and interpret it as a list of strings.
     * 
     * @return The list of strings obtained from LazyOTF.
     */
    List<String> receiveListOfStrings();

    /**
     * Receive a data packet from LazyOTF and do not interpret it.
     */
    void receiveAndDrop();

    /**
     * Send a data packet to JTorX containing the given list of strings.
     * 
     * @param it
     *            The list of strings to be sent.
     */
    void sendListOfStrings(List<String> it);

    /**
     * Receive a data packet from LazyOTF and interpret it as a string.
     * 
     * @return The string obtained from LazyOTF.
     */
    String receiveSimpleString();

    /**
     * Send a data packet to JTorX containing a string.
     * 
     * @param it
     *            The string to be sent.
     */
    void sendSimpleString(String it);

    /**
     * Receive a data packet from LazyOTF and interpret it as a Boolean.
     * 
     * @return The Boolean obtained from LazyOTF.
     */
    Boolean receiveSimpleBoolean();

    /**
     * Send a data packet to JTorX containing a Boolean.
     * 
     * @param it
     *            The Boolean to be sent.
     */
    void sendSimpleBoolean(Boolean it);

    /**
     * Send a data packet to JTorX containing an error message.
     * 
     * @param errorMsg
     *            The error message to be sent.
     */
    void sendErrorMessage(String errorMsg);

    /**
     * Receive a data packet from LazyOTF and interpret it as an error message.
     * 
     * @return The error message obtained from LazyOTF.
     */
    String receiveErrorMessage();

    /**
     * @return an Observable notifying its observers about data received via
     *         this communication handler (after calls to receive*()).
     */
    Observable getEventManager();

    @SuppressWarnings("serial")
    public class ChannelErrorException extends RuntimeException {

    }

    @SuppressWarnings("serial")
    public class FormatErrorException extends RuntimeException {
        public FormatErrorException(String msg) {
            super(msg);
        }
    }

    @SuppressWarnings("serial")
    public class UnexpectedResponseException extends RuntimeException {
        private boolean isErrorMessage = false;

        public UnexpectedResponseException(String msg, boolean isErrorMessage) {
            super(msg);
            this.isErrorMessage = isErrorMessage;
        }

        public boolean isErrorMessage() {
            return isErrorMessage;
        }
    }
}
