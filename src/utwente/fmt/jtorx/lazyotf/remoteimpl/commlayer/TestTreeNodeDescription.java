package utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer;

import java.util.List;

/**
 * A communication-layer representation of test tree nodes.
 */
public class TestTreeNodeDescription {
    /** String representation of the incoming edge. */
    private final String incomingEdgeName;

    /** String representation of the node's test type. */
    private final String testType;

    /** The node's weight. */
    private final int weight;

    /**
     * List containing information about the node's instantiated location
     * descriptions.
     */
    private final List<InstantiatedLocationDescription> locations;

    /** List containing the node's child IDs. */
    private final List<String> subnodeIDs;

    /** String representation of the node's own ID. */
    private final String ownID;

    /** A Map from strategy IDs to their respective test type results. */
    // private final Map<String, String> strategyResults;

    /**
     * Construct a new TestTreeNodeDescription instance.
     * 
     * @param incomingEdgeName
     *            String representation of the incoming edge.
     * @param testType
     *            String representation of the node's test type.
     * @param weight
     *            The node's weight.
     * @param locations
     *            List containing information about the node's instantiated
     *            location descriptions.
     * @param subnodeIDs
     *            List containing the node's child IDs.
     * @param ownID
     *            String representation of the node's own ID.
     */
    public TestTreeNodeDescription(String incomingEdgeName, String testType, int weight,
            List<InstantiatedLocationDescription> locations, List<String> subnodeIDs, String ownID) {
        this.incomingEdgeName = incomingEdgeName;
        this.testType = testType;
        this.weight = weight;
        this.locations = locations;
        this.subnodeIDs = subnodeIDs;
        this.ownID = ownID;
        // this.strategyResults = strategyResults;
    }

    /**
     * @return The incoming edge's name.
     */
    public String getIncomingEdgeName() {
        return incomingEdgeName;
    }

    /**
     * @return The name of the node's test type.
     */
    public String getTestTypeName() {
        return testType;
    }

    /**
     * @return The node's weight.
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @return A list of information about the instantiated locations contained
     *         in this node.
     */
    public List<InstantiatedLocationDescription> getLocationDescriptions() {
        return locations;
    }

    /**
     * @return A list of the child IDs associated with this node.
     */
    public List<String> getSubnodeIDs() {
        return subnodeIDs;
    }

    /**
     * @return This node's ID.
     */
    public String getID() {
        return ownID;
    }

    /**
     * @return A Map from strategy IDs to their respective test type results.
     */
    /*
     * public Map<String, String> getStrategyResults() { return
     * this.strategyResults; }
     */
}
