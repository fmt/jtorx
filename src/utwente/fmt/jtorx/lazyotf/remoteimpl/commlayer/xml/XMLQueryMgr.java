package utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml;

import java.io.BufferedReader;
import java.io.OutputStream;
import java.util.Observable;

import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.DeprecatedQueryCommunicationHandler;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.QueryCommunicationHandler;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.QueryMgr;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.ResponseCommunicationHandler;

/**
 * Communication manager for XML-based communication between JTorX and LazyOTF.
 * These objects keep track of communication handlers and synchronisation
 * objects for their communication channel.
 */
public class XMLQueryMgr implements QueryMgr {
    private final XMLResponseCommunicationHandler responseHandler;
    private final QueryCommunicationHandler queryHandler;
    private final Object syncObj;
    private final EventManager eventManager;

    public enum EventKind {
        MESSAGE_EXCHANGED;
    }

    public XMLQueryMgr(BufferedReader source, OutputStream sink, Object syncObject) {
        responseHandler = new XMLResponseCommunicationHandler(source, sink);
        queryHandler = new DeprecatedQueryCommunicationHandler(sink);
        syncObj = syncObject;
        eventManager = new EventManager();
    }

    @Override
    public ResponseCommunicationHandler getResponseHandler() {
        return responseHandler;
    }

    @Override
    public QueryCommunicationHandler getQueryHandler() {
        return queryHandler;
    }

    @Override
    public Object getSyncObject() {
        return syncObj;
    }

    @Override
    public void onMessageExchanged() {
        eventManager.onMessageExchanged();
    }

    @Override
    public Observable getEventManager() {
        return eventManager;
    }

    private static class EventManager extends Observable {

        public void onMessageExchanged() {
            setChanged();
            notifyObservers(EventKind.MESSAGE_EXCHANGED);
        }
    }
}
