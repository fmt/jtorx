package utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml;

/* 2.1 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.CommunicationItem;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.InstantiatedLocationDescription;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.LocationValuationDescription;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.ResponseCommunicationHandler;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.TestTreeNodeDescription;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.LocationList;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.NdLocation;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.NdLocations;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.NdStrategyResult;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.NdStrategyResults;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.NdSubnodes;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.NdValuation;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.NdVariable;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.NodeDescription;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.ObjectFactory;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.TorxLocation;
import utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.XmlResponse;

/**
 * A communication handler taking care of the LazyOTF->JTorX communication based
 * on XML.
 */
public class XMLResponseCommunicationHandler implements
        ResponseCommunicationHandler {
    /** This communication handler's input stream. */
    private final BufferedReader source;

    /** This communication handler's output stream. */
    private final OutputStream sink;

    /** An JAXB XML unmarshaller suitable for LazyOTF responses. */
    private final Unmarshaller unmarshaller;

    /** An JAXB XML marshaller suitable for LazyOTF responses. */
    private final Marshaller marshaller;

    /** An JAXB XML object factory. */
    private final ObjectFactory xmlFactory;

    /** This instance's event manager. */
    private final EventManager eventManager;

    /**
     * true => the instances of this class are allowed to print debug messages.
     */
    private final Boolean debug = false;

    /**
     * Pretty printer for {@link CommunicationItem}s.
     */
    private final CommunicationItem.PrettyPrinter<XmlResponse> pprinterResponse;

    /**
     * Construct a new XML response communication handler.
     * 
     * @param source
     *            An input stream, source of XML data.
     * @param sink
     *            An output stream, sink of XML data.
     */
    public XMLResponseCommunicationHandler(BufferedReader source,
            OutputStream sink) {
        this.sink = sink;
        this.source = source;
        eventManager = new EventManager();
        xmlFactory = new utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.ObjectFactory();
        try {
            JAXBContext jaxbContext = JAXBContext
                    .newInstance("utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response");
            unmarshaller = jaxbContext.createUnmarshaller();
            marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                    Boolean.TRUE);
        }
        catch (JAXBException e) {
            e.printStackTrace();
            throw new IllegalStateException("Unable to create JAXB context");
        }

        pprinterResponse = new CommunicationItem.PrettyPrinter<XmlResponse>() {
            @Override
            public String prettyPrint(XmlResponse msg) {
                StringWriter sw = new StringWriter();
                try {
                    marshaller.marshal(msg, sw);
                }
                catch (JAXBException e) {
                    e.printStackTrace();
                    throw new IllegalArgumentException(
                            "XML argument could not be marshalled!");
                }
                return sw.toString();
            }
        };
    }

    @Override
    public List<InstantiatedLocationDescription> receiveListOfLocationDescriptions() {
        XmlResponse resp = receive();

        List<InstantiatedLocationDescription> result = new ArrayList<InstantiatedLocationDescription>();

        if (resp.getLocations() != null) {
            List<TorxLocation> tl = resp.getLocations().getLocation();
            if (tl != null) {
                for (TorxLocation loc : tl) {
                    /* TODO: deal with empty valuation */
                    LocationValuationDescription valuation = makeValuationDescription(loc
                            .getValuation());
                    Map<String, String> stratMap = new HashMap<String, String>();
                    result.add(new InstantiatedLocationDescription(loc
                            .getName(), valuation, stratMap));
                }
            }
        }
        else {
            throwUnexpectedMessageException(resp);
        }

        return result;
    }

    @Override
    public List<String> receiveListOfStrings() {
        XmlResponse resp = receive();
        if (resp.getList() != null) {
            return resp.getList().getIt();
        }
        else {
            throwUnexpectedMessageException(resp);
            return null; /* dead */
        }
    }

    @Override
    public Boolean receiveSimpleBoolean() {
        XmlResponse resp = receive();

        if (resp.isSimpleBoolean() != null) {
            return resp.isSimpleBoolean();
        }
        else {
            throwUnexpectedMessageException(resp);
            return null; /* dead */
        }
    }

    @Override
    public String receiveSimpleString() {
        XmlResponse resp = receive();

        if (resp.getSimpleString() != null) {
            return resp.getSimpleString();
        }
        else {
            throwUnexpectedMessageException(resp);
            return null; /* dead */
        }
    }

    @Override
    public TestTreeNodeDescription receiveTestTreeNodeDescription() {
        XmlResponse resp = receive();

        if (resp.getNodeDescription() != null) {
            NodeDescription nd = resp.getNodeDescription();
            List<InstantiatedLocationDescription> locDesc = new ArrayList<InstantiatedLocationDescription>();

            for (NdLocation loc : nd.getLocations().getLocation()) {
                String name = loc.getName();
                LocationValuationDescription lvd = makeValuationDescription(loc
                        .getValuation());
                Map<String, String> strategyEval = new HashMap<String, String>();

                if (loc.getStrategyEval() != null) {
                    for (NdStrategyResult ndr : loc.getStrategyEval()
                            .getStrategy()) {
                        strategyEval.put(ndr.getId(), ndr.getResult());
                    }
                }
                locDesc.add(new InstantiatedLocationDescription(name, lvd,
                        strategyEval));
            }

            /*
             * Map<String, String> strategyResults = new HashMap<String,
             * String>(); for (NdStrategyResult sr :
             * nd.getStrategyResults().getStrategy()) {
             * strategyResults.put(sr.getId(), sr.getResult()); }
             */

            try {
                return new TestTreeNodeDescription(nd.getIncomingEdge(),
                        nd.getType(), Integer.parseInt(nd.getWeight()),
                        locDesc, nd.getSubnodes().getSubnode(), nd.getId());
            }
            catch (NumberFormatException e) {
                throw new FormatErrorException(
                        "Node weights must be parseable as an Integer (got "
                                + nd.getWeight() + ")");
            }
        }
        else {
            throwUnexpectedMessageException(resp);
            return null; /* dead */
        }
    }

    @Override
    public String receiveErrorMessage() {
        XmlResponse resp = receive();
        if (resp.isError()) {
            return resp.getErrormsg();
        }
        else {
            throwUnexpectedMessageException(resp);
            return null; /* dead */
        }
    }

    @Override
    public void receiveAndDrop() {
        receive();
    }

    @Override
    public void sendListOfStrings(List<String> it) {
        XmlResponse resp = xmlFactory.createXmlResponse();
        utwente.fmt.jtorx.lazyotf.remoteimpl.commlayer.xml.response.List theList = xmlFactory
                .createList();
        resp.setList(theList);
        theList.getIt().addAll(it);
        send(resp);
    }

    @Override
    public void sendLocationDescriptions(
            List<InstantiatedLocationDescription> it) {
        XmlResponse resp = xmlFactory.createXmlResponse();
        LocationList locList = xmlFactory.createLocationList();
        resp.setLocations(locList);

        for (InstantiatedLocationDescription ild : it) {
            TorxLocation tl = xmlFactory.createTorxLocation();
            tl.setName(ild.getName());
            tl.setValuation(makeXMLValuationDescription(ild.getValuation()));
            locList.getLocation().add(tl);
        }
        send(resp);
    }

    @Override
    public void sendSimpleBoolean(Boolean it) {
        XmlResponse resp = xmlFactory.createXmlResponse();
        resp.setSimpleBoolean(it);
        send(resp);
    }

    @Override
    public void sendSimpleString(String it) {
        XmlResponse resp = xmlFactory.createXmlResponse();
        resp.setSimpleString(it);
        send(resp);

    }

    @Override
    public void sendErrorMessage(String errorMsg) {
        XmlResponse resp = xmlFactory.createXmlResponse();
        resp.setError(true);
        resp.setErrormsg(errorMsg);
        send(resp);
    }

    @Override
    public void sendTestTreeNodeDescription(TestTreeNodeDescription it) {
        XmlResponse resp = xmlFactory.createXmlResponse();
        NodeDescription nd = xmlFactory.createNodeDescription();
        resp.setNodeDescription(nd);
        nd.setId(it.getID());
        nd.setIncomingEdge(it.getIncomingEdgeName());
        nd.setType(it.getTestTypeName());
        nd.setWeight(it.getWeight() + "");

        NdSubnodes subnodes = xmlFactory.createNdSubnodes();
        subnodes.getSubnode().addAll(it.getSubnodeIDs());
        nd.setSubnodes(subnodes);

        NdLocations locations = xmlFactory.createNdLocations();
        nd.setLocations(locations);

        for (InstantiatedLocationDescription ild : it.getLocationDescriptions()) {
            NdLocation loc = xmlFactory.createNdLocation();
            locations.getLocation().add(loc);
            loc.setName(ild.getName());
            loc.setValuation(makeXMLValuationDescription(ild.getValuation()));

            NdStrategyResults stratResults = xmlFactory
                    .createNdStrategyResults();
            loc.setStrategyEval(stratResults);

            if (ild.getStrategyResults() != null) {
                for (String id : ild.getStrategyResults().keySet()) {
                    NdStrategyResult singleStrat = xmlFactory
                            .createNdStrategyResult();
                    singleStrat.setId(id);
                    singleStrat.setResult(ild.getStrategyResults().get(id));
                    stratResults.getStrategy().add(singleStrat);
                }
            }
        }

        send(resp);
    }

    /**
     * Receive a packet of XML-formatted data from the output stream and return
     * a corresponding XML root object.
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    private XmlResponse receive() {
        if (source == null) {
            throw new IllegalStateException(
                    "Unable to receive without having a source stream!");
        }

        try {
            String line = "";
            try {
                while (!line.contains("</xmlResponse>")) {
                    String newPart = source.readLine();
                    if (newPart == null) {
                        throw new IllegalStateException("Prematurely lost connection to SymToSim.");
                    }

                    if (newPart.trim().equals("") || newPart.equals("\n")) {
                        continue;
                    }

                    line += newPart + "\n";
                }
            }
            catch (IOException e) {
                throw new IllegalStateException("Prematurely lost connection to SymToSim.");
            }

            if (debug) {
                System.err.println("XML subsys: Recvd line " + line);
            }

            StringReader sr = new StringReader(line);
            Object XMLRootObj = unmarshaller.unmarshal(sr);
            XmlResponse result;
            if (XMLRootObj instanceof JAXBElement<?>) {
                JAXBElement<XmlResponse> rootElement;
                rootElement = (JAXBElement<XmlResponse>) XMLRootObj;
                result = rootElement.getValue();
            }
            else if (XMLRootObj instanceof XmlResponse) {
                result = (XmlResponse) XMLRootObj;
            }
            else {
                throw new IllegalStateException(
                        "Internal error: XML root element of unexpected type "
                                + XMLRootObj.getClass().getName());
            }

            CommunicationItem<XmlResponse> commItem;
            commItem = new CommunicationItem<XmlResponse>(result, line,
                    (result.isError() != null) ? result.isError() : false,
                    pprinterResponse);
            eventManager.onReceiveMessage(commItem);
            return result;
        }
        catch (JAXBException e) {
            e.printStackTrace(); /* TODO remove */
            throw new FormatErrorException(e.toString());

        }
    }

    /**
     * Marshal an XML root object and send via the output stream.
     * 
     * @param it
     *            The XML root object to be sent.
     */
    private void send(XmlResponse it) {
        if (sink == null) {
            throw new IllegalStateException(
                    "Unable to send without having a sink stream!");
        }

        try {
            marshaller.marshal(it, sink);
            sink.write('\n');
            sink.flush();
        }
        catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert an XML valuation description to a communication-layer valuation
     * description.
     * 
     * @param val
     *            The XML valuation description to be converted.
     * @return The corresponding communication-layer valuation description.
     */
    private LocationValuationDescription makeValuationDescription(
            NdValuation val) {
        Map<String, String> types = new HashMap<String, String>();
        Map<String, String> values = new HashMap<String, String>();

        for (NdVariable var : val.getVariable()) {
            String key = var.getName();
            types.put(key, var.getType());
            values.put(key, var.getValue());
        }

        return new LocationValuationDescription(types, values);
    }

    /**
     * Convert a commnication-layer valuation description to an XML valuation
     * description.
     * 
     * @param it
     *            The communication-layer valuation description.
     * @return The corresponding XML valuation description.
     */
    private NdValuation makeXMLValuationDescription(
            LocationValuationDescription it) {
        NdValuation result = xmlFactory.createNdValuation();
        for (String variable : it.getVariableNames()) {
            NdVariable nvar = xmlFactory.createNdVariable();
            nvar.setName(variable);
            nvar.setType(it.getType(variable));
            nvar.setValue(it.getValuation(variable));
            result.getVariable().add(nvar);
        }
        return result;
    }

    /**
     * @param b
     *            A Boolean or null
     * @return truee if b is not null and b represents "true".
     */
    private boolean isNotNullAndTrue(Boolean b) {
        return (b != null) ? (b == true) : false;
    }

    /**
     * Throw an UnexpectedResponseException, marking it as an error response
     * when neccessary.
     * 
     * @param it
     *            The XML root element which did not contain the expected data.
     */
    private void throwUnexpectedMessageException(XmlResponse it) {
        if (isNotNullAndTrue(it.isError())) {
            throw new UnexpectedResponseException(it.getErrormsg(), true);
        }
        else {
            throw new UnexpectedResponseException(it.toString(), false);
        }
    }

    @Override
    public Observable getEventManager() {
        return eventManager;
    }

    /**
     * @return true iff this class prints debug messages.
     */
    public Boolean isDebugEnabled() {
        return debug;
    }

    private static class EventManager extends Observable {
        public void onReceiveMessage(CommunicationItem<XmlResponse> msg) {
            setChanged();
            notifyObservers(msg);
        }
    }
}
