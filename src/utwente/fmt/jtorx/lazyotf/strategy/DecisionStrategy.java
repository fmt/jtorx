package utwente.fmt.jtorx.lazyotf.strategy;

import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;

/**
 * Generic decision strategy description class. Can be used to describe
 * arbitrary decision strategies not taking any parameters.
 */
public class DecisionStrategy {
    /** The decision strategy's internal name. */
    private String implementationName;

    /**
     * The decision strategy's "display name".
     */
    private String displayName;

    /**
     * Construct a new DecisionStrategy instance.
     * 
     * @param implementationName
     *            The strategy's internal name.
     */
    public DecisionStrategy(String implementationName) {
        this.implementationName = implementationName;
    }

    /**
     * Construct a new DecisionStrategy instance.
     */
    public DecisionStrategy() {
        implementationName = "DefaultStrategy";
    }

    /**
     * Set the decision strategy's internal name.
     * 
     * @param name
     *            The new name.
     */
    public void setImplementationName(String name) {
        implementationName = name;
    }

    /**
     * @return The decision strategy's internal name.
     */
    public String getImplementationName() {
        return implementationName;
    }

    /**
     * Set the decision strategy's alternative ("display") name.
     * 
     * @param displayName
     *            The new "display" name.
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return The strategy's "display" name.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Use the supplied strategy control interface to install and activate this
     * strategy.
     * 
     * @param sci
     *            The LazyOTF instance's strategy control interface.
     * @param domain
     *            The set of locations for which this strategy may evaluate to
     *            other test types than ORDINARY.
     * @return An instance-specific handle to this decision strategy.
     */
    public DecisionStrategyHandle install(StrategyControlInterface sci, Set<Location> domain) {
        return sci.installDecisionStrategy(this, domain);
    }

    /**
     * Update the LazyOTF instance to reflect all changes made to this strategy.
     * 
     * @param sci
     *            The LazyOTF instance's strategy control interface.
     * @param handle
     *            The instance-specific handle to this decision strategy.
     */
    public void update(StrategyControlInterface sci, DecisionStrategyHandle handle) {

    }

    /**
     * Check if the decision strategy can be applied to a model containing only
     * the given locations and test types.
     * 
     * @param locations
     *            The available locations.
     * @param testTypes
     *            The available test types.
     * @return true iff the decision strategy can be used.
     */
    public boolean isUsableInContext(Set<Location> locations, Set<TestType> testTypes) {
        return true;
    }

    @Override
    public String toString() {
        String result = "{Strategy moniker='" + getDisplayName() + "' strategy-name='"
                + getImplementationName() + "'";
        result += "}";
        return result;
    }

    /**
     * Accepts a {@link DecisionStrategyVisitor} object. This method
     * calls {@link DecisionStrategyVisitor#visit(DecisionStrategy)}
     * with the accepting {@link DecisionStrategy} instance as its
     * argument.
     * @param visitor The {@link DecisionStrategyVisitor} getting
     *         visited.
     */
    public void accept(DecisionStrategyVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public int hashCode() {
        return displayName.hashCode();
    }
}
