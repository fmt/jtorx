package utwente.fmt.jtorx.lazyotf.strategy;

import java.util.ArrayList;
import java.util.List;

/**
 * A factory for {@link DecisionStrategy} objects.
 */
public class DecisionStrategyFactory {
    /**
     * Make a new strategy object of the given name.
     * 
     * @param name
     *            The strategy's name.
     * @return A fresh DecisionStrategy instance of the requested type.
     */
    public DecisionStrategy makeStrategy(String name) {
        if (name.equals("DefaultStrategy")) {
            return makeDefaultStrategy();
        }
        else if (name.equals("MappingStrategy")) {
            return makeMappingStrategy();
        }
        else if (name.equals("SatStrategy")) {
            return makeSatStrategy();
        }
        else {
            throw new IllegalArgumentException("No such strategy: " + name);
        }
    }

    /**
     * @return A fresh DefaultStrategy instance.
     */
    public DecisionStrategy makeDefaultStrategy() {
        return new DecisionStrategy("DefaultStrategy");
    }

    /**
     * @return A fresh MappingStrategy instance.
     */
    public MappingStrategy makeMappingStrategy() {
        return new MappingStrategy();
    }

    /**
     * @return A fresh SatStrategy instance.
     */
    public SatStrategy makeSatStrategy() {
        return new SatStrategy();
    }

    /**
     * @return A list of available DecisionStrategy classes.
     */
    public static List<Class<? extends DecisionStrategy>> getStrategyTypes() {
        ArrayList<Class<? extends DecisionStrategy>> result = new ArrayList<Class<? extends DecisionStrategy>>();

        result.add(MappingStrategy.class);
        result.add(SatStrategy.class);
        result.add(DecisionStrategy.class);

        return result;
    }
}
