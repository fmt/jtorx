package utwente.fmt.jtorx.lazyotf.strategy;

/**
 * A handle class for {@link DecisionStrategy} objects. These handles are used
 * by implementations to identify decision strategies.
 */ 
public class DecisionStrategyHandle {
    /** The actual, low-level handle. */
    private final Object handle;

    /** The decision strategy referenced by this handle. */
    private final DecisionStrategy decisionStrategy;

    /**
     * Construct a new DecisionStrategyHandle instance.
     * 
     * @param it
     *            The low-level handle to be encapsulated.
     * @param referencedStrategy
     *            The strategy to be referenced by this handle.
     */
    public DecisionStrategyHandle(Object it, DecisionStrategy referencedStrategy) {
        this.handle = it;
        this.decisionStrategy = referencedStrategy;
    }

    /**
     * @return The referenced strategy.
     */
    public DecisionStrategy getStrategy() {
        return this.decisionStrategy;
    }

    /**
     * @return The low-level handle.
     */
    public Object getHandle() {
        return this.handle;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other == null || !(other instanceof DecisionStrategyHandle)) {
            return false;
        }
        else {
            DecisionStrategyHandle otherH = (DecisionStrategyHandle) other;
            return otherH.getHandle().equals(getHandle())
                    && otherH.getStrategy() == getStrategy();
        }
    }

    @Override
    public int hashCode() {
        return handle.hashCode();
    }
}
