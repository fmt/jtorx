package utwente.fmt.jtorx.lazyotf.strategy;

/**
 * A visitor interface for {@link DecisionStrategy} and its inherited classes.
 */
public interface DecisionStrategyVisitor {
    /**
     * Visits the given {@link DecisionStrategy}.
     * @param it The visited {@link DecisionStrategy} object.
     */
    void visit(DecisionStrategy it);

    /**
     * Visits the given {@link MappingStrategy}.
     * @param it The visited {@link MappingStrategy} object.
     */
    void visit(MappingStrategy it);

    /**
     * Visits the given {@link SatStrategy}.
     * @param it The visited {@link SatStrategy} object.
     */
    void visit(SatStrategy it);
}
