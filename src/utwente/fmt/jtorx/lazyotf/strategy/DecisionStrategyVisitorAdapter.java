package utwente.fmt.jtorx.lazyotf.strategy;

/**
 * A default implementation of {@link DecisionStrategyVisitor}. This class
 * serves as a convenience class for implementing decision strategy visitors.
 * Unless overridden, its methods have no other effects than calling
 * {@link # otherMethodCalled()}.
 */
public class DecisionStrategyVisitorAdapter implements DecisionStrategyVisitor {
    @Override
    public void visit(DecisionStrategy it) {
        otherMethodCalled(it);
    }

    @Override
    public void visit(MappingStrategy it) {
        otherMethodCalled(it);
    }

    @Override
    public void visit(SatStrategy it) {
        otherMethodCalled(it);
    }

    /**
     * The "default" method, called by non-overridden {@link # visit}
     * methods. Unless overridden, this method has no effect.
     * @param it The visited {@link DecisionStrategy}.
     */
    protected void otherMethodCalled(DecisionStrategy it) {

    }
}
