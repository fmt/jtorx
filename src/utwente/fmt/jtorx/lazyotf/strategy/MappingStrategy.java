package utwente.fmt.jtorx.lazyotf.strategy;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;

/**
 * A decision strategy statically mapping locations to location test types.
 */
public class MappingStrategy extends DecisionStrategy {
    /** The map associating locations with their respective test types. */
    private final Map<Location, TestType> testtypeMap;

    /**
     * Construct a new MappingStrategy instance.
     */
    public MappingStrategy() {
        super("MappingStrategy");
        testtypeMap = new HashMap<Location, TestType>();
    }

    /**
     * Associate the given location with the supplied test type.
     * 
     * @param loc
     *            The location.
     * @param type
     *            The test type.
     */
    public void associate(Location loc, TestType type) {
        testtypeMap.put(loc, type);
    }

    /**
     * Dissociate the given location from this mapping.
     * 
     * @param loc
     *            The location to be dissociated.
     */
    public void dissociate(Location loc) {
        if (testtypeMap.get(loc) == null) {
            throw new IllegalArgumentException("No type associated with " + loc.getName());
        }
        testtypeMap.remove(loc);
    }

    /**
     * @param loc
     *            A Location
     * @return The given location's test type if it is associated with one by
     *         this strategy.
     */
    public TestType getType(Location loc) {
        if (testtypeMap.get(loc) == null) {
            throw new IllegalArgumentException("No type associated with " + loc.getName());
        }
        return testtypeMap.get(loc);
    }

    /**
     * @param loc
     *            A location
     * @return <tt>true</tt> if the strategy assigns a test type to the given
     *         location.
     */
    public boolean isAssigningTestType(Location loc) {
        return testtypeMap.containsKey(loc);
    }

    /**
     * @return The set of locations associated by this strategy with a test
     *         type.
     */
    public Set<Location> getLocations() {
        return testtypeMap.keySet();
    }

    @Override
    public DecisionStrategyHandle install(StrategyControlInterface sci, Set<Location> domain) {
        return sci.installDecisionStrategy(this, domain);
    }

    @Override
    public void update(StrategyControlInterface sci, DecisionStrategyHandle handle) {
        for (Location loc : testtypeMap.keySet()) {
            sci.simpleMap(handle, loc, testtypeMap.get(loc));
        }
    }

    @Override
    public boolean isUsableInContext(Set<Location> locations, Set<TestType> testTypes) {
        for (Location loc : testtypeMap.keySet()) {
            if (!locations.contains(loc) || !testTypes.contains(testtypeMap.get(loc))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void accept(DecisionStrategyVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public int hashCode() {
        return super.hashCode() >> 2 + testtypeMap.hashCode() >> 2;
    }
}
