package utwente.fmt.jtorx.lazyotf.strategy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;

/**
 * A representation for LazyOTF SatStrategy decision strategies.
 */
public class SatStrategy extends DecisionStrategy {
    /**
     * A map associating locations with their conditions. Each test type may be
     * associated with a condition. The highest-priority test type whose
     * associated condition is satisfied by the location's valuation gets
     * assigned to said location.
     */
    private final Map<Location, Map<TestType, Condition>> conditionMap;

    /**
     * Construct a new SatStrategy instance.
     */
    public SatStrategy() {
        super("SatStrategy");
        conditionMap = new HashMap<Location, Map<TestType, Condition>>();
    }

    /**
     * Associate a type/condition pair with the given location.
     * 
     * @param loc
     *            The aforementioned location.
     * @param type
     *            The type to be assigned to the given location iff the
     *            location's valuation satisfies the given condition and there
     *            is no higher-priority test type associated with this location
     *            having a satisfied condition as well.
     * @param condition
     *            A Condition instance. Must be evaluable for the given
     *            location.
     */
    public void associate(Location loc, TestType type, Condition condition) {
        Map<TestType, Condition> locConditionMap = conditionMap.get(loc);
        if (locConditionMap == null) {
            locConditionMap = new HashMap<TestType, Condition>();
            conditionMap.put(loc, locConditionMap);
        }
        locConditionMap.put(type, condition);
    }

    /**
     * Dissociate the given location from the given test type, removing the
     * involved condition as well.
     * 
     * @param loc
     *            A location.
     * @param type
     *            A type associated with that location by this strategy
     *            instance.
     */
    public void dissociate(Location loc, TestType type) {
        Map<TestType, Condition> locConditionMap = conditionMap.get(loc);
        if (locConditionMap == null) {
            throw new IllegalArgumentException("Unknown location " + loc.getName());
        }
        if (locConditionMap.get(type) == null) {
            throw new IllegalArgumentException("No condition associated with "
                    + type.getInternalName() + " for " + loc.getName());
        }
        locConditionMap.remove(type);
        if (locConditionMap.isEmpty()) {
            conditionMap.remove(loc);
        }
    }

    /**
     * @param loc
     *            A location.
     * @param type
     *            A test type associated with the given location by this
     *            strategy instance.
     * @return The condition belonging to the location/test-type association.
     */
    public Condition getCondition(Location loc, TestType type) {
        Map<TestType, Condition> locConditionMap = conditionMap.get(loc);
        if (locConditionMap == null) {
            throw new IllegalArgumentException("Unknown location " + loc.getName());
        }
        if (locConditionMap.get(type) == null) {
            throw new IllegalArgumentException("No condition associated with "
                    + type.getInternalName() + " for " + loc.getName());
        }
        return locConditionMap.get(type);
    }

    /**
     * @param loc
     *            A location.
     * @return The set of test types associated with the given location by this
     *         strategy instance.
     */
    public Set<TestType> getAssociatedTestTypes(Location loc) {
        Map<TestType, Condition> locConditionMap = conditionMap.get(loc);
        if (locConditionMap == null) {
            return new HashSet<TestType>(); // throw new
                                            // IllegalArgumentException("Unknown location "
                                            // + loc.getName());
        }
        return locConditionMap.keySet();
    }

    /**
     * @return The set of locations associated with conditions and test types by
     *         this strategy instance.
     */
    public Set<Location> getLocations() {
        return conditionMap.keySet();
    }

    @Override
    public DecisionStrategyHandle install(StrategyControlInterface sci, Set<Location> domain) {
        return sci.installDecisionStrategy(this, domain);
    }

    @Override
    public void update(StrategyControlInterface sci, DecisionStrategyHandle handle) {
        for (Location loc : getLocations()) {
            for (TestType type : conditionMap.get(loc).keySet()) {
                sci.constraintMap(handle, loc, type, conditionMap.get(loc).get(type));
            }
        }
    }

    @Override
    public boolean isUsableInContext(Set<Location> locations, Set<TestType> testTypes) {
        for (Location loc : getLocations()) {
            for (TestType type : conditionMap.get(loc).keySet()) {
                if (!locations.contains(loc) || !testTypes.contains(type)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void accept(DecisionStrategyVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public int hashCode() {
        return super.hashCode() >> 2 + conditionMap.hashCode() >> 2;
    }

    /**
     * A visitor interface for {@link Condition} objects.
     */
    public interface ConditionVisitor {
        /**
         * Visit the given {@link Condition} object.
         * 
         * @param it
         *            The visited {@link Condition} object.
         */
        void visit(Condition it);

        /**
         * Visit the given {@link DumontCondition} object.
         * 
         * @param it
         *            The visited {@link DumontCondition} object.
         */
        void visit(DumontCondition it);

        /**
         * Visit the given {@link AnyMetaCondition} object.
         * 
         * @param it
         *            The visited {@link AnyMetaCondition} object.
         */
        void visit(AnyMetaCondition it);

        /**
         * Visit the given {@link AllMetaCondition} object.
         * 
         * @param it
         *            The visited {@link AllMetaCondition} object.
         */
        void visit(AllMetaCondition it);

        /**
         * Visit the given {@link MetaCondition} object.
         * 
         * @param it
         *            The visited {@link MetaCondition} object.
         */
        void visit(MetaCondition<?> it);
    }

    /**
     * A convenience default implementation of {@link ConditionVisitor},
     * implementing all methods by calling {@link # otherMethodCalled(Condition)}
     * .
     */
    public static class ConditionVisitorAdapter implements ConditionVisitor {
        @Override
        public void visit(Condition it) {
            otherMethodCalled(it);
        }

        @Override
        public void visit(DumontCondition it) {
            otherMethodCalled(it);
        }

        @Override
        public void visit(AnyMetaCondition it) {
            otherMethodCalled(it);
        }

        @Override
        public void visit(AllMetaCondition it) {
            otherMethodCalled(it);
        }

        @Override
        public void visit(MetaCondition<?> it) {
            otherMethodCalled(it);
        }

        /**
         * The "default" method, called by non-overridden {@link # visit}
         * methods. Unless overridden, this method has no effect.
         * 
         * @param c
         *            The visited {@link Condition}.
         */
        public void otherMethodCalled(Condition c) {
        }
    }

    /**
     * A class representing a location/test-type association condition.
     */
    public abstract static class Condition {
        /** The condition's name. */
        private String name = null;

        /**
         * @return The condition's name.
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the condition's name.
         * 
         * @param name
         *            The new condition name.
         */
        public void setName(String name) {
            this.name = name;
        }

        @Override
        public abstract Condition clone();

        /**
         * Clones the condition into the given condition, e.g. after the
         * operation {@code b.cloneInto(c)}, {@code c.equals(b)} holds and
         * {@code b} does not get modified.
         * 
         * @param c
         *            The target condition.
         */
        protected void cloneInto(Condition c) {
            c.setName(name);
        }

        /**
         * Accept a {@link ConditionVisitor} by calling
         * {@link ConditionVisitor#visit(Condition)}.
         * 
         * @param visitor
         *            The visiting visitor.
         */
        public void accept(ConditionVisitor visitor) {
            visitor.visit(this);
        }
    }

    /**
     * The "Any" meta-condition. Evaluates to {@code true} iff any of its
     * subconditions evaluates to {@code true}.
     */
    public static class AnyMetaCondition extends MetaCondition<Condition> {
        /**
         * Constructs a new {@link AnyMetaCondition} instance.
         * 
         * @param conditions
         *            The collection of the metacondition's subconditions.
         */
        public AnyMetaCondition(Collection<Condition> conditions) {
            super(conditions, "||");
        }

        @Override
        public AnyMetaCondition clone() {
            AnyMetaCondition result = new AnyMetaCondition(getOperands());
            cloneInto(result);
            return result;
        }

        @Override
        public void accept(ConditionVisitor visitor) {
            visitor.visit(this);
        }
    }

    /**
     * The "All" meta-condition. Evaluates to {@code true} iff all of its
     * subconditions evaluate to {@code true}.
     */
    public static class AllMetaCondition extends MetaCondition<Condition> {
        /**
         * Constructs a new {@link AllMetaCondition} instance.
         * 
         * @param conditions
         *            The collection of the metacondition's subconditions.
         */
        public AllMetaCondition(Collection<Condition> conditions) {
            super(conditions, "&&");
        }

        @Override
        public AllMetaCondition clone() {
            AllMetaCondition result = new AllMetaCondition(getOperands());
            cloneInto(result);
            return result;
        }

        @Override
        public void accept(ConditionVisitor visitor) {
            visitor.visit(this);
        }
    }

    /**
     * An abstract class for meta-conditions. This class deals with the
     * management of subconditions; The concrete operation on the subconditions
     * can be implemented by subclassing this class.
     * 
     * @param <T>
     *            The condition type.
     */
    public abstract static class MetaCondition<T> extends Condition {
        /** The metacondition's operands. */
        private final ArrayList<T> operands;

        /** The metacondition's operator. */
        private final String operator;

        /**
         * Constructs a new {@link MetaCondition} object.
         * 
         * @param conditions
         *            The metacondition's subconditions.
         * @param operator
         *            The operator in string representation.
         */
        public MetaCondition(Collection<T> conditions, String operator) {
            this.operands = new ArrayList<T>(conditions);
            this.operator = operator;
        }

        /**
         * @return The metacondition's subconditions.
         */
        public ArrayList<T> getOperands() {
            return this.operands;
        }

        /**
         * Adds a new subcondition to the metacondition list.
         * 
         * @param c
         *            The new subcondition.
         */
        public void addCondition(T c) {
            this.operands.add(c);
        }

        /**
         * Adds a new subcondition to the metacondition list at a given index.
         * 
         * @param index
         *            The subcondition's index.
         * @param c
         *            The new subcondition.
         */
        public void add(int index, T c) {
            this.operands.add(index, c);
        }

        /**
         * Moves a condition within the metacondition list.
         * 
         * @param c
         *            The moved subcondition.
         * @param newIndex
         *            The new index of {@code c}.
         */
        public void moveCondition(T c, int newIndex) {
            this.operands.remove(c);
            this.operands.add(newIndex, c);
        }

        /**
         * Removes a condition from the metacondition.
         * 
         * @param c
         *            The removed metacondition.
         */
        public void removeCondition(Condition c) {
            this.operands.remove(c);
        }

        @Override
        public String toString() {
            String result = "(";
            Iterator<T> acIterator = operands.iterator();
            while (acIterator.hasNext()) {
                result += acIterator.next().toString();
                if (acIterator.hasNext()) {
                    result += " " + operator + " ";
                }
            }
            result += ")";
            return result;
        }

        @Override
        public void accept(ConditionVisitor visitor) {
            visitor.visit(this);
        }

        @Override
        public int hashCode() {
            return operator.hashCode(); // TODO
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof MetaCondition<?>)) {
                return false;
            }
            else {
                MetaCondition<?> mc = (MetaCondition<?>) obj;
                if (!mc.operator.equals(this.operator)
                        || mc.operands.size() != this.operands.size()) {
                    return false;
                }
                else { /* same operands? */
                    Iterator<?> otherOprIt = mc.operands.iterator();
                    Iterator<T> ourOprIt = this.operands.iterator();
                    while (otherOprIt.hasNext()) {
                        if (!otherOprIt.next().equals(ourOprIt.next())) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
    }

    /**
     * A class representing a Dumont condition.
     */
    public static class DumontCondition extends Condition {
        /** The string containing the actual condition. */
        private String condition;

        /**
         * Construct a new DumontCondition instance.
         * 
         * @param dumontCondition
         *            A Dumont condition.
         */
        public DumontCondition(String dumontCondition) {
            condition = dumontCondition;
        }

        /**
         * @return The Dumont condition's string representation.
         */
        @Override
        public String toString() {
            return "(" + condition + ")";
        }

        /**
         * @return The raw Dumont condition.
         */
        public String getCondition() {
            return condition;
        }

        /**
         * Set the Dumont condition's string representation.
         * 
         * @param condition
         *            The new condition.
         */
        public void setCondition(String condition) {
            this.condition = condition;
        }

        @Override
        public DumontCondition clone() {
            DumontCondition result = new DumontCondition(condition);
            cloneInto(result);
            return result;
        }

        @Override
        public void accept(ConditionVisitor visitor) {
            visitor.visit(this);
        }
        
        @Override
        public int hashCode() {
            return condition.hashCode(); // TODO: Bug: condition may change.
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof DumontCondition)) {
                return false;
            }
            else {
                DumontCondition other = (DumontCondition) obj;
                return other.condition.equals(condition);
            }
        }
    }

}
