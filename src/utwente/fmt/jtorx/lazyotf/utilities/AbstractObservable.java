package utwente.fmt.jtorx.lazyotf.utilities;

/**
 * Abstract {@link Observable} implementation with observer counting.
 */
public abstract class AbstractObservable implements Observable {
    
    /** The number of currently installed observers. */
    int observerCount = 0;
    
    @Override
    public void addObserver(Observer o) {
        observerCount++;
    }

    @Override
    public void removeObserver(Observer o) {
        observerCount--;
    }
    
    /**
     * @return {@code true} iff any observers have been registered.
     */
    public boolean hasObservers() {
        return observerCount > 0;
    }

    @Override
    public abstract void notifyObservers(Object o);
}
