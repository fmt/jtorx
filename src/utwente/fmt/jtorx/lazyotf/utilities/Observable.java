package utwente.fmt.jtorx.lazyotf.utilities;

/**
 * <p>
 * An interface for observable classes.
 * </p>
 * 
 * <p>
 * This observer/observable system is built around the idea that any object can
 * become an observer by inheriting from the {@link Observer} marker interface.
 * The "real" observers are one-argument methods marked with a
 * {@link ObserverMethod @ObserverMethod} annotation within the observer.
 * </p>
 * 
 * <p>
 * When calling {@link # notifyObservers(Object)}, the observer methods whose
 * argument has the type of the notification objects are invoked with the given
 * notification object.
 * </p>
 */
public interface Observable {
    /**
     * Adds an {@link Observer} to the observable object.
     * 
     * @param o
     *            The new {@link Observer}.
     */
    void addObserver(Observer o);

    /**
     * Removes an {@link Observer} from the observable object.
     * 
     * @param o
     *            The {@link Observer} which should be removed.
     */
    void removeObserver(Observer o);

    /**
     * Notify the observers, passing them the given notification object.
     * 
     * @param o
     *            The notification object.
     */
    void notifyObservers(Object o);
}
