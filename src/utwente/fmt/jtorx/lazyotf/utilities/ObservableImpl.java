package utwente.fmt.jtorx.lazyotf.utilities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A simple {@link Observable} implementation using reflection to call the
 * observer methods.
 */
public class ObservableImpl extends AbstractObservable {
    /** A map from observer method argument types to the set of respective
     * observer methods.
     */
    private final Map<Class<?>, Set<ObserverObjAndMethod>> observerMap = new HashMap<>();
    
    /**
     * The set of allowed notification object types. This set is also used to
     * restrict the kinds of accepted observer methods. When {@code null}, the
     * according checks are disabled.
     */
    private final Set<Class<?>> notificationTypes;

    /**
     * Constructs a new {@link ObservableImpl} instance. The constructed
     * instance has no restrictions with respect to the domain of
     * notification object types and observer method argument types.
     */
    public ObservableImpl() {
        notificationTypes = null;
    }

    /**
     * Constructs a new {@link ObservableImpl} instance. The constructed
     * instance restricts the set of accepted notification object types
     * and observer method argument types to the classes contained
     * in {@code notificationTypeClasses}. The checks are only performed
     * via assertions.
     * @param notificationTypeClasses The set of allowed notification object
     *         types.
     */
    public ObservableImpl(Class<?>[] notificationTypeClasses) {
        notificationTypes = new HashSet<Class<?>>();
        for (Class<?> c : notificationTypeClasses) {
            notificationTypes.add(c);
        }
    }

    @Override
    public void addObserver(Observer o) {
        for (Method method : o.getClass().getMethods()) {
            if (method.getAnnotation(ObserverMethod.class) != null) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length == 1) {
                    assert notificationTypes == null
                            || notificationTypes.contains(parameterTypes[0]) : "Observer has observation methods whose argument type is not among the notification types";
                    ObserverObjAndMethod om = new ObserverObjAndMethod(o, method);
                    addObserverToMap(parameterTypes[0], om);
                }
                else {
                    throw new IllegalArgumentException("Observers must have exactly one argument");
                }
            }
        }
        super.addObserver(o);
    }

    @Override
    public void removeObserver(Observer o) {
        for (Class<?> methodType : observerMap.keySet()) {
            observerMap.get(methodType).remove(o);
        }
        super.removeObserver(o);
    }

    @Override
    public void notifyObservers(Object message) {
        Class<?> messageType = message.getClass();
        assert notificationTypes == null || notificationTypes.contains(messageType) : "Notification argument is not a notification type.";
        if (observerMap.containsKey(messageType)) {
            for (ObserverObjAndMethod om : observerMap.get(messageType)) {
                om.invoke(message);
            }
        }
    }

    /**
     * Adds an observer to the method-type/observer-method map.
     * @param methodType The observer method's argument type.
     * @param method The observer method object.
     */
    private void addObserverToMap(Class<?> methodType, ObserverObjAndMethod method) {
        if (!observerMap.containsKey(methodType)) {
            observerMap.put(methodType, new HashSet<ObserverObjAndMethod>());
        }
        observerMap.get(methodType).add(method);
    }

    /**
     * A representation of an observer method along with the object it
     * originates from.
     */
    private class ObserverObjAndMethod {
        /** The observer method. */
        private final Method m;
        
        /** The observer. */
        private final Object origin;

        /**
         * Constructs a new {@link ObserverObjAndMethod} instance.
         * @param originObj The object providing the observer method.
         * @param m The observer method.
         */
        public ObserverObjAndMethod(Object originObj, Method m) {
            origin = originObj;
            this.m = m;
        }

        /**
         * Invokes the observer method, passing the given notification object.
         * @param o The notification object.
         */
        public void invoke(Object o) {
            try {
                synchronized (m) {
                    boolean methodIsAccessible = m.isAccessible();
                    if (!methodIsAccessible) {
                        m.setAccessible(true);
                    }
                    m.invoke(origin, o);
                    if (!methodIsAccessible) {
                        m.setAccessible(false);
                    }
                }

            }
            catch (IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
