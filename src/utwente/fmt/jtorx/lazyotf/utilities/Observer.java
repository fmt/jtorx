package utwente.fmt.jtorx.lazyotf.utilities;

/**
 * The observer marker interface. This interface only serves to improve
 * code readability.
 */
public interface Observer {

}
