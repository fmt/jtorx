package utwente.fmt.jtorx.lazyotf.utilities;

import java.util.EnumMap;
import java.util.Map;

/**
 * An {@link Observable} implementation categorizing observers by an enum type.
 * 
 * @param <T>
 *            The categorization enum type.
 */
public class ObserverCategorizingObservable<T extends Enum<T>> extends AbstractObservable {
    /**
     * A map associating categories and sub-observables. Each category has its
     * own {@link ObservableImpl} object dealing with the corresponding
     * observers.
     */
    private final Map<T, ObservableImpl> observerMap;

    /**
     * The default category used for observers added via
     * {@link # addObserver(Observer)}.
     */
    private final T defaultCategory;

    /**
     * The set of accepted notification object types, see
     * {@link ObservableImpl#notificationTypes}.
     */
    private final Class<?>[] notificationTypes;

    /**
     * Constructs a new {@link ObserverCategorizingObservable} instance.
     * 
     * @param defaultCategory
     *            The default category, used for observers added without
     *            specifying an observer category.
     */
    public ObserverCategorizingObservable(T defaultCategory) {
        this(defaultCategory, null);
    }

    /**
     * Constructs a new {@link ObserverCategorizingObservable} instance.
     * 
     * @param defaultCategory
     *            The default category, used for observers added without
     *            specifying an observer category.
     * @param notificationTypes
     *            The set of accepted notification object types (see
     *            {@link ObservableImpl#ObservableImpl(Class[])}).
     */
    public ObserverCategorizingObservable(T defaultCategory, Class<?>[] notificationTypes) {
        this.defaultCategory = defaultCategory;
        observerMap = new EnumMap<T, ObservableImpl>(defaultCategory.getDeclaringClass());
        this.notificationTypes = notificationTypes;
    }

    /**
     * Adds an observer to the default observer category.
     * 
     * @param o
     *            The observer.
     */
    @Override
    public void addObserver(Observer o) {
        addObserver(o, defaultCategory);
    }

    /**
     * Adds an observer to the given observer category.
     * 
     * @param o
     *            The added observer.
     * @param category
     *            The observer's category.
     */
    public void addObserver(Observer o, T category) {
        if (!observerMap.containsKey(category)) {
            observerMap.put(category, new ObservableImpl(notificationTypes));
        }
        observerMap.get(category).addObserver(o);
        super.addObserver(o);
    }

    @Override
    public void notifyObservers(Object message) {
        for (T priority : observerMap.keySet()) {
            notifyObservers(message, priority);
        }
    }

    /**
     * Notifies the observers associated with the given observer category.
     * 
     * @param message
     *            The notification object.
     * @param category
     *            The notification category.
     */
    public void notifyObservers(Object message, T category) {
        if (observerMap.containsKey(category)) {
            observerMap.get(category).notifyObservers(message);
        }
    }

    @Override
    public void removeObserver(Observer o) {
        for (T priority : observerMap.keySet()) {
            observerMap.get(priority).removeObserver(o);
        }
        super.removeObserver(o);
    }
}
