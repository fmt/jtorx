package utwente.fmt.jtorx.lazyotf.utilities;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The observer method marker. One-argument methods marked with this annotation
 * are recognized as observer methods by {@link Observable} implementations.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ObserverMethod {
}
