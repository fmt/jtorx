package utwente.fmt.jtorx.lazyotf.utilities;

import java.util.HashSet;
import java.util.Set;

import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;

/**
 * A utility class.
 */
public final class Utils {
    /**
     * Compute the subset of labels having the given prefix.
     * 
     * @param labels
     *            A set of labels.
     * @param constraint
     *            The LabelConstraint to be used for filtering.
     * @return All labels contained in the given set satisfying the given
     *         constraint.
     */
    public static Set<Label> filterLabels(final Set<Label> labels, final LabelConstraint constraint) {
        final Set<Label> result = new HashSet<Label>();
        for (final Label l : labels) {
            if (constraint.isSatisfiedBy(l)) {
                result.add(l);
            }
        }
        return result;
    }

    /** Utility class - cannot be instantiated. */
    private Utils() {
    }

    public static class IndirectReference<T> {
        public T referencedObject;

        public IndirectReference(T referencedObject) {
            this.referencedObject = referencedObject;
        }

        public IndirectReference() {
            this.referencedObject = null;
        }
    }
}
