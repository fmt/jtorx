package utwente.fmt.jtorx.logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URL;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;



import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.exec.ExecuteStreamHandler;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.OS;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;

import utwente.fmt.jtorx.logger.mux.Multicast;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.FailureReporter;

 public abstract class AniServer {
	
	class StreamReader extends Thread {
		private BufferedReader is;
		final int ARRAYSIZE=1024;
		private String buf = "";
		private String pfx;

		StreamReader(String name, BufferedReader is, String pfx) {
			super("AniServer-"+name);
			this.is = is;
			this.pfx = pfx;
		}

		public void run() {
			try {
				int n;
				char[] cbuf = new char[ARRAYSIZE];
				while (true /*!done*/ /*err.ready()*/){
					n  = is.read(cbuf, 0, ARRAYSIZE);
					if (n <= 0)
						return;
					buf += new String(cbuf,0,n);
					int i = buf.indexOf("\n");
					while (i >= 0) {
						AniServer.subprocessLine(pfx, buf.substring(0,i).replaceAll("\r", "").trim());
						buf = buf.substring(i+1);
						i = buf.indexOf("\n");
					}
				}
			} catch (IOException e) {
				System.err.println("Animation "+kitName+" run io exception: "+e.getMessage());
				//if (!e.getMessage().equalsIgnoreCase("Socket closed") &&
				//		!e.getMessage().equalsIgnoreCase("Connection reset") &&
				//		!e.getMessage().equalsIgnoreCase("Socket is closed"))
				//	e.printStackTrace();
				try {
					is.close();
				} catch (IOException e1) {
					System.err.println("Animation "+kitName+" run close io exception: "+e1.getMessage());
					//e1.printStackTrace();
				}
			}
		}
	}
	
	public DataOutputStream newAnimator(String title, String tag, Boolean reuse, Multicast mc) {
		return newAnimator(title, tag, reuse, mc, null);
	}
	public DataOutputStream newAnimator(String title, String tag, Boolean reuse, Multicast mc, String filename) {
		
		if (done || !running)
			return null;
		
		if (srvPort < 0) {
			errorReporter.report("Animation "+kitName+": server still starting: cannot start "+tag+" animation: "+title);
			return null;
		}

		try {
			Socket sock = new Socket(java.net.InetAddress.getByName(null), srvPort);
			StreamReader reader = new StreamReader(title+"-"+tag+"-"+sock.getLocalSocketAddress().toString(),
					new BufferedReader(new InputStreamReader(sock.getInputStream())), "sock: ");
			reader.start();
			DataOutputStream out = new DataOutputStream(sock.getOutputStream());
			String msg = "-args";
			if (reuse) 
				msg += " -r";
			if (mc != null)
				msg += " -m "+mc.getAddress();
			if (tag != null && !tag.trim().equals(""))
				msg += " -k \""+tag.trim()+"\"";
			if (tag != null && !tag.trim().equals("") && title != null && !title.trim().equals(""))
				msg += " -t \""+tag.trim()+": "+title.trim()+"\"";
			if (filename != null && !filename.trim().equals(""))
				msg += " \""+filename.trim().replace("\\", "\\\\")+"\"";
			msg += "\n";
			System.err.println("newAnimator-msg: "+msg);
			out.write(msg.getBytes());
			out.flush();
			return out;
		} catch (UnknownHostException e) {
			System.err.println("Animation "+kitName+" newAnimator unknown host exception: "+e.getMessage());
			//e.printStackTrace();
			errorReporter.report("Animation "+kitName+": don't know about host: "+localHost+".");
			return null;
		} catch (IOException e) {
			System.err.println("Animation "+kitName+" newAnimator io exception: "+e.getMessage());
			//if (!e.getMessage().toLowerCase().startsWith("connection refused"))
			//		e.printStackTrace();
			errorReporter.report("Animation "+kitName+": couldn't get I/O for "
					+ "the connection to: "+localHost+": "+e.getMessage());
			return null;
		}

	}
	public void done() {
		System.err.println("Animation "+kitName+" done starting="+starting+" running="+running+" srvPort="+srvPort);
		
		done = true;
		try {
			finalize();
		} catch (Throwable e) {
			System.err.println("Animation "+kitName+" done caught exception: "+e.getMessage());
			//e.printStackTrace();
		}

		
		if (starting || (running && srvPort < 0)) {
			destroy(5);
			System.err.println("Animation "+kitName+": animation process is still starting up");
			System.err.println("Animation "+kitName+": we have to wait until it detects eof on stdin");
			System.err.println("Animation "+kitName+": this may take a while...");
			return;
		}

		if (! running)
			return;
		if (srvPort < 0)
			return;
	
		try {
			Socket sock = new Socket(java.net.InetAddress.getByName(null), srvPort);
			StreamReader reader = new StreamReader(kitName+"-done",new BufferedReader(new InputStreamReader(sock.getInputStream())), "sock-exit: ");
			reader.start();
			DataOutputStream sockOut = new DataOutputStream(sock.getOutputStream());
			String msg = "-exit";
			msg += "\n";
			sockOut.write(msg.getBytes());
			sockOut.flush();
			sockOut.close();
			out.close();
			return;
		} catch (IllegalArgumentException e) {
			// when we get here, most likely we had a race:
			// onProcessComplete was run after we  started processing in this done() method
			System.err.println("Animation "+kitName+" done illegal argument exception: "+e.getMessage());
			errorReporter.report("Animation "+kitName+": no server to tell to quit: don't know about host: "+localHost+".");
			return;
		} catch (UnknownHostException e) {
			System.err.println("Animation "+kitName+" done unknown host exception: "+e.getMessage());
			//e.printStackTrace();
			errorReporter.report("Animation "+kitName+": telling server to quit: don't know about host: "+localHost+".");
			return;
		} catch (IOException e) {
			System.err.println("Animation "+kitName+" done io exception: "+e.getMessage());
			if (!e.getMessage().equalsIgnoreCase("Socket closed") &&
					!e.getMessage().equalsIgnoreCase("Connection reset") &&
					!e.getMessage().equalsIgnoreCase("Socket is closed") &&
					! e.getMessage().startsWith("Connection refused")) {
				//e.printStackTrace();
				errorReporter.report("Animation "+kitName+": telling server to quit: couldn't get I/O for "
					+ "the connection to: "+localHost+".");
			}
			return;
		}

	}
	class ResultHandler implements ExecuteResultHandler {

		public void onProcessComplete(int exitValue) {
			System.err.println("Animation "+kitName+"  onProcessComplete: "+exitValue);
			running = false;
			srvPort = -1;
			if (activatedDestroyer !=null)
				activatedDestroyer.interrupt();

			for (FailureReporter b: buttonUpdater)
				b.notRunning();
			if (! done)
				startServer();
		}

		public void onProcessFailed(ExecuteException e) {
			System.err.println("Animation "+kitName+" onProcessFailed: "+e.getMessage());
			running = false;
			srvPort = -1;
			if (activatedDestroyer !=null)
				activatedDestroyer.interrupt();
			errorReporter.report("Animation "+kitName+" exits with failure"+expectedFailureMsg+": "+e.getMessage());
			for (FailureReporter b: buttonUpdater)
				b.notRunning();
		}

	}
	class StreamHandler implements ExecuteStreamHandler {

		public void setProcessErrorStream(InputStream is) throws IOException {
			err = new BufferedReader(new InputStreamReader(is));
		}

		public void setProcessInputStream(OutputStream os) throws IOException {
			in = new DataOutputStream(os);
		}

		public void setProcessOutputStream(InputStream is) throws IOException {
			out = new BufferedReader(new InputStreamReader(is));
		}

		public void start() throws IOException {
			System.err.println("StreamHandler "+kitName+" start done="+done);

			StreamReader errReader = new StreamReader(kitName, err, "suberr: ");
			errReader.start();
			if (done) {
				StreamReader outReader = new StreamReader(kitName, out, "subout: ");
				outReader.start();
				if (in!=null) 
					try {
						in.close();
					} catch (IOException e) {
						System.err.println("StreamHandler "+kitName+" start done="+done+ " exception closing in: "+e.getMessage());
					}
				destroy(100);
				return;
			}

			String line = out.readLine();
			dbgMsgln(line);
			if (line != null) {
				String words[] = line.split("[ \\t]");
				String portStr = words[2];
				System.out.println("port: "+portStr);
				try {
					srvPort = Integer.parseInt(portStr);
					for (FailureReporter b: buttonUpdater)
						b.isRunning();
				}
				catch(NumberFormatException nfe) {
					System.err.println("port is not a number: "+portStr);
					out.close();
					return;
				}
			}
		}

		public void stop() {
			System.err.println("StreamHandler "+kitName+" stop");
		}

		// start a separate thread to consume the error stream,
		// to avoid that the explorer program gets blocked while writing its standard error,
		// http://forums.sun.com/thread.jspa?threadID=676129&messageID=9744480
	}
	
	private void startServer() {
		if(done) {
			System.err.println("startServer "+kitName+" not starting server: done="+done);
			return;
		}
		starting = true;
		System.err.println("startServer "+kitName+" about to start server");
		exec.setStreamHandler(new StreamHandler());
		try {
			exec.execute(cmd, new ResultHandler());
			running = true;
			starting = false;
		} catch (ExecuteException e) {
			System.err.println("startServer "+kitName+" execute exception: "+e.getMessage());
			//e.printStackTrace();
			errorReporter.report("Animation "+kitName+": error starting server: "+e.getMessage());
		} catch (IOException e) {
			System.err.println("startServer "+kitName+" io exception: "+e.getMessage());
			//e.printStackTrace();
			errorReporter.report("Animation "+kitName+": error starting server: "+e.getMessage());
		}
	}

	public boolean isRunning() {
		return running;
	}
	public boolean isStarting() {
		return starting;
	}

	public void start() {
		if(done)
			return;

		if (! running) {
			startServer();
			if (running)
				errorReporter.report("Animation "+kitName+": started server");
		}
	}
	
	public void addFailureReporter(FailureReporter b) {
		buttonUpdater.add(b);
	}
	public void removeFailureReporter(FailureReporter b) {
		buttonUpdater.remove(b);
	}

	// brute force kill process
	public void destroy(final long delay) {
		activatedDestroyer = new Thread() {
			public void run() {
				// System.err.println("Animation "+kitName+": destroy "+delay);
				try {
					Thread.sleep(delay);
					if (isRunning() || isStarting()) {
						System.err.println("Animation "+kitName+": trying to kill server program");
						expectedFailureMsg = " (not unexpectedly, because we had to kill the process)";
						destroyer.run();
					} else {
						System.err.println("Animation "+kitName+": not killing server program running="+isRunning()+" "+"starting="+isStarting());
					
					}
				} catch (InterruptedException e) {
					System.err.println("Animation "+kitName+": destroyer was interrupted: ");
					//e.printStackTrace();
				}
			}
		};
		activatedDestroyer.start();
	}
	


	public AniServer(ErrorReporter r, String k) {
		errorReporter = r;
		kitName = k;
		
		kitsDir = getKitsDir();

		exec = new DefaultExecutor();
		destroyer = new ShutdownHookProcessDestroyer();
		exec.setProcessDestroyer(destroyer);

		
		//String prog = "";
		//if (kitProgDir != null && ! kitProgDir.trim().equals(""))
		//	prog += kitProgDir + "/";
		//prog += "tclkit";
		
		String prog = getKitProg();
			
		cmd = new CommandLine(prog);


		// check that this succeeds
		//File dir = new File( dirName );

		cmd.addArgument(kitsDir+File.separator+kitName);
		cmd.addArgument("-server");
		cmd.addArgument("-private");
		cmd.addArgument("-surviveWindowDestroy");

	}
	public BufferedReader getOut() {
		return out;
	}
	public DataOutputStream getIn() {
		return in;
	}
	public BufferedReader getErr() {
		return err;
	}

	public void dbgMsgln(String s) {
		if (debug)
			System.out.println("dbg "+kitName+": "+s);
	}
	public void subprocessDbgMsgln(String s) {
		if (showSubProcessDebug)
			System.out.println("subdbg "+kitName+": "+s);
	}
	public static void subprocessLine(String pfx, String s) {
		if (showSubProcessStderr)
			System.out.println(pfx+s);
	}

	protected void finalize() throws Throwable {
		try {
			in.write(new String("-exit\n").getBytes());
			in.flush();
			in.close();

		} finally {
			super.finalize();
		}
	}
	
	private File getRunDir() {
		URL u = this.getClass().getProtectionDomain().getCodeSource().getLocation();
		//System.err.println("dir: "+u);

		File f = null;
		try {
			f = new File(URLDecoder.decode(u.getFile(), "UTF-8")).getAbsoluteFile();
		} catch (UnsupportedEncodingException e) {
			System.err.println("Animation "+kitName+": caught exception: "+e.getMessage());
			//e.printStackTrace();
		}
		//System.err.println("dir-2: "+f);
		return f;
	}
	private String getKitsDir () {
		if (OS.isFamilyMac())
			return getRunDir().getParent();
		else 
			return getRunDir().getParent();
	}
	private String getKitProg() {
		String kitProgRoot = getRunDir().getParent();
		String dir;
		if (OS.isFamilyMac()) {
			dir = getRunDir().getParent();
			return dir+File.separator+"tclkit";
		} else if  (OS.isFamilyWindows()) {
			dir = kitProgRoot;
			return dir+File.separator+"tclkit.exe";
		}
		return "tclkit";
	}

	private DataOutputStream in;
	//private DataInputStream inputStream;
	//private DataInputStream errorStream;
	private BufferedReader out;
	private BufferedReader err;
	private static Boolean debug = false;
	private static Boolean showSubProcessStderr = true;
	private static Boolean showSubProcessDebug = true;
	private int srvPort = -1;

	private ErrorReporter errorReporter;
	private Collection<FailureReporter> buttonUpdater = new ArrayList<FailureReporter>();
	private CommandLine  cmd;
	private Executor exec;
	private ShutdownHookProcessDestroyer destroyer;
	private Thread activatedDestroyer = null;
	private boolean done = false;
	private boolean running = false;
	private boolean starting = false;
	private String kitName = null;
	private String expectedFailureMsg = "";
	
	private String localHost = "local host";
	
	
	private String kitsDir = null;

}
