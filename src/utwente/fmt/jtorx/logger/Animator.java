package utwente.fmt.jtorx.logger;

import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;

public interface Animator {
	public enum UpdateMode { OFF, ON };
	
	public void init(DriverInitializationResult s, String id);
	public void addInterActionResult(DriverInterActionResult l, String pId, String id);
	public void addLogMessage(long time, String src, String msg);
	public void addConfigMessage(long time, String src, String name, String val);
	public void end(DriverFinalizationResult e);
	public void setUpdateMode (UpdateMode m);
}
