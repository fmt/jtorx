package utwente.fmt.jtorx.logger.anidot;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import utwente.fmt.jtorx.logger.Animator.UpdateMode;

public class Anidot implements Appendable {
	public enum RenderMode { OFF, DELAY, ON };
	
	public void extendGraph(String cmds) {
		if (out == null)
			return;

		try {
			String s;
			s = cmds+"\n";
			if (debug) System.err.println(s);
			out.write(s.getBytes());
			out.flush();
		} catch (IOException e) {
			cleanup();
		}
	}
	
	/*
	public void colorGraph(String next) {
		colorGraph(next, null, null, null, null);
	}
	public void colorGraph(String nextDir, String nextIndir) {
		colorGraph(nextDir, nextIndir, null, null, null);
	}
	
	public void colorGraph(String next, String src, String dst) {
		colorGraph(next, null, null, src, dst);
	}
	*/


	public void colorGraph(String nextDir, String nextIndir, String seen, String src, String dst) {
		if (out == null)
			return;
		if (nextIndir == null)
			nextIndir = "";
		
		String pfx = null;
		if (src!=null && dst != null)
			pfx = "tree "+src+" "+dst;
		
		String prev = prevMap.get(src);
		
		if (debug) System.err.println("out: "+out+" src=("+src+") dst=("+dst+") seen=("+seen+") nextDir=("+nextDir+") nextIndir=("+nextIndir+") prev=("+prev+")");


		String s = "";
		try {
			if (pfx!=null)
				s += " "+pfx;
			if (seen != null)
				s += " -c green "+ seen;
			if (prev != null)
				s += " -c grey "+ prev;
			if (!nextIndir.trim().equals(""))
				s += " -c orange "+nextIndir;
			s += " -c red "+nextDir;
			s += "\n";
			if (debug) System.err.println(s);
			out.write(s.getBytes());
			out.flush();
		} catch (IOException e) {
			cleanup();
		}
		prevMap.put(dst, nextIndir+" "+nextDir);
	}
	

	public void setRender(RenderMode r) {
		if (out == null)
			return;

		String s = "ctl render ";
		if (r == RenderMode.ON)
			s += "on";
		else if (r == RenderMode.OFF)
			s += "off";
		else if (r == RenderMode.DELAY)
			s += "delay";
		s += "\n";
		try {
			out.write(s.getBytes());
			out.flush();
		} catch (IOException e) {
			cleanup();
		}
	}
	public void setUpdateMode(UpdateMode m) {
		if (out == null)
			return;

		String s = "ctl update ";
		if (m == UpdateMode.ON)
			s += "on";
		else if (m == UpdateMode.OFF)
			s += "off";
		s += "\n";
		try {
			out.write(s.getBytes());
			out.flush();
		} catch (IOException e) {
			cleanup();
		}
	}


	public void end() {
		if (out == null)
			return;
		try {
			out.close();
		} catch (IOException e) {
			cleanup();
		}
	}
	
	
	public Appendable append(CharSequence csq) throws IOException {
		if (out == null)
			return this;

		out.write(csq.toString().getBytes());
		out.flush();
		return this;
	}

	public Appendable append(char c) throws IOException {
		if (out == null)
			return this;

		out.writeChar(c);
		out.flush();
		return this;
	}

	public Appendable append(CharSequence csq, int start, int end)
			throws IOException {
		if (out == null)
			return this;

		out.write(csq.subSequence(start, end).toString().getBytes());
		out.flush();
		return this;
	}
	
	public boolean hasNeato() {
		return hasNeato;
	}
	public boolean prefab() {
		return prefab;
	}

	public Anidot(DataOutputStream o, String name, boolean hasNeato, boolean prefab) {
		out = o;
		this.hasNeato = hasNeato;
		this.prefab = prefab;
		
		if (out == null)
			return;
	}
	
	private void cleanup() {
		if (out == null)
			return;

		try {
			out.close();
			out = null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private DataOutputStream out;
	private boolean debug = false;
	private boolean hasNeato;
	private boolean prefab;
	private Map<String,String> prevMap = new HashMap<String,String>();

}
