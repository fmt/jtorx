package utwente.fmt.jtorx.logger.anidot;


import java.io.DataOutputStream;


import utwente.fmt.jtorx.logger.AniServer;
import utwente.fmt.jtorx.logger.mux.Multicast;
import utwente.fmt.jtorx.ui.ErrorReporter;

public class AnidotSrv extends  AniServer {

	public Anidot newAnidot(String title, String tag, Boolean reuse, Multicast mc, String filename) {
		DataOutputStream o = newAnimator(title, tag, reuse, mc, filename);
		if (o == null)
			return null;
		return new Anidot(o, title, hasNeato, (filename!=null&&!filename.trim().equals("")));
	}
	public Anidot newAnidot(String title, String tag, Boolean reuse, Multicast mc) {
		return newAnidot(title, tag, reuse, mc, null);
	}
	
	public AnidotSrv(ErrorReporter r) {
		super(r, "anidot.kit");
		// neato is now also available on windows, with our latest anidot.kit
	}
	
	private boolean hasNeato = true;
}
