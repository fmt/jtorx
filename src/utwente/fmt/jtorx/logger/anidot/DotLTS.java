package utwente.fmt.jtorx.logger.anidot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;

import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class DotLTS {
	
	public class DotEdge {
		
		public String getSource() {
			return src;
		}
		public String getLabel() {
			return lbl;
		}
		public String getDestination() {
			return dst;
		}
		
		@Override
		public boolean equals(Object o) {
			if(this == o)
				return true;
			if (o instanceof DotEdge) {
				DotEdge s = (DotEdge) o;
				return (s.src.equals(src) &&
						s.lbl.equals(lbl) &&
						s.dst.equals(dst));
			}
			return false;
		}

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 31 * hash + (null == src ? 0 : src.hashCode());
			hash = 31 * hash + (null == lbl ? 0 : lbl.hashCode());
			hash = 31 * hash + (null == dst ? 0 : dst.hashCode());
			// hash = 31 * hash + (null == allStates ? 0 : allStates.hashCode());
			return hash;
		}

		public DotEdge(Transition<?extends State> t) {
			src = getNodeName(t.getSource());
			lbl = t.getLabel().getString();
			dst = getNodeName(t.getDestination());
		}
		private String src;
		private String lbl;
		private String dst;
	}
	
	private DotEdge doAddEdge(Transition<?extends State> t) {
		DotEdge de = new DotEdge(t);
		if (debug) System.err.println("DotLTS.doAddEdge t="+de.toString()+" t.src="+de.getSource()+
				" t.dst="+de.getDestination()+" t.lbl="+de.getLabel());

		if (! edgemap.containsKey(de)) {
			if (debug) {
				System.err.println("\tnot in map:");
				for(DotEdge tt: edgemap.keySet()){
					System.err.println("\tmap: t="+tt.toString()+" tt.src="+tt.getSource()+
							" tt.dst="+tt.getDestination()+" tt.lbl="+tt.getLabel());
				}
			}
			edgemap.put(de, nextNr++);
			Vector<Position> pos = t.getPosition();
			String dst = de.getDestination();
			String src = de.getSource();
			String name = ""+(nextNr-1);
			String label = escape(de.getLabel());
			String style = "";
			if (! t.getLabel().isObservable())
				style = " style dashed";
			if (anidot!=null && anidot.hasNeato() && pos!=null && pos.size() > 0) {
				int i = 0;
				for(Position p: pos) {
					dst = "e_"+name+"_"+i;
					if (anidot != null)
						anidot.extendGraph("dot node "+dst+
								//" style invis"+
								" label \"\""+
								" shape point"+
								" width 0"+
								" height 0"+
								" pos \""+p.getX()+","+p.getY()+"!"+"\"");
					if (anidot != null)
						anidot.extendGraph("dot edge "+src+"->"+dst+
								" label \""+label+"\""+
								" name "+name+
								" dir none"+ style);
					src = dst;
					label = "";
					i++;
				}
			}
			Position lp = t.getLabelPosition();

			String ext = "";
			if (anidot!=null && anidot.hasNeato() && lp!=null)
				ext = " lp \""+lp.getX()+","+lp.getY()+"!"+"\"";
			if (anidot != null)
				anidot.extendGraph("dot edge "+src+"->"+de.getDestination()+
						" label \""+label+"\""+
						" name "+name+ style + ext);
		}
		return de;
	}
	public void addEdge(Transition<?extends State> t) {
		String name;
		if (anidot!=null && !anidot.prefab()) {
			DotEdge de = doAddEdge(t);
			name = ""+ edgemap.get(de);
		} else
			name = t.getEdgeName(); //TODO possible bug
		edgeHighlights += " " + name;
		if (!seenEdgesSet.add(name))
			seenEdges += " " + name;
	}
	public void addIndirectEdge(Transition<?extends State> t) {
		String name;
		if (anidot!=null && !anidot.prefab()) {
			DotEdge de = doAddEdge(t);
			name = "" + edgemap.get(de);
		} else
			name = t.getEdgeName(); //TODO possible bug
		indirectEdgeHighlights += " " + name;
		if (!seenEdgesSet.add(name))
			seenEdges += " " + name;
	}
	
	
	private void doAddNode(State s, String v, boolean useBoxShape) {
		String ext ="";
		if (v != null) {
			if (useBoxShape)
				ext = " shape box";
			if (!v.equalsIgnoreCase(s.getID()))
				ext += " label \""+v+"\"";
		}
		Position p = s.getPosition();
		if (anidot != null && anidot.hasNeato() && p!=null)
			ext += " pos \""+p.getX()+","+p.getY()+"!"+"\"";
		if (p!=null && !setLayoutAttribute)
			if (anidot != null && anidot.hasNeato()) {
				anidot.extendGraph("dot setgraphattr layout neato");
				setLayoutAttribute = true;
			}
		if (! nodes.contains(s.getID())) {
			nodes.add(s.getID());
			if (anidot != null)
					anidot.extendGraph("dot node "+s.getID()+ext);
		}
		//else if (v != null && !v.equalsIgnoreCase(s.getID()))
		//	anidot.extendGraph("dot node "+s.getID()+ext);

	}
	public void addNode(State s) {
		if (anidot!=null && !anidot.prefab())
			doAddNode(s, s.getLabel(false), false);
		nodeHighlights += " "+getNodeName(s)+" ";
	}
	public void addNode(State s, String v) {
		String l = s.getLabel(false);
		if (v.equalsIgnoreCase(l))
			doAddNode(s, v, true);
		else
			doAddNode(s, l+"\\n"+v, true);
		nodeHighlights += " "+getNodeName(s)+" ";
	}
	public void addIndirectNode(State s) {
		if (anidot!=null && !anidot.prefab())
			doAddNode(s, s.getLabel(false), false);
		indirectNodeHighlights += " "+getNodeName(s)+" ";
	}
	
	public void addRoot(String name, Position p) {
		String ext = "";
		if (anidot != null && anidot.hasNeato() && p!=null)
			ext += " pos \""+p.getX()+","+p.getY()+"!"+"\"";
		else
			ext += " pos \"0,0!\"";
		if (anidot!=null && !anidot.prefab())
			anidot.extendGraph("dot node ___all shape box label \""+name+"\""+ext);
	}
	
	// NOTE do not use the method below, because
	// it seems that anidot is unabe to actually hide the node
	// (does not understand 'style invisible' ?)
	public void addInvisibleRoot() {
		if (anidot!=null && !anidot.prefab())
			anidot.extendGraph("dot node ___all shape point style invisible");
	}
	
	public void  addInitialEdge(State n) {
		if (anidot!=null && !anidot.prefab())
			anidot.extendGraph("dot edge ___all->"+n.getID()+" name enil");
		edgeHighlights += " "+"enil"; //TODO need fix here
	}
	
	public void stepBegin() {
		stepBegin(null);
	}
	public void stepBegin(String id) {
		treeNodeSrc = id;
		nodeHighlights = "";
		edgeHighlights = "";
		indirectNodeHighlights = "";
		indirectEdgeHighlights = "";
		seenEdges = "";
		//if (anidot != null)
		//	anidot.setRender(RenderMode.OFF);
	}

	public void stepEnd() {
		stepEnd(null);
	}
	public void stepEnd(String id) {
		treeNodeDst = id;
		if (anidot != null) {
			//anidot.setRender(RenderMode.ON);
			if (indirectNodeHighlights.equals("") && indirectEdgeHighlights.equals("") )
				anidot.colorGraph("-n "+nodeHighlights+" -e "+edgeHighlights, null, " -E "+seenEdges, treeNodeSrc, treeNodeDst);
			else
				anidot.colorGraph("-n "+nodeHighlights+" -e "+edgeHighlights, "-n "+indirectNodeHighlights+" -e "+indirectEdgeHighlights, " -E "+seenEdges, treeNodeSrc, treeNodeDst);
		}
	}
	
	public DotLTS(Anidot a) {
		anidot = a;
		edgemap = new HashMap<DotEdge, Integer>();
		nodes = new HashSet<String>();
		seenEdgesSet = new HashSet<String>();
		setLayoutAttribute = false;
	}
	
	private String getNodeName(State s) {
		String nn = s.getNodeName();
		String id = s.getID();
		String res = "";
		if (anidot!=null && anidot.prefab() && nn!=null && !nn.trim().equals(""))
			res = nn.trim();
		else
			res =  s.getID();
		if (debug)
			System.err.println("DotLTS-getNodeName: id="+id+" nn="+nn+" res="+res);
		return res;
	}
	
	private String escape(String s) {
		int i=0;
		char c;
		String res = "";
		while(i<s.length()) {
			c = s.charAt(i);
			i++;
			if (c == '\\')
				res += "\\\\";
			else if (c == '"')
				res += "\\\"";
			else
				res += c;
		}
		return res;
	}
	
	
	private HashMap<DotEdge, Integer> edgemap;
	private HashSet<String> nodes;
	private HashSet<String> seenEdgesSet;
	private String seenEdges = "";
	private int nextNr = 0;
	private Anidot anidot;
	private String nodeHighlights = "";
	private String indirectNodeHighlights = "";
	private String edgeHighlights = "";
	private String indirectEdgeHighlights = "";
	private boolean debug = false;
	private String treeNodeSrc = null;
	private String treeNodeDst = null;
	private boolean setLayoutAttribute = false;
}
