package utwente.fmt.jtorx.logger.anidot;

import java.util.Iterator;

import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.torx.DriverSimInitializationResult;
import utwente.fmt.jtorx.torx.DriverSimInterActionResult;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.SuspensionAutomatonState;
import utwente.fmt.lts.Transition;


public class ImplToDot extends ModelOrImplToDot {

	public void init(DriverInitializationResult s, String id) {
		if (s!=null && s instanceof DriverSimInitializationResult) {
			DriverSimInitializationResult r = (DriverSimInitializationResult) s;
			handleInitState(r.getAdapterState(), id);
		}
	}
	
	public void addLogMessage(long t, String src, String msg) {
	}
	public void addConfigMessage(long t, String src, String name, String val) {
	}


	protected void handleInitStateDetail(CompoundState cs) {
		if (debug)
			System.out.println("ImplToDot handleInitStateDetail CompoundState "+cs+ "...");

		State s = null;
		for (Iterator<? extends State> it = cs.getSubStates(); it.hasNext(); ) {
			s = it.next();
			dotLTS.addInitialEdge(s);
		}
		// the code below is effectively a special case for an implementation
		// that has tau transitions from the initial state, which it has taken
		// when it is started.
		// the code assumes that the implementation is in a single state at a time.
		// getSubStates, above gave its initial state.
		// getSubTransitions shows how it walked from the initial state.
		// we use that to highlight the transitions that were walked,
		// and to obtain the final destination state of the initial internal walk
		// NOTE: we can obtain the final destination here,
		// because elsewhere we take care to give the sub-transitions in 'natural' order.
		for (Iterator<? extends Transition<?extends State>> it = cs.getSubTransitions(); it.hasNext(); ) {
			Transition<?extends State> t = it.next();
			dotLTS.addIndirectNode(t.getSource()); // adds orange node on path to red one
			dotLTS.addIndirectEdge(t);
			s = t.getDestination();
			if (debug)
				System.out.println("ImplToDot edge "+t.getSource().getID()+ "-> "+t.getDestination().getID());
		}
		if (debug)
			System.out.println("ImplToDot state "+s.getID());
		dotLTS.addNode(s);
		if (debug)
			System.out.println("ImplToDot handleInitStateDetail CompoundState "+cs+" done");
	}
	
	@Override
	protected void handleInitStateDetail(SuspensionAutomatonState sas) {
		boolean state_found = false;
		if (debug)
			System.out.println("ImplToDot handleInitStateDetail SuspensionAutomatonState "+sas);
		for (Iterator<? extends State> it = sas.getDirectSubStates(); it.hasNext(); )
			dotLTS.addInitialEdge(it.next());
		for (Iterator<? extends Transition<?extends State>> it = sas.getSubTransitions(); it.hasNext(); ) {
			Transition<?extends State> t = it.next();
			dotLTS.addIndirectNode(t.getSource()); // adds orange node on path to red one
			dotLTS.addIndirectEdge(t);
			if (debug)
				System.out.println("ImplToDot edge "+t.getSource().getID()+ "-> "+t.getDestination().getID());
		}
		// the next code assumes there is only one indirect-sub-state: the current adapter state
		for (Iterator<? extends State> it = sas.getIndirectSubStates(); it.hasNext(); ) {
			dotLTS.addNode(it.next());
			state_found = true;
		}
		// the next code is there for when we use a Primer on a trace: without it we would not have an initial state
		if (! state_found) {
			for (Iterator<? extends State> it = sas.getDirectSubStates(); it.hasNext(); )
				dotLTS.addNode(it.next());
		}

		if (debug)
			System.out.println("ImplToDot handleInitStateDetail SuspensionAutomatonState "+sas+" done");

	}

	
	public void addInterActionResult(DriverInterActionResult l, String pId, String id) {
		if (debug)
			System.out.println("ImplToDot addInterActionResult InterActionResult "+l+" ...");
		if (l instanceof DriverSimInterActionResult)
			doAddInterActionResult((DriverSimInterActionResult) l, pId, id);
		if (debug)
			System.out.println("ImplToDot addInterActionResult InterActionResult "+l+" done");
		
	}

	public void doAddInterActionResult(DriverSimInterActionResult l, String pId, String id) {
		if (debug)
			System.out.println("ImplToDot addInterActionResult DriverSimInterActionResult "+l+" ...");
		handleInterActionTransition(l.getAdapterTransition(), pId, id, true);
	}


	public ImplToDot(Anidot a, String n, DotLTS d, LTS l) {
		super(a, n, d, l);
	}


	private boolean debug = false;


}
