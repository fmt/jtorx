package utwente.fmt.jtorx.logger.anidot;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


import utwente.fmt.jtorx.logger.Animator;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.guided.GuidedLTS;
import utwente.fmt.jtorx.torx.guided.GuidedState;
import utwente.fmt.jtorx.torx.guided.GuidedTransition;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.SuspensionAutomatonState;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.State;

public abstract class ModelOrImplToDot implements Animator {
	
	public void handleInitState(CompoundState cs, String id) {
		if (cs instanceof GuidedState) {
			GuidedState gcs = (GuidedState) cs;
			CompoundState s = gcs.getCompoundState(lts);
			if (s != null)
				doHandleInitState(s, id);
		} else {
			doHandleInitState(cs, id);
		}
	}
	
	public void doHandleInitState(CompoundState cs, String id) {
		if (!graphStarted){
			dotLTS.stepBegin("-");
			handleInitStateDetailByType(cs);
			dotLTS.stepEnd(id);
			graphStarted = true;
		}
	}
	
	private void handleInitStateDetailByType(CompoundState cs) {
		if (cs instanceof SuspensionAutomatonState )
			handleInitStateDetail((SuspensionAutomatonState)cs);
		else
			handleInitStateDetail(cs);
	}

	protected abstract void handleInitStateDetail(SuspensionAutomatonState sas);
	protected abstract void handleInitStateDetail(CompoundState sas);

	
	private Position getRootPosition(LTS l) {
		if (l instanceof GuidedLTS) { // seems it never happens that l is GuidedLTS
			GuidedLTS gl = (GuidedLTS) l;
			CompoundLTS cl = gl.getSubLTS(l);
			if (cl != null)
				return getRootPosition(cl);
		} else if (l instanceof CompoundLTS) {
			CompoundLTS cl = (CompoundLTS) l;
			return getRootPosition(cl);
		}
		return l.getRootPosition();

	}
	private Position getRootPosition(CompoundLTS l) {
		LTS sub = l.getSubLTS();
		if (sub!=null)
			return sub.getRootPosition();
		return l.getRootPosition();
	}
	
	
	protected void handleInterActionTransition(CompoundTransition<? extends CompoundState> cc, String pId, String id, boolean showSourceTauTrans) {
		if(debug)
			System.out.println("\thandleInterActionTransition CompoundTransition: "+cc);
		if (cc instanceof GuidedTransition<?>) {
			GuidedTransition<?extends GuidedState> gcc = (GuidedTransition<?extends CompoundState>)cc;
			CompoundTransition<?extends CompoundState> t = gcc.getCompoundTransition(lts);
			if (t != null)
				doHandleInterActionTransition(t, pId, id, showSourceTauTrans);
		} else
			doHandleInterActionTransition(cc, pId, id, showSourceTauTrans);
	}
	
	// when visualizing implementation we want to visualize the trace of tau transitions that precedes
	// and follows the given observable transition
	// to achieve this we may have to visualize outgoing tau transitions of the _source_
	// state of the given transitions;
	// however, for models (or when simulating suspension automaton) we do _not_ want to show
	// outgoing tau transitions of the source state of the given transition.
	// hence, parameter showSourceTauTrans controls whether or not these outgoing transitions
	// of the source state of the given transition are shown.
	private void doHandleInterActionTransition(CompoundTransition<? extends CompoundState> iat, String pId, String id, boolean showSourceTauTrans) {
		if(debug)
			System.out.println("\tdoHandleInterActionTransition CompoundTransition ("+showSourceTauTrans+"): "+iat+" {");
		dotLTS.stepBegin(pId);
		if (iat != null) {
			CompoundState s = iat.getSource();
			CompoundState d = iat.getDestination();
			if (s!=null && d!=null) {
				Set<State> sourceStatesToSkip = new HashSet<State>();
				if(showSourceTauTrans) {
					for (Iterator<? extends Transition<?extends State>> it = s.getSubTransitions(); it.hasNext(); ) {
						Transition<?extends State> t = it.next();
						if (debug)
							System.out.println("\t\tsrc-i-compound-s: "+t.getSource().getID()+" -- "+t.getLabel().getString()+" -> "+t.getDestination().getID());
						dotLTS.addIndirectNode(t.getDestination()); // adds orange node on path to red one
						dotLTS.addIndirectEdge(t);
						sourceStatesToSkip.add(t.getSource());
						sourceStatesToSkip.remove(t.getDestination());
					}
				}

				handleInterActionTransitionStateDetail(d);

				for (Iterator<? extends Transition<?extends State>> it = iat.getSubTransitions(); it.hasNext(); ) {
					Transition<?extends State> t = it.next();
					if (debug)
						System.out.println("\t\ttr-i-compound-t: "+t.getSource().getID()+" -- "+t.getLabel().getString()+" -> "+t.getDestination().getID());
					dotLTS.addEdge(t);
				}

				for (Iterator<? extends Transition<?extends State>> it = d.getSubTransitions(); it.hasNext(); ) {
					Transition<?extends State> t = it.next();
					if (debug)
						System.out.println("\t\tdst-i-compound-s: "+t.getSource().getID()+" -- "+t.getLabel().getString()+" -> "+t.getDestination().getID());
					// when visualizing implementation, try to avoid coloring the source state of the very first
					// tau transition of the trail, _if_ this source state is the destination of the (trail of the)
					// previous transition (test step)
					// rephrased: if we are visualizing an implementation, and in the previous step we got to
					// a certain state 's', then (unless we do a delta) in the current step state 's' should
					// be grey and the current highlighted trail should start with an outgoing transition from 's'
					if (!showSourceTauTrans || !sourceStatesToSkip.contains(t.getSource()))
						dotLTS.addIndirectNode(t.getSource()); // adds orange node on path to red one
					dotLTS.addIndirectEdge(t);
				}
			}
		}
		dotLTS.stepEnd(id);
		if(debug)
			System.out.println("\tdone doHandleInterActionTransition CompoundTransition: "+iat+" }");
	}
	
	private void handleInterActionTransitionStateDetail(CompoundState cs) {
		if(debug)
			System.out.println("\thandleInterActionTransitionStateDetail CompoundState: "+cs+" "+cs.getID()+ " "+cs.getLabel(false)+ " {");
		if (cs instanceof SuspensionAutomatonState ) {
			if(debug)
				System.out.println("\t\thandleInterActionTransitionStateDetail SuspensionAutomatonState: "+cs+" "+cs.getID()+ " "+cs.getLabel(false));
			SuspensionAutomatonState sas = (SuspensionAutomatonState) cs;
			for (Iterator<? extends State> it = sas.getDirectSubStates(); it.hasNext(); ) {
				State s = it.next();
				dotLTS.addNode(s);
				if (debug)
					System.out.println("\t\ti-compound-s-dir: "+s.getID());
			}
			for (Iterator<? extends State> it = sas.getIndirectSubStates(); it.hasNext(); ) {
				State s = it.next();
				dotLTS.addIndirectNode(s);
				if (debug)
					System.out.println("\t\ti-compound-s-ind: "+s.getID());
			}
		} else {
			if(debug)
				System.out.println("\t\thandleInterActionTransitionStateDetail no-SuspensionAutomatonState: "+cs+" "+cs.getID()+ " "+cs.getLabel(false));

			for (Iterator<? extends State> it = cs.getSubStates(lts); it.hasNext(); ) {
				State s = it.next();
				dotLTS.addNode(s);
				if (debug)
					System.out.println("\t\ti-compound-s-sub: "+s.getID());
			}
		}
		if (debug)
			System.out.println("\t done handleInterActionTransitionStateDetail CompoundState: "+cs+" "+cs.getID()+ " "+cs.getLabel(false)+ " }");
	}
	

	public void end(DriverFinalizationResult e) {
		if (anidot != null)
			anidot.end();
	}
	
	public void setUpdateMode(Animator.UpdateMode m) {
		anidot.setUpdateMode(m);
	}
	
	public ModelOrImplToDot(Anidot a, String n) {
		this(a, n, null, null);
	}
	public ModelOrImplToDot(Anidot a, String n, DotLTS d, LTS l) {
		anidot = a;
		name = n;
		
		if (d != null)
			dotLTS = d;
		else
			dotLTS = new DotLTS(anidot);
		dotLTS.addRoot(name, getRootPosition(l));
		if (anidot != null)
			anidot.setRender(Anidot.RenderMode.DELAY);
		
		lts = l;
		if (debug)
			System.out.println("ModelOrImplToDot: lts "+l);
	}
	
	private Anidot anidot;
	private String name;
	private boolean graphStarted = false;
	
	protected DotLTS dotLTS;
	protected LTS lts;

	private boolean debug = false;
}
