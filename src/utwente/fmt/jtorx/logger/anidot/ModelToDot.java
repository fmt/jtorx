package utwente.fmt.jtorx.logger.anidot;


import java.util.Iterator;

import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.SuspensionAutomatonState;
import utwente.fmt.lts.Transition;


public class ModelToDot extends ModelOrImplToDot {
	
	public void init(DriverInitializationResult s, String id) {
		if (s!=null)
			handleInitState(s.getState(), id);
	}
	
	public void addLogMessage(long t, String src, String msg) {
	}
	public void addConfigMessage(long t, String src, String name, String val) {
	}

	protected void handleInitStateDetail(SuspensionAutomatonState sas) {
		if (debug)
			System.out.println("ModelToDot handleInitStateDetail SuspensionAutomatonState "+sas+" ...");
		for (Iterator<? extends State> it = sas.getDirectSubStates(); it.hasNext(); ) {
			State s = it.next();
			dotLTS.addNode(s);
			dotLTS.addInitialEdge(s);
		}
		for (Iterator<? extends State> it = sas.getIndirectSubStates(); it.hasNext(); )
			dotLTS.addIndirectNode(it.next());
		for (Iterator<? extends Transition<?extends State>> it = sas.getSubTransitions(); it.hasNext(); ) 
			dotLTS.addIndirectEdge(it.next());
		if (debug)
			System.out.println("ModelToDot handleInitStateDetail SuspensionAutomatonState "+sas+" done");
	}
	protected void handleInitStateDetail(CompoundState sas) {
		if (debug)
			System.out.println("ModelToDot handleInitStateDetail CompoundState "+sas);
		for (Iterator<? extends State> it = sas.getSubStates(); it.hasNext(); ) {
			State s = it.next();
			dotLTS.addNode(s);
			dotLTS.addInitialEdge(s);
		}
	}
	
	
	public void addInterActionResult(DriverInterActionResult l, String pId, String id) {
		if (debug)
			System.out.println("ModelToDot addInterActionResult InterActionResult  "+l.getLabel().getString()+"  "+l+" ...");
		// TODO make sure all states that we return are compound states,
		// also the fail state, and possible other states that we add.
		// currently this is the case.
		// However, stuff will break as soon as that is no longer the case
		// TODO really fix this by changing return type in Model or so
		CompoundTransition<? extends CompoundState> ct = l.getTransition();
		handleInterActionTransition(ct, pId, id, false);
		if (debug)
			System.out.println("ModelToDot addInterActionResult InterActionResult "+l+" done");
	}

	
	public ModelToDot(Anidot a, String n, DotLTS d, LTS lts) {
		super(a, n, d, lts);
	}
	
	private boolean debug = false;
	
}
