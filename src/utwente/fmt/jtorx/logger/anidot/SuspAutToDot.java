package utwente.fmt.jtorx.logger.anidot;


import utwente.fmt.jtorx.logger.Animator;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;


public class SuspAutToDot implements Animator {
	
	public void init(DriverInitializationResult r, String id) {
		CompoundState s = r.getState();
		if (!graphStarted){
			dotLTS.stepBegin("-");
			if (debug)
				System.out.println("SuspAutToDot handleInitStateDetail State "+s+" ...");
			dotLTS.addNode(s);
			dotLTS.addInitialEdge(s);
			if (debug)
				System.out.println("SuspAutToDot handleInitStateDetail State "+s+" done");
			dotLTS.stepEnd(id);
			graphStarted = true;
		}
	}

	public void end(DriverFinalizationResult e) {
		if (anidot != null)
			anidot.end();
	}
	
	public void setUpdateMode(Animator.UpdateMode m) {
		anidot.setUpdateMode(m);
	}

	public void addLogMessage(long t, String src, String msg) {
	}
	public void addConfigMessage(long t, String src, String name, String val) {
	}

	public void addInterActionResult(DriverInterActionResult l, String pId, String id) {
		if (debug)
			System.out.println("SuspAutToDot addInterActionResult InterActionResult "+l+" ...");
		dotLTS.stepBegin(pId);
		CompoundTransition<? extends CompoundState> ct = l.getTransition();
		if (l.hasVerdict()) {
			if (debug)
				System.out.println("Testrun: verdict "+l.getVerdict().getString());
			dotLTS.addNode(l.getDst(), l.getVerdict().getString());
		} else
			dotLTS.addNode(l.getDst());
		dotLTS.addNode(ct.getDestination());
		dotLTS.addEdge(ct);
		//dotLTS.addEdge(l.getTransition());
		dotLTS.stepEnd(id);
		if (debug)
			System.out.println("SuspAutToDot addInterActionResult InterActionResult "+l+" done");
	}

	public SuspAutToDot(Anidot a, String n, DotLTS d, LTS l) {
		anidot = a;
		name = n;
		
		if (d != null)
			dotLTS = d;
		else
			dotLTS = new DotLTS(anidot);
		dotLTS.addRoot(name, l.getRootPosition());
		if (anidot != null)
			anidot.setRender(Anidot.RenderMode.DELAY);
		
		//lts = l;
	}
	private Anidot anidot;
	private String name;
	private boolean graphStarted = false;
	private DotLTS dotLTS;
	//private LTS lts;
	
	private boolean debug = false;

}
