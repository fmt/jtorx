package utwente.fmt.jtorx.logger.anidot;


import utwente.fmt.jtorx.logger.Animator;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.lts.CompoundState;

public class TestrunToDot implements Animator {
	public void addInterActionResult(DriverInterActionResult l, String pId, String id) {
		dotLTS.stepBegin(pId);
		if (l.hasVerdict()) {
			if (debug)
				System.out.println("Testrun: verdict "+l.getVerdict().getString());
			dotLTS.addNode(l.getDst(), l.getVerdict().getString());
		} else
			dotLTS.addNode(l.getDst());
		dotLTS.addEdge(l.getTransition());
		dotLTS.stepEnd(id);
	}
	
	public void init(DriverInitializationResult r, String id) {
		CompoundState s = r.getState();
		if(s==null)
			return;
		if (!graphStarted){
			dotLTS.stepBegin("-");
			dotLTS.addNode(s);
			dotLTS.addInitialEdge(s);
			dotLTS.stepEnd(id);
			graphStarted = true;
		}
	}

	public void addLogMessage(long t, String src, String msg) {
	}
	public void addConfigMessage(long t, String src, String name, String val) {
	}

	public void end(DriverFinalizationResult e) {
		if (anidot != null)
			anidot.end();
	}
	
	public void setUpdateMode(Animator.UpdateMode m) {
		anidot.setUpdateMode(m);
	}
	
	public TestrunToDot(Anidot a, String n) {
		anidot = a;
		name = n;

		dotLTS = new DotLTS(anidot);
		dotLTS.addRoot(name, null);
		if (anidot != null)
			anidot.setRender(Anidot.RenderMode.DELAY);
	}
	
	private Anidot anidot;
	private String name;
	private boolean graphStarted = false;
	private DotLTS dotLTS;

	private boolean debug = false;
}
