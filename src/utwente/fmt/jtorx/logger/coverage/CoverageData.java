package utwente.fmt.jtorx.logger.coverage;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.logger.Animator;
import utwente.fmt.jtorx.testgui.CoveragePane;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.torx.guided.GuidedState;
import utwente.fmt.jtorx.torx.guided.GuidedTransition;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;

public class CoverageData implements Animator {
	
	public enum EdgeType {
		DELTALOOP,
		OTHERLOOP,
		DELTANONLOOP,
		OTHERNONLOOP,
		NEdgeType,
	}
	
	
	static public String getNodeName(State s) {
		String nn = s.getNodeName();
		String l = s.getLabel(false);
		String id = s.getID();
		String res = "";
		if (nn!=null && !nn.trim().equals(""))
			res = nn.trim();
		else if (l!=null && !l.trim().equals(""))
			res = l.trim();
		else
			res =  "id-"+s.getID();
		if (false)
			System.err.println("CoverageData-getNodeName: id="+id+" l="+l+" nn="+nn+" res="+res);
		return res;
	}
	

	
	// unfold the model and the suspension automaton to obtain statistics we care about:
	// - nr of states
	// - nr of transitions:
	//   - selfloop transitions     (quiescence/non-quiescence)
	//   - non-selfloop transitions (quiescence/non-quiescence)
	//   distinguish between inputs/outputs?
	private void unfold(LTS lts, LTSCoverage cv) throws LTSException {
		cv.setExpanded(true);
		cv.resetModelStatistics();
		
		if(debug) System.out.println("unfold past start");
		
		HashSet<State> expanded = new HashSet<State>();
		Vector<State> work = new Vector<State>();
		Iterator<? extends State> sit = lts.init();
		if (sit==null) {
			return;
		}
		while(sit.hasNext())
			work.add(sit.next());
		if(debug) System.out.println("unfold past inits");
		while(work.size() > 0) {
			//System.out.println("  in work loop");
			State s = work.firstElement();
			work.remove(0);
			// TODO make robust in other way? throw error?
			if (s!=null) {
				//System.out.println("  in work loop: s!=null");
				Iterator<? extends Transition<?extends State>> tit = null;
				if (!expanded.contains(s)) {
					//System.out.println("  in work loop: s notin expanded, interp="+interp);
					tit = s.menu(interp.isAny());
					//System.out.println("  in work loop: after s.menu");
					expanded.add(s);
					cv.addNode(s);
					while(tit.hasNext()) {
						//System.out.println("    in work loop in succ loop");
						Transition<?extends State> t = tit.next();
						State d = t.getDestination();
						if (!expanded.contains(d)) {
							d.menu(interp.isAny()); // force expansion to canonicalize d
							work.add(d);
						}
						cv.addNode(d);
						cv.addEdge(t);
					}
				}
			}
		}
	}
	
	// here we initialize with two separate LTS'es,
	// could also initialize with RunItemData
	
	// how do identify corresponding states/transitions from different LTS'es?
	// 1. assume deterministic state space exploration,
	//    and for each test run first explore entire state space?
	//    we should be able to combine this with state space exploration
	//    that we do for visualization...
	// 2. for .aut and .graphml files we can use label/info, and for
	//    some .tx models we can use node information...
	//    let RunItemData tell us which to use?
	//    or let RunItemData implement a function that given State returns
	//    some canonical info, or null?
	//    but then RunItemData should also be able to tell whether it can
	//    return canonical info?
	//    or add a function to LTS.State to return canonical data?
	
	public void setModel(LTS model, CompoundLTS suspaut, IOLTSInterpretation i) throws LTSException {
		interp = i; // unfold in depends on this!
		lts = suspaut;
		if (!modelData.isExpanded()) {
			System.out.println("unfold model");
			unfold(model, modelData);
		} else {
			System.out.println("not unfolding model");
		}
		if (!suspautData.isExpanded()) {
			System.out.println("unfold suspaut");
			unfold(suspaut, suspautData);
		} else {
			System.out.println("not unfolding suspaut");
		}
		System.out.println("report model...");
		System.out.println(modelData.getModelStatistics().getReport());
		System.out.println("report suspaut...");
		System.out.println(suspautData.getModelStatistics().getReport());
		if (coveragePane != null) {
			if(debug) System.out.println("report to pane");
			coveragePane.reportModelSuspautNumbers(modelData.getModelStatistics(), suspautData.getModelStatistics());
			coveragePane.reportModelSuspautSum(modelData.getVisitSumStatistics(), suspautData.getVisitSumStatistics());
		}
		if(debug) System.out.println("report done");

	}
	
	public void setPane(CoveragePane p) {
		coveragePane = p;
		if(debug) System.out.println("coverage - setPane coverage: "+this+" pane: "+p);
		coveragePane.reportModelSuspautNumbers(modelData.getModelStatistics(), suspautData.getModelStatistics());
		coveragePane.reportModelSuspautSum(modelData.getVisitSumStatistics(), suspautData.getVisitSumStatistics());
		Vector<Statistics> modelVisits = modelData.getVisitStatistics();
		Vector<Statistics> suspautVisits = suspautData.getVisitStatistics();

		for(int i=0; i < modelVisits.size(); i++) {
			coveragePane.reportModelSuspautRun(i+1, modelVisits.get(i), suspautVisits.get(i));
		}
	}
	public void unsetPane() {
		coveragePane = null;
	}
	
	public void setErrorReporter(ErrorReporter r) {
		errReporter = r;
	}
	public void unsetErrorReporter() {
		errReporter = null;
	}

	public String getReport() {
		String res = "";
		res += modelData.getReport();
		res += suspautData.getReport();
		return res;
	}
	
	
	// implement Animator interface: this gives us the data for one run
	public void init(DriverInitializationResult r, String id) {
		System.out.println("coverage-logger-init: "+id+" has-verdict: "+r.hasVerdict());
		
		// do this here, and not in setModel, because when setModel is called
		// errReporter has not yet been set :-(
		if (errReporter != null) {
			if(debug) System.out.println("report to log");
			Statistics ms = modelData.getModelStatistics();
			Statistics ss = suspautData.getModelStatistics();
			errReporter.log(System.currentTimeMillis(), "MODELCOUNTS",
						"#modnode "+ms.nrOfNodes()+" "+
						"#modedge "+ms.nrOfEdges()+" "+
						"#sanode "+ss.nrOfNodes()+" "+
						"#saedge "+ss.nrOfEdges()+" "+
						"#sadeltaloop "+ss.nrOfEdgesByType()[EdgeType.DELTALOOP.ordinal()]+" "+
						"#sadeltanonloop "+ss.nrOfEdgesByType()[EdgeType.DELTANONLOOP.ordinal()]+" "+
						"#saotherloop "+ss.nrOfEdgesByType()[EdgeType.OTHERLOOP.ordinal()]+" "+
						"#saothernonloop "+ss.nrOfEdgesByType()[EdgeType.OTHERNONLOOP.ordinal()]+" "+
						"#modotherloop "+ms.nrOfEdgesByType()[EdgeType.OTHERLOOP.ordinal()]+" "+
						"#modothernonloop "+ms.nrOfEdgesByType()[EdgeType.OTHERNONLOOP.ordinal()]
						);
		}

		modelData.startRun(id);
		suspautData.startRun(id);
		if (r.hasVerdict())
			return;
		CompoundState cs = r.getState();
		if (cs instanceof GuidedState) {
			GuidedState gcs = (GuidedState) cs;
			CompoundState s = gcs.getCompoundState(lts);
			if (s != null)
				doHandleInitState(s);
			else
				System.err.println("coverage-logger: init null");
		} else {
			doHandleInitState(cs);
		}
	}
	private void doHandleInitState(CompoundState n) {
		suspautData.visitNode(n);
		Iterator<? extends State> sit = n.getSubStates();
		while (sit.hasNext()) {
			State subn = sit.next();
			modelData.visitNode(subn);
		}
		reportRun("coverage init report run");
	
	}
	

	public void addInterActionResult(DriverInterActionResult r, String pId, String id){
		if (r.hasVerdict())
			return;
		CompoundTransition<? extends CompoundState> cc = r.getTransition();
		if (cc instanceof GuidedTransition<?>) {
			GuidedTransition<?extends GuidedState> gcc = (GuidedTransition<?extends CompoundState>)cc;
			CompoundTransition<?extends CompoundState> t = gcc.getCompoundTransition(lts);
			if (t != null)
				doHandleInterActionTransition(t, pId, id);
		} else
			doHandleInterActionTransition(cc, pId, id);
	}
	private void doHandleInterActionTransition(CompoundTransition<? extends CompoundState> t, String pId, String id) {
		suspautData.visitEdge(t);
		Iterator<? extends Transition<?extends State>> tit = t.getSubTransitions();
		while (tit.hasNext()) {
			Transition<? extends State> subt = tit.next();
			modelData.visitEdge(subt);
		}
		
		CompoundState n = t.getDestination();
		suspautData.visitNode(n);
		Iterator<? extends State> sit = n.getSubStates();
		while (sit.hasNext()) {
			State subn = sit.next();
			modelData.visitNode(subn);
		}
		
		reportRun("coverage interactresult report run");	
	}
	
	private void reportRun(String what) {
		modelData.report();
		suspautData.report();
		if (coveragePane != null)
			coveragePane.reportModelSuspautSum(modelData.getVisitSumStatistics(), suspautData.getVisitSumStatistics());
		if(debug) System.err.println(what);
		if (coveragePane != null)
			coveragePane.reportModelSuspautRun(modelData.getVisitCount(), modelData.getLastVisitStatistics(), suspautData.getLastVisitStatistics());
		if (errReporter != null) {
			Statistics ms = modelData.getLastVisitStatistics();
			Statistics ss = suspautData.getLastVisitStatistics();
			errReporter.log(System.currentTimeMillis(), "COVERAGEVISIT",
						"#modnode "+ms.nrOfNodes()+" "+
						"#modedge "+ms.nrOfEdges()+" "+
						"#sanode "+ss.nrOfNodes()+" "+
						"#saedge "+ss.nrOfEdges()+" "+
						"#sadeltaloop "+ss.nrOfEdgesByType()[EdgeType.DELTALOOP.ordinal()]+" "+
						"#sadeltanonloop "+ss.nrOfEdgesByType()[EdgeType.DELTANONLOOP.ordinal()]+" "+
						"#saotherloop "+ss.nrOfEdgesByType()[EdgeType.OTHERLOOP.ordinal()]+" "+
						"#saothernonloop "+ss.nrOfEdgesByType()[EdgeType.OTHERNONLOOP.ordinal()]+" "+
						"#modotherloop "+ms.nrOfEdgesByType()[EdgeType.OTHERLOOP.ordinal()]+" "+
						"#modothernonloop "+ms.nrOfEdgesByType()[EdgeType.OTHERNONLOOP.ordinal()]
						);
		}
	}

	public void addLogMessage(long time, String src, String msg) {
	}
	public void addConfigMessage(long time, String src, String name, String val) {
	}
	public void end(DriverFinalizationResult e) {
		modelData.endRun();
		suspautData.endRun();
		modelData.report();
		suspautData.report();
	}
	public void setUpdateMode (UpdateMode m) {
	}
	
	public String getModelInfo() {
		return modelFileName;
	}
	public String getImplInfo() {
		return impl;
	}
	
	public void reset() {
		if(debug) System.err.println("CoverageData.reset start");
		modelData.resetVisitCounts();
		suspautData.resetVisitCounts();
		if (false && coveragePane != null) {
			coveragePane.reset();
			coveragePane.reportModelSuspautNumbers(modelData.getModelStatistics(), suspautData.getModelStatistics());
			coveragePane.reportModelSuspautSum(modelData.getVisitSumStatistics(), suspautData.getVisitSumStatistics());
		}
		if(debug) System.err.println("CoverageData.reset end");
	}
	
	public int modelStateVisitCount(State s) {
		return modelData.getNodeVisitCount(getNodeName(s));
	}
	public int modelTransitionVisitCount(Transition<?extends State> t) {
		return modelData.getEdgeVisitCount(new CoverageEdge(t));
	}
	public int saStateVisitCount(State s) {
		return suspautData.getNodeVisitCount(getNodeName(s));
	}
	public int saTransitionVisitCount(Transition<?extends State> t) {
		return suspautData.getEdgeVisitCount(new CoverageEdge(t));
	}
	public int saTransitionCount() {
		return suspautData.getEdgeCount();
	}
	

	// when do we create a new CoverageData?
	// do we have a separate CoverageData for each different model?
	// when model is changed?
	public CoverageData(String f, LTS lts, String i) {
		System.out.println("CoverageData("+f+", "+i+")");
		modelFileName = f;
		impl = i;
		this.lts = lts;
	}
	
	static private boolean debug = false;
	private ErrorReporter errReporter = null;
	
	private IOLTSInterpretation interp = null;
	
	private LTSCoverage modelData = new LTSCoverage("mdl");
	private LTSCoverage suspautData = new LTSCoverage("sa");
	
	private String modelFileName;
	private String impl;
	
	private CoveragePane coveragePane = null;
	private LTS lts;
}
