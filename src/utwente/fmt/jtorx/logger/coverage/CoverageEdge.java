package utwente.fmt.jtorx.logger.coverage;

import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class CoverageEdge {
	
	public String getSource() {
		return src;
	}
	public String getLabel() {
		return lbl;
	}
	public String getDestination() {
		return dst;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		if (o instanceof CoverageEdge) {
			CoverageEdge s = (CoverageEdge) o;
			return (s.src.equals(src) &&
					s.lbl.equals(lbl) &&
					s.dst.equals(dst));
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == src ? 0 : src.hashCode());
		hash = 31 * hash + (null == lbl ? 0 : lbl.hashCode());
		hash = 31 * hash + (null == dst ? 0 : dst.hashCode());
		// hash = 31 * hash + (null == allStates ? 0 : allStates.hashCode());
		return hash;
	}

	public CoverageEdge(Transition<?extends State> t) {
		src = CoverageData.getNodeName(t.getSource());
		lbl = t.getLabel().getString();
		dst = CoverageData.getNodeName(t.getDestination());
	}
	private String src;
	private String lbl;
	private String dst;
}

