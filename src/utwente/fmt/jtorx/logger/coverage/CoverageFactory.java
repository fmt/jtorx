package utwente.fmt.jtorx.logger.coverage;

import java.util.HashMap;

import utwente.fmt.lts.LTS;

public class CoverageFactory {
	
	
	public static CoverageData getCoverageData(String model, LTS lts, String impl) {
		CoverageKey k = new CoverageKey(model, impl);
		CoverageData r = map.get(k);
		if (r==null) {
			r = new CoverageData(model, lts, impl);
			map.put(k, r);
		}
		return r;
	}
	
	public static CoverageData getCoverageData(String model, String impl) {
		CoverageKey k = new CoverageKey(model, impl);
		CoverageData r = map.get(k);
		return r;
	}
	
	public static void removeCoverageData(String model, String impl) {
		CoverageKey k = new CoverageKey(model, impl);
		CoverageData r = map.get(k);
		if (r!=null) {
			map.remove(r);
		}
	}
	
	private static HashMap<CoverageKey,CoverageData> map = new HashMap<CoverageKey,CoverageData>();

}
