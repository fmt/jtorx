package utwente.fmt.jtorx.logger.coverage;

public class CoverageKey {
	
	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		if (o instanceof CoverageKey) {
			CoverageKey k = (CoverageKey) o;
			if (k.model!=null && k.model.equals(model) && k.impl!=null && k.impl.equals(impl))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == model ? 0 : model.hashCode());
		hash = 31 * hash + (null == impl ? 0 : impl.hashCode());
		return hash;
	}

	public CoverageKey(String m, String i) {
		model = m;
		impl = i;
	}
	private String model;
	private String impl;
}
