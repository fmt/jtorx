package utwente.fmt.jtorx.logger.coverage;

import java.util.HashMap;
import java.util.Vector;

import utwente.fmt.jtorx.logger.coverage.CoverageData.EdgeType;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class LTSCoverage {
	
	
	public void addNode(State s) {
		if (debug) System.out.println("addNode "+id+"");
		String n = CoverageData.getNodeName(s);
		if (!nodeVisits.containsKey(n)) {
			nodeVisits.put(n, 0);
			items.nrOfNodes++;
		}
	}
	public void visitNode(State s) {
		String n = CoverageData.getNodeName(s);
		if (!nodeVisits.containsKey(n)) {
			if (debug) System.out.println("visit-node "+id+": node not found: \""+n+"\" state: "+s.getID());
			return;
		}
		int i = nodeVisits.get(n);
		if (i==0) {
			visits.lastElement().nrOfNodes++;
			sums.nrOfNodes++;
		}
		nodeVisits.put(n, i+1);
		
	}
	public EdgeType getEdgeType(Transition<?extends State> t) {
		EdgeType tp;
		if (t.getSource().equals(t.getDestination())) {
			if (t.getLabel().getString().trim().equalsIgnoreCase("delta"))
				tp = EdgeType.DELTALOOP;
			else
				tp = EdgeType.OTHERLOOP;
		} else {
			if (t.getLabel().getString().trim().equalsIgnoreCase("delta"))
				tp = EdgeType.DELTANONLOOP;
			else
				tp = EdgeType.OTHERNONLOOP;
		}
		return tp;
	}
	public void addEdge(Transition<?extends State> t) {
		CoverageEdge ce = new CoverageEdge(t);
		if (!edgeVisits.containsKey(ce)) {
			if (debug) System.err.println("addEdge "+id+": new: "+t.getEdgeName()+": "+ce.getSource()+"--"+t.getLabel().getString()+"-->"+ce.getDestination());
			edgeVisits.put(ce, 0);
			items.nrOfEdges++;
			EdgeType tp = getEdgeType(t);
			items.nrOfEdgesByType[tp.ordinal()]++;
		} else {
			if (debug) System.err.println("addEdge "+id+": known: "+t.getEdgeName()+": "+ce.getSource()+"--"+t.getLabel().getString()+"-->"+ce.getDestination());

		}
	}
	public void visitEdge(Transition<?extends State> t) {
		CoverageEdge ce = new CoverageEdge(t);
		if (!edgeVisits.containsKey(ce)) {
			if(debug) System.err.println("unknown edge "+id+": "+t.getEdgeName()+": "+ce.getSource()+"--"+t.getLabel().getString()+"-->"+ce.getDestination());
			return;
		}
		int i = edgeVisits.get(ce);
		if (i==0) {
			visits.lastElement().nrOfEdges++;
			sums.nrOfEdges++;
			EdgeType tp = getEdgeType(t);
			visits.lastElement().nrOfEdgesByType[tp.ordinal()]++;
			sums.nrOfEdgesByType[tp.ordinal()]++;
		}
		edgeVisits.put(ce, i+1);
	}
	
	public void setExpanded(boolean v) {
		isExpanded = v;
	}
	public boolean isExpanded() {
		return isExpanded;
	}

	public void report() {
		if (debug) System.out.println(getReport());
	}
	
	public String getReport() {
		int n=0, e=0, ql=0, l=0, qt=0, t=0;
		String res = "";
		res += "\t"+"#Nodes"+"\t"+"#Edges"+"\t"+"#Qloops"+"\t"+"loops"+"\t"+"Qtrans"+"\t"+"trans";
		res += id+
				"\t"+items.nrOfNodes+
				"\t"+items.nrOfEdges+
				"\t"+items.nrOfEdgesByType[EdgeType.DELTALOOP.ordinal()]+
				"\t"+items.nrOfEdgesByType[EdgeType.OTHERLOOP.ordinal()]+
				"\t"+items.nrOfEdgesByType[EdgeType.DELTANONLOOP.ordinal()]+
				"\t"+items.nrOfEdgesByType[EdgeType.OTHERNONLOOP.ordinal()];
		for(Statistics s: visits) {
			res += s.getId()+
					"\t"+s.nrOfNodes+
					"\t"+s.nrOfEdges+
					"\t"+s.nrOfEdgesByType[EdgeType.DELTALOOP.ordinal()]+
					"\t"+s.nrOfEdgesByType[EdgeType.OTHERLOOP.ordinal()]+
					"\t"+s.nrOfEdgesByType[EdgeType.DELTANONLOOP.ordinal()]+
					"\t"+s.nrOfEdgesByType[EdgeType.OTHERNONLOOP.ordinal()];
			n += s.nrOfNodes;
			e += s.nrOfEdges;
			ql += s.nrOfEdgesByType[EdgeType.DELTALOOP.ordinal()];
			l += s.nrOfEdgesByType[EdgeType.OTHERLOOP.ordinal()];
			qt += s.nrOfEdgesByType[EdgeType.DELTANONLOOP.ordinal()];
			t += s.nrOfEdgesByType[EdgeType.OTHERNONLOOP.ordinal()];
		}
		res += id+"-sum"+
				"\t"+n+
				"\t"+e+
				"\t"+ql+
				"\t"+l+
				"\t"+qt+
				"\t"+t;
		return res;
	}
	
	public void startRun(String id) {
		visits.add(new Statistics(this.id+"-"+id));
	}
	public void endRun() {}
	
	public void resetVisitCounts() {
		for(String k: nodeVisits.keySet())
			nodeVisits.put(k, 0);
		for(CoverageEdge k: edgeVisits.keySet())
			edgeVisits.put(k, 0);
		visits.clear();
		sums.reset();
	}
	
	public void resetModelStatistics() {
		items.reset();
	}
	
	public Statistics getModelStatistics() {
		return items;
	}
	public Statistics getVisitSumStatistics() {
		return sums;
	}
	public Vector<Statistics> getVisitStatistics() {
		return visits;
	}
	public int getVisitCount() {
		return visits.size();
	}
	public Statistics getLastVisitStatistics() {
		return visits.lastElement();
	}
	public int getNodeVisitCount(String s) {
		if (!nodeVisits.containsKey(s))
				return 0;
		return nodeVisits.get(s);
	}
	public int getEdgeVisitCount(CoverageEdge e) {
		if (!edgeVisits.containsKey(e))
				return 0;
		return edgeVisits.get(e);
	}
	public int getEdgeCount() {
		return items.nrOfEdges();
	}

	
	public LTSCoverage(String id) {
		this.id = id;
		items = new Statistics(id);
		visits = new Vector<Statistics>();
		sums = new Statistics("sum");
	}

	private HashMap<String,Integer>nodeVisits = new HashMap<String,Integer>();
	private HashMap<CoverageEdge,Integer>edgeVisits = new HashMap<CoverageEdge,Integer>();
	private Statistics items;
	private Statistics sums;
	private Vector<Statistics> visits;
	private boolean isExpanded = false;
	private String id;
	private boolean debug = false;
}

