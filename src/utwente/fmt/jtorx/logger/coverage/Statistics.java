package utwente.fmt.jtorx.logger.coverage;

import utwente.fmt.jtorx.logger.coverage.CoverageData.EdgeType;

public class Statistics {
	int nrOfNodes = 0;
	int nrOfEdges = 0;
	int nrOfEdgesByType[] = new int[EdgeType.NEdgeType.ordinal()];
	
	public void reset() {
		nrOfNodes = 0;
		nrOfEdges = 0;
		for(int i=0; i < EdgeType.NEdgeType.ordinal(); i++)
			nrOfEdgesByType[i] = 0;
	}
	
	public String getReport() {
		String res = "";
		res += id+" "+"#Nodes: "+nrOfNodes;
		res += id+" "+"#Edges: "+nrOfEdges;
		res += id+" "+"#delta selfloops: "+nrOfEdgesByType[EdgeType.DELTALOOP.ordinal()];
		res += id+" "+"#other selfloops: "+nrOfEdgesByType[EdgeType.OTHERLOOP.ordinal()];
		res += id+" "+"#delta nonloops: "+nrOfEdgesByType[EdgeType.DELTANONLOOP.ordinal()];
		res += id+" "+"#other nonloops: "+nrOfEdgesByType[EdgeType.OTHERNONLOOP.ordinal()];
		return res;
	}
	
	public String getId() {
		return id;
	}
	
	public int nrOfNodes() { return nrOfNodes; }
	public int nrOfEdges() { return nrOfEdges; }
	public int[] nrOfEdgesByType() { return nrOfEdgesByType; }
	public String getID() { return id; }
	
	public Statistics(String id) {
		this.id = id;
	}
	
	private String id;
}
