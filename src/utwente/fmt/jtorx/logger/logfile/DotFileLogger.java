package utwente.fmt.jtorx.logger.logfile;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import utwente.fmt.jtorx.logger.Animator;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.State;
import utwente.fmt.lts.SuspensionAutomatonState;
import utwente.fmt.lts.Transition;

public class DotFileLogger implements Animator {

	// TODO do something with id?
	public void init(DriverInitializationResult s, String id) {
		CompoundState cs = s.getState();
		write("// "+stepNr+" "+cs.getID()+"\n");
		writeState(cs);
	}

	// do something with pId and id?
	public void addInterActionResult(DriverInterActionResult l, String pId, String id) {
		stepNr++;
		CompoundTransition<? extends State> cc = l.getTransition();
		if (cc != null) {
			CompoundState cs = cc.getDestination();
			write("\n");
			write("// "+stepNr+" "+cs.getID()+"\n");
			writeTransition(cc);
			writeState(cs);
		} else {
			write("// "+stepNr+" "+"(null)"+"\n");
		}
		prevNr++;
	}
	
	public void addLogMessage(long t, String src, String msg) {
	}
	public void addConfigMessage(long t, String src, String name, String val) {
	}


	public void end(DriverFinalizationResult r) {
		System.err.println("DotFileLogger-end "+stepNr);
		write("}\n");
		if (f!=null) {
			try {
				f.close();
			} catch (IOException e) {
				System.err.println("error closing log file ");
				e.printStackTrace();
			}
			f = null;
		}
	}
	
	// TODO to something?
	public void setUpdateMode(Animator.UpdateMode m) {
	}

	public DotFileLogger(String fileName) {
		System.err.println("DotFileLogger-constructor ");
	
		try {
			f = new BufferedWriter(new FileWriter(fileName));
		} catch (IOException e) {
			System.err.println("DotFileLogger could not create log file "+fileName);
			e.printStackTrace();
			f = null;
		}
		
		write("digraph jtorx {\n");

	}
	
	private void writeState(CompoundState cs) {
		if (useClusters) {
			write("subgraph cluster_step"+stepNr+"{"+"\n");
			write("\t"+"label="+stepNr);
		}
		if (cs instanceof SuspensionAutomatonState ) {
			SuspensionAutomatonState sas = (SuspensionAutomatonState) cs;
			Iterator<? extends State> it;
			it = sas.getDirectSubStates();
			if (it.hasNext()) {
				if (useSubClusters)
					write("\t"+"{"+"\n");
				for (; it.hasNext(); ) {
					State s = it.next();
					write("\t\t"+mkDstId(s.getID())+"[label=\""+s.getID()+"\"]"+"\n");
				}
				if (useSubClusters) {
					write("\t\t"+"[rank=same]"+"\n");
					write("\t"+"}"+"\n");
				}
			}
			it = sas.getIndirectSubStates();
			if  (it.hasNext()) {
				if (useSubClusters)
					write("\t"+"{"+"\n");
				for (;  it.hasNext(); ) {
					State s = it.next();
					write("\t\t"+mkDstId(s.getID())+"[label=\""+s.getID()+"\"]"+"\n");
				}
				if (useSubClusters) {
					write("\t\t"+"[rank=same]"+"\n");
					write("\t"+"}"+"\n");
				}
			}
		}

		// tau transition
		for (Iterator<? extends Transition<?extends State>> it = cs.getSubTransitions(); it.hasNext(); ) {
			Transition<?extends State> t = it.next();
			write("\t"+mkDstId(t.getSource().getID())+" -> "+mkDstId(t.getDestination().getID())+" [style=dashed,color=red,label=\""+t.getLabel().getString()+"\"]"+"\n");
		}
		
		if (useClusters)
			write("}"+"\n");

	}
	
	private void writeTransition(CompoundTransition<? extends State> cc) {
		Iterator<? extends Transition<?extends State>> it = cc.getSubTransitions();
		if (it.hasNext())
			// transition from the model
			for (; it.hasNext(); ) {
				Transition<?extends State> t = it.next();
				write("\t"+mkSrcId(t.getSource().getID())+" -> "+mkDstId(t.getDestination().getID())+" [label=\""+t.getLabel().getString()+"\"]"+"\n");
			}
		else {
			// delta transition
			Iterator<? extends State> sit = cc.getDestination().getSubStates();
			for (; sit.hasNext(); ) {
				State s = sit.next();
				write("\t"+mkSrcId(s.getID())+" -> "+mkDstId(s.getID())+" [style=dashed,color=blue,label=\""+cc.getLabel().getString()+"\"]"+"\n");
			}
		}
	}
	
	private String mkSrcId(String s) {
		return "\""+prevNr+"_"+s+"\"";
	}
	private String mkDstId(String s) {
		return "\""+stepNr+"_"+s+"\"";
	}


	private void write(String s) {
		if (f!=null) {
			try {
				f.write(s);
				f.flush();
			} catch (IOException e) {
				System.err.println("DotFileLogger could not write to log file ");
				e.printStackTrace();
				f = null;
			}
		}
	}
	
	private int stepNr = 0;
	private int prevNr = 0;
	private BufferedWriter f;
	private boolean useClusters =  true;
	private boolean useSubClusters =  true;

}
