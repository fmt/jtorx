package utwente.fmt.jtorx.logger.logfile;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import utwente.fmt.jtorx.logger.Animator;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.State;
import utwente.fmt.lts.SuspensionAutomatonState;

public class FileLogger implements Animator {

	// TODO do something with id?
	public void init(DriverInitializationResult s, String id) {
		System.err.println("FileLogger-init "+stepNr);
		String stmp = fmt.format(s.getEndTime());

		CompoundState cs = s.getState();
		if (cs==null) {
			write(stmp+" "+"ERROR "+stepNr+"\n");
			return;
		}
		writeStats(stmp+" ", cs);
		write(stmp+" "+"STATEID "+stepNr+" "+cs.getID()+"\n");
		
	}

	// TODO do something with pId and id?
	public void addInterActionResult(DriverInterActionResult l, String pId, String id) {
		stepNr++;
		System.err.println("FileLogger-addIR "+stepNr);

		CompoundTransition<? extends State> cc = l.getTransition();
		if (cc != null) {
			CompoundState cs = cc.getDestination();
			String stmp = fmt.format(l.getEndTime());
			writeStats(stmp+" ", cs);
			write("ABSTRACT "+stepNr+" "+cc.getLabel().getString()+"\n");
			write(stmp+" "+"STATEID "+stepNr+" "+cs.getID()+"\n");
		}
	}
	
	public void addLogMessage(long t, String src, String msg) {
		String stmp = fmt.format(t);
		write(stmp+" "+"LOG "+stepNr+" ("+src+") "+msg+"\n");
	}
	public void addConfigMessage(long t, String src, String name, String val) {
		String stmp = fmt.format(t);
		write(stmp+" "+"CONFIG "+stepNr+" ("+
				(src==null?"":src)+
				") "+name+"="+val+"\n");
	}

	public void end(DriverFinalizationResult r) {
		System.err.println("FileLogger-end "+stepNr);
		if (f!=null) {
			try {
				f.close();
			} catch (IOException e) {
				System.err.println("error closing log file ");
				e.printStackTrace();
			}
			f = null;
		}
	}
	
	// TODO to something?
	public void setUpdateMode(Animator.UpdateMode m) {
	}

	public FileLogger(String fileName) {
		System.err.println("FileLogger-constructor ");
	
		try {
			f = new BufferedWriter(new FileWriter(fileName));
		} catch (IOException e) {
			System.err.println("could not create log file "+fileName);
			e.printStackTrace();
			f = null;
		}

	}
	
	private void writeStats(String pfx, CompoundState cs) {
		int subStateCount  = 0;
		int directSubStateCount = 0;
		int indirectSubStateCount = 0;
		for (Iterator<? extends State> it = cs.getSubStates(); it.hasNext(); ) {
			it.next();
			subStateCount++;
			// System.err.println("\tFileLogger-init "+stepNr+" "+subStateCount);
		}
		String s = pfx+"STATS "+stepNr+" #states "+subStateCount;
		
		if (cs instanceof SuspensionAutomatonState ) {
			SuspensionAutomatonState sas = (SuspensionAutomatonState) cs;
			for (Iterator<? extends State> it = sas.getDirectSubStates(); it.hasNext(); ) {
				it.next();
				directSubStateCount++;
			}
			for (Iterator<? extends State> it = sas.getIndirectSubStates(); it.hasNext(); ) {
				it.next();
				indirectSubStateCount++;
			}
			s += " #init "+directSubStateCount;
			if (indirectSubStateCount > 0)
				s += " #trans "+indirectSubStateCount;
		}
		write(s+"\n");

	}
	
	private void write(String s) {
		if (f!=null) {
			try {
				f.write(s);
				f.flush();
			} catch (IOException e) {
				System.err.println("could not write to log file ");
				e.printStackTrace();
				f = null;
			}
		}
	}
	
	private int stepNr = 0;
	private BufferedWriter f;
	private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");


}
