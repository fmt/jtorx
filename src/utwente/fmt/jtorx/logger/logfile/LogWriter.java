package utwente.fmt.jtorx.logger.logfile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import utwente.fmt.jtorx.logger.Animator;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.SuspensionAutomatonState;

public class LogWriter implements Animator {

	// TODO do something with id?
	public void init(DriverInitializationResult s, String id) {
		if(debug) System.err.println("LogWriter-init "+stepNr);
		String stmp = fmt.format(s.getEndTime());

		CompoundState cs = s.getState();
		if (cs==null) {
			write(stmp+" "+"ERROR "+stepNr+"\n");
			return;
		}
		writeStats(stmp+" ", cs);
		write(stmp+" "+"STATEID "+stepNr+" "+cs.getID()+"\n");
		write(stmp+" "+"STATELABEL "+stepNr+" "+cs.getLabel(false)+"\n");
		write(stmp+" "+"STATENAME "+stepNr+" "+cs.getNodeName()+"\n");
		
	}

	// do something with pId and id?
	public void addInterActionResult(DriverInterActionResult l, String pId, String id) {
		stepNr++;
		if(debug) System.err.println("LogWriter-addIR "+stepNr);

		String stmp = fmt.format(l.getEndTime());
		CompoundTransition<? extends State> cc = l.getTransition();
		if (cc != null) {
			CompoundState cs = cc.getDestination();
			writeStats(stmp+" ", cs);
			write(stmp+" "+"ABSTRACT "+stepNr+" "+cc.getLabel().getString()+"\n");
			String c = l.getConcrete();
			if (c!=null)
				write(stmp+" "+"CONCRETE "+stepNr+" "+c+"\n");
			write(stmp+" "+"STATEID "+stepNr+" "+cs.getID()+"\n");
			write(stmp+" "+"STATELABEL "+stepNr+" "+cs.getLabel(false)+"\n");
			write(stmp+" "+"NODENAME "+stepNr+" "+cs.getNodeName()+"\n");
			write(stmp+" "+"EDGENAME "+stepNr+" "+cc.getEdgeName()+"\n");
			String sen = cs.getSubEdgeName();
			if (sen != null)
				write(stmp+" "+"SUBEDGENAME "+stepNr+" "+sen+"\n");
		}
		write(stmp+" "+"TIMESTAMP "+stepNr+" "+l.getTimeStamp()+"\n");
		write(stmp+" "+"KIND "+stepNr+" "+l.getKind()+"\n");

	}

	public void end(DriverFinalizationResult r) {
		if(debug) System.err.println("LogWriter-end "+stepNr);
		String stmp = fmt.format(r.getEndTime());
		Iterator<?extends Label> expected = r.getExpected();
		if (r.hasVerdict())
			write(stmp+" "+"VERDICT "+stepNr+" "+r.getVerdict().getString()+"\n");
		if (expected!=null) {
			while(expected.hasNext()) {
				Label l = expected.next();
				write(stmp+" "+"EXPECTED "+stepNr+" "+l.getString()+
						(l.getConstraint()!=null?" ["+l.getConstraint().getString()+"]":"")+
						"\n");
			}
		}
		write(stmp+" "+"EOT "+stepNr+"\n");
	}
	
	// TODO to something?
	public void setUpdateMode(Animator.UpdateMode m) {
	}
	
	public void addLogMessage(long t, String src, String msg) {
		String stmp = fmt.format(t);
		write(stmp+" "+"LOG "+stepNr+" ("+src+") "+msg+"\n");
	}
	public void addConfigMessage(long t, String src, String name, String val) {
		String stmp = fmt.format(t);
		write(stmp+" "+"CONFIG "+stepNr+" ("+
				(src==null?"":src)+
				") "+name+"="+val+"\n");
	}


	public LogWriter(Appendable a) {
		if(debug) System.err.println("LogWriter-constructor ");
		app = a;
	}

	
	private void writeStats(String pfx, CompoundState cs) {
		int subStateCount  = 0;
		int directSubStateCount = 0;
		int indirectSubStateCount = 0;
		for (Iterator<? extends State> it = cs.getSubStates(); it.hasNext(); ) {
			it.next();
			subStateCount++;
			// System.err.println("\tLogWriter-init "+stepNr+" "+subStateCount);
		}
		String s = pfx+"STATS "+stepNr+" #states "+subStateCount;
		
		if (cs instanceof SuspensionAutomatonState ) {
			SuspensionAutomatonState sas = (SuspensionAutomatonState) cs;
			for (Iterator<? extends State> it = sas.getDirectSubStates(); it.hasNext(); ) {
				it.next();
				directSubStateCount++;
			}
			for (Iterator<? extends State> it = sas.getIndirectSubStates(); it.hasNext(); ) {
				it.next();
				indirectSubStateCount++;
			}
			s += " #init "+directSubStateCount;
			if (indirectSubStateCount > 0)
				s += " #trans "+indirectSubStateCount;
		}
		write(s+"\n");

	}
	
	private void write(String s) {
		if (app!=null) {
			try {
				app.append(s);
			} catch (IOException e) {
				System.err.println("could not append to log: "+e.getMessage());
				// e.printStackTrace();
				app = null;
			}
		}
	}
	
	private Appendable app;
	private int stepNr = 0;
	private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	private boolean debug = false;

}
