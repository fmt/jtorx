package utwente.fmt.jtorx.logger.msc;

import java.io.DataOutputStream;
import java.io.IOException;

public class Msc {
	
	public void write(String s) {
		if (out == null)
			return;
		try {
				out.write(s.getBytes());
		} catch (IOException e) {
			cleanup();
		}
	}
	public void flush() {
		if (out == null)
			return;
		try {
				out.flush();
		} catch (IOException e) {
			cleanup();
		}
	}

	public void end() {
		if (out == null)
			return;
		try {
			out.close();
		} catch (IOException e) {
			cleanup();
		}
	}
	
	public Msc(DataOutputStream o, String name) {
		out = o;
	}
	
	private void cleanup() {
		try {
			out.close();
			out = null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private DataOutputStream out;
}

