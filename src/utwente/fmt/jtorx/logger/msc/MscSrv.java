package utwente.fmt.jtorx.logger.msc;

import java.io.DataOutputStream;


import utwente.fmt.jtorx.logger.AniServer;
import utwente.fmt.jtorx.logger.mux.Multicast;
import utwente.fmt.jtorx.ui.ErrorReporter;


public class MscSrv extends AniServer {

	public Msc newMsc(String title, Boolean reuse, Multicast mc) {
		DataOutputStream o = newAnimator(title, null, reuse, mc);
		if (o == null)
			return null;
		return new Msc(o, title);
	}

	public MscSrv(ErrorReporter r) {
		super(r, "animsc.kit");
	}

}
