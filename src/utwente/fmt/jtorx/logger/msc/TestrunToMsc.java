package utwente.fmt.jtorx.logger.msc;

// TODO
//  - delta  -> (Quiescence) for outputs
//  - delta  -> (Refusal)   for inputs
//  - add 'Expected' (but this then needs to be made available)

import java.util.HashMap;
import java.util.Iterator;


import utwente.fmt.jtorx.logger.Animator;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.InterActionResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.torx.LabelPlusVerdict;
import utwente.fmt.lts.Label;
import utwente.fmt.test.Verdict;

public class TestrunToMsc implements Animator {

	// TODO use id
	public void init(DriverInitializationResult s, String id) {
	}

	// TODO use pId and id
	public void addInterActionResult(DriverInterActionResult l, String pId, String id) {
		if (msc == null)
			return;

		String iut, tst;
		iut = addInstance("iut");
		if (!l.getPCO().equals(""))
			tst = addInstance(l.getPCO());
		else if (!l.getChan().equals(""))
			tst = addInstance("("+l.getChan()+")");
		else
			tst = addInstance("");
		String label = escape(l.getLabel().getString());
		if (l.getKind() == InterActionResult.Kind.INPUT) {
			msc.write(tst+": out \""+label+"\" to "+iut+";\n");
			msc.write(iut+": in \""+label+"\" from "+tst+";\n");
		} else {
			msc.write(iut+": out \""+label+"\" to "+tst+";\n");
			msc.write(tst+": in \""+label+"\" from "+iut+";\n");
		}
		msc.flush();
	}

	public void end(DriverFinalizationResult e) {
		if (msc == null)
			return;
		
		Verdict v = e.getVerdict();
		if (v!=null) {
			String iut, tst;
			iut = addInstance("iut");
			tst = addInstance("(exp)");
			msc.write(iut+": out \"Verdict: "+
					v.getString()+
					"\" to "+tst+";\n");
			msc.write(tst+": in \"Verdict: "+
					v.getString()+
					"\" from "+iut+";\n");
		}
		
		Iterator<? extends Label> exp_it = e.getExpected();
		if (exp_it!=null) {
			while (exp_it.hasNext()) {
				String iut, tst;
				iut = addInstance("iut");
				tst = addInstance("(exp)");

				Label l = exp_it.next();
				String lv = "";
				if (l instanceof LabelPlusVerdict) {
					LabelPlusVerdict lpv = (LabelPlusVerdict)l;
					if (lpv.getVerdict() == null) {
						lv = " : "+"("+e.getPositiveDefaultVerdict().getString()+")";
					}
				}
				msc.write(iut+": out \"Expected: "+
						l.getString()+
						(l.getConstraint()!=null?" ["+l.getConstraint().getString()+"]":"")+
						lv+
						"\" to "+tst+";\n");
				msc.write(tst+": in \"Expected: "+
						l.getString()+
						(l.getConstraint()!=null?" ["+l.getConstraint().getString()+"]":"")+
						lv+
						"\" from "+iut+";\n");
			}
			msc.flush();
		}
		
		for (Iterator<String> it = instances.keySet().iterator(); it.hasNext(); )
			msc.write(it.next()+": endinstance;\n");
		msc.write("endmsc;\n");
		msc.flush();
		
		msc.end();
	}
	
	// TODO to something?
	public void setUpdateMode(Animator.UpdateMode m) {
	}

	
	public void addLogMessage(long t, String src, String msg) {
	}
	public void addConfigMessage(long t, String src, String name, String val) {
	}



	public TestrunToMsc(Msc m, String n) {
		msc = m;
		name = n.replace(':', '_'); // avoid syntax error issue in msc viewer

		if (msc == null)
			return;

		msc.write("msc \""+name+"\";\n");
		msc.flush();
		addInstance("iut");
		msc.flush();

	}
	
	private String addInstance(String n) {
		if (! instances.containsKey(n)) {
			defineInstance(n);
			String i = instances.get(n);
			msc.write("inst "+i+";\n");
			msc.write(i+": instancehead \""+n+"\";\n");
		}
		return instances.get(n);
	}
	private void defineInstance(String n) {
		boolean normalize = true;
		String instName = "";
		if (normalize) {
			int nr = instances.size();
			instName += "i"+nr+"_";
			if (n.equals("iut"))
				; // nothing to do
			else if (n.startsWith("("))
				instName += "chan";
			else
				instName += "pco";
			instName += "_"+stripOuterParentheses(n);
			instances.put(n, instName);
		}
	}
	private String stripOuterParentheses(String n) {
		if (n.startsWith("(") && n.endsWith(")")) {
			int ln = n.length();
			return n.substring(1, ln-1);
		}
		else
			return n;
	}
	
	private String escape(String s) {
		int i=0;
		char c;
		String res = "";
		while(i<s.length()) {
			c = s.charAt(i);
			i++;
			if (c == '\\')
				res += "\\\\";
			else if (c == '"')
				res += "\\\"";
			else
				res += c;
		}
		return res;
	}


	private Msc msc;
	private String name;
	private HashMap<String,String> instances = new HashMap<String,String>();

}

