package utwente.fmt.jtorx.logger.mux;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import java.util.Map.Entry;

import utwente.fmt.jtorx.logger.Animator;
import utwente.fmt.jtorx.logger.anidot.Anidot;
import utwente.fmt.jtorx.logger.anidot.AnidotSrv;
import utwente.fmt.jtorx.logger.anidot.DotLTS;
import utwente.fmt.jtorx.logger.anidot.ImplToDot;
import utwente.fmt.jtorx.logger.anidot.ModelToDot;
import utwente.fmt.jtorx.logger.anidot.SuspAutToDot;
import utwente.fmt.jtorx.logger.anidot.TestrunToDot;
import utwente.fmt.jtorx.logger.msc.Msc;
import utwente.fmt.jtorx.logger.msc.MscSrv;
import utwente.fmt.jtorx.logger.msc.TestrunToMsc;
import utwente.fmt.jtorx.testgui.Config;
import utwente.fmt.jtorx.testgui.RunItemData;
import utwente.fmt.jtorx.testgui.RunSessionData;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.writers.AniDotLTSWriter;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class LogMux {

	class LogMessage {

		public LogMessage(long time, String src, String s) {
			this.time = time;
			this.src = src;
			this.s = s;
		}
		long time;
		String src;
		String s;
	}
	
	public static enum Kind {
		MODEL, IMPL, GUIDE, SUSPAUT, TRACE;
	};
	
	
	public void addInterActionResult(DriverInterActionResult ll, String pId, String id) {
		for (Animator a: anims)
			a.addInterActionResult(ll, pId, id);

		// NOTE: the remainder is only here for debugging: prints info to stdout
		if(debug) System.out.println("step "+ll.getStepNr());
		printInteractionResult(ll);
	}

	public void prepareForSession() {
		pending = new Vector<LogMessage>();	
	}
	
	public void startSession(String t, String mt, String it, DriverInitializationResult s, Anidot am, DotLTS dm, String id) {
		multicastCur = new Multicast();
		allMulticast.add(multicastCur);
		System.err.println("multicast addr "+multicastCur.getAddress());
		
		if (logmsc != null)
			addAnimator(new TestrunToMsc(logmsc.newMsc(t, true, multicastCur), t));
	
		if (loganidot != null) {
			addAnimator(new TestrunToDot(loganidot.newAnidot(t, "Testrun", true, multicastCur), t));
			if (am == null)
				am = loganidot.newAnidot(mt, "Model", true, multicastCur);
			addAnimator(new ModelToDot(am, mt, dm, null));
		}
		
		for (Animator a: anims)
			a.init(s, id);
	}


	public void startSession(RunSessionData d, String id) {
		
		sessionData = d;
		long curTime = System.currentTimeMillis();
		
		if ((logmsc!=null && logmsc.isRunning()) && (loganidot!=null && loganidot.isRunning())) {

			System.out.println("startSession spec "+d.getModelLTS()+" guide "+d.getGuideLTS());

			multicastCur = new Multicast(d.getHighlighter());
			allMulticast.add(multicastCur);
			System.err.println("multicast addr "+multicastCur.getAddress());

			if (d.getVizMsc() && logmsc != null) {
				Msc myMsc = logmsc.newMsc(d.getTitle(), true, multicastCur);
				if (myMsc !=  null)
					addAnimator(new TestrunToMsc(myMsc, d.getTitle()));
			}

			if (d.getVizRun() && loganidot != null) {
				Anidot testrunAnidot = loganidot.newAnidot(d.getTitle(), "Testrun", true, multicastCur);
				if (testrunAnidot != null)
					addAnimator(new TestrunToDot(testrunAnidot, d.getTitle()));
			}

			if (d.getVizModel() && loganidot != null)
				startDotAnimator(d.getModelTitle(), Kind.MODEL, d.getModelWriter(), d.getModelLTS(), d.getModelData());

			if (d.getVizGuide() && loganidot != null)
				startDotAnimator(d.getGuideTitle(), Kind.GUIDE, d.getGuideWriter(), d.getGuideLTS(), d.getGuideData());

			if (d.getVizImpl() && loganidot != null)
				startDotAnimator(d.getImplTitle(), Kind.IMPL, d.getImplWriter(), d.getImplLTS(), d.getImplData());

			if (d.getVizSuspAut() && loganidot != null && d.getSuspAutLTS()!=null)
				startDotAnimator(d.getSuspAutTitle(), Kind.SUSPAUT, d.getSuspAutWriter(), d.getSuspAutLTS());

			if (d.getVizTrace() && loganidot != null && d.getTraceLTS()!=null)
				startDotAnimator(d.getTraceTitle(), Kind.TRACE, d.getTraceWriter(), d.getTraceLTS());

			if (anims.size() < 1) {
				multicastCur.end();
				multicastCur = null;
			}

			// following two lines write log- and dot-files;
			// the file names should be given from the gui.
			// until we extend the gui they are commented out.
			//	addAnimator(new FileLogger("/tmp/jtorx-run.log"));
			//	addAnimator(new DotFileLogger("/tmp/jtorx-run.gv"));

		}
		
		Config c = new Config();
		d.fillConfig(c);
		for (Animator a: anims)
			for (Entry<String,String> p: c.getConfig())
				a.addConfigMessage(curTime, null, p.getKey(), p.getValue()==null?"":p.getValue());

		if (pending != null) {
			for (LogMessage p: pending)
				for (Animator a: anims)
					a.addLogMessage(p.time, p.src, p.s);
			pending = null;
		}

		for (Animator a: anims)
			a.init(d.getInitResult(), id);
	}
	
	public void stopSession() {
		pending = null;
		for (Animator a: anims)
			a.end(sessionData.getFinResult());
		anims.clear();
		if (multicastCur != null) {
			multicastCur.end();
			multicastCur = null;
		}
	}
	
	public String getMulticastAddress() {
		if (multicastCur != null)
			return multicastCur.getAddress();
		return null;
	}
	
	public void addLogMessage(long time, final String src, final String s) {
		if (pending != null) {
			pending.add(new LogMessage(time, src, s));
		} else {
			for (Animator a: anims)
				a.addLogMessage(time, src, s);
		}
	}
	
	public void setUpdateMode(Animator.UpdateMode m) {
		for (Animator a: anims)
			a.setUpdateMode(m);
	}

	
	public void done() {
		System.err.println("logmux "+this+" done: "+done);
		if (done)
			return;
		done = true;
		if (logmsc != null)
			logmsc.done();
		if (loganidot != null)
			loganidot.done();
		for (Multicast mc: allMulticast) {
			System.err.println("logmux "+this+" done: mc.done: "+mc);
				mc.done();
		}
	}
	
	public Anidot newAnidot(String title, Kind k) {
		return newAnidot(title, k, null, null);
	}
	public Anidot newAnidot(String title, Kind k, String filename) {
		return newAnidot(title, k, null, filename);
	}
	private Anidot newAnidot(String title, Kind k, Multicast mc, String filename) {
		switch(k) {
		case MODEL:
			return loganidot.newAnidot(title, "Model", true, mc, filename);
		case GUIDE:
			return loganidot.newAnidot(title, "Guide", true, mc, filename);
		case IMPL:
			return loganidot.newAnidot(title, "Impl", true, mc, filename);
		case SUSPAUT:
			return loganidot.newAnidot(title, "SuspAut", true, mc, filename);
		case TRACE:
			return loganidot.newAnidot(title, "Trace", true, mc, filename);


		default:
			err.report("internal error: LogMux: newAnidot: unknown kind: "+k);
			return null;
		}
	}
	public Animator newAnimator(Kind k, Anidot a, String t, DotLTS d, LTS l) {
		//System.out.println("newAnitmator d="+d+" l="+l);
		switch(k) {
		case MODEL:
			return new ModelToDot(a, t, d, l);
		case GUIDE:
			return new ModelToDot(a, t, d, l);
		case IMPL:
			return new ImplToDot(a, t, d, l);
		case SUSPAUT:
			return new SuspAutToDot(a, t, d, l);
		case TRACE:
			return new ImplToDot(a, t, d, l); //was: ModelToDot


		default:
			err.report("internal error: LogMux: newAnimator: unknown kind: "+k);
			return null;
		}
	}

	
	public LogMux(ErrorReporter r, LogServices s) {
		err = r;
		if(s!=null) {
			logmsc = s.getMsc();
			loganidot = s.getAnidot();
		}
	}
	
	private void startDotAnimator(String title, Kind k, AniDotLTSWriter writer, LTS lts) {
		startDotAnimator(title, k, writer, lts, null);
	}
	private void startDotAnimator(String title, Kind k, AniDotLTSWriter writer, LTS lts, RunItemData rid) {
		String gv_filename = null;
		Anidot modelAnidot = null;
		
		if (rid!=null)
			gv_filename = rid.getGraphVizFilename(err);

		if (gv_filename!=null) {
			modelAnidot = newAnidot(title, k, multicastCur, gv_filename);
			if (modelAnidot != null) {
				DotLTS modelDotLTS = new DotLTS(modelAnidot);
				modelAnidot.setRender(Anidot.RenderMode.DELAY);
				addAnimator(newAnimator(k, modelAnidot, title, modelDotLTS, lts));
				return;
			}
		} 

		modelAnidot = newAnidot(title, k, multicastCur, null);
		if (modelAnidot != null) {
			DotLTS modelDotLTS = new DotLTS(modelAnidot);
			modelAnidot.setRender(Anidot.RenderMode.OFF);
			writer.report("Exploring "+title);
			writer.write(modelDotLTS);
			writer.report("");
			modelAnidot.setRender(Anidot.RenderMode.DELAY);
			addAnimator(newAnimator(k, modelAnidot, title, modelDotLTS, lts));
		}
	}
	
	public void addAnimator(Animator a) {
		if (a!=null)
			anims.add(a);
	}
	
	private void printInteractionResult(DriverInterActionResult ll) {
		// TODO make sure all states that we return are compound states,
		// also the fail state, and possible other states that we add.
		// currently this is the case.
		// However, stuff will break as soon as that is no longer the case 
		CompoundTransition<? extends State> cc = ll.getTransition();
		if (cc != null) {
			for (Iterator<? extends Transition<?extends State>> it = cc.getSubTransitions(); it.hasNext(); ) {
				Transition<?extends State> t = it.next();
				if(debug) System.out.println("\tcompound-t: "+t.getSource().getID()+" -- "+t.getLabel().getString()+" -> "+t.getDestination().getID());
			}

			CompoundState cs = cc.getDestination();
			for (Iterator<? extends Transition<?extends State>> it = cs.getSubTransitions(); it.hasNext(); ) {
				Transition<?extends State> t = it.next();
				if(debug) System.out.println("\tcompound-s: "+t.getSource().getID()+" -- "+t.getLabel().getString()+" -> "+t.getDestination().getID());
			}
			for (Iterator<? extends State> it = cs.getSubStates(); it.hasNext(); ) {
				State t = it.next();
				if(debug) System.out.println("\tcompound-s-state: "+t.getID());
			}
		}
		else
			if(debug) System.out.println("\t no compounds");
	}

	private Vector<LogMessage> pending = null;
	
	private Multicast multicastCur = null;
	private Collection<Multicast> allMulticast = new Vector<Multicast>();
	private MscSrv logmsc = null;
	private AnidotSrv loganidot = null;
	private Collection<Animator> anims = new Vector<Animator>();
	private RunSessionData sessionData = null;
	private ErrorReporter err = null;

	private boolean done = false;
	private boolean debug = false;
}
