package utwente.fmt.jtorx.logger.mux;

import utwente.fmt.jtorx.logger.anidot.AnidotSrv;
import utwente.fmt.jtorx.logger.msc.MscSrv;
import utwente.fmt.jtorx.ui.ErrorReporter;

public class LogServices {
	
	public MscSrv getMsc() {
		return logmsc;
	}
	public AnidotSrv getAnidot() {
		return loganidot;
	}
	
	public void start() {
		logmsc.start();
		loganidot.start();
	}
	
	public boolean allRunning() {
		return logmsc.isRunning() && loganidot.isRunning();
	}
	
	public LogServices (ErrorReporter err) {
		logmsc = new MscSrv(err);
		loganidot = new AnidotSrv(err);
	}

	private MscSrv logmsc = null;
	private AnidotSrv loganidot = null;

}
