package utwente.fmt.jtorx.logger.mux;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import utwente.fmt.jtorx.ui.StepHighlighter;

public class Multicast {

	public class MultiServerThread extends Thread {
	    private Socket peer = null;

	    public MultiServerThread(Socket sock) {
	    	super("Multicast-MultiServerThread-"+sock.getLocalSocketAddress().toString()+"-"+sock.getRemoteSocketAddress().toString());
	    	peer = sock;
	    }

	    public void run() {
		    BufferedReader in;
			try {
				in = new BufferedReader(
						    new InputStreamReader(
						    peer.getInputStream()));
			} catch (IOException e1) {
				System.err.println("mc: "+peer+" run reader io exception "+e1.getMessage());
				e1.printStackTrace();
				clients.remove(peer);
				if (clients.size() <= 1)
					try {
						listenSocket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				return;
			}

		    String inputLine;

		    Socket c;
		    try {
				while ((inputLine = in.readLine()) != null) {
					System.err.println("mc: "+peer+" read: "+inputLine);
					if (highlighter != null)
						highlighter.highlight(inputLine);
					inputLine += "\n";
					for (Iterator<Socket> it=clients.iterator(); it.hasNext(); ) {
						c = it.next();
						if (c != peer)
							try {
								System.err.println("mc: "+peer+" writing to "+c);
								c.getOutputStream().write(inputLine.getBytes());
								c.getOutputStream().flush();
							} catch (IOException e) {
								System.err.println("mc: "+peer+" run writer io exception "+e.getMessage());
								System.err.println("mc: "+peer+" removing "+c);
								e.printStackTrace();
								clients.remove(c);
							    if (clients.size() <= 1)
									try {
										listenSocket.close();
									} catch (IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}

							}
					}
				}
			} catch (IOException e) {
				System.err.println("mc: "+peer+" run reader/loop io exception "+e.getMessage());
				// System.err.println("mc: "+peer+" eof");
				if (! e.getMessage().equalsIgnoreCase("Connection reset") && 
						!e.getMessage().equalsIgnoreCase("Socket closed"))
					e.printStackTrace();
			}
			try {
				peer.close();
			} catch (IOException e1) {
				System.err.println("mc: "+peer+" run closing sock io exception "+e1.getMessage());
				e1.printStackTrace();
			}
		    clients.remove(peer);
		    if (clients.size() <= 1) {
		    	// System.err.println("mc: "+peer+" client count <=1 "+clients.size());
				try {
					listenSocket.close();
				} catch (IOException e1) {
					System.err.println("mc: "+peer+" run closing listen sock io exception "+e1.getMessage());
					e1.printStackTrace();
				}
		    }
		    // out.close();
		    try {
				in.close();
				peer.close();
			} catch (IOException e) {
				System.err.println("mc: "+peer+" run closing in  io exception "+e.getMessage());
				e.printStackTrace();
			}
		  
			System.err.println("mc: "+peer+" cleaned up in MultiServerThread");
	
	    }
	}
	
	public class ListenThread extends Thread {
		public ListenThread() {
			super("Multicast-Listenthread");
		}
		public void run() {
			try {
				while (clients.size() > 0 || starting) {
					starting = false;
					Socket c = listenSocket.accept();
					System.err.println("mc:  "+listenSocket+ " ListenThread add "+c.getLocalSocketAddress().toString());
					clients.add(c);
					new MultiServerThread(c).start();
				}
				System.err.println("mc: "+listenSocket+ " ListenThread end, closing listen sock");
				listenSocket.close();

			} catch (IOException e) {
				System.err.println("mc: "+listenSocket+ " ListenThread io exception "+e.getMessage());
				if (!e.getMessage().equalsIgnoreCase("Socket closed"))
					e.printStackTrace();
			}

		}
	}
	
	
	public void end() {
		System.err.println("mc "+listenSocket+" : end: closing listen socket");
		try {
			listenSocket.close();
		} catch (IOException e) {
			System.err.println("mc "+listenSocket+" : end: exception while closing");
			e.printStackTrace();
		}
	}
	
	public void done() {
		System.err.println("mc: "+listenSocket+" : done:");
		end();
	
		// create our own copy of the client list, to avoid
		// getting ConcurrentModificationException because
		// each socket that we close here will trigger an
		// exception in the reading loop in MultiServerThread,
		// which will cause the socket to be removed from the client list
		ArrayList<Socket> myClients = new ArrayList<Socket>();
		myClients.addAll(clients);
		for(Socket c: myClients) {
			System.err.println("mc: done: closing client socket "+c);
			try {
				c.close();
			} catch (IOException e) {
				System.err.println("mc: done: exception while closing "+c);
				e.printStackTrace();
			}

		}
	}
	
	public String getAddress() {
		// because below, in Multicast(),  we force use of localhost address,
		// here we can just obtain the IP address of the listenSocket
		// (and no longer have to specify it by hand as in commented-out line below)
		//return "tcp!127.0.0.1!"+listenSocket.getLocalPort();
		return "tcp!"+listenSocket.getInetAddress().getHostAddress()+"!"+listenSocket.getLocalPort();
	}
	
	public Multicast() {
		this(null);
	}
	public Multicast(StepHighlighter hl) {
		try {
			// NOTE: we use localhost address using getByName(null)
			// (this may break if there is no localhost/loopback available!!)
			// becasuse we want to force use of localhost loopback address,
			// we no longer use the commented-out call below.
			// listenSocket = new ServerSocket(0);
			listenSocket = new ServerSocket(0, 0, java.net.InetAddress.getByName(null));
		} catch (IOException e) {
			System.err.println("mc: Multicast io exception "+e.getMessage());
			System.err.println("Could not listen");
			e.printStackTrace();
			return;
		}
		System.err.println("mc: "+listenSocket.getLocalSocketAddress().toString());
		highlighter = hl;
		new ListenThread().start();
	}

	private ServerSocket listenSocket;
	private boolean starting =  true;
	private StepHighlighter highlighter = null;
	
	private Set<Socket> clients = new HashSet<Socket>();
 }
