package utwente.fmt.jtorx.logger.mux;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;


import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.LineHandler;


public class MulticastClient {

	// TODO move Adapter StreamReader to a separate file,
	// as reusable AdapterStreamReader ????
	class StreamReader extends Thread {
		private BufferedReader is;
		final int ARRAYSIZE=1024;
		private String buf = "";
		private String name;
 
		StreamReader(String n, BufferedReader is) {
			super(n);
			this.is = is;
			this.name = n;
		}
 
		public void run() {
			try {
				int n;
				char[] cbuf = new char[ARRAYSIZE];
				while (true /*err.ready()*/){
					n  = is.read(cbuf, 0, ARRAYSIZE);
					if (n <= 0)
						return;
					buf += new String(cbuf,0,n);
					int i = buf.indexOf("\n");
					while (i >= 0) {
						String s = buf.substring(0,i).replaceAll("\r", "").trim();
						if (handler!=null)
							handler.handle(s);
						buf = buf.substring(i+1);
						i = buf.indexOf("\n");
					}
				}
			} catch (IOException e) {
				System.err.println(name+" streamreader io exception: "+e.getMessage());
				if (!e.getMessage().equalsIgnoreCase("Socket closed"))
					e.printStackTrace();
				try {
					is.close();
				} catch (IOException e1) {
					System.err.println(name+" streamreader close io exception: "+e1.getMessage());
					e1.printStackTrace();
				}
			}
		}
	}
	
	public void sendRequest(String s) throws IOException {
		if (!running || in==null)
			return;
		in.write(s.getBytes());
		in.flush();
	}

	public void done() {
		if (sock !=null)
			try {
				sock.close();
			} catch (IOException e) {
				System.err.println("mc: end: caught exception while closing "+sock+": "+e.getMessage());
				//e.printStackTrace();
			}
	}
	
	public MulticastClient(ErrorReporter r, String hp, LineHandler h) {
		if (hp==null) {
			r.report("MulticastClient: cannot connect: server address is null");
			return;
		}
		String hostPlusPort[] = hp.split("!",3);
		if (hostPlusPort.length == 3 && hostPlusPort[0].equals("tcp")) {
			host = hostPlusPort[1];
			try {
				port = Integer.parseInt(hostPlusPort[2]);
			}
			catch(NumberFormatException nfe) {
				r.report("MulticastClient: server port number: value is not a number: "+hostPlusPort[1]);
			}
		}
		//missing.check(implHost.equals(""), "implementation-host-name");
		// missing.check(implPort < 0, "implementation-port-number");
		handler = h;

		running = false;
		name = "multicast-client";
		
		if (host.equals("") ||  (port < 0)) {
			r.report(name+" cannot connect to multicast: cannot parse address string: "+hp);
			return;
		}
		try {
			sock = new Socket(host, port);
			sock.setSoLinger(false, 0);
			sock.setSoTimeout(0);
			out = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			StreamReader reader = new StreamReader(name+sock.getLocalSocketAddress().toString()+"-"+sock.getRemoteSocketAddress().toString(), out);
			reader.start();
			in = new DataOutputStream(sock.getOutputStream());
			running = true;
		} catch (IllegalArgumentException e) {
			r.report(name+" "+"could not connect: "+e.getMessage());
			// e.printStackTrace();
		} catch (UnknownHostException e) {
			r.report(name+" "+"unknown host \""+host+"\": "+e.getMessage());
			// e.printStackTrace();
		} catch (IOException e) {
			r.report(name+" "+"error connecting socket: "+e.getMessage());
			// e.printStackTrace();
		}

	}

	private boolean running = false;
	private String name;


	private Socket sock;
	private DataOutputStream in;
	private BufferedReader out;
	private LineHandler handler;
	
	private String host;
	private int port;


}
