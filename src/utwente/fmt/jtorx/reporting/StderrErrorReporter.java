package utwente.fmt.jtorx.reporting;

import java.text.SimpleDateFormat;

import utwente.fmt.jtorx.ui.ErrorReporter;

public class StderrErrorReporter implements ErrorReporter {
	
		public void report(final String s) {
			System.err.println("errreporter: "+s);
		}
		
		//TODO stop the test run
		public void endOfTest(final String s) {
			System.err.println("errreporter: eot: "+s);
			// stop the test run
		}

		public void log(long t, final String src, final String s) {
			System.err.println("errreporter: ("+src+") "+fmt.format(t)+" "+s);
		}
		
		private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
}
