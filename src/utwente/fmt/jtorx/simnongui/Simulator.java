package utwente.fmt.jtorx.simnongui;


import java.io.IOException;
import java.util.HashMap;


import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.interpretation.HardcodedInterpretation;
import utwente.fmt.jtorx.reporting.StderrErrorReporter;
import utwente.fmt.jtorx.torx.adapter.lts.AdapterImpl;
import utwente.fmt.jtorx.torx.explorer.aut.AutLTS;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.torx.explorer.torx.TorxExplorer;
import utwente.fmt.jtorx.utils.cmdint.Command;
import utwente.fmt.jtorx.utils.cmdint.CommandInterpreter;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;

public class Simulator {

	class CmdQuit implements Command {
		public Boolean compute(String arg) {
			cleanup();
			return false;
		}
	}
	
	class CmdInput implements Command {
		public Boolean compute(String arg) {
			Label l;
			if (arg==null || arg.trim().equals("")){
				l = myAdapter.randomStimulus();
			} else {
				l = new LibLabel(arg);
				myAdapter.applyStimulus(l);
			}
			System.out.println("input: "+l.getString());
			return true;
		}
	}
	
	class CmdOutput implements Command {
		public Boolean compute(String arg) {
			Label l = myAdapter.getObservation().getLabel();
			System.out.println("output: "+l.getString());
			return true;
		}
	}
	
	class CmdMenu implements Command {
		public Boolean compute(String arg) {
			myAdapter.printMenu();
			return true;
		}
	}
	class CmdState implements Command {
		public Boolean compute(String arg) {
			myAdapter.printState();
			return true;
		}
	}

	
	class Commands implements utwente.fmt.jtorx.utils.cmdint.Commands {
		public Command lookup(String s) {

			return cmdTable.get(s);
		}
	
		public Commands () {
			cmdTable = new HashMap<String,Command>();
			cmdTable.put("quit", new CmdQuit());
			cmdTable.put("q", new CmdQuit());
			cmdTable.put("exit", new CmdQuit());
			cmdTable.put("x", new CmdQuit());
			cmdTable.put("menu", new CmdMenu());
			cmdTable.put("m", new CmdMenu());
			cmdTable.put("state", new CmdState());
			cmdTable.put("s", new CmdState());
			cmdTable.put("input", new CmdInput());
			cmdTable.put("i", new CmdInput());
			cmdTable.put("output", new CmdOutput());
			cmdTable.put("o", new CmdOutput());
		}
		private HashMap<String, Command> cmdTable;
	}

	private void cleanup() {
		if (doneCalled)
			return;
		doneCalled = true;
		myAdapter.done();
	}
	
	
	public void run(String[] args) throws IOException {

		//if (args.length != 2) {
		//	System.err.println("usage: ltsim/sim model.aut");
		//	return;
		//}
		
			
		// here we might want something that looks at the file
		// type and does the right thing:
		// .aut -> use AutLTS
		
		LTS lts;
		System.out.println("args cnt="+args.length);
		for (int i=0; i<args.length; i++) {
			System.out.println("arg["+i+"]="+args[i]);
		}
		if (args.length == 1 && args[0].endsWith(".aut"))
			lts = new AutLTS(args[0]);
		else {
			String cmd[] = new String[args.length-1];
			String dir = args[0];
			System.arraycopy(args, 1, cmd, 0, args.length-1);
			lts = new TorxExplorer(new StderrErrorReporter(), cmd, dir, null);
		}
		//	lts = new TorxExplorer(args[1:], new String[0], args[0]);
		interp = new HardcodedInterpretation();	
		myAdapter = new AdapterImpl(lts, interp, seed, true/*synthDelta*/);

		myAdapter.start();
		
		cmds = new Commands();
		CommandInterpreter cmdInterp = new CommandInterpreter();
		cmdInterp.loop("tester> ", cmds);
		cleanup();


	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		Simulator s = new Simulator();
		s.run(args);
	}

	private IOLTSInterpretation interp;
	private Commands  cmds;
	private AdapterImpl myAdapter;
	private Boolean doneCalled = false;
	private long seed = 0;
}
