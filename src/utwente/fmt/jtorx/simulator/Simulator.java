package utwente.fmt.jtorx.simulator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.swt.SWTException;

import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.logger.Animator.UpdateMode;
import utwente.fmt.jtorx.logger.mux.LogMux;
import utwente.fmt.jtorx.logger.mux.MulticastClient;
import utwente.fmt.jtorx.testgui.RunItemData;
import utwente.fmt.jtorx.testgui.RunSessionData;
import utwente.fmt.jtorx.testgui.SimPathPane;
import utwente.fmt.jtorx.testgui.SimStartPane;
import utwente.fmt.jtorx.testgui.SimTreePane;
import utwente.fmt.jtorx.testgui.Testgui;
import utwente.fmt.jtorx.torx.DriverSimInitializationResult;
import utwente.fmt.jtorx.torx.DriverSimInterActionResult;
import utwente.fmt.jtorx.torx.InstantiationImpl;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.jtorx.torx.wrap.WrapLTS;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.LineHandler;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.utils.term.TermParser;
import utwente.fmt.jtorx.writers.AniDotExtendedLTSWriter;
import utwente.fmt.jtorx.writers.AniDotLTSWriter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Term;
import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.UnificationResult;
import utwente.fmt.lts.LTS.LTSException;
import utwente.fmt.test.Verdict;

public class Simulator {
	
	public enum SIMUpdateMode { OFF, ON };
	
	public class DriverSimInitializationResultImpl implements DriverSimInitializationResult {

		public long getEndTime() {
			return 0;
		}

		public long getStartTime() {
			return 0;
		}

		public CompoundState getState() {
			Iterator<?extends CompoundState> it = null;
			try {
				it = lts.init();
			} catch (LTSException e) {
				System.err.println("simulator: cannot access model: caught LTSException: "+e.getMessage());
				return null;
			}
			if (it.hasNext())
				return it.next();
			return null;
		}
		
		public CompoundState getAdapterState() {
			if (trace==null)
				return null;
			Iterator<?extends CompoundState> it = null;
			try {
				it = trace.init();
			} catch (LTSException e) {
				System.err.println("simulator: cannot access trace: caught LTSException: "+e.getMessage());
				return null;
			}
			if (it.hasNext())
				return it.next();
			return null;
		
		}

		public Verdict getVerdict() {
			return null;
		}

		public Boolean hasVerdict() {
			return false;
		}
		
		/*
		public DriverInitializationResultImpl(LTS l) {
			lts  = new CompoundLTSWrap(l);
		}
		*/
		public DriverSimInitializationResultImpl(CompoundLTS l, CompoundLTS t) {
			lts  = l;
			trace = t;
		}

		private CompoundLTS lts = null;
		private CompoundLTS trace = null;
	}
	
	public class DriverSimInterActionResultImpl implements DriverSimInterActionResult {

		public String getChan() {
			return null;
		}

		public String getConcrete() {
			return null;
		}

		public State getDst() {
			return trans.getDestination();
		}

		public long getEndTime() {
			return 0;
		}

		public Kind getKind() {
			// TODO Auto-generated method stub
			return null;
		}

		public Label getLabel() {
			return trans.getLabel();
		}

		public String getPCO() {
			return null;
		}

		public State getSrc() {
			return trans.getSource();
		}

		public long getStartTime() {
			return 0;
		}

		public int getStepNr() {
			return 0;
		}
		
		public long getTimeStamp() {
			return 0;
		}

		public CompoundTransition<? extends CompoundState> getTransition() {
			return trans;
		}
		
		public CompoundTransition<? extends CompoundState> getAdapterTransition() {
			return tracetrans;
		}


		public Verdict getVerdict() {
			return null;
		}

		public Boolean hasVerdict() {
			return false;
		}
		
		public DriverSimInterActionResultImpl(CompoundTransition<? extends CompoundState> t,
				CompoundTransition<? extends CompoundState> tt) {
			trans = t;
			tracetrans = tt;
		}
		
		private CompoundTransition<? extends CompoundState> trans = null;
		private CompoundTransition<? extends CompoundState> tracetrans = null;
		
	}
	
	public class LineHandlerImpl implements LineHandler {

		public void handle(String s) {
			try {
				int step = Integer.parseInt(s);
				if (simpathPane!=null)
					simpathPane.highLight(step);
			}
			catch(NumberFormatException nfe) {
				errorReporter.report("Simulator: highlighter: step is not a number: "+s);
			}
		}
	}
	
	public boolean haveTraceToShow() {
		return traceToShow!=null;
	}

	public void expand(State s) {

	}

	public void start(RunSessionData rsd) {
		start(rsd, traceToShow);
	}

	public void start(RunSessionData rsd, RunItemData traceData) {
		requestedToStop = false;
		simtreePane.runBegins();
		simstartPane.runBegins();
		simpathPane.runBegins();

		if (clts == null)
			return;
		
		RunItemData data = runData.getDataByKind(kind);
		if (data==null) {
			System.err.println("simulator: error: no runItemData");
			return;		
		}

		
		runData.setTitle("Simulation of model "+ kind);
	
		runData.setVizMsc(false);
		runData.setVizRun(false);
	
		if (rsd!=null) {
			runData.setVizModel(rsd.getVizModel());
			runData.setVizSuspAut(rsd.getVizSuspAut());
			runData.setVizTrace(rsd.getVizTrace());
		}
		
		runData.setModelData(data);

		System.err.println("simulator: setting up");
		//runData.setModelLTS(lts);
		//System.err.println("Simulator start lts="+lts+" rsd="+rsd+" data-lts="+data.getLTS()+" fn="+data.getModelFilename());
		
		String kindStr = "";
		if (kind!=LogMux.Kind.MODEL && kind!=LogMux.Kind.SUSPAUT)
			kindStr = "(as "+kind+") ";
			
		if (lts==null) {
			File modelFile = new File(data.getModelFilename());
			runData.setModelWriter(new AniDotLTSWriter(data.getLTS(),  interp, stateSpaceProgress));
			if (runData.getModelTitle()==null||runData.getModelTitle().equals(""))
				runData.setModelTitle("Simulation of model "+ kindStr + Testgui.titleOf(modelFile));
		
			runData.setSuspAutLTS(clts);
			runData.setSuspAutWriter(new AniDotExtendedLTSWriter(clts,  interp, stateSpaceProgress));
			if (runData.getSuspAutTitle()==null||runData.getSuspAutTitle().equals(""))
				runData.setSuspAutTitle("Simulation of suspension automaton of "+ Testgui.titleOf(modelFile));
		} else {
			File modelFile = new File(data.getModelFilename());
			runData.setModelWriter(new AniDotLTSWriter(data.getLTS(),  interp, stateSpaceProgress));
			if (runData.getModelTitle()==null||runData.getModelTitle().equals(""))
				runData.setModelTitle("Simulation of model "+ kindStr + Testgui.titleOf(modelFile));
		}
		
		DriverSimInitializationResult initResult;
		
		RunItemData myTraceData = traceData;
		LTS traceLTS = null;
		if (traceData!=null){
			traceLTS = traceData.getLTS();
			trace = traceData.getCompoundLTS();
			if (trace==null && traceLTS!=null) {
				myTraceData = new RunItemData();
				trace = new WrapLTS(traceLTS);
				myTraceData.setLTS(trace);
				myTraceData.setFilename(traceData.getModelFilename(), traceData.getInstFilename());
				myTraceData.setTitle(traceData.getTitle());
				traceData = myTraceData;
				traceTransCount = traceLTS.getNrOfTransitions();
			}
		
			if (traceData!=null && traceData.getCompoundLTS()!=null) {
				trace = traceData.getCompoundLTS();
				runData.setTraceData(traceData);
				File traceFile = new File(traceData.getModelFilename());
				if(traceData.getTitle()!=null)
					runData.setTraceTitle(traceData.getTitle());
				else
					runData.setTraceTitle("Trace "+Testgui.titleOf(traceFile));
				runData.setTraceLTS(trace);
				runData.setTraceWriter(new AniDotLTSWriter(traceLTS,  interp, stateSpaceProgress));
			}
		}
		System.err.println("simulator: about to get initResult");
		initResult = new DriverSimInitializationResultImpl(clts, trace);
		forceInitExpansion(clts);
		if (trace!=null)
			forceInitExpansion(trace);
		runData.setInitResult(initResult);
		runData.setModelLTS(clts);
		System.err.println("simulator: set model LTS");
	
		
		// TODO the following breaks if we have more than one initial state
		String id = "";
		CompoundState root = initResult.getState();
		id = simtreePane.addRoot(root);
		simpathPane.init(id);
		//System.out.println("about to logMux.startSession: runData.getModelTS()="+runData.getModelLTS());
		try {
			logMux.startSession(runData, id);
		} catch (Exception e) {
			System.out.println("startSession caught exception (trace folows): "+e.getMessage());
			e.printStackTrace();
		}
		//System.out.println("after logMux.startSession: runData="+runData);
		multicastClient = new MulticastClient(errorReporter, logMux.getMulticastAddress(), new LineHandlerImpl());
		//System.out.println("after new multicastClient: client="+multicastClient);

		if (traceData!=null && traceData.getCompoundLTS()!=null) {
			final CompoundState tRoot = root;
			final String tId = id;
			final RunItemData tTraceData = traceData;
			new Thread("simulation trace runner") {
				public void run() {
					try {
						progressBegin(traceTransCount);
						updateMode = SIMUpdateMode.ON;
						String endId = runTrace(tRoot, tId, tTraceData.getCompoundLTS());
						if (! requestedToStop)
							progressEnd(traceTransCount);
						updateMode = SIMUpdateMode.ON;
						simtreePane.select(endId);
					} catch (SWTException e) {
						System.err.println("Simulator trace-runner: exception: "+e.getMessage());
					}
				}
			}.start();
		}
	}

	public void stop() {
		requestedToStop = true;
		simtreePane.runEnds();
		simstartPane.runEnds();
		simpathPane.runEnds();
		logMux.stopSession();
	}

	public void setPanes(SimStartPane sp, SimPathPane pp, SimTreePane tp) {
		simstartPane = sp;
		simpathPane = pp;
		simtreePane = tp;
	}

	public void noShow() {
		updateMode = SIMUpdateMode.OFF;
		logMux.setUpdateMode(UpdateMode.OFF);
	}
	public void yesShow() {
		updateMode = SIMUpdateMode.ON;
		logMux.setUpdateMode(UpdateMode.ON);
		
	}
	
	public void mcastWrite(String s) {
		try {
			multicastClient.sendRequest(s);
		} catch (IOException e) {
			System.err.println("simulator mcastWrite exception: "+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public void showTransition(CompoundTransition<? extends CompoundState> t,
			CompoundTransition<? extends CompoundState> tt, 
			String pId, String id) {
		System.err.println("simulator.showTransition("+pId+", "+id+") updateMode="+updateMode);
		forceExpansion(t.getDestination());
		if(tt!=null)
			forceExpansion(tt.getDestination());
		// show transition in animators
		DriverSimInterActionResult ll = new DriverSimInterActionResultImpl(t, tt);
		logMux.addInterActionResult(ll, pId, id);
		
		// set up transition trace that we (later) show in SimPathPane
		simpathPane.addTransition(t.getLabel().getString(), pId, id);
		
		// try to show the computed transition trace in SimPathPane
		showTransitionPath(id);
	}
	
	public void showTransitionPath(String id) {
		if (updateMode!=SIMUpdateMode.ON)
			return;
		simpathPane.showTransitionPath(id);
	}
	
	public void handleGivenStimulusEvent(final Label l) {
		simtreePane.handleGivenStimulusEvent(l);
	}

	public String handleGetSolutionEvent() {
		return simtreePane.handleGetSolutionEvent();
	}


	public void end()  {
		if (multicastClient!=null)
			multicastClient.done();
	}

	public Simulator(CompoundLTS cl, LogMux m, LogMux.Kind k, RunSessionData r, RunItemData tts, ErrorReporter err, ProgressReporter pr, ProgressReporter spr) {
		clts = cl;
		logMux = m;
		kind = k;
		runData = r;
		stateSpaceProgress = spr;
		errorReporter = err;
		traceToShow = tts;
		// System.out.println("Simulator compoundLTS cl="+cl);
	}
	public Simulator(LTS l, LogMux m, LogMux.Kind k, RunSessionData r, RunItemData tts, ErrorReporter err, ProgressReporter pr, ProgressReporter spr) {
		lts = l;
		logMux = m;
		kind = k;
		runData = r;
		stateSpaceProgress = spr;
		errorReporter = err;
		clts = new WrapLTS(lts);
		traceToShow = tts;
		// System.out.println("Simulator LTS l="+l);

	}

	private String runTrace(CompoundState model_root, String root_id, CompoundLTS trace) {
		if (trace==null)
			return root_id;
		
		updateMode = SIMUpdateMode.ON;
		// assume we really have a trace:
		//  - single initial state
		//  - single outgoing transition from each state (except last)
		//  - no loops or cycles (we should check for that, to make sure we terminate)
		CompoundState trace_root = null;
		Iterator<?extends CompoundState> it = null;
		try {
			it = trace.init();
		} catch (LTSException e) {
			System.err.println("simulator: cannot simulate trace: caught LTSException: "+e.getMessage());
			return root_id;
		}
		while(it.hasNext()) {
			if (trace_root!=null) {
				errorReporter.report("simulator: cannot simulate trace: more than a single initial state");
				return root_id;
			}
			trace_root = it.next();
		}

		CompoundState model_parent = model_root;
		CompoundState model_child = null;
		CompoundState trace_parent = trace_root;
		CompoundState trace_child = null;

		CompoundTransition<?extends CompoundState> model_trans;
		CompoundTransition<?extends CompoundState> trace_trans;
		Label trace_label;
		Iterator<?extends CompoundTransition<?extends CompoundState>> model_tit;
		Iterator<?extends CompoundTransition<?extends CompoundState>> trace_tit;
		String parent_id, child_id, id;
		parent_id = root_id;
		int stepNr = 1;
		// TODO do not have a parent tuple (model_parent,parent_id)
		// but a set of tuples,
		// such that in each step of the main loop
		// we treat each tuple of the set as a parent, to construct the
		// next set of parent tuples
		// This will allow us to follow a trace in an non-deterministic LTS
		// as long as the trace does not contain Primer-synthesized actions
		// like quiescence.
		while(model_parent!=null && trace_parent!=null && !requestedToStop) {
			progressStep(stepNr, traceTransCount);
			trace_tit = trace_parent.menu(interp.isAny());
			trace_trans = null;
			while(trace_tit.hasNext()) {
				if (trace_trans!=null) {
					errorReporter.report("simulator: "+stepNr+" : cannot simulate trace: more than a single menu element");
					return parent_id;
				}
				trace_trans = trace_tit.next();
			}
			if(trace_trans==null) {
				errorReporter.report("simulator: "+stepNr+" :  end of trace reached");
				return parent_id;
			}
			trace_label = trace_trans.getLabel();
			trace_child = trace_trans.getDestination();
			System.err.println("trace step "+stepNr+" "+trace_trans.getSource()+" "+trace_trans.getSource().getID()+" "+trace_trans.getSource().getLabel(false)+
					" --- "+trace_label.getString()+" ---> "+trace_child+" "+
					trace_child.getID()+" "+trace_child.getLabel(false));

			model_tit = model_parent.menu(interp.isAny());
			simtreePane.beginUnfold(model_parent, parent_id);
			child_id = null;
			id = null;
			model_child = null;
			ArrayList<CompoundTransition<?extends CompoundState>> nonTraceTrans = new ArrayList<CompoundTransition<?extends CompoundState>>();
			ArrayList<CompoundTransition<?extends CompoundState>> noinstTraceTrans = new ArrayList<CompoundTransition<?extends CompoundState>>();
			ArrayList<CompoundTransition<?extends CompoundState>> instTraceTrans = new ArrayList<CompoundTransition<?extends CompoundState>>();
			ArrayList<CompoundTransition<?extends CompoundState>> instresTraceTrans = new ArrayList<CompoundTransition<?extends CompoundState>>();
			CompoundTransition<?extends CompoundState> res = model_parent.next(trace_label);
			while(model_tit.hasNext()) {
				model_trans = model_tit.next();
				// forceExpansion(model_trans.getDestination());
			
				Label t_l = model_trans.getLabel();
				Term t_tm = TermParser.parse(t_l);
				Term l_tm = TermParser.parse(trace_label);
				UnificationResult uni_res = t_tm.unify(l_tm);
				System.out.println("Simulator: trying: l: ("+trace_label.getString()+") "+trace_label.hashCode()+" "+trace_label.toString()+" tl:("+t_l.getString()+")"+t_l.hashCode()+" "+t_l.toString());
				if (uni_res.isUnifyable()) {
					System.out.println(" is-unifyable");
					ArrayList<UnificationBinding> b = uni_res.getBindings();
					if (b==null || b.isEmpty()) {
						System.out.println(" no bindings");						
						noinstTraceTrans.add(model_trans);
					} else {
						CompoundTransition<?extends CompoundState> t_inst = model_trans.instantiate(new InstantiationImpl(trace_label, l_tm, b));
						System.out.println("Simulator.next: inst-result="+t_inst);
						if (t_inst != null) { // i.e. instantiation was succesful
							instTraceTrans.add(model_trans);
							instresTraceTrans.add(t_inst);
						} else {
							nonTraceTrans.add(model_trans);
						}
					}
				
				} else
					nonTraceTrans.add(model_trans);
			}
			if (noinstTraceTrans.size() > 0 || instTraceTrans.size() > 0) {
				yesShow(); // to allow highlighting of item that matches trace
				for(CompoundTransition<?extends CompoundState> t: noinstTraceTrans) {
					child_id = simtreePane.addChild(model_parent, parent_id, t, trace_trans);
					id = child_id; // make sure we don't erroneously think id==null, below
					model_child = t.getDestination();		
				}
				Iterator<CompoundTransition<?extends CompoundState>> mod_it = instTraceTrans.iterator();
				Iterator<CompoundTransition<?extends CompoundState>> res_it = instresTraceTrans.iterator();
				while(mod_it.hasNext() && res_it.hasNext()) {
					CompoundTransition<?extends CompoundState> mod_t = mod_it.next();
					CompoundTransition<?extends CompoundState> res_t = res_it.next();
					simtreePane.addChild(model_parent, parent_id, mod_t, null);
					if (res==null) {
						child_id = simtreePane.addChild(model_parent, parent_id, res_t, trace_trans);
						id = child_id; // make sure we don't erroneously think id==null, below
						model_child = res_t.getDestination();
					}
				}
				if (res!=null && ((noinstTraceTrans.size() + instTraceTrans.size()) > 1 || instTraceTrans.size() > 0)) {
					// NOTE res should be non-null, because noinstTraceTrans.size() + instTraceTrans.size()) > 1
					child_id = simtreePane.addChild(model_parent, parent_id, res, trace_trans);
					id = child_id; // make sure we don't erroneously think id==null, below
					model_child = res.getDestination();
				}
				noShow(); // to disallow highlighting of all sibbling items
			}
			for(CompoundTransition<?extends CompoundState> t: nonTraceTrans)
				id = simtreePane.addChild(model_parent, parent_id, t, null);
			simtreePane.endUnfold(model_parent, parent_id);
			updateMode = SIMUpdateMode.ON;
			if (id==null) {
				errorReporter.report("simulator: "+stepNr+" :  reached end of model before reaching end of trace");
				return parent_id;
			} else if (child_id==null) {
				errorReporter.report("simulator: "+stepNr+" :  trace diverged from model, which does not have: "+trace_label.getString());
				return parent_id;
			} else if (model_child==null) {
				errorReporter.report("simulator: "+stepNr+" :  model has no destination for label: "+trace_label.getString());
				return parent_id;
			} else if (trace_child==null) {
				errorReporter.report("simulator: "+stepNr+" :  trace has no destination for label: "+trace_label.getString());
				return parent_id;
			}
			simtreePane.select(child_id);
			parent_id = child_id;
			model_parent = model_child;
			trace_parent = trace_child;
			stepNr++;
		}
		if (requestedToStop)
			errorReporter.report("simulator: "+stepNr+" : stopped");

		return parent_id;
	}

	private void progressBegin(long max) {
		if (max < 0)
			return;
		stateSpaceProgress.reset();
		stateSpaceProgress.setMax((int) max); // TODO fix cast
	}
	private void progressEnd(long max) {
		if (max < 0)
			return;
		stateSpaceProgress.ready();
	}

	private void progressStep(long nr, long max) {
		if (max < 0)
			return;
		stateSpaceProgress.setLevel((int)nr);
	}
	
	private void forceInitExpansion(LTS l) {
		if (l!=null) {
			Iterator<? extends State> it;
			try {
				it = l.init();
				if (it!=null)
					while(it.hasNext())
						forceExpansion(it.next());
			} catch (LTSException e) {
				System.err.println("simulator: caught LTSException: "+e.getMessage());
			}
		}
	}
	
	private void forceExpansion(State s) {
		if (s!=null)
			s.menu(interp.isAny());
	}


	private LTS lts;
	private RunItemData traceToShow;
	private CompoundLTS clts;
	private CompoundLTS trace = null;
	private long traceTransCount = -1;
	
	private LogMux.Kind kind;
	private LogMux logMux;
	private MulticastClient multicastClient = null;
	private Interpretation interp = new AnyInterpretation();

	private ProgressReporter stateSpaceProgress = null;
	private ErrorReporter errorReporter;

	private SimStartPane simstartPane;
	private SimPathPane simpathPane;
	private SimTreePane simtreePane;

	private RunSessionData runData; // = new RunSessionData();

	private boolean requestedToStop = false;
	private SIMUpdateMode updateMode;
}
