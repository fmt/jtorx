package utwente.fmt.jtorx.testgui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import utwente.fmt.interpretation.InterpKind;
import utwente.fmt.interpretation.TraceKind;

public class Config {
	public void add(String k, int v) {
		c.put(k, ""+v);
	}
	public int getInt(String k) {
		String s = c.get(k);
		if (s==null)
			return -1;
		if (s.trim().equals(""))
			return -1;
		try {
			return Integer.parseInt(s);
		} catch(Exception e) {
			return -1;
		}
	}
	
	public void add(String k, boolean v) {
		c.put(k, ""+v);
	}
	public boolean getBool(String k) {
		String s = c.get(k);
		if (s!=null)
			return s.equals("true");
		return false;
	}

	public void add(String k, TimeUnit v) {
		if (v!=null)
			c.put(k, ""+v);
		else
			c.put(k, null);
	}
	public TimeUnit getTimeUnit(String k) {
		try {
			return TimeUnit.valueOf(c.get(k));
		} catch(Exception e) {
			return null;
		}
	}
	
	public void add(String k, RunItemData.LTSKind v) {
		if (v!=null)
			c.put(k, ""+v);
		else
			c.put(k, null);
	}
	public RunItemData.LTSKind getLTSKind(String k) {
		try {
			return RunItemData.LTSKind.valueOf(c.get(k));
		} catch (Exception e) {
			return null;
		}
	}
	
	public void add(String k, RunItemData.AdapterKind v) {
		if (v!=null)
			c.put(k, ""+v);
		else
			c.put(k, null);
	}
	public RunItemData.AdapterKind getAdapterKind(String k) {
		try {
			return RunItemData.AdapterKind.valueOf(c.get(k));
		} catch(Exception e) {
			return  null;
		}
	}
	
	public void add(String k, TraceKind v) {
		if (v!=null)
			c.put(k, ""+v);
		else
			c.put(k, null);
	}
	public TraceKind getTraceKind(String k) {
		try {
			return TraceKind.valueOf(c.get(k));
		} catch(Exception e) {
			return null;
		}
	}
	
	public void add(String k, InterpKind v) {
		if (v!=null)
			c.put(k, ""+v);
		else
			c.put(k, null);
	}
	public InterpKind getInterpKind(String k) {
		try {
			return InterpKind.valueOf(c.get(k));
		} catch(Exception e) {
			return null;
		}
	}
	
	public void add(String k, String v) {
		if (v!=null)
			c.put(k, v);
		else
			c.put(k, null);
	}
	public String getString(String k) {
		return c.get(k);
	}
	
	public Collection<Entry<String,String>> getConfig() {
		return c.entrySet();
	}
	
	public void twrite(BufferedWriter w) throws IOException {
		doWrite(w, true);
	}
	public void write(BufferedWriter w) throws IOException {
		doWrite(w, false);
	}
	private void doWrite(BufferedWriter w, boolean dbg) throws IOException {
		ArrayList<String> keys = new ArrayList<String>(c.keySet());
		Collections.sort(keys);
		for(String k: keys) {
			String val = c.get(k);
			w.write((dbg&&val==null?"--":"")+k+" "+(val==null?"":val)+"\n");
		}
		w.flush();
	}
	
	static String standardLogPrefix = "1970-01-01T01:00:00.000+0100 CONFIG 0 () ";
	public void read(BufferedReader r) throws IOException {
		String s;
		boolean isLogFile = false;
		s = r.readLine();
		if (s == null)
			return;
		if (s.startsWith(standardLogPrefix)) {
			isLogFile = true;
		} else if (s.length() > 0 && Character.isUpperCase(s.charAt(0))) {
			isLogFile = false;
		} else { // unknown file format!
			return;
		}
		int sep;
		while(s != null) {
			if (isLogFile && s.startsWith(standardLogPrefix)) {
				s = s.substring(standardLogPrefix.length());
				sep = s.indexOf('=');
			} else if (!isLogFile) {
				sep = s.indexOf(' ');
			} else {
				break;
			}
			String key = s.substring(0, sep);
			String val = s.substring(sep+1).trim(); // skip space + witespace
			c.put(key, val);
			s = r.readLine();
		}
	}

	public Config() {
		c = new HashMap<String,String>();
	}

	private HashMap<String,String> c = null;
}
