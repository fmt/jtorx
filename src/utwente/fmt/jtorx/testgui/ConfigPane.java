package utwente.fmt.jtorx.testgui;

import java.util.concurrent.TimeUnit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.InterpKind;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.interpretation.ActionListInterpretation;
import utwente.fmt.jtorx.interpretation.PostfixDeltaInterpretation;
import utwente.fmt.jtorx.interpretation.PostfixInterpretation;
import utwente.fmt.jtorx.interpretation.PrefixDeltaInterpretation;
import utwente.fmt.jtorx.interpretation.PrefixInterpretation;
import utwente.fmt.jtorx.testgui.lazyotf.config.LazyOTFConfigPane;


public class ConfigPane implements JTorXPane {
	
	// ======================================================================
	
	// general JTorXPane interface functions
	
	public Group getGroup() {
		return configGroup;
	}
	
	public void runSetup() {
		reInit();
	}
	public void runBegins() {
		reInit();
	}
	private void reInit() {
		modelText.setEditable(false);
		guideText.setEditable(false);
		implText.setEditable(false);
		modelBrowseButton.setEnabled(false);
		guideBrowseButton.setEnabled(false);
		implBrowseButton.setEnabled(false);
		modelDropTarget.removeDropListener(modelDropListener);
		guideDropTarget.removeDropListener(guideDropListener);
		implDropTarget.removeDropListener(implDropListener);
		implCombo.setEnabled(false);
		implRTToggleButton.setEnabled(false);
		interpCombo.setEnabled(false);
		interpInText.setEnabled(false);
		interpOutText.setEnabled(false);
		traceKindCombo.setEnabled(false);
		timeoutText.setEnabled(false);
		unitCombo.setEnabled(false);
		instModelText.setEnabled(false);
		instModelBrowseButton.setEnabled(false);
		instModelToggleButton.setEnabled(false);
		instImplText.setEnabled(false);
		instImplBrowseButton.setEnabled(false);
		instImplToggleButton.setEnabled(false);
		interpDeltaToggleButton.setEnabled(false);
		synthDeltaToggleButton.setEnabled(false);
		angelicCompletionToggleButton.setEnabled(false);
		divergenceToggleButton.setEnabled(false);
		useCoverageToggleButton.setEnabled(false);
		guideToggleButton.setEnabled(false);
		guideCombo.setEnabled(false);
		lazyOTFConfigPane.setEnabled(false);
		useLazyOTFToggleButton.setEnabled(false);
	}

	public void runEnds() {
		modelText.setEditable(true);
		guideText.setEditable(true);
		implText.setEditable(true);
		modelBrowseButton.setEnabled(true);
		guideBrowseButton.setEnabled(true);
		implBrowseButton.setEnabled(true);
		modelDropTarget.addDropListener(modelDropListener);
		guideDropTarget.addDropListener(guideDropListener);
		implDropTarget.addDropListener(implDropListener);
		implCombo.setEnabled(true);
		implRTToggleButton.setEnabled(true);
		interpCombo.setEnabled(true);
		interpInText.setEnabled(true);
		interpOutText.setEnabled(true);
		traceKindCombo.setEnabled(true);
		timeoutText.setEnabled(true);
		unitCombo.setEnabled(true);
		instModelText.setEnabled(true);
		instModelBrowseButton.setEnabled(true);
		instModelToggleButton.setEnabled(true);
		instImplText.setEnabled(true);
		instImplBrowseButton.setEnabled(true);
		instImplToggleButton.setEnabled(true);
		interpDeltaToggleButton.setEnabled(true);
		synthDeltaToggleButton.setEnabled(true);
		angelicCompletionToggleButton.setEnabled(true);
		divergenceToggleButton.setEnabled(true);
		useCoverageToggleButton.setEnabled(true);
		guideToggleButton.setEnabled(true);
		guideCombo.setEnabled(true);

		useLazyOTFToggleButton.setEnabled(true);
		lazyOTFConfigPane.setEnabled(isLazyOTFEnabled());
	}
	
	// ======================================================================
	
	// getter for configuration element that is obtained from multiple UI items
	// (i.e. it has no corresponding setter)
	
	public IOLTSInterpretation getInterpretation() {
		return interp;
	}
	
	// ======================================================================
	
	// getters and setters for configuration items
	
	public TraceKind getTraceKind() {
		return traceKind;
	}
	private void setTraceKind(TraceKind v) {
		System.err.println("setTraceKind: "+v);
		if (v!=null) {
			for (int i=0; i<traceInfo.length; i++) {
				if (traceInfo[i].kind.equals(v)) {
					traceKindCombo.select(i);
					traceKindCombo.notifyListeners(SWT.Selection, null);
					return;
				}
			}
		}
		traceKindCombo.deselectAll();
		traceKindCombo.notifyListeners(SWT.Selection, null);
	}
	
	public InterpKind getInterpKind() {
		//System.err.println("getInterpKind: "+interpKind);
		return interpKind;
	}
	private void setInterpKind(InterpKind v) {
		System.err.println("setInterpKind: "+v);
		if (v!=null) {
			for (int i=0; i<interpInfo.length; i++) {
				if (interpInfo[i].kind.equals(v)) {
					interpCombo.select(i);
					interpCombo.notifyListeners(SWT.Selection, null);
					return;
				}
			}
		}
		interpCombo.deselectAll();
		interpCombo.notifyListeners(SWT.Selection, null);
	}
	
	public boolean getReconDelta() {
		return interpDeltaToggleButton.getSelection();
	}
	public void setReconDelta(boolean v) {
		interpDeltaToggleButton.setSelection(v);
		interpDeltaToggleButton.notifyListeners(SWT.Selection, null);
	}
	
	public boolean getSynthDelta() {
		return synthDeltaToggleButton.getSelection();
	}
	public void setSynthDelta(boolean v) {
		synthDeltaToggleButton.setSelection(v);
		synthDeltaToggleButton.notifyListeners(SWT.Selection, null);
	}
	
	public boolean getAngelicCompletion() {
		return angelicCompletionToggleButton.getSelection();
	}
	public void setAngelicCompletion(boolean v) {
		angelicCompletionToggleButton.setSelection(v);
		angelicCompletionToggleButton.notifyListeners(SWT.Selection, null);
	}
	
	public boolean getDivergence() {
		return divergenceToggleButton.getSelection();
	}
	public void setDivergence(boolean v) {
		divergenceToggleButton.setSelection(v);
		divergenceToggleButton.notifyListeners(SWT.Selection, null);
	}
	
	public boolean getStripAddLabelAnno() {
		if (implKindHasStripLabels() && interpHasStripLabels())
			return implStripAddAnnoButton.getSelection();
		return false;
	}
	public void setStripAddLabelAnno(boolean v) {
		implStripAddAnnoButton.setSelection(v);
		implStripAddAnnoButton.notifyListeners(SWT.Selection, null);
	}

	public String getInterpInputs() {
		return interpInText.getText().trim();
	}
	public void setInterpInputs(String v) {
		if (v==null)
			v = "";
		interpInText.setText(v);
		interpInText.notifyListeners(SWT.Modify, null);
	}
	
	public String getInterpOutputs() {
		return interpOutText.getText().trim();
	}
	public void setInterpOutputs(String v) {
		if (v==null)
			v = "";
		interpOutText.setText(v);
		interpOutText.notifyListeners(SWT.Modify, null);
	}

	public boolean useGuide() {
		return guideToggleButton.getSelection();
	}
	public void useGuide(boolean v) {
		guideToggleButton.setSelection(v);
		guideToggleButton.notifyListeners(SWT.Selection, null);
	}
	
	public boolean getUseCoverage() {
		return useCoverageToggleButton.getSelection();
	}
	public void setUseCoverage(boolean v) {
		useCoverageToggleButton.setSelection(v);
		useCoverageToggleButton.notifyListeners(SWT.Selection, null);
	}


	public String getModelFilename() {
		return getFilename(modelText);
	}
	public void setModelFilename(String v) {
		if (v==null)
			v = "";
		modelText.setText(v);
		modelText.notifyListeners(SWT.Modify, null);
	}
	
	public String getImplFilename() {
		return getFilename(implText);
	}
	public void setImplFilename(String v) {
		if (v==null)
			v = "";
		implText.setText(v);
		implText.notifyListeners(SWT.Modify, null);
	}
	
	public RunItemData.AdapterKind getImplKind() {
		return implKind;
	}
	public void setImplKind(RunItemData.AdapterKind v) {
		System.err.println("setImplKind: "+v);
		if (v!=null) {
			for (int i=0; i<implInfo.length; i++) {
				if (implInfo[i].adapterKind.equals(v)) {
					implCombo.select(i);
					implCombo.notifyListeners(SWT.Selection, null);
					return;
				}
			}
		}
		implCombo.deselectAll();
		implCombo.notifyListeners(SWT.Selection, null);
	}
	
	public boolean useImplRealTime() {
		return implRTToggleButton.getSelection();
	}
	public void useImplRealTime(boolean v) {
		implRTToggleButton.setSelection(v);
		implRTToggleButton.notifyListeners(SWT.Selection, null);
	}
	
	public String getGuideFilename() {
		return getFilename(guideText);
	}
	public void setGuideFilename(String v) {
		if (v==null)
			v = "";
		guideText.setText(v);
		guideText.notifyListeners(SWT.Modify, null);
	}
	
	public RunItemData.LTSKind getGuideKind() {
		return guideKind;
	}
	public void setGuideKind(RunItemData.LTSKind v) {
		System.err.println("setGuideKind: "+v);
		if (v!=null) {
			for (int i=0; i<guideInfo.length; i++) {
				if (guideInfo[i].ltsKind.equals(v)) {
					guideCombo.select(i);
					guideCombo.notifyListeners(SWT.Selection, null);
					return;
				}
			}
		}
		guideCombo.deselectAll();
		guideCombo.notifyListeners(SWT.Selection, null);
	}
	
	public String getInstModelFilename() {
		return getFilename(instModelText);
	}
	public void setInstModelFilename(String v) {
		if (v==null)
			v = "";
		instModelText.setText(v);
		instModelText.notifyListeners(SWT.Modify, null);
	}
	
	public String getInstImplFilename() {
		return getFilename(instImplText);
	}
	public void setInstImplFilename(String v) {
		if (v==null)
			v = "";
		instImplText.setText(v);
		instImplText.notifyListeners(SWT.Modify, null);
	}
	
	public boolean useModelInstantiator() {
		return instModelToggleButton.getSelection();
	}
	public void useModelInstantiator(boolean v) {
		instModelToggleButton.setSelection(v);
		instModelToggleButton.notifyListeners(SWT.Selection, null);
	}
	
	public boolean useImplInstantiator() {
		return instImplToggleButton.getSelection();
	}
	public void useImplInstantiator(boolean v) {
		instImplToggleButton.setSelection(v);
		instImplToggleButton.notifyListeners(SWT.Selection, null);
	}
	
	public int getTimeoutValue() {
		if (!timeoutItemsVisibility)
			return -1;
		String t = timeoutText.getText();
		if (t.trim().equals(""))
			return -1;
		int n = -1;
		try {
			n = Integer.parseInt(t);
		}
		catch(NumberFormatException nfe) {
			app.addStatusMsg("timeout: value is not a number: "+t);
		}
		return n;
	}
	public void setTimeoutValue(int v) {
		if (v==-1) {
			timeoutText.setText("");
		} else {
			timeoutText.setText(""+v);
		}
		timeoutText.notifyListeners(SWT.Modify, null);
	}
	
	public TimeUnit getTimeoutUnit() {
		if (!timeoutItemsVisibility)
			return null;
		return unitValue;
	}
	public void setTimeoutUnit(TimeUnit v) {
		System.err.println("setTimeoutUnit: "+v);
		if (v!=null) {
			for (int i=0; i<unitInfo.length; i++) {
				if (unitInfo[i].value.equals(v)) {
					unitCombo.select(i);
					unitCombo.notifyListeners(SWT.Selection, null);
					return;
				}
			}
		}
		unitCombo.deselectAll();
		unitCombo.notifyListeners(SWT.Selection, null);
	}

//	if (fileName!=null && act==BrowseAction.INSERT)
//		textField.insert(fileName);
//	else if (fileName!=null && act==BrowseAction.REPLACE)
//		textField.setText(fileName);

//	public void openConfigFileDialog() {
//		String s  = app.openFileDialog("Load Configuration", "");
//		//if (s!=null)
//		//	modelText.setText(s);
//	}
//	public void saveConfigFileDialog() {
//		String s  = app.saveFileDialog("Save Configuration", "");
//	}

	// ======================================================================
	
	// Code for dialog boxes, visibility etc.

	public void openModelFileDialog() {
		String s  = app.openFileDialog("Open Model", modelText.getText());
		if (s!=null)
			modelText.setText(s);
	}
	public void openGuideFileDialog() {
		String s = app.openFileDialog("Open Guide", guideText.getText());
		if (s!=null)
			guideText.setText(s);
	}
	public void saveGuideFileDialog() {
		String s = app.saveFileDialog("Save Guide as", guideText.getText());
		if (s!=null) {
			//guideText.setText(s);
		}
	}
	public void openImplementationFileDialog() {
		String s = app.openFileDialog("Open Implementation", implText.getText());
		int idx = implCombo.getSelectionIndex();
		if (s!=null && idx >= 0 && idx < implInfo.length &&
				(implInfo[idx].adapterKind == RunItemData.AdapterKind.STDINOUTADAPTER ||
				 implInfo[idx].adapterKind == RunItemData.AdapterKind.TORXADAPTER))
			implText.insert(s);
		else if (s!=null)
			implText.setText(s);
	}
	public void openInstModelFileDialog() {
		String s = app.openFileDialog("Open Instantiator", implText.getText());
		if (s!=null)
			instModelText.insert(s);
		else if (s!=null)
			instModelText.setText(s);
	}
	public void openInstImplFileDialog() {
		String s = app.openFileDialog("Open Instantiator", implText.getText());
		if (s!=null)
			instImplText.insert(s);
		else if (s!=null)
			instImplText.setText(s);
	}


	
	public void setImplSimMode() {
		implCombo.select(0);
		implCombo.notifyListeners(SWT.Selection, null);
	}
	public void setGuideButtonVisibility(boolean b) {
		guideToggleButton.setVisible(b);
		guideTextLabel.setVisible(b);
		guideCombo.setVisible(b);
	}
	public void setGuideFileItemsVisibility(boolean b) {
		guideTextLabel.setVisible(b);
		guideText.setVisible(b);
	}
	public void setGuideViewItemsVisibility(boolean b) {
		guideViewButton.setVisible(b);
		treeConfigPane.setGuideItemsVisibility(b);
		guideBrowseButton.setVisible(b);
	}
	
	public void setGuideItemsVisibility(boolean b) {
		if (b)
			treeConfigPane.addGuide();
		else
			treeConfigPane.rmGuide();
		if (guideCombo.getSelectionIndex() < 0)
			b = false;
		boolean bb = b;
		 // TODO: HACK: should check the boolean array: guideKindBrowsable
		if (guideCombo.getSelectionIndex() > 1)
			bb = false;
		guideTextLabel.setVisible(bb);
		guideText.setVisible(bb);
		guideBrowseButton.setVisible(b);
		guideViewButton.setVisible(b);
		treeConfigPane.setGuideItemsVisibility(b);
	}
	
	public void setImplRealTimeItemsVisibility(boolean b) {
		implRTToggleButton.setVisible(b);
	}
	public void setImplViewItemsVisibility(boolean b) {
		implViewButton.setVisible(b);
		treeConfigPane.setImplItemsVisibility(b);
	}
	public void setImplBrowseItemsVisibility(boolean b) {
		implBrowseButton.setVisible(b);
	}
	public void setImplTimeoutItemsVisibility(boolean b) {
		timeoutLabel.setVisible(b);
		timeoutText.setVisible(b);
		unitCombo.setVisible(b);
		timeoutItemsVisibility = b;
	}
	public void setInstModelItemsVisibility(boolean b) {
		instModelText.setVisible(b);
		instModelBrowseButton.setVisible(b);
	}
	public void setInstImplItemsVisibility(boolean b) {
		instImplText.setVisible(b);
		instImplBrowseButton.setVisible(b);
	}
	public void setInstImplVisibility(boolean b) {
		instImplToggleButton.setVisible(b);
		if (b)
			setInstImplItemsVisibility(instImplToggleButton.getSelection());
		else
			setInstImplItemsVisibility(false);
	}
	
	// ======================================================================
	
	// Configuration save and restore functions
	
	public void fillConfig(Config c) {
		c.add("TRACEKIND", getTraceKind());
		c.add("INTERPKIND", getInterpKind());
		c.add("RECONDELTA", getReconDelta());
		c.add("SYNTHDELTA", getSynthDelta());
		c.add("ANGELCOMPL", getAngelicCompletion());
		c.add("DIVERGENCE", getDivergence());
		c.add("ADDRMLABELANNO", getStripAddLabelAnno());

		c.add("INTERPINPUTS", getInterpInputs());
		c.add("INTERPOUTPUTS", getInterpOutputs());
		
		c.add("USECOVERAGE", getUseCoverage());
		c.add("USEGUIDE", ""+useGuide());
		
		c.add("MODELTEXT", getModelFilename());
		c.add("IMPLTEXT", getImplFilename());
		c.add("ADAPTERKIND", getImplKind());
		c.add("USEREALTIME", useImplRealTime() );

		c.add("GUIDETEXT", getGuideFilename());
		c.add("GUIDEKIND", getGuideKind());
		
		c.add("INSTMODELTEXT", getInstModelFilename());
		c.add("INSTIMPLTEXT", getInstImplFilename());
		c.add("USEMODELINST", useModelInstantiator());
		c.add("USEIMPLINST", useImplInstantiator());
		
		c.add("TIMEOUTVAL", getTimeoutValue());
		c.add("TIMEOUTUNIT", getTimeoutUnit());
	}
	public void loadConfig(Config c) {
		setTraceKind(c.getTraceKind("TRACEKIND"));
		setInterpKind(c.getInterpKind("INTERPKIND"));
		setReconDelta(c.getBool("RECONDELTA"));
		setSynthDelta(c.getBool("SYNTHDELTA"));
		setAngelicCompletion(c.getBool("ANGELCOMPL"));
		setDivergence(c.getBool("DIVERGENCE"));
		setStripAddLabelAnno(c.getBool("ADDRMLABELANNO"));
		
		setInterpInputs(c.getString("INTERPINPUTS"));
		setInterpOutputs(c.getString("INTERPOUTPUTS"));
		
		setUseCoverage(c.getBool("USECOVERAGE"));
		useGuide(c.getBool("USEGUIDE"));
		
		setModelFilename(c.getString("MODELTEXT"));
		setImplFilename(c.getString("IMPLTEXT"));
		setImplKind(c.getAdapterKind("ADAPTERKIND"));
		useImplRealTime(c.getBool("USEREALTIME"));
		
		setGuideFilename(c.getString("GUIDETEXT"));
		setGuideKind(c.getLTSKind("GUIDEKIND"));
		setInstModelFilename(c.getString("INSTMODELTEXT"));
		setInstImplFilename(c.getString("INSTIMPLTEXT"));
		useModelInstantiator(c.getBool("USEMODELINST"));
		useImplInstantiator(c.getBool("USEIMPLINST"));
		
		setTimeoutValue(c.getInt("TIMEOUTVAL"));
		setTimeoutUnit(c.getTimeUnit("TIMEOUTUNIT"));
	}

	// ======================================================================

	public ConfigPane(Composite parent, Testgui g) {
		app = g;
		int ncolumns = 9;

		// Config Pane
		configGroup = new Group(parent, SWT.NONE);
		configGroup.setText("Configuration:");
		GridLayout configLayout = new GridLayout();
		configLayout.numColumns = ncolumns;
		configGroup.setLayout(configLayout);
		
		tabFolder = new CTabFolder(configGroup, SWT.BORDER);
		GridData tabFolderLayoutData = new GridData(GridData.FILL_HORIZONTAL);
		tabFolderLayoutData.horizontalSpan = ncolumns;
		tabFolder.setLayoutData(tabFolderLayoutData);

		
		Composite plainConfigGroup = new Composite(tabFolder, SWT.NONE);
	//	GridData plainConfigLayoutData = new GridData(GridData.FILL_HORIZONTAL);
	//	plainConfigLayoutData.horizontalSpan = 7;
	//	plainConfigGroup.setLayoutData(plainConfigLayoutData);
		GridLayout plainConfigLayout = new GridLayout();
		plainConfigLayout.numColumns = ncolumns;
		plainConfigGroup.setLayout(plainConfigLayout);

		Composite restConfigGroup = new Composite(configGroup, SWT.NONE);
		GridData restConfigLayoutData = new GridData(GridData.FILL_HORIZONTAL);
		restConfigLayoutData.horizontalSpan = ncolumns;
		restConfigGroup.setLayoutData(restConfigLayoutData);
		GridLayout restConfigLayout = new GridLayout();
		restConfigLayout.numColumns = ncolumns;
		restConfigGroup.setLayout(restConfigLayout);



		treeConfigPane = new TreeConfigPane(tabFolder, g);

		



		org.eclipse.swt.widgets.Label modelLabel = new org.eclipse.swt.widgets.Label(plainConfigGroup, SWT.LEFT);
		modelLabel.setText("Model:");
		modelLabel.pack();
//		GridData modelLayoutData = new GridData();
		// modelLayoutData.horizontalAlignment = GridData.HORIZONTAL_ALIGN_BEGINNING;
//		modelLayoutData.grabExcessVerticalSpace = true;
		// modelLayoutData.verticalAlignment = GridData.VERTICAL_ALIGN_CENTER;
//		modelLabel.setLayoutData(modelLayoutData);

		modelText = new Text(plainConfigGroup, SWT.SINGLE);
		GridData modelTextGridData = new GridData(GridData.FILL_HORIZONTAL);
		modelTextGridData.horizontalSpan = ncolumns-3;
		modelText.setLayoutData(modelTextGridData);
		modelText.addModifyListener(new ModifyListener() { 
			public void modifyText(ModifyEvent e) {
				treeConfigPane.setModelText(modelText.getText());
			} 
		});


		modelBrowseButton = new Button(plainConfigGroup,SWT.PUSH);
		modelBrowseButton.setText("Browse");
		modelBrowseButton.pack();
		modelBrowseButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				openModelFileDialog();
			} 
		}); 
		modelBrowseButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));

		modelViewButton = new Button(plainConfigGroup,SWT.PUSH);
		modelViewButton.setText("View");
		modelViewButton.pack();
		modelViewButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.doHandleViewModelEvent();
			} 
		}); 
		modelViewButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));
		app.registerDotVizButton(modelViewButton);

		modelDropTarget = new DropTarget(modelText, DND.DROP_COPY | DND.DROP_DEFAULT);
		modelDropTarget.setTransfer(new Transfer[] {TextTransfer.getInstance(), FileTransfer.getInstance()});
		modelDropListener = new TextDropTargetAdapter(modelText);
		modelDropTarget.addDropListener(modelDropListener);

		
		instModelToggleButton = new Button(plainConfigGroup, SWT.LEFT|SWT.CHECK);
		instModelToggleButton.setText("Instantiator:");
		instModelToggleButton.pack();

		instModelText = new Text(plainConfigGroup, SWT.SINGLE);
		GridData instModelTextLayoutData = new GridData(GridData.FILL_HORIZONTAL);
		instModelTextLayoutData.horizontalSpan = ncolumns-3;
		instModelText.setLayoutData(instModelTextLayoutData);

		instModelBrowseButton = new Button(plainConfigGroup,SWT.PUSH);
		instModelBrowseButton.setText("Browse");
		instModelBrowseButton.pack();
		instModelBrowseButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				openInstModelFileDialog();
			} 
		}); 
		GridData instModelBrowseLayoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		instModelBrowseLayoutData.horizontalSpan = 2;
		instModelBrowseButton.setLayoutData(instModelBrowseLayoutData);

		instModelToggleButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				setInstModelItemsVisibility(useModelInstantiator());
			} 
		}); 


		guideToggleButton = new Button(plainConfigGroup, SWT.LEFT|SWT.CHECK);
		guideToggleButton.setText("Guide:");
		guideToggleButton.pack();
		// guideToggleButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));

		guideCombo = new Combo (plainConfigGroup, SWT.READ_ONLY);
		guideCombo.setSize (200, 200);
		String [] guideItems = new String[guideInfo.length];
		for(int g_i=0; g_i<guideInfo.length; g_i++) {
			guideItems[g_i] = guideInfo[g_i].label;
		}
		guideCombo.setItems (guideItems);
		GridData guideComboGridData = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
		guideComboGridData.horizontalSpan = ncolumns-4;
		guideCombo.setLayoutData(guideComboGridData);

		org.eclipse.swt.widgets.Label coverageLabel =  new org.eclipse.swt.widgets.Label(plainConfigGroup, SWT.RIGHT);
		coverageLabel.setText("Coverage:");
		coverageLabel.pack();
		GridData coverageData = new GridData(GridData.VERTICAL_ALIGN_CENTER|GridData.HORIZONTAL_ALIGN_END);
		coverageData.horizontalSpan = 1;
		coverageLabel.setLayoutData(coverageData);
		
		useCoverageToggleButton = new Button(plainConfigGroup, SWT.LEFT|SWT.CHECK);
		useCoverageToggleButton.setText("Use");
		useCoverageToggleButton.pack();
		GridData useCoverageToggleGridData = new GridData();
		useCoverageToggleGridData.horizontalSpan = 2;
		useCoverageToggleButton.setLayoutData(useCoverageToggleGridData);

		
		guideTextLabel =  new org.eclipse.swt.widgets.Label(plainConfigGroup, SWT.LEFT);
		guideTextLabel.setText("");
		guideTextLabel.pack();

		guideText = new Text(plainConfigGroup, SWT.SINGLE);
		GridData guideTextGridData = new GridData(GridData.FILL_HORIZONTAL);
		guideTextGridData.horizontalSpan = ncolumns-3;
		guideText.setLayoutData(guideTextGridData);

		guideText.addModifyListener(new ModifyListener() { 
			public void modifyText(ModifyEvent e) {
				treeConfigPane.setGuideText(guideText.getText());
			} 
		}); 

		guideBrowseButton = new Button(plainConfigGroup,SWT.PUSH);
		guideBrowseButton.setText("Browse");
		guideBrowseButton.pack();
		guideBrowseButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				if (guideIndex >= 0 && guideInfo[guideIndex].browsable)
					openGuideFileDialog();
				else if (guideIndex >= 0 && guideInfo[guideIndex].savable)
					app.doHandleSaveGuideEvent();
			} 
		}); 
		guideBrowseButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));

		guideViewButton = new Button(plainConfigGroup,SWT.PUSH);
		guideViewButton.setText("View");
		guideViewButton.pack();
		guideViewButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.doHandleViewGuideEvent();
			} 
		}); 
		guideViewButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));
		app.registerDotVizButton(guideViewButton);

		guideDropTarget = new DropTarget(guideText, DND.DROP_COPY | DND.DROP_DEFAULT);
		guideDropTarget.setTransfer(new Transfer[] {TextTransfer.getInstance(), FileTransfer.getInstance()});
		guideDropListener = new TextDropTargetAdapter(guideText);
		guideDropTarget.addDropListener(guideDropListener);



		guideToggleButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.setGuideItemsVisibility(useGuide());
			} 
		}); 

		/* LazyOTF checkbox */
		useLazyOTFToggleButton = new Button(plainConfigGroup, SWT.LEFT | SWT.CHECK);
		useLazyOTFToggleButton.setText("Choose stimuli using LazyOTF");
		useLazyOTFToggleButton.setSelection(false);
		useLazyOTFToggleButton.pack();
		GridData useLazyOTFToggleButtonGridData = new GridData(GridData.FILL_HORIZONTAL);
		useLazyOTFToggleButtonGridData.horizontalSpan = ncolumns;
		useLazyOTFToggleButton.setLayoutData(useLazyOTFToggleButtonGridData);
		useLazyOTFToggleButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				app.setUseLazyOTF(isLazyOTFEnabled());
				lazyOTFConfigPane.setEnabled(isLazyOTFEnabled());
			}
		});

		org.eclipse.swt.widgets.Label implLabel = new org.eclipse.swt.widgets.Label(plainConfigGroup, SWT.LEFT);
		implLabel.setText("Implementation:");
		implLabel.pack();
		// implLabel.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		
		implCombo = new Combo (plainConfigGroup, SWT.READ_ONLY);
		//implCombo.setSize (200, 200);
		String [] implItems = new String[implInfo.length];
		for(int i_i=0; i_i<implInfo.length; i_i++) {
			implItems[i_i] = implInfo[i_i].label;
		}
		implCombo.setItems (implItems);
		GridData implComboGridData = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
		implComboGridData.horizontalSpan = ncolumns-3;
		implCombo.setLayoutData(implComboGridData);

		implStripAddAnnoButton = new Button(plainConfigGroup, SWT.LEFT|SWT.CHECK);
		implStripAddAnnoButton.setText("-?+!");
		//implRTToggleButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		implStripAddAnnoButton.pack();
		GridData implStripAddAnnoGridData = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
		implStripAddAnnoGridData.horizontalSpan = 1;
		implStripAddAnnoButton.setLayoutData(implStripAddAnnoGridData);
		implStripAddAnnoButton.setVisible(false);
	
		implRTToggleButton = new Button(plainConfigGroup, SWT.LEFT|SWT.CHECK);
		implRTToggleButton.setText("RealTime");
		//implRTToggleButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		implRTToggleButton.pack();
		GridData implRTToggleGridData = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
		implRTToggleGridData.horizontalSpan = 1;
		implRTToggleButton.setLayoutData(implRTToggleGridData);
		implRTToggleButton.setVisible(false);
		
		implTextLabel =  new org.eclipse.swt.widgets.Label(plainConfigGroup, SWT.LEFT);
		implTextLabel.setText("");
		implTextLabel.pack();
		// implTextLabel.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));


		implText = new Text(plainConfigGroup, SWT.SINGLE);
		GridData implTextGridData = new GridData(GridData.FILL_HORIZONTAL);
		implTextGridData.horizontalSpan = ncolumns-3;
		implText.setLayoutData(implTextGridData);

		implText.addModifyListener(new ModifyListener() { 
			public void modifyText(ModifyEvent e) {
				treeConfigPane.setImplText(implText.getText());
			} 
		}); 


		implBrowseButton = new Button(plainConfigGroup,SWT.PUSH);
		implBrowseButton.setText("Browse");
		implBrowseButton.pack();
		implBrowseButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				openImplementationFileDialog();
			} 
		}); 
		implBrowseButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));

		implViewButton = new Button(plainConfigGroup,SWT.PUSH);
		implViewButton.setText("View");
		implViewButton.pack();
		implViewButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.doHandleViewImplEvent();
			} 
		}); 
		implViewButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));
		app.registerDotVizButton(implViewButton);

		implDropTarget = new DropTarget(implText, DND.DROP_COPY | DND.DROP_DEFAULT);
		implDropTarget.setTransfer(new Transfer[] {TextTransfer.getInstance(), FileTransfer.getInstance()});
		implDropListener = new TextDropTargetAdapter(implText);
		implDropTarget.addDropListener(implDropListener);
		
		
		instImplToggleButton = new Button(plainConfigGroup, SWT.LEFT|SWT.CHECK);
		instImplToggleButton.setText("Instantiator:");
		instImplToggleButton.pack();

		instImplText = new Text(plainConfigGroup, SWT.SINGLE);
		GridData instImplTextLayoutData = new GridData(GridData.FILL_HORIZONTAL);
		instImplTextLayoutData.horizontalSpan = ncolumns-3;
		instImplText.setLayoutData(instImplTextLayoutData);

		instImplBrowseButton = new Button(plainConfigGroup,SWT.PUSH);
		instImplBrowseButton.setText("Browse");
		instImplBrowseButton.pack();
		instImplBrowseButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				openInstImplFileDialog();
			} 
		}); 
		GridData instImplBrowseLayoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		instImplBrowseLayoutData.horizontalSpan = 2;
		instImplBrowseButton.setLayoutData(instImplBrowseLayoutData);

		instImplToggleButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				setInstImplItemsVisibility(useImplInstantiator());
			} 
		}); 

		instImplToggleButton.setVisible(false);
		
		
		CTabItem plainItem = new CTabItem(tabFolder, SWT.NONE);
		plainItem.setText("Config Items");
		plainItem.setControl(plainConfigGroup);
		plainItem.setToolTipText("Configuration for all tool functionality offered by JTorX");
	
		CTabItem treeItem = new CTabItem(tabFolder, SWT.NONE);
		treeItem.setText("Components");
		treeItem.setControl(treeConfigPane.getGroup());
		treeItem.setToolTipText("View on Tool Components resulting from Configuration, giving access to Visualization and Simulation");

		/* LazyOTF tab */
		lazyOTFtab = new CTabItem(tabFolder, SWT.NONE);
		lazyOTFtab.setText("LazyOTF");
		lazyOTFtab.setToolTipText("LazyOTF configuration");
		lazyOTFParentComposite = new Composite(tabFolder, SWT.NONE);
		lazyOTFtab.setControl(lazyOTFParentComposite);
		lazyOTFConfigPane = new LazyOTFConfigPane(lazyOTFParentComposite);
		lazyOTFConfigPane.setEnabled(false);
		lazyOTFtab.dispose(); // to be recreated by setLazyOTFConfigurationTabVisible
				
		tabFolder.setSelection(0); 
		tabFolder.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				if (tabFolder.getSelectionIndex() == 1){
				} else {
				}
			}  }); 
	
		plainConfigGroup.pack();
		//treeConfigPane.getGroup().pack();
		tabFolder.pack();

		
		
		timeoutLabel = new org.eclipse.swt.widgets.Label(restConfigGroup, SWT.LEFT);
		timeoutLabel.setText("Timeout:");
		timeoutLabel.pack();

		timeoutText = new Text(restConfigGroup, SWT.SINGLE );
		
	    int columns = 15;
	    GC gc = new GC(timeoutText);
	    FontMetrics fm = gc.getFontMetrics();
	    int width = columns * fm.getAverageCharWidth();
	    int height = fm.getHeight();
	    gc.dispose();
	    Point p = timeoutText.computeSize(width, height);
	    timeoutText.setSize(p);
		timeoutText.pack(); 

		GridData timeoutTextGridData = new GridData(GridData.VERTICAL_ALIGN_BEGINNING|GridData.VERTICAL_ALIGN_CENTER);
		timeoutTextGridData.minimumWidth = width;
		timeoutTextGridData.minimumHeight = height;
		timeoutTextGridData.widthHint = width;
		timeoutTextGridData.heightHint = height;
		timeoutText.setLayoutData(timeoutTextGridData);
		
		// org.eclipse.swt.widgets.Label unitLabel = new org.eclipse.swt.widgets.Label(configGroup, SWT.LEFT);
		// unitLabel.setText("Unit:");
		// unitLabel.pack();

		unitCombo = new Combo (restConfigGroup, SWT.READ_ONLY);
		String [] unitItems = new String[unitInfo.length];
		for(int u_i=0;u_i<unitInfo.length; u_i++) {
			unitItems[u_i] = unitInfo[u_i].label;
		}
		unitCombo.setItems (unitItems);

		GridData unitComboGridData = new GridData(GridData.VERTICAL_ALIGN_BEGINNING|GridData.BEGINNING);
		unitComboGridData.horizontalSpan = ncolumns-2;
		unitCombo.setLayoutData(unitComboGridData);
		
		unitCombo.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.addStatusMsg("");
				int idx = unitCombo.getSelectionIndex();
				if (debug)
					System.err.println("unit combo index "+idx);
				if (idx >= 0 && idx < unitInfo.length) {
					unitValue = unitInfo[idx].value;
					//implTextLabel.update();
					//app.getDisplay().update();
				} else {
					//app.addStatusMsg("oops... internal error: unitCombo out of bounds");
					unitValue = null;
				}
			} 
		});

		timeoutLabel.setVisible(false);
		timeoutText.setVisible(false);
		unitCombo.setVisible(false);
		timeoutItemsVisibility =  false;

		guideCombo.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.addStatusMsg("");
				guideIndex = guideCombo.getSelectionIndex();
				if (debug)
					System.err.println("guide combo index "+guideIndex);
				if (guideIndex >= 0 && guideIndex < guideInfo.length) {
					guideTextLabel.setText(guideInfo[guideIndex].type);
					guideTextLabel.pack();
					guideKind = guideInfo[guideIndex].ltsKind;
					guideBrowseButton.setText(guideInfo[guideIndex].dialogType);
					guideBrowseButton.pack();
					setGuideFileItemsVisibility(useGuide() && guideInfo[guideIndex].browsable);
					setGuideViewItemsVisibility(useGuide() && guideInfo[guideIndex].viewable);
					//implTextLabel.update();
					//app.getDisplay().update();
				} else {
					app.addStatusMsg("oops... internal error: guideCombo out of bounds");
					guideTextLabel.setText("");
					guideTextLabel.pack();
					guideKind = null;
					setGuideFileItemsVisibility(false);
					setGuideViewItemsVisibility(false);
				}
			} 
		});
	
		implCombo.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.addStatusMsg("");
				int idx = implCombo.getSelectionIndex();
				if (debug)
					System.err.println("impl combo index "+idx);
				if (idx >= 0 && idx < implInfo.length) {
					implTextLabel.setText(implInfo[idx].type);
					implTextLabel.pack();
					implKind = implInfo[idx].adapterKind;
					app.setImplSeedItemsVisibility(implInfo[idx].needsSeed);
					app.setImplViewItemsVisibility(implInfo[idx].viewable);
					app.setImplBrowseItemsVisibility(implInfo[idx].browsable);
					app.setImplTimeoutItemsVisibility(implInfo[idx].needsTimeout);
					app.setImplRealTimeItemsVisibility(implInfo[idx].hasRealtime);
					implStripAddAnnoButton.setVisible(implKindHasStripLabels()
							&& interpHasStripLabels());
					setInstImplVisibility(implInfo[idx].mayNeedInstantiator);
					//implTextLabel.update();
					//app.getDisplay().update();
				} else {
					// app.addStatusMsg("oops... internal error: implCombo out of bounds");
					implTextLabel.setText("");
					implTextLabel.pack();
					implKind = null;
					app.setImplSeedItemsVisibility(false);
					app.setImplViewItemsVisibility(false);
					app.setImplBrowseItemsVisibility(false);
					app.setImplTimeoutItemsVisibility(false);
					app.setImplRealTimeItemsVisibility(false);
					implStripAddAnnoButton.setVisible(false);
					setInstImplVisibility(false);
				}
			} 
		}); 
		



		org.eclipse.swt.widgets.Label interpLabel = new org.eclipse.swt.widgets.Label(restConfigGroup, SWT.LEFT);
		interpLabel.setText("Interpretation:");
		interpLabel.pack();
		// interpLabel.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));

		/* 
		interpText = new Text(configGroup, SWT.SINGLE);
		interpText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		final Button interpButton = new Button(configGroup,SWT.PUSH);
		interpButton.setText("Browse");
		interpButton.pack();
		interpButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				addStatusMsg("");
				fileDialog.setText("Open Interpretation");
				fileDialog.setFilterPath(interpText.getText()); 
				String fileName = fileDialog.open();
				if (fileName!=null)
					interpText.setText(fileName);
			} 
		}); 
		interpButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));
		 */

		
		interpCombo = new Combo (restConfigGroup, SWT.READ_ONLY);
		String [] interpItems = new String[interpInfo.length];
		for(int ii_i=0; ii_i<interpInfo.length; ii_i++) {
			interpItems[ii_i] = interpInfo[ii_i].label;
		}
		interpCombo.setItems (interpItems);
		interpCombo.setSize (200, 200);

		GridData interpComboLayoutData = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
		interpComboLayoutData.horizontalSpan = 3;
		interpCombo.setLayoutData(interpComboLayoutData);
		
		
		org.eclipse.swt.widgets.Label traceKindLabel = new org.eclipse.swt.widgets.Label(restConfigGroup, SWT.RIGHT);
		traceKindLabel.setText("Trace kind:");
		//traceKindLabel.pack();

		
		traceKindCombo = new Combo (restConfigGroup, SWT.READ_ONLY);
		String [] traceItems = new String[traceInfo.length];
		for(int t_i=0; t_i<traceInfo.length; t_i++) {
			traceItems[t_i] = traceInfo[t_i].label;
		}
		traceKindCombo.setItems (traceItems);

		//traceKindCombo.setSize (200, 200);
		traceKindCombo.pack();
		
		
		final org.eclipse.swt.widgets.Label deltaLabel = new org.eclipse.swt.widgets.Label(restConfigGroup, SWT.RIGHT);
		deltaLabel.setText("Delta:");
		deltaLabel.pack();
		GridData deltaData = new GridData(GridData.VERTICAL_ALIGN_CENTER|GridData.HORIZONTAL_ALIGN_END);
		deltaData.horizontalSpan = 1;
		deltaLabel.setLayoutData(deltaData);
		
		interpDeltaToggleButton = new Button(restConfigGroup, SWT.LEFT|SWT.CHECK);
		interpDeltaToggleButton.setText("recognize");
		interpDeltaToggleButton.pack();
		// interpDeltaToggleButton.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));

		GridData interpDeltaLayoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		interpDeltaLayoutData.horizontalSpan = 1;
		interpDeltaToggleButton.setLayoutData(interpDeltaLayoutData);


		synthDeltaToggleButton = new Button(restConfigGroup, SWT.LEFT|SWT.CHECK);
		synthDeltaToggleButton.setText("synth");
		synthDeltaToggleButton.pack();
		synthDeltaToggleButton.setSelection(true);

		GridData synthDeltaLayoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		synthDeltaLayoutData.horizontalSpan = 1;
		synthDeltaToggleButton.setLayoutData(synthDeltaLayoutData);


		interpInLabel = new org.eclipse.swt.widgets.Label(restConfigGroup, SWT.LEFT);
		interpInLabel.setText("Input actions:");
		interpInLabel.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		interpInText = new Text(restConfigGroup, SWT.SINGLE);
		GridData interpInLayoutData = new GridData(GridData.FILL_HORIZONTAL);
		interpInLayoutData.horizontalSpan = ncolumns-3;
		interpInText.setLayoutData(interpInLayoutData);

		divergenceToggleButton = new Button(restConfigGroup, SWT.RIGHT|SWT.CHECK);
		divergenceToggleButton.setText("delta for tau loops");
		divergenceToggleButton.pack();

		GridData divergenceLayoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		divergenceLayoutData.horizontalSpan = 2;
		divergenceToggleButton.setLayoutData(divergenceLayoutData);



		interpOutLabel = new org.eclipse.swt.widgets.Label(restConfigGroup, SWT.LEFT);
		interpOutLabel.setText("Output actions:");
		interpOutLabel.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		interpOutText = new Text(restConfigGroup, SWT.SINGLE);
		GridData interpOutLayoutData = new GridData(GridData.FILL_HORIZONTAL);
		interpOutLayoutData.horizontalSpan = ncolumns-3;
		interpOutText.setLayoutData(interpOutLayoutData);
		
		angelicCompletionToggleButton = new Button(restConfigGroup, SWT.RIGHT|SWT.CHECK);
		angelicCompletionToggleButton.setText("angelic completion");
		angelicCompletionToggleButton.pack();

		GridData angelicCompletionLayoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		angelicCompletionLayoutData.horizontalSpan = 2;
		angelicCompletionToggleButton.setLayoutData(angelicCompletionLayoutData);

		
		interpInLabel.setVisible(false);
		interpOutLabel.setVisible(false);
		interpInText.setVisible(false);
		interpOutText.setVisible(false);
		interpCombo.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.addStatusMsg("");
				int idx = interpCombo.getSelectionIndex();
				if (debug)
					System.err.println("interp combo index "+idx);
				if (idx >= 0 && idx < interpInfo.length) {
					interpKind = interpInfo[idx].kind;
					interp = getReconDelta() ? interpInfo[idx].deltaInterp : interpInfo[idx].interp;
					interpInLabel.setVisible(interpInfo[idx].needsLabels);
					interpOutLabel.setVisible(interpInfo[idx].needsLabels);
					interpInText.setVisible(interpInfo[idx].needsLabels);
					interpOutText.setVisible(interpInfo[idx].needsLabels);
					implStripAddAnnoButton.setVisible(implKindHasStripLabels()
							&& interpHasStripLabels());
				} else {
					// app.addStatusMsg("oops... internal error: interpComb out of bounds");
					interpKind = null;
					interp = null;
					interpInLabel.setVisible(false);
					interpOutLabel.setVisible(false);
					interpInText.setVisible(false);
					interpOutText.setVisible(false);
					implStripAddAnnoButton.setVisible(false);
				}
			//	if (idx == 3) {
			//		app.addStatusMsg("oops... \"from file:\"  setting not implemented yet");
			//	} 
			} 
		});
		interpDeltaToggleButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				int idx = interpCombo.getSelectionIndex();
				if (debug)
					System.err.println("interpDeltaToggleButton: interp combo index "+idx);
				if (idx >= 0 && idx < interpInfo.length)
					interp = getReconDelta() ? interpInfo[idx].deltaInterp : interpInfo[idx].interp;
				if (idx == 2 && getReconDelta()) {
					String tmp = " "+interpOutText.getText()+" ";
					boolean found = tmp.contains(" delta ");
					if (true)
						System.err.println("interpDeltaToggleButton: match("+tmp+") : "+found);

					if (! found)
						interpOutText.setText("delta"+" "+interpOutText.getText());
				} else if (idx == 2 && !getReconDelta()) {
					String tmp = " "+interpOutText.getText()+" ";
					interpOutText.setText(tmp.replaceAll(" +delta +", " ").trim());
				}
			} 
		}); 


		traceKindCombo.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.addStatusMsg("");
				int idx = traceKindCombo.getSelectionIndex();
				System.err.println("traceKind combo index "+idx);
				if (idx >= 0 && idx < traceInfo.length) {
					traceKind = traceInfo[idx].kind;
				} else {
					// app.addStatusMsg("oops... internal error: traceKindComb out of bounds");
					traceKind = null;
				}
			} 
		});

		
		enableListeners();
	}
	
	// ======================================================================
	

	private String getFilename(Text t) {
		if (t.getText() != null)
			return t.getText().trim();
		else
			return "";
	}
	
	private void enableListeners() {
		interpInText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) { 
				actionListInterpretation.updateInputs(interpInText.getText().trim());
			}
		});
		interpOutText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) { 
				actionListInterpretation.updateOutputs(interpOutText.getText().trim());
			}
		});
	}

	// ======================================================================
	
	// Tables that describe the information for the combo items
	
	public class GuideInfo {
		public String label;
		public String type;
		public boolean viewable;
		public boolean browsable;
		public String dialogType;
		public boolean savable;
		public RunItemData.LTSKind ltsKind;
		
		public GuideInfo(String l, String t, boolean v, boolean b, String d, boolean s, RunItemData.LTSKind k) {
			label = l;
			type = t;
			viewable = v;
			browsable = b;
			dialogType = d;
			savable = s;
			ltsKind = k;
		}
	}
	// get rid of IOCOCHECKERSINGLETRACE "Save" dialogType, and add Save to iocoChecker Pane?
	// get rid of IOCOCHECKERMULTITRACE "Save" dialogType, and add Save to iocoChecker Pane?

	private GuideInfo[] guideInfo = new GuideInfo[] {
			new GuideInfo("use model below (already containing epsilon labels)", "Model:", true, true,  "Browse", false, RunItemData.LTSKind.MODEL),
			new GuideInfo("use trace below (add epsilon label to end states)",   "Trace:", true, true,  "Browse", false, RunItemData.LTSKind.TRACE),
			new GuideInfo("use selected iocoChecker failure suspension trace",   "",       true, false, "Save",   true,  RunItemData.LTSKind.IOCOCHECKERSINGLETRACE),
			// next item is not shown for now, because it stopped working when we changed iocoChecker to show results immediately
			// new GuideInfo("use combination of all iocoChecker failures",         "",       true, false, "Sve",    true,  RunItemData.LTSKind.IOCOCHECKERMULTITRACE),
	};

	public class ImplInfo {
		public String label;
		public String type;
		public boolean needsSeed;
		public boolean viewable;
		public boolean browsable;
		public boolean needsTimeout;
		public boolean hasRealtime;
		public boolean mayNeedInstantiator;
		public boolean hasStripLabels;
		public RunItemData.AdapterKind adapterKind;
		
		public ImplInfo(String l, String t, boolean s, boolean v, boolean b, boolean nt, boolean r, boolean i, boolean sl, RunItemData.AdapterKind k) {
			label = l;
			type = t;
			needsSeed = s;
			viewable = v;
			browsable = b;
			needsTimeout = nt;
			hasRealtime = r;
			mayNeedInstantiator = i;
			hasStripLabels = sl;
			adapterKind = k;
		}
	}

	private ImplInfo[] implInfo = new ImplInfo[] {
			new ImplInfo("simulation of given model - directly connected",      "Model:",                         true,  true,  true,  true /*a timed Model or model-used-as-Impl may need it*/, false, true,  false, RunItemData.AdapterKind.SIMADAPTER),
            new ImplInfo("ioco simulation of given model - directly connected", "Model:",                         true,  true,  true,  true /*a timed Model or model-used-as-Impl may need it*/, false, true,  false, RunItemData.AdapterKind.IOCOSIMADAPTER),
			new ImplInfo("real program, comm. labels on stdin/stdout",          "Program+args:",                  false, false, true,  true,                                                     true,  false, true,  RunItemData.AdapterKind.STDINOUTADAPTER),
			new ImplInfo("real program, comm. labels via tcp, JTorX is client", "Hostname!Portnr:",               false, false, false, true,                                                     false, false, true,  RunItemData.AdapterKind.TCPCLIENTLABELADAPTER),
			new ImplInfo("real program, comm. labels via tcp, JTorX is server", "*!Portnr:",                      false, false, false, true,                                                     false, false, true,  RunItemData.AdapterKind.TCPSERVERLABELADAPTER),
			new ImplInfo("real program, started via given TorX adapter",        "Adapter:"/*"Adapter Program:"*/, false, false, true,  true,                                                     false, false, false, RunItemData.AdapterKind.TORXADAPTER),
			new ImplInfo("re-run of a saved test run log",                      "Log:",                           false, true,  true,  true /* actually, rthe Model may need it...*/,            false, false, false, RunItemData.AdapterKind.LOGADAPTER),
	};
	
	private boolean implKindHasStripLabels() {
		int idx = implCombo.getSelectionIndex();
		if (idx >= 0 && idx < implInfo.length) 
			return implInfo[idx].hasStripLabels;
		return false;
	}

	public class UnitInfo {
		public String label;
		public TimeUnit value;
		
		public UnitInfo(String l, TimeUnit v) {
			label = l;
			value = v;
		}

	}
	private UnitInfo[] unitInfo = new UnitInfo[] {
			new UnitInfo("seconds",      TimeUnit.SECONDS),
			new UnitInfo("milliseconds", TimeUnit.MILLISECONDS),
			// new UnitInfo("microseconds", TimeUnit.MICROSECONDS),
	};
	
	public class InterpInfo {
		public String label;
		public IOLTSInterpretation interp;
		public IOLTSInterpretation deltaInterp;
		public boolean needsLabels;
		public boolean hasStripLabels;
		public InterpKind kind;
		
		public InterpInfo(String l, IOLTSInterpretation i, IOLTSInterpretation di, boolean nl, boolean hsl, InterpKind k) {
			label = l;
			interp = i;
			deltaInterp = di;
			needsLabels = nl;
			hasStripLabels = hsl;
			kind = k;
		}
	}
	
	private IOLTSInterpretation prefixInterpretation = new PrefixInterpretation();
	private IOLTSInterpretation postfixInterpretation = new PostfixInterpretation();
	private IOLTSInterpretation prefixDeltaInterpretation = new PrefixDeltaInterpretation();
	private IOLTSInterpretation postfixDeltaInterpretation = new PostfixDeltaInterpretation();

	private ActionListInterpretation actionListInterpretation = new ActionListInterpretation("","");
	
	private InterpInfo[] interpInfo = new InterpInfo[] {
			new InterpInfo("in? out!",           postfixInterpretation,    postfixDeltaInterpretation, false, true,  InterpKind.POSTFIX),
			new InterpInfo("?in !out",           prefixInterpretation,     prefixDeltaInterpretation,  false, true,  InterpKind.PREFIX),
			new InterpInfo("action names below", actionListInterpretation, actionListInterpretation,   true,  false, InterpKind.ACTIONLIST),
			// new InterpInfo("from file:",         null,                     null,                       false, false, null),
	};

	
	private boolean interpHasStripLabels() {
		int idx = interpCombo.getSelectionIndex();
		if (idx >= 0 && idx < interpInfo.length) 
			return interpInfo[idx].hasStripLabels;
		return false;
	}

	public class TraceInfo {
		public String label;
		public TraceKind kind;
		
		public TraceInfo(String l, TraceKind k) {
			label = l;
			kind = k;
		}
	}
	private TraceInfo[] traceInfo = new TraceInfo[] {
			new TraceInfo("Straces", TraceKind.STRACES),
			new TraceInfo("Utraces", TraceKind.UTRACES),
	};

	public boolean isLazyOTFEnabled() {
		return useLazyOTFToggleButton.getSelection();
	}
	
	public void setLazyOTFEnabled(boolean enabledness) {
		useLazyOTFToggleButton.setSelection(enabledness);
		lazyOTFConfigPane.setEnabled(enabledness);
	}

	public void setLazyOTFConfigurationTabVisible(boolean visibility) {
		if(visibility == false) {
			if(lazyOTFtab != null && !lazyOTFtab.isDisposed()) {
				lazyOTFtab.dispose();
			}
		}
		else if(this.lazyOTFavailable == true) {
			if(lazyOTFtab == null || lazyOTFtab.isDisposed()) {
				lazyOTFtab = new CTabItem(tabFolder, SWT.NONE);
				lazyOTFtab.setControl(lazyOTFParentComposite);
				lazyOTFtab.setText("LazyOTF");
				lazyOTFtab.setToolTipText("LazyOTF configuration");
				configGroup.layout();
			}
		}
	}

	public LazyOTFConfigPane getLazyOTFConfigPane() {
		return lazyOTFConfigPane;
	}
	
	public void setLazyOTFAvailable(boolean available) {
		this.lazyOTFavailable = available;
		this.useLazyOTFToggleButton.setEnabled(available);
		setLazyOTFConfigurationTabVisible(false);
	}
	
	private Testgui app;
	private Group configGroup = null;
	
	private final CTabFolder tabFolder;
	private TreeConfigPane treeConfigPane = null;
	
	private Text modelText = null;
	private Button modelBrowseButton = null;
	private Button modelViewButton = null;
	private DropTarget modelDropTarget = null;
	private DropTargetListener modelDropListener = null;

	private Button guideToggleButton = null;
	private Combo guideCombo = null;
	private int guideIndex = -1;
	private org.eclipse.swt.widgets.Label guideTextLabel = null;
	private RunItemData.LTSKind guideKind = null;
	private Text guideText = null;
	private Button guideBrowseButton = null;
	private Button guideViewButton = null;
	private DropTarget guideDropTarget = null;
	private DropTargetListener guideDropListener = null;
	
	private org.eclipse.swt.widgets.Label timeoutLabel;
	private Text timeoutText = null;
	private Combo unitCombo = null;
	private TimeUnit unitValue = null;
	private boolean timeoutItemsVisibility = false;

	private Combo implCombo = null;
	private org.eclipse.swt.widgets.Label implTextLabel = null;
	private RunItemData.AdapterKind implKind = null;
	private Button implRTToggleButton = null;
	private Button implStripAddAnnoButton = null;
	private Text implText = null;
	private Button implBrowseButton = null;
	private Button implViewButton = null;
	private DropTarget implDropTarget = null;
	private DropTargetListener implDropListener = null;

	private InterpKind interpKind = null;
	private Combo interpCombo = null;
	private Button useCoverageToggleButton = null;
	private Button angelicCompletionToggleButton = null;
	private Button divergenceToggleButton = null;
	private Button interpDeltaToggleButton = null;
	//private Text interpText = null;
	private org.eclipse.swt.widgets.Label interpInLabel;
	private org.eclipse.swt.widgets.Label interpOutLabel;
	private Text interpInText = null;
	private Text interpOutText = null;
	
	private Combo traceKindCombo = null;
	private TraceKind traceKind = null;
	private Button synthDeltaToggleButton = null;
	
	private Button instModelToggleButton = null;
	private Button instModelBrowseButton = null;
	private Text instModelText = null;
	
	private Button instImplToggleButton = null;
	private Button instImplBrowseButton = null;
	private Text instImplText = null;


	private IOLTSInterpretation interp = null;
	
	private boolean debug = false;

	private final LazyOTFConfigPane lazyOTFConfigPane;
	private final Composite lazyOTFParentComposite;
	private Button useLazyOTFToggleButton = null;
	private CTabItem lazyOTFtab = null;
	private boolean lazyOTFavailable = true;
}
