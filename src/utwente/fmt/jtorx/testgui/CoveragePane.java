package utwente.fmt.jtorx.testgui;

import java.util.Collections;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import utwente.fmt.jtorx.logger.coverage.CoverageData;
import utwente.fmt.jtorx.logger.coverage.CoverageFactory;
import utwente.fmt.jtorx.logger.coverage.CoverageData.EdgeType;
import utwente.fmt.jtorx.logger.coverage.Statistics;
import utwente.fmt.jtorx.utils.Pair;

public class CoveragePane implements JTorXPane {

	public void runSetup() {
		// TODO Auto-generated method stub
	}
	public void runBegins() {
		resetCoverageButton.setEnabled(false);
	}
	public void runEnds() {
		resetCoverageButton.setEnabled(true);
	}
	
	public Group getGroup() {
		return coverageGroup;
	}

	
	// coverage: keep track of it elsewhere, here only gui.
	//	-> for now, keep track of data in coverage.CoverageData
	
	//  while we do not change model, guidance or implementation
	//	keep the same suspension automaton, for which we keep
	//	track of coverage information.
	//	-> unfortunately, each test run now gives us new model, new suspension automaton
	
	//	guidance information may vary over the runs, as long as model remains the same
	//	(we compute coverage of model, not of model+guidance)
	//	maintain model/susp automaton coverage info as part of (with) RunSessionData?
	//	for suspension automaton (and model) keep track of:
	//		- states
	//			- visited
	//			- expanded
	//			- unexpanded
	//		- transitions
	//			- visited
	//			- special count of quiescence transitions
	//				- those that change to different state
	//				- those that remain in same state
	//
	//	update during test run?
	//	update at end of test run?
	
	//	what about infinite models?
	//	what about models for which we do not know whether/not finite?
	// how much work is it to compute all possible traces of a given length?
	// (to make the connection to semantic coverage)
	// are there coverage (equivalence) classes among those traces?
	// for example, via work of Nicu Goga?
	
	// in gui:
	//	- reset coverage info button?
	//	- list of all runs, per run ability to toggle run on/off (whether/not contribute to coverage)
	//	  (also let effect of toggling show in visualization?)
	//	- data (numbers that we keep) in table
	
	public void reportModelSuspautNumbers(Statistics m, Statistics sa) {
		final Statistics[] all = { m, sa };
		app.getDisplay().syncExec (new Runnable () {
			public void run () {
				System.out.println("reportModelSuspautNumbers...table.getItemCount:"+table.getItemCount());
				TableItem item ;
				if (table.getItemCount() <= 0) {
					item = new TableItem (table, SWT.NONE);
				} else {
					item = table.getItem(0);
				}
				int i = 0;
				for(Statistics s: all) {
					item.setText(i++, s.getID());
					item.setText(i++, ""+s.nrOfNodes());
					item.setText(i++, ""+s.nrOfEdges());
					item.setText(i++, ""+s.nrOfEdgesByType()[EdgeType.DELTALOOP.ordinal()]);
					item.setText(i++, ""+s.nrOfEdgesByType()[EdgeType.OTHERLOOP.ordinal()]);
					item.setText(i++, ""+s.nrOfEdgesByType()[EdgeType.DELTANONLOOP.ordinal()]);
					item.setText(i++, ""+s.nrOfEdgesByType()[EdgeType.OTHERNONLOOP.ordinal()]);
				}
			}
		});
	}
	public void reportModelSuspautItem( int k, Statistics m, Statistics sa) {
		final Statistics[] all = { m, sa };
		final int n = k;
		app.getDisplay().syncExec (new Runnable () {
			public void run () {
				TableItem item;
				System.out.println("reportModelSuspautItem...n:"+n);
				while (table.getItemCount() <= n) {
					item = new TableItem (table, SWT.NONE);
					table.pack();
				}
					item = table.getItem(n);
					int i = 0;
					for(Statistics s: all) {
						item.setText(i++, s.getID());
						item.setText(i++, ""+s.nrOfNodes());
						item.setText(i++, ""+s.nrOfEdges());
						item.setText(i++, ""+s.nrOfEdgesByType()[EdgeType.DELTALOOP.ordinal()]);
						item.setText(i++, ""+s.nrOfEdgesByType()[EdgeType.OTHERLOOP.ordinal()]);
						item.setText(i++, ""+s.nrOfEdgesByType()[EdgeType.DELTANONLOOP.ordinal()]);
						item.setText(i++, ""+s.nrOfEdgesByType()[EdgeType.OTHERNONLOOP.ordinal()]);
					}

				for (i=0; i<titles.length; i++) {
					table.getColumn (i).pack ();
				}
			}
		});
	}
	public void reportModelSuspautRun( int k, Statistics m, Statistics sa) {
		reportModelSuspautItem(k+2, m, sa);
	}
	
	public void reportModelSuspautSum(Statistics m, Statistics sa) {
		reportModelSuspautItem(1, m, sa);
	}
	
	public void setCoverage(CoverageData c) {
		System.out.println("setCoverage("+c+")");
		if (c==coverage)
			return;
		
		if (coverage != null) {
			coverage.unsetPane();
			reset();
		}
		coverage = c;
		if (coverage!=null) {

			coverage.setPane(this);
			String mi = coverage.getModelInfo();
			String ii = coverage.getImplInfo();
			Pair<String,String> p = new Pair<String,String>(mi, ii);
			if (!knownCombinations.contains(p)) {
				knownCombinations.add(p);
			}
			updateCombos(mi, ii);
		}

	}
	
	private void updateCombos(String mi, String ii) {
		modelCombo.removeSelectionListener(selListener);
		implCombo.removeSelectionListener(selListener);

		Vector<String> v = new Vector<String>();
		int sz;
		String[] ami;
		
		v.clear();
		for(Pair<String,String> pp: knownCombinations) {
			if ((ii.equals(stdItems[0]) || pp.getRight().equals(ii)) &&!v.contains(pp.getLeft()))
				v.add(pp.getLeft());
		}
		Collections.sort(v);
		sz = stdItems.length+v.size();
		ami = new String[sz];
		for(int i=0; i<stdItems.length; i++)
			ami[i] = stdItems[i];
		for(int i=0; i<v.size(); i++)
			ami[i+stdItems.length] = v.get(i);
		modelCombo.setItems (ami);
		if (mi.equals(app.getModelFilename()))
			modelCombo.select(1);
		else {
		    for(int i=0; i<ami.length; i++) {
			    if (ami[i].equals(mi))
				    modelCombo.select(i);
		    }
		}
		
		v.clear();
		for(Pair<String,String> pp: knownCombinations) {
			if ((mi.equals(stdItems[0]) || pp.getLeft().equals(mi)) &&!v.contains(pp.getRight()))
				v.add(pp.getRight());
		}
		Collections.sort(v);
		sz = stdItems.length+v.size();
		ami = new String[sz];
		for(int i=0; i<stdItems.length; i++)
			ami[i] = stdItems[i];
		for(int i=0; i<v.size(); i++)
			ami[i+stdItems.length] = v.get(i);
		implCombo.setItems (ami);
		if (ii.equals(app.getImplFilename()))
			implCombo.select(1);
		else {
		    for(int i=0; i<ami.length; i++) {
			    if (ami[i].equals(ii))
				    implCombo.select(i);
		    }
		}
	
		modelCombo.addSelectionListener(selListener);
		implCombo.addSelectionListener(selListener);
	}
	
	public void selectCoverage(String mi, String ii) {
		if (mi.equals(stdItems[1]))
			mi = app.getModelFilename();
		if (ii.equals(stdItems[1]))
			ii = app.getImplFilename();
		
		Pair<String,String> p = new Pair<String,String>(mi,ii);
		if (knownCombinations.contains(p)) {
			CoverageData c = CoverageFactory.getCoverageData(mi, ii);
			if (coverage != c) {
				System.out.println("trying to set coverage to ("+mi+") ("+ii+") : "+ c);
				setCoverage(c);
			} else {
				System.out.println("already have coverage ("+mi+") ("+ii+") : "+ c);
				updateCombos(mi, ii);
			}

		} else {
			System.out.println("NOT trying to set coverage to ("+mi+") ("+ii+")");
			updateCombos(mi, ii);
		}
	}
	
	public void reset() {
		table.removeAll();
	}
	
	public CoveragePane(Composite parent, Testgui g) {
		app = g;
		System.err.println("coveragepane");
		coverageGroup = new Group(parent, SWT.NONE);
		
		Group infoGroup = new Group(coverageGroup, SWT.NONE);
		GridLayout infoLayout = new GridLayout();
		infoLayout.numColumns = 2;
		infoGroup.setLayout(infoLayout);
		
		modelItems = stdItems;
		implItems = stdItems;
		selListener = new SelectionListener() { 
			public void widgetDefaultSelected(SelectionEvent arg0) {
				//updateCombos(modelCombo.getText(), implCombo.getText() );
				
			}

			public void widgetSelected(SelectionEvent arg0) {
				//updateCombos(modelCombo.getText(), implCombo.getText() );
				selectCoverage(modelCombo.getText(), implCombo.getText() );
				
			} 
		};
		
		modelLabel =  new org.eclipse.swt.widgets.Label(infoGroup, SWT.RIGHT);
		modelLabel.setText("Model:");
		modelLabel.pack();
		
		modelCombo = new Combo (infoGroup, SWT.READ_ONLY);
		modelCombo.setSize (200, 200);
		modelCombo.setItems (modelItems);
		GridData modelComboGridData = new GridData(GridData.VERTICAL_ALIGN_BEGINNING|GridData.FILL_HORIZONTAL);
		modelCombo.setLayoutData(modelComboGridData);
		//modelCombo.addListener(SWT.Selection, new Listener() { 
		//	public void handleEvent(Event e) {
		//		updateCombos(modelCombo.getText(), implCombo.getText() );
		//	} 
		//});

		
		implLabel =  new org.eclipse.swt.widgets.Label(infoGroup, SWT.RIGHT);
		implLabel.setText("Impl:");
		implLabel.pack();
		
		implCombo = new Combo (infoGroup, SWT.READ_ONLY);
		implCombo.setSize (200, 200);
		implCombo.setItems (implItems);
		GridData implComboGridData = new GridData(GridData.VERTICAL_ALIGN_BEGINNING|GridData.FILL_HORIZONTAL);
		implCombo.setLayoutData(implComboGridData);
		//implCombo.addListener(SWT.Selection, new Listener() { 
		//	public void handleEvent(Event e) {
		//		updateCombos(modelCombo.getText(), implCombo.getText() );
		//	} 
		//});
		
		modelCombo.addSelectionListener(selListener);
		implCombo.addSelectionListener(selListener);


		
		Group tableGroup = new Group(coverageGroup, SWT.NONE);
		table = new Table (tableGroup, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION | SWT.H_SCROLL | SWT.V_SCROLL);
		FillLayout fillHLayout = new FillLayout(SWT.HORIZONTAL);
		tableGroup.setLayout(fillHLayout);
		//table.setLayout(new GridLayout());

		table.setLinesVisible (true);
		table.setHeaderVisible (true);
		//GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		//data.heightHint = 200;
		//table.setLayoutData(data);
		for (int i=0; i<titles.length; i++) {
			TableColumn column = new TableColumn (table, SWT.NONE);
			column.setText (titles [i]);
		}	
		for (int i=0; i<titles.length; i++) {
			table.getColumn (i).pack ();
		}	
		
		table.pack();
		
		Group buttonGroup  = new Group(coverageGroup, SWT.NONE);
		//new Group(logGroup, SWT.NONE);
		RowLayout buttonLayout = new RowLayout();
		buttonLayout.wrap = false;
		buttonLayout.type = SWT.HORIZONTAL;
		buttonLayout.pack = true; 
		buttonGroup.setLayout(buttonLayout);
		
		resetCoverageButton = new Button(buttonGroup, SWT.PUSH);
		resetCoverageButton.setText("Reset");
		resetCoverageButton.pack();
		resetCoverageButton.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				reset();
				if (coverage != null)
					coverage.reset();
			} 
		}); 
		
		
		PaneContainer logPaneContainer = new PaneContainer(coverageGroup, SWT.VERTICAL);
		logPaneContainer.addFixed(infoGroup);
		logPaneContainer.addResizable(tableGroup);
		logPaneContainer.addFixed(buttonGroup);

		infoGroup.pack();
		tableGroup.pack();
		buttonGroup.pack();
		logPaneContainer.layout();
		coverageGroup.pack();
	}
	
	private Group coverageGroup = null;
	private Table table = null;
	private Testgui app;
	private String[] titles = {
			"run#   ", "#Nodes", "#Edges", "#QLoops", "#NQLoops", "#QTrans", "#NQTrans", 
			"run#   ", "#Nodes", "#Edges", "#QLoops", "#NQLoops", "#QTrans", "#NQTrans",
		};
	
	private CoverageData coverage = null;
	private Button resetCoverageButton;
	
	private org.eclipse.swt.widgets.Label modelLabel;
	private org.eclipse.swt.widgets.Label implLabel;
	
	private Combo modelCombo;
	private Combo implCombo;
	private String[] stdItems = new String[]{
				"",				// list all possibilities
				"from config",	// currently configured item (e.g. of current testrun)
			};
	private String[] modelItems;
	private String[] implItems;

	private Vector<Pair<String,String>> knownCombinations = new Vector<Pair<String,String>>();
	private SelectionListener selListener;


}
