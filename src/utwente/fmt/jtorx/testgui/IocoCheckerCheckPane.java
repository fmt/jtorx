package utwente.fmt.jtorx.testgui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;


public class IocoCheckerCheckPane implements JTorXPane {

	public void runSetup() {
		simGroup.setEnabled(false);
	}
	public void runBegins() {
		checkButton.setText("Stop");
		checkButton.setEnabled(true);
		simGroup.setEnabled(false);
		checkableRelations.setEnabled(false);
	}
	public void runEnds() {
		checkButton.setText("Check");
		checkButton.setEnabled(true);
		simGroup.setEnabled(true);
		checkableRelations.setEnabled(true);
	}

	public Group getGroup() {
		return checkGroup;
	}
	
	public void setImplViewItemsVisibility(boolean b) {
		checkButton.setEnabled(b); // nicer than making it entirely invisible
	}
	
	public void setSelectionItemsVisibility(boolean b) {
		simGroup.setVisible(b);
	}
	
	public boolean hasStopRequest() {
		return stopRequested;
	}
	
	public int autoStopCount() {
		String s = autoStopCount.getText().trim();
		if (s.equals(""))
			return 0;
		int count;
		
		try {
				count = Integer.parseInt(s);
		}
		catch(NumberFormatException nfe) {
			app.addStatusMsg("iocoChecker.maxcount is not a number: "+s);
			return 0;
		}
		if (count < 0) {
			app.addStatusMsg("iocoChecker.maxcount is not a non-negative number: "+s);
			return 0;
		}
		return count;
	}

	public IocoCheckerCheckPane(Composite parent, Testgui g) {
		app = g;

		// Run Pane
		checkGroup = new Group(parent, SWT.NONE);
		RowLayout checkLayout = new RowLayout();
		checkGroup.setLayout(checkLayout);
		checkLayout.wrap = false;
		checkLayout.type = SWT.HORIZONTAL;
		//checkGroup.setLayout(new FillLayout(SWT.HORIZONTAL));
//		checkGroup.setLayout(new FormLayout());
//		checkGroup.setText("Check run:");

		Group startGroup  = new Group(checkGroup, SWT.NONE);
		RowLayout startLayout = new RowLayout();
		startLayout.wrap = false;
		startLayout.type = SWT.HORIZONTAL;
		//startLayout.pack = true; 
		startGroup.setLayout(startLayout);

		checkButton = new Button(startGroup,SWT.PUSH);
		checkButton.setText("Check");
		checkButton.pack();
		
		autoStopLabel = new org.eclipse.swt.widgets.Label(startGroup, SWT.NONE);
		autoStopLabel.setText("#Results:");
		autoStopLabel.pack();
		autoStopCount = new Text(startGroup, SWT.SINGLE );
		autoStopCount.pack();
		
		autoStopCount.setText("5     ");
		
		checkableRelationsLabel = new org.eclipse.swt.widgets.Label(startGroup, SWT.NONE);
		checkableRelationsLabel.setText("Relation:");
		checkableRelationsLabel.pack();
		
		checkableRelations = new Combo(startGroup, SWT.READ_ONLY);
		checkableRelations.add(CheckableRelation.IOCO.toString());
		checkableRelations.add(CheckableRelation.REFINES.toString());
		checkableRelations.setText(CheckableRelation.getDefaultRelation().toString());
		checkableRelations.pack();
		startGroup.pack();

		
		simGroup = new Group(checkGroup, SWT.NONE);
		RowLayout simLayout = new RowLayout();
		simLayout.wrap = false;
		simLayout.type = SWT.HORIZONTAL;
		//simLayout.pack = true; 
		simGroup.setLayout(simLayout);

		org.eclipse.swt.widgets.Label simLabel = new org.eclipse.swt.widgets.Label(simGroup, SWT.NONE);
		simLabel.setText("Selected result: ");
		simLabel.pack();

		simModelButton = new Button(simGroup,SWT.PUSH);
		simModelButton.setText("Simulate in Model");
		simModelButton.pack();
		
		simSUTButton = new Button(simGroup,SWT.PUSH);
		simSUTButton.setText("Simulate in Impl");
		simSUTButton.pack();
		
		saveButton = new Button(simGroup,SWT.PUSH);
		saveButton.setText("Save");
		saveButton.pack();

		simGroup.pack();
		
		checkGroup.pack();
		
		checkButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				checkButton.setEnabled(false);
				if (checkButton.getText().equals("Check")) {
					stopRequested =  false;
					app.startIocoCheck(isCheckRefinesRequested());
				} else if (checkButton.getText().equals("Stop")) {
					stopRequested =  true;
				}
				
			}
		}); 
		
		simModelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				app.simulateSelectedIocoCheckerTraceInModel();
			}
		}); 

		simSUTButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				app.simulateSelectedIocoCheckerTraceInImplementation();
			}
		}); 
		
		saveButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				app.saveSelectedIocoCheckerTrace();
			}
		});

		setSelectionItemsVisibility(false);
	}
	
	private boolean isCheckRefinesRequested() {
	    return checkableRelations.getText().equals(CheckableRelation.REFINES.toString());
	}

	private enum CheckableRelation {
	    IOCO("(u)ioco"),
	    REFINES("refines");

	    private final String name;

	    private CheckableRelation(String name) {
	        this.name = name;
	    }

	    public String toString() {
	        return name;
	    }

	    public static CheckableRelation getDefaultRelation() {
	        return IOCO;
	    }
	}
	
	private Testgui app;
	private Group checkGroup = null;
	private Button checkButton = null;
	private org.eclipse.swt.widgets.Label autoStopLabel = null;
	private Text autoStopCount = null;
	private boolean stopRequested = false;
	private Group simGroup = null;
	private Button simModelButton = null;
	private Button simSUTButton = null;
	private Button saveButton = null;
	private Combo checkableRelations = null;
	private org.eclipse.swt.widgets.Label checkableRelationsLabel = null;
}
