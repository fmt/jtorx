package utwente.fmt.jtorx.testgui;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import utwente.fmt.jtorx.iocochecker.IocoCheckerFailure;
import utwente.fmt.jtorx.iocochecker.IocoCheckerResult;
import utwente.fmt.lts.LTS;

public class IocoCheckerFailuresPane  implements JTorXPane {

	public void runSetup() {
		failuresTable.removeAll();
		failures = null;
	}
	public void runBegins() {
		failuresTable.removeAll();
		failureArray = new IocoCheckerFailure[0];
		failures = null;
	}
	public void runEnds() {
	}

	public Group getGroup() {
		return failuresGroup;
	}

	public void reset() {
		failuresTable.removeAll();
	}
	public void showFailures(IocoCheckerResult r) {
		failures = r;
		
		if (r==null) {
			failuresGroup.setText("Check could not be done due to errors (see messages pane)");
			failuresTable.setVisible(false);
			app.setIocoCheckSelectionItemsVisibility(false);
			return;
		}
		
		String errorMessage = r.getErrorMessage();
		if (errorMessage!=null) {
			failuresGroup.setText("Check could not be done due to errors: "+errorMessage);
			failuresTable.setVisible(false);
			app.setIocoCheckSelectionItemsVisibility(false);
			return;
		}
		
		List<IocoCheckerFailure> ffl = r.getResults();
		if (ffl.size() == 0) {
			failuresGroup.setText("The implementation is "+r.getRelationName()+
					" conforming to the specification.");
			failuresTable.setVisible(false);
			app.setIocoCheckSelectionItemsVisibility(false);
		} else {
			failuresGroup.setText("Failures:");
			failuresTable.setVisible(true);
			app.setIocoCheckSelectionItemsVisibility(true);
		}

		if (false) {
		for(IocoCheckerFailure f: ffl) {
			String[] a = f.getTabelRowItems();
			TableItem item = new TableItem (failuresTable, SWT.NONE);
			for(int i=0; i<4; i++)
				item.setText(i, a[i]);
		}
		}
	}
	public void insertFailure(IocoCheckerFailure f) {
		failuresGroup.setText("Failures:");
		failuresTable.setVisible(true);
		app.setIocoCheckSelectionItemsVisibility(true);
		
		int idx = Arrays.binarySearch(failureArray, f, new Comparator<IocoCheckerFailure>() {
			public int compare(IocoCheckerFailure o1, IocoCheckerFailure o2) {
				int diff = o1.getTraceLength() - o2.getTraceLength();
				if (diff!=0)
					return diff;
				return o1.getTabelRowItems()[0].compareTo(o2.getTabelRowItems()[0]);
			}
		});
		if (idx < 0) {
			int insertidx = -idx - 1;
			
			IocoCheckerFailure[] tmp = new IocoCheckerFailure[failureArray.length+1];
			System.arraycopy(failureArray, 0, tmp, 0, insertidx);
			tmp[insertidx] = f;
			System.arraycopy(failureArray, insertidx, tmp, insertidx + 1, failureArray.length - insertidx);
			failureArray =  tmp;


			TableItem item = new TableItem (failuresTable, SWT.NONE, insertidx);
			String[] a = f.getTabelRowItems();
			for(int i=0; i<4; i++)
				item.setText(i, a[i]);
			}
	}
	
	public LTS getSelectedLTS() {
		if (failureArray.length==0) {
			app.addStatusMsg("no iocoChecker failure results");
			return null;
		}
		int idx = failuresTable.getSelectionIndex();
		if (idx<0) {
			app.addStatusMsg("no iocoChecker failures item selected (selidx="+idx+")");
			return null;
		}
		if (idx >= failureArray.length) {
			app.addStatusMsg("no such iocoChecker failures item (selidx="+idx+")");
			return null;
		}
		LTS result = failureArray[idx].getLTS();
		if (result==null)
			app.addStatusMsg("no LTS for iocoChecker failures selection (selidx="+idx+")");
		return result;
	}
	
	public LTS getCombinedLTS() {
		if (failures==null) {
			app.addStatusMsg("no iocoChecker failure results");
			return null;
		}
		return failures.getLTSCombinedResult();
	}
	
	public LTS getModelLTS() {
		if (failures==null)
			return null;
		return failures.getModelLTS();
	}

	public LTS getImplementationLTS() {
		if (failures==null)
			return null;
		return failures.getImplementationLTS();
	}

	public String getModelFileName() {
		if (failures==null)
			return null;
		return failures.getModelFilename();
	}

	public String getImplementationFileName() {
		if (failures==null)
			return null;
		return failures.getImplementationFilename();
	}


	public IocoCheckerFailuresPane(Composite parent, Testgui g) {
		app = g;

		failuresGroup = new Group(parent, SWT.NONE);
		failuresGroup.setLayout(new FillLayout(SWT.HORIZONTAL|SWT.VERTICAL));
		failuresGroup.setText("Failures:");

		failuresTable = new Table(failuresGroup, SWT.BORDER | SWT.FULL_SELECTION | SWT.VIRTUAL);
		failuresTable.setLayout(new FillLayout(SWT.HORIZONTAL|SWT.VERTICAL));
		failuresTable.setLinesVisible (true);
		failuresTable.setHeaderVisible (true);

		traceColumn = new TableColumn (failuresTable, SWT.NONE);
		traceColumn.setText("Suspension Trace");
		implColumn = new TableColumn (failuresTable, SWT.NONE);
		implColumn.setText("Implementation Actions");
		specColumn = new TableColumn (failuresTable, SWT.NONE);
		specColumn.setText("Specification Actions");
		unexpColumn = new TableColumn (failuresTable, SWT.NONE);
		unexpColumn.setText("Unexpected Actions");

		traceColumn.pack();
		implColumn.pack();
		specColumn.pack();

		failuresGroup.pack();
		
		failuresGroup.setText("Results:");
		failuresTable.setVisible(false);

		failuresGroup.addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				Rectangle area = failuresGroup.getClientArea();
				Point size = failuresTable.computeSize(SWT.DEFAULT, SWT.DEFAULT);
				ScrollBar vBar = failuresTable.getVerticalBar();
				int width = area.width - failuresTable.computeTrim(0,0,0,0).width; // - vBar.getSize().x;
				if (size.y > area.height + failuresTable.getHeaderHeight()) {
					// Subtract the scrollbar width from the total column width
					// if a vertical scrollbar will be required
					Point vBarSize = vBar.getSize();
					width -= vBarSize.x;
				}
				Point oldSize = failuresTable.getSize();
				if (oldSize.x > area.width) {
					// table is getting smaller so make the columns 
					// smaller first and then resize the table to
					// match the client area width
					traceColumn.setWidth(width/4);
					implColumn.setWidth((width - traceColumn.getWidth())/3);
					specColumn.setWidth((width - traceColumn.getWidth() - implColumn.getWidth())/2);
					unexpColumn.setWidth(width - traceColumn.getWidth() - implColumn.getWidth() - specColumn.getWidth());
					failuresTable.setSize(area.width, area.height);
				} else {
					// table is getting bigger so make the table 
					// bigger first and then make the columns wider
					// to match the client area width
					failuresTable.setSize(area.width, area.height);
					traceColumn.setWidth(width/4);
					implColumn.setWidth((width - traceColumn.getWidth())/3);
					specColumn.setWidth((width - traceColumn.getWidth() - implColumn.getWidth())/2);
					unexpColumn.setWidth(width - traceColumn.getWidth() - implColumn.getWidth() - specColumn.getWidth());
				}
			}
		});
	}

	private Testgui app;

	private Group failuresGroup = null;
	private Table failuresTable = null;
	private TableColumn traceColumn = null;
	private TableColumn implColumn = null;
	private TableColumn specColumn = null;
	private TableColumn unexpColumn = null;
	private IocoCheckerResult failures = null;
	private IocoCheckerFailure[] failureArray = new IocoCheckerFailure[0];
}

