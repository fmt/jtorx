package utwente.fmt.jtorx.testgui;

public interface JTorXPane {
	void runSetup();
	void runBegins();
	void runEnds();
}
