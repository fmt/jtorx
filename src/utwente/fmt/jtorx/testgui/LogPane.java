package utwente.fmt.jtorx.testgui;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;


public class LogPane implements JTorXPane {

	public void runSetup() {
	}	
	public void runBegins() {
	}
	public void runEnds() {
	}

	public Group getGroup() {
		return logGroup;
	}

	public LogPane(Composite parent, Testgui g, LogTextPane p) {
		app = g;
		logtextPane = p;

		// Run Pane
		logGroup = new Group(parent, SWT.NONE);
		// runGroup.setLayout(new FillLayout(SWT.HORIZONTAL));
//		logGroup.setLayout(new FormLayout());
//		logGroup.setText("Test Log:");

		Group buttonGroup  = logGroup;
			//new Group(logGroup, SWT.NONE);
		RowLayout buttonLayout = new RowLayout();
		buttonLayout.wrap = false;
		buttonLayout.type = SWT.HORIZONTAL;
		buttonLayout.pack = true; 
		buttonGroup.setLayout(buttonLayout);

		saveButton = new Button(buttonGroup,SWT.PUSH);
		saveButton.setText("Save");
		saveButton.pack();
		
		logGroup.pack();
		
		saveButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				String filename = app.saveLogDialog("Save log file as", "");
				saveLog(filename);
			}
		}); 

	}
	
	private void saveLog(String fname) {
		if (fname==null || fname.trim().equals(""))
			return;

		FileOutputStream fout;
		try {
			fout = new FileOutputStream (fname);
			BufferedWriter w = new BufferedWriter(new OutputStreamWriter(fout));
			logtextPane.write(w);
			w.flush();
			fout.close();
			app.addStatusMsg("written test run log to: "+fname);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			app.addStatusMsg("cannot write test run log: "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			app.addStatusMsg("cannot write test run log: "+e.getMessage());
			e.printStackTrace();
		}
	}

	
	private Testgui app;
	private Group logGroup = null;
	private Button saveButton = null;
	private LogTextPane logtextPane = null;
}
