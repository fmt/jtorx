package utwente.fmt.jtorx.testgui;

import java.io.IOException;
import java.io.Writer;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class LogTextPane implements JTorXPane, Appendable {
	
	public Group getGroup() {
		return logGroup;
	}
	
	public void runSetup() {
		logText.setText("");
		logLineEnd = new Vector<Integer>();
		logLineEnd.add(0);
	}
	public void runBegins() {
		logText.setText("");
		logLineEnd = new Vector<Integer>();
		logLineEnd.add(0);
	}
	public void runEnds() {
	}
	
	public void write(Writer w) throws IOException {
		int last = logText.getCharCount();
		int start = 0;
		int bufsiz = 4*1024;
		int end = start + bufsiz - 1;
		while(start < last) {
			if (end > last)
				end = last;
			w.write(logText.getText(start,end));
			start = end+1;
			end = start + bufsiz - 1;
		}
	}

	public void appendLine(String s) {
		logText.append(s+"\n");
		logLineEnd.add(logText.getCharCount()-1); // subtract \n
	}
	public void clear() {
		logText.setText("");
		logLineEnd = new Vector<Integer>();
		logLineEnd.add(0);
	}

	public void highLight(int n) {
		int start, end;
		
		if (n < 0 || n >= logLineEnd.size())
			return;
		if (n == 0) {
			start = 0;
			end = 0;
		} else {
			start = logLineEnd.elementAt(n-1)+1; // compensate for \n
			end = logLineEnd.elementAt(n);
		}
		logText.clearSelection();
		logText.setSelection(start, end);
		logText.showSelection();
		logText.update();
		app.getDisplay().update();
	}

	public LogTextPane(Composite parent, Testgui g, String t) {
		app = g;
		display = app.getDisplay();
		
		// Log Pane
		logGroup = new Group(parent, SWT.NONE);
		logGroup.setText("Log: "+t);
		FillLayout fillHLayout = new FillLayout(SWT.HORIZONTAL);
		logGroup.setLayout(fillHLayout);

		logText = new Text(logGroup, SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL); 
		logText.setEditable(false); 
		logText.setText("");


	}
	
	private Testgui app;
	private Group logGroup;
	
	private Text logText =  null;
	private Vector<Integer> logLineEnd = new Vector<Integer>();
	private Display display;
	
	
	// methods to make this an appendable
	public Appendable append(CharSequence csq) throws IOException {
		if (csq==null)
			csq = "null";
		doAppend(csq.toString());
		return this;
	}

	public Appendable append(char c) throws IOException {
		doAppend(""+c);
		return this;
	}

	public Appendable append(CharSequence csq, int start, int end) throws IOException {
		if (csq==null)
			csq = "null";
		if (start<0 || end<0 || start>end || end>csq.length())
			throw new IndexOutOfBoundsException();
		doAppend(csq.subSequence(start,end).toString());
		return this;
	}
	// System.err.println("error-reporter: "+s);
	
	private void doAppend(final String s) {
		if (display.getThread() == Thread.currentThread()) {
			logText.append(s);
		} else
			new Thread("Testgui-MyErrorReporter") {
				public void run() {
					display.syncExec (new Runnable () {
						public void run () {
							logText.append(s);
						}
					});
				}
			}.start();
	}

}
