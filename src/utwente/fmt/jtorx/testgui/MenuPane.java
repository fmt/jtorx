package utwente.fmt.jtorx.testgui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.eekboom.utils.Strings;

import utwente.fmt.lts.Label;

public class MenuPane implements JTorXPane {
	
	public Group getGroup() {
		return menuGroup;
	}
	
	public void runSetup() {
		clear();
		inputsList.setEnabled(false);
		menuInputsGroup.setEnabled(false);
		menuInputsGroup.setText("Possible Stimuli:");
		menuGroup.setText("Current tester state gives:");
	}
	public void runBegins() {
		inputsList.setEnabled(true);
		menuInputsGroup.setEnabled(true);
		menuInputsGroup.setText("Possible Stimuli:");
		menuGroup.setText("Current tester state gives:");
	}
	public void runEnds() {
		inputsList.setEnabled(false);
		menuInputsGroup.setEnabled(false);
		
	}
	public void prepareToShowFailure() {
		menuGroup.setText("Last executed test step (observation), above, is not predicted by model, as shown below:");
		inputsList.removeAll();
		selectedInput = -1;
		app.notifyMenuInputDeselected();
		menuInputsGroup.setText("  "); // show some spaces instead of empty string: otherwise group resizes (would have no title)
	}
	public void clear() {
		inputsList.removeAll();
		outputsList.clear();
		outputsTable.removeAll();
		outputsTable.setItemCount(0);
		inputsMap.clear();
		selectedInput = -1;
		app.notifyMenuInputDeselected();
	}
	public void addInput(String s, Label l) {
		inputsList.add(s);
		inputsMap.put(s, l);
	}
	
	// TODO split this routine in two:
	//  - one that is called for strings that we want to be sorted:
	//    they do not yet have to appear in the table because the
	//    table will be refilled when finalizeOutputs is called.
	//  - one that is called for strings that we want to be at the end
	//    of the table; these are inserted _after_ finalizeOutputs is called.
	public void addOutput(String l, String v) {
		outputsList.add(new String[] {l, v});
		// force update of table
		outputsTable.setItemCount(outputsList.size());
	}
	
	public Label getSelectedStimulus() {
		String[] sel = inputsList.getSelection() ;
		if (debug) System.err.println("getSelectedStimulus len="+sel.length);
		if (sel.length != 1) {
			// addStatusMsg("need one selected input menu item (not "+sel.length+")");
			return null;
		}
		Label l = inputsMap.get(sel[0]);
		if (l == null) {
			app.addStatusMsg("internal error: no label associated with selection");
		}
		return l;
	}

	public void finalizeInputs() {
		sort(inputsList);
	}
	public void finalizeOutputs() {
		// sort outputs
		Collections.sort(outputsList, new Comparator<String[]>() {
	          public int compare(String[] arg0, String[] arg1) {
	        	  int c1 = naturalOrder.compare(arg0[0], arg1[0]);
	        	  if (c1 != 0)
						return c1;
					else
						return naturalOrder.compare(arg0[1], arg1[1]);
	          }
	        });
	    // update data displayed in table	
		outputsTable.setItemCount(outputsList.size());
		outputsTable.clearAll();
	}

	private void sort(List l) {
		String[] items;
		items = l.getItems();
		java.util.Arrays.sort(items, naturalOrder);
		l.setItems(items);
	}

	public MenuPane(Composite parent, Testgui g) {
		app = g;
		
		// Menu Pane
		menuGroup = new Group(parent, SWT.NONE);
		menuGroup.setText("Current tester state gives:");
		//menuGroup.setLayout(fillHLayout);
		menuGroup.setLayout(new FormLayout());

		FillLayout fillHLayout = new FillLayout(SWT.HORIZONTAL);
		
		final Sash sashMenu = new Sash (menuGroup, SWT.VERTICAL);
		sashMenu.pack();

		
		menuInputsGroup = new Group(menuGroup, SWT.NONE);
		menuInputsGroup.setText("Possible Stimuli:");
		menuInputsGroup.setLayout(fillHLayout);
		inputsList = new List(menuInputsGroup, SWT.SINGLE | SWT.NONE | SWT.V_SCROLL);
		inputsList.setEnabled(false);
		menuInputsGroup.pack();

		menuOutputsGroup = new Composite(menuGroup, SWT.NONE);
		//menuOutputsGroup.setText("Expected Observations and associated Verdicts:");
		menuOutputsGroup.setLayout(fillHLayout);  
		//outputsList = new List(menuOutputsGroup, SWT.SINGLE | SWT.NONE | SWT.V_SCROLL);

		

		outputsTable = new Table(menuOutputsGroup, SWT.FULL_SELECTION | SWT.VIRTUAL);
		outputsTable.setLayout(new FillLayout(SWT.HORIZONTAL|SWT.VERTICAL));
		outputsTable.setLinesVisible (true);
		outputsTable.setHeaderVisible (true);

		labelColumn = new TableColumn (outputsTable, SWT.NONE);
		labelColumn.setText("Expected Observations:");
		verdictColumn = new TableColumn (outputsTable, SWT.NONE);
		verdictColumn.setText("Associated Verdicts:");

		labelColumn.pack();
		verdictColumn.pack();
		
		menuOutputsGroup.pack();

		outputsTable.addListener(SWT.SetData, new Listener() {
		      public void handleEvent(Event e) {
		        TableItem item = (TableItem) e.item;
		        int index = outputsTable.indexOf(item);
		        String[] datum = outputsList.get(index);
		        item.setText(datum);
		      }
		    });

		FormData formMenuInputs = new FormData(); 
		formMenuInputs.top = new FormAttachment(0, 0); 
		formMenuInputs.left = new FormAttachment(0, 0); 
		formMenuInputs.bottom = new FormAttachment(100, 0); 
		formMenuInputs.right = new FormAttachment(sashMenu, 0); 
		menuInputsGroup.setLayoutData(formMenuInputs); 

		final int menuPercent = 50;
		final int menuLimit = 0;
		final FormData formMenuSash = new FormData(); 
		formMenuSash.top = new FormAttachment(0, 0); 
		formMenuSash.left = new FormAttachment(menuPercent, 0); 
		formMenuSash.bottom = new FormAttachment(100, 0); 
		sashMenu.setLayoutData(formMenuSash); 
		sashMenu.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				Rectangle sashRect = sashMenu.getBounds ();
				Rectangle shellRect = menuGroup.getClientArea ();
				int right = shellRect.width - sashRect.width - menuLimit;
				e.x = Math.max (Math.min (e.x, right), menuLimit);
				if (e.x != sashRect.x)  {
					// formMenuSash.left = new FormAttachment (0, e.x-sashRect.width);
					formMenuSash.left = new FormAttachment (0, e.x);
					// seems that relative size does not work:
					//formMenuSash.left = new FormAttachment (e.x/shellRect.width, 0);
					menuGroup.layout ();
					app.getDisplay().update();
				}
			}
		});

		FormData formMenuOutputs = new FormData(); 
		formMenuOutputs.top = new FormAttachment(0, 0); 
		formMenuOutputs.left = new FormAttachment(sashMenu, 0); 
		formMenuOutputs.bottom = new FormAttachment(100, 0); 
		formMenuOutputs.right = new FormAttachment(100, 0); 
		menuOutputsGroup.setLayoutData(formMenuOutputs); 

		menuGroup.pack();

		inputsList.addMouseListener(new MouseListener() {
			private long lastClickTime = 0;
			private int doubleClickTime = app.getDisplay().getDoubleClickTime();
			
			public void mouseDoubleClick(MouseEvent e) { 
				app.addStatusMsg("");
				Label l = getSelectedStimulus();
				app.handleGivenStimulusEvent(l);
			}
			public void mouseDown(MouseEvent arg0) {
				long curTime = System.currentTimeMillis();
				if (debug) System.err.println("inputsList mousedown "+doubleClickTime+"?"+(curTime-lastClickTime)+" ("+selectedInput+") ("+inputsList.getSelectionIndex()+")");
			}
			// NOTE the following code needs changing if we would allow multiple selections
			public void mouseUp(MouseEvent arg0) {
				long curTime = System.currentTimeMillis();
				if (debug) System.err.println("inputsList mouseup "+doubleClickTime+"?"+(curTime-lastClickTime)+" ("+selectedInput+") ("+inputsList.getSelectionIndex()+")");
				int sel = inputsList.getSelectionIndex();
				if (curTime - lastClickTime > doubleClickTime && selectedInput!=-1 && selectedInput==sel) {
					inputsList.deselectAll();
					selectedInput = -1;
					app.notifyMenuInputDeselected();
				} else {
					selectedInput = sel;
					app.notifyMenuInputSelected();
				}
				lastClickTime = System.currentTimeMillis();
			}
		});
		
		menuOutputsGroup.addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				//if (true) return;
				
				Rectangle area = menuOutputsGroup.getClientArea();
				Point size = outputsTable.computeSize(SWT.DEFAULT, SWT.DEFAULT);
				ScrollBar vBar = outputsTable.getVerticalBar();
				int width = area.width - outputsTable.computeTrim(0,0,0,0).width; // - vBar.getSize().x;
				if (size.y > area.height + outputsTable.getHeaderHeight()) {
					// Subtract the scrollbar width from the total column width
					// if a vertical scrollbar will be required
					Point vBarSize = vBar.getSize();
					width -= vBarSize.x;
				}
				Point oldSize = outputsTable.getSize();
				if (oldSize.x > area.width) {
					// table is getting smaller so make the columns 
					// smaller first and then resize the table to
					// match the client area width
					verdictColumn.setWidth(width/3);
					labelColumn.setWidth((width - verdictColumn.getWidth()));
					outputsTable.setSize(area.width, area.height);
				} else {
					// table is getting bigger so make the table 
					// bigger first and then make the columns wider
					// to match the client area width
					outputsTable.setSize(area.width, area.height);
					verdictColumn.setWidth(width/3);
					labelColumn.setWidth((width - verdictColumn.getWidth()));
				}
			}
		});

	}
	
	private Testgui app;
	
	private Group menuGroup = null;
	private Group menuInputsGroup = null;
	private List inputsList = null;
	private ArrayList<String[]> outputsList = new ArrayList<String[]>();
	private int selectedInput = -1; // we use this to be able to deselect input; needs change if we allow multiple selections
	private HashMap<String,Label> inputsMap = new HashMap<String,Label>();
	
	private Composite menuOutputsGroup = null;
	private Table outputsTable = null;
	private TableColumn labelColumn = null;
	private TableColumn verdictColumn = null;
	
	private Comparator<String> naturalOrder = Strings.getNaturalComparator();

	
	private boolean debug = false;
}
