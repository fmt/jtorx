package utwente.fmt.jtorx.testgui;

import org.apache.commons.exec.OS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import com.agynamix.platform.frontend.gui.CarbonUIEnhancer;
import com.sme.ui.CocoaUIEnhancer;

public class Menubar implements JTorXPane {

	private void setEnabledFileItems(boolean b) {
		loadConfigItem.setEnabled(b);
	}
	public void setEnabledFontNormalItem(boolean b) {
		fontNormalItem.setEnabled(b);
	}

	
	public void runSetup() {
		setEnabledFileItems(false);
	}
	public void runBegins() {
		setEnabledFileItems(false);
	}
	public void runEnds() {
		setEnabledFileItems(true);
	}

	public void setImplBrowseItemsVisibility(boolean b) {
	}

	public Menubar(Testgui g, Shell shell) {
		app = g;

		Menu menu = new Menu(shell, SWT.BAR); 
		shell.setMenuBar(menu); 

		MenuItem file = new MenuItem(menu, SWT.CASCADE); 
		file.setText("File"); 
		Menu filemenu = new Menu(shell, SWT.DROP_DOWN); 
		file.setMenu(filemenu); 

		loadConfigItem = new MenuItem(filemenu, SWT.PUSH); 
		loadConfigItem.setText("Load Configuration..."); 
		loadConfigItem.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.loadConfigFileDialog();
			} 
		}); 
		//loadConfigItem.setEnabled(false);
		
		saveConfigItem = new MenuItem(filemenu, SWT.PUSH); 
		saveConfigItem.setText("Save Configuration..."); 
		saveConfigItem.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.saveConfigFileDialog();
			} 
		}); 
		
		replayLogItem = new MenuItem(filemenu, SWT.PUSH); 
		replayLogItem.setText("Load Log for re-play..."); 
		replayLogItem.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) {
				app.loadLogForReplayDialog();
			} 
		}); 

		
		MenuItem view = new MenuItem(menu, SWT.CASCADE); 
		view.setText("View"); 
		Menu viewmenu = new Menu(shell, SWT.DROP_DOWN); 
		view.setMenu(viewmenu); 
		
		fontLargerItem = new MenuItem(viewmenu, SWT.PUSH); 
		fontLargerItem.setText("Make Text Bigger"); 
		fontLargerItem.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) { 
				app.changeFontSize(+1);
			} 
		}); 
		fontNormalItem = new MenuItem(viewmenu, SWT.PUSH); 
		fontNormalItem.setText("Make Text Normal Size"); 
		fontNormalItem.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) { 
				app.resetFontSize();
			} 
		}); 
		fontNormalItem.setEnabled(false);
		fontSmallerItem = new MenuItem(viewmenu, SWT.PUSH); 
		fontSmallerItem.setText("Make Text Smaller"); 
		fontSmallerItem.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) { 
				app.changeFontSize(-1);
			} 
		}); 

		new MenuItem(viewmenu, SWT.SEPARATOR);
		restartVisualizersItem = new MenuItem(viewmenu, SWT.PUSH); 
		restartVisualizersItem.setText("Restart  Visualization Services"); 
		restartVisualizersItem.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event e) { 
				app.restartLogServices();
			} 
		});
		
		viewmenu.addMenuListener(new MenuListener() {
			public void menuHidden(MenuEvent arg0) {
			}
			public void menuShown(MenuEvent arg0) {
				if (app!=null && app.getLogServices()!=null)
					restartVisualizersItem.setEnabled(! app.getLogServices().allRunning());
			}
		});



		if (! OS.isFamilyMac()) {
			// for the mac the quit button need not be added,
			// and the about button will be added to the application menu
			new MenuItem(filemenu, SWT.SEPARATOR);

			MenuItem quitItem = new MenuItem(filemenu, SWT.PUSH); 
			quitItem.setText("Quit"); 
			quitItem.addListener(SWT.Selection, new Listener() { 
				public void handleEvent(Event e) { 
					app.done();
				} 
			}); 

			
			MenuItem help = new MenuItem(menu, SWT.CASCADE); 
			help.setText("Help"); 
			Menu helpmenu = new Menu(shell, SWT.DROP_DOWN); 
			help.setMenu(helpmenu); 

			MenuItem aboutItem = new MenuItem(helpmenu, SWT.PUSH); 
			aboutItem.setText("About"); 
			aboutItem.addListener(SWT.Selection, new Listener() { 
				public void handleEvent(Event e) { 
					app.runAbout();
				} 
			}); 

		} else {
			// TODO fix this code: we test twice for same condition.
			if (SWT.getPlatform().equals("carbon"))
				new CarbonUIEnhancer("About JTorX").hookApplicationMenu(app.getDisplay(),
					new Listener() { public void handleEvent(Event e) { /*overkill*/ app.done(); } },
					new Listener() { public void handleEvent(Event e) { app.runAbout(); } },
					(true ?
						null :
						new Listener() { public void handleEvent(Event e) { app.runPreferences(); } }
					)
					);
			else if (SWT.getPlatform().equals("cocoa"))
				new CocoaUIEnhancer("JTorX").hookApplicationMenu(app.getDisplay(),
						new Listener() { public void handleEvent(Event e) { /*overkill*/ app.done(); } },
						new Listener() { public void handleEvent(Event e) { app.runAbout(); } },
						(true ?
							null :
							new Listener() { public void handleEvent(Event e) { app.runPreferences(); } }
						)
						);
			else
				System.err.println("Unknown mac platform, not running any UIEnhancer: "+SWT.getPlatform());
		}
	}

	private MenuItem loadConfigItem = null;
	private MenuItem replayLogItem = null;
	private MenuItem saveConfigItem = null;
	private MenuItem fontLargerItem = null;
	private MenuItem fontNormalItem = null;
	private MenuItem fontSmallerItem = null;
	private MenuItem restartVisualizersItem = null;
	private Testgui app = null;
}
