package utwente.fmt.jtorx.testgui;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

public class MessagePane implements JTorXPane {
	
	
	public Group getGroup() {
		return statusGroup;
	}
	
	public void runSetup() {
	}
	public void runBegins() {
	}

	public void runEnds() {
	}

	public void addStatusMsg(String s) {
		if (! s.trim().equals("")) {
			if (statusMsgs.size() <= 0 || !statusMsgs.lastElement().equals(s))
				statusMsgs.add(s);
			statusBlank = false;
			showStatusMsg(statusMsgs.size()-1);
		} else {
			statusBlank = true;
			showStatusMsg(statusMsgs.size());
		}
	}

	private void showStatusMsg(int i) {
		if (i >= 0 && i < statusMsgs.size()) {
			status.setText(statusMsgs.elementAt(i));
			//status.pack(false);
			statusIndex = i;
			statusUp.setEnabled((i < statusMsgs.size()-1) || statusBlank);
			statusDown.setEnabled(i > 0);
		} else if (i == statusMsgs.size() && statusBlank){
			status.setText("");
			//status.pack(false);
			statusIndex = i;
			statusUp.setEnabled(false);
			statusDown.setEnabled(i > 0);
		}
		status.update();
	}


	public MessagePane(Composite parent, Testgui g) {
		//app = g;
		
		// StatusLine Pane
		statusGroup = new Group(parent, SWT.NONE);
		statusGroup.setLayout(new FormLayout());
		statusGroup.setText("Messages:");

		status = new Text(statusGroup, SWT.MULTI|SWT.WRAP|SWT.V_SCROLL|SWT.H_SCROLL ); 
		status.setEditable(false);

		Composite statusButtonGroup = new Composite(statusGroup, SWT.NONE);
		statusButtonGroup.setLayout(new FillLayout(SWT.VERTICAL));
		statusUp = new Button(statusButtonGroup,SWT.ARROW | SWT.UP);
		statusDown = new Button(statusButtonGroup,SWT.ARROW | SWT.DOWN);
		statusUp.pack();
		statusDown.pack();
		
		
		FormData formStatusButtonGroup = new FormData();
		formStatusButtonGroup.right = new FormAttachment(100); 
		formStatusButtonGroup.top = new FormAttachment(0); 
		formStatusButtonGroup.bottom = new FormAttachment(100); 
		statusButtonGroup.setLayoutData(formStatusButtonGroup);

		FormData formStatusText = new FormData();
		formStatusText.right = new FormAttachment(statusButtonGroup, -10); 
		formStatusText.left = new FormAttachment(0, 0);
		formStatusText.top = new FormAttachment(0);
		formStatusText.bottom = new FormAttachment(100);
		status.setLayoutData(formStatusText);

		statusButtonGroup.pack();

		statusGroup.pack();

		statusMsgs.add("-- start of messages --");
		addStatusMsg("");

		if (debug)
			System.out.println("status height: "+status.getSize().y);

	

		// code taken from: 
		//  http://www.java2s.com/Code/Java/SWT-JFace-Eclipse/Resizeatextcontrolshowabout10characters.htm
		int lines = 2;
		GC gc = new GC(status);
		FontMetrics fm = gc.getFontMetrics();
		int height = lines * fm.getHeight();
		gc.dispose();
		if (debug)
			System.out.println("status height: "+height);
		// text.computeSize(width, height).height();
		//formStatus.top = new FormAttachment(100,-height);

		statusUp.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) { 
				showStatusMsg(statusIndex+1);
			}  }); 
		statusDown.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) { 
				showStatusMsg(statusIndex-1);
			}  }); 

	}
	
	//private Testgui app = null;
	private Group statusGroup;
	
	// private org.eclipse.swt.widgets.Label  status = null;
	private Text  status = null;
	private Button statusUp;
	private Button statusDown;
	
	private Vector<String> statusMsgs = new Vector<String>();
	private int statusIndex = 0;
	private boolean statusBlank = true;
	private boolean debug = false;
}
