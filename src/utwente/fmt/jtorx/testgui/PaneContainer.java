package utwente.fmt.jtorx.testgui;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Composite;

public class PaneContainer {
	
	public class ControlData {
		public Control getControl() {
			return control;
		}
		public Point computeSize() {
			Point result;
			if (sashOrient==SWT.HORIZONTAL) {
				Point p = control.computeSize(SWT.DEFAULT, sizeHint);
				if (sizeHint==SWT.DEFAULT)
					result = p;
				else
					result = new Point(p.x, sizeHint);
			} else {
				Point p = control.computeSize(sizeHint,SWT.DEFAULT);
				if (sizeHint==SWT.DEFAULT)
					result = p;
				else
					result = new Point(sizeHint, p.y);
			}
			System.err.println("computeSize "+control+" sh="+sizeHint+" "+result);
			return result;
		}
		public ControlData(Control c, boolean f, int sizeHint) {
			control =  c;
			fixedSize = f;
			this.sizeHint = sizeHint;
			if (fixedSize) {
				c.pack(); // force computation of size
				if (sashOrient==SWT.HORIZONTAL)
					size = c.getBounds().height;
				else 
					size = c.getBounds().width;
			} else
				size = 0;
		}
		private Control control;
		private boolean fixedSize;
		private int size;
		private int maxBotFromContainerTop;
		private int minTopFromContainerBot;
		private int sizeHint;
	}
	public class SashData {
		public int i;
	}
	
	public class SashListener implements Listener {
		public void handleEvent (Event e) {
			Rectangle sashRect = sash.getBounds ();
			Rectangle parentRect = parent.getClientArea ();
			int bot = getBound(parentRect) - getBound(sashRect);
			int i = n-1; // our index in sashes
			ControlData preControlData = controls.get(i);
			ControlData postControlData = controls.get(i+1);
			int maxBot = preControlData.maxBotFromContainerTop;
			int minTop = postControlData.minTopFromContainerBot;
			if (debug)
				System.err.println("Sashlistener "+i+" "+maxBot+" "+minTop+" "+getPos(e)+" "+e.toString());
			setPos(e, Math.max(0, Math.min (getPos(e), bot)));
			if (debug)
				System.err.println("\tmax-min "+i+" "+maxBot+" "+minTop+" "+getPos(e));
			if (maxBot > 0)
				setPos(e, Math.min(getPos(e), maxBot));
			if (minTop > 0)
				setPos(e, Math.max(getPos(e), parentRect.height - minTop));
			if (debug)
				System.err.println("\ttop-bot "+i+" "+maxBot+" "+minTop+" "+getPos(e));
			if (getPos(e) != getPos(sashRect))  {
				FormData layoutData = (FormData) sash.getLayoutData();
				if (minTop > 0)
					setPreControl(layoutData, new FormAttachment (100, getPos(e)-getBound(parentRect)));
				else
					setPreControl(layoutData, new FormAttachment (0, getPos(e)));
				// NOTE we depend on the fact that we can use e.detail to store our
				// own sash identifier.
				if (e.detail==0)
					e.detail = n;
				if (e.detail>=n && n > 1) {
					Sash prevSash = sashes.get(i-1);
					if (preControlData.fixedSize) {
						int ey = getPos(e);
						setPos(e, ey - preControlData.size);
						//prevSash.moveBelow(sash);
						prevSash.notifyListeners(SWT.Selection, e);
						setPos(e, ey); // restore position, or else sashPathMenu jumps to sashTeststepMessages position
					} else if (getPos(e) < getPos(prevSash.getBounds())) {
						//prevSash.moveBelow(sash);
						prevSash.notifyListeners(SWT.Selection, e);		
					}
				}
				if (e.detail<=n && n < sashes.size()) {
					Sash postSash = sashes.get(i+1);
					if (postControlData.fixedSize) {
						int ey = getPos(e);
						setPos(e, ey + postControlData.size);
						//postSash.moveBelow(sash);
						postSash.notifyListeners(SWT.Selection, e);
						setPos(e, ey); // restore position, or else sashPathMenu jumps to sashTeststepMessages position
					} else if(getPos(e) > getPos(postSash.getBounds())) {
						//postSash.moveBelow(sash);
						postSash.notifyListeners(SWT.Selection, e);
					}
				}
				if (e.detail==n)
					parent.layout ();
			}
		}
		public SashListener(Sash s, int n) {
			this.n = n;
			sash = s;
		}
		private int n; // origin 1, range: [1, sashes.size()]
		private Sash sash;
	}
	
	public void addFixed(Control g) {
		add(g, true, SWT.DEFAULT);
	}
	public void addResizable(Control g) {
		add(g, false, SWT.DEFAULT);
	}
	public void addResizable(Control g, int sizeHint) {
		add(g, false, sizeHint);
	}
	public void print(String s) {
		int i = 0;
		for (ControlData c: controls) {
			System.err.println(s+""+i+" "+(c.fixedSize?"F":"R")+" "+c.size+" "+c.maxBotFromContainerTop+" "+c.minTopFromContainerBot);
			i++;
		}
	}
	public void layout() {
		if (debug)
			print("pre-layout: ");
		int parentAvail = getBound(parent.getClientArea());
		int i;
		i = 0;
		Sash s;
		ControlData cd;
	
		int y = 0;
		while (i < controls.size()-1) {
			s = sashes.get(i);
			cd = controls.get(i);
			if (debug)
				System.err.println("layout: "+i+" y="+y);
			y += getPos(cd.computeSize());
			y += margin;
			FormData sl = (FormData) s.getLayoutData();
			setPreControl(sl, new FormAttachment (0, y));
			y += getPos(s.computeSize(SWT.DEFAULT,SWT.DEFAULT));
			y += margin;
			i++;
		}

		i = controls.size() -1;
		y = 0;
		while (i > 0 && controls.get(i).fixedSize) {
			s = sashes.get(i-1);
			cd = controls.get(i);
			int soffset = parentAvail - getPos(s.getBounds());
			y += getPos(cd.computeSize()) +
				margin +
				getPos(s.computeSize(SWT.DEFAULT,SWT.DEFAULT));
			FormData sl = (FormData) s.getLayoutData();
			setPreControl(sl, new FormAttachment (100, -y));
			cd.minTopFromContainerBot = y;
			if (debug)
				System.err.println("\tlayout control("+i+") mintop "+y+" ("+soffset+")");
			i--;
		}
		i = 0;
		int prevMaxBot = 0;
		while (i<controls.size() && controls.get(i).fixedSize) {
			cd = controls.get(i);
			s = (i< sashes.size() ? sashes.get(i) : null);
			cd.maxBotFromContainerTop =
				prevMaxBot+
				getPos(cd.computeSize()) +
				(s==null? 0 : margin + getPos(s.computeSize(SWT.DEFAULT,SWT.DEFAULT)));
			prevMaxBot = cd.maxBotFromContainerTop;
			if (debug)
				System.err.println("\tlayout control("+i+") maxbot "+prevMaxBot);
			i++;
		}
		int j;
		for (i=0; i < sashes.size(); i++) {
			FormData cl = (FormData) controls.get(i).getControl().getLayoutData();
			setPostControl(cl, new FormAttachment (sashes.get(i), 0));
			for(j=0; j < controls.size(); j++)
				sashes.get(i).moveAbove(controls.get(j).getControl());

		}
		if (debug)
			print("post-layout: ");
	}
	
	public PaneContainer(Composite p, int o) {
		parent = p;
		
		//o = SWT.VERTICAL; // HACK: parts of the code assume vertical panes
		
		if (o==SWT.HORIZONTAL)
			sashOrient = SWT.VERTICAL;
		else
			sashOrient = SWT.HORIZONTAL;
		p.setLayout(new FormLayout());
	}
	
	private void setPreMargin(FormData d, FormAttachment a) {
		if (sashOrient==SWT.HORIZONTAL)
			d.left = a;
		else
			d.top = a;
	}
	private void setPostMargin(FormData d, FormAttachment a) {
		if (sashOrient==SWT.HORIZONTAL)
			d.right = a;
		else
			d.bottom = a;
	}
	private void setPreControl(FormData d, FormAttachment a) {
		if (sashOrient==SWT.HORIZONTAL)
			d.top = a;
		else
			d.left = a;
	}
	private void setPostControl(FormData d, FormAttachment a) {
		if (sashOrient==SWT.HORIZONTAL)
			d.bottom = a;
		else
			d.right = a;
	}
	private void setSize(FormData d, int sizeHint) {
		if (sashOrient==SWT.HORIZONTAL)
			d.height = sizeHint;
		else
			d.width = sizeHint;
	}

	
	private int getBound(Rectangle r) {
		if (sashOrient==SWT.HORIZONTAL)
			return r.height;
		else
			return r.width;
	}
	private int getPos(Point p) {
		if (sashOrient==SWT.HORIZONTAL)
			return p.y;
		else
			return p.x;
	}
	private int getPos(Rectangle p) {
		if (sashOrient==SWT.HORIZONTAL)
			return p.y;
		else
			return p.x;
	}
	private int getPos(Event p) {
		if (sashOrient==SWT.HORIZONTAL)
			return p.y;
		else
			return p.x;
	}
	private void setPos(Event p, int v) {
		if (sashOrient==SWT.HORIZONTAL)
			p.y = v;
		else
			p.x = v;
	}



	
	private void add(Control g, boolean f, int sizeHint) {
		System.err.println("add "+g+" sh="+sizeHint);
		Sash sash = null;
		if (controls.size() > 0) {
			sash = new Sash (parent, sashOrient);
			sashes.add(sash);
			int i = sashes.size();
			FormData layoutData = new FormData();
			sash.setLayoutData(layoutData);
			setPreMargin(layoutData,new FormAttachment(0, 0));
			setPostMargin(layoutData, new FormAttachment(100, 0)); 
			setPreControl(layoutData, new FormAttachment(controls.lastElement().getControl(), 0));
			sash.addListener (SWT.Selection, new SashListener (sash, i));
			layoutData = (FormData)controls.lastElement().getControl().getLayoutData();
			setPostControl(layoutData, null);
		}
		FormData layoutData = new FormData();
		g.setLayoutData(layoutData);
		controls.add(new ControlData(g, f, sizeHint));
		setPreMargin(layoutData, new FormAttachment(0, margin));
		setPostMargin(layoutData, new FormAttachment(100, -margin)); 
		if (sash==null)
			setPreControl(layoutData, new FormAttachment(0,margin));
		else
			setPreControl(layoutData, new FormAttachment(sash,0));
		setPostControl(layoutData, new FormAttachment(100));
		setSize(layoutData, sizeHint);
	}

	private Vector<ControlData> controls = new Vector<ControlData>();
	private Vector<Sash> sashes =  new Vector<Sash>();
	private Composite parent;
	private int sashOrient;
	private int margin = 5;
	
	private boolean debug = false;
}
