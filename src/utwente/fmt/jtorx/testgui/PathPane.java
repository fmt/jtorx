package utwente.fmt.jtorx.testgui;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class PathPane implements JTorXPane {
	
	public Group getGroup() {
		return pathGroup;
	}
	
	public void runSetup() {
		pathText.setText("");
		pathLineEnd = new Vector<Integer>();
		pathLineEnd.add(0);
	}
	public void runBegins() {
		pathText.setText("");
		pathLineEnd = new Vector<Integer>();
		pathLineEnd.add(0);
	}
	public void runEnds() {
	}
	public void appendLine(String s) {
		pathText.append(s+"\n");
		pathLineEnd.add(pathText.getCharCount()-1); // subtract \n
	}
	public void highLight(int n) {
		int start, end;
		
		if (n < 0 || n >= pathLineEnd.size())
			return;
		if (n == 0) {
			start = 0;
			end = 0;
		} else {
			start = pathLineEnd.elementAt(n-1)+1; // compensate for \n
			end = pathLineEnd.elementAt(n);
		}
		pathText.clearSelection();
		pathText.setSelection(start, end);
		pathText.showSelection();
		pathText.update();
		app.getDisplay().update();
	}

	public PathPane(Composite parent, Testgui g) {
		app = g;
		
		// Path Pane
		pathGroup = new Group(parent, SWT.NONE);
		pathGroup.setText("Executed test steps:");
		FillLayout fillHLayout = new FillLayout(SWT.HORIZONTAL);
		pathGroup.setLayout(fillHLayout);

		pathText = new Text(pathGroup, SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL); 
		pathText.setEditable(false); 
		pathText.setText("\n\n");


	}
	
	private Testgui app;
	private Group pathGroup;
	
	private Text pathText =  null;
	private Vector<Integer> pathLineEnd = new Vector<Integer>();


}
