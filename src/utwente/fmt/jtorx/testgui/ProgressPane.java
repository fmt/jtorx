package utwente.fmt.jtorx.testgui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ProgressBar;

import utwente.fmt.jtorx.ui.ProgressReporter;

// see: http://www.roseindia.net/tutorials/swt/progress-bar.shtml

public class ProgressPane implements JTorXPane {
	
	public Composite getGroup() {
		return progressGroup;
	}
	
	public void runSetup() {
	}
	public void runBegins() {
	}

	public void runEnds() {
	}

	public ProgressReporterBar getProgressReporterBar() {
		return progress;
	}

	public ProgressReporterBar getStateSpaceProgressReporterBar() {
		return stateSpaceProgress;
	}


	public class ProgressReporterBar implements ProgressReporter {
		public void ready() {
			if (noUpdates) return;
			
			new Thread("progress-reporter ready") {
				public void run() {
				app.getDisplay().asyncExec (new Runnable () {
					public void run () {
						detProgressBar.setSelection(detProgressBar.getMaximum());
						cur = max;
						counts.setText(cur+" of "+max);
						//counts.update();
						//app.getDisplay().update();
					}
				});
			}
			}.start();
		}
		public void stop() {
			if (noUpdates) return;
			
			new Thread("progress-reporter stop") {
				public void run() {
				app.getDisplay().asyncExec (new Runnable () {
					public void run () {
						detProgressBar.setState(SWT.PAUSED);
						counts.setText(cur+" of "+max);
						//counts.update();
						//app.getDisplay().update();
					}
				});
			}
			}.start();
		}
		
		public void reset() {
			if (noUpdates) return;
			
			new Thread("progress-reporter reset") {
				public void run() {
				app.getDisplay().asyncExec (new Runnable () {
					public void run () {
						detProgressBar.setSelection(0);
						detProgressBar.setMaximum(0);
						//counts.setText("        ");
						cur = 0;
						max = 0;
						//((FormData)counts.getLayoutData()).left = new FormAttachment(detProgressBar, 10);
						//((FormData)detProgressBar.getLayoutData()).right = null;
						//counts.update();
						//counts.pack();
						//progressGroup.pack();
						//app.getDisplay().update();
					}
				});
			}
			}.start();
		}
		public void setActivity(final String s) {
			if (noUpdates) return;
			
			new Thread("progress-reporter setActivity") {
				public void run() {
				app.getDisplay().asyncExec (new Runnable () {
					public void run () {
						activity.setText(s);
						//activity.update();

						if (s.trim().equals("")) {
							//counts.setText("");
							//counts.update();
							//counts.pack();
							//((FormData)counts.getLayoutData()).left = new FormAttachment(activity, 0);
							//((FormData)detProgressBar.getLayoutData()).right = ((FormData)detProgressBar.getLayoutData()).left;
						} else {
							//((FormData)counts.getLayoutData()).left = new FormAttachment(detProgressBar, 10);
							//((FormData)detProgressBar.getLayoutData()).right = null;
							//counts.update();
							//counts.pack();
						}
						if (longestActivity.length() < s.length()) {
							longestActivity = s;
							activity.pack();
							progressGroup.pack();
						}
						//app.getDisplay().update();
					}
				});
			}
			}.start();
		}

		public void setLevel(final int l) {
			if (noUpdates) return;
			
			new Thread("progress-reporter setLevel") {
				public void run() {
				app.getDisplay().asyncExec (new Runnable () {
					public void run () {
						detProgressBar.setSelection(l);
						cur = l;
						counts.setText(cur+" of "+max);
						//counts.update();
						//counts.pack();
						//app.getDisplay().update();
					}
				});
			}
			}.start();
		}
		public void setMax(final int m) {
			if (noUpdates) return;
			
			new Thread("progress-reporter setmax") {
				public void run() {
				app.getDisplay().asyncExec (new Runnable () {
					public void run () {
						detProgressBar.setMaximum(m);
						max = m;
						counts.setText(cur+" of "+max);
						//counts.update();
						//app.getDisplay().update();
					}
				});
			}
			}.start();
		}
		public ProgressReporterBar(ProgressBar b, ProgressBar i, org.eclipse.swt.widgets.Label a, org.eclipse.swt.widgets.Label c) {
			detProgressBar = b;
			indetProgressBar = i;
			activity = a;
			counts = c;
		}
		public void startUndeterminate() {
			if (noUpdates) return;
			
			if (indetProgressBar==null)
				return;

			if (app.getDisplay().getThread().equals(Thread.currentThread())) {
				indetProgressBar.moveAbove(detProgressBar);
				app.getDisplay().update();
			} else
				app.getDisplay().asyncExec (new Runnable () {
				public void run () {
					indetProgressBar.moveAbove(detProgressBar);
					app.getDisplay().update();
				}
			});
		}
		public void stopUndeterminate() {
			if (noUpdates) return;
			
			if (indetProgressBar==null)
					return;
			if (app.getDisplay().getThread().equals(Thread.currentThread())) {
				detProgressBar.moveAbove(indetProgressBar);
				app.getDisplay().update();
			} else

				app.getDisplay().asyncExec (new Runnable () {
				public void run () {
					detProgressBar.moveAbove(indetProgressBar);
					app.getDisplay().update();
				}
			});
		}

		private ProgressBar detProgressBar, indetProgressBar;
		private int cur = 0;
		private int max = 0;
		private org.eclipse.swt.widgets.Label activity;
		String longestActivity = "";
		private org.eclipse.swt.widgets.Label counts;
	}


	public ProgressPane(Composite parent, Testgui g) {
		app = g;
		
		progressGroup = new Composite(parent, SWT.NONE);
		progressGroup.setLayout(new FormLayout());

		activityLabel = new org.eclipse.swt.widgets.Label(progressGroup, SWT.NONE);
		activityLabel.setText("");
		activityLabel.pack();
		detProgressBar = new ProgressBar(progressGroup, SWT.SMOOTH);
		indetProgressBar = new ProgressBar(progressGroup, SWT.INDETERMINATE);
		progressLabel = new org.eclipse.swt.widgets.Label(progressGroup, SWT.NONE);
		progressLabel.setText("0 of 0");
		progressLabel.pack();
		stateSpaceActivityLabel = new org.eclipse.swt.widgets.Label(progressGroup, SWT.NONE);
		stateSpaceActivityLabel.setText("");
		stateSpaceActivityLabel.pack();
		stateSpaceProgressBar = new ProgressBar(progressGroup, SWT.SMOOTH);
		stateSpaceProgressLabel = new org.eclipse.swt.widgets.Label(progressGroup, SWT.NONE);
		stateSpaceProgressLabel.setText("0 of 0");
		stateSpaceProgressLabel.pack();

		FormData formActivityLabel = new FormData();
		formActivityLabel.left = new FormAttachment(0); 
		formActivityLabel.top = new FormAttachment(0);  // better without this?
		formActivityLabel.bottom = new FormAttachment(100);
		activityLabel.setLayoutData(formActivityLabel);

		FormData formProgressBar = new FormData();
		formProgressBar.left = new FormAttachment(activityLabel, 10);
		formProgressBar.top = new FormAttachment(0);
		formProgressBar.bottom = new FormAttachment(100);
		detProgressBar.setLayoutData(formProgressBar);
		indetProgressBar.setLayoutData(formProgressBar);
		detProgressBar.moveAbove(indetProgressBar);
		
		FormData formProgressLabel = new FormData();
		formProgressLabel.left = new FormAttachment(detProgressBar, 10);
		formProgressLabel.top = new FormAttachment(0);  // better without this?
		formProgressLabel.bottom = new FormAttachment(100);
		progressLabel.setLayoutData(formProgressLabel);
		
		FormData formStateSpaceActivityLabel = new FormData();
		formStateSpaceActivityLabel.left = new FormAttachment(progressLabel, 30); 
		formStateSpaceActivityLabel.top = new FormAttachment(0);
		formStateSpaceActivityLabel.bottom = new FormAttachment(100);
		stateSpaceActivityLabel.setLayoutData(formStateSpaceActivityLabel);
		
		FormData formStateSpaceProgressBar = new FormData();
		formStateSpaceProgressBar.left = new FormAttachment(stateSpaceActivityLabel, 10); 
		formStateSpaceProgressBar.top = new FormAttachment(0);
		formStateSpaceProgressBar.bottom = new FormAttachment(100);
		stateSpaceProgressBar.setLayoutData(formStateSpaceProgressBar);
		
		FormData formStateSpaceProgressLabel = new FormData();
		formStateSpaceProgressLabel.left = new FormAttachment(stateSpaceProgressBar, 10);
		formStateSpaceProgressLabel.right = new FormAttachment(100); 
		formStateSpaceProgressLabel.top = new FormAttachment(0); // better without this?
		formStateSpaceProgressLabel.bottom = new FormAttachment(100);
		stateSpaceProgressLabel.setLayoutData(formStateSpaceProgressLabel);
		
		progress = new ProgressReporterBar(detProgressBar, indetProgressBar, activityLabel, progressLabel);
		stateSpaceProgress = new ProgressReporterBar(stateSpaceProgressBar, null, stateSpaceActivityLabel, stateSpaceProgressLabel);

		
//		progress.setActivity(" ");
//		stateSpaceProgress.setActivity(" ");
		
//		progressGroup.pack();
	}
	
	private Testgui app = null;
	private Composite progressGroup;
	
	private ProgressBar detProgressBar;
	private ProgressBar indetProgressBar;
	private ProgressBar stateSpaceProgressBar;
	private ProgressReporterBar progress = null;
	private ProgressReporterBar stateSpaceProgress = null;
	private org.eclipse.swt.widgets.Label activityLabel = null;
	private org.eclipse.swt.widgets.Label progressLabel = null;
	private org.eclipse.swt.widgets.Label stateSpaceActivityLabel = null;
	private org.eclipse.swt.widgets.Label stateSpaceProgressLabel = null;

	private boolean noUpdates = true;
}
