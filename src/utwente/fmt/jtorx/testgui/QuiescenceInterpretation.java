package utwente.fmt.jtorx.testgui;


import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;

public class QuiescenceInterpretation implements IOLTSInterpretation {
	
	public class QuiescenceConstraint implements LabelConstraint {
		public Boolean isSatisfiedBy(Label l) {
			return givenIsOutput.isSatisfiedBy(l) ||
				l.getString().trim().equalsIgnoreCase("delta") ||
				l.getString().trim().equalsIgnoreCase("epsilon");
		}
		public QuiescenceConstraint(LabelConstraint lc) {
			givenIsOutput = lc;
		}
		private LabelConstraint givenIsOutput;
	}
	public LabelConstraint isInput() {
		return givenInterp.isInput();
	}
	public LabelConstraint isOutput() {
		return isOutput;
	}
	public LabelConstraint isInternal() {
		return givenInterp.isInternal();
	}
	public LabelConstraint isAny() {
		return givenInterp.isAny();
	}	
	public QuiescenceInterpretation(IOLTSInterpretation i) {
		givenInterp = i;
		isOutput = new QuiescenceConstraint(i.isOutput());
	}
	private IOLTSInterpretation givenInterp;
	private LabelConstraint isOutput;
}
