package utwente.fmt.jtorx.testgui;

public class RollCall {
	public void check(boolean b, String s) {
		if (b) {
			if (!missing.equals(""))
				missing += " and";
			missing += " "+s;
		}
	}
	public void reset() {
		missing = "";
	}
	public String getMissing() {
		return missing;
	}

	private String missing = "";
}
