package utwente.fmt.jtorx.testgui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.exec.OS;
import org.xml.sax.SAXException;

import utwente.fmt.jtorx.logger.anidot.Anidot;
import utwente.fmt.jtorx.logger.anidot.DotLTS;
import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.torx.Instantiator;
import utwente.fmt.jtorx.torx.explorer.aut.AutLTS;
import utwente.fmt.jtorx.torx.explorer.graphml.GraphMLLTS;
import utwente.fmt.jtorx.torx.explorer.gv.GvLTS;
import utwente.fmt.jtorx.torx.explorer.log.LogLTS;
import utwente.fmt.jtorx.torx.explorer.torx.TorxExplorer;
import utwente.fmt.jtorx.torx.instantiator.InstantiatorImpl;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.LTS;

import com.alexmerz.graphviz.ParseException;

public class RunItemData {
	
	// LTSkind is used to indicate the source of the LTS;
	// actually, the source implies several ilities, should we make these explicit?
	//  - for guide: already contains epsilon, or epsilon should be synthesized?
	//        we assume: MODEL already contains epsilon, TRACE does not
	//  - for model, impl: already contains delta, or delta should be synthesized?
	//                     (models used for guide typically already contain delta)
	//        we assume: MODEL does not yet contain delta, TRACE does contain delta

	public enum LTSKind {
		MODEL,
		TRACE,
		TORXEXPLORER,
		IOCOCHECKERSINGLETRACE,
		IOCOCHECKERMULTITRACE,
	}

	// LTSkind is used to indicate the source of the LTS
	public enum AdapterKind {
		LOGADAPTER,
		SIMADAPTER,
		IOCOSIMADAPTER,
		TORXADAPTER,
		STDINOUTADAPTER,
		PERFMEASURINGSTDINOUTADAPTER,
		TCPCLIENTLABELADAPTER,
		TCPSERVERLABELADAPTER,
	}

	public Anidot getAnidot() {
		return anidot;
	}
	public void setFilename(String m, String i) {
		modelFilename = m;
		instFilename = i;
	}
	public String getModelFilename() {
		return modelFilename;
	}
	public String getInstFilename() {
		return modelFilename;
	}
	public void setLTS(LTS l) {
		lts = l;
	}
	public LTS getLTS() {
		return lts;
	}
	public void setLTS(CompoundLTS l) {
		clts = l;
		lts = l;
	}
	public CompoundLTS getCompoundLTS() {
		return clts;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String s) {
		title = s;
	}
	
	public void setEnvVars(String[] s) {
		envVars = s;
	}


	public void setDotLTS(DotLTS l) {
		dotlts = l;
	}
	public void setAnidot(Anidot l) {
		anidot = l;
	}
	public LTSKind getLTSKind() {
		return ltsKind;
	}
	public void setLTSKind(LTSKind k) {
		ltsKind = k;
	}
	public AdapterKind getAdapterKind() {
		return adapterKind;
	}
	public void setAdapterKind(AdapterKind k) {
		adapterKind = k;
	}
	
	// does it already contains delta, or must delta be synthesized?
	public boolean containsDelta() {
		return (ltsKind==LTSKind.TRACE);
	}
	// does it already contains epsilon, or must epsilon be synthesized?
	public boolean containsEpsilon() {
		return (ltsKind==LTSKind.MODEL || ltsKind==LTSKind.IOCOCHECKERMULTITRACE);
	}

	
	public void clear() {
		lts = null;
		clts = null;
		dotlts = null;
		anidot = null;
		inst = null;
	}
	public void endPreviousIfDifferent(String m, String i) {
		clear();
		// crude hack to make sure that we always get a new model
		// because we really need to do so when we use a .tx (torx explorer)
		/*
		if (filename != null && !filename.equals(s)) {
			if (anidot != null)
				anidot.end();
			clear();
		}
		*/
	}
	
	public boolean startInstantiatorIfNeeded(ErrorReporter err, ProgressReporter progress) {
		if (instFilename!=null && instFilename.equalsIgnoreCase("")) {
			//err.report("no instantiator to start");
			inst = null;
			return true;
		}
		inst = new InstantiatorImpl(err, instFilename, null);
		if (!inst.start()) {
			err.report("failed to start instantiator");
			inst =  null;
			return false;
		}
		err.report("started instantiator");
		return true;
	}

	public boolean readLTSFileIfNeeded(ErrorReporter err, ProgressReporter progress) {
		if (lts == null) {
			lts = readModelFile(modelFilename, err, progress);
			if (lts == null)
				return false;
		}
		return true;
	}
	
	public String getGraphVizFilename(ErrorReporter err) {
		return getGraphVizFilename(modelFilename, err);
	}
	public String getGraphVizFilename(String fn, ErrorReporter err) {
		if (fn==null)
			return null;
		
		File gv_file = new File(fn+".gv");
		String gv_filename = null;

		try {
			gv_filename = gv_file.getCanonicalPath();
		} catch (IOException e) {
			err.report("caught exception when constructing .gv path: "+e.getMessage());
		}
		
		if (gv_filename!=null && gv_file.canRead()) {
			System.err.println("gv filename readable: "+gv_filename);
			return gv_filename;
		}
		
		gv_file = createTempGraphVizFile(fn, err, new NullProgressReporter());
		if (gv_file!=null) {
			try {
				gv_filename = gv_file.getCanonicalPath();
			} catch (IOException e) {
				err.report("caught exception when constructing .gv path: "+e.getMessage());
			}
			
			if (gv_filename!=null && gv_file.canRead()) {
				System.err.println("gv filename readable: "+gv_filename);
				return gv_filename;
			}
		}
		
		
		System.err.println("gv filename not readable: "+gv_filename);
		
		return null;

	}
	
	private File invokeGraphvizCreationCommand(String fn, File f, String cmd[], ErrorReporter err, ProgressReporter progress) {
		try {
			String n = f.getName();
			n = n.substring(0, n.lastIndexOf('.'));
			File temp = File.createTempFile(n+"-jtorx",".gv");
			System.err.println(" tmp gv file: "+temp.getName());
			temp.deleteOnExit();
			Process proc = Runtime.getRuntime().exec(cmd);
			OutputStream o = new FileOutputStream(temp);
			InputStream i = proc.getInputStream ();
			copy(i, o);
			return temp;
		} catch (IOException e) {
			String msg = "cannot create graphviz file from "+fn;
			if (e.getMessage() != null)
				msg += ": "+e.getMessage();
			err.report(msg);
			return null;
		}
	}
	
	private File createTempGraphVizFile(String fileName, ErrorReporter err, ProgressReporter progress) {
		System.err.println("trying to create tmp gv file for "+fileName);
		if (fileName.endsWith(".jrrc")) {		
			File explorerFile = new File(fileName);
			String cmd[] = new String[6];
			cmd[0] = getProg("jararaca", ".exe");
			cmd[1] = "-s";
			cmd[2] = "-e";
			cmd[3] = "-L";
			cmd[4] = "-d";
			cmd[5] = explorerFile.getAbsoluteFile().getPath();
			return invokeGraphvizCreationCommand(fileName, explorerFile, cmd, err, progress);
		} else if (fileName.endsWith(".sax")) {
			File explorerFile = new File(fileName);
			String cmd[] = new String[] {
			"java",
			"-classpath",
			getProg("lib/sigar-bin/lib/sigar.jar", "") + ":" + getProg("SymToSim.jar",""),
			"symtosim.Main",
			"-a",
			"-x",
			explorerFile.getAbsoluteFile().getPath(),
			"-d"};
			return invokeGraphvizCreationCommand(fileName, explorerFile, cmd, err, progress);
		} else if (fileName.endsWith(".tp")) {
			File explorerFile = new File(fileName);
			String cmd[] = new String[7];
			cmd[0] = getProg("jararaca", ".exe");
			cmd[1] = "-s";
			cmd[2] = "-e";
			cmd[3] = "-l";
			cmd[4] = "EPSILON";
			cmd[5] = "-d";
			cmd[6] = explorerFile.getAbsoluteFile().getPath();
			return invokeGraphvizCreationCommand(fileName, explorerFile, cmd, err, progress);
		}
		return null;
	}


	public LTS readModelFile(String fileName, ErrorReporter err, ProgressReporter progress) {
		//System.err.println("readModelFilename: "+fileName);
			
		if (fileName.trim().equals(""))
			return null;
		else if (fileName.endsWith(".aut")) {
			try {
				return new AutLTS(fileName);
			} catch (IOException e1) {
				String msg = "cannot read "+fileName;
				if (e1.getMessage() != null)
					msg += ": "+e1.getMessage();
				err.report(msg);
				return null;
			}
		} else if (fileName.endsWith(".graphml")) {
			try {
				return new GraphMLLTS(fileName, err);
			} catch (IOException e1) {
				String msg = "cannot read "+fileName;
				if (e1.getMessage() != null)
					msg += ": "+e1.getMessage();
				err.report(msg);
				return null;
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		} else if (fileName.endsWith(".gv") || fileName.endsWith(".dot")) {
			try {
				return new GvLTS(fileName);
			} catch (IOException e1) {
				String msg = "cannot read "+fileName;
				if (e1.getMessage() != null)
					msg += ": "+e1.getMessage();
				err.report(msg);
				return null;
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		} else if (fileName.endsWith(".jrrc")) {
			try {
				File explorerFile = new File(fileName);
				
				String cmd[] = new String[5];
				cmd[0] = getProg("jararaca", ".exe");
				cmd[1] = "-s";
				cmd[2] = "-e";
				cmd[3] = "-L";
				cmd[4] = explorerFile.getAbsoluteFile().getPath();
				String dir = explorerFile.getAbsoluteFile().getParent();
				return new TorxExplorer(err, cmd, dir, envVars, progress, inst);
			} catch (IOException e1) {
				String msg = "cannot read "+fileName;
				if (e1.getMessage() != null)
					msg += ": "+e1.getMessage();
				err.report(msg);
				return null;
			}
		} else if (fileName.endsWith(".log")) {
			try {
				return new LogLTS(fileName);
			} catch (IOException e1) {
				String msg = "cannot read "+fileName;
				if (e1.getMessage() != null)
					msg += ": "+e1.getMessage();
				err.report(msg);
				return null;
			}
		} else if (fileName.endsWith(".sax")) {
			try {
				File explorerFile = new File(fileName);
				
				boolean enableTauClosureOptimization = Boolean.getBoolean("stsimulator.enableTauClosureOptimization");
				
				int argsLen = 7 + (useLazyOTF ? 1 : 0) + (enableTauClosureOptimization ? 1 : 0);
				String cmd[] = new String[argsLen];
				cmd[0] = "java";
				cmd[1] = "-classpath";
				cmd[2] = getProg("lib/sigar-bin/lib/sigar.jar","") + ":" + getProg("SymToSim.jar","");
				cmd[3] = "symtosim.Main";
				cmd[4] = "-a";
				cmd[5] = "-x";
				cmd[6] = explorerFile.getAbsoluteFile().getPath();
				if(useLazyOTF) {
					cmd[7] = "-l";
				}
				if(enableTauClosureOptimization) {
					cmd[8] = "-t";
				}
				//String dir = explorerFile.getAbsoluteFile().getParent();
				String dir = System.getProperty("user.dir");
				System.err.println("about to start explorer:");
				for (String w: cmd) {
					System.err.println("\targ: "+w);
				}
				System.err.println("end of explorer args");
				return new TorxExplorer(err, cmd, dir, envVars, progress, inst);
			} catch (IOException e1) {
				String msg = "cannot read "+fileName;
				if (e1.getMessage() != null)
					msg += ": "+e1.getMessage();
				err.report(msg);
				return null;
			}
		} else if (fileName.endsWith(".tp")) {
			try {
				File explorerFile = new File(fileName);
				
				String cmd[] = new String[6];
				cmd[0] = getProg("jararaca", ".exe");
				cmd[1] = "-s";
				cmd[2] = "-e";
				cmd[3] = "-l";
				cmd[4] = "EPSILON";
				cmd[5] = explorerFile.getAbsoluteFile().getPath();
				String dir = explorerFile.getAbsoluteFile().getParent();
				return new TorxExplorer(err, cmd, dir, envVars, progress, inst);
			} catch (IOException e1) {
				String msg = "cannot read "+fileName;
				if (e1.getMessage() != null)
					msg += ": "+e1.getMessage();
				err.report(msg);
				return null;
			}
		} else if (fileName.endsWith(".tx") ||
				   fileName.endsWith(".tx.bat") ||
				   fileName.endsWith(".tx.exe")) {
			try {
				File explorerFile = new File(fileName);
				
				String cmd[] = new String[1];
				cmd[0] = explorerFile.getAbsoluteFile().getPath();
				String dir = explorerFile.getAbsoluteFile().getParent();
				return new TorxExplorer(err, cmd, dir, envVars, progress, inst);
			} catch (IOException e1) {
				String msg = "cannot read "+fileName;
				if (e1.getMessage() != null)
					msg += ": "+e1.getMessage();
				err.report(msg);
				return null;
			}

		} else {
			err.report("unknown file name suffix, so don't know what to do: "+fileName);
			return null;
		}
	}
	
	
	// copied from AniServer: should be moved to utils
	private File getRunDir() {
		URL u = this.getClass().getProtectionDomain().getCodeSource().getLocation();
		//System.err.println("dir: "+u);

		File f = null;
		try {
			f = new File(URLDecoder.decode(u.getFile(), "UTF-8")).getAbsoluteFile();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.err.println("dir-2: "+f);
		return f;
	}
	private String getProg(String basename, String suffix) {
		String kitProgRoot = getRunDir().getParent();
		String dir;
		if (OS.isFamilyMac()) {
			dir = getRunDir().getParent();
			return dir+File.separator+basename;
		} else if  (OS.isFamilyWindows()) {
			dir = kitProgRoot;
			return dir+File.separator+basename+suffix;
		}
		dir = kitProgRoot;
		return dir+File.separator+basename;
	}
	
	static final int BUFF_SIZE = 8192;
	static final byte[] buffer = new byte[BUFF_SIZE];

	public static void copy(InputStream in, OutputStream out) throws IOException{
	   try {
	      while (true) {
	         synchronized (buffer) {
	            int amountRead = in.read(buffer);
	            if (amountRead == -1) {
	               break;
	            }
	            out.write(buffer, 0, amountRead); 
	         }
	      } 
	   } finally {
	      if (in != null) {
	         in.close();
	      }
	      if (out != null) {
	         out.close();
	      }
	   }
	}


	public void setUsingLazyOTF(boolean useLazyOTF) {
		this.useLazyOTF = useLazyOTF;
	}

	private String modelFilename;
	private String instFilename;
	private String title; // to hold the context info
	private LTS lts;
	private CompoundLTS clts;
	private DotLTS dotlts;
	private Anidot anidot;
	private LTSKind ltsKind;
	private AdapterKind adapterKind;
	private String[] envVars = null;
	private Instantiator inst = null;
	private boolean useLazyOTF;
}
