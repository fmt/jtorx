package utwente.fmt.jtorx.testgui;

import java.util.concurrent.TimeUnit;

import utwente.fmt.interpretation.InterpKind;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.logger.mux.LogMux;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.ui.StepHighlighter;
import utwente.fmt.jtorx.writers.AniDotLTSWriter;
import utwente.fmt.lts.LTS;

public class RunSessionData {
	
	public void reset() {
		title = "";
		modelTitle = "";
		guideTitle = "";
		implTitle = "";
		suspAutTitle = "";
		traceTitle = "";
		
		initResult = null;
		finResult = null;
		
		modelWriter = null;
		guideWriter = null;
		implWriter = null;
		suspAutWriter = null;
		traceWriter = null;
		
		modelLTS = null;
		guideLTS = null;
		implLTS = null;
		suspAutLTS = null;
		traceLTS = null;
		
		showMsc = false;
		showRun = false;
		showMod = false;
		showGuide = false;
		showImp = false;
		showSuspAut = false;
		showLogPane = false;
		showCoverage = false;
		
		highlighter = null;
		
		seed = -1;
		timeoutValue = -1;
		timeoutUnit = null;
		
		modelInstantiator = "";
		implInstantiator = "";

		useGuide = false;
		useCoverage = false;
		useModelInstantiator = false;
		useImplInstantiator = false;
		useImplRealTime = false;
		addRmLabelAnnoPfx = false;
		addRmLabelAnnoSfx = false;
		synthDelta = false;
		reconDelta = false;
		angelicCompletion = false;
		divergence = false;
		
		modelData = null;
		guideData = null;
		implData = null;

		interpKind = null;
		interpInputs = null;
		interpOutputs = null;
		traceKind = null;
	}
	
	public void setTitle(String s) {
		title = s;
	}
	public void setModelTitle(String s) {
		modelTitle = s;
	}
	public void setGuideTitle(String s) {
		guideTitle = s;
	}
	public void setImplTitle(String s) {
		implTitle = s;
	}
	public void setSuspAutTitle(String s) {
		suspAutTitle = s;
	}
	public void setTraceTitle(String s) {
		traceTitle = s;
	}


	
	public String getTitle() {
		return title;
	}
	public String getModelTitle() {
		return modelTitle;
	}
	public String getGuideTitle() {
		return guideTitle;
	}
	public String getImplTitle() {
		return implTitle;
	}
	public String getSuspAutTitle() {
		return suspAutTitle;
	}
	public String getTraceTitle() {
		return traceTitle;
	}



	public void setInitResult(DriverInitializationResult r) {
		initResult = r;
	}
	public void setFinResult(DriverFinalizationResult r) {
		finResult = r;
	}
	public void setHighlighter(StepHighlighter h) {
		highlighter = h;
	}

	
	public DriverInitializationResult getInitResult() {
		return initResult;
	}
	public DriverFinalizationResult getFinResult() {
		return finResult;
	}
	public StepHighlighter getHighlighter() {
		return highlighter;
	}

	public void setModelLTS(LTS l) {
		modelLTS = l;
	}
	public void setGuideLTS(LTS l) {
		guideLTS = l;
	}
	public void setImplLTS(LTS l) {
		implLTS = l;
	}
	public void setSuspAutLTS(LTS l) {
		suspAutLTS = l;
	}
	public void setTraceLTS(LTS l) {
		traceLTS = l;
	}

	
	public LTS getModelLTS() {
		return modelLTS;
	}
	public LTS getGuideLTS() {
		return guideLTS;
	}
	public LTS getImplLTS() {
		return implLTS;
	}
	public LTS getSuspAutLTS() {
		return suspAutLTS;
	}
	public LTS getTraceLTS() {
		return traceLTS;
	}


	public void setModelWriter(AniDotLTSWriter w) {
		modelWriter = w;
	}
	public void setGuideWriter(AniDotLTSWriter w) {
		guideWriter = w;
	}
	public void setImplWriter(AniDotLTSWriter w) {
		implWriter = w;
	}
	public void setSuspAutWriter(AniDotLTSWriter w) {
		suspAutWriter = w;
	}
	public void setTraceWriter(AniDotLTSWriter w) {
		traceWriter = w;
	}


	public AniDotLTSWriter getModelWriter() {
		return modelWriter;
	}
	public AniDotLTSWriter getGuideWriter() {
		return guideWriter;
	}
	public AniDotLTSWriter getImplWriter() {
		return implWriter;
	}
	public AniDotLTSWriter getSuspAutWriter() {
		return suspAutWriter;
	}
	public AniDotLTSWriter getTraceWriter() {
		return traceWriter;
	}


	public void setVizMsc(boolean b) {
		showMsc = b;
	}
	public void setVizRun(boolean b) {
		showRun = b;
	}
	public void setVizModel(boolean b) {
		showMod = b;
	}
	public void setVizGuide(boolean b) {
		showGuide = b;
	}
	public void setVizImpl(boolean b) {
		showImp = b;
	}
	public void setVizSuspAut(boolean b) {
		showSuspAut = b;
	}
	public void setVizTrace(boolean b) {
		showTrace = b;
	}
	public void setVizLogPane(boolean b) {
		showLogPane = b;
	}
	public void setVizCoverage(boolean b) {
		showCoverage = b;
	}


	public boolean getVizMsc() {
		return showMsc;
	}
	public boolean getVizRun() {
		return showRun;
	}
	public boolean getVizModel() {
		return showMod;
	}
	public boolean getVizGuide() {
		return showGuide;
	}
	public boolean getVizImpl() {
		return showImp;
	}
	public boolean getVizSuspAut() {
		return showSuspAut;
	}
	public boolean getVizTrace() {
		return showTrace;
	}
	public boolean getVizLogPane() {
		return showLogPane;
	}
	public boolean getVizCoverage() {
		return showCoverage;
	}


	
	public void setSeed(long s) {
		seed = s;
	}
	public long getSeed() {
		return seed;
	}
	public void setSimSeed(long s) {
		simSeed = s;
	}
	public long getSimSeed() {
		return simSeed;
	}
	
	public void setUseGuide(boolean s) {
		useGuide = s;
	}
	public boolean getUseGuide() {
		return useGuide;
	}
	public void setUseCoverage(boolean s) {
		useCoverage = s;
	}
	public boolean getUseCoverage() {
		return useCoverage;
	}
	public void setUseModelInstantiator(boolean s) {
		useModelInstantiator = s;
	}
	public void setUseImplInstantiator(boolean s) {
		useImplInstantiator = s;
	}
	public boolean getUseModelInstantiator() {
		return useModelInstantiator;
	}
	public boolean getUseImplInstantiator() {
		return useImplInstantiator;
	}
	public void setUseImplRealTime(boolean s) {
		useImplRealTime = s;
	}
	public boolean getUseImplRealTime() {
		return useImplRealTime;
	}
	public void setAddRmLabelAnnoPfx(boolean s) {
		addRmLabelAnnoPfx = s;
	}
	public boolean getAddRmLabelAnnoPfx() {
		return addRmLabelAnnoPfx;
	}
	public void setAddRmLabelAnnoSfx(boolean s) {
		addRmLabelAnnoSfx = s;
	}
	public boolean getAddRmLabelAnnoSfx() {
		return addRmLabelAnnoSfx;
	}


	public void setSynthDelta(boolean s) {
		synthDelta = s;
	}
	public boolean getSynthDelta() {
		return synthDelta;
	}
	public void setReconDelta(boolean s) {
		reconDelta = s;
	}
	public boolean getReconDelta() {
		return reconDelta;
	}
	public void setAngelicCompletion(boolean s) {
		angelicCompletion = s;
	}
	public boolean getAngelicCompletion() {
		return angelicCompletion;
	}
	public void setDivergence(boolean s) {
		divergence = s;
	}
	public boolean getDivergence() {
		return divergence;
	}

	
	public void setTimeoutValue(int s) {
		timeoutValue = s;
	}
	public int getTimeoutValue() {
		return timeoutValue;
	}
	public void setTimeoutUnit(TimeUnit v) {
		timeoutUnit = v;
	}
	public TimeUnit getTimeoutUnit() {
		return timeoutUnit;
	}

	public void setModelData(RunItemData s) {
		modelData = s;
	}
	public void setGuideData(RunItemData s) {
		guideData = s;
	}
	public void setImplData(RunItemData s) {
		implData = s;
	}
	public void setModelPrimerData(RunItemData s) {
		modelPrimerData = s;
	}
	public void setGuidePrimerData(RunItemData s) {
		guidePrimerData = s;
	}
	public void setCombinatorData(RunItemData s) {
		combinatorData = s;
	}
	public void setTraceData(RunItemData s) {
		traceData = s;
	}


	public RunItemData getModelData() {
		return modelData;
	}
	public RunItemData getGuideData() {
		return guideData;
	}
	public RunItemData getImplData() {
		return implData;
	}
	public RunItemData getModelPrimerData() {
		return modelPrimerData;
	}
	public RunItemData getGuidePrimerData() {
		return guidePrimerData;
	}
	public RunItemData getCombinatorData() {
		return combinatorData;
	}
	public RunItemData getTraceData() {
		return traceData;
	}
	
	public RunItemData getDataByKind(LogMux.Kind k) {
		switch(k) {
		case SUSPAUT: // TODO is this correct? or should we reurn modelPrimerData????
		case MODEL:
			return modelData;
		case GUIDE:
			return guideData;
		case IMPL:
			return implData;
		default:
			return null;
		}
	}



	public void setTraceKind(TraceKind s) {
		traceKind = s;
	}
	public TraceKind getTraceKind() {
		return traceKind;
	}
	
	public void setModelInstantiator(String s) {
		modelInstantiator = s;
	}
	public void setImplInstantiator(String s) {
		implInstantiator = s;
	}
	public String getModelInstantiator() {
		return modelInstantiator;
	}
	public String getImplInstantiator() {
		return implInstantiator;
	}


	public void setInterpKind(InterpKind s) {
		interpKind = s;
	}
	public void setInterpInputs(String s) {
		interpInputs = s;
	}
	public void setInterpOutputs(String s) {
		interpOutputs = s;
	}
	public InterpKind getInterpKind() {
		return interpKind;
	}
	public String getInterpInputs() {
		return interpInputs;
	}
	public String getInterpOutputs() {
		return interpOutputs;
	}
	
	
	public void fillConfig(Config c) {
		c.add("SEED", ""+getSeed());
		
		c.add("TIMEOUTVAL", getTimeoutValue());
		c.add("TIMEOUTUNIT", getTimeoutUnit());
		
		c.add("USEREALTIME", getUseImplRealTime());
		c.add("ADDRMLABELANNOPREFIX", getAddRmLabelAnnoPfx());
		c.add("ADDRMLABELANNOSUFFIX", getAddRmLabelAnnoSfx());
		
		c.add("TRACEKIND", getTraceKind());
		
		if (getModelData()!=null) {
			c.add("MODELTEXT", getModelData().getModelFilename());
			c.add("MODELKIND", getModelData().getLTSKind());
		}
		c.add("USEMODELINST", getUseModelInstantiator());
		c.add("INSTMODELTEXT", getModelInstantiator());
		
		if (getImplData() != null) {
			c.add("IMPLTEXT", getImplData().getModelFilename());
			c.add("IMPLKIND", getImplData().getLTSKind());
			c.add("ADAPTERKIND", getImplData().getAdapterKind());
			if (getImplData().getAdapterKind() == RunItemData.AdapterKind.SIMADAPTER
			        || getImplData().getAdapterKind() == RunItemData.AdapterKind.IOCOSIMADAPTER) {
				c.add("SIMSEED", ""+getSimSeed());
			}
		}
		c.add("USEIMPLINST", getUseImplInstantiator());
		c.add("INSTIMPLTEXT", getImplInstantiator());

		c.add("USEGUIDE", ""+getUseGuide());
		if (getGuideData() != null) {
			c.add("GUIDETEXT", getGuideData().getModelFilename());
			c.add("GUIDEKIND", getGuideData().getLTSKind());
		}
		c.add("USECOVERAGE", getUseCoverage());
		c.add("VIZCOVERAGE", getVizCoverage());
		c.add("INTERPKIND", getInterpKind());
		c.add("SYNTHDELTA", getSynthDelta());
		c.add("RECONDELTA", getReconDelta());
		c.add("ANGELCOMPL", getAngelicCompletion());
		c.add("DIVERGENCE", getDivergence());
		c.add("INTERPINPUTS", getInterpInputs());
		c.add("INTERPOUTPUTS", getInterpOutputs());
	}


	public void setLazyOTFInstance(LazyOTF inst) {
		this.lazyOTFInstance = inst;
	}

	public LazyOTF getLazyOTFInstance() {
		return this.lazyOTFInstance;
	}
	


    private LazyOTF lazyOTFInstance = null;
    private String title;
	private String modelTitle;
	private String guideTitle;
	private String implTitle;
	private String suspAutTitle;
	private String traceTitle;
	private DriverInitializationResult initResult;
	private DriverFinalizationResult finResult;
	private AniDotLTSWriter modelWriter;
	private AniDotLTSWriter guideWriter;
	private AniDotLTSWriter implWriter;
	private AniDotLTSWriter suspAutWriter;
	private AniDotLTSWriter traceWriter;
	private LTS modelLTS;
	private LTS guideLTS;
	private LTS implLTS;
	private LTS suspAutLTS;
	private LTS traceLTS;
	private boolean showMsc;
	private boolean showRun;
	private boolean showMod;
	private boolean showGuide;
	private boolean showImp;
	private boolean showSuspAut;
	private boolean showTrace;
	private boolean showLogPane;
	private boolean showCoverage;
	private StepHighlighter highlighter;
	
	private long seed;
	private long simSeed;
	private int timeoutValue;
	private TimeUnit timeoutUnit;
	
	private String modelInstantiator;
	private String implInstantiator;
	
	private boolean useGuide;
	private boolean useCoverage;
	private boolean useModelInstantiator;
	private boolean useImplInstantiator;
	private boolean useImplRealTime;
	private boolean addRmLabelAnnoPfx;
	private boolean addRmLabelAnnoSfx;
	private boolean synthDelta;
	private boolean reconDelta;
	private boolean angelicCompletion;
	private boolean divergence;

	private RunItemData modelData;
	private RunItemData implData;
	private RunItemData guideData;
	private RunItemData modelPrimerData;
	private RunItemData guidePrimerData;
	private RunItemData combinatorData;
	private RunItemData traceData;

	
	private InterpKind interpKind;
	private String interpInputs;
	private String interpOutputs;
	
	private TraceKind traceKind;
	
}
