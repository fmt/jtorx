package utwente.fmt.jtorx.testgui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;


public class SimPathPane implements JTorXPane {
	
	public Group getGroup() {
		return pathGroup;
	}
	
	public void runSetup() {
		clear();
	}public void runBegins() {
		clear();
	}
	public void runEnds() {
	}
	
	public void init(String id) {
		clear(); //overkill
		ArrayList<String> curTrace = new ArrayList<String>(1);
		curTrace.add(id);
		traceMap.put(id,curTrace);
		pathId = id;
		if (debug)
			System.err.println("init id="+id+" curTrace="+curTrace);
	}
	public void addTransition(String trans, String pId, String id) {
		// set up transition trace that we (later) show in SimPathPane
		parentMap.put(id, pId);
		ArrayList<String> parentTrace = traceMap.get(pId);
		ArrayList<String> curTrace = new ArrayList<String>(parentTrace==null?1:parentTrace.size()+1);
		if (parentTrace!=null)
			curTrace.addAll(parentTrace);
		curTrace.add(id);
		traceMap.put(id, curTrace);
		transitionMap.put(id, trans);
		if (debug)
			System.err.println("addTransition id="+id+" pId="+pId+" trans="+trans+" curTrace="+curTrace+" parTrace="+parentTrace);
	}
	
	public void showTransitionPath(String id) {
		ArrayList<String>curTrace = traceMap.get(id);
		if (curTrace==null) {
			System.err.println("SimPathPane.showTransitionPath: unknown id: "+id);
			return;
		}
		if (debug)
			System.err.println("ShowTransitionPath id="+id+" curTrace="+curTrace);
		int idx = curTrace.lastIndexOf(pathId);
		if (debug)
			System.err.println("\tShowTransitionPath id="+id+" curTrace="+curTrace+" pathId="+pathId+" idx="+idx);
		while(pathId!=null && idx < 0) {
			dropFromEnd();
			idx = curTrace.lastIndexOf(pathId);
			if (debug)
				System.err.println("\tShowTransitionPath loop id="+id+" curTrace="+curTrace+" pathId="+pathId+" idx="+idx);
		}
		if (idx < 0)
			idx = 0;
		else
			idx++;
		String stepId;
		while(idx < curTrace.size()) {
			stepId = curTrace.get(idx);
			appendStep(stepId, (idx)+" "+transitionMap.get(stepId));
			idx++;
		}
	}
	
	public void highLight(final int n) {
		if (app.getDisplay().getThread()==Thread.currentThread())
			doHighLight(n);
		else {
			app.getDisplay().syncExec (new Runnable () {
				public void run () {
					doHighLight(n);
				}
			});
		}
	}



	private void doHighLight(int n) {
		int start, end;
		
		if (n < 0 || n >= pathLineEnd.size())
			return;
		if (n == 0) {
			start = 0;
			end = 0;
		} else {
			start = pathLineEnd.elementAt(n-1);
			end = pathLineEnd.elementAt(n);
		}
		pathText.clearSelection();
		pathText.setSelection(start, end);
		pathText.showSelection();
		pathText.update();
		System.err.println("SimPathPane: highlight "+n+" selection=" + pathText.getSelection ());
		app.getDisplay().update();
	}

	public SimPathPane(Composite parent, Testgui g) {
		app = g;
		
		// Path Pane
		pathGroup = new Group(parent, SWT.NONE);
		pathGroup.setText("Executed steps:");
		FillLayout fillHLayout = new FillLayout(SWT.HORIZONTAL);
		pathGroup.setLayout(fillHLayout);

		pathText = new Text(pathGroup, SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL); 
		pathText.setEditable(false); 
		pathText.setText("\n\n");
	}
	
	private void appendStep(String id, String s) {
		pathText.append(s+"\n");
		pathLineEnd.add(pathText.getCharCount());
		pathId = id;
	}
	private void clear() {
		transitionMap.clear();
		parentMap.clear();
		pathText.setText("");
		pathLineEnd.clear();
		pathLineEnd.add(0);
		pathId = null;
	}
	
	private void dropFromEnd() {
		int start, end;
		int n = pathLineEnd.size() - 1;
		if (n == 0) {
			start = 0;
			end = 0;
		} else {
			start = pathLineEnd.elementAt(n-1);
			end = pathLineEnd.elementAt(n);
		}
		pathText.clearSelection();
		pathText.setSelection(start, end);
		pathText.insert("");
		pathLineEnd.removeElementAt(n);
		pathId = parentMap.get(pathId);
	}


	
	private Testgui app;
	private Group pathGroup;
	
	private Text pathText =  null;
	private Vector<Integer> pathLineEnd = new Vector<Integer>();
	private String pathId = null;

	private Map<String, String> transitionMap = new HashMap<String, String>();
	private Map<String, String> parentMap = new HashMap<String, String>();
	private Map<String, ArrayList<String>> traceMap = new HashMap<String, ArrayList<String>>();
	
	private boolean debug = false;
}
