package utwente.fmt.jtorx.testgui;

import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import java.io.File;

import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.logger.anidot.Anidot;
import utwente.fmt.jtorx.logger.anidot.DotLTS;
import utwente.fmt.jtorx.logger.mux.LogMux;
import utwente.fmt.jtorx.simulator.Simulator;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.writers.AniDotLTSWriter;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.LTS.LTSException;


public class SimStartPane implements JTorXPane {

	public void runSetup() {
		simButton.setText("Abort");
	}
	public void runBegins() {
		simButton.setText("Stop");
	}
	public void runEnds() {
		simButton.setText("Start");
	}

	public Group getGroup() {
		return simGroup;
	}
		
	public void openTraceFileDialog() {
		String s  = app.openFileDialog("Open Trace", traceText.getText());
		if (s!=null)
			traceText.setText(s);
	}
	
	public LTS viewTrace() {
		String fileName = traceText.getText().trim();
		if (fileName.equals(""))
			return null;

		File f = new File(fileName);
		String title = Testgui.titleOf(f);
		LogMux.Kind kind = LogMux.Kind.TRACE;
		LTS lts = traceData.readModelFile(fileName, app.getErrorReporter(), app.getProgressReporter());
		if (lts == null)
			return null;
		
		if (traceData.getAnidot() != null)
			traceData.getAnidot().end();
		Anidot anidot = app.getLogMux().newAnidot(title, kind);
		if (anidot != null) {
			Iterator<?extends State> it = null;
			try {
				it = lts.init();
				traceData.setFilename(fileName, ""); // TODO assume no instantiator necessary
				traceData.setLTS(lts);
				DotLTS dotLTS = new DotLTS(anidot);
				anidot.setRender(Anidot.RenderMode.OFF);
				new AniDotLTSWriter(lts, interpAny, dotLTS, app.getStateSpaceProgressReporter() ).write();
				dotLTS.addRoot(title, lts.getRootPosition());
				// dotLTS.addInvisibleRoot(); // hiding the node does not work
				while(it.hasNext())
					dotLTS.addInitialEdge(it.next());
				anidot.setRender(Anidot.RenderMode.ON);
			} catch (LTSException e) {
				System.err.println("viewTrace: caught LTSException: "+e.getMessage());
			}
			anidot.end();
			// rid.clear();
		} else {
			app.getErrorReporter().report("simulator: view trace: no anidot");
		}
		return lts;

	}

	public SimStartPane(Composite parent, Testgui g, Simulator s, String title) {
		app = g;
		simulator = s;

		// Run Pane
		simGroup = new Group(parent, SWT.NONE);
		/*
		RowLayout simLayout = new RowLayout();
		simLayout.wrap = false;
		simLayout.type = SWT.HORIZONTAL;
		simLayout.fill = true;
		simGroup.setLayout(simLayout);
		//simLayout.pack = true; 
		 */
		GridLayout simLayout = new GridLayout();
		simLayout.numColumns = 3;
		simGroup.setLayout(simLayout);
		
		if (title!=null && !title.equalsIgnoreCase(""))
			simGroup.setText("Simulation run: "+title);
		else
			simGroup.setText("Simulation run:");

		Group startGroup  = new Group(simGroup, SWT.NONE);
		RowLayout startLayout = new RowLayout();
		startLayout.wrap = false;
		startLayout.type = SWT.HORIZONTAL;
		//startLayout.pack = true; 
		startGroup.setLayout(startLayout);
		
		GridData startLayoutData = new GridData();
		startLayoutData.verticalAlignment = GridData.FILL;
		startLayoutData.grabExcessVerticalSpace = true;
		startGroup.setLayoutData(startLayoutData);


		simButton = new Button(startGroup,SWT.PUSH);
		simButton.setText("Start");
		simButton.pack();

		startGroup.pack();

		simButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				simButton.setEnabled(false);
				traceData.setFilename(traceText.getText().trim(), ""); // TODO assume no instantiator necessary
				
				runData.reset();
				runData.setVizModel(modVizButton.getSelection());
				runData.setVizSuspAut(suspAutVizButton.getSelection());
				runData.setVizTrace(traceVizButton.getSelection());
				
				if (simButton.getText().equals("Start")) {
					if (useTraceButton.getSelection() && ! traceData.readLTSFileIfNeeded(app.getErrorReporter(), app.getProgressReporter()))
						return;
					if (useTraceButton.getSelection())
						simulator.start(runData, traceData);
					else
						simulator.start(runData);
				} else { // TODO add support for Abort
					simulator.stop();
				}
				simButton.setEnabled(true);
			}
		}); 
		
		Group vizGroup = new Group(simGroup, SWT.NONE);
		RowLayout vizLayout = new RowLayout();
		vizLayout.wrap = false;
		vizLayout.type = SWT.HORIZONTAL;
		vizLayout.pack = true;
		vizGroup.setLayout(vizLayout);

		GridData vizLayoutData = new GridData();
		vizLayoutData.verticalAlignment = GridData.FILL;
		vizLayoutData.grabExcessVerticalSpace = true;
		vizGroup.setLayoutData(vizLayoutData);

		org.eclipse.swt.widgets.Label vizLabel = new org.eclipse.swt.widgets.Label(vizGroup, SWT.NONE);
		vizLabel.setText("Show:");
		
		modVizButton = new Button(vizGroup, SWT.CHECK);
		modVizButton.setText("model");
		modVizButton.setSelection(true);
		app.registerDotVizButton(modVizButton);
		suspAutVizButton = new Button(vizGroup, SWT.CHECK);
		suspAutVizButton.setText("sa");
		suspAutVizButton.setSelection(false);
		app.registerDotVizButton(suspAutVizButton);
		traceVizButton = new Button(vizGroup, SWT.CHECK);
		traceVizButton.setText("trace");
		traceVizButton.setSelection(true);
		app.registerDotVizButton(traceVizButton);

		vizGroup.pack();


		
		Group traceGroup  = new Group(simGroup, SWT.NONE);
		GridData traceGroupGridData = new GridData(GridData.FILL_HORIZONTAL);
		traceGroup.setLayoutData(traceGroupGridData);
		FormLayout traceLayout = new FormLayout();
		traceGroup.setLayout(traceLayout);

		GridData traceLayoutData = new GridData();
		traceLayoutData.verticalAlignment = GridData.FILL;
		traceLayoutData.horizontalAlignment = GridData.FILL;
		traceLayoutData.grabExcessVerticalSpace = true;
		traceLayoutData.grabExcessHorizontalSpace = true;
		traceGroup.setLayoutData(traceLayoutData);


		useTraceButton = new Button(traceGroup,SWT.LEFT|SWT.CHECK);
		useTraceButton.setText("Use trace:");
		useTraceButton.pack();
		FormData useTraceButtonData = new FormData();
		useTraceButtonData.left = new FormAttachment(0);
		useTraceButtonData.top = new FormAttachment(0);
		useTraceButtonData.bottom = new FormAttachment(100);
		useTraceButton.setLayoutData(useTraceButtonData);

		viewButton = new Button(traceGroup,SWT.LEFT|SWT.PUSH);
		viewButton.setText("View");
		viewButton.pack();
		FormData viewButtonData = new FormData();
		viewButtonData.top = new FormAttachment (useTraceButton,0,SWT.CENTER);
		//viewButtonData.top = new FormAttachment(0);
		//viewButtonData.bottom = new FormAttachment(100);
		viewButtonData.right = new FormAttachment(100);
		viewButton.setLayoutData(viewButtonData);


		browseButton = new Button(traceGroup,SWT.LEFT|SWT.PUSH);
		browseButton.setText("Browse");
		browseButton.pack();
		FormData browseButtonData = new FormData();
		browseButtonData.top = new FormAttachment (useTraceButton,0,SWT.CENTER);
		//browseButtonData.top = new FormAttachment(0);
		//browseButtonData.bottom = new FormAttachment(100);
		browseButtonData.right = new FormAttachment(viewButton, -5);
		browseButton.setLayoutData(browseButtonData);


		traceText = new Text(traceGroup, SWT.SINGLE);
		traceText.pack();
		traceText.addModifyListener(new ModifyListener() { 
			public void modifyText(ModifyEvent e) {
				// treeConfigPane.setGuideText(guideText.getText());
			} 
		}); 
		FormData traceTextData = new FormData();
		traceTextData.top = new FormAttachment (useTraceButton,0,SWT.CENTER);
		traceTextData.left = new FormAttachment(useTraceButton, 5);
		traceTextData.right = new FormAttachment(browseButton, -5);
		//traceTextData.top = new FormAttachment(0);
		//traceTextData.bottom = new FormAttachment(100);
		traceText.setLayoutData(traceTextData);


		traceGroup.pack();
		
		Group instGroup  = new Group(simGroup, SWT.NONE);
		FormLayout instLayout = new FormLayout();
		instGroup.setLayout(instLayout);
		//instLayout.wrap = false;
		//instLayout.type = SWT.HORIZONTAL;
		//instLayout.pack = false; 
		//instGroup.setLayout(instLayout);
		GridData instLayoutData = new GridData();
		instLayoutData.horizontalAlignment = GridData.FILL;
		instLayoutData.grabExcessHorizontalSpace = true;
		instLayoutData.horizontalSpan = 3;
		instGroup.setLayoutData(instLayoutData);
		
		org.eclipse.swt.widgets.Label instLabel = new org.eclipse.swt.widgets.Label(instGroup, SWT.NONE);
		instLabel.setText("Instantiation:");
		FormData instLabelData = new FormData();
		instLabelData.top = new FormAttachment(0);
		instLabelData.left = new FormAttachment(0);
		instLabel.setLayoutData(instLabelData);

		
		instGetButton = new Button(instGroup,SWT.PUSH|SWT.CENTER);
		instGetButton.setText("Get");
		instGetButton.setEnabled(true);
		FormData instGetButtonData = new FormData();
		instGetButtonData.left = new FormAttachment(instLabel, 0);
		instGetButtonData.top = new FormAttachment(0);
		//instGetButtonData.bottom = new FormAttachment(100);
		instGetButton.setLayoutData(instGetButtonData);
		instGetButton.pack();

		instUseButton = new Button(instGroup,SWT.PUSH|SWT.CENTER);
		instUseButton.setText("Use");
		instUseButton.setEnabled(true);
		FormData instButtonData = new FormData();
		instButtonData.left = new FormAttachment(instGetButton, 0);
		instButtonData.top = new FormAttachment(0);
		//instButtonData.bottom = new FormAttachment(100);
		instUseButton.setLayoutData(instButtonData);
		instUseButton.pack();

		
		instText = new Text(instGroup, SWT.SINGLE );
		FormData instTextData = new FormData();
		instTextData.top = new FormAttachment(instUseButton, 0,SWT.CENTER);
		instTextData.left = new FormAttachment(instUseButton, 5);
		instTextData.right = new FormAttachment(100);
		//instTextData.bottom = new FormAttachment(100);
		instText.setLayoutData(instTextData);
		instText.pack();
		
		instGroup.pack();
		
		simContainer = new PaneContainer(simGroup, SWT.HORIZONTAL);
		simContainer.addResizable(startGroup);
		simContainer.addResizable(vizGroup);
		simContainer.addResizable(traceGroup);
		simContainer.addResizable(instGroup, 20);

		simGroup.pack();
		
		simContainer.layout();

		
		browseButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				openTraceFileDialog();
			}
		}); 
		viewButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				viewTrace();
			}
		});
		instGetButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				String s = simulator.handleGetSolutionEvent();
				if (s!=null)
					instText.setText(s);
			}
		}); 
		instUseButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				String s = instText.getText();
				if (s!=null && !s.equals("")) {
					Label l = new LibLabel(s);
					simulator.handleGivenStimulusEvent(l);
				}
			}
		}); 


		if (s.haveTraceToShow())
			traceGroup.setEnabled(false);

	}
	
	private Testgui app;
	private Group simGroup = null;
	private PaneContainer simContainer = null;
	private Button simButton = null;
	private Button modVizButton = null;
	private Button suspAutVizButton = null;
	private Button traceVizButton = null;
	private Button useTraceButton = null;
	private Button browseButton = null;
	private Button viewButton = null;
	private Text traceText = null;
	private Button instGetButton = null;
	private Button instUseButton = null;
	private Text instText = null;
	private Simulator simulator = null;
	private RunSessionData runData = new RunSessionData();
	private RunItemData traceData = new RunItemData();
	private Interpretation interpAny = new AnyInterpretation();
	
}
