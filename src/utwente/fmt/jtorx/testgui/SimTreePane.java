package utwente.fmt.jtorx.testgui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.simulator.Simulator;
import utwente.fmt.jtorx.torx.InstantiationImpl;
import utwente.fmt.jtorx.utils.term.TermParser;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Term;
import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.UnificationResult;

public class SimTreePane implements JTorXPane {

	public void runSetup() {
		tree.removeAll();
		mapStateItems = new HashMap<CompoundState, TreeItem[]>();
		mapIdToItem = new HashMap<String, TreeItem>();
		nextNode = 0;
		running = false;
	}
	public void runBegins() {
		tree.removeAll();
		mapStateItems = new HashMap<CompoundState, TreeItem[]>();
		mapIdToItem = new HashMap<String, TreeItem>();
		nextNode = 0;
		running = true;
	}

	public void runEnds() {
		running = false;
	}
	
	public Composite getGroup() {
		return simtreeGroup;
	}
	
	public String addRoot(CompoundState s) {
		TreeItem treeItem = new TreeItem (tree, SWT.NONE);
		treeItem.setText("<root>");
		treeItem.setData("state", s);
		treeItem.setData("unfolded", false);
		String id = "tn"+nextNode++;
		treeItem.setData("id", id);
		TreeItem[] items =  new TreeItem[1];
		items[0] = treeItem;
		mapStateItems.put(s, items);
		mapIdToItem.put(id, treeItem);
		showTransitionOrState(treeItem);
		return id;
	}
	
	public String addChild(TreeItem i, CompoundTransition<? extends CompoundState> t) {
		return 	addChild(i, t, null, null);
	}
	public String addChild(TreeItem i, CompoundTransition<? extends CompoundState> t, TreeItem orig) {
		return 	addChild(i, t, null, orig);
	}
	public String addChild(TreeItem i, CompoundTransition<? extends CompoundState> t, final CompoundTransition<? extends CompoundState> tt) {
		return 	addChild(i, t, tt, null);
	}

	public String addChild(final TreeItem i, final CompoundTransition<? extends CompoundState> t, final CompoundTransition<? extends CompoundState> tt, final TreeItem orig) {
		final String id = "tn"+nextNode++;
		if (app.getDisplay().getThread()==Thread.currentThread())
			doAddChild(id, i, t, tt, orig);
		else {
			app.getDisplay().syncExec (new Runnable () {
				public void run () {
					doAddChild(id, i, t, tt, orig);
				}
			});
		}
		return id;
	}
	private void doAddChild(String id, TreeItem i, CompoundTransition<? extends CompoundState> t, CompoundTransition<? extends CompoundState> tt, TreeItem orig) {
		TreeItem treeItem = new TreeItem (i, SWT.NONE);
		mapIdToItem.put(id, treeItem);
		CompoundState d = t.getDestination();
		String sfx = "";
		if (d!=null) {
			System.err.println("dst: id="+d.getID()+ " lbl="+d.getLabel(false));
			if (orig!=null) {
				sfx = " (* Instantiated Event *)";
				treeItem.setData("orig", orig);
			}
			TreeItem[] elsewhere = mapStateItems.get(d);
			int nelsewhere = (elsewhere==null?0:elsewhere.length);
			if (elsewhere!=null && nelsewhere > 0 && isParent(elsewhere[0], treeItem)) {
				sfx += " (* Recursion Detected *)";
				treeItem.setData("recursion", elsewhere[0]);
			} else if (elsewhere !=null && nelsewhere > 0) {
				sfx += " (* Analysed Elsewhere *)";
				treeItem.setData("elsewhere", elsewhere[0]);
			} 
			TreeItem[] ew = new TreeItem[nelsewhere+1];
			if (elsewhere!=null) {
				for (int j=0;j< nelsewhere;j++)
					ew[j] = elsewhere[j];
			}
			ew[nelsewhere] = treeItem;	
			mapStateItems.put(d, ew);
		}
		
		treeItem.setText(format(t.getLabel(), sfx));
		treeItem.setData("state", t.getDestination());
		treeItem.setData("trans", t);
		if (tt!=null)
			treeItem.setData("tracetrans", tt);
		treeItem.setData("unfolded", false);
		treeItem.setData("id", id);
		showTransitionOrState(treeItem);
	}
	private String format(Label l, String sfx) {
		String l_s = l.getString();
		String c = l.getConstraint()!=null?l.getConstraint().getString():null;
		String c_s = "";
		if (c!=null && !c.trim().equals(""))
			c_s = " ["+c+"]";
		return l_s + c_s + sfx;
	}

	public void select(final String id) {
		if (app.getDisplay().getThread()==Thread.currentThread()) {
			doSelect(id);
		} else {
			app.getDisplay().syncExec (new Runnable () {
				public void run () {
					doSelect(id);
				}
			});
		}
	}
	
	private void doSelect(String id) {
		TreeItem item = findItem(id);
		System.out.println("SimTreePane select "+id+" : "+item);
		if (item!=null) {
			tree.select(item);
			tree.showSelection();
			highlightTransitionOrState(item);
		}
	}
	
	public void nohandleGivenStimulusEvent(final Label l) {
	}

	public void handleGivenStimulusEvent(final Label l) {
		if (app.getDisplay().getThread()==Thread.currentThread()) {
			doHandleGivenStimulusEvent(l);
		} else {
			app.getDisplay().syncExec (new Runnable () {
				public void run () {
					doHandleGivenStimulusEvent(l);
				}
			});
		}
	}
	
	private void nodoHandleGivenStimulusEvent(final Label l) {

		final TreeItem[] items = tree.getSelection();

		if (items!=null && items.length==1) {
			TreeItem item = items[0];
			if (item==null) {
				System.out.println("Simulator: nothing to instantiate (empty item found)");
				return;
			}
			final CompoundState s = (CompoundState)item.getData("state");
			if (s==null) {
				System.out.println("Simulator: nothing to instantiate (no transition found)");
				return;
			}
			System.out.println("Simulator: trying to instantiate ...");
		}
	}
	private void doHandleGivenStimulusEvent(final Label l) {

		if (l==null) {
			System.out.println("Simulator: no instantiation label");
			return;
		}

		final TreeItem[] items = tree.getSelection();

		if (items!=null && items.length==1) {
			TreeItem item = items[0];
			if (item==null) {
				System.out.println("Simulator: nothing to instantiate (empty item found)");
				return;
			}
			final CompoundState s = (CompoundState)item.getData("state");
			if (s==null) {
				System.out.println("Simulator: nothing to instantiate (no state found)");
				return;
			}
			CompoundTransition<?extends CompoundState> res = s.next(l);
			if (res!=null) {
				addChild(item.getParentItem(), res, item);
			} else {
				CompoundTransition<? extends CompoundState> t = (CompoundTransition<? extends CompoundState>)item.getData("trans");
				if (t==null) {
					System.out.println("Simulator: nothing to instantiate (no transition found)");
					return;
				}
				Label t_l = t.getLabel();
				if (t_l==null) {
					System.out.println("Simulator: no transition label");
					return;
				}
					Term t_tm = TermParser.parse(t_l);
				if (t_tm==null) {
					System.out.println("Simulator: could not parse transition label");
					return;
				}
				Term l_tm = TermParser.parse(l);
				if (l_tm==null) {
					System.out.println("Simulator: could not parse instantiation label");
					return;
				}
				UnificationResult uni_res = t_tm.unify(l_tm);
				System.out.println("Simulator: trying: l: ("+l.getString()+") "+l.hashCode()+" "+l.toString()+" tl:("+t_l.getString()+")"+t_l.hashCode()+" "+t_l.toString());
				if (uni_res.isUnifyable()) {
					System.out.println(" is-unifyable");
					ArrayList<UnificationBinding> b = uni_res.getBindings();
					if (b==null || b.isEmpty()) {
						System.out.println("Simulator instantiation had no bindings: nothing new to add to tree");						
						// identical to what we already had?
						// no need to insert?
					} else {
						CompoundTransition<?extends CompoundState> t_inst = t.instantiate(new InstantiationImpl(l, l_tm, b));
						System.out.println("Simulator.next: inst-result="+t_inst);
						if (t_inst != null) { // i.e. instantiation was succesful
							addChild(item.getParentItem(), t_inst, item);
						} else {
							System.out.println("Simulator instantiation failed");
						}
					}	
				}
			}
		}

	}
	
	public String handleGetSolutionEvent() {
		//	if (app.getDisplay().getThread()==Thread.currentThread()) {
				return doHandleGetSolutionEvent();
		//	} else {
		//		app.getDisplay().syncExec (new Runnable () {
		//			public void run () {
		//				return doHandleGetSolutionEvent();
		//			}
		//		});
		//	}
		}
		
		
	private String doHandleGetSolutionEvent() {

		final TreeItem[] items = tree.getSelection();

		if (items!=null && items.length==1) {
			TreeItem item = items[0];
			if (item==null) {
				System.out.println("Simulator: nothing to get solution for (empty item found)");
				return null;
			}
			CompoundTransition<? extends CompoundState> t = (CompoundTransition<? extends CompoundState>)item.getData("trans");
			if (t==null) {
				System.out.println("Simulator: nothing to get solution for (no transition found)");
				return null;
			}
			// TODO: should use general instantiator,
			// which allows us also to use external instantiator
			// (which we then also have to start if necessary whens starting the simulator!!)
			// and then obtain solution using something like:
			//		Label l = myInstantiator.getInstantiation(t);
			Label l = t.getSolution();
			if (l==null) {
				System.out.println("Simulator: failed to get solution");
				return null;
			} else {
				return l.getString();
			}
		} else {
			System.out.println("Simulator: nothing to get solution for (please select a single item)");
			return null;
		}

	}


	public SimTreePane(Composite parent, Testgui g, Simulator s) {
		app = g;
		simulator = s;
		
		simtreeGroup = new Composite(parent, SWT.NONE);
		
		tree = new Tree(simtreeGroup, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.VIRTUAL );
		tree.setHeaderVisible(true);
		tree.setLinesVisible(true);
		
		simtreeGroup.setLayout( new FillLayout() );
		
		// tree.addListener (SWT.MouseUp, mouseListener);
		// tree.addListener (SWT.MouseDown, mouseListener);

		/*
		tree.addListener (SWT.MouseUp, new Listener () {
			public void handleEvent (Event event) {
				Point point = new Point (event.x, event.y);
				final TreeItem item = tree.getItem (point);
				if (event.button == 1 && item != null) {
					System.out.println ("Mouse up: " + item);
					showTransitionOrState(item);
				}
			}

		});
		*/
		
		tree.addSelectionListener(new SelectionListener () {
			public void widgetDefaultSelected(SelectionEvent event) {
				final TreeItem item = (TreeItem)event.item;
				if (running && item.getItemCount() == 0) {
					unfold(item);
				}
			}
			public void widgetSelected(SelectionEvent event) {
				final TreeItem item = (TreeItem)event.item;
				highlightTransitionOrState(item);
			}
		});
		
		tree.addKeyListener(new KeyListener() {

			public void keyPressed(KeyEvent event) {
				if (event.keyCode==SWT.ARROW_RIGHT) {
					final TreeItem[] items = tree.getSelection();
					if (items!=null && items.length==1) {
						TreeItem item = items[0];
						if (running && item.getItemCount() == 0) {
							unfold(item);
						}
					}
				}
			}

			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});

		tree.addListener (SWT.MouseDown, new Listener () {
			public void handleEvent (Event event) {
				Point point = new Point (event.x, event.y);
				final TreeItem item = tree.getItem (point);
				if (event.button == 3 && item != null) {
					System.out.println ("Mouse 3 down: " + item);
					final Boolean unfolded = (Boolean)item.getData("unfolded");
					final TreeItem elsewhere = (TreeItem)item.getData("elsewhere");
					final TreeItem recursion = (TreeItem)item.getData("recursion");
					final TreeItem orig = (TreeItem)item.getData("orig");
					final TreeItem parent = item.getParentItem();
					final TreeItem[] sibblings = parent==null?null:parent.getItems();
					final CompoundState s = (CompoundState)item.getData("state");
					final TreeItem[] identical = (s==null)?null:mapStateItems.get(s);
					if ((unfolded==null||unfolded!=true) || elsewhere!=null || recursion!=null ||
							parent!=null || (sibblings!=null && sibblings.length > 1) ||
							(identical!=null && identical.length > 1)) {
						Menu menu = new Menu (app.getShell(), SWT.POP_UP);
						if ((unfolded==null||unfolded!=true)) {
							MenuItem unfoldItem = new MenuItem (menu, SWT.PUSH);
							unfoldItem.setText ("Unfold");
							unfoldItem.setEnabled(running);
							unfoldItem.addListener (SWT.Selection, new Listener () {
								public void handleEvent (Event e) {
									unfold(item);
								}
							});
						}
						if (elsewhere!=null) {
							MenuItem elsewhereItem = new MenuItem (menu, SWT.PUSH);
							elsewhereItem.setText ("Elsewhere");
							elsewhereItem.addListener (SWT.Selection, new Listener () {
								public void handleEvent (Event e) {
									System.out.println ("Elsewhere: "+item);
									tree.setSelection(elsewhere);
									highlightTransitionOrState(elsewhere);
								}
							});
						}
						if (recursion!=null) {
							MenuItem recursionItem = new MenuItem (menu, SWT.PUSH);
							recursionItem.setText ("Recursion");
							recursionItem.addListener (SWT.Selection, new Listener () {
								public void handleEvent (Event e) {
									System.out.println ("Recursion: "+item);
									tree.setSelection(recursion);
									highlightTransitionOrState(recursion);
								}
							});
						}
						if (orig!=null) {
							MenuItem unInstantiatedItem = new MenuItem (menu, SWT.PUSH);
							unInstantiatedItem.setText ("UnInstantiated");
							unInstantiatedItem.addListener (SWT.Selection, new Listener () {
								public void handleEvent (Event e) {
									System.out.println ("UnInstantiated: "+item);
									tree.setSelection(orig);
									highlightTransitionOrState(orig);
								}
							});
						}

						if (parent!=null) {
							MenuItem parentItem = new MenuItem (menu, SWT.PUSH);
							parentItem.setText ("Parent");
							parentItem.addListener (SWT.Selection, new Listener () {
								public void handleEvent (Event e) {
									System.out.println ("Parent: "+item);
									tree.setSelection(parent);
									highlightTransitionOrState(parent);
								}
							});
						}

						if (sibblings!=null && sibblings.length > 1) {
							MenuItem sibblingItem = new MenuItem (menu, SWT.PUSH);
							sibblingItem.setText ("Sibbling");
							sibblingItem.addListener (SWT.Selection, new Listener () {
								public void handleEvent (Event e) {
									System.out.println ("Sibbling: "+item);
									TreeItem sibbling = sibblings[0];
									boolean found =  false;
									for(TreeItem ti: sibblings) {
										if (found) {
											sibbling = ti;
											break;
										}
										if (ti.equals(item))
											found=true;
									}
									tree.setSelection(sibbling);
									highlightTransitionOrState(sibbling);
								}
							});
						}
						if (identical!=null && identical.length > 1) {
							MenuItem identicalItem = new MenuItem (menu, SWT.PUSH);
							identicalItem.setText ("Identical");
							identicalItem.addListener (SWT.Selection, new Listener () {
								public void handleEvent (Event e) {
									System.out.println ("Identical: "+item);
									TreeItem result = identical[0];
									boolean found =  false;
									for(TreeItem ti: identical) {
										if (found) {
											result = ti;
											break;
										}
										if (ti.equals(item))
											found=true;
									}
									tree.setSelection(result);
									highlightTransitionOrState(result);
								}
							});
						}


						menu.setLocation (tree.toDisplay(point));
						menu.setVisible (true);
						while (!menu.isDisposed () && menu.isVisible ()) {
							if (!app.getDisplay().readAndDispatch ()) app.getDisplay().sleep ();
						}
						menu.dispose ();
					}
				}
			}
		});
	}
	
	private void showTransitionOrState(TreeItem item) {
		CompoundTransition<? extends CompoundState> t = (CompoundTransition<? extends CompoundState>)item.getData("trans");
		CompoundTransition<? extends CompoundState> tt = (CompoundTransition<? extends CompoundState>)item.getData("tracetrans");
		String id = (String)item.getData("id");
		String parentId = (item.getParentItem()!=null?
				(String)item.getParentItem().getData("id"):
				"-");
		if (t != null)
			simulator.showTransition(t, tt, parentId, id);
		else
			showInitialState(item);
	}
	
	private void highlightTransitionOrState(TreeItem item) {
		String id = (String)item.getData("id");
		simulator.showTransitionPath(id);
		simulator.mcastWrite("tree "+id+"\n");
	}
	
	private void showInitialState(TreeItem item) {
	}

	
	private boolean isParent(TreeItem potential, TreeItem child) {
		if (potential==null||child==null)
			return false;
		TreeItem parent = child.getParentItem();
		while(parent != null && parent != potential)
			parent = parent.getParentItem();
		return (parent == potential);
	}
	
	private void unfold(TreeItem item) {
		if (!running)
			return;
		System.out.println ("Unfold: "+item);
		Iterator<?extends CompoundTransition<?extends CompoundState>> it = ((CompoundState)item.getData("state")).menu(interp.isAny());
		simulator.noShow();
		while(it.hasNext()) {
			addChild(item, it.next());
		}
		item.setData("unfolded", true);
		item.setExpanded(true);
		showTransitionOrState(item);
		simulator.yesShow();
	}
	
	public void beginUnfold(CompoundState s, String id){
		TreeItem item = findItem(id);
		if (item!=null)
			System.out.println ("beginUnfold "+id+" : found");
		else
			System.out.println ("beginUnfold "+id+" : NOT found");
		simulator.noShow();
	}
	public String addChild(CompoundState s, String id, CompoundTransition<? extends CompoundState> mt, CompoundTransition<? extends CompoundState> tt) {
		TreeItem item = findItem(id);
		if (item==null) {
			System.out.println ("SimTreePane: addChild: no match found for id="+id);
			return null;
		}
		return addChild(item, mt, tt);
	}
	public void endUnfold(final CompoundState s, final String id) {
		if (app.getDisplay().getThread()==Thread.currentThread())
			doEndUnfold(s, id);
		else {
			app.getDisplay().syncExec (new Runnable () {
				public void run () {
					doEndUnfold(s, id);
				}
			});
		}
	}

	private void doEndUnfold(CompoundState s, String id) {
		final TreeItem item = findItem(id);
		if (item!=null) {
			item.setData("unfolded", true);
			item.setExpanded(true);
			// no need to: showTransitionOrState(item): already shown in simulator
			simulator.yesShow();
			System.out.println ("endUnfold "+id+" : found");
		} else {
			System.out.println ("endUnfold "+id+" : NOT found");
		}
	}
	
	private TreeItem findItem(String id) {
		return mapIdToItem.get(id);
	}


	
	private Listener mouseListener = new Listener() {

        public void handleEvent(Event e) {

            String string = "UNKNOWN";

            switch (e.type) {

                case SWT.MouseDown: string = "DOWN"; break;

                case SWT.MouseUp: string = "UP"; break;

                case SWT.MouseMove: string = "MOVE"; break;

                case SWT.MouseDoubleClick:

                    string = "DOUBLE";

                    break;

                case SWT.MouseEnter: string="ENTER"; break;

                case SWT.MouseExit: string = "EXIT"; break;

                case SWT.MouseHover: string="HOVER"; break;

            }

            string += ": stateMask=0x"

                + Integer.toHexString(e.stateMask);

            if ((e.stateMask & SWT.CTRL) != 0)

                string += " CTRL";

            if ((e.stateMask & SWT.ALT) != 0)

                string += " ALT";

            if ((e.stateMask & SWT.SHIFT) != 0)

                string += " SHIFT";

            if ((e.stateMask & SWT.COMMAND) != 0)

                string += " COMMAND";

            if ((e.stateMask & SWT.BUTTON1) != 0)

                string += " BUTTON1";

            if ((e.stateMask & SWT.BUTTON2) != 0)

                string += " BUTTON2";

            if ((e.stateMask & SWT.BUTTON3) != 0)

                string += " BUTTON3";

            string += ", button=0x"

                + Integer.toHexString(e.button);

            string += ", x=" + e.x + ", y=" + e.y;

            System.out.println(string);

        }

    };

	
	private Testgui app;

	private Composite simtreeGroup = null;
	private Tree tree = null;
	private Interpretation interp = new AnyInterpretation();
	private Simulator simulator = null;

	private Map<CompoundState, TreeItem[]> mapStateItems = null;
	private Map<String, TreeItem> mapIdToItem = null;
	private long nextNode = 0;
	private boolean running = false;
}
