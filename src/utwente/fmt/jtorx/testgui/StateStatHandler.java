package utwente.fmt.jtorx.testgui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.lts.State;
import utwente.fmt.lts.SuspensionAutomatonState;

public class StateStatHandler {

	public void runBegins(DriverInitializationResult i) {
		map.clear();
		nextnr = 0;
		add(i.getState());
	}
	//TODO avoid issue instead of dealing with it here
	public void step(DriverInterActionResult i) {
		if (i!=null)
			add(i.getDst());
	}

	private void add(State s) {
		if(forReal) {
			if (! map.containsKey(s)) {
				map.put(s, nextnr);
				nextnr++;
			}

			if (s instanceof SuspensionAutomatonState ) {
				SuspensionAutomatonState sas = (SuspensionAutomatonState) s;
				System.err.println("STATEID super="+map.get(s)+
						" init: "+toSortedString(sas.getDirectSubStates())+
						" trans: "+toSortedString(sas.getIndirectSubStates()));
			} else
				System.err.println("STATEID super="+map.get(s));
		}
	}
	
	private String toSortedString(Iterator<? extends State> it) {
		ArrayList<String> a = new ArrayList<String>();
		String res = "";
		
		while (it.hasNext())
			a.add(it.next().getID());
		Collections.sort(a);

		for (String s: a)
			res  += " "+s;
		return res;
	}

	public StateStatHandler() {

	}
	

	private Map<State,Integer> map = new HashMap<State,Integer>();
	private int nextnr = 0;
	private boolean forReal = false;
	
}
