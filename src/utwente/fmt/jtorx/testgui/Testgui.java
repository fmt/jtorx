package utwente.fmt.jtorx.testgui;

import iocochecker.FailureSituation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.moioli.swtloader.SWTLoader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import utwente.fmt.Version;
import utwente.fmt.engine.Testrun.RunState;
import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.InterpKind;
import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.engine.Engine;
import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.iocochecker.IocoChecker;
import utwente.fmt.jtorx.iocochecker.IocoCheckerFailure;
import utwente.fmt.jtorx.iocochecker.IocoCheckerResult;
import utwente.fmt.jtorx.iocochecker.IocoCheckerWrapper;
import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverFinalizationEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverInitializedEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.DriverEvents.DriverSUTInteractionEvent;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.EventCategory;
import utwente.fmt.jtorx.lazyotf.remoteimpl.RemoteLazyOTF;
import utwente.fmt.jtorx.lazyotf.utilities.Observer;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverMethod;
import utwente.fmt.jtorx.logger.anidot.Anidot;
import utwente.fmt.jtorx.logger.anidot.DotLTS;
import utwente.fmt.jtorx.logger.coverage.CoverageData;
import utwente.fmt.jtorx.logger.logfile.LogWriter;
import utwente.fmt.jtorx.logger.mux.LogMux;
import utwente.fmt.jtorx.logger.mux.LogServices;
import utwente.fmt.jtorx.simulator.Simulator;
import utwente.fmt.jtorx.testgui.RunItemData.AdapterKind;
import utwente.fmt.jtorx.testgui.lazyotf.config.LazyOTFConfigCtrl;
import utwente.fmt.jtorx.testgui.lazyotf.config.LazyOTFConfigCtrl.ConfigPaneAccessor;
import utwente.fmt.jtorx.testgui.lazyotf.config.LazyOTFConfigPane;
import utwente.fmt.jtorx.testgui.lazyotf.testrun.LazyOTFIntrospection;
import utwente.fmt.jtorx.testgui.lazyotf.testrun.LazyOTFIntrospectionImpl;
import utwente.fmt.jtorx.testgui.lazyotf.testrun.TestguiAccessor;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.torx.DriverInterActionResultConsumer;
import utwente.fmt.jtorx.torx.InterActionResult;
import utwente.fmt.jtorx.torx.LabelPlusVerdict;
import utwente.fmt.jtorx.torx.driver.VerdictImpl;
import utwente.fmt.jtorx.torx.explorer.guide.GuidanceLTS;
import utwente.fmt.jtorx.torx.primer.Primer;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.FailureReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.ui.StepHighlighter;
import utwente.fmt.jtorx.utils.ChildStarter;
import utwente.fmt.jtorx.utracechecker.UtraceChecker;
import utwente.fmt.jtorx.utracechecker.UtraceCheckerResult;
import utwente.fmt.jtorx.writers.AniDotLTSWriter;
import utwente.fmt.jtorx.writers.AutWriter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.LTS.LTSException;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.test.Verdict;

public class Testgui {

	// methods to interface with GUI components in other classes

	public enum BrowseAction {
		REPLACE,
		INSERT,
	}

	public enum LayoutMode {
		UNSPECIFIED,
		PORTRAIT,
		LANDSCAPE,
	}


	public Display getDisplay() {
		return display;
	}

	public void changeFontSize(int inc) {
		fontDiff += inc;
		menubar.setEnabledFontNormalItem(fontDiff != 0);
		changeFontSize(toplevelShell, inc);

		testPaneContainer.layout();
		iococheckPaneContainer.layout();
		utracescheckPaneContainer.layout();
		coveragePaneContainer.layout();
		paneContainer.layout();

		toplevelShell.layout(true, true);
		//toplevelShell.redraw();
	}
	public void resetFontSize() {
		menubar.setEnabledFontNormalItem(false);
		changeFontSize(toplevelShell, -fontDiff);
		fontDiff = 0;

		testPaneContainer.layout();
		iococheckPaneContainer.layout();
		utracescheckPaneContainer.layout();
		coveragePaneContainer.layout();
		paneContainer.layout();

		toplevelShell.layout(true, true);
		//toplevelShell.redraw();
	}


	private void changeFontSize(Composite p, int inc) {
		Vector<Composite> work = new Vector<Composite>();
		Map<Font,Font> replacement = new HashMap<Font,Font>();

		work.add(p);
		while(work.size() > 0) {
			Composite cur = work.firstElement();
			work.remove(0);

			for(Control c: cur.getChildren()) {
				if (c instanceof Composite)
					work.add((Composite) c);

				Font f = c.getFont();
				Font g = replacement.get(f);
				if (g==null) {
					FontData[] fd = f.getFontData();
					if (fd.length >= 1) {
						g = new Font(f.getDevice(), fd[0].getName(), fd[0].getHeight()+inc, fd[0].getStyle());
						fontsToBeDisposed.add(g);
						System.err.println("changeFontSize "+c+" f="+f+" height="+fd[0].getHeight()+" -> g="+g);
						replacement.put(f,g);
					}
				}
				if(g!=null) {
					// System.err.println("changeFontSize "+c+" f="+f+" => g="+g);
					c.setFont(g);
					c.pack();
				} else
					System.err.println("changeFontSize internal error "+c+" g=null f="+f);
			}
			cur.pack();
			cur.layout(true);
		}
		for(Font f: replacement.keySet())
			if (fontsToBeDisposed.contains(f)) { // avoid removing default fonts: they may be used by newly created panes
				f.dispose(); // no need to check whether f is in values set
				fontsToBeDisposed.remove(f);
			}
	}

	public ErrorReporter getErrorReporter() {
		return err;
	}
	public ProgressReporter getProgressReporter() {
		return progress;
	}
	public ProgressReporter getStateSpaceProgressReporter() {
		return stateSpaceProgress;
	}

	public LogMux getLogMux() {
		return logMux;
	}
	public LogServices getLogServices() {
		return logServices;
	}


	public void runAbout() {
		addStatusMsg("");
		MessageBox messageBox = new MessageBox(toplevelShell, SWT.OK);

		int swtVersion = SWT.getVersion();
		String swtVersionString = "";
		swtVersionString += swtVersion/1000;
		swtVersionString += "."+(swtVersion%1000)/100;
		swtVersionString += "."+(swtVersion%100);
		String messageText = "Version: "+Version.getVersionAndDate()+"\n"+
		"See: http://fmt.cs.utwente.nl/tools/jtorx/"+"\n\n"+
		"Copyright (c) Axel Belinfante 2008-2012."+"\n"+
		"All rights reserved." +"\n\n\n" +
		"SWT:"+"\n" +
		"\tplatform: "+SWT.getPlatform()+"\n"+
		"\tversion: "+swtVersionString
		;

		messageBox.setText("JTorX");
		messageBox.setMessage(messageText);
		messageBox.open();
	}

	public void runPreferences() {
		// currently preferences menu is disabled;
		// when we add content here, we should also enable
		// the pref menu entry in CarbonUIEnhancer.java
	}

	public void done() {
		cleanup();
		System.exit(0); 
	}
	
	
	// ============================================
	
	public class Testrun  implements utwente.fmt.engine.Testrun, utwente.fmt.configuration.Testrun {
		
		// ====================================
		// To implement interface engine.Testrun
		
		public void addStatusMsg(String s) {
			app.addStatusMsg(s);
		}
		public ProgressReporter getStateSpaceProgressReporter() {
			return app.getStateSpaceProgressReporter();
		}
		public void runSetup() {
			for(JTorXPane p: app.testPanes)
				p.runSetup();
		}
		public long parseSeed(String s, String e) {
			return app.parseSeed(s, e);
		}
		public long initSeed(String s) {
			return app.initSeed(s);
		}
		public long initSimSeed(String s, long l) {
			return app.initSimSeed(s, l);
		}
		public void prepareLogMuxForSession() {
			logMux.prepareForSession();
		}
		public void startLogMuxSession(RunSessionData rsd) {
			logMux.startSession(rsd, runId);
		}
		public void abortTestrun() {
			app.abortTestrun();
		}
	
		// called from non-gui thread
		
		public void runBegins() {
			display.syncExec (new Runnable () {
				public void run () {
					for(JTorXPane p: app.testPanes)
						p.runBegins();

					if(useLazyOTF) {
						/* Create LazyOTF window
						 * TODO: Might be out of place here, maybe make the window a JTorXPane,
						 * hide it after creation and show it via p.runBegins()?
						 */
						
						TestguiAccessor acc = new TestguiAccessor() {
							@Override
							public void handleGivenStimulusEvent(Label stim) {
								app.handleGivenStimulusEvent(stim);
							}
							@Override
							public void handleObserveEvent() {
								app.handleObserveEvent();
							}
							@Override
							public void updateModelMenu() {
								app.updateModelMenu();
							}	
						};
						
						LazyOTF lotfInstance = runData.getLazyOTFInstance();
						TorXInterfaceExtensions tif = null;
						if(lotfInstance instanceof RemoteLazyOTF) {
							RemoteLazyOTF lvs = (RemoteLazyOTF)lotfInstance;
							tif = lvs.getTorXInterfaceExtensions();
						}
						
						final LazyOTFIntrospection traversalViz =
								LazyOTFIntrospectionImpl.makeInstance(runData.getLazyOTFInstance(),
										(LazyOTFAwareSimOnlineTestingDriver)runState.myDriver,
										acc, tif);
					    /* Glue between driver and test visualisation */
					    ((LazyOTFAwareSimOnlineTestingDriver)runState.myDriver).addObserver(new Observer() {
							@ObserverMethod
							public void onSUTInteraction(
									DriverSUTInteractionEvent eir) {
								traversalViz.registerInteractionResult(eir.interactionResult);
							}

							@ObserverMethod
							public void onInit(
									DriverInitializedEvent dsir) {
								traversalViz.init();
							}

							@ObserverMethod
							public void onFinish(DriverFinalizationEvent dfr) {
								traversalViz.finish();
							}
					    }, EventCategory.POST_POLICY_PHASE);
					    traversalViz.init();
					}
				}
			});
		}

		public void stopTestrun() {
			display.syncExec (new Runnable () {
				public void run () {
					app.stopTestrun();
				}
			});
		}
		public void addLogger(final boolean inWindow, final String t) {
			display.syncExec (new Runnable () {
				public void run () {
					if (inWindow)
						addLogPane(t);
					else
						addStdoutLogger(t);
				}
			});
		}
		public void addCoverageLogger(CoverageData coverage) {
			logMux.addAnimator(coverage);
		}
		public void showMenu() {
			display.syncExec (new Runnable () {
				public void run () {
					printModelMenu(true);
				}
			});
		}
		public void showVerdict(final Verdict verdict) {
			display.syncExec (new Runnable () {
				public void run () {
					testrunPane.setVerdict(verdict);
				}
			});
		}
		public void showCoverage(final CoverageData coverage) {
			display.syncExec (new Runnable () {
				public void run () {
					coveragePane.setCoverage(coverage);
				}
			});
		}
	
		// ====================================
		// To implement interface configuration.Testrun
		public IOLTSInterpretation getInterpretation() {
			return app.configPane.getInterpretation();
		}
		public boolean useGuide() {
			return app.configPane.useGuide();
		}
		public boolean useCoverage() {
			return app.configPane.getUseCoverage();
		}
		public boolean useModelInstantiator() {
			return app.configPane.useModelInstantiator();
		}
		public String getInstModelFilename() {
			return app.configPane.getInstModelFilename();
		}
		public boolean useImplInstantiator() {
			return app.configPane.useImplInstantiator();
		}
		public String getInstImplFilename() {
			return app.configPane.getInstImplFilename();
		}
		public String getModelFilename() {
			return app.getModelFilename();
		}
		public String getGuideFilename() {
			return app.configPane.getGuideFilename();
		}
		public String getImplFilename() {
			return app.getImplFilename();
		}
		public RunItemData.LTSKind getGuideKind() {
			return app.configPane.getGuideKind();
		}
		public RunItemData.AdapterKind getImplKind() {
			return app.configPane.getImplKind();
		}
		public TraceKind getTraceKind() {
			return app.configPane.getTraceKind();
		}
		public int getTimeoutValue() {
			return app.configPane.getTimeoutValue();
		}
		public TimeUnit getTimeoutUnit() {
			return app.configPane.getTimeoutUnit();
		}
		public LTS getSelectedLTS() {
			return app.iocoCheckerFailuresPane.getSelectedLTS();
		}
		public LTS getCombinedLTS() {
			return app.iocoCheckerFailuresPane.getCombinedLTS();
		}
		public boolean useImplRealTime() {
			return app.configPane.useImplRealTime();
		}
		public boolean getStripAddLabelAnno() {
			return app.configPane.getStripAddLabelAnno();
		}
		public InterpKind getInterpKind() {
			return app.configPane.getInterpKind();
		}
		public boolean getReconDelta() {
			return app.configPane.getReconDelta();
		}
		public boolean getSynthDelta() {
			return app.configPane.getSynthDelta();
		}
		public boolean getAngelicCompletion() {
			return app.configPane.getAngelicCompletion();
		}
		public boolean getDivergence() {
			return app.configPane.getDivergence();
		}

		public String getInterpInputs() {
			return app.configPane.getInterpInputs();
		}
		public String getInterpOutputs() {
			return app.configPane.getInterpOutputs();
		}
		public String getSeed() {
			return app.testrunPane.getSeed();
		}
		public void setSeed(String s) {
			app.testrunPane.setSeed(s);
		}
		public String getSimSeed() {
			return app.testrunPane.getSimSeed();
		}
		public void setSimSeed(String s) {
			app.testrunPane.setSimSeed(s);
		}
		public boolean vizModel() {
			return app.testrunPane.vizModel();
		}
		public boolean vizMsc() {
			return app.testrunPane.vizMsc();
		}
		public boolean vizRun() {
			return app.testrunPane.vizRun();
		}
		public boolean vizLogPane() {
			return app.testrunPane.vizLogPane();
		}
		public boolean vizSuspAutom() {
			return app.testrunPane.vizSuspAutom();
		}
		public boolean vizGuide() {
			return app.testrunPane.vizGuide();
		}
		public boolean vizImpl() {
			return app.testrunPane.vizImpl();
		}
		public boolean vizCoverage() {
			return app.testrunPane.vizCoverage();
		}

		public boolean isLazyOTFEnabled() {
			return app.isLazyOTFEnabled();
		} 

		public LazyOTFConfig getLazyOTFConfig() {
			return app.getLazyOTFConfig();
		}

		//========= specific for use here
		
		public String getRunId() {
			return runId;
		}
		
		//========= constructor and private data
		public Testrun(Testgui app) {
			this.app = app;
			this.runId = ""+(logPaneCount-1);
		}
		private Testgui app;
		private String runId;
	}
	
	
	// ============================================

	// file dialog box
	public String openConfigFileDialog(String title, String filterPath) {
		addStatusMsg("");
		configFileDialog.setText(title);
		configFileDialog.setFilterPath(filterPath);
		String fileName = configFileDialog.open();
		return fileName;
	}
	public String saveConfigFileDialog(String title, String filterPath) {
		addStatusMsg("");
		configSaveDialog.setText(title);
		configSaveDialog.setFilterPath(filterPath);
		String fileName = configSaveDialog.open();
		return fileName;
	}
	
	public void loadConfigFileDialog() {
		// configPane.openConfigFileDialog();
		String s  = openConfigFileDialog("Load Configuration", "");
		loadConfig(runData, s, false);
	}
	public void saveConfigFileDialog() {
		String s = saveConfigFileDialog("Save Configuration", "");
		saveConfig(runData, s);		
	}
	
	public String openLogDialog(String title, String filterPath) {
		addStatusMsg("");
		logDialog.setText(title);
		logDialog.setFilterPath(filterPath);
		String fileName = logDialog.open();
		return fileName;
	}
	public String saveLogDialog(String title, String filterPath) {
		addStatusMsg("");
		logSaveDialog.setText(title);
		logSaveDialog.setFilterPath(filterPath);
		String fileName = logSaveDialog.open();
		return fileName;
	}

	public void loadLogForReplayDialog() {
		String s  = openLogDialog("Load Log for re-play", "");
		loadConfig(runData, s, true);
	}
	
	public void saveLogDialog() {
		String s = saveLogDialog("Save Log", "");
		saveConfig(runData, s);		
	}

	
	public String openFileDialog(String title, String filterPath) {
		addStatusMsg("");
		fileDialog.setText(title);
		fileDialog.setFilterPath(filterPath);
		String fileName = fileDialog.open();
		return fileName;
	}
	public String saveFileDialog(String title, String filterPath) {
		addStatusMsg("");
		saveDialog.setText(title);
		saveDialog.setFilterPath(filterPath);
		String fileName = saveDialog.open();
		return fileName;
	}


	public void openModelFileDialog() {
		configPane.openModelFileDialog();
	}
	public void openGuideFileDialog() {
		configPane.openGuideFileDialog();
	}
	public void openImplementationFileDialog() {
		configPane.openImplementationFileDialog();
	}
	public String getModelFilename() {
		return configPane.getModelFilename();
	}
	public String getImplFilename() {
		return configPane.getImplFilename();
	}


	public IOLTSInterpretation getInterpretation() {
		return configPane.getInterpretation();
	}

	public TraceKind getTraceKind() {
		return configPane.getTraceKind();
	}

	public boolean getSynthDelta() {
		return configPane.getSynthDelta();
	}

	public boolean getAngelicCompletion() {
		return configPane.getAngelicCompletion();
	}
	
	public boolean getDivergence() {
		return configPane.getDivergence();
	}

	public RunItemData getModelData() {
		return modelData;
	}
	public RunItemData getGuideData() {
		return guideData;
	}
	public RunItemData getImplData() {
		return implData;
	}
	public RunItemData getModelPrimerData() {
		return modelPrimerData;
	}
	public RunItemData getGuidePrimerData() {
		return guidePrimerData;
	}

	public RunItemData getCombinatorData() {
		return combinatorData;
	}

	public void registerDotVizButton(Button b) {
		dotVizButtons.add(b);
	}
	public void registerMscVizButton(Button b) {
		mscVizButtons.add(b);
	}


	public LTS doHandleViewGuideEvent() {
		RunItemData.LTSKind cfgKind = configPane.getGuideKind();
		if (cfgKind == RunItemData.LTSKind.MODEL) {
			String filename = configPane.getGuideFilename();
			return doHandleViewEvent(guideData, filename, cfgKind, LogMux.Kind.GUIDE);
		} else if (cfgKind == RunItemData.LTSKind.TRACE) {
			String filename = configPane.getGuideFilename();
			return doHandleViewEvent(guideData, filename, cfgKind, LogMux.Kind.GUIDE);
		} else if (cfgKind == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE) {
			LTS lts = iocoCheckerFailuresPane.getSelectedLTS();
			if (lts==null)
				return null;
			String filename = "";
			return doHandleViewLTSEvent(guideData, lts, filename, "iocoCheckerSelectedTrace", LogMux.Kind.GUIDE);
		} else if (cfgKind == RunItemData.LTSKind.IOCOCHECKERMULTITRACE) {
			LTS lts = iocoCheckerFailuresPane.getCombinedLTS();
			if (lts==null)
				return null;
			String filename = "";
			return doHandleViewLTSEvent(guideData, lts, filename, "iocoCheckerCombinedTrace", LogMux.Kind.GUIDE);
		}
		return null;
	}
	public void doHandleSaveGuideEvent() {
		if (configPane.getGuideKind() == RunItemData.LTSKind.MODEL) {
			// error
		} else if (configPane.getGuideKind() == RunItemData.LTSKind.TRACE) {
			// error
		} else if (configPane.getGuideKind() == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE) {
			LTS lts = iocoCheckerFailuresPane.getSelectedLTS();
			if (lts != null) {
				String filename = saveFileDialog("Save selected iocoChecker failure LTS as", "");
				saveLTS(lts, filename, "Guidance");
			}
		} else if (configPane.getGuideKind() == RunItemData.LTSKind.IOCOCHECKERMULTITRACE) {
			LTS lts = iocoCheckerFailuresPane.getCombinedLTS();
			if (lts != null) {
				String filename = saveFileDialog("Save combined iocoChecker failure LTS as", "");
				saveLTS(lts, filename, "Guidance");
			}
		}
	}

	public void saveSelectedUtrace() {
		LTS lts = utracesCheckFailuresPane.getSelectedLTS();
		if (lts != null) {
			String filename = saveFileDialog("Save utraceChecker failure LTS as", "");
			saveLTS(lts, filename, "UtraceChecker failure");
		}
	}

	public void saveSelectedIocoCheckerTrace() {
		LTS lts = iocoCheckerFailuresPane.getSelectedLTS();
		if (lts != null) {
			String filename = saveFileDialog("Save iocoChecker failure LTS as", "");
			saveLTS(lts, filename, "IocoChecker failure");
		}
	}


	private void saveConfig(RunSessionData d, String fname) {
		if (fname==null || fname.trim().equals(""))
			return;
		if (d==null)
			return;

		FileOutputStream fout;
		try {
			fout = new FileOutputStream (fname);
			PrintStream tout = System.err;
			BufferedWriter w = new BufferedWriter(new OutputStreamWriter(fout));
			BufferedWriter t = null;
			if (debug) {
				t = new BufferedWriter(new OutputStreamWriter(tout));
			}
			Config c = new Config();
			if (debug) {
				tout.println("saveConfig after init ---------------");
				c.twrite(t);
				tout.println("saveConfig done after init ---------------");
				System.err.println("saveConfig: init TIMEOUTVAL: int: "+c.getInt("TIMEOUTVAL"));
				System.err.println("saveConfig: init TIMEOUTVAL: string: "+c.getString("TIMEOUTVAL"));
				System.err.println("saveConfig: init INTERPKIND: string: "+c.getString("INTERPKIND"));
			}
			d.fillConfig(c);
			if (debug) {
				tout.println("saveConfig after RunSessionData ---------------");
				c.twrite(t);
				tout.println("saveConfig done after RunSessionData ---------------");
				System.err.println("saveConfig: d: TIMEOUTVAL: int: "+c.getInt("TIMEOUTVAL"));
				System.err.println("saveConfig: d: TIMEOUTVAL: string: "+c.getString("TIMEOUTVAL"));
				System.err.println("saveConfig: d INTERPKIND: string: "+c.getString("INTERPKIND"));
			}
			configPane.fillConfig(c);
			if (debug) {
				tout.println("saveConfig after configPane ---------------");
				c.twrite(t);
				tout.println("saveConfig done after configPane ---------------");
				System.err.println("saveConfig: cfg: TIMEOUTVAL: int: "+c.getInt("TIMEOUTVAL"));
				System.err.println("saveConfig: cfg: TIMEOUTVAL: string: "+c.getString("TIMEOUTVAL"));
				System.err.println("saveConfig: d-val: TIMEOUTVAL: "+d.getTimeoutValue());
				System.err.println("saveConfig: cfg INTERPKIND: string: "+c.getString("INTERPKIND"));
			}
			testrunPane.fillConfig(c);
			if (debug) {
				tout.println("saveConfig after testrunPane ---------------");
				c.twrite(t);
				tout.println("saveConfig done after testrunPane ---------------");
				System.err.println("saveConfig: test TIMEOUTVAL: int: "+c.getInt("TIMEOUTVAL"));
				System.err.println("saveConfig: test TIMEOUTVAL: string: "+c.getString("TIMEOUTVAL"));
				System.err.println("saveConfig: test INTERPKIND: string: "+c.getString("INTERPKIND"));
			}
			
			if(this.isLazyOTFEnabled()) {
				c.add("LAZYOTF", true);
				lazyOTFcontroller.fillConfig(c);
				if(debug) {
					tout.println("saveConfig after lazyOTFController ---------------");
					c.twrite(t);
					tout.println("saveConfig done after lazyOTFController ---------------");
					System.err.println("saveConfig: test LAZYOTFCFG: " + c.getString("LAZYOTFCFG"));
				}
				addStatusMsg("written configuration to: "+fname);
			}
			else {
				c.add("LAZYOTF", false);
			}

			c.write(w);
			fout.close();
			addStatusMsg("written configuration to: "+fname);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			addStatusMsg("cannot write configuration: "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			addStatusMsg("cannot write configuration: "+e.getMessage());
			e.printStackTrace();
		}
	}

	private void loadConfig(RunSessionData d, String fname, boolean loadingLogForReplay) {
		if (fname==null || fname.trim().equals(""))
			return;
		if (d==null)
			return;

		FileInputStream fin;
		try {
			fin = new FileInputStream (fname);
			BufferedReader r = new BufferedReader(new InputStreamReader(fin));
			Config c = new Config();
			c.read(r);
			fin.close();
			if (loadingLogForReplay) { // tweak log settings to use log as SUT
				c.add("ADAPTERKIND", AdapterKind.LOGADAPTER);
				c.add("IMPLTEXT", fname);
				// should we disable things like impl instantiator?
			}
			//d.loadConfig(c);
			configPane.loadConfig(c);
			testrunPane.loadConfig(c);
			if(c.getBool("LAZYOTF") == true) {
				setUseLazyOTF(true);
				this.lazyOTFcontroller.loadConfig(c);
			}
			addStatusMsg("read configuration from: "+fname);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			addStatusMsg("cannot read configuration: "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			addStatusMsg("cannot read configuration: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	private void saveLTS(LTS lts, String fname, String kind) {
		if (fname==null || fname.trim().equals(""))
			return;

		FileOutputStream fout;
		try {
			fout = new FileOutputStream (fname);
			BufferedWriter w = new BufferedWriter(new OutputStreamWriter(fout));
			AutWriter aw = new AutWriter(lts, new AnyInterpretation(), w);
			aw.write();
			w.flush();
			fout.close();
			addStatusMsg("written "+kind+" lts to: "+fname);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			addStatusMsg("cannot write "+kind+" lts: "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			addStatusMsg("cannot write "+kind+" lts: "+e.getMessage());
			e.printStackTrace();
		}
	}

	public void doHandleViewModelEvent() {
		String filename = configPane.getModelFilename();
		doHandleViewEvent(modelData, filename, RunItemData.LTSKind.MODEL, LogMux.Kind.MODEL);
	}
	public void doHandleViewImplEvent() {
		String filename = configPane.getImplFilename();
		doHandleViewEvent(implData, filename, RunItemData.LTSKind.MODEL, LogMux.Kind.IMPL);
	}
	public LTS doHandleViewEvent(RunItemData rid, String fileName, RunItemData.LTSKind cfgKind, LogMux.Kind kind) {
		if (false && configPane.getInterpretation() == null) {
			addStatusMsg("choose interp");
			return null;
		}
		addStatusMsg("");
		if (! fileName.equals("")) {
			LTS lts = rid.readModelFile(fileName, err, progress);
			if (cfgKind==RunItemData.LTSKind.TRACE)
				lts = new GuidanceLTS(lts);
			return doHandleViewLTSEvent(rid, lts, fileName, titleOf(new File(fileName)), kind);
		}
		return null;
	}
	private LTS  doHandleViewLTSEvent(RunItemData rid, LTS lts, String fileName, String title, LogMux.Kind kind) {
		if (false && configPane.getInterpretation() == null) {
			addStatusMsg("choose interp");
			return null;
		}
		addStatusMsg("");

		if (lts == null)
			return null;
		if (rid.getAnidot() != null)
			rid.getAnidot().end();


		String gv_filename = rid.getGraphVizFilename(fileName, err);

		Anidot anidot = null;

		if (gv_filename!=null) {
			anidot = logMux.newAnidot(title, kind, gv_filename);
			if (anidot != null) {
				anidot.end();
				return null;
			}
		} 

		anidot = logMux.newAnidot(title, kind);
		if (anidot != null) {
			Iterator<?extends State> it = null;
			try {
				it = lts.init();
				rid.setFilename(fileName, "");
				rid.setLTS(lts);
				DotLTS dotLTS = new DotLTS(anidot);
				anidot.setRender(Anidot.RenderMode.OFF);
				new AniDotLTSWriter(lts, interpAny, dotLTS, getStateSpaceProgressReporter() ).write();
				dotLTS.addRoot(title, lts.getRootPosition());
				// dotLTS.addInvisibleRoot(); // hiding the node does not work

				if (it==null)
					return null;
				while(it.hasNext())
					dotLTS.addInitialEdge(it.next());
				anidot.setRender(Anidot.RenderMode.ON);
			} catch (LTSException e) {
				System.err.println("doHandleViewLTSEvent: caught LTSException: "+e.getMessage());
			}

			anidot.end();
			// rid.clear();
		}
		return lts;

	}


	public void doHandleSimulateLTSEvent(RunItemData rid, LogMux.Kind kind) {
		if (false && configPane.getInterpretation() == null) {
			addStatusMsg("choose interp");
			return;
		}

		if (!processModelConfig(rid, kind))
			return;

		addStatusMsg("");
		RunSessionData simRunData = new RunSessionData();
		simRunData.setModelData(modelData);
		simRunData.setModelPrimerData(modelPrimerData);
		simRunData.setGuideData(guideData);
		simRunData.setGuidePrimerData(guidePrimerData);
		simRunData.setCombinatorData(combinatorData);
		simRunData.setImplData(implData);

		System.err.println("simulator: about to startSimulator: "+rid.getLTS());
		System.err.println("simulator: about to startSimulator...");
		startSimulator(rid.getLTS(), kind, simRunData, null);

		//		if (rid.getCompoundLTS() == null) // || rid.getLTS() == null)
		//			startSimulator(rid.getLTS(), kind, simRunData, null);
		//		else {
		//			startSimulator(rid.getCompoundLTS(), kind, simRunData, null);
		//			simRunData.setVizSuspAut(true); 
		//		}
		return;
	}
	public void doHandleSimulateModelPrimerEvent(RunItemData rid, LogMux.Kind kind) {

		if (!processModelConfig(rid, kind))
			return;

		LTS lts = modelData.getLTS();
		IOLTSInterpretation cfgInterp = getInterpretation();
		TraceKind traceKind = getTraceKind();

		missing.reset();
		missing.check(lts == null, "no model lts");
		missing.check(cfgInterp == null, "no interpretation");
		missing.check(traceKind == null, "no trace kind");
		if (!missing.getMissing().equals("")) {
			getErrorReporter().report("Primver: View: "+missing.getMissing());
			return;
		}


		boolean tracesOnly = !getSynthDelta() /*tracesOnly*/;
		boolean angelicCompletion = getAngelicCompletion();
		boolean divergence = getDivergence();

		final IOLTSInterpretation interp = cfgInterp;
		CompoundLTS clts = 
			new Primer(lts, interp,
					tracesOnly,
					true/*remember states for matching*/,
					traceKind,
					angelicCompletion,
					divergence,
					getErrorReporter(), getProgressReporter());

		modelPrimerData.setLTS(clts);

		addStatusMsg("");
		RunSessionData simRunData = new RunSessionData();
		simRunData.setModelData(modelData);
		simRunData.setModelPrimerData(modelPrimerData);
		simRunData.setGuideData(guideData);
		simRunData.setGuidePrimerData(guidePrimerData);
		simRunData.setCombinatorData(combinatorData);
		simRunData.setImplData(implData);

		startSimulator(clts, kind, simRunData, null);

		//		if (rid.getCompoundLTS() == null) // || rid.getLTS() == null)
		//			startSimulator(rid.getLTS(), kind, simRunData, null);
		//		else {
		//			startSimulator(rid.getCompoundLTS(), kind, simRunData, null);
		//			simRunData.setVizSuspAut(true); 
		//		}
		return;
	}


	public void simulateSelectedUtrace() {
		if (false && configPane.getInterpretation() == null) {
			addStatusMsg("choose interp");
			return;
		}
		addStatusMsg("");
		LTS selected = utracesCheckFailuresPane.getSelectedLTS();
		LTS lts = utracesCheckFailuresPane.getLTS();
		String fileName = utracesCheckFailuresPane.getFileName();
		if (lts==null) {
			addStatusMsg("no utraces failure to select");
			return;
		} else if (selected==null) {
			addStatusMsg("no utraces failure selected");
			return;
		}
		// TODO make sure that what we do in next line is identical to what we did when we run the Utraces check
		boolean tracesOnly = !getSynthDelta() /*tracesOnly*/;
		boolean angelicCompletion = getAngelicCompletion();
		boolean divergence = getDivergence();

		CompoundLTS clts = new Primer(lts, configPane.getInterpretation(), tracesOnly, true, TraceKind.STRACES, angelicCompletion, divergence, err, progress);


		RunSessionData simRunData = new RunSessionData();
		RunItemData simItemData = new RunItemData();
		RunItemData traceItemData = new RunItemData();
		simItemData.setFilename(fileName, "");
		simItemData.setLTSKind(RunItemData.LTSKind.MODEL);
		simItemData.setLTS(lts);

		traceItemData.setFilename(fileName, "");
		traceItemData.setLTSKind(RunItemData.LTSKind.TRACE);
		traceItemData.setLTS(selected);
		traceItemData.setTitle("utraceChecker trace");


		simRunData.setModelData(simItemData);
		simRunData.setModelPrimerData(null);
		simRunData.setGuideData(null);
		simRunData.setGuidePrimerData(null);
		simRunData.setCombinatorData(null);
		simRunData.setImplData(null);

		simRunData.setModelLTS(simItemData.getLTS());

		File modelFile = new File(simItemData.getModelFilename());
		simRunData.setTitle("selected utraceChecker trace in "+"Model "+Testgui.titleOf(modelFile));
		simRunData.setModelTitle("Simulation of utraceChecker trace in Model "+ Testgui.titleOf(modelFile));


		startSimulator(clts, LogMux.Kind.SUSPAUT, simRunData, traceItemData);

		return;
	}

	public void simulateSelectedIocoCheckerTraceInModel() {
		if (false && configPane.getInterpretation() == null) {
			addStatusMsg("choose interp");
			return;
		}
		addStatusMsg("");
		LTS selected = iocoCheckerFailuresPane.getSelectedLTS();
		LTS lts = iocoCheckerFailuresPane.getModelLTS();
		String fileName = iocoCheckerFailuresPane.getModelFileName();
		if (lts==null) {
			addStatusMsg("no iocoChecker failure to select");
			return;
		} else if (selected==null) {
			addStatusMsg("no iocoChecker failure selected");
			return;
		}
		// TODO make sure that what we do in next line is identical to what we did when we run the Iocochecker
		//  - it does synthesize Delta labels
		//  - it does not add angelic completion to _model_, as far as we know
		//  - it does not add delta for tau loops (divergence)

		CompoundLTS clts = new Primer(lts, configPane.getInterpretation(), false, true, TraceKind.STRACES, false, false, err, progress);


		RunSessionData simRunData = new RunSessionData();
		RunItemData simItemData = new RunItemData();
		RunItemData traceItemData = new RunItemData();
		simItemData.setFilename(fileName, "");
		simItemData.setLTSKind(RunItemData.LTSKind.MODEL);
		simItemData.setLTS(lts);

		traceItemData.setFilename(fileName, "");
		traceItemData.setLTSKind(RunItemData.LTSKind.TRACE);
		traceItemData.setLTS(selected);
		traceItemData.setTitle("iocoChecker trace");


		simRunData.setModelData(simItemData);
		simRunData.setModelPrimerData(null);
		simRunData.setGuideData(null);
		simRunData.setGuidePrimerData(null);
		simRunData.setCombinatorData(null);
		simRunData.setImplData(null);

		File modelFile = new File(simItemData.getModelFilename());
		simRunData.setTitle("selected iocoChecker trace in "+"Model "+Testgui.titleOf(modelFile));
		simRunData.setModelTitle("Simulation of iocoChecker trace in Model "+ Testgui.titleOf(modelFile));

		simRunData.setModelLTS(simItemData.getLTS());


		startSimulator(clts, LogMux.Kind.SUSPAUT, simRunData, traceItemData);

		return;
	}

	public void simulateSelectedIocoCheckerTraceInImplementation() {
		if (false && configPane.getInterpretation() == null) {
			addStatusMsg("choose interp");
			return;
		}
		addStatusMsg("");
		LTS selected = iocoCheckerFailuresPane.getSelectedLTS();
		LTS lts = iocoCheckerFailuresPane.getImplementationLTS();
		String fileName = iocoCheckerFailuresPane.getImplementationFileName();
		if (lts==null) {
			addStatusMsg("no iocoChecker failure to select");
			return;
		} else if (selected==null) {
			addStatusMsg("no iocoChecker failure selected");
			return;
		}
		// TODO make sure that what we do in next line is identical to what we did when we run the IocoChecker --
		// except fitted for simulation of _implementation_: angelicCompletion must be true
		// we set divergence to false; (or do we want to simulate that we loop forever?)
		CompoundLTS clts = new Primer(lts, configPane.getInterpretation(), false, true, TraceKind.STRACES, true, false, err, progress);


		RunSessionData simRunData = new RunSessionData();
		RunItemData simItemData = new RunItemData();
		RunItemData traceItemData = new RunItemData();
		simItemData.setFilename(fileName, "");
		simItemData.setLTSKind(RunItemData.LTSKind.MODEL);
		simItemData.setLTS(lts);

		traceItemData.setFilename(fileName, "");
		traceItemData.setLTSKind(RunItemData.LTSKind.TRACE);
		traceItemData.setLTS(selected);
		traceItemData.setTitle("iocoChecker trace");


		simRunData.setModelData(simItemData);
		simRunData.setModelPrimerData(null);
		simRunData.setGuideData(null);
		simRunData.setGuidePrimerData(null);
		simRunData.setCombinatorData(null);
		simRunData.setImplData(null);

		simRunData.setModelLTS(simItemData.getLTS());

		File modelFile = new File(simItemData.getModelFilename());
		simRunData.setTitle("selected iocoChecker trace in "+"Implementation "+Testgui.titleOf(modelFile));
		simRunData.setModelTitle("Simulation of iocoChecker trace in Implementation "+ Testgui.titleOf(modelFile));



		startSimulator(clts, LogMux.Kind.SUSPAUT, simRunData, traceItemData);

		return;
	}


	public void handleGivenStimulusEvent(final Label l) {
		teststepPane.enableStimObs(false,false);
		new Thread("Testgui-handleGivenStimulusEvent") {
			public void run() {
				final DriverInterActionResult r = runState.myDriver.stimulate(l);
				display.syncExec (new Runnable () {
					public void run () {
						if (r == null) {
							System.out.println("(sorry, input menu is empty)");
						} else if (handleInterAction(r)) {
							printModelMenu(true);
						}
					}
				});
			}
		}.start();
	}
	public void handleStimulateEvent() {
		teststepPane.enableStimObs(false,false);
		new Thread("Testgui-handleStimulateEvent") {
			Label l = null;
			public void run() {
				display.syncExec (new Runnable () {
					public void run () {
						l = menuPane.getSelectedStimulus();
					}
				});
				final DriverInterActionResult r;
				if (l!=null)
					r = runState.myDriver.stimulate(l);
				else
					r = runState.myDriver.stimulate();
				display.syncExec (new Runnable () {
					public void run () {
						handleInteractionEvent(r);
					}
				});
			}
		}.start();
	}
	public void handleObserveEvent() {
		teststepPane.enableStimObs(false,false);
		new Thread("Testgui-handleObserveEvent") {
			public void run() {
				final DriverInterActionResult r = runState.myDriver.observe();
				display.syncExec (new Runnable () {
					public void run () {
						handleInteractionEvent(r);
					}
				});
			}
		}.start();
	}

	public String handleGetSolutionEvent() {
		// TODO fix: only enable button when instantiator is available
		Label l = menuPane.getSelectedStimulus();
		// TODO fix: only enable button when stimulus has been selected
		if (l==null) {
			addStatusMsg("no stimulus selected");
			return null;
		}
		Label res = runState.myModel.findTransition(runState.myDriver.getTesterState(), l).getSolution();
		if (res!=null) {
			addStatusMsg("");
			return res.getString();
		} else
			addStatusMsg("failed to obtain instantiation");
		return null;
	}
	
	public void notifyMenuInputDeselected() {
		teststepPane.enableInstGet(false);
	}
	public void notifyMenuInputSelected() {
		Label l = menuPane.getSelectedStimulus();
		if (l==null)
			teststepPane.enableInstGet(false);
		else
			teststepPane.enableInstGet(l.getVariables().hasNext());
	}

	private void handleInteractionEvent(DriverInterActionResult ll) {
		addStatusMsg("");
		if (ll == null) {
			System.out.println("(sorry, input menu is empty)");
		} else if (handleInterAction(ll)) {
			printModelMenu(true);
		}
	}

	public void handleAutoRunStopEvent() {
		addStatusMsg("");
		runState.myDriver.stopAuto();
		// no need to tell gui to switch stop button; this will be done automatically
		// when the driver tells that it has done the last test step of the auto run
	}
	public void handleAutoRunStartEvent(final int inc) {
		class AutoConsumer implements DriverInterActionResultConsumer {
			private Boolean goOn;
			public boolean consume(final DriverInterActionResult ll) {
				if(debug) System.err.println("AutoConsumer: consume: "+ll);
				goOn = false;
				int sleepTime = teststepPane.getDelayMS();
				if (sleepTime > 0) {
				try{
					if(debug) System.err.println("trying to sleep...");
					Thread.sleep(sleepTime);
					if(debug) System.err.println("trying to sleep... slept");
				}
				catch(InterruptedException ie){
					System.err.println("sleep... interrupted "+ie.getMessage());
				}
				}
				if (weHaveBeenDisposedOf)
					return false;
				display.syncExec (new Runnable () {
					public void run () {
						goOn = handleInterAction(ll);
						if (goOn) {
							if (teststepPane.getAutoPrintModelMenu())
								printModelMenu(false);
							else
								clearModelMenu(); // OPTIMIZABLE: not needed if already cleared
						}
					}
				});
				return goOn;
			}
			public void end() {
				if (weHaveBeenDisposedOf)
					return;
				display.syncExec (new Runnable () {
					public void run () {
						try {
							printModelMenu(true);
							teststepPane.resetAutoRunning();
						} catch (Exception e) {
							System.err.println("caught exception in handleAutoRunStartEvent.end(): "+e.getMessage());
						}
					}
				});
			}
		}

		new Thread("Testgui-handleAutoRunStartEvent") {
			public void run() {
				runState.myDriver.autoStep(inc, new AutoConsumer());   
			}
		}.start();
	}

	public void addStatusMsg(String s) {
		//System.err.println("addStatusmsg do "+s);
		messagePane.addStatusMsg(s);
		//System.err.println("addStatusmsg done "+s);
	}

	// methods for dynamic updating of gui

	public void setGuideItemsVisibility(boolean b) {
		configPane.setGuideItemsVisibility(b);
		testrunPane.setGuideItemsVisibility(b);
	}
	public void setImplViewItemsVisibility(boolean b) {
		configPane.setImplViewItemsVisibility(b);
		testrunPane.setImplViewItemsVisibility(b);
		iocoCheckerPane.setImplViewItemsVisibility(b);
	}
	public void setImplBrowseItemsVisibility(boolean b) {
		configPane.setImplBrowseItemsVisibility(b);
		menubar.setImplBrowseItemsVisibility(b);
	}
	public void setImplSeedItemsVisibility(boolean b) {
		testrunPane.setImplSeedItemsVisibility(b);
	}
	public void setImplTimeoutItemsVisibility(boolean b) {
		configPane.setImplTimeoutItemsVisibility(b);
	}
	public void setImplRealTimeItemsVisibility(boolean b) {
		configPane.setImplRealTimeItemsVisibility(b);
	}
	public void setImplSimMode() {
		configPane.setImplSimMode();
	}

	public void setUtracesCheckSelectionItemsVisibility(boolean b) {
		utracesCheckPane.setSelectionItemsVisibility(b);
	}

	public void setIocoCheckSelectionItemsVisibility(boolean b) {
		iocoCheckerPane.setSelectionItemsVisibility(b);
	}

	// methods for the functionality of the gui itself

	private String formatLabel(String kind, String p, String s, String c){
		String chan = null;
		String pco = p;
		String lbl = s;
		String chanPco = "";
		String cstrnt = "";
		if (s.equalsIgnoreCase("delta"))
			lbl = "(Quiescence)";
		if (kind.equals("input"))
			chan = "in";
		else if (kind.equals("output") || kind.equals("expected"))
			chan = "out";
		if ((kind.equals("input") || kind.equals("output")) && pco == null && !s.equals("delta")) {
			if (s.length() > 3)
				pco = s.substring(0, 3);
			else
				pco = s;
		}
		if (c!=null && !c.trim().equals(""))
			cstrnt = " ["+c+"]";
		if (chan != null)
			chanPco += chan;
		if (pco != null && !pco.equals(""))
			chanPco += ":"+pco;
		if (chan != null || pco != null)
			chanPco = "("+chanPco+ ")";
		return kind+chanPco+": "+lbl+cstrnt;
	}
	private String formatInterAction(DriverInterActionResult l, String p) {
		String kind ="";
		if (l.getKind() == InterActionResult.Kind.INPUT)
			kind = "input";
		if (l.getKind() == InterActionResult.Kind.OUTPUT)
			kind = "output";
		return formatLabel(kind, p, l.getLabel().getString(),
				l.getLabel().getConstraint()!=null?l.getLabel().getConstraint().getString():null);
	}
	private  void printModelMenu(boolean enableButtons) {
		// need check in next line, because we unset myDriver at end of test run (in stopTestrun),
		// and this method may be called (much later) via handleAutoRunStartEvent.end()
		if (runState.myDriver==null)
			return;
		menuPane.clear();
		Iterator<Label> it;
		it = runState.myDriver.getModelInputs().iterator();
		Boolean haveInputs = it.hasNext();
		while (it.hasNext()) {
			Label l = it.next();
			String s = formatLabel("input", "", l.getString(),
					l.getConstraint()!=null?l.getConstraint().getString():null);
			menuPane.addInput(s, l);
		}
		menuPane.finalizeInputs();

		Boolean haveOutputs;
		Verdict pv = runState.myModel.getPositiveDefaultVerdict();
		String pvs = (pv!=null? "("+pv.getString()+")" : "");
		Collection<LabelPlusVerdict> lvcol = runState.myDriver.getModelOutputVerdicts();
		// TODO change the code below such that we assume that
		// defining the LabelPlusVerdict iterator will always succeed
		// if lvcol is non-null?
		Iterator<LabelPlusVerdict> lvit = null;
		if (lvcol != null)
			lvit = lvcol.iterator();
		if(lvit!=null) {
			haveOutputs = lvit.hasNext();
			while (lvit.hasNext()) {
				LabelPlusVerdict lv = lvit.next();
				Label l = lv.getLabel();
				Verdict v = lv.getVerdict();
				if (v!=null)
					menuPane.addOutput(formatLabel("output", "", l.getString(),
							l.getConstraint()!=null?l.getConstraint().getString():null), v.getString());
				else
					menuPane.addOutput(formatLabel("output", "", l.getString(),
							l.getConstraint()!=null?l.getConstraint().getString():null), pvs);
			}
		} else {
			it = runState.myDriver.getModelOutputs().iterator();
			haveOutputs = it.hasNext();
			while (it.hasNext()) {
				Label l = it.next();
				menuPane.addOutput(formatLabel("output", "", l.getString(),
						l.getConstraint()!=null?l.getConstraint().getString():null), pvs);
			}
		}
		menuPane.finalizeOutputs();

		Verdict nv = runState.myModel.getNegativeDefaultVerdict();
		if (nv!=null)
			menuPane.addOutput("*", nv.getString());



		if (enableButtons && runState.verdict == null) {
			teststepPane.enableStimObs(haveInputs, haveOutputs);
		} else {
			teststepPane.enableStimObs(false, false);
		}
	}

	private Boolean handleInterAction(DriverInterActionResult ll) {
		// TODO better fix than this sympton fixing
		if (ll == null) {
			System.out.println("oops handleInterAction: DriverInterActionResult==null");
			stopTestrun();
			return false;
		}

		stateStats.step(ll);
		if (weHaveBeenDisposedOf)
			return false;
		pathPane.appendLine(ll.getStepNr()+" "+formatInterAction(ll, null));

		logMux.addInterActionResult(ll, cfgRun.getRunId(), cfgRun.getRunId());

		if (ll.hasVerdict()) {
			runState.verdict = ll.getVerdict();
			testrunPane.setVerdict(runState.verdict);
			if (runState.verdict.isFail()){
				menuPane.prepareToShowFailure();
				printModelMenu(false);
				System.out.println("spec disagrees with iut");
				Iterator<Label> exp = runState.myDriver.getModelOutputs().iterator();
				if (!exp.hasNext())
					System.out.println("(oops internal error? no outputs, no delta)");
				else {
					System.out.println("outputs expected by spec:");
					while(exp.hasNext()) {
						Label l = exp.next();
						System.out.println(formatLabel("expected", null, l.getString(),
								l.getConstraint()!=null?l.getConstraint().getString(): null));
					}
					System.out.println("(end of expected outputs)");
				}
				System.out.println("state according to spec:");
				System.out.println("(end of state)");
			}
			stopTestrun();
			return false;
		}
		return true;
	}

	private void clearModelMenu() {
		menuPane.clear();
	}

	public boolean processModelConfig(RunItemData data, LogMux.Kind kind) {
		missing.reset();
		String cfgFilename = null;
		boolean useInstantiator = false;
		String cfgInstFileName = null;
		String checkKind = "";
		RunItemData.AdapterKind implKind = configPane.getImplKind();

		switch (kind) {
		case MODEL:
		case SUSPAUT:
			checkKind = "model";
			cfgFilename = configPane.getModelFilename();
			useInstantiator = configPane.useModelInstantiator();
			cfgInstFileName = configPane.getInstModelFilename();
			break;
		case GUIDE:
			checkKind = "guide";
			cfgFilename = configPane.getGuideFilename();
			//TODO we do not support instantiator for Guide
			break;
		case IMPL:
			checkKind = "impl";
			cfgFilename = configPane.getImplFilename();
			useInstantiator = configPane.useImplInstantiator();
			cfgInstFileName = configPane.getInstImplFilename();
			break;

		}

		missing.check(cfgFilename.equals(""), checkKind);
		missing.check(kind==LogMux.Kind.IMPL && implKind == null, "implementation kind");
		if (!missing.getMissing().equals("")) {
			addStatusMsg("specify"+missing.getMissing());
			return false;
		}
		if (kind==LogMux.Kind.IMPL && implKind!=RunItemData.AdapterKind.SIMADAPTER
		        && implKind != RunItemData.AdapterKind.IOCOSIMADAPTER ) {
			addStatusMsg("unsupported implementation kind: "+implKind+" (can only simulate models)");
			return false;
		}

		int timeoutValue = -1;
		TimeUnit unitValue = null;
		//if (implKind == RunItemData.AdapterKind.STDINOUTADAPTER ||
		//		implKind == RunItemData.AdapterKind.TCPCLIENTLABELADAPTER ||
		//		implKind == RunItemData.AdapterKind.TCPSERVERLABELADAPTER) {
		timeoutValue = configPane.getTimeoutValue();
		unitValue = configPane.getTimeoutUnit();

		// TODO this explicit setting of env vars should not be necessary:
		// it should be implicitly done by the runData.setTimeoutValue/Unit calls
		String[] envVars = null;
		if (timeoutValue!= -1 && unitValue!=null){
			envVars = new String[2];
			envVars[0] = "TORXTIMEOUT="+timeoutValue;
			envVars[1] = "TORXTIMEUNIT="+unitValue.name();
			data.setEnvVars(envVars);
		};
		runData.setTimeoutValue(timeoutValue);
		runData.setTimeoutUnit(unitValue);

		String instFileName = "";
		if (useInstantiator && cfgInstFileName!=null)
			instFileName = cfgInstFileName;

		data.endPreviousIfDifferent(cfgFilename, instFileName);
		data.setFilename(cfgFilename, instFileName);
		System.err.println("simulator: about to start instantiator if needed");
		if (! data.startInstantiatorIfNeeded(err, progress)) {
			return false;
		}
		System.err.println("simulator: about to start instantiator if needed");
		if (! data.readLTSFileIfNeeded(err, progress))
			return false;

		System.err.println("simulator: end of processModelConfig");
		return true;
	}
	public void startTestrun() {
		cfgRun = new Testrun(this);
		Engine e = new Engine();
		modelData.setUsingLazyOTF(useLazyOTF);
		implData.setUsingLazyOTF(useLazyOTF);
		e.startTestRun(cfgRun, cfgRun, runState,
				runData, modelData, modelPrimerData, guideData, guidePrimerData, combinatorData, implData,
				err, progress, stateStats,highlighter);
	}
	
/*	public void old_startTestrun() {
		addStatusMsg("-- new test run --");
		addStatusMsg("");
		missing.reset();

		for(JTorXPane p: testPanes)
			p.runSetup();


		final IOLTSInterpretation interp = configPane.getInterpretation();
		final boolean useGuide = configPane.useGuide();
		final boolean useCoverage = configPane.getUseCoverage();
		final boolean resetCoverage = configPane.getResetCoverage();
		final boolean useModelInstantiator = configPane.useModelInstantiator();
		final String cfgInstModelFileName = configPane.getInstModelFilename();
		final boolean useImplInstantiator = configPane.useImplInstantiator();
		final String cfgInstImplFileName = configPane.getInstImplFilename();

		final String cfgModelFilename = configPane.getModelFilename();
		final String cfgGuideFilename = configPane.getGuideFilename();
		final String cfgImplFilename = configPane.getImplFilename();
		final RunItemData.LTSKind guideKind = configPane.getGuideKind();
		LTS tmpIocoCheckerTestPurpose = null;
		final RunItemData.AdapterKind implKind = configPane.getImplKind();
		final TraceKind traceKind = configPane.getTraceKind();

		final int timeoutValue = configPane.getTimeoutValue();
		final TimeUnit unitValue = configPane.getTimeoutUnit();
		//if (implKind == RunItemData.AdapterKind.STDINOUTADAPTER ||
		//		implKind == RunItemData.AdapterKind.TCPCLIENTLABELADAPTER ||
		//		implKind == RunItemData.AdapterKind.TCPSERVERLABELADAPTER) {


		//}

		missing.check(cfgModelFilename.equals(""), "model");
		if (configPane.useGuide() && (guideKind==RunItemData.LTSKind.MODEL || guideKind==RunItemData.LTSKind.TRACE))
			missing.check(cfgGuideFilename.equals(""), "guide");
		if (configPane.useGuide() && guideKind == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE) {
			tmpIocoCheckerTestPurpose = iocoCheckerFailuresPane.getSelectedLTS();
			missing.check(tmpIocoCheckerTestPurpose==null, "iocoChecker-trace-as-guide");
		}
		if (configPane.useGuide() && guideKind == RunItemData.LTSKind.IOCOCHECKERMULTITRACE) {
			tmpIocoCheckerTestPurpose = iocoCheckerFailuresPane.getCombinedLTS();
			missing.check(tmpIocoCheckerTestPurpose==null, "iocoChecker-traces-as-guide");
		}
		final LTS iocoCheckerTestPurpose = tmpIocoCheckerTestPurpose;

		missing.check(cfgImplFilename.equals(""), "implementation");
		missing.check(implKind == null, "implementation kind");
		missing.check(interp == null, "interpretation");
		missing.check(traceKind == null, "trace kind");
		if (implKind == RunItemData.AdapterKind.STDINOUTADAPTER ||
				implKind == RunItemData.AdapterKind.TCPCLIENTLABELADAPTER ||
				implKind == RunItemData.AdapterKind.TCPSERVERLABELADAPTER) {
			missing.check(timeoutValue < 0, "timeout-value");
			missing.check(unitValue == null, "timeout-unit");
		}
		int tmpImplPort = -1;
		String tmpImplHost = "";
		if ((implKind == RunItemData.AdapterKind.TCPCLIENTLABELADAPTER ||
				implKind == RunItemData.AdapterKind.TCPSERVERLABELADAPTER)
				&& !cfgImplFilename.equals("")) {
			// be liberal: map crazy (sequence of) separator(s) onto !
			String tmp = cfgImplFilename.replace(' ', '!');
			tmp = tmp.replace(':', '!');
			tmp = tmp.replaceFirst("!!*","!");
			String hostPlusPort[] = tmp.split("!",2);
			if (hostPlusPort.length == 2) {
				tmpImplHost = hostPlusPort[0];
				try {
					tmpImplPort = Integer.parseInt(hostPlusPort[1]);
				}
				catch(NumberFormatException nfe) {
					addStatusMsg("impl port number: value is not a number: "+hostPlusPort[1]);
				}
			}
			missing.check(tmpImplHost.equals(""), "implementation-host-name");
			missing.check(tmpImplPort < 0, "implementation-port-number");
		}
		final int implPort = tmpImplPort;
		final String implHost = tmpImplHost;



		if (!missing.getMissing().equals("")) {
			addStatusMsg("specify"+missing.getMissing());
			abortTestrun();
			return;
		}

		final boolean implUsesModel = (implKind == RunItemData.AdapterKind.SIMADAPTER ||
				implKind == RunItemData.AdapterKind.LOGADAPTER);
		final boolean useImplRealTime = (implKind==RunItemData.AdapterKind.STDINOUTADAPTER && configPane.useImplRealTime());
		final boolean addRmLabelAnnoPfx = configPane.getStripAddLabelAnno() &&
		configPane.getInterpKind() == InterpKind.PREFIX;
		final boolean addRmLabelAnnoSfx = configPane.getStripAddLabelAnno() &&
		configPane.getInterpKind() == InterpKind.POSTFIX;

		addStatusMsg("");

		runData.reset();
		runData.setUseGuide(useGuide);
		runData.setUseModelInstantiator(useModelInstantiator);
		runData.setUseImplInstantiator(useImplInstantiator);
		runData.setUseImplRealTime(useImplRealTime);
		runData.setModelData(modelData);
		runData.setModelPrimerData(modelPrimerData);
		runData.setGuideData(guideData);
		runData.setGuidePrimerData(guidePrimerData);
		runData.setCombinatorData(combinatorData);
		runData.setImplData(implData);

		runData.setTimeoutValue(timeoutValue);
		runData.setTimeoutUnit(unitValue);

		runData.setAddRmLabelAnnoPfx(addRmLabelAnnoPfx);
		runData.setAddRmLabelAnnoSfx(addRmLabelAnnoSfx);

		runData.setInterpKind(configPane.getInterpKind());
		runData.setReconDelta(configPane.getReconDelta());
		runData.setSynthDelta(configPane.getSynthDelta());
		runData.setAngelicCompletion(configPane.getAngelicCompletion());
		runData.setInterpInputs(configPane.getInterpInputs());
		runData.setInterpOutputs(configPane.getInterpOutputs());
		runData.setTraceKind(traceKind);
		
		runData.setSeed(parseSeed(testrunPane.getSeed(), "seed is not a number"));
		runData.setSimSeed(parseSeed(configPane.getSimSeed(), "sim seed is not a number"));

		runData.setVizModel(testrunPane.vizModel());
		runData.setVizMsc(testrunPane.vizMsc());
		runData.setVizRun(testrunPane.vizRun());
		runData.setVizLogPane(testrunPane.vizLogPane());
		runData.setVizSuspAut(testrunPane.vizSuspAutom());
		runData.setVizGuide(testrunPane.vizGuide());
		runData.setVizImpl(testrunPane.vizImpl());





		// How to deal with special case where particular TorX-explorer has Instantiator built-in?
		// always allow user to override and user external Instantiator
		// TODO extend torx-explorer protocol to allow 'user' to ask for properties
		//  - finite/infinite ?
		//  - known-to-be-symbolic ?
		//  - has built-in instantiator ?
		// e.g. by having comamand: p
		// and answer  P list of property names that apply

		String instModelFileName = "";
		if (useModelInstantiator && cfgInstModelFileName!=null)
			instModelFileName = cfgInstModelFileName;
		runData.setModelInstantiator(instModelFileName);

		String instImplFileName = "";
		if (useImplInstantiator && cfgInstImplFileName!=null)
			instImplFileName = cfgInstImplFileName;
		runData.setImplInstantiator(instImplFileName);

		modelData.endPreviousIfDifferent(cfgModelFilename, instModelFileName);
		modelData.setFilename(cfgModelFilename, instModelFileName);
		modelData.setLTSKind(RunItemData.LTSKind.MODEL);

		// TODO this needs more thought: should endPreviousIfDifferent
		// look not only at filename but also at kind???

		guideData.setLTSKind(guideKind);
		if (useGuide && (guideKind==RunItemData.LTSKind.MODEL || guideKind==RunItemData.LTSKind.TRACE)) {
			guideData.endPreviousIfDifferent(cfgGuideFilename, "");
			guideData.setFilename(cfgGuideFilename, "");
		} else if (useGuide && 
				(guideKind == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE ||
						guideKind == RunItemData.LTSKind.IOCOCHECKERMULTITRACE)) {
			guideData.setFilename("", "");
		} else {
			guideData.setFilename("", "");
		}
		implData.endPreviousIfDifferent(cfgImplFilename, instImplFileName);
		implData.setFilename(cfgImplFilename, instImplFileName);
		implData.setAdapterKind(implKind);
		implData.setLTSKind(RunItemData.LTSKind.MODEL);

		// TODO this explicit setting of env vars should not be necessary:
		// it should be implicitly done by the runData.setTimeoutValue/Unit calls
		String[] tmpEnvVars = null;
		if (runData.getTimeoutValue()!= -1 && runData.getTimeoutUnit()!=null){
			tmpEnvVars = new String[2];
			tmpEnvVars[0] = "TORXTIMEOUT="+runData.getTimeoutValue();
			tmpEnvVars[1] = "TORXTIMEUNIT="+runData.getTimeoutUnit().name();
			modelData.setEnvVars(tmpEnvVars);
			guideData.setEnvVars(tmpEnvVars);
			implData.setEnvVars(tmpEnvVars);
		};
		final String[] envVars = tmpEnvVars;

		if (! modelData.startInstantiatorIfNeeded(err, progress)) {
			abortTestrun();
			return;
		}
		if (! modelData.readLTSFileIfNeeded(err, progress)) {
			abortTestrun();
			return;
		}
		if (useGuide && (guideKind==RunItemData.LTSKind.MODEL) &&
				! guideData.readLTSFileIfNeeded(err, progress)) {
			abortTestrun();
			return;
		}
		if (useGuide && (guideKind==RunItemData.LTSKind.TRACE) &&
				! guideData.readLTSFileIfNeeded(err, progress)) {
			abortTestrun();
			return;
		}
		if (useGuide &&
				(guideKind == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE ||
						guideKind == RunItemData.LTSKind.IOCOCHECKERMULTITRACE))
			guideData.setLTS(iocoCheckerTestPurpose);
		if (useGuide && !guideData.containsEpsilon())
			guideData.setLTS(new GuidanceLTS(guideData.getLTS()));

		if (implUsesModel &&  ! implData.startInstantiatorIfNeeded(err, progress)) {
			abortTestrun();
			return;
		}
		if (implUsesModel && ! implData.readLTSFileIfNeeded(err, progress)){
			abortTestrun();
			return;
		}

		runData.setSeed(initSeed(""+runData.getSeed()));
		testrunPane.setSeed(""+runData.getSeed());
		
		if (implData.getAdapterKind() == RunItemData.AdapterKind.SIMADAPTER) {
			runData.setSimSeed(initSimSeed(""+runData.getSimSeed(), runData.getSeed()));
			configPane.setSimSeed(""+runData.getSimSeed());
		}
		

		// menuPane.getGroup().setText("Current tester state gives:");


		runData.setHighlighter(highlighter);
		File modelFile = new File(modelData.getModelFilename());
		final File implFile = new File(implData.getModelFilename());
		runData.setTitle(titleOf(modelFile)+"-"+titleOf(implFile));
		runData.setModelTitle(titleOf(modelFile));
		runData.setImplTitle(titleOf(implFile));
		runData.setVizMsc(testrunPane.vizMsc());
		runData.setVizRun(testrunPane.vizRun());
		runData.setVizLogPane(testrunPane.vizLogPane());
		runData.setVizCoverage(testrunPane.vizCoverage());

		final boolean cacheAllModelStates = testrunPane.vizSuspAutom() || testrunPane.vizCoverage() || useCoverage;
		final boolean synthDelta = configPane.getSynthDelta();
		final boolean angelicCompletion = configPane.getAngelicCompletion();

		final boolean vizModel = testrunPane.vizModel();
		final boolean vizSuspAutom = testrunPane.vizSuspAutom();
		final boolean vizGuide = testrunPane.vizGuide();
		final boolean vizImpl = testrunPane.vizImpl();
		final boolean vizCoverage = testrunPane.vizCoverage();
		
		testStartThread = new Thread("Testgui-startTestRun") {
			public void run() {

				CompoundLTS myPrimer = new Primer(modelData.getLTS(), interp, !synthDelta tracesOnly, cacheAllModelStates, traceKind, angelicCompletion, err, progress);
				runData.setModelLTS(myPrimer);
				runData.setModelWriter(new AniDotLTSWriter(modelData.getLTS(),  interp, getStateSpaceProgressReporter()));

				quiescenceInterp = new QuiescenceInterpretation(interp);
				CoverageData coverage = null;
				if(vizCoverage || useCoverage) {
					coverage = CoverageFactory.getCoverageData(modelData.getModelFilename(), myPrimer);
					if (resetCoverage)
						coverage.reset();
					try {
						coveragePane.setCoverage(coverage);
						coverage.setModel(modelData.getLTS(), myPrimer, quiescenceInterp);
					} catch (LTSException e) {
						System.err.println("caught exception when setting model or coverage:");
						e.printStackTrace();
					}
				} else {
					// TODO put message in coverage pane that tells that it contains old data
				}
				
				Set<CompoundLTS> guides = new HashSet<CompoundLTS>();
				
				if (useCoverage) {
					// create coverage guide based on primer
					// add coverage guide to guides
					CoverageGuide cg = new CoverageGuide(myPrimer, coverage, quiescenceInterp);
					guides.add(cg);
				}

				if (useGuide) {
					// TODO make sure choice for always STRACES here is correct
					CompoundLTS myGuide = new Primer(guideData.getLTS(), quiescenceInterp, true tracesOnly, cacheAllModelStates, TraceKind.STRACES, false  no AngelicCompletion, err, progress);
					Set<CompoundLTS> specs = new HashSet<CompoundLTS>();
					specs.add(myPrimer);
					guides.add(myGuide);
					if (guideKind==RunItemData.LTSKind.MODEL || guideKind==RunItemData.LTSKind.TRACE) {
						File guideFile = new File(guideData.getModelFilename());
						runData.setGuideTitle(titleOf(guideFile));
					} else
						runData.setGuideTitle("IocoCheckerTrace");
					runData.setGuideLTS(myGuide);
					runData.setGuideWriter(new AniDotLTSWriter(guideData.getLTS(),  quiescenceInterp, getStateSpaceProgressReporter()));
					runData.setVizGuide(vizGuide);
				}
				if (guides.size() > 0) {
					Set<CompoundLTS> specs = new HashSet<CompoundLTS>();
					specs.add(myPrimer);
					CompoundLTS myGuidedPrimer = new GuidedLTS(specs, guides, interp, enableCache: !useCoverage);
					myModel = new ExtendedModelImpl(myGuidedPrimer, interp);
					runData.setSuspAutLTS(myGuidedPrimer);
					runData.setSuspAutWriter(new AniDotExtendedLTSWriter(myGuidedPrimer,  interp, getStateSpaceProgressReporter()));
				} else {
					myModel = new ExtendedModelImpl(myPrimer, interp);
					runData.setSuspAutLTS(myPrimer);
					runData.setSuspAutWriter(new AniDotExtendedLTSWriter(myPrimer,  interp, getStateSpaceProgressReporter()));
					runData.setVizGuide(false);
				}
				
				runData.setSuspAutTitle(runData.getModelTitle());


				//TODO: fix adapter hierarchy, such that we can use Adapter here
				SimAdapter myAdapter = null;
				if (implData.getAdapterKind() == RunItemData.AdapterKind.SIMADAPTER) {
					myAdapter = new AdapterSimImpl(implData.getLTS(), interp, simSeed, ! implData.containsDelta());
					System.err.println("testgui: created Adapter");
					myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, myModel, myAdapter, seed);
					System.err.println("testgui: created Driver");
					runData.setImplTitle(titleOf(implFile));
					runData.setImplLTS(implData.getLTS());
					System.err.println("testgui: about to setImplWriter");
					runData.setImplWriter(new AniDotLTSWriter(implData.getLTS(),  interp, getStateSpaceProgressReporter()));
				} else if (implData.getAdapterKind() == RunItemData.AdapterKind.LOGADAPTER) {
					myAdapter = new LogAdapter(implData.getLTS(), interp, seed, true);
					myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, myModel, myAdapter, seed);
					runData.setImplTitle(titleOf(implFile));
					runData.setImplLTS(implData.getLTS());
					System.err.println("testgui: about to setImplWriter");
					runData.setImplWriter(new AniDotLTSWriter(implData.getLTS(),  interp, getStateSpaceProgressReporter()));
				} else if (implData.getAdapterKind() == RunItemData.AdapterKind.TORXADAPTER) {
					String cmd[] = new String[1];
					cmd[0] = implFile.getAbsoluteFile().getPath();
					String dir = implFile.getAbsoluteFile().getParent();
					myAdapter = new AdapterImpl(err, implData.getModelFilename(), dir, envVars, progress);
					myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, myModel, myAdapter, seed);
					runData.setVizImpl(false);
				} else if (implData.getAdapterKind() == RunItemData.AdapterKind.STDINOUTADAPTER) {
					myAdapter = new TimedStdInOutAdapter(err, implData.getModelFilename(), null, envVars, useImplRealTime, addRmLabelAnnoPfx, addRmLabelAnnoSfx, progress, timeoutValue, unitValue);
					System.err.println("testgui: created Adapter");
					myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, myModel, myAdapter, seed);
					System.err.println("testgui: created Driver");
					runData.setVizImpl(false);
				} else if (implData.getAdapterKind() == RunItemData.AdapterKind.TCPCLIENTLABELADAPTER) {
					myAdapter = new TcpClientLabelAdapter(err, implHost, implPort, addRmLabelAnnoPfx, addRmLabelAnnoSfx,  progress, timeoutValue, unitValue);
					System.err.println("testgui: created Adapter");
					myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, myModel, myAdapter, seed);
					System.err.println("testgui: created Driver");
					runData.setVizImpl(false);
				} else if (implData.getAdapterKind() == RunItemData.AdapterKind.TCPSERVERLABELADAPTER) {
					myAdapter = new TcpServerLabelAdapter(err, implHost, implPort, addRmLabelAnnoPfx, addRmLabelAnnoSfx,  progress, timeoutValue, unitValue);
					System.err.println("testgui: created Adapter");
					myDriver = new ExtendedCompoundSimOnLineTestingDriver(err, myModel, myAdapter, seed);
					System.err.println("testgui: created Driver");
					runData.setVizImpl(false);
				} else {
					System.err.println("testgui: adapter not set");
					abortTestrun();
					return;
				}

				System.err.println("testgui: about to execute myDriver.init");
				final DriverInitializationResult initResult = myDriver.init();
				System.err.println("testgui: executed myDriver.init");
				runData.setInitResult(initResult);
				System.err.println("testgui: runData.setInitResult");

				stateStats.runBegins(initResult);
				System.err.println("testgui: stateStats.runBegins");

				display.syncExec (new Runnable () {
					public void run () {
						if (runData.getVizLogPane())
							addLogPane(runData.getTitle());
						else
							addStdoutLogger(runData.getTitle());
						//logMux.addAnimator(coverage); // must this be here? or below?
					}
				});
				if (vizCoverage || useCoverage) {
					logMux.addAnimator(coverage);
				} else {
					// TODO update text or so in pane (also suggested in comment above)
				}
				logMux.startSession(runData, ""+(logPaneCount-1));

				display.syncExec (new Runnable () {
					public void run () {
						for(JTorXPane p: testPanes)
							p.runBegins();

						System.err.println("testgui: testPanes runBegins");

						// early stop of test run in case of error verdict
						if (initResult.hasVerdict() && initResult.getVerdict().getString().equals("error")) {
							System.err.println("testgui: have initial error verdict; stopping test run");
							verdict = initResult.getVerdict();
							testrunPane.setVerdict(verdict);
							System.err.println("testgui: stopping test run verdict="+verdict.getString());
							stopTestrun();
							return;
						}
						System.err.println("testgui: no init verdict");

						verdict = null;
						printModelMenu(true);

						// now that we have initialized gui and visualiations,
						// check if we have another verdict, because now we can
						// visualize the reason for the verdict.
						if (initResult.hasVerdict()) {
							System.err.println("testgui: have initial verdict");
							verdict = initResult.getVerdict();
							testrunPane.setVerdict(verdict);
							System.err.println("testgui: stopping test run verdict="+verdict.getString());
							stopTestrun();
						}
					}
				});
			}
		};
		testStartThread.start();
	}
*/
	public void abortTestrun() {
		System.err.println("aborting test run...");
		if (runState.testStartThread!=null)
			runState.testStartThread.interrupt();
		runState.testStartThread = null;
		runState.verdict = new VerdictImpl("error");
		stopTestrun();
	}

	public void stopTestrun() {
		for(JTorXPane p: testPanes)
			p.runEnds();
		System.err.println("stopTestRun(): done testPanes");

		if (runState.myDriver != null && teststepPane.isAutoRunning()) {
			runState.myDriver.stopAuto();
			// no need to invoke the following here:
			//    teststepPane.resetAutoRunning();
			// it will be invoked by
			//    AutoConsumer.end()
		}
		System.err.println("stopTestRun(): done myDriver.stopAuto();");

		if (runState.myDriver != null) {
			DriverFinalizationResult f = runState.myDriver.finish();
			System.err.println("stopTestRun(): done myDriver.finish();");
			runData.setFinResult(f);
			System.err.println("stopTestRun(): done runData.setFinResult(f);");
			if (runState.verdict == null) {
				runState.verdict = f.getVerdict();
				System.err.println("stopTestRun(): done f.getVerdict();");
			}
		}
		System.err.println("stopTestRun(): about to test if verdict==null");
		if (runState.verdict == null)
			runState.verdict = new VerdictImpl("error");
		testrunPane.setVerdict(runState.verdict);

		System.err.println("stopTestRun(): done testrunPane.setVerdict(verdict="+runState.verdict.getString()+");");

		logMux.stopSession();

		runState.myDriver = null;

		System.err.println("stopTestRun(): done logMux.stopSession();");

		// TODO this may leak resources: call method end ??
		// TODO make sure that we clear on demand, i.e.
		// keep track of dependencies, and only clear
		// when dependencies change
		// modelData.clear();
		// implData.clear();
	}



	public void startIocoCheck(boolean checkRefines) {
		addStatusMsg("-- new ioco check run --");
		addStatusMsg("");
		missing.reset();

		IOLTSInterpretation interp = configPane.getInterpretation();
		String cfgModelFilename = configPane.getModelFilename();
		String cfgImplFilename = configPane.getImplFilename();
		TraceKind traceKind = configPane.getTraceKind();

		boolean useModelInstantiator = configPane.useModelInstantiator();
		String cfgInstModelFileName = configPane.getInstModelFilename();
		boolean useImplInstantiator = configPane.useImplInstantiator();
		String cfgInstImplFileName = configPane.getInstImplFilename();

		String instModelFileName = "";
		if (useModelInstantiator && cfgInstModelFileName!=null)
			instModelFileName = cfgInstModelFileName;
		runData.setModelInstantiator(instModelFileName);

		String instImplFileName = "";
		if (useImplInstantiator && cfgInstImplFileName!=null)
			instImplFileName = cfgInstImplFileName;
		runData.setImplInstantiator(instImplFileName);

		missing.check(cfgModelFilename.equals(""), "model");
		missing.check(cfgImplFilename.equals(""), "implementation");
		missing.check(interp == null, "interpretation");
		missing.check(traceKind == null, "trace kind");

		if (!missing.getMissing().equals("")) {
			addStatusMsg("specify"+missing.getMissing());
			iocoCheckerPane.runEnds();
			return;
		}

		if (!instModelFileName.equals("") || !instImplFileName.equals("")) {
			addStatusMsg("cannot handle symbolic "+
					(!instModelFileName.equals("") ? "model " : "")+
					(!instImplFileName.equals("") ? "implementation " : "")
			);
			iocoCheckerPane.runEnds();
			return;
		}


		addStatusMsg("");

		modelData.endPreviousIfDifferent(cfgModelFilename, instModelFileName);
		modelData.setFilename(cfgModelFilename, instModelFileName);
		implData.endPreviousIfDifferent(cfgImplFilename, instImplFileName);
		implData.setFilename(cfgImplFilename, instImplFileName);

		if (! modelData.startInstantiatorIfNeeded(err, progress)) {
			iocoCheckerPane.runEnds();
			return;
		}
		if (! modelData.readLTSFileIfNeeded(err, progress)) {
			iocoCheckerPane.runEnds();
			return;
		}
		if (! implData.startInstantiatorIfNeeded(err, progress)) {
			iocoCheckerPane.runEnds();
			return;
		}
		if (! implData.readLTSFileIfNeeded(err, progress)) {
			iocoCheckerPane.runEnds();
			return;
		}

		runData.reset();

		for(JTorXPane p: iococheckPanes)
			p.runBegins();

		final int autoStopCount = iocoCheckerPane.autoStopCount();
		iocoChecker = new IocoChecker(err, modelData.getLTS(), implData.getLTS(),
				cfgModelFilename, cfgImplFilename, traceKind, interp,
				new iocochecker.IocoCheckerUser() {

			private int added = 0;
			private int processed = 0;
			private int failures = 0;

			public boolean hasStopRequest() {
				return iocoCheckerPane.hasStopRequest() || 
				(autoStopCount!=0 && failures >= autoStopCount);
			}

			public void progressReportSituationAdded() {
				added++;
				display.syncExec (new Runnable () {
					public void run () {
						progress.setMax(added);
					}
				});
			}

			public void progressReportSituationProcessed() {
				processed++;
				display.syncExec (new Runnable () {
					public void run () {
						progress.setLevel(processed);
					}
				});

			}

			public void reportFailure(FailureSituation f) {
				failures++;
				final IocoCheckerFailure ff = IocoCheckerWrapper.wrapFailureSituation(f);
				display.syncExec (new Runnable () {
					public void run () {
						stateSpaceProgress.setActivity("failures found: ");
						stateSpaceProgress.setLevel(failures);
						stateSpaceProgress.setMax(failures);
						iocoCheckerFailuresPane.insertFailure(ff);
					}
				});
			}

			public void progressReportCheckingFinished() {
				display.syncExec (new Runnable () {
					public void run () {
						progress.stop();
						stateSpaceProgress.ready();
						progress.setActivity("iocoChecker... done");
						progress.setActivity("processing iocoChecker results...");
					}
				});
			}

		}, checkRefines);
		iocoCheckerFailuresPane.reset();
		progress.reset();
		progress.setActivity("running iocoChecker...");
		stateSpaceProgress.reset();
		//progress.startUndeterminate();

		iocoCheckerPane.runBegins();


		new Thread("Testgui-startIocoCheck") {
			public void run() {

				try {
					final IocoCheckerResult res = iocoChecker.check();
					display.syncExec (new Runnable () {
						public void run () {
							progress.setActivity("processing iocoChecker results... done");
							progress.setActivity("displaying results");
							iocoCheckerFailuresPane.showFailures(res);
							// progress.stopUndeterminate();
							progress.setActivity("");
							iocoCheckerPane.runEnds();
						}
					});
				} catch (java.lang.OutOfMemoryError e) {
					display.syncExec (new Runnable () {
						public void run () {
							progress.setActivity("iocoChecker... out of memory");
							addStatusMsg("iocoChecker... out of memory");
							iocoCheckerFailuresPane.showFailures(new IocoCheckerResult("No result because check run out of memory"));
							// progress.stopUndeterminate();
							progress.setActivity("");
							iocoCheckerPane.runEnds();
						}
					});
				}

			}
		}.start();

	}


	public void startUtracesCheck() {
		addStatusMsg("-- new utraces check run --");
		addStatusMsg("");
		missing.reset();

		IOLTSInterpretation interp = configPane.getInterpretation();
		String cfgModelFilename = configPane.getModelFilename();
		boolean useModelInstantiator = configPane.useModelInstantiator();
		String cfgInstModelFileName = configPane.getInstModelFilename();

		String instModelFileName = "";
		if (useModelInstantiator && cfgInstModelFileName!=null)
			instModelFileName = cfgInstModelFileName;

		missing.check(cfgModelFilename.equals(""), "model");
		missing.check(interp == null, "interpretation");

		if (!missing.getMissing().equals("")) {
			addStatusMsg("specify"+missing.getMissing());
			return;
		}

		if (!instModelFileName.equals("")) {
			addStatusMsg("cannot handle symbolic model");
			return;
		}

		addStatusMsg("");

		modelData.endPreviousIfDifferent(cfgModelFilename, instModelFileName);
		modelData.setFilename(cfgModelFilename, instModelFileName);

		if (! modelData.startInstantiatorIfNeeded(err, progress))
			return;
		if (! modelData.readLTSFileIfNeeded(err, progress))
			return;

		runData.reset();

		/*
		for(JTorXPane p: utracecheckPanes)
			p.runBegins();
		 */

		final UtraceChecker utraceChecker = new UtraceChecker(modelData.getLTS(), cfgModelFilename, interp, err, progress, getStateSpaceProgressReporter());

		for(JTorXPane p: utracescheckPanes)
			p.runBegins();

		new Thread("Testgui-startUtracesCheck") {
			public void run() {
				final UtraceCheckerResult res = utraceChecker.check();
				display.syncExec (new Runnable () {
					public void run () {
						utracesCheckFailuresPane.showFailures(res);
						for(JTorXPane p: utracescheckPanes)
							p.runEnds();
					}
				});

			}
		}.start();

	}



	public void startSimulator(CompoundLTS clts, LogMux.Kind kind, RunSessionData r, RunItemData td) {
		addStatusMsg("-- new compoundLTS simulator --");
		addStatusMsg("");

		if (clts!=null) {
			CTabItem simItem = new CTabItem(tabFolder, SWT.CLOSE);
			if (td!=null)
				simItem.setText("Simulate trace in "+kind.toString());
			else
				simItem.setText("Simulate "+kind.toString());
			Composite simGroup = new Composite(tabFolder, SWT.NONE);
			simItem.setControl(simGroup);

			LogMux simLogMux =  new LogMux(err, logServices);
			logMuxList.add(simLogMux);

			final Simulator sim = new Simulator(clts, simLogMux, kind, r, td, err, progress, stateSpaceProgress);
			SimStartPane simstartPane = new SimStartPane(simGroup, this, sim, r.getTitle());
			SimPathPane simpathPane = new SimPathPane(simGroup, this);
			SimTreePane simtreePane = new SimTreePane(simGroup, this, sim);
			sim.setPanes(simstartPane, simpathPane, simtreePane);

			simItem.addDisposeListener(new DisposeListener() {
				public void widgetDisposed(DisposeEvent arg0) {
					sim.stop();
					// TODO clean up (garbage collect) simulator?
				}
			});

			PaneContainer simPaneContainer = new PaneContainer(simGroup, SWT.VERTICAL);
			simPaneContainer.addFixed(simstartPane.getGroup());
			simPaneContainer.addResizable(simpathPane.getGroup());
			simPaneContainer.addResizable(simtreePane.getGroup());

			simGroup.pack();
			simPaneContainer.layout();
			changeFontSize(simGroup, fontDiff);
		}
	}

	public void startSimulator(LTS lts, LogMux.Kind kind, RunSessionData r, RunItemData td) {
		addStatusMsg("-- new LTS simulator --");
		addStatusMsg("");

		if (lts!=null) {
			CTabItem simItem = new CTabItem(tabFolder, SWT.CLOSE);
			if (td!=null)
				simItem.setText("Simulate trace in "+kind.toString());
			else
				simItem.setText("Simulate "+kind.toString());
			Composite simGroup = new Composite(tabFolder, SWT.NONE);
			simItem.setControl(simGroup);

			LogMux simLogMux =  new LogMux(err, logServices);
			logMuxList.add(simLogMux);

			final Simulator sim = new Simulator(lts, simLogMux, kind, r, td, err, progress, stateSpaceProgress);
			SimStartPane simstartPane = new SimStartPane(simGroup, this, sim, r.getTitle());
			SimPathPane simpathPane = new SimPathPane(simGroup, this);
			SimTreePane simtreePane = new SimTreePane(simGroup, this, sim);
			sim.setPanes(simstartPane, simpathPane, simtreePane);

			simItem.addDisposeListener(new DisposeListener() {
				public void widgetDisposed(DisposeEvent arg0) {
					sim.stop();
					sim.end(); // if we do not do this, a connection to LogMux.Multicast may be hanging, preventing us to exit
					// TODO clean up (garbage collect) simulator?
				}
			});

			PaneContainer simPaneContainer = new PaneContainer(simGroup, SWT.VERTICAL);
			simPaneContainer.addFixed(simstartPane.getGroup());
			simPaneContainer.addResizable(simpathPane.getGroup());
			simPaneContainer.addResizable(simtreePane.getGroup());

			simGroup.pack();
			simPaneContainer.layout();
			changeFontSize(simGroup, fontDiff);
		}
	}


	public Shell getShell() {
		return toplevelShell;
	}

	public void restartLogServices() {
		logServices.start();
	}


	private void initGui(final Shell shell, boolean demoMode) {
		tabFolder = new CTabFolder(shell, SWT.BORDER);
		tabFolder.setUnselectedCloseVisible(false);

		handCursor = new Cursor(display, SWT.CURSOR_HAND);
		red = display.getSystemColor(SWT.COLOR_RED);


		Composite configGroup;
		if (demoMode) {
			CTabItem configItem = new CTabItem(tabFolder, SWT.NONE);
			configItem.setText("Config");
			configGroup = new Composite(tabFolder, SWT.NONE);
			configItem.setControl(configGroup);
		} else
			configGroup = shell;


		CTabItem utracescheckItem = new CTabItem(tabFolder, SWT.NONE);
		utracescheckItem.setText("UtracesCheck");
		utracescheckItem.setToolTipText("Check whether there is a difference between the Straces and the Utraces of the Model");
		Composite utracescheckGroup = new Composite(tabFolder, SWT.NONE);
		utracescheckItem.setControl(utracescheckGroup);

		CTabItem iococheckItem = new CTabItem(tabFolder, SWT.NONE);
		iococheckItem.setText("IocoCheck");
		iococheckItem.setToolTipText("Check whether IOCO relation holds between Implementation and Model -- where the Implementation must be given as model, not as real program");
		Composite iococheckGroup = new Composite(tabFolder, SWT.NONE);
		iococheckItem.setControl(iococheckGroup);

		CTabItem testItem = new CTabItem(tabFolder, SWT.NONE);
		testItem.setText("Test");
		testItem.setToolTipText("Test whether the Implementation conforms to the Model, by actually running the Implementation (as real program or as simulated model), providing stimuli and obtaining observations");
		
		Composite testGroup = new Composite(tabFolder, SWT.NONE);
		testItem.setControl(testGroup);

		CTabItem coverageItem = new CTabItem(tabFolder, SWT.NONE);
		coverageItem.setText("Coverage");
		coverageItem.setToolTipText("Coverage placeholder");
		Composite coverageGroup = new Composite(tabFolder, SWT.NONE);
		coverageItem.setControl(coverageGroup);
	
		tabFolder.setSelection(0); 
		tabFolder.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				if (false) {
					if (tabFolder.getSelectionIndex() == 1){
						setImplSimMode();
						configPane.setGuideButtonVisibility(false);
						setGuideItemsVisibility(false);
					} else {
						configPane.setGuideButtonVisibility(true);
						setGuideItemsVisibility(configPane.useGuide());
					}
				}
			}  }); 


		configPane = new ConfigPane(configGroup, this);

		messagePane = new MessagePane(shell, this);
		progressPane = new ProgressPane(shell,this);

		testrunPane = new TestrunPane(testGroup, this);
		pathPane = new PathPane(testGroup, this);
		menuPane = new MenuPane(testGroup, this);
		teststepPane = new TeststepPane(testGroup, this);

		iocoCheckerPane = new IocoCheckerCheckPane(iococheckGroup, this);
		iocoCheckerFailuresPane = new IocoCheckerFailuresPane(iococheckGroup, this);

		utracesCheckPane = new UtracesCheckPane(utracescheckGroup, this);
		utracesCheckFailuresPane = new UtracesCheckFailuresPane(utracescheckGroup, this);
		
		coveragePane = new CoveragePane(coverageGroup, this);


		menubar = new Menubar(this, toplevelShell);

		testPanes.add(configPane);
		testPanes.add(testrunPane);
		testPanes.add(pathPane);
		testPanes.add(menuPane);
		testPanes.add(teststepPane);
		testPanes.add(messagePane);
		testPanes.add(progressPane);
		testPanes.add(menubar);
		testPanes.add(coveragePane);

		iococheckPanes.add(iocoCheckerPane);
		iococheckPanes.add(iocoCheckerFailuresPane);

		utracescheckPanes.add(utracesCheckPane);
		utracescheckPanes.add(utracesCheckFailuresPane);

		logServices = new LogServices(err);
		logServices.getMsc().addFailureReporter(mscButtonDisabler);
		logServices.getAnidot().addFailureReporter(anidotButtonDisabler);
		logServices.start();
		logMux = new LogMux(err, logServices);
		logMuxList.add(logMux);

		progress = progressPane.getProgressReporterBar();
		stateSpaceProgress = progressPane.getStateSpaceProgressReporterBar();

		paneContainer = new PaneContainer(shell, SWT.VERTICAL);

		if (demoMode)
			configGroup.setLayout(new FillLayout(SWT.HORIZONTAL|SWT.VERTICAL));

		testPaneContainer = new PaneContainer(testGroup, SWT.VERTICAL);
		testPaneContainer.addFixed(testrunPane.getGroup());
		testPaneContainer.addResizable(pathPane.getGroup());
		testPaneContainer.addResizable(menuPane.getGroup());
		testPaneContainer.addFixed(teststepPane.getGroup());

		iococheckPaneContainer = new PaneContainer(iococheckGroup, SWT.VERTICAL);
		iococheckPaneContainer.addFixed(iocoCheckerPane.getGroup());
		iococheckPaneContainer.addResizable(iocoCheckerFailuresPane.getGroup());

		utracescheckPaneContainer = new PaneContainer(utracescheckGroup, SWT.VERTICAL);
		utracescheckPaneContainer.addFixed(utracesCheckPane.getGroup());
		utracescheckPaneContainer.addResizable(utracesCheckFailuresPane.getGroup());
		
		coveragePaneContainer = new PaneContainer(coverageGroup, SWT.VERTICAL);
		coveragePaneContainer.addResizable(coveragePane.getGroup());


		if (!demoMode)
			paneContainer.addFixed(configPane.getGroup());
		paneContainer.addResizable(tabFolder);
		paneContainer.addFixed(messagePane.getGroup());
		paneContainer.addFixed(progressPane.getGroup());

		/*
		shell.addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				System.err.println("resized!");
				Rectangle area = shell.getClientArea();
				Point size = shell.computeSize(SWT.DEFAULT, SWT.DEFAULT);
				int oldy = tabFolder.getSize().y + messagePane.getGroup().getSize().y+progressPane.getGroup().getSize().y;
				int others;
				int remains;
				if (oldy > area.height) {
					// table is getting smaller so make the columns 
					// smaller first and then resize the table to
					// match the client area width
					others = messagePane.getGroup().getSize().y+progressPane.getGroup().getSize().y;
					remains = size.y - others;
					tabFolder.setSize(size.x,remains);
					//tree.setSize(area.width, area.height);
				} else {
					others = messagePane.getGroup().getSize().y+progressPane.getGroup().getSize().y;
					remains = size.y - others;
					tabFolder.setSize(size.x,remains);

				}

			}
		});
		 */


		//paneContainer.print();

		// first we have to pack toplevelShell, so all sizes are computed;
		// then we have to store/freeze sizes by calling paneContainer.layout()
		// it should be possible to do this differently

		if (demoMode)
			configGroup.pack();
		testGroup.pack();
		testPaneContainer.layout();

		iococheckGroup.pack();
		iococheckPaneContainer.layout();
		utracescheckGroup.pack();
		utracescheckPaneContainer.layout();
		
		coverageGroup.pack();
		coveragePaneContainer.layout();


		shell.pack();
		toplevelShell.pack();
		paneContainer.layout();

		//paneContainer.print();

		setGuideItemsVisibility(configPane.useGuide());
		configPane.setInstModelItemsVisibility(configPane.useModelInstantiator());
		configPane.setInstImplItemsVisibility(configPane.useImplInstantiator());


		final PaintListener paintListener = new PaintListener(){ 
			public void paintControl(PaintEvent e){ 
				if (tabItemToDraw!=null) {
					e.gc.setLineWidth(3);
					e.gc.setForeground(red);
					e.gc.drawRectangle(tabItemToDraw);
					tabItemNeedsUndraw  = true;
				}
			} 
		};

		// code from http://www.eclipsezone.com/forums/thread.jspa?messageID=91860880
		// 4:16 PM on Sep 17, 2003, Veronika Irvine
		Listener listener = new Listener() {
			boolean drag = false;
			boolean exitDrag = false;
			boolean needsListener = false;
			CTabItem dragItem = null;
			public void handleEvent(Event e) {
				Point p = new Point(e.x, e.y);
				if (e.type == SWT.DragDetect) {
					p = tabFolder.toControl(display.getCursorLocation()); //see bug 43251
				}
				switch (e.type) {
				case SWT.DragDetect: {
					//System.err.println("drag");
					CTabItem item = tabFolder.getItem(p);
					if (item == null) return;
					drag = true;
					exitDrag = false;
					dragItem = item;
					tabItemNeedsUndraw = false;
					tabFolder.setCursor(handCursor);
					tabFolder.addPaintListener(paintListener);
					needsListener = false;
					break;
				}
				case SWT.MouseUp: {
					//System.err.println("up");
					tabFolder.setCursor(null);
					tabFolder.removePaintListener(paintListener);
					if (!drag) return;
					CTabItem item = tabFolder.getItem(new Point(p.x, 1));
					if (item != null) {
						int index = tabFolder.indexOf(item);
						int oldIndex = tabFolder.indexOf(dragItem);
						if (oldIndex < index)
							index+=1;
						CTabItem newItem = new CTabItem(tabFolder, dragItem.getStyle(), index);
						newItem.setText(dragItem.getText());
						Control c = dragItem.getControl();
						dragItem.setControl(null);
						newItem.setControl(c);
						dragItem.dispose();
						tabFolder.setSelection(newItem);
					}
					drag = false;
					exitDrag = false;
					dragItem = null;
					tabItemNeedsUndraw = false;
					needsListener = false;
					break;
				}
				case SWT.MouseMove: {
					// System.err.println("move"+" "+drag+" "+exitDrag+" "+needsListener+" "+needsUndraw);
					CTabItem item = tabFolder.getItem(p);
					if (item==null && drag) {
						exitDrag = true;
						drag = false;
					} else if (item!=null && exitDrag) {
						exitDrag = false;
						drag = true;
					}
					if (!drag) {
						if (tabItemNeedsUndraw) {
							tabItemNeedsUndraw = false;
							tabFolder.removePaintListener(paintListener);
							needsListener = true;
							tabFolder.redraw();
							display.update();
						}
						return;
					}
					// System.out.println("\tmove-draw)");
					if (needsListener) {
						tabFolder.addPaintListener(paintListener);
						needsListener = false;
					}
					tabItemToDraw = item.getBounds();
					tabFolder.redraw();
					display.update();
					break;
				}
				}
			}
		};
		tabFolder.addListener(SWT.DragDetect, listener);
		tabFolder.addListener(SWT.MouseUp, listener);
		tabFolder.addListener(SWT.MouseMove, listener);





		toplevelShell.open();
	}


	class MyErrorReporter implements ErrorReporter {
		private void doReport(final String s) {
			try {
				addStatusMsg(s);
			} catch (org.eclipse.swt.SWTException e) {
				if (e.getMessage().contains("Device is disposed") ||
						e.getMessage().contains("Widget is disposed"))
					System.out.println("Caught Testgui ErrorReporter exception: "+e.getMessage());
				else {
					System.out.println("Unexpected Testgui ErrorReporter exception: "+e.getMessage());
					e.printStackTrace();
				}
			}
		}
		public void report(final String s) {
			if (weHaveBeenDisposedOf) {
				System.out.println("ErrorReporter (display already disposed): "+s);
				return;
			}
			System.err.println("run-errreporter: "+s);

			// System.err.println("error-reporter: "+s);
			if (display.getThread() == Thread.currentThread()) {
				System.err.println("cur-errreporter: "+s);
				doReport(s);

			} else
				new Thread("Testgui-MyErrorReporter") {
				public void run() {
					if (display.isDisposed())
						System.out.println("ErrorReporter (display already disposed): "+s);
					else
						display.asyncExec (new Runnable () {
							public void run () {
								// System.err.println("run-errreporter: "+s);
								doReport(s);
							}
						});
				}
			}.start();
		}

		public void log(long time, final String src, final String s) {
			if (weHaveBeenDisposedOf)
				System.out.println("ErrorReporter (display already disposed): "+s);
			else
				logMux.addLogMessage(time, src, s);
		}

		private void doEndOfTest(final String s) {
			try {
				System.err.println("cur-errreporter: eot: "+s);
				addStatusMsg(s);
				stopTestrun();
			} catch (org.eclipse.swt.SWTException e) {
				if (e.getMessage().contains("Device is disposed") ||
						e.getMessage().contains("Widget is disposed"))
					System.out.println("Caught Testgui ErrorReporter exception: "+e.getMessage());
				else {
					System.out.println("Unexpected Testgui ErrorReporter exception: "+e.getMessage());
					e.printStackTrace();
				}
			}
		}
		public void endOfTest(final String s) {
			System.err.println("errreporter: eot: "+s);
			// TODO implement status line or so
			if (weHaveBeenDisposedOf) {
				System.out.println("ErrorReporter (display already disposed): "+s);
				return;
			}
			if (display.getThread() == Thread.currentThread()) {
				doEndOfTest(s);
			} else
				new Thread("Testgui-endOfTest") {
				public void run() {
					if (display.isDisposed())
						System.out.println("ErrorReporter (display already disposed): "+s);
					else
						display.syncExec (new Runnable () {
							public void run () {
								doEndOfTest(s);
							}
						});
				}
			}.start();
		}

	}



	class MyStepHighlighter implements StepHighlighter {
		public void highlight(final String s) {
			new Thread("Testgui-MyStepHighlighter") {
				public void run() {
					display.syncExec (new Runnable () {
						public void run () {
							try {
								int step = Integer.parseInt(s);
								pathPane.highLight(step);
							}
							catch(NumberFormatException nfe) {
								addStatusMsg("highlighter: step is not a number: "+s);
							}
						}
					});
				}
			}.start();
		}
	}

	class MscButtonDisabler implements FailureReporter {
		public void notRunning() {
			if (display.isDisposed())
				return;
			try {
				display.syncExec (new Runnable () {
					public void run () {
						mscVizButtons.disable();
					}
				});
			} catch (org.eclipse.swt.SWTException e) {
				if (e.getMessage().contains("Device is disposed") ||
						e.getMessage().contains("Widget is disposed"))
					System.out.println("Caught Testgui MscButtonDisabler exception: "+e.getMessage());
				else {
					System.out.println("Unexpected Testgui MscButtonDisabler exception: "+e.getMessage());
					e.printStackTrace();
				}
			}
		}
		public void isRunning() {
			if (display.isDisposed())
				return;
			try {
				display.syncExec (new Runnable () {
					public void run () {
						mscVizButtons.enable();
					}
				});
			} catch (org.eclipse.swt.SWTException e) {
				if (e.getMessage().contains("Device is disposed") ||
						e.getMessage().contains("Widget is disposed"))
					System.out.println("Caught Testgui MscButtonDisabler exception: "+e.getMessage());
				else {
					System.out.println("Unexpected Testgui MscButtonDisabler exception: "+e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	class AnidotButtonDisabler implements FailureReporter {
		public void notRunning() {
			if (display.isDisposed())
				return;
			try {
				display.syncExec (new Runnable () {
					public void run () {
						dotVizButtons.disable();
					}
				});
			} catch (org.eclipse.swt.SWTException e) {
				if (e.getMessage().contains("Device is disposed") ||
						e.getMessage().contains("Widget is disposed"))
					System.out.println("Caught Testgui AnidotButtonDisabler exception: "+e.getMessage());
				else {
					System.out.println("Unexpected Testgui AnidotButtonDisabler exception: "+e.getMessage());
					e.printStackTrace();
				}
			}
		}
		public void isRunning() {
			if (display.isDisposed())
				return;
			try {
				display.syncExec (new Runnable () {
					public void run () {
						dotVizButtons.enable();
					}
				});
			} catch (org.eclipse.swt.SWTException e) {
				if (e.getMessage().contains("Device is disposed") ||
						e.getMessage().contains("Widget is disposed"))
					System.out.println("Caught Testgui AnidotButtonDisabler exception: "+e.getMessage());
				else {
					System.out.println("Unexpected Testgui AnidotButtonDisabler exception: "+e.getMessage());
					e.printStackTrace();
				}
			}
		}

	}


	public Testgui (LayoutMode layoutMode, int minHeight) {
		runData.reset();

		Display.setAppName("JTorX");

		display = new Display();

		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		System.err.println("Primary screen size: "+bounds.width+"x"+bounds.height);
		if (layoutMode==LayoutMode.UNSPECIFIED && bounds.height < minHeight) {
			System.err.println("Primary screen height < "+minHeight+" using landscape layout");
			System.err.println("(you can overrule this by giving comand line option --portrait )");
			layoutMode = LayoutMode.LANDSCAPE;
		} else if (layoutMode==LayoutMode.UNSPECIFIED && bounds.height >= minHeight) {
			System.err.println("Primary screen height >= "+minHeight+" using portrait layout");
			System.err.println("(you can overrule this by giving comand line option --landscape )");
		}


		toplevelShell = new Shell(display);
		toplevelShell.setText("JTorX");

		File iconFile = new File(getRunDir().getParent()+File.separator+iconFileName);
		if (iconFile.exists()) {
			icon = new Image(display, iconFile.getAbsolutePath());
			toplevelShell.setImage(icon);
		}
		/*
		Tray tray = display.getSystemTray();
		if(tray != null && icon !=  null) {
			TrayItem trayItem = new TrayItem(tray, SWT.NONE);
			trayItem.setImage(icon);
		}
		 */

		String[] configFilterExtensions = {"*.jtorx", "*.log","*.jtorx;*.log",  "*"};
		String[] configFilterNames = {
				"JTorX configuration files (*.jtorx)",
				"JTorX log files (*.log)",
				"All supported Formats",
				"All files (*.*)"
		};
		
		configFileDialog = new FileDialog(toplevelShell, SWT.OPEN); 
		configFileDialog.setText("FileDialog Demo"); 
		configFileDialog.setFilterExtensions(configFilterExtensions);
		configFileDialog.setFilterNames(configFilterNames);

		configSaveDialog = new FileDialog(toplevelShell, SWT.SAVE);
		configSaveDialog.setText("Save Configuration as");
		configSaveDialog.setFilterExtensions(configFilterExtensions);
		configSaveDialog.setFilterNames(configFilterNames);
		configSaveDialog.setOverwrite(true);

		String[] logFilterExtensions = {"*.log", "*"};
		String[] logFilterNames = {
				"JTorX log files (*.log)",
				"All files (*.*)"
		};
		
		logDialog = new FileDialog(toplevelShell, SWT.OPEN); 
		logDialog.setText("FileDialog Demo"); 
		logDialog.setFilterExtensions(logFilterExtensions);
		logDialog.setFilterNames(logFilterNames);

		logSaveDialog = new FileDialog(toplevelShell, SWT.SAVE); 
		logSaveDialog.setText("Save Log as"); 
		logSaveDialog.setFilterExtensions(logFilterExtensions);
		logSaveDialog.setFilterNames(logFilterNames);
		logSaveDialog.setOverwrite(true);

		String[] filterExtensions = {"*.aut;*.graphml;*.dot;*.gv;*.sax;*.tp;*.jrrc;*.tx;*.tx.bat;*.tx.exe;*.log", "*.aut", "*.graphml", "*.gv;*.dot", "*.sax", "*.tp", "*.jrrc", "*.log", "*.tx;*.tx.bat;*.tx.exe", "*"};
		String[] filterNames = {
				"All supported Formats",
				"Aldebaran files (*.aut)",
				"Graphml files (*.graphml)",
				"GraphViz files (*.gv,*.dot)",
				"STS as XML files (*.sax)",
				"Jararaca files as test purpose (*.tp)",
				"Jararaca files as model (*.jrrc)",
				"JTorX log files (*.log)",
				"TorX-Explorer programs (*.tx[.bat,.exe])",
				"All files (*.*)"
		};
		
		fileDialog = new FileDialog(toplevelShell, SWT.OPEN); 
		fileDialog.setText("FileDialog Demo"); 
		fileDialog.setFilterExtensions(filterExtensions);
		fileDialog.setFilterNames(filterNames);

		saveDialog = new FileDialog(toplevelShell, SWT.SAVE);
		saveDialog.setText("Save as");
		saveDialog.setFilterExtensions(filterExtensions);
		saveDialog.setFilterNames(filterNames);
		saveDialog.setOverwrite(true);

		err = new MyErrorReporter();
		highlighter = new MyStepHighlighter();

		mscButtonDisabler = new MscButtonDisabler();
		anidotButtonDisabler = new AnidotButtonDisabler();

		toplevelShell.addShellListener(new ShellListener() {
			public void shellClosed(ShellEvent arg0) {
				System.out.println("closed");
				weHaveBeenDisposedOf = true;
				cleanup();
			}
			// only for debugging
			public void shellActivated(ShellEvent arg0) {
				// System.out.println("activated");
			}
			public void shellDeactivated(ShellEvent arg0) {
				// System.out.println("deactivated");
			}
			public void shellDeiconified(ShellEvent arg0) {
				// System.out.println("deiconified");		
			}
			public void shellIconified(ShellEvent arg0) {
				// System.out.println("iconified");
			}
		});

		toplevelShell.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent arg0) {
				System.out.println("disposed");
				weHaveBeenDisposedOf = true;
				cleanup();
			}
		});
		
		initGui(toplevelShell, layoutMode==LayoutMode.LANDSCAPE);

		final Testgui _this = this;
		ConfigPaneAccessor cpa = new ConfigPaneAccessor() {
			@Override
			public LazyOTFConfigPane getLazyOTFConfigPane() {
				return _this.configPane.getLazyOTFConfigPane();
			}

			@Override
			public String getModelFilename() {
				return _this.configPane.getModelFilename();
			}

			@Override
			public void setLazyOTFAvailable(boolean available) {
				_this.configPane.setLazyOTFAvailable(available);
			}
		};
		lazyOTFcontroller = new LazyOTFConfigCtrl(cpa);

		while(!toplevelShell.isDisposed()) 
			if(!display.readAndDispatch()) 
				display.sleep(); 
		System.err.println("end of loop");
		display.dispose();   
		System.err.println("after display.dispose");
	}

	private void cleanup() {
		if (modelData!=null && modelData.getLTS()!=null) {
			modelData.getLTS().done();
			modelData = null;
		}
		if (guideData!=null && guideData.getLTS()!=null) {
			guideData.getLTS().done();
			guideData = null;
		}
		if (implData!=null && implData.getLTS()!=null) {
			implData.getLTS().done();
			implData = null;
		}

		for (LogMux l: logMuxList)
			l.done();

		ChildStarter.cleanup();
	}

	protected void finalize() throws Throwable {
		System.err.println("finalize");
		try {
			cleanup();
		} finally {
			super.finalize();
		}
	}
	
	private long parseSeed(String s, String err) {
		long n  = 0;
		if (!s.trim().equals("")) {
			try {
				n = Long.parseLong(s);
			}
			catch(NumberFormatException nfe) {
				addStatusMsg(err+": "+s);
			}
		}
		return n;

	}

	private long initSeed(String s) {
		long n  = 0;
		if (!s.trim().equals("")) {
			try {
				n = Long.parseLong(s);
			}
			catch(NumberFormatException nfe) {
				addStatusMsg("seed is not a number: "+s);
			}
		}
		if (n == 0) {
			Random rand = new Random();
			n = rand.nextInt();
			if (n < 0)
				n = -n;
		}
		return n;
	}
	private long initSimSeed(String s, long rs) {
		long n  = 0;
		if (!s.trim().equals("")) {
			try {
				n = Long.parseLong(s);
			}
			catch(NumberFormatException nfe) {
				addStatusMsg("sim seed is not a number: "+s);
			}
		}
		if (n == 0) {
			n = rs;
		}
		return n;
	}

	public static String titleOf(File f) {
		return f.getName().split("\\.",2)[0];
	}


	private File getRunDir() {
		URL u = this.getClass().getProtectionDomain().getCodeSource().getLocation();
		File f = null;
		try {
			f = new File(URLDecoder.decode(u.getFile(), "UTF-8")).getAbsoluteFile();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return f;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    setupLogging(Level.WARNING);

		LayoutMode layoutMode = LayoutMode.UNSPECIFIED;


		if (args.length == 1 && args[0].equals("--demo"))
			layoutMode = LayoutMode.LANDSCAPE;
		else if  (args.length == 1 && args[0].equals("--landscape"))
			layoutMode = LayoutMode.LANDSCAPE;
		else if  (args.length == 1 && args[0].equals("--portrait"))
			layoutMode = LayoutMode.PORTRAIT;

		new SWTLoader();
		new Testgui(layoutMode, minHeightForPortrait);

	}
	
    private static void setupLogging(Level level) {
        Logger root = Logger.getLogger("");
        root.setLevel(Level.ALL);
        for (Handler handler : root.getHandlers()) {
            if (handler instanceof ConsoleHandler) {
                handler.setLevel(level);
            }
        }
    }

	private void addLogPane(String t) {
		CTabItem logItem = new CTabItem(tabFolder, SWT.CLOSE);
		logItem.setText("Log "+logPaneCount++);
		Composite logGroup = new Composite(tabFolder, SWT.NONE);
		logItem.setControl(logGroup);

		LogTextPane logtextPane = new LogTextPane(logGroup, this, t);
		LogPane logPane = new LogPane(logGroup, this, logtextPane);
		LogWriter logWriter = new LogWriter(logtextPane);
		logMux.addAnimator(logWriter);


		PaneContainer logPaneContainer = new PaneContainer(logGroup, SWT.VERTICAL);
		logPaneContainer.addResizable(logtextPane.getGroup());
		logPaneContainer.addFixed(logPane.getGroup());

		logGroup.pack();
		logPaneContainer.layout();
		changeFontSize(logGroup, fontDiff);
	}

	private void addStdoutLogger(String t) {
		LogWriter logWriter = new LogWriter(System.out);
		logMux.addAnimator(logWriter);
	}

	public void setUseLazyOTF(boolean useLazyOTF) {
		this.useLazyOTF = useLazyOTF; 
		configPane.setLazyOTFConfigurationTabVisible(this.useLazyOTF);
		configPane.setLazyOTFEnabled(useLazyOTF);
	}

	public boolean isLazyOTFEnabled() {
		return this.useLazyOTF;
	}
	
	public LazyOTFConfig getLazyOTFConfig() {
		final Set<LazyOTFConfig> lc = new HashSet<LazyOTFConfig>();
		display.syncExec (new Runnable () {
			public void run () {
				lc.add(lazyOTFcontroller.getConfiguration(false));
			}
		});
		return lc.iterator().next();
	}

	public void updateModelMenu() {
		printModelMenu(true);
	}


	//private long seed = 0;
	//private long simSeed = 0;

	private String iconFileName = "torx.ico";
	private Image icon = null;
	private static int minHeightForPortrait = 875;


	private Cursor handCursor;
	private Color red;
	private boolean tabItemNeedsUndraw = false;
	private Rectangle tabItemToDraw = null;


	private RollCall missing = new RollCall();
	private boolean weHaveBeenDisposedOf = false;
	private Interpretation interpAny = new AnyInterpretation();
	//private IOLTSInterpretation quiescenceInterp =  null;

	private RunItemData guideData = new RunItemData();
	private RunItemData guidePrimerData = new RunItemData();
	private RunItemData modelData = new RunItemData();
	private RunItemData modelPrimerData = new RunItemData();
	private RunItemData combinatorData = new RunItemData();
	private RunItemData implData = new RunItemData();
	private RunSessionData runData = new RunSessionData();

	private RunState runState = new RunState();
	//private utwente.fmt.jtorx.torx.OnLineTestingDriver myDriver;
	//private ExtendedModel myModel;

	//private Verdict verdict;

	private LogServices logServices = null;
	private LogMux logMux = null;
	private Collection<LogMux> logMuxList = new Vector<LogMux>();


	private int fontDiff = 0;

	private StepHighlighter highlighter =  null;
	private ErrorReporter err = null;
	private FailureReporter mscButtonDisabler = null;
	private FailureReporter anidotButtonDisabler = null;
	private ProgressReporter progress = null;
	private ProgressReporter stateSpaceProgress = null;


	private VizButtonHandler dotVizButtons =  new VizButtonHandler(false);
	private VizButtonHandler mscVizButtons =  new VizButtonHandler(false);

	// private Thread testStartThread = null;
	// gui related stuff
	private Display display = null; 
	private Shell toplevelShell = null;
	private Menubar menubar = null;
	private FileDialog configFileDialog = null;
	private FileDialog configSaveDialog = null;
	private FileDialog logDialog = null;
	private FileDialog logSaveDialog = null;
	private FileDialog fileDialog = null;
	private FileDialog saveDialog = null;

	private CTabFolder tabFolder = null;

	private Vector<JTorXPane> testPanes = new Vector<JTorXPane>();
	private ConfigPane configPane = null;
	private CoveragePane coveragePane = null;

	private TestrunPane testrunPane = null;
	private PathPane pathPane = null;
	private MenuPane menuPane = null;
	private TeststepPane teststepPane = null;
	private MessagePane messagePane = null;
	private ProgressPane progressPane = null;

	private Vector<JTorXPane> iococheckPanes = new Vector<JTorXPane>();
	private Vector<JTorXPane> utracescheckPanes = new Vector<JTorXPane>();

	private IocoCheckerCheckPane iocoCheckerPane = null;
	private IocoCheckerFailuresPane iocoCheckerFailuresPane = null;
	private IocoChecker iocoChecker = null;

	private UtracesCheckPane utracesCheckPane = null;
	private UtracesCheckFailuresPane utracesCheckFailuresPane = null;

	private StateStatHandler stateStats = new StateStatHandler();
	private Testrun cfgRun = null;

	private PaneContainer paneContainer = null;
	private PaneContainer testPaneContainer = null;
	private PaneContainer iococheckPaneContainer = null;
	private PaneContainer utracescheckPaneContainer = null;
	private PaneContainer coveragePaneContainer = null;

	private int logPaneCount = 1; // start with 1, to better match msc numbering

	private Set<Font> fontsToBeDisposed = new HashSet<Font>();
	
	private boolean debug = false;

	private boolean useLazyOTF = false;
	private LazyOTFConfigCtrl lazyOTFcontroller;
}
