package utwente.fmt.jtorx.testgui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import utwente.fmt.test.Verdict;

public class TestrunPane implements JTorXPane {

	// ======================================================================
	
	// general JTorXPane interface functions
	
	public void runSetup() {
		startButton.setText("Abort");
		resetVerdict();
	}
	public void runBegins() {
		startButton.setText("Stop");
		resetVerdict();
	}
	public void runEnds() {
		startButton.setText("Start");
	}

	public Group getGroup() {
		return runGroup;
	}

	// ======================================================================
	
	// testrun-pane-specific set/get methods
	
	public void setVerdict(Verdict verdict) {
		verdictText.setText(verdict.getString());
		setVerdictColor(verdict);
	}
	private void resetVerdict() {
		verdictText.setText("");
		if (verdictBackground == null)
			verdictBackground = verdictText.getBackground();
		else
			verdictText.setBackground(verdictBackground);
		if (verdictForeground == null)
			verdictForeground = verdictText.getForeground();
		else
			verdictText.setForeground(verdictForeground);

	}

	private void setVerdictColor(Verdict v) {

		if (v.isFail()) {
			verdictText.setForeground(red);
			verdictText.setBackground(gray);
		} else if (v.getString().equalsIgnoreCase("pass") ||
				v.getString().equalsIgnoreCase("pass,hit")) {
			verdictText.setForeground(green);
			verdictText.setBackground(gray);
		} else if (v.getString().equalsIgnoreCase("pass,miss")) {
			verdictText.setForeground(yellow);
			verdictText.setBackground(gray);
		} 	//else
		//verdictText.setForeground(defaultColor);
	}


	// ======================================================================
	
	// getters and setters for configuration items
	
	public String getSeed() {
		return seedText.getText().trim();
	}
	public void setSeed(String s) {
		seedText.setText(s);
	}

	public String getSimSeed() {
		return simSeedText.getText().trim();
	}
	public void setSimSeed(String s) {
		simSeedText.setText(s);
		simSeedText.notifyListeners(SWT.Modify, null);
	}


	public boolean vizMsc() {
		return mscVizButton.getSelection();
	}
	public void vizMsc(boolean v) {
		mscVizButton.setSelection(v);
	}
	
	public boolean vizRun() {
		return runVizButton.getSelection();
	}
	public void vizRun(boolean v) {
		runVizButton.setSelection(v);
	}
	
	public boolean vizModel() {
		return modVizButton.getSelection();
	}
	public void vizModel(boolean v) {
		modVizButton.setSelection(v);
	}

	public boolean vizGuide() {
		return guideVizButton.getSelection();
	}
	public void vizGuide(boolean v) {
		guideVizButton.setSelection(v);
	}

	public boolean vizImpl() {
		return impVizButton.getSelection();
	}
	public void vizImpl(boolean v) {
		impVizButton.setSelection(v);
	}

	public boolean vizSuspAutom() {
		return suspAutVizButton.getSelection();
	}
	public void vizSuspAutom(boolean v) {
		suspAutVizButton.setSelection(v);
	}

	public boolean vizLogPane() {
		return logPaneButton.getSelection();
	}
	public void vizLogPane(boolean v) {
		logPaneButton.setSelection(v);
	}

	public boolean vizCoverage() {
		return coverageButton.getSelection();
	}
	public void vizCoverage(boolean v) {
		coverageButton.setSelection(v);
	}

	// ======================================================================
	
	// Code for dialog boxes, visibility etc.
	
	public void setGuideItemsVisibility(boolean b) {
		guideVizButton.setVisible(b);
	}
	
	public void setImplViewItemsVisibility(boolean b) {
		impVizButton.setVisible(b);
	}
	
	public void setImplSeedItemsVisibility(boolean b) {
		simSeedLabel.setVisible(b);
		simSeedText.setVisible(b);
	}



	// ======================================================================
	
	// Configuration save and restore functions
	
	public void fillConfig(Config c) {
		c.add("SEED", getSeed());
		c.add("SIMSEED", getSimSeed());

		
		c.add("VIZMSC", vizMsc());
		c.add("VIZRUN", vizRun());
		c.add("VIZMODEL", vizModel());
		c.add("VIZGUIDE", vizGuide());
		c.add("VIZIMPL", vizImpl());
		c.add("VIZSA", vizSuspAutom());
		c.add("VIZLOG", vizLogPane());
		c.add("VIZCOVERAGE", vizCoverage());
	}
	public void loadConfig(Config c) {
		setSeed(c.getString("SEED"));
		setSimSeed(c.getString("SIMSEED"));
		
		vizMsc(c.getBool("VIZMSC"));
		vizRun(c.getBool("VIZRUN"));
		vizModel(c.getBool("VIZMODEL"));
		vizGuide(c.getBool("VIZGUIDE"));
		vizImpl(c.getBool("VIZIMPL"));
		vizSuspAutom(c.getBool("VIZSA"));
		vizLogPane(c.getBool("VIZLOG"));
		vizCoverage(c.getBool("VIZCOVERAGE"));
	}


	public TestrunPane(Composite parent, Testgui g) {
		app = g;

		// Run Pane
		runGroup = new Group(parent, SWT.NONE);
		// runGroup.setLayout(new FillLayout(SWT.HORIZONTAL));
		runGroup.setLayout(new FormLayout());
		runGroup.setText("Test run:");

		Group startGroup  = new Group(runGroup, SWT.NONE);
		RowLayout startLayout = new RowLayout();
		startLayout.wrap = false;
		startLayout.type = SWT.HORIZONTAL;
		startLayout.pack = true; 
		startGroup.setLayout(startLayout);

		startButton = new Button(startGroup,SWT.PUSH);
		startButton.setText("Start");
		startButton.pack();
		
		Composite seedGroup = new Composite(startGroup, SWT.NONE);
		GridLayout seedLayout = new GridLayout();
		seedLayout.numColumns = 2;
		seedGroup.setLayout(seedLayout);
		

		org.eclipse.swt.widgets.Label seedLabel = new org.eclipse.swt.widgets.Label(seedGroup, SWT.NONE);
		seedLabel.setText("Seed:");
		seedLabel.pack();
		seedLabel.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, true, 1, 1));
		
		seedText = new Text(seedGroup, SWT.SINGLE );
	    int columns = 15;
	    GC gc = new GC(seedText);
	    FontMetrics fm = gc.getFontMetrics();
	    int width = columns * fm.getAverageCharWidth();
	    int height = fm.getHeight();
	    gc.dispose();
	    Point p = seedText.computeSize(width, height);
	    GridData layoutDataSeedText = new GridData(SWT.RIGHT, SWT.BOTTOM, true, true, 1, 1);
	    layoutDataSeedText.widthHint= p.x;
	    layoutDataSeedText.heightHint= p.y;
		seedText.setLayoutData(layoutDataSeedText);
		seedText.setToolTipText("Seed for random number generator used for test-derivation and execution");
		
		simSeedLabel = new org.eclipse.swt.widgets.Label(seedGroup, SWT.NONE);
		simSeedLabel.setText("SimSeed:");
		simSeedLabel.pack();
		simSeedLabel.setLayoutData(new GridData(SWT.LEFT, SWT.BOTTOM, true, true, 1, 1));
		simSeedLabel.setVisible(false);
		
		simSeedText = new Text(seedGroup, SWT.SINGLE );
	    GC simgc = new GC(simSeedText);
	    FontMetrics simfm = simgc.getFontMetrics();
	    int simwidth = columns * simfm.getAverageCharWidth();
	    int simheight = simfm.getHeight();
	    simgc.dispose();
	    Point simp = simSeedText.computeSize(simwidth, simheight);
	    GridData layoutDataSimSeedText = new GridData(SWT.RIGHT, SWT.BOTTOM, true, true, 1, 1);
		layoutDataSimSeedText.widthHint= simp.x;
		layoutDataSimSeedText.heightHint= simp.y;
		simSeedText.setLayoutData(layoutDataSimSeedText);
		simSeedText.setVisible(false);
		simSeedText.setToolTipText("Seed for random number generator used for simulator of model-that-is-to-be-used-as-implementation");


		seedGroup.pack();
		
		startGroup.pack();

		Group vizGroup = new Group(runGroup, SWT.NONE);
		if (false) {
			RowLayout vizLayout = new RowLayout();
			vizLayout.wrap = false;
			vizLayout.type = SWT.HORIZONTAL;
			vizLayout.pack = true; 
			vizGroup.setLayout(vizLayout);
		} else {
			GridLayout vizLayout = new GridLayout();
			vizLayout.numColumns = 5;
			vizGroup.setLayout(vizLayout);
		}


		org.eclipse.swt.widgets.Label vizLabel = new org.eclipse.swt.widgets.Label(vizGroup, SWT.NONE);
		vizLabel.setText("Show:");

		modVizButton = new Button(vizGroup, SWT.CHECK);
		modVizButton.setText("model");
		modVizButton.setSelection(true);
		app.registerDotVizButton(modVizButton);
		guideVizButton = new Button(vizGroup, SWT.CHECK);
		guideVizButton.setText("guide");
		guideVizButton.setSelection(true);
		app.registerDotVizButton(guideVizButton);
		
		suspAutVizButton = new Button(vizGroup, SWT.CHECK);
		suspAutVizButton.setText("sa");
		suspAutVizButton.setSelection(false);
		app.registerDotVizButton(suspAutVizButton);
		
		coverageButton = new Button(vizGroup, SWT.CHECK);
		coverageButton.setText("cov");
		coverageButton.setSelection(false);
		
		
		final org.eclipse.swt.widgets.Label dummy = new org.eclipse.swt.widgets.Label(vizGroup, SWT.NONE);
		GridData dummyLayoutData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
		dummyLayoutData.horizontalSpan = 1;
		dummy.setLayoutData(dummyLayoutData);
		
		impVizButton = new Button(vizGroup, SWT.CHECK);
		impVizButton.setText("impl");
		impVizButton.setSelection(true);
		app.registerDotVizButton(impVizButton);
	    GridData layoutDataImpVizButton = new GridData(/*SWT.LEFT, SWT.BOTTOM, true, true, 1, 1*/);
	    //layoutDataImpVizButton.widthHint= simp.x;
	    //layoutDataImpVizButton.heightHint= simp.y;
	    //layoutDataImpVizButton.horizontalSpan = 1;
	    impVizButton.setLayoutData(layoutDataImpVizButton);
	    impVizButton.setVisible(false);
	    
		logPaneButton = new Button(vizGroup, SWT.CHECK);
		logPaneButton.setText("log");
		logPaneButton.setSelection(true);
		
		mscVizButton = new Button(vizGroup, SWT.CHECK);
		mscVizButton.setText("msc");
		mscVizButton.setSelection(true);
		app.registerMscVizButton(mscVizButton);
		
		runVizButton = new Button(vizGroup, SWT.CHECK);
		runVizButton.setText("run");
		runVizButton.setSelection(true);
		app.registerDotVizButton(runVizButton);
		




		vizGroup.pack();


		Group verdictGroup  = new Group(runGroup, SWT.NONE);
		RowLayout verdictLayout = new RowLayout();
		verdictLayout.wrap = false;
		verdictLayout.type = SWT.HORIZONTAL;
		verdictLayout.pack = true; 
		verdictGroup.setLayout(verdictLayout);

		org.eclipse.swt.widgets.Label verdictLabel = new org.eclipse.swt.widgets.Label(verdictGroup, SWT.NONE);
		verdictLabel.setText("Verdict:");
		verdictText = new Text(verdictGroup, SWT.SINGLE);
		verdictText.setEditable(false); 

		verdictGroup.pack();

		FormData startConfig = new FormData(); 
		startConfig.left = new FormAttachment(0, 0); 
		startConfig.top = new FormAttachment(0,0); 
		startConfig.bottom = new FormAttachment(100,0); 
		startGroup.setLayoutData(startConfig); 

		FormData vizConfig = new FormData(); 
		vizConfig.left = new FormAttachment(startGroup, 0); 
		vizConfig.top = new FormAttachment(0,0); 
		vizConfig.bottom = new FormAttachment(100,0); 
		vizGroup.setLayoutData(vizConfig); 

		FormData verdictConfig = new FormData(); 
		verdictConfig.left = new FormAttachment(vizGroup, 0); 
		verdictConfig.top = new FormAttachment(0,0); 
		verdictConfig.bottom = new FormAttachment(100,0); 
		verdictGroup.setLayoutData(verdictConfig); 

		runGroup.pack();

		red = app.getDisplay().getSystemColor(SWT.COLOR_RED);
		green = app.getDisplay().getSystemColor(SWT.COLOR_DARK_GREEN);
		yellow = app.getDisplay().getSystemColor(SWT.COLOR_YELLOW);
		gray = app.getDisplay().getSystemColor(SWT.COLOR_GRAY);

		startButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				startButton.setEnabled(false);
				if (startButton.getText().equals("Start")) {
					app.startTestrun();
				} else if (startButton.getText().equals("Abort")) {
					app.abortTestrun();
				} else {
					app.stopTestrun();
				}
				startButton.setEnabled(true);
			}  }); 
	}
	
	private Testgui app;

	private Group runGroup = null;

	private Button startButton = null;
	private Text seedText = null;
	private Text simSeedText = null;
	private org.eclipse.swt.widgets.Label simSeedLabel = null;
	private Button mscVizButton = null;
	private Button runVizButton = null;
	private Button modVizButton = null;
	private Button guideVizButton = null;
	private Button impVizButton = null;
	private Button suspAutVizButton = null;
	private Button logPaneButton = null;
	private Button coverageButton = null;


	private Text verdictText = null;

	private Color red;
	private Color green;
	private Color yellow;
	private Color gray;
	private Color verdictBackground = null;
	private Color verdictForeground = null;


}
