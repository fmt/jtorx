package utwente.fmt.jtorx.testgui;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Text;

import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.lts.Label;


public class TeststepPane implements JTorXPane {
	
	public Group getGroup() {
		return commandsGroup;
	}
	
	public void runSetup() {
		// NOTE: order of following statements DOES matter
		running = false;
		autoButton.setText("Auto"); // just to be sure, in case it got stuck on "Stop"
		instText.setText("");
		setEnabled(false, false, false);
	}
	public void runBegins() {
		// NOTE: order of following statements DOES matter
		running = true;
		autoButton.setText("Auto"); // just to be sure, in case it got stuck on "Stop"
		instText.setText("");
		setEnabled(true, true, false);
	}
	public void runEnds() {
		// NOTE: order of following statements DOES matter
		setEnabled(false, false, false);
		running = false;
	}
	public void enableStimObs(boolean enStim, boolean enObs) {
		setEnabled(enStim, enObs, true);
	}
	private void setEnabled(boolean enStim, boolean enObs, boolean honourIsRunning) {
		if (!running)
			return;
		stimButton.setEnabled(enStim);
		obsButton.setEnabled(enObs);
		boolean b = enStim || enObs || (honourIsRunning && isAutoRunning());
		autoButton.setEnabled(b);
		autoPrintMenuButton.setEnabled(b);
		instUseButton.setEnabled(enStim && !instText.getText().trim().equals(""));
	}
	public void enableInstGet(boolean enable) {
		instGetButton.setEnabled(enable);
	}
	public int getDelayMS() {
		return delayMS;
	}
	public boolean getAutoPrintModelMenu() {
		return autoPrintModelMenu;
	}
	public boolean isAutoRunning() {
		return autoButton.getText().equals("Stop");
	}
	public void resetAutoRunning() {
		autoButton.setText("Auto");
		if (running)
			autoButton.setEnabled(true);
	}

	public TeststepPane(Composite parent, Testgui g) {
		app = g;
		
		// Button Pane
		commandsGroup = new Group(parent, SWT.NONE);
		commandsGroup.setText("Next test step(s):");
		// RowLayout commandLayout = new RowLayout();
		
		// commandsGroup.setLayout(new FormLayout());
		
		//commandLayout.wrap = false;
		//commandLayout.type = SWT.HORIZONTAL;
		//commandLayout.pack = true; 
		//commandsGroup.setLayout(commandLayout);

		Group stepGroup  = new Group(commandsGroup, SWT.NONE);
		RowLayout stepLayout = new RowLayout();
		stepLayout.wrap = false;
		stepLayout.type = SWT.HORIZONTAL;
		stepLayout.pack = true; 
		stepGroup.setLayout(stepLayout);


		stimButton = new Button(stepGroup,SWT.PUSH);
		stimButton.setText("Stimulate");
		stimButton.pack();
		stimButton.setEnabled(false);

		obsButton = new Button(stepGroup,SWT.PUSH);
		obsButton.setText("Observe");
		obsButton.pack();
		obsButton.setEnabled(false);


		Group autoGroup = new Group(commandsGroup, SWT.NONE);
		RowLayout autoLayout = new RowLayout();
		autoLayout.wrap = false;
		autoLayout.type = SWT.HORIZONTAL;
		autoLayout.pack = true; 
		autoGroup.setLayout(autoLayout);
		autoLayout.spacing = 5;
		// autoGroup.setLayout(new FormLayout());


		autoButton = new Button(autoGroup,SWT.PUSH);
		autoButton.setText("Auto");
		autoButton.setEnabled(false);
		autoButton.pack();

		Composite autoStepGroup = new Composite(autoGroup, SWT.NONE);
		RowLayout autoStepLayout = new RowLayout();
		autoStepLayout.wrap = false;
		autoStepLayout.type = SWT.HORIZONTAL;
		autoStepLayout.pack = true; 
		autoStepLayout.marginWidth = 0;
		autoStepLayout.marginTop = 0;
		autoStepLayout.marginBottom = 0;
		autoStepGroup.setLayout(autoStepLayout);


		org.eclipse.swt.widgets.Label autoLabel = new org.eclipse.swt.widgets.Label(autoStepGroup, SWT.NONE);
		autoLabel.setText("Steps:");
		autoLabel.pack();
		final Text autoCount = new Text(autoStepGroup, SWT.SINGLE );
		autoCount.pack();


		Composite scaleGroup = new Composite(autoGroup, SWT.NONE);
		RowLayout scaleLayout = new RowLayout();
		scaleLayout.wrap = false;
		scaleLayout.type = SWT.HORIZONTAL;
		scaleLayout.pack = true; 
		scaleLayout.marginWidth = 0;
		scaleLayout.marginTop = 0;
		scaleLayout.marginBottom = 0;
		scaleGroup.setLayout(scaleLayout);


		//scaleGroup.setLayout(new FormLayout());
		org.eclipse.swt.widgets.Label scaleLabel = new org.eclipse.swt.widgets.Label(scaleGroup, SWT.NONE);
		scaleLabel.setText("Delay:");
		scaleLabel.pack();

		scale = new Scale (scaleGroup, SWT.HORIZONTAL);
		scale.setMinimum(0);
		scale.setMaximum(1000);
		scale.setSelection(0);
		scale.setIncrement(1);
		scale.setPageIncrement(100);
		//		scale.setSize(autoCount.getSize());
		//scale.setText("speed");
		scale.pack();


		//FormData formAutoButton = new FormData(); 
		//formAutoButton.left = new FormAttachment(0, 0); 
		//formAutoButton.top = new FormAttachment(0,0); 
		//formAutoButton.bottom = new FormAttachment(100, 0); 
		//autoButton.setLayoutData(formAutoButton);

		//FormData formAutoLabel = new FormData(); 
		//formAutoLabel.left = new FormAttachment(autoButton, 0); 
		//formAutoLabel.top = new FormAttachment(0,0); 
		//formAutoLabel.bottom = new FormAttachment(100, 0); 
		//autoLabel.setLayoutData(formAutoLabel);

		//FormData formScale = new FormData(); 
		//formScale.right = new FormAttachment(100, 0);
		//formScale.left = new FormAttachment(100, -40); 
		//formScale.top = new FormAttachment(0,0); 
		//formScale.bottom = new FormAttachment(100, 0); 
		//scale.setLayoutData(formScale); 

		//FormData formAutoPrintMenu = new FormData(); 
		//formAutoPrintMenu.right = new FormAttachment(scale, 0); 
		//formAutoPrintMenu.top = new FormAttachment(0,0); 
		//formAutoPrintMenu.bottom = new FormAttachment(100, 0); 
		//autoPrintMenuButton.setLayoutData(formAutoPrintMenu); 

		//FormData formAutoText = new FormData(); 
		//formAutoText.right = new FormAttachment(autoPrintMenuButton, 0); 
		//formAutoText.left = new FormAttachment(autoLabel, 0); 
		////formAutoText.top = new FormAttachment(0,0); 
		////formAutoText.bottom = new FormAttachment(100, 0); 
		//autoCount.setLayoutData(formAutoText); 


		RowData formScale = new RowData(); 
		//		formScale.right = new FormAttachment(100, 0);
		//		formScale.left = new FormAttachment(100, -40); 
		//TODO fix layout, scale size etc
		formScale.width = autoCount.getBounds().width;
		formScale.height = autoButton.getBounds().height;
		//		formScale.height = autoButton.getBounds().height;
		scale.setLayoutData(formScale);

		autoPrintMenuButton = new Button(autoGroup,SWT.CHECK);
		autoPrintMenuButton.setSelection(autoPrintModelMenu);
		autoPrintMenuButton.setText("update menu");
		autoPrintMenuButton.setEnabled(false);
		autoPrintMenuButton.pack();

		autoGroup.pack(); 

		//	FormData formScaleLabel = new FormData(); 
		//	formScale.width = 40;
		//	formScale.height = 15;
		//	scaleG.setLayoutData(formScale); 


		//	FormData formScaleGroup = new FormData(); 
		//	formScaleGroup.top = new FormAttachment(100,-autoGroup.getBounds().height+5); 
		//formScaleGroup.bottom = new FormAttachment(100, 0); 
		//	scaleGroup.setLayoutData(formScaleGroup);
		
		Group instGroup  = new Group(commandsGroup, SWT.NONE);
		FormLayout instLayout = new FormLayout();
		instGroup.setLayout(instLayout);
		//instLayout.wrap = false;
		//instLayout.type = SWT.HORIZONTAL;
		//instLayout.pack = false; 
		//instGroup.setLayout(instLayout);
	
		org.eclipse.swt.widgets.Label instLabel = new org.eclipse.swt.widgets.Label(instGroup, SWT.NONE);
		instLabel.setText("Instantiation:");
		FormData instLabelData = new FormData();
		instLabelData.left = new FormAttachment(0);
		instLabelData.top = new FormAttachment(0);
		//instButtonData.bottom = new FormAttachment(100);
		instLabel.setLayoutData(instLabelData);
		instLabel.pack();

		instGetButton = new Button(instGroup,SWT.PUSH|SWT.CENTER);
		instGetButton.setText("Get");
		instGetButton.setEnabled(false);
		FormData instGetButtonData = new FormData();
		instGetButtonData.left = new FormAttachment(instLabel, 0);
		instGetButtonData.top = new FormAttachment(0);
		//instGetButtonData.bottom = new FormAttachment(100);
		instGetButton.setLayoutData(instGetButtonData);
		instGetButton.pack();

		instUseButton = new Button(instGroup,SWT.PUSH|SWT.CENTER);
		instUseButton.setText("Use");
		instUseButton.setEnabled(false);
		FormData instUseButtonData = new FormData();
		instUseButtonData.left = new FormAttachment(instGetButton, 0);
		instUseButtonData.top = new FormAttachment(0);
		//instUseButtonData.bottom = new FormAttachment(100);
		instUseButton.setLayoutData(instUseButtonData);
		instUseButton.pack();

		
		instText = new Text(instGroup, SWT.SINGLE );
		FormData instTextData = new FormData();
		instTextData.top = new FormAttachment(instUseButton, 0,SWT.CENTER);
		instTextData.left = new FormAttachment(instUseButton, 0);
		instTextData.right = new FormAttachment(100);
		//instTextData.bottom = new FormAttachment(100);
		instText.setLayoutData(instTextData);
		instText.pack();
		
		instGroup.pack();
		
		commandsContainer = new PaneContainer(commandsGroup, SWT.HORIZONTAL);
		commandsContainer.addResizable(stepGroup);
		commandsContainer.addResizable(autoGroup);
		commandsContainer.addResizable(instGroup, 20);

		commandsGroup.pack();
		
		commandsContainer.layout();

		autoPrintMenuButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) { 
				//status.setText("");
				app.addStatusMsg("");
				autoPrintModelMenu = autoPrintMenuButton.getSelection();
			}  }); 


		scale.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int digits = scale.getSelection();
				delayMS = digits;
				System.err.println("delay = "+delayMS);
			}
		});

		stimButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				app.handleStimulateEvent();
			}
		}); 
		obsButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				app.handleObserveEvent();
			}
	  }); 
		
		instGetButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				String s = app.handleGetSolutionEvent();
				if (s!=null) {
					instText.setText(s);
					instUseButton.setEnabled(true);
				}
			}
		}); 

		instUseButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				String s = instText.getText();
				Label l = new LibLabel(s);
				app.handleGivenStimulusEvent(l);
			}
		}); 

		autoButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				if (autoButton.getText().equals("Stop")) {
					autoButton.setEnabled(false);
					app.handleAutoRunStopEvent();
					//autoButton.setText("Auto");
				} else {
					String s = autoCount.getText();
					int inc = 0;
					if (!s.trim().equals("")) {
						try {
							inc = Integer.parseInt(s);
						}
						catch(NumberFormatException nfe) {
							app.addStatusMsg("stepcount is not a number: "+s);
							return;
						}
					}
					if (inc < 0)
						app.addStatusMsg("stepcount is not a non-negative number: "+s);
					else {
						autoButton.setText("Stop");
						stimButton.setEnabled(false);
						obsButton.setEnabled(false);
						app.handleAutoRunStartEvent(inc);
					}
				}
			}  }); 


	}
	
	private Group commandsGroup = null;
	private PaneContainer commandsContainer = null;
	Testgui app;
	
	private Button stimButton = null;
	private Button obsButton = null;
	private Text instText = null;
	private Button instGetButton = null;
	private Button instUseButton = null;
	private Button autoButton = null;
	private Button autoPrintMenuButton = null;
	private Scale scale  =  null;
	
	private Boolean autoPrintModelMenu = true;
	private int delayMS = 0;
	
	private boolean running = false;


}
