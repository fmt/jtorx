package utwente.fmt.jtorx.testgui;

import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.widgets.Text;

class TextDropTargetAdapter extends DropTargetAdapter {
	FileTransfer fileTransfer = FileTransfer.getInstance();
	TextTransfer textTransfer = TextTransfer.getInstance();
	public void dragEnter(DropTargetEvent event) {
		if (event.detail == DND.DROP_DEFAULT)
			event.detail = DND.DROP_COPY;
	}
	public void dragOperationChanged(DropTargetEvent event) {
		if (event.detail == DND.DROP_DEFAULT)
			event.detail = DND.DROP_COPY;
	}
	public void dragOver(DropTargetEvent event) {
		event.detail = DND.DROP_NONE;
		for(int i=0; i < event.dataTypes.length; i++) {
			if (fileTransfer.isSupportedType(event.dataTypes[i])) {
				event.currentDataType = event.dataTypes[i];
				event.detail = DND.DROP_COPY;
				return;
			}
		}
		for(int i=0; i < event.dataTypes.length; i++) {
			if (textTransfer.isSupportedType(event.dataTypes[i])) {
				event.currentDataType = event.dataTypes[i];
				event.detail = DND.DROP_COPY;
				return;
			}
		}
	}
	public void drop(DropTargetEvent event) {
		if (fileTransfer.isSupportedType(event.currentDataType)) {
			String[] files = (String[])event.data;
			if (files != null && files.length > 0) {
				myText.setText(files[0]);
				return;
			}
		}
		if (textTransfer.isSupportedType(event.currentDataType)) {
			String text = (String)event.data;
			if (text != null) {
				myText.setText(text);
				return;
			}
		}
	}
	public TextDropTargetAdapter(Text t ){
		myText = t;
	}
	private Text myText;
}
