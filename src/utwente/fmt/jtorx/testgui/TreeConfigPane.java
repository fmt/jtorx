package utwente.fmt.jtorx.testgui;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.logger.anidot.Anidot;
import utwente.fmt.jtorx.logger.anidot.DotLTS;
import utwente.fmt.jtorx.logger.mux.LogMux;
import utwente.fmt.jtorx.torx.guided.GuidedLTS;
import utwente.fmt.jtorx.torx.primer.Primer;
import utwente.fmt.jtorx.writers.AniDotExtendedLTSWriter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.LTS.LTSException;

public class TreeConfigPane implements JTorXPane {
	
	enum ColKind {
		COMP,
		VIEW,
		// BROWSE,
		RUN,
		SIM,
		FILE
	}
	/**
	 * Holds the data that we associate with a TreeItem,
	 * and that we need to manipulate when we do tree surgery,
	 * i.e. the item editors for the buttons.
	 */
	class ItemData {
		TreeEditor editors[] = new TreeEditor[ColKind.values().length]; // component, view, animate, simulate, file
		boolean isEditable[] = {false, false}; // any column beyond given is not editable
		
		boolean isExpanded = false; // need to keep track when doing surgery
		// see: http://dev.eclipse.org/newslists/news.eclipse.platform.swt/msg22287.html
		
		TreeItem item = null; // link back to item to which the data belongs
		// we use this field to have indirect access from a TreeEditor editor callback
		// to the current instance of the item to which the callback is associated.
		// we need this indirection to surive tree surgery during reparenting
		
		RunItemData rundata = null;
		LogMux.Kind kind;
	}
	
	public Composite getGroup() {
		return configGroup;
	}

	public IOLTSInterpretation getInterpretation() {
		return null;
	}

	public void runSetup() {
	}
	public void runBegins() {
	}
	public void runEnds() {
	}
	
	public void openModelFileDialog() {
	}
	public void openGuideFileDialog() {
	}
	public void openImplementationFileDialog() {
	}
	
	public boolean useGuide() {
		return false;
	}
	
	public void setGuideItemsVisibility(boolean b) {
		recursiveSetViewItemsVisibility(LogMux.Kind.GUIDE, b);
	}
	public void setImplItemsVisibility(boolean b) {
		recursiveSetViewItemsVisibility(LogMux.Kind.IMPL, b);
	}
	public void setModelItemsVisibility(boolean b) {
		recursiveSetViewItemsVisibility(LogMux.Kind.MODEL, b);
	}


	public String getModelFilename() {
		return null;
	}
	public String getImplFilename() {
		return null;
	}
	public RunItemData.AdapterKind getImplKind() {
		return null;
	}
	public String getGuideFilename() {
		return null;
	}

	public void setModelText(String s) {
		for (TreeItem i: tree.getItems())
			setItemText(i, s, LogMux.Kind.MODEL, "");
	}
	public void setGuideText(String s) {
		for (TreeItem i: tree.getItems())
			setItemText(i, s, LogMux.Kind.GUIDE, "");
	}
	public void setImplText(String s) {
		for (TreeItem i: tree.getItems())
			setItemText(i, s, LogMux.Kind.IMPL, "");
	}

	private void setItemText(TreeItem i, String s, LogMux.Kind k, String pfx) {
		if (debug)
			System.err.println("tree-config-modelset("+pfx+")("+s+")");
		ItemData subItemData = (ItemData)i.getData();
		if (debug)
			System.err.println("tree-config-modelset("+pfx+") "+i+" ...");
		int n = ColKind.COMP.ordinal();
		if (debug)
			System.err.println("tree-config-modelset("+pfx+") "+i+" "+n+" ("+i.getText(n)+") ...");
		if (i.getText(n).equals("Driver") || i.getText(n).equals("Combinator")|| i.getText(n).equals("Primer")) {
			if (debug)
				System.err.println("tree-config-modelset("+pfx+") known composite: "+i.getText(n)+" children: "+i.getItems());
			for (TreeItem j: i.getItems())
				setItemText(j, s, k, pfx+"-"+i.getText(n));
		} else {
			if (debug)
				System.err.println("tree-config-modelset("+pfx+") no known composite: "+i.getText(n));
			ItemData itemData = (ItemData)i.getData();
			int m = ColKind.FILE.ordinal();
			 if (itemData==null || itemData.rundata==null)
				 return;
			 if (itemData.kind==k) {
				 subItemData.item.setText(m, s);
				 if (debug)
						System.err.println("tree-config-modelset("+pfx+") "+i+" ("+s+")");
			 }
		}
	}

	public TreeConfigPane(Composite parent, Testgui g) {
		app = g;

		// Config Pane
		configGroup = new Composite(parent, SWT.NONE);
//		configGroup.setText("Configuration:");
		// configGroup.setLayout(new FillLayout(SWT.HORIZONTAL|SWT.VERTICAL));

		
		black = app.getDisplay().getSystemColor (SWT.COLOR_BLACK);
		
		tree = new Tree(configGroup, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL );
		tree.setHeaderVisible(true);
		tree.setLinesVisible(true);
		colComp = new TreeColumn(tree, SWT.LEFT);
		colComp.setText("Component");
		colComp.setWidth(100);
		//colBrowse = new TreeColumn(tree, SWT.CENTER);
		//colBrowse.setText("Browse");

		colView = new TreeColumn(tree, SWT.CENTER);
		colView.setText("View");		
		colRun = new TreeColumn(tree, SWT.CENTER);
		colRun.setText("Run");
		//colFile.setWidth(50);
		
		colSim = new TreeColumn(tree, SWT.CENTER);
		colSim.setText("Simulate");

		
		colFile = new TreeColumn(tree, SWT.LEFT);
		colFile.setText("File");
		colFile.setWidth(10);
		colFile.setAlignment(SWT.LEFT);


		final Menu menu = new Menu (tree);
		if(false)
			tree.setMenu (menu);

		MenuItem m;
		m = new MenuItem (menu, SWT.PUSH);
		m.setText ("Add Model");
		m.addListener(SWT.Selection, new Listener() {
			public void handleEvent (Event event) {
				addModel();
			}
		});
		m = new MenuItem (menu, SWT.PUSH);
		m.setText ("Add Impl");
		m.addListener(SWT.Selection, new Listener() {
			public void handleEvent (Event event) {
				addImpl();
			}
		});
		m = new MenuItem (menu, SWT.PUSH);
		m.setText ("Add Guide");
		m.addListener(SWT.Selection, new Listener() {
			public void handleEvent (Event event) {
				addGuide();
			}
		});

		menu.addListener (SWT.Show, new Listener () {
			public void handleEvent (Event event) {
				MenuItem [] menuItems = menu.getItems ();
				TreeItem [] treeItems = tree.getSelection ();
				for (int i=0; i<menuItems.length; i++) {
					String text = menuItems [i].getText ();
					int index = 0;
					while (index<treeItems.length) {
						if (treeItems [index].getText ().equals (text)) break;
						index++;
					}
					menuItems [i].setEnabled (index != treeItems.length);
				}
				menuItems [0].setEnabled (true);
				menuItems [1].setEnabled (true);
				menuItems [2].setEnabled (true);
			}
		});
		

		final TreeEditor editor = new TreeEditor (tree);
		tree.addMouseListener(new MouseAdapter() {
			public void mouseDoubleClick(MouseEvent event) {
				Point point = new Point (event.x, event.y);
				final TreeItem item = tree.getItem(point);
				if (item==null)
					return;
				ItemData data = (ItemData)item.getData();
				if (data==null)
					return; //should not happen
				for(int i = 0; i < tree.getColumnCount(); i++) {
					Rectangle rect = item.getBounds(i);
					if (rect.contains(point)) {
						// this is the column that was clicked
						final int column = i;
						if (i<data.isEditable.length && data.isEditable[i]) {
							boolean showBorder = true;
							final Composite composite = new Composite (tree, SWT.NONE);
							if (showBorder) composite.setBackground (black);
							final Text text = new Text (composite, SWT.NONE);
							final int inset = showBorder ? 1 : 0;
							composite.addListener (SWT.Resize, new Listener () {
								public void handleEvent (Event e) {
									Rectangle rect = composite.getClientArea ();
									text.setBounds (rect.x + inset, rect.y + inset, rect.width - inset * 2, rect.height - inset * 2);
								}
							});
							Listener textListener = new Listener () {
								public void handleEvent (final Event e) {
									switch (e.type) {
									case SWT.FocusOut:
										item.setText (column, text.getText ());
										composite.dispose ();
										break;
									case SWT.Verify:
										String newText = text.getText ();
										String leftText = newText.substring (0, e.start);
										String rightText = newText.substring (e.end, newText.length ());
										GC gc = new GC (text);
										Point size = gc.textExtent (leftText + e.text + rightText);
										gc.dispose ();
										size = text.computeSize (size.x, SWT.DEFAULT);
										editor.horizontalAlignment = SWT.LEFT;
										Rectangle itemRect = item.getBounds (), rect = tree.getClientArea ();
										editor.minimumWidth = Math.max (size.x, itemRect.width) + inset * 2;
										int left = itemRect.x, right = rect.x + rect.width;
										editor.minimumWidth = Math.min (editor.minimumWidth, right - left);
										editor.minimumHeight = size.y + inset * 2;
										editor.grabHorizontal = editor.grabVertical = true;
										editor.layout ();
										break;
									case SWT.Traverse:
										switch (e.detail) {
										case SWT.TRAVERSE_RETURN:
											item.setText (column, text.getText ());
											//FALL THROUGH
										case SWT.TRAVERSE_ESCAPE:
											composite.dispose ();
											e.doit = false;
										}
										break;
									}
								}
							};
							text.addListener (SWT.FocusOut, textListener);
							text.addListener (SWT.Traverse, textListener);
							text.addListener (SWT.Verify, textListener);
							editor.setEditor (composite, item, column);
							text.setText (item.getText(column));
							text.selectAll ();
							text.setFocus ();
						}
					}
				}
			}
		});
		
		
		configGroup.addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				Rectangle area = configGroup.getClientArea();
				Point size = configGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT);
				ScrollBar vBar = tree.getVerticalBar();
				int width = area.width - (tree.computeTrim(0,0,0,0).width  + vBar.getSize().x);
				if (debug)
					System.err.println("tree-cfggroup area: "+area.width+" tree: "+tree.computeTrim(1,1,1,1).width+" gridwidth: "+tree.getGridLineWidth());
				if (size.y > area.height + tree.getHeaderHeight()) {
					// Subtract the scrollbar width from the total column width
					// if a vertical scrollbar will be required
					Point vBarSize = vBar.getSize();
					width -= vBarSize.x;
				}
				Point oldSize = tree.getSize();
				int others;
				int remains;
				if (oldSize.x > area.width) {
					// table is getting smaller so make the columns 
					// smaller first and then resize the table to
					// match the client area width
					others = colComp.getWidth()+colView.getWidth()+colRun.getWidth()+colSim.getWidth();
					remains = width - others;
					colFile.setWidth(remains);
					tree.setSize(area.width, area.height);
				} else {
					// table is getting bigger so make the table 
					// bigger first and then make the columns wider
					// to match the client area width
					tree.setSize(area.width, area.height);
					others = colComp.getWidth()+colView.getWidth()+colRun.getWidth()+colSim.getWidth();
					remains = width - others;
					colFile.setWidth(remains);
				}
				if (debug)
					System.err.println("tree-cfggroup others: "+others+
						"remains: "+remains);
				if (debug)
					System.err.println("tree-cfggroup area: "+area.width+
						" tree: "+tree.computeTrim(1,1,1,1).width+
						" comp: "+colComp.getWidth()+" view: "+colView.getWidth()+
						" run: "+colRun.getWidth()+
						" sim: "+colSim.getWidth()+
						" file: "+colFile.getWidth());
			}
		});

		configGroup.setLayout( new FillLayout() );
		
		Button tmp;
		tmp = new Button(configGroup, SWT.PUSH);

		//tmp.setText("browse");
		//tmp.pack();
		//colBrowse.setWidth(tmp.getBounds().width);
		tmp.setText("view");
		tmp.pack();
		colView.setWidth(tmp.getBounds().width);
		tmp.setText("simulate");
		tmp.pack();
		colSim.setWidth(tmp.getBounds().width);
		tmp.dispose();
		
		tmp = new Button(configGroup, SWT.CHECK);
		tmp.setText("viz ");
		tmp.pack();
		colRun.setWidth(tmp.getBounds().width);
		tmp.dispose();
		

		addModel();
		addGuide();
		addImpl();
		
		/*
		 * NOTE: MeasureItem, PaintItem and EraseItem are called repeatedly.
		 * Therefore, it is critical for performance that these methods be
		 * as efficient as possible.
		 */
		Listener paintListener = new Listener() {
			public void handleEvent(Event event) {
				switch(event.type) {		
					case SWT.MeasureItem: {
						TreeItem item = (TreeItem)event.item;
						String text = getText(item, event.index);
						Point size;
						size = event.gc.textExtent(text);
						if (event.index==ColKind.VIEW.ordinal() || event.index==ColKind.RUN.ordinal()) {
							ItemData data = (ItemData) item.getData();
							if (data!=null) {
								TreeEditor e = data.editors[event.index];
								if (e!=null) {
									Rectangle r = e.getEditor().getBounds();
									size.x = r.width;
									size.y = r.height;
									if (debug)
										System.err.println("paintlistener measyer item"+event.index+" ("+size+")");
								}
							}
						}
						event.width = size.x;
						event.height = Math.max(event.height, size.y);
						if (debug)
							System.err.println("paintlistener measyer "+event.index+" ("+event.width+","+event.height+")");
						break;
					}
					case SWT.PaintItem: {
						TreeItem item = (TreeItem)event.item;
						String text = getText(item, event.index);
						Point size = event.gc.textExtent(text);					
						int offset2 = event.index == 0 ? Math.max(0, (event.height - size.y) / 2) : 0;
						event.gc.drawText(text, event.x, event.y + offset2, true);
						break;
					}
					case SWT.EraseItem: {	
						event.detail &= ~SWT.FOREGROUND;
						break;
					}
				}
			}
			String getText(TreeItem item, int column) {
				String text = item.getText(column);
				if (column != 0) {
					TreeItem parent = item.getParentItem();
					int index = parent == null ? tree.indexOf(item) : parent.indexOf(item);
					if (false && (index+column) % 3 == 1){
						text +="\nnew line";
					}
					if (false && (index+column) % 3 == 2) {
						text +="\nnew line\nnew line";
					}
				}
				return text;
			}
		};
		tree.addListener(SWT.MeasureItem, paintListener);
		tree.addListener(SWT.PaintItem, paintListener);
		tree.addListener(SWT.EraseItem, paintListener);

			
		tree.pack();

	}
		
	private void addModel() {
		storeExpanded(tree.getItems());
		if (driverItem==null)
			driverItem = addDriverItem(tree);
		TreeItem parent = (combinatorItem==null? driverItem : combinatorItem);
		TreeItem primerItem = addPrimerItem(tree, parent);
		TreeItem modelItem =  addModelItem(tree, primerItem);
		useExpanded(tree.getItems()); // have to do this _after_ surgery
		tree.showItem(modelItem); // have to do this _after_ useExpanded
		tree.update();
		for(TreeEditor e: treeEditors)
			e.layout(); // needed to force immediate (re)positioning
	}
	
	private void addImpl() {
		storeExpanded(tree.getItems());
		if (driverItem==null)
			driverItem = addDriverItem(tree);
		TreeItem guideItem = addImplItem(tree, driverItem);
		useExpanded(tree.getItems()); // have to do this _after_ surgery
		tree.showItem(guideItem); // have to do this _after_ useExpanded
		tree.update();
		for(TreeEditor e: treeEditors)
			e.layout(); // needed to force immediate (re)positioning
	}
	
	public void addGuide() {
		if (combinatorItem!=null)
			return;

		storeExpanded(tree.getItems());
		if (driverItem==null)
			driverItem = addDriverItem(tree);
		if (combinatorItem==null)
			combinatorItem = addCombinatorItem(tree, driverItem);
		TreeItem primerItem = addPrimerItem(tree, combinatorItem);
		TreeItem guideItem = addGuideItem(tree, primerItem);
		useExpanded(tree.getItems()); // have to do this _after_ surgery
		tree.showItem(guideItem); // have to do this _after_ useExpanded
		tree.update();
		for(TreeEditor e: treeEditors)
			e.layout(); // needed to force immediate (re)positioning
	}
	
	public void rmGuide() {
		if (combinatorItem==null)
			return;

		storeExpanded(tree.getItems());
		// unlike Model-Primer from Combinator, and
		//    - if Driver present, reatach to Driver
		//    - otherwise, have at root
		// remove Guide
		// remove Primer of Guide
		// remove Combinator
		for(TreeItem i: combinatorItem.getItems()) {
			if (isModel(i))
				reparent(i, driverItem, tree);
		}
		recusiveDispose(combinatorItem);
		combinatorItem = null;
		useExpanded(tree.getItems()); // have to do this _after_ surgery
		tree.update();
		for(TreeEditor e: treeEditors)
			e.layout(); // needed to force immediate (re)positioning
	}
	
	private void recusiveDispose(TreeItem i) {
		for(TreeItem j: i.getItems()) {
			recusiveDispose(j);
		}
		ItemData data = (ItemData)i.getData();
		for(TreeEditor e: data.editors) {
			if (e!=null) {
				Control c = e.getEditor();
				e.dispose();
				if (c!=null)
					c.dispose();
			}
		}
		i.dispose();
	}
	
	private void recursiveSetViewItemsVisibility(LogMux.Kind k, boolean b) {
		doRecursiveSetViewItemsVisibility(null, k, b);
	}

	private boolean doRecursiveSetViewItemsVisibility(TreeItem i, LogMux.Kind k, boolean b) {
		if (debug)
			System.out.println("doRecursiveSetViewItemsVisibility "+i+" "+k+" "+b);
		boolean hasType = false;
		for(TreeItem j: (i==null?tree.getItems() : i.getItems())) {
			hasType = doRecursiveSetViewItemsVisibility(j, k, b) || hasType;
		}
		if (i==null)
			return hasType;
		ItemData data = (ItemData)i.getData();
		if (data.kind==k || hasType) {
			for(TreeEditor e : data.editors) {
				if (e!=null) {
					Control c = e.getEditor();
					if (c!=null) {
						c.setVisible(b);
						if (debug)
							System.out.println("\tset doRecursiveSetViewItemsVisibility "+i+" "+k+" "+b);
					}
				}
			}
		}
		return hasType;

	}

	private TreeItem addDriverItem(Tree t) {
		return addItem(t, "Driver", false, false);
	}
	
	private TreeItem addPrimerItem(Tree t, TreeItem p) {
		TreeItem item =  addItem(t, p, "Primer", false, true);
		ItemData data = (ItemData)item.getData();
		data.kind = LogMux.Kind.SUSPAUT;
		data.rundata = null; // to be set when child is added
		addViewPrimerListener(data);
		addSimPrimerListener(data);
		return item;
	}
	
	private TreeItem addCombinatorItem(Tree t, TreeItem p) {
		TreeItem treeItem = addItem(t, p, "Combinator", false, true);
		for(TreeItem i:p.getItems()) {
			if (i != treeItem && i.getData()!=null && ((ItemData)i.getData()).kind!=LogMux.Kind.IMPL)
				reparent(i, treeItem);
		}
		ItemData data = (ItemData)treeItem.getData();
		data.kind = LogMux.Kind.SUSPAUT;
		data.rundata = app.getCombinatorData();
		addViewCombinatorListener(data);
		addSimCombinatorListener(data);
		return treeItem;
	}
		
	private TreeItem addModelItem(Tree t, TreeItem p) {
		TreeItem item = addItem(t, p, "Model", true, true);
		ItemData data = (ItemData)item.getData();
		data.kind = LogMux.Kind.MODEL;
		data.rundata = app.getModelData();
		addViewLTSListener(data);
		addSimLTSListener(data);
		ItemData pdata = (ItemData)p.getData();
		pdata.rundata = app.getModelPrimerData();
		return item;
	}
	
	private TreeItem addGuideItem(Tree t, TreeItem p) {
		TreeItem item = addItem(t, p, "Guide", true, true);
		ItemData data = (ItemData)item.getData();
		data.kind = LogMux.Kind.GUIDE;
		data.rundata = app.getGuideData();
		addGuideViewLTSListener(data);
		addGuideSimLTSListener(data);
		ItemData pdata = (ItemData)p.getData();
		pdata.rundata = app.getGuidePrimerData();
		return item;
	}
	
	private TreeItem addImplItem(Tree t, TreeItem p) {
		TreeItem item = addItem(t, p, "Impl", true, true/*depends on kind*/);
		ItemData data = (ItemData)item.getData();
		data.kind = LogMux.Kind.IMPL;
		data.rundata = app.getImplData();
		addViewLTSListener(data);
		addSimLTSListener(data);
		return item;
	}
	
	private TreeItem addItem(Tree t, String s, boolean hasFile, boolean hasView) {
		TreeItem treeItem = new TreeItem (t, SWT.NONE);
		return populateItem(t, treeItem, s, hasFile, hasView);
	}
	private TreeItem addItem(Tree t, TreeItem p, String s, boolean hasFile, boolean hasView) {
		TreeItem treeItem = new TreeItem (p, SWT.NONE);
		return populateItem(t, treeItem, s, hasFile, hasView);
	}
	private TreeItem populateItem(Tree t, TreeItem i, String s, boolean hasFile, boolean hasView) {
		String[] text = new String[]{s, "", "", ""};
		i.setText(text);
		final ItemData data = new ItemData();
		int j = ColKind.RUN.ordinal();
		i.setData(data);
		data.item = i;
		data.editors[j] = addButtonItem(t, i, j, "viz", SWT.CHECK);
		app.registerDotVizButton((Button) data.editors[j].getEditor());
		data.isExpanded = true;
		i.setExpanded(true);
		return i;
	}
	
	private void addViewButton(final ItemData data) {
		int j = ColKind.VIEW.ordinal();
		data.editors[j] = addButtonItem(data.item.getParent(), data.item, j, "View", SWT.PUSH);
		app.registerDotVizButton((Button) data.editors[j].getEditor());

	}
	private void addSimButton(final ItemData data) {
		int j = ColKind.SIM.ordinal();
		data.editors[j] = addButtonItem(data.item.getParent(), data.item, j, "Simulate", SWT.PUSH);
	}

	
	private void addViewLTSListener(final ItemData data) {
		addViewButton(data);
		int j = ColKind.VIEW.ordinal();
		data.editors[j].getEditor().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				// access text cell indirectly via data.item,
				// to surive tree surgery in which TreeItem are disposed
				// and recreated
				int k = ColKind.FILE.ordinal();
				String fileName = data.item.getText(k);
				app.doHandleViewEvent(data.rundata, fileName, data.rundata.getLTSKind(), data.kind);
			} 
		}); 
	}
	
	private void addSimLTSListener(final ItemData data) {
		addSimButton(data);
		int j = ColKind.SIM.ordinal();
		data.editors[j].getEditor().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				// access text cell indirectly via data.item,
				// to surive tree surgery in which TreeItem are disposed
				// and recreated
				//int k = ColKind.FILE.ordinal();
				app.doHandleSimulateLTSEvent(data.rundata, data.kind);
			} 
		}); 
	}
	
	private void addGuideSimLTSListener(final ItemData data) {
		addSimButton(data);
		int j = ColKind.SIM.ordinal();
		data.editors[j].getEditor().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				// access text cell indirectly via data.item,
				// to surive tree surgery in which TreeItem are disposed
				// and recreated
				//int k = ColKind.FILE.ordinal();
				app.doHandleSimulateLTSEvent(data.rundata, data.kind);
			} 
		}); 
	}
	private void addSimPrimerListener(final ItemData data) {
		addSimButton(data);
		int j = ColKind.SIM.ordinal();
		data.editors[j].getEditor().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				// access text cell indirectly via data.item,
				// to surive tree surgery in which TreeItem are disposed
				// and recreated
				//int k = ColKind.FILE.ordinal();
				app.doHandleSimulateModelPrimerEvent(data.rundata, data.kind);
			} 
		}); 
	}
	private void addSimCombinatorListener(final ItemData data) {
		addSimButton(data);
		int j = ColKind.SIM.ordinal();
		data.editors[j].getEditor().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				// access text cell indirectly via data.item,
				// to surive tree surgery in which TreeItem are disposed
				// and recreated
				//int k = ColKind.FILE.ordinal();
				app.doHandleSimulateLTSEvent(data.rundata, data.kind);
			} 
		}); 
	}


	private void addGuideViewLTSListener(final ItemData data) {
		addViewButton(data);
		int j = ColKind.VIEW.ordinal();
		data.editors[j].getEditor().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				// access text cell indirectly via data.item,
				// to surive tree surgery in which TreeItem are disposed
				// and recreated
				//int k = ColKind.FILE.ordinal();
				//String fileName = data.item.getText(k);
				//app.doHandleViewEvent(data.rundata, fileName, data.kind);
				data.rundata.setLTS(app.doHandleViewGuideEvent());
			} 
		}); 
	}

	
	
	private void addViewPrimerListener(final ItemData data) {
		addViewButton(data);
		int j = ColKind.VIEW.ordinal();
		data.editors[j].getEditor().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				app.getErrorReporter().report("");
				// access text cell indirectly via data.item,
				// to surive tree surgery in which TreeItem are disposed
				// and recreated
				
				// lookup Explorer child for which we have to create a Driver,
				// if it is not yet there
				if (data.item.getItemCount() != 1) {
					app.getErrorReporter().report("Primer: View: no Explorer child");
					return;
				}
				ItemData subItemData = (ItemData)data.item.getItems()[0].getData();
				LTS lts = subItemData.rundata.getLTS();
				IOLTSInterpretation cfgInterp = app.getInterpretation();
				TraceKind traceKind = app.getTraceKind();

				missing.reset();
				if (subItemData.kind==LogMux.Kind.GUIDE)
					missing.check(lts == null, "no guide lts");
				else
					missing.check(lts == null, "no model lts");
				missing.check(cfgInterp == null, "no interpretation");
				missing.check(traceKind == null, "no trace kind");
				if (!missing.getMissing().equals("")) {
					app.getErrorReporter().report("Primver: View: "+missing.getMissing());
					return;
				}


				boolean tracesOnly = !app.getSynthDelta() /*tracesOnly*/;
				boolean angelicCompletion = app.getAngelicCompletion();
				boolean divergence = app.getDivergence();
				if (subItemData.kind==LogMux.Kind.GUIDE) {
					cfgInterp = new QuiescenceInterpretation(cfgInterp);
					tracesOnly = true;
				}
				final IOLTSInterpretation interp = cfgInterp;
				data.rundata.setLTS(new Primer(lts, interp,
							tracesOnly,
							true/*remember states for matching*/,
							traceKind,
							angelicCompletion,
							divergence,
							app.getErrorReporter(), app.getProgressReporter()));
				data.rundata.setFilename(null, null);
				if (data.rundata.getAnidot() != null)
					data.rundata.getAnidot().end();
				new Thread("run-anidot-viewer") {
					public void run() {
				Anidot anidot = app.getLogMux().newAnidot("suspension automaton", data.kind);
				if (anidot != null) {
					Iterator<?extends utwente.fmt.lts.State> it = null;
					try {
						it = data.rundata.getLTS().init();
						DotLTS dotLTS = new DotLTS(anidot);
						anidot.setRender(Anidot.RenderMode.OFF);
						new AniDotExtendedLTSWriter(data.rundata.getCompoundLTS(), interp, dotLTS, app.getStateSpaceProgressReporter() ).write();
						dotLTS.addRoot("suspension automaton", null);

						while(it.hasNext())
							dotLTS.addInitialEdge(it.next());
						anidot.setRender(Anidot.RenderMode.ON);
					} catch (LTSException e) {
						System.err.println("addViewPrimerListener: caught LTSException: "+e.getMessage());
					}

					anidot.end();
						// rid.clear(); // not needed here, to allow reuse?
				}
					}
				}.start();
			} 
		}); 
	}
	private TreeItem getChild(TreeItem treeItem) {
		if (treeItem.getItemCount()==1)
			return treeItem.getItem(0);
		return null;
	}
	private boolean isModel(TreeItem item) {
		ItemData itemData = (ItemData)item.getData();
		if (itemData==null || itemData.rundata==null)
			return false;
		if (itemData.kind==LogMux.Kind.MODEL)
			return true;
		if (itemData.kind==LogMux.Kind.SUSPAUT && getChild(item)!=null)
			return isModel(getChild(item));
		else
			return false;
	}
	private boolean isGuide(TreeItem item) {
		ItemData itemData = (ItemData)item.getData();
		if (itemData==null || itemData.rundata==null)
			return false;
		if (itemData.kind==LogMux.Kind.GUIDE)
			return true;
		if (itemData.kind==LogMux.Kind.SUSPAUT && getChild(item)!=null)
			return isGuide(getChild(item));
		else
			return false;
	}

	private void addViewCombinatorListener(final ItemData data) {
		addViewButton(data);
		int j = ColKind.VIEW.ordinal();
		data.editors[j].getEditor().addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				// access text cell indirectly via data.item,
				// to surive tree surgery in which TreeItem are disposed
				// and recreated
				
				// lookup Explorer child for which we have to create a Driver,
				// if it is not yet there
				missing.reset();
				IOLTSInterpretation interp = app.getInterpretation();

				Set<CompoundLTS> specs = new HashSet<CompoundLTS>();
				Set<CompoundLTS> guides = new HashSet<CompoundLTS>();
				for (TreeItem i: data.item.getItems()) {
					ItemData subItemData = (ItemData)i.getData();
					if (isGuide(i)) {
						missing.check(subItemData.rundata.getCompoundLTS() == null, "no guide lts");
						guides.add(subItemData.rundata.getCompoundLTS());
					}
					else if (isModel(i)) {
						missing.check(subItemData.rundata.getCompoundLTS() == null, "no model lts");
						specs.add(subItemData.rundata.getCompoundLTS());
					}
				}
				missing.check(interp == null, "no interpretation");

				if (!missing.getMissing().equals("")) {
					app.getErrorReporter().report("Combinator: View: "+missing.getMissing());
					return;
				}

				CompoundLTS myGuidedPrimer = new GuidedLTS(specs, guides, interp, true/*enableCache*/, app.getErrorReporter());
				data.rundata.setFilename(null, null);
				data.rundata.setLTS(myGuidedPrimer);
				if (data.rundata.getAnidot() != null)
					data.rundata.getAnidot().end();
				Anidot anidot = app.getLogMux().newAnidot("suspension automaton", data.kind);
				if (anidot != null) {
					Iterator<?extends State> it = null;
					try {
						it = data.rundata.getLTS().init();
						DotLTS dotLTS = new DotLTS(anidot);
						anidot.setRender(Anidot.RenderMode.OFF);
						new AniDotExtendedLTSWriter(myGuidedPrimer, interp, dotLTS, app.getStateSpaceProgressReporter() ).write();
						dotLTS.addRoot("guided suspension automaton", null);
						
						while(it.hasNext())
							dotLTS.addInitialEdge(it.next());
						anidot.setRender(Anidot.RenderMode.ON);
					} catch (LTSException e1) {
						System.err.println("addViewCombinatorListener: caught LTSException: "+e1.getMessage());
					}
					anidot.end();
						// rid.clear(); // not needed here, to allow reuse?
				}

			} 
		}); 

	}

	
	private TreeEditor addButtonItem(Tree t, TreeItem i, int c, String s, int style) {
		Button b = new Button(t,style);
		b.setText(s);
		b.setSize(i.getBounds().width, i.getBounds().height);
		b.setBounds(i.getBounds());
		//b.pack();
		TreeEditor e = new TreeEditor(t);
		e.grabHorizontal = true; //= e.grabVertical = true;
		e.setEditor (b, i, c);
		treeEditors.add(e);
		return e;
	}
	
	private TreeItem reparent(TreeItem o, TreeItem p, Tree t) {
		if (p!=null)
			return reparent(o, p);
		else
			return reparent(o, t);
	}
	
	private TreeItem reparent(TreeItem o, TreeItem p) {
		TreeItem n = new TreeItem (p, o.getStyle(), 0);
		return doreparent(o, n);
	}
	private TreeItem reparent(TreeItem o, Tree p) {
		TreeItem n = new TreeItem (p, o.getStyle(), 0);
		return doreparent(o, n);
	}
	private TreeItem doreparent(TreeItem o, TreeItem n) {
		ItemData data = (ItemData)o.getData();
		data.item = n;
		n.setData(data);
		updateEditors(data, o, n);
		n.setImage(o.getImage());
		for(int j=0; j<o.getParent().getColumnCount(); j++)
			n.setText(j, o.getText(j));
		for(TreeItem j:o.getItems())
			reparent(j, n);
		o.dispose();
		return n;
	}

	private void updateEditors(ItemData d, TreeItem o, TreeItem n) {
		if (d == null)
			return;
		for(TreeEditor e: d.editors)
			if (e!=null)
				e.setItem(n);
	}

	private void storeExpanded(TreeItem items[]) {
		ItemData data;
		for (TreeItem i: items) {
			data = (ItemData)i.getData();
			if (data!=null) {
				data.isExpanded = i.getExpanded();
				if (debug)
					System.out.println("storeExpanded: tree item "+i+" isExpanded "+data.isExpanded);
				storeExpanded(i.getItems());
			}
		}
	}
	
	private void useExpanded(TreeItem items[]) {
		ItemData data;
		for (TreeItem i: items) {
			data = (ItemData)i.getData();
			if (data!=null) {
				i.setExpanded(data.isExpanded);
				if (debug)
					System.out.println("useExpanded: tree item "+i+" isExpanded "+data.isExpanded);
				useExpanded(i.getItems());
			}
		}
	}
	
	private Testgui app;
	private Composite configGroup = null;
	
	private RollCall missing = new RollCall();
	private Tree tree = null;
	private TreeItem driverItem = null;
	private TreeItem combinatorItem = null;
	
	private TreeColumn colComp = null;
	private TreeColumn colView = null;
	//private TreeColumn colBrowse = null;
	private TreeColumn colRun = null;
	private TreeColumn colFile = null;
	private TreeColumn colSim = null;
	
	private Vector<TreeEditor> treeEditors = new Vector<TreeEditor>();
	
	private Color black;
	private boolean debug = false;
}
