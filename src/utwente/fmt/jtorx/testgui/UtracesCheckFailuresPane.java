package utwente.fmt.jtorx.testgui;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import utwente.fmt.jtorx.utracechecker.UtraceCheckerResult;
import utwente.fmt.jtorx.utracechecker.UtraceLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class UtracesCheckFailuresPane  implements JTorXPane {

	public void runSetup() {
	}
	public void runBegins() {
	}
	public void runEnds() {
	}

	public Group getGroup() {
		return failuresGroup;
	}
	
	public LTS getSelectedLTS() {
		if (failures==null) {
			app.addStatusMsg("no iocoChecker failure results");
			return null;
		}

		int idx = failuresTable.getSelectionIndex();
		if (idx<0) {
			app.addStatusMsg("no utraceChecker failures item selected (selidx="+idx+")");
			return null;
		}
		TableItem selected = failuresTable.getItem(idx);
		if (selected==null) {
			app.addStatusMsg("could not get selected item");
			return null;
		}
		List<CompoundTransition<?extends CompoundState>> t = (List<CompoundTransition<?extends CompoundState>>)selected.getData("trace");

		if (t==null) {
			app.addStatusMsg("could not get selected item");
			return null;		
		}
		return new UtraceLTS(t);
	}
	
	public LTS getLTS() {
		if (failures==null)
			return null;
		return failures.getLTS();
	}

	public String getFileName() {
		if (failures==null)
			return null;
		return failures.getFileName();
	}

	public void showFailures(UtraceCheckerResult r) {
		failuresTable.removeAll();
		failures = r;
		
		if (r==null) {
			failuresGroup.setText("Check delivered no result (messages pane may explain why)");
			failuresTable.setVisible(false);
			app.setUtracesCheckSelectionItemsVisibility(false);
			return;
		}
		Collection<? extends CompoundState> sl = r.getStates();
		if (sl==null || sl.size() == 0) {
			failuresGroup.setText("The Utraces are identical to the Straces.");
			failuresTable.setVisible(false);
			app.setUtracesCheckSelectionItemsVisibility(false);
		} else {
			failuresGroup.setText("Differences between Straces and Utraces of the specification:");
			failuresTable.setVisible(true);
			app.setUtracesCheckSelectionItemsVisibility(true);

			for(CompoundState s: sl) {
				List<CompoundTransition<?extends CompoundState>> t = r.getTrace(s);
				Collection<? extends Transition<? extends State>> i = r.getTransitions(s);
				if (t!=null || i!=null) {
					TableItem item = new TableItem (failuresTable, SWT.NONE);
					item.setText(0, (t!=null?printTrace(t.iterator()):""));
					item.setText(1, (s!=null?s.getLabel(false): ""));
					item.setText(2, (i!=null?printLabels(i.iterator()):""));
					item.setText(3, (i!=null?printSourceStates(i.iterator()):""));
					item.setData("trace", t);
				}
			}
		}
	}
	
	/*
	public LTS getSelectedLTS() {
		if (failures==null) {
			app.addStatusMsg("no iocoChecker failure results");
			return null;
		}
		List<LTS> ltsList = failures.getLTSResult();
		int idx = failuresTable.getSelectionIndex();
		if (ltsList==null) {
			app.addStatusMsg("no iocoChecker failure LTS results");
			return null;
		}
		if (idx<0) {
			app.addStatusMsg("no iocoChecker failures item selected (selidx="+idx+")");
			return null;
		}
		if (idx >= ltsList.size()) {
			app.addStatusMsg("no LTS for iocoChecker failures selection (selidx="+idx+">=#LTS="+ltsList.size()+")");
			return null;
		}
		return ltsList.get(idx);
	}
	
	public LTS getCombinedLTS() {
		if (failures==null) {
			app.addStatusMsg("no iocoChecker failure results");
			return null;
		}
		return failures.getLTSCombinedResult();
	}
	*/

	public UtracesCheckFailuresPane(Composite parent, Testgui g) {
		app = g;

		failuresGroup = new Group(parent, SWT.NONE);
		failuresGroup.setLayout(new FillLayout(SWT.HORIZONTAL|SWT.VERTICAL));
		failuresGroup.setText("Results:");

		failuresTable = new Table(failuresGroup, SWT.BORDER | SWT.FULL_SELECTION | SWT.VIRTUAL);
		failuresTable.setLayout(new FillLayout(SWT.HORIZONTAL|SWT.VERTICAL));
		failuresTable.setLinesVisible (true);
		failuresTable.setHeaderVisible (true);

		traceColumn = new TableColumn (failuresTable, SWT.NONE);
		traceColumn.setText("Suspension Trace leading to");
		suspAutStateColumn = new TableColumn (failuresTable, SWT.NONE);
		suspAutStateColumn.setText("Suspension Automaton State");
		inputsColumn = new TableColumn (failuresTable, SWT.NONE);
		inputsColumn.setText("Omitted Inputs");
		ltsStatesColumn = new TableColumn (failuresTable, SWT.NONE);
		ltsStatesColumn.setText("Enabled in Model State(s)");


		traceColumn.pack();
		suspAutStateColumn.pack();
		inputsColumn.pack();

		failuresGroup.pack();
		
		failuresGroup.setText("Results:");
		failuresTable.setVisible(false);

		failuresGroup.addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				Rectangle area = failuresGroup.getClientArea();
				Point size = failuresTable.computeSize(SWT.DEFAULT, SWT.DEFAULT);
				ScrollBar vBar = failuresTable.getVerticalBar();
				int width = area.width - failuresTable.computeTrim(0,0,0,0).width; // - vBar.getSize().x;
				if (size.y > area.height + failuresTable.getHeaderHeight()) {
					// Subtract the scrollbar width from the total column width
					// if a vertical scrollbar will be required
					Point vBarSize = vBar.getSize();
					width -= vBarSize.x;
				}
				Point oldSize = failuresTable.getSize();
				if (oldSize.x > area.width) {
					// table is getting smaller so make the columns 
					// smaller first and then resize the table to
					// match the client area width
					traceColumn.setWidth(width/4);
					ltsStatesColumn.setWidth((width - traceColumn.getWidth())/3);
					suspAutStateColumn.setWidth((width - traceColumn.getWidth() - ltsStatesColumn.getWidth())/2);
					inputsColumn.setWidth(width - traceColumn.getWidth() - ltsStatesColumn.getWidth() - suspAutStateColumn.getWidth());
					failuresTable.setSize(area.width, area.height);
				} else {
					// table is getting bigger so make the table 
					// bigger first and then make the columns wider
					// to match the client area width
					failuresTable.setSize(area.width, area.height);
					traceColumn.setWidth(width/4);
					ltsStatesColumn.setWidth((width - traceColumn.getWidth())/3);
					suspAutStateColumn.setWidth((width - traceColumn.getWidth() - ltsStatesColumn.getWidth())/2);
					inputsColumn.setWidth(width - traceColumn.getWidth() - ltsStatesColumn.getWidth() - suspAutStateColumn.getWidth());
				}
			}
		});
	}
	
	private String printTrace(Iterator<CompoundTransition<?extends CompoundState>> it) {
		String s = "";
		if (it.hasNext())
			s += it.next().getLabel().getString();
		while(it.hasNext()) {
			s += " . ";
			s += it.next().getLabel().getString();
		}
		return s;
	}
	private String printLabels(Iterator<? extends Transition<? extends State>> it) {
		Set<Label> labels = new HashSet<Label>();
		Label l;
		String s = "";
		if (it.hasNext()) {
			l = it.next().getLabel();
			labels.add(l);
			s += "{ ";
			s += l.getString();
		}
		while(it.hasNext()) {
			l = it.next().getLabel();
			if (! labels.contains(l)) {
				s += ", ";
				s += l.getString();
			}
		}
		if (!s.equals(""))
			s += " }";
		return s;
	}

	private String printSourceStates(Iterator<? extends Transition<? extends State>> it) {
		Set<State> states = new HashSet<State>();
		State p;
		String s = "";
		if (it.hasNext()) {
			p = it.next().getSource();
			states.add(p);
			s += "{ ";
			s += p.getLabel(true);
		}
		while(it.hasNext()) {
			p = it.next().getSource();
			if (! states.contains(p)) {
				s += ", ";
				s += p.getLabel(true);
			}
		}
		if (!s.equals(""))
			s += " }";
		return s;
	}

	private Testgui app;

	private Group failuresGroup = null;
	private Table failuresTable = null;
	private TableColumn traceColumn = null;
	private TableColumn suspAutStateColumn = null;
	private TableColumn inputsColumn = null;
	private TableColumn ltsStatesColumn = null;
	private UtraceCheckerResult failures = null;
}

