package utwente.fmt.jtorx.testgui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;


public class UtracesCheckPane implements JTorXPane {

	public void runSetup() {
		//checkButton.setText("Stop");
		checkButton.setEnabled(false);
		simGroup.setEnabled(false);
	}
	public void runBegins() {
		//checkButton.setText("Stop");
		checkButton.setEnabled(false);
		simGroup.setEnabled(false);
	}
	public void runEnds() {
		//checkButton.setText("Check Model");
		checkButton.setEnabled(true);
		simGroup.setEnabled(true);
	}

	public Group getGroup() {
		return checkGroup;
	}
	
	public void setSelectionItemsVisibility(boolean b) {
		simGroup.setVisible(b);
	}

	public UtracesCheckPane(Composite parent, Testgui g) {
		app = g;

		// Run Pane
		checkGroup = new Group(parent, SWT.NONE);
		RowLayout checkLayout = new RowLayout();
		checkGroup.setLayout(checkLayout);
		checkLayout.wrap = false;
		checkLayout.type = SWT.HORIZONTAL;

		// runGroup.setLayout(new FillLayout(SWT.HORIZONTAL));
//		checkGroup.setLayout(new FormLayout());
//		checkGroup.setText("Utraces Check run:");

		Group startGroup  = new Group(checkGroup, SWT.NONE);
		RowLayout startLayout = new RowLayout();
		startLayout.wrap = false;
		startLayout.type = SWT.HORIZONTAL;
		startLayout.pack = true; 
		startGroup.setLayout(startLayout);

		checkButton = new Button(startGroup,SWT.PUSH);
		checkButton.setText("Check Model");
		checkButton.pack();
		
		
		checkButton.addSelectionListener(new SelectionAdapter() { 
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				//checkButton.setEnabled(false);
				app.startUtracesCheck();
			}
		}); 

		simGroup  = new Group(checkGroup, SWT.NONE);
		RowLayout simLayout = new RowLayout();
		simLayout.wrap = false;
		simLayout.type = SWT.HORIZONTAL;
		simLayout.pack = true; 
		simGroup.setLayout(simLayout);

		org.eclipse.swt.widgets.Label simLabel = new org.eclipse.swt.widgets.Label(simGroup, SWT.NONE);
		simLabel.setText("Selected result: ");
		simLabel.pack();

		simButton = new Button(simGroup,SWT.PUSH);
		simButton.setText("Simulate");
		simButton.pack();
		
		simButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				app.simulateSelectedUtrace();
			}
		});
		
		saveButton = new Button(simGroup,SWT.PUSH);
		saveButton.setText("Save");
		saveButton.pack();

		saveButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				app.addStatusMsg("");
				app.saveSelectedUtrace();
			}
		});

		startGroup.pack();
		simGroup.pack();
		checkGroup.pack();
		
		setSelectionItemsVisibility(false);
		
	}
	
	private Testgui app;
	private Group checkGroup = null;
	private Button checkButton = null;
	private Button simButton = null;
	private Button saveButton = null;
	private Group simGroup = null;
}
