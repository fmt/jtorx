package utwente.fmt.jtorx.testgui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Button;

public class VizButtonHandler {

	public void disable() {
		isEnabled = false;
		if (buttons==null)
			return;
		for(Button b: buttons)
			disableVizButton(b);
	}
	public void enable() {
		isEnabled = true;
		if (buttons==null)
			return;
		for(Button b: buttons)
			enableVizButton(b);
	}
	public void add(final Button b) {
		buttons.add(b);
		initVizButtonData(b);
		if (!isEnabled)
			disableVizButton(b);
		b.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				buttons.remove(b);
			}
		});
	}
	
	public VizButtonHandler(boolean initiallyEnabled) {
		isEnabled = initiallyEnabled;
	}

	private void disableVizButton(Button b) {
		initVizButtonData(b);
		b.setSelection(false);
		b.setEnabled(false);
	}
	private void enableVizButton(Button b) {
		b.setSelection((Boolean)b.getData("selection"));
		b.setEnabled((Boolean)b.getData("enabled"));
	}
	private void initVizButtonData(Button b) {
		b.setData("selection",b.getSelection());
		b.setData("enabled",b.getEnabled());
	}

	private Collection<Button> buttons = Collections.synchronizedList(new ArrayList<Button>());
	private boolean isEnabled;
}
