package utwente.fmt.jtorx.testgui.lazyotf.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;

/**
 * <p>
 * A converter from {@link LazyOTFConfigSessionData} objects to proper LazyOTF
 * configuration instances. The former are used by the GUI, the latter by
 * LazyOTF itself.
 * </p>
 * 
 * <p>
 * The most prominent difference between the two data structures is the usage of
 * named location sets vs. "ordinary" locations.
 * </p>
 */
public class ConfigSessionData2LazyOTFConfig {

    private static Set<Location> makeLocationSet(Set<TestTypeGroup> testTypeGroups) {
        Set<Location> addedLocations = new HashSet<>();
        for (TestTypeGroup ttg : testTypeGroups) {
            for (Location l : ttg.getStates()) {
                addedLocations.add(l);
            }
        }

        return addedLocations;
    }

    /**
     * Convert the given set of UI test type groups to a set of proper LazyOTF
     * test type groups. Conflicting assignments due to ambiguous usage of
     * location sets are resolved by {@link UITestTypeGroup}.
     * 
     * @param uiGroups
     *            The set of UI test type groups.
     * @param strategyMap
     *            A map from {@link UIDecisionStrategy} instances to their
     *            respective {@link DecisionStrategy} instances.
     * @return The new set of {@link TestTypeGroup} instances corresponding to
     *         the ones contained in <i>uiGroups</i>.
     */
    private static Set<TestTypeGroup> makeTestTypeGroupSet(Collection<UITestTypeGroup> uiGroups,
            Map<UIDecisionStrategy, DecisionStrategy> strategyMap) {

        Set<TestTypeGroup> result = new HashSet<>();
        for (UITestTypeGroup uiTTG : uiGroups) {
            result.add(uiTTG.makeTestTypeGroup(strategyMap));
        }
        return result;
    }

    /**
     * Convert the given set of UI decision strategies to a set of proper
     * LazyOTF test type decision strategies. Conflicting assignments due to
     * ambiguous usage of location sets are resolved by the strategy objects.
     * 
     * @param uiStrategies
     *            The set of {@link UIDecisionStrategy} objects to be converted.
     * @return A set of {@link DecisionStrategy} instances corresponding to the
     *         ones contained in <i>uiStrategies</i>.
     */
    private static Map<UIDecisionStrategy, DecisionStrategy>
            makeDecisionStrategySet(Collection<UIDecisionStrategy> uiStrategies) {

        Map<UIDecisionStrategy, DecisionStrategy> result = new HashMap<>();
        for (UIDecisionStrategy uiDS : uiStrategies) {
            result.put(uiDS, uiDS.makeDecisionStrategy());
        }
        return result;
    }

    /**
     * Convert a {@link LazyOTFConfigSessionData} object to a
     * {@link LazyOTFConfig} object.
     * 
     * @param configSessionData
     *            The configuration session data object to be converted.
     * @return The corresponding {@link LazyOTFConfig} instance.
     */
    public static LazyOTFConfig makeLazyOTFConfig(LazyOTFConfigSessionData
            configSessionData) {

        LazyOTFConfig result = new LazyOTFConfig();

        result.setCommunicationLoggingEnabled(configSessionData.isCommunicationLoggingEnabled());

        result.setAllPath2Weights(configSessionData.getPath2Weights());
        result.setAutoRecompute(
                configSessionData.isAutomaticTestTreeRecomputationEnabled());
        result.setAutoStop(configSessionData.isAutoStepStopModeEnabled());
        result.setTestTreeWeigher(
                configSessionData.getEdgeWeightAggregatorName());
        result.setGlobalSatStrategyConditions(
                configSessionData.getGlobalConditions());
        result.setUseCP(configSessionData.isCheckedPREDfsVisitorEnabled());
        result.setMaxRecursionDepth(configSessionData.getMaxRecursionDepth());
        result.setTraversalMethod(configSessionData.getVisitorName());
        result.setNodeValue2Weights(new ArrayList<NodeValue2Weight>(
                configSessionData.getCustomNodeValue2Weights()));

        Map<UIDecisionStrategy, DecisionStrategy> strategyMap =
                makeDecisionStrategySet(configSessionData.getStrategies());
        result.setDecisionStrategies(new ArrayList<DecisionStrategy>(
                strategyMap.values()));
        result.setTestTypeGroups(makeTestTypeGroupSet(
                configSessionData.getTestTypeGroups(), strategyMap));
        result.setLocations(makeLocationSet(result.getTestTypeGroups()));
        result.setEnabledPath2Weights(
                configSessionData.getEnabledPath2Weights());
        result.setInitialRecursionDepth(configSessionData.getInitialRecursionDepth());
        result.setRecursionDepthModThreshold(
                configSessionData.getRecursionDepthModThreshold());

        return result;
    }
}
