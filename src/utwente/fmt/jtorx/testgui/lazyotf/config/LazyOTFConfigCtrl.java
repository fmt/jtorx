package utwente.fmt.jtorx.testgui.lazyotf.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTF.InactiveException;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfigurationOptions;
import utwente.fmt.jtorx.lazyotf.LocationMgr;
import utwente.fmt.jtorx.lazyotf.TestTree2Weight;
import utwente.fmt.jtorx.lazyotf.TraversalMethod;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.policy.Defaults;
import utwente.fmt.jtorx.lazyotf.policy.LazyOTFPolicyFactoryImpl;
import utwente.fmt.jtorx.lazyotf.remoteimpl.ExpressionValidatorImpl;
import utwente.fmt.jtorx.lazyotf.remoteimpl.StandaloneLazyOTF;
import utwente.fmt.jtorx.lazyotf.remoteimpl.StandaloneLazyOTF.ModelNotFoundException;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.Condition;
import utwente.fmt.jtorx.testgui.Config;
import utwente.fmt.jtorx.testgui.lazyotf.config.LazyOTFConfigPane.ButtonName;
import utwente.fmt.jtorx.testgui.lazyotf.config.Preconfigurations.Preconfiguration;
import utwente.fmt.jtorx.testgui.lazyotf.config.XMLConfigurationHandler.ExportException;
import utwente.fmt.jtorx.testgui.lazyotf.config.XMLConfigurationHandler.IllegalSemanticsException;
import utwente.fmt.jtorx.testgui.lazyotf.config.XMLConfigurationHandler.ParseException;
import utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit.WeightsEditor;
import utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit.WeightsEditorFactory;
import utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit.WeightsEditorImpl;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit.LocationSetEditor;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit.LocationSetEditorFactory;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit.LocationSetEditorImpl;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.LocationTypeEditor;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.LocationTypeEditorFactory;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.LocationTypeEditorImpl;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.TestTypeGroupCtrl;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.TestTypeGroupView.CtrlInterface;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.EditorInvocation;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategyVisitorAdapter;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIMappingStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UISatStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;
import utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit.StrategyEditor;
import utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit.StrategyEditorFactory;
import utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit.StrategyEditorImpl;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * The controller for the LazyOTF configuration pane. This instance maintains a
 * {@link LazyOTFConfigurationSession} object.
 */
public class LazyOTFConfigCtrl {
    /** An accessor to various properties of the JTorX configuration pane. */
    private final ConfigPaneAccessor jtorxPaneAccessor;

    /** The LazyOTF configuration pane view. */
    private final LazyOTFConfigPane lazyOTFPane;

    /** The "Test Goal" editor view. */
    private MiniTestTypeGroupEditorView testTypeGroupView;

    /** The "Test Goal" editor controller. */
    private MiniTestTypeGroupEditorCtrl testTypeGroupCtrl;

    /**
     * The configuration session data object maintained by this controller
     * instance.
     */
    private final LazyOTFConfigSessionData configSessionData;

    /**
     * A standalone LazyOTF instance used for getting configuration options and
     * basic model data.
     */
    private final StandaloneLazyOTF standaloneInstance;

    /** The factories of the sub-editors. */
    private final EditorFactories editorFactories;

    private static final int DEFAULT_MAX_RECURSION_DEPTH = 10;
    private static final int DEFAULT_INIT_RECURSION_DEPTH = 2;
    private static final int DEFAULT_REC_DEPTH_MOD_THRESH = 10;
    private static final boolean DEFAULT_AUTORECOMPUTE_ENABLED = true;

    /**
     * Constructs a new {@link LazyOTFConfigCtrl} instance. A
     * {@link LazyOTFConfigurationSession} object is created and initialized to
     * default values. The GUI is updated accordingly.
     * 
     * @param jtorxPaneAccessor
     *            An accessor to various properties of the JTorX configuration
     *            pane.
     */
    public LazyOTFConfigCtrl(ConfigPaneAccessor jtorxPaneAccessor) {
        this(jtorxPaneAccessor, new EditorFactories());
    }

    /**
     * Constructs a new {@link LazyOTFConfigCtrl} instance. A
     * {@link LazyOTFConfigurationSession} object is created and initialized to
     * default values. The GUI is updated accordingly.
     * 
     * @param jtorxPaneAccessor
     *            An accessor to various properties of the JTorX configuration
     *            pane.
     * @param factories
     *            An object providing factories for the various sub- editors.
     */
    public LazyOTFConfigCtrl(ConfigPaneAccessor jtorxPaneAccessor, EditorFactories factories) {
        this.jtorxPaneAccessor = jtorxPaneAccessor;
        lazyOTFPane = jtorxPaneAccessor.getLazyOTFConfigPane();
        configSessionData = new LazyOTFConfigSessionData();
        lazyOTFPane.clearTestTypeAssignmentViewParent(true);
        standaloneInstance = new StandaloneLazyOTF(
                new LazyOTFPolicyFactoryImpl(new LazyOTFConfig()));
        standaloneInstance.makeModellessInstance();
        testTypeGroupView = null;
        testTypeGroupCtrl = null;
        editorFactories = factories;
        configSessionData.setTestTypes(TestType.ALL_TEST_TYPES);
        try {
            loadConfigurationOptions();
        }
        catch (IllegalStateException e) {
            // Initializing LazyOTF aborted
            return;
        }

        setDefaults();
        setNoModel(true);
        setupListeners();
        configSessionData.setCommunicationLoggingEnabled(true);
    }

    /**
     * Resets the configuration pane by clearing the model-specific data from
     * the session object. Clears the "Test Goal" editor.
     * 
     * @param preserveWeights
     *            If <tt>true</tt>, node value and test tree trace weighers are
     *            not cleared.
     */
    private void setNoModel(boolean preserveWeights) {
        configSessionData.clearLocationSetAssociations();
        if (!preserveWeights) {
            configSessionData.setPath2Weights(new HashSet<Path2Weight>());
            configSessionData.setEnabledPath2Weights(new HashSet<Path2Weight>());
            configSessionData.setCustomNodeValue2Weights(new HashSet<NodeValue2Weight>());
        }
        configSessionData.setTestTypeGroups(new HashSet<UITestTypeGroup>());
        configSessionData.setLocations(new HashSet<NamedSet<Location>>());
        configSessionData.setLocationSets(new HashSet<NamedSet<Location>>());
        configSessionData.setStrategies(new ArrayList<UIDecisionStrategy>());
        configSessionData.setGlobalConditions(new HashSet<Condition>());
        configSessionData.setCurrentFileName(null);
        lazyOTFPane.clearTestTypeAssignmentViewParent(true);
        testTypeGroupView = null;
        testTypeGroupCtrl = null;
        lazyOTFPane.setButtonEnabled(ButtonName.ADV_TT_ASSIGNMENT, false);
        lazyOTFPane.setButtonEnabled(ButtonName.EDIT_HEURISTICS, false);
        lazyOTFPane.setPreconfigButtonEnabled(false);
        updateView();
    }

    /**
     * Loads configuration information for the given model and update the
     * session object accordingly. Updates the "Test Goal" editor.
     * 
     * @param filename
     *            The model's file name.
     * @throws ModelUnavailableException
     *             when the model could not be loaded. See the exception's
     *             message string for details.
     */
    private void setModel(String filename) throws ModelUnavailableException {
        setNoModel(true);

        try {
            standaloneInstance.resetAndRead(filename);
        }
        catch (ModelNotFoundException e) {
            throw new ModelUnavailableException("Model file " + filename + "does not exist.");
        }

        configSessionData.setCurrentFileName(filename);
        LazyOTF lazyOTFinst = standaloneInstance.getLazyOTFInstance();

        /* Add locations. */
        LocationMgr locMgr = lazyOTFinst.getLocationManager();
        for (Location l : locMgr.getLocations()) {
            configSessionData.getBasisLocationSets().add(NamedSet.makeNamedSet(null, l));
        }

        /* Install default test type group preconfiguration */
        Preconfigurations.makeDefaultPreconfig().install(configSessionData);

        /* Display the testGoalGroup within the main pane. */
        lazyOTFPane.clearTestTypeAssignmentViewParent(false);
        final MiniTestTypeGroupEditorView ttgView = new MiniTestTypeGroupEditorView(
                lazyOTFPane.getTestTypeAssignmentViewParent(),
                configSessionData.getTestTypes(), new CtrlInterface() {

                    @Override
                    public Set<NamedSet<Location>> getNamedLocationSets() {
                        return configSessionData.getLocationSets();
                    }

                    @Override
                    public Set<NamedSet<Location>> getLocations() {
                        return configSessionData.getBasisLocationSets();
                    }
                });
        TestTypeGroupCtrl ttgCtrl = new TestTypeGroupCtrl(ttgView.getTestTypeGroupView(),
                new GenericListener<NamedSet<Location>>() {
                    @Override
                    public void onFiredEvent(NamedSet<Location> initialSelection) {
                        displayLocationSetEditor(initialSelection);
                        ttgView.redraw();
                    }
                });
        testTypeGroupView = ttgView;

        UITestTypeGroup initialGroup = configSessionData.getTestTypeGroups().iterator().next();
        ttgCtrl.setTestTypeGroup(initialGroup);
        testTypeGroupCtrl = new MiniTestTypeGroupEditorCtrl(ttgView, ttgCtrl);
        updateTestTypeGroupMiniEditor();

        for (TestType t : initialGroup.getTestTypes()) {
            ttgView.getTestTypeGroupView().setExpanded(t, true);
        }

        lazyOTFPane.updateTestTypeAssignmentViewParent();

        lazyOTFPane.setButtonEnabled(ButtonName.ADV_TT_ASSIGNMENT, true);
        lazyOTFPane.setPreconfigButtonEnabled(true);
        lazyOTFPane.setButtonEnabled(ButtonName.EDIT_HEURISTICS, true);

        updateView();
    }

    /**
     * Prints an error message and disable LazyOTF configuration.
     * 
     * @param errorMsg
     *            The error message.
     */
    private void deactivateLazyOTF(String errorMsg) {
        System.err.println(errorMsg);
        jtorxPaneAccessor.setLazyOTFAvailable(false);
    }

    /**
     * Loads the configuration options from a dummy LazyOTF instance. On
     * failure, it deactivates LazyOTF completely.
     * 
     * @throws IllegalStateException
     *             when loading the configuration options failed.
     */
    private void loadConfigurationOptions() {
        try {
            LazyOTF lotf = standaloneInstance.getLazyOTFInstance();
            if (lotf != null) {
                configSessionData.setConfigOptions(lotf.getConfigurationOptions());    
            }
            else {
                deactivateLazyOTF("Error: Unable to connect to the underlying LazyOTF instance.");
                throw new IllegalStateException();
            }
        }
        catch (InactiveException e) {
            deactivateLazyOTF("Error: Unable to get configuration options from dummy LazyOTF instance!");
            throw new IllegalStateException();
        }

        lazyOTFPane.setGraphTraversalMethods(configSessionData.getConfigOptions().traversalMethods);
        lazyOTFPane.setEdgeWeightAggregators(configSessionData.getConfigOptions().testTreeWeighers);
    }

    /**
     * Sets the configuration data to its default values and update the GUI
     * accordingly.
     */
    private void setDefaults() {
        LazyOTFConfigurationOptions lotfConfigOpts = configSessionData.getConfigOptions();

        lazyOTFPane.setAutorecomputeEnabled(DEFAULT_AUTORECOMPUTE_ENABLED);
        configSessionData.setAutomaticTestTreeComputationEnabled(DEFAULT_AUTORECOMPUTE_ENABLED);

        lazyOTFPane.setMaxRecursionDepthMinValue(1);
        lazyOTFPane.setMaxRecursionDepthMaxValue(Integer.MAX_VALUE);
        lazyOTFPane.setInitialRecursionDepthMinValue(1);
        lazyOTFPane.setInitialRecursionDepthMaxValue(Integer.MAX_VALUE);
        lazyOTFPane.setTraversalDepthThresholdMinValue(1);
        lazyOTFPane.setTraversalDepthThresholdMaxValue(Integer.MAX_VALUE);

        lazyOTFPane.setMaxTraversalRecursionDepth(DEFAULT_MAX_RECURSION_DEPTH);
        configSessionData.setMaxRecursionDepth(DEFAULT_MAX_RECURSION_DEPTH);
        lazyOTFPane.setInitialTraversalRecursionDepth(DEFAULT_INIT_RECURSION_DEPTH);
        configSessionData.setInitialRecursionDepth(DEFAULT_INIT_RECURSION_DEPTH);
        lazyOTFPane.setTraversalDepthThreshold(DEFAULT_REC_DEPTH_MOD_THRESH);
        configSessionData.setRecursionDepthModThreshold(DEFAULT_REC_DEPTH_MOD_THRESH);

        /* Set up the edge weight aggregator combo box */
        List<TestTree2Weight> edgeWeightAggregators = lotfConfigOpts.testTreeWeighers;
        lazyOTFPane.setEdgeWeightAggregators(edgeWeightAggregators);
        TestTree2Weight defaultTree2Weight;
        defaultTree2Weight = lotfConfigOpts
                .tree2WeightByName(lotfConfigOpts.defaultTestTreeWeigher);

        int defaultTree2WeightIdx = edgeWeightAggregators.indexOf(defaultTree2Weight);

        if (defaultTree2WeightIdx >= 0) {
            configSessionData.setEdgeWeightAggregatorName(defaultTree2Weight);
            lazyOTFPane.setEdgeWeightAggregatorSelection(defaultTree2WeightIdx);
        }
        else {
            deactivateLazyOTF("Error: Illegal default Tree2Weight " + defaultTree2Weight.getName());
            return;
        }

        /* Set up the graph traversal method selection */
        List<TraversalMethod> graphTraversalMethods = lotfConfigOpts.traversalMethods;
        lazyOTFPane.setGraphTraversalMethods(graphTraversalMethods);
        String defaultGTMName = lotfConfigOpts.defaultTraversalMethod;
        TraversalMethod tm = lotfConfigOpts.traversalMethodByName(defaultGTMName);

        int defaultTraversalIdx = graphTraversalMethods.indexOf(tm);
        if (defaultTraversalIdx >= 0) {
            lazyOTFPane.setCurrentGraphTraversalMethod(defaultTraversalIdx);
            configSessionData.setVisitorName(lotfConfigOpts.traversalMethodByName(defaultGTMName));
        }
        else {
            deactivateLazyOTF("Error: Illegal default graph traversal method " + defaultGTMName);
            return;
        }

        configSessionData.setPath2Weights(new HashSet<Path2Weight>(configSessionData
                .getConfigOptions().path2Weights));
        configSessionData.setEnabledPath2Weights(new HashSet<Path2Weight>());

        /* Set up the default {@link Path2Weight} */
        for (Path2Weight g : lotfConfigOpts.path2Weights) {
            if (g.getName().equals(lotfConfigOpts.defaultPath2Weight)) {
                configSessionData.getEnabledPath2Weights().add(g);
            }
        }

        /* Set up the default node value weigher */
        Set<NodeValue2Weight> nodeValueWeighers = new HashSet<>();
        nodeValueWeighers.add(Defaults.defaultNodeValue2Weight());
        configSessionData.setCustomNodeValue2Weights(nodeValueWeighers);

        configSessionData.setLogFilename("");
    }

    /**
     * Sets up the view listeners.
     */
    private void setupListeners() {
        lazyOTFPane.addSelectButtonListener(ButtonName.LOAD_MODEL, new Runnable() {
            @Override
            public void run() {
                try {
                    setModel(jtorxPaneAccessor.getModelFilename());
                }
                catch (ModelUnavailableException e) {
                    lazyOTFPane.displayErrorMessageBox("Could not load model data:\n"
                            + e.getMessage());
                    setNoModel(true);
                }
            }
        });

        lazyOTFPane.addSelectButtonListener(ButtonName.EDIT_HEURISTICS, new Runnable() {
            @Override
            public void run() {
                displayWeightsEditor();
            }
        });

        lazyOTFPane.addSelectButtonListener(ButtonName.ADV_TT_ASSIGNMENT, new Runnable() {
            @Override
            public void run() {
                displayLocationTypeEditor();
            }
        });

        lazyOTFPane.addSelectTraversalMethodChangedListener(new GenericListener<Integer>() {
            @Override
            public void onFiredEvent(Integer i) {
                configSessionData.setVisitor(i);
                lazyOTFPane.setCurrentGraphTraversalMethod(i);
            }
        });

        lazyOTFPane.addSetAutoRecomputeListener(new GenericListener<Boolean>() {
            @Override
            public void onFiredEvent(Boolean it) {
                configSessionData.setAutomaticTestTreeComputationEnabled(it);
            }
        });

        lazyOTFPane.addSelectEdgeWeightAggregatorListener(new GenericListener<Integer>() {
            @Override
            public void onFiredEvent(Integer i) {
                configSessionData.setTreeWeigherIndex(i);
            }
        });

        lazyOTFPane.addSetMaxRecursionDepthListener(new GenericListener<Integer>() {
            @Override
            public void onFiredEvent(Integer i) {
                configSessionData.setMaxRecursionDepth(i);
            }
        });

        lazyOTFPane.addSetInitialRecursionDepthListener(new GenericListener<Integer>() {
            @Override
            public void onFiredEvent(Integer i) {
                configSessionData.setInitialRecursionDepth(i);
            }
        });

        lazyOTFPane.addSetTraversalDepthThresholdListener(new GenericListener<Integer>() {
            @Override
            public void onFiredEvent(Integer i) {
                configSessionData.setRecursionDepthModThreshold(i);
            }
        });

        lazyOTFPane.addSelectPreconfigListener(
                new GenericListener<Preconfiguration>() {
                    @Override
                    public void onFiredEvent(Preconfiguration preconfig) {
                        installPreconfig(preconfig);
                    }
                });

        lazyOTFPane.addToggleAutostopModeListener(new Runnable() {
            @Override
            public void run() {
                configSessionData.setAutoSteStopMode(lazyOTFPane.isAutostopEnabled());
            }
        });

        lazyOTFPane.addSetLoggingFileRequestedListener(new Runnable() {
            @Override
            public void run() {
                String newFileName = LogFileSelector.open(configSessionData.getLogFilename());
                if (newFileName != null) {
                    configSessionData.setLogFilename(newFileName);
                }
            }
        });
    }

    /**
     * Updates the LazyOTF pane to reflect the current configuration data.
     */
    private void updateView() {
        lazyOTFPane.setAutostopEnabled(configSessionData.isAutoStepStopModeEnabled());
        lazyOTFPane.setAutorecomputeEnabled(configSessionData
                .isAutomaticTestTreeRecomputationEnabled());
        lazyOTFPane.setCurrentGraphTraversalMethod(configSessionData.getTraversalMethodIndex());
        lazyOTFPane.setEdgeWeightAggregatorSelection(configSessionData.getTreeWeigherIndex());
        lazyOTFPane.setMaxTraversalRecursionDepth(configSessionData.getMaxRecursionDepth());
        lazyOTFPane.setInitialTraversalRecursionDepth(configSessionData.getInitialRecursionDepth());
        lazyOTFPane.setTraversalDepthThreshold(configSessionData.getRecursionDepthModThreshold());
        if (testTypeGroupView != null) {
            testTypeGroupView.redraw();
        }
    }

    /**
     * Updates the test type group mini-editor, displaying the default group if
     * the current one is not among the current set of test type groups.
     */
    private void updateTestTypeGroupMiniEditor() {
        testTypeGroupCtrl.update(configSessionData.getTestTypeGroups(),
                configSessionData.getTestTypeGroups().iterator().next());
    }

    /**
     * Validates and installs the given configuration session data. Updates the
     * GUI afterwards.
     * 
     * @param newConfig
     *            The new configuration session data.
     */
    private void installConfigSessionData(LazyOTFConfigSessionData newConfig) {
        newConfig.validate();
        configSessionData.clearLocationSetAssociations();
        configSessionData.setAutomaticTestTreeComputationEnabled(newConfig
                .isAutomaticTestTreeRecomputationEnabled());
        configSessionData.setAutoSteStopMode(newConfig.isAutoStepStopModeEnabled());
        configSessionData.setCheckedPREDfsVisitorEnabled(newConfig.isCheckedPREDfsVisitorEnabled());
        configSessionData.setCustomNodeValue2Weights(newConfig.getCustomNodeValue2Weights());
        configSessionData.setEdgeWeightAggregatorName(newConfig.getEdgeWeightAggregatorName());
        configSessionData.setEnabledPath2Weights(newConfig.getEnabledPath2Weights());
        configSessionData.setGlobalConditions(newConfig.getGlobalConditions());
        configSessionData.setPath2Weights(newConfig.getPath2Weights());
        configSessionData.setLocationSets(newConfig.getLocationSets());
        configSessionData.setMaxRecursionDepth(newConfig.getMaxRecursionDepth());
        configSessionData.setInitialRecursionDepth(newConfig.getInitialRecursionDepth());
        configSessionData.setRecursionDepthModThreshold(newConfig.getRecursionDepthModThreshold());
        configSessionData.setStrategies(newConfig.getStrategies());
        configSessionData.setTestTypeGroups(newConfig.getTestTypeGroups());
        configSessionData.setVisitorName(newConfig.getVisitorName());
        configSessionData.setLogFilename(newConfig.getLogFilename());
        testTypeGroupCtrl.setTestTypeGroup(newConfig.getTestTypeGroups().iterator().next());

        updateView();
        updateTestTypeGroupMiniEditor();
    }

    /**
     * Install a LazyOTF preconfiguration. This may override the current test
     * type group and weights configuration.
     * 
     * @param it
     *            The preconfiguration data.
     */
    private void installPreconfig(Preconfiguration it) {
        it.install(configSessionData);
        updateTestTypeGroupMiniEditor();
    }

    /**
     * Load the LazyOTF configuration from the given JTorX configuration object.
     * The configuration is expected as a field named LAZYOTFCFG and must be in
     * a format readable by {@link XMLConfigurationHandler}.
     * 
     * @param jtorxCfg
     *            The JTorX configuration object.
     */
    public void loadConfig(Config jtorxCfg) {
        String newModelFile = jtorxPaneAccessor.getModelFilename();
        try {
            setModel(newModelFile);
            XMLConfigurationHandler confh = new XMLConfigurationHandler(
                    configSessionData.getTestTypes(), configSessionData.getBasisLocationSets());
            confh.readConfigFromString(jtorxCfg.getString("LAZYOTFCFG"));
            LazyOTFConfigSessionData newCSD = confh.getConfiguration(configSessionData
                    .getConfigOptions());

            newCSD.setConfigOptions(configSessionData.getConfigOptions());
            newCSD.setLocations(configSessionData.getBasisLocationSets());
            newCSD.getCustomNodeValue2Weights().add(Defaults.defaultNodeValue2Weight());
            newCSD.setTestTypes(configSessionData.getTestTypes());

            installConfigSessionData(newCSD);
        }
        catch (ModelUnavailableException
                | IllegalSemanticsException
                | ParseException e) {
            lazyOTFPane.displayErrorMessageBox("Unable to load the LazyOTF configuration:\n"
                    + e.getMessage());
        }

    }

    /**
     * Stores the LazyOTF Configuration to the given JTorX configuration object.
     * The configuration is stored in a field named LAZYOTFCFG. Encoding is done
     * by an {@link XMLConfigurationHandler} object.
     * 
     * @param jtorxCfg
     *            The JTorX configuration object.
     */
    public void fillConfig(Config jtorxCfg) {
        if (configSessionData.getCurrentFileName() == null) {
            lazyOTFPane
                    .displayErrorMessageBox("Unable to save the LazyOTF configuration: No file selected");
            return;
        }
        else if (!configSessionData.getCurrentFileName().equals(
                jtorxPaneAccessor.getModelFilename())) {
            try {
                setModel(jtorxPaneAccessor.getModelFilename());
            }
            catch (ModelUnavailableException e) {
                lazyOTFPane.displayErrorMessageBox("Unable to save the LazyOTF configuration:\n"
                        + e.getMessage());
                return;
            }
        }

        XMLConfigurationHandler confh = new XMLConfigurationHandler(
                configSessionData.getTestTypes(), configSessionData.getBasisLocationSets());
        try {
            String configuration = confh.getXMLConfiguration(configSessionData);
            jtorxCfg.add("LAZYOTFCFG", configuration);
        }
        catch (ExportException e) {
            lazyOTFPane.displayErrorMessageBox("Unable to save the LazyOTF configuration:\n"
                    + e.getMessage());
        }

    }

    /**
     * Gets the configuration object as a {@link LazyOTFConfig} instance.
     * 
     * @param makePassiveLocationMgr
     *            If set to <tt>true</tt>, the instance will have a passive
     *            {@link LocationMgr}. (See the documentation of
     *            {@link LocationMgr} for details.)
     * @return A new {@link LazyOTFConfig} instance reflecting the current
     *         configuration.
     */
    public LazyOTFConfig getConfiguration(boolean makePassiveLocationMgr) {
        return ConfigSessionData2LazyOTFConfig.makeLazyOTFConfig(configSessionData);
    }

    /**
     * Displays the weights editor and updates the session object afterwards.
     */
    private void displayWeightsEditor() {
        assert configSessionData.getCurrentFileName() != null;
        WeightsEditor ed = editorFactories.getWeightsEditorFactory().makeWeightsEditor(
                configSessionData,
                new ExpressionValidatorImpl(standaloneInstance.getLazyOTFInstance()));
        ed.open();
        configSessionData.setCustomNodeValue2Weights(new HashSet<>(ed
                .getModifiedNodeValue2Weights()));
        configSessionData.setPath2Weights(new HashSet<>(ed
                .getModifiedPath2WeightSet()));
    }

    /**
     * Displays the location type editor and updates the session object
     * afterwards.
     */
    private void displayLocationTypeEditor() {
        assert configSessionData.getCurrentFileName() != null;
        final LocationTypeEditor lte;
        lte = editorFactories.getLocationTypeEditorFactory().makeLocationTypeEditor(
                configSessionData, makeEditorInvocationInstance());
        /* modal location type editor window gets displayed here */
        lte.open(new Runnable() {
            @Override
            public void run() {
                Set<UITestTypeGroup> newGroups = lte.getTestTypeGroups();
                configSessionData.setTestTypeGroups(newGroups);
                if (testTypeGroupCtrl != null) {
                    updateTestTypeGroupMiniEditor();
                }
            }
        });
    }

    /**
     * Displays the named location set editor and updates the session object
     * accordingly.
     * 
     * @param initialSelection
     *            The initially selected named location set. May be
     *            <tt>null</tt> if none should be selected initially.
     * @return The {@link LocationSetEditor} instance of the closed editor.
     */
    private LocationSetEditor displayLocationSetEditor(NamedSet<Location> initialSelection) {
        assert configSessionData.getCurrentFileName() != null;

        LocationSetEditor lsed;

        if (initialSelection == null) {
            lsed = editorFactories.getLocationSetEditorFactory().makeLocationSetEditor(
                    configSessionData);
        }
        else {
            lsed = editorFactories.getLocationSetEditorFactory().makeLocationSetEditor(
                    configSessionData, initialSelection);
        }

        lsed.open();
        updateSessionDataAfterEditingLocationSets(lsed);
        return lsed;
    }

    /**
     * Update the various data structures within the session data object after
     * editing the set of named location sets.
     * 
     * @param lsed
     *            The named set editor instance.
     */
    private void updateSessionDataAfterEditingLocationSets(LocationSetEditor lsed) {
        final Set<NamedSet<Location>> deletedSets = lsed.getDeletedLocationSets();

        configSessionData.setLocationSets(lsed.getLocationSets());

        /* Update test type groups */
        for (UITestTypeGroup uiTTG : configSessionData.getTestTypeGroups()) {
            /* Remove deleted location sets from the test type group */
            for (NamedSet<Location> deletedSet : deletedSets) {
                if (uiTTG.getLocations().contains(deletedSet)) {
                    uiTTG.removeAssociation(deletedSet);
                }
            }
        }

        /* Update decision strategies: Remove deleted location sets */
        for (UIDecisionStrategy uiStrat : configSessionData.getStrategies()) {
            uiStrat.accept(new UIDecisionStrategyVisitorAdapter() {
                @Override
                public void visit(UIMappingStrategy strategy) {
                    for (NamedSet<Location> deletedSet : deletedSets) {
                        if (strategy.getLocationSets().contains(deletedSet)) {
                            strategy.setTestType(deletedSet, null);
                        }
                    }
                }

                @Override
                public void visit(UISatStrategy strategy) {
                    for (NamedSet<Location> deletedSet : deletedSets) {
                        if (strategy.getLocationSets().contains(deletedSet)) {
                            for (TestType t : strategy.getAssociatedTestTypes(deletedSet)) {
                                strategy.removeAssociation(deletedSet, t);
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * Displays the decision strategy editor and updates the session data after
     * the window is closed.
     */
    private void displayDecisionStrategyEditor() {
        assert configSessionData.getCurrentFileName() != null;
        StrategyEditor s = editorFactories.getStrategyEditorFactory().makeStrategyEditor(
                configSessionData,
                makeEditorInvocationInstance(),
                new ExpressionValidatorImpl(standaloneInstance.getLazyOTFInstance()));
        s.open();
        configSessionData.setStrategies(s.getModifiedStrategySet());
    }

    /**
     * @return An object allowing the invocation of the various editors.
     */
    private EditorInvocation makeEditorInvocationInstance() {
        return new EditorInvocation() {
            @Override
            public void displayDecisionStrategyEditor() {
                LazyOTFConfigCtrl.this.displayDecisionStrategyEditor();
            }

            @Override
            public void displayWeightsEditor() {
                LazyOTFConfigCtrl.this.displayWeightsEditor();
            }

            @Override
            public void displayTestObjectiveEditor() {
                LazyOTFConfigCtrl.this.displayLocationTypeEditor();
            }

            @Override
            public LocationSetEditingResults displayLocationSetEditor() {
                return displayLocationSetEditor(null);
            }

            @Override
            public LocationSetEditingResults displayLocationSetEditor(
                    NamedSet<Location> initialSelection) {
                final LocationSetEditor lsed =
                        LazyOTFConfigCtrl.this.displayLocationSetEditor(initialSelection);
                return new EditorInvocation.LocationSetEditingResults() {
                    @Override
                    public Set<NamedSet<Location>> newLocationSets() {
                        return lsed.getNewLocationSets();
                    }

                    @Override
                    public Set<NamedSet<Location>> deletedLocationSets() {
                        return lsed.getDeletedLocationSets();
                    }
                };
            }
        };
    }

    @SuppressWarnings("serial")
    protected class ModelUnavailableException extends Exception {
        public ModelUnavailableException(String msg) {
            super(msg);
        }
    }

    /**
     * Accessor class for JTorX' ConfigPane.
     */
    public interface ConfigPaneAccessor {
        String getModelFilename();

        LazyOTFConfigPane getLazyOTFConfigPane();

        void setLazyOTFAvailable(boolean available);
    }

    /**
     * A class containing the factories for the various sub-editors. The default
     * factories are defined within this class.
     */
    public static class EditorFactories {
        /** The weights editor factory. */
        private final WeightsEditorFactory weightsEditorFactory;

        /** The location set editor factory. */
        private final LocationSetEditorFactory locationSetEditorFactory;

        /** The strategy editor factory. */
        private final StrategyEditorFactory strategyEditorFactory;

        /** The location type editor factory. */
        private final LocationTypeEditorFactory locationTypeEditorFactory;

        /**
         * Construct a new {@link EditorFactories} instance.
         * 
         * @param gmef
         *            The weights editor factory.
         * @param lsef
         *            The location set editor factory.
         * @param sef
         *            The strategy editor factory.
         * @param ltef
         *            The location type editor factory.
         */
        public EditorFactories(WeightsEditorFactory gmef,
                LocationSetEditorFactory lsef,
                StrategyEditorFactory sef,
                LocationTypeEditorFactory ltef) {
            weightsEditorFactory = gmef;
            locationSetEditorFactory = lsef;
            locationTypeEditorFactory = ltef;
            strategyEditorFactory = sef;
        }

        /**
         * Construct a new {@link EditorFactories} instance using only default
         * editor factories.
         */
        public EditorFactories() {
            this(null, null, null, null);
        }

        /**
         * @return The weights editor factory. If it was configured as
         *         <tt>null</tt>, the default weights editor factory is
         *         returned.
         */
        public WeightsEditorFactory getWeightsEditorFactory() {
            if (weightsEditorFactory == null) {
                return WeightsEditorImpl.getFactory();
            }
            else {
                return weightsEditorFactory;
            }
        }

        /**
         * @return The location set editor factory. If it was configured as
         *         <tt>null</tt>, the default location set editor factory is
         *         returned.
         */
        public LocationSetEditorFactory getLocationSetEditorFactory() {
            if (locationSetEditorFactory == null) {
                return LocationSetEditorImpl.getFactory();
            }
            else {
                return locationSetEditorFactory;
            }
        }

        /**
         * @return The test type group editor factory. If it was configured as
         *         <tt>null</tt>, the default test type group editor factory is
         *         returned.
         */
        public LocationTypeEditorFactory getLocationTypeEditorFactory() {
            if (locationTypeEditorFactory == null) {
                return LocationTypeEditorImpl.getFactory();
            }
            else {
                return locationTypeEditorFactory;
            }
        }

        /**
         * @return The strategy editor factory. If it was configured as
         *         <tt>null</tt>, the default strategy editor factory is
         *         returned.
         */
        public StrategyEditorFactory getStrategyEditorFactory() {
            if (strategyEditorFactory == null) {
                return StrategyEditorImpl.getFactory();
            }
            else {
                return strategyEditorFactory;
            }
        }
    }
}
