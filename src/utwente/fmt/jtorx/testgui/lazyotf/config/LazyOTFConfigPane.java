package utwente.fmt.jtorx.testgui.lazyotf.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Tree;

import utwente.fmt.jtorx.lazyotf.TestTree2Weight;
import utwente.fmt.jtorx.lazyotf.TraversalMethod;
import utwente.fmt.jtorx.testgui.lazyotf.config.Preconfigurations.Preconfiguration;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * The LazyOTF configuration pane view.
 */
public class LazyOTFConfigPane {
    /** The config pane's parent widget. */
    private final Composite parent;

    /** The group for test type assignment related widget. */
    private final Group testTypeAssignmentGroup;

    /** The group for advanced settings widgets. */
    private final Group advancedSettingsGroup;

    /**
     * The parent control for the test type group editor within the main pane.
     */
    private final Composite testTypeGroupView;

    /** The button for the "debug" menu. */
    private final Button debugButton;

    /** The button for the preconfig menu. */
    private final Button preconfigButton;

    /** The max. model traversal depth spinner. */
    private final Spinner maxRecursionDepthSpinner;

    /** The initial model traversal depth spinner. */
    private final Spinner initRecursionDepthSpinner;

    /** The model traversal depth change threshold spinner. */
    private final Spinner traversalDepthChangeThreshSpinner;

    /** The combo box for the edge weight aggregators. */
    private final Combo edgeWeightAggregatorCombo;

    /** The names of the buttons not associated with a menu. */
    public enum ButtonName {
        LOAD_MODEL, EDIT_HEURISTICS, ADV_TT_ASSIGNMENT
    };

    /** A map from button names to actual buttons. */
    private final Map<ButtonName, Button> buttons;

    /** A map from button names to click-listeners. */
    private final Map<ButtonName, Set<Runnable>> buttonSelectionListeners;

    /**
     * The set of listeners getting fired when the edge weight aggregator
     * selection changes.
     */
    private final Set<GenericListener<Integer>> selectEdgeWeightAggregatorListeners;

    /**
     * The set of listeners getting fired when the model traversal method
     * selection changes.
     */
    private final Set<GenericListener<Integer>> selectTraversalMethodChangedListeners;

    /**
     * The set of listeners getting fired when the auto-recompute flag is
     * changed.
     */
    private final Set<GenericListener<Boolean>> setAutoRecomputeListeners;

    /**
     * The set of listeners getting fired when the max. model traversal depth is
     * changed.
     */
    private final Set<GenericListener<Integer>> setMaxRecursionDepthListeners;

    /**
     * The set of listeners getting fired when the initial model traversal depth
     * is changed.
     */
    private final Set<GenericListener<Integer>> setInitialRecursionDepthListeners;

    /**
     * The set of listeners getting fired when the traversal depth threshold is
     * changed.
     */
    private final Set<GenericListener<Integer>> setTraversalDepthThresholdListeners;

    /**
     * The set of listeners getting fired when the user requests to change the
     * logging file.
     */
    private final Set<Runnable> setLoggingFileRequestListeners;

    /**
     * The set of listeners getting fired when the user requests to change the
     * autostep autostop mode.
     */
    private final Set<Runnable> toggleAutostopModeRequestListeners;

    /* Initialized by setupDebugMenu() */
    /** The list of model traversal methods. */
    private List<TraversalMethod> dbgGraphTraversalMethods = new ArrayList<>();

    /** The current model traversal method selection. */
    private int dbgCurrentGraphTraversalMethod = -1;

    /** The auto-recompute flag menu item. */
    private MenuItem dbgSetAutorecompute;

    /** The auto-stop flag menu item. */
    private MenuItem dbgSetAutostop;

    /** The "set logfile" debug menu item. */
    private MenuItem dbgSetLogfile;

    /**
     * The set of listeners getting fired when the user requests to use a
     * preconfiguration.
     */
    private final Set<GenericListener<Preconfiguration>> selectPreconfigListeners;

    /**
     * Constructs a new LazyOTF pane.
     * 
     * @param parent
     *            The pane's parent widget.
     */
    public LazyOTFConfigPane(Composite parent) {
        this.parent = parent;

        buttons = new HashMap<>();

        GridLayout mainLayout = new GridLayout();
        mainLayout.numColumns = 2;
        this.parent.setLayout(mainLayout);

        /* Set up the groups. */
        testTypeAssignmentGroup = new Group(parent, SWT.NONE);
        testTypeAssignmentGroup.setText("Test type assignment");
        advancedSettingsGroup = new Group(parent, SWT.NONE);
        advancedSettingsGroup.setText("Advanced");

        GridLayout testTypeAssignmentLayout = new GridLayout();
        testTypeAssignmentLayout.numColumns = 3;
        testTypeAssignmentGroup.setLayout(testTypeAssignmentLayout);
        testTypeAssignmentGroup.setLayoutData(new GridData(GridData.FILL_BOTH));

        GridLayout advLayout = new GridLayout();
        advLayout.numColumns = 2;
        advancedSettingsGroup.setLayout(advLayout);
        advancedSettingsGroup.setLayoutData(new GridData(GridData.FILL_VERTICAL));

        /* Set up the "test goals" section */
        testTypeGroupView = new Composite(testTypeAssignmentGroup, SWT.NONE);
        GridData testTypeAssignmentTreeLayoutData = new GridData(GridData.FILL_BOTH);
        testTypeAssignmentTreeLayoutData.horizontalSpan = 4;
        testTypeGroupView.setLayoutData(testTypeAssignmentTreeLayoutData);
        testTypeGroupView.setLayout(new FillLayout());

        Button editTestTypeGroupsButton = new Button(testTypeAssignmentGroup, SWT.PUSH);
        editTestTypeGroupsButton.setText("Custom test objectives...");
        buttons.put(ButtonName.ADV_TT_ASSIGNMENT, editTestTypeGroupsButton);

        Button loadModelButton = new Button(testTypeAssignmentGroup, SWT.PUSH);
        loadModelButton.setText("Load model");
        buttons.put(ButtonName.LOAD_MODEL, loadModelButton);

        /* Set up the "advanced" section */
        Label recDepthLabel = new Label(advancedSettingsGroup, SWT.NONE);
        recDepthLabel.setText("Max. traversal depth:");
        recDepthLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        maxRecursionDepthSpinner = new Spinner(advancedSettingsGroup, SWT.NONE);
        maxRecursionDepthSpinner.setSelection(0);

        Label recDepthInitLabel = new Label(advancedSettingsGroup, SWT.NONE);
        recDepthInitLabel.setText("Initial traversal depth:");
        recDepthInitLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        initRecursionDepthSpinner = new Spinner(advancedSettingsGroup, SWT.NONE);
        initRecursionDepthSpinner.setSelection(0);

        Label thresholdLabel = new Label(advancedSettingsGroup, SWT.NONE);
        thresholdLabel.setText("Threshold:");
        thresholdLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        traversalDepthChangeThreshSpinner = new Spinner(advancedSettingsGroup,
                SWT.NONE);
        traversalDepthChangeThreshSpinner.setSelection(0);

        GridData span2_1 = new GridData(GridData.FILL_BOTH);
        span2_1.horizontalSpan = 2;
        Composite advLowerPart = new Composite(advancedSettingsGroup, SWT.NONE);
        advLowerPart.setLayoutData(span2_1);

        GridLayout advLowerPartLayout = new GridLayout();
        advLowerPartLayout.numColumns = 1;
        advLowerPartLayout.marginWidth = 0;
        advLowerPartLayout.marginHeight = 0;

        advLowerPart.setLayout(advLowerPartLayout);

        Label ewaLabel = new Label(advLowerPart, SWT.NONE);
        ewaLabel.setText("Test tree weigher:");

        edgeWeightAggregatorCombo = new Combo(advLowerPart, SWT.READ_ONLY);
        edgeWeightAggregatorCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Label spacer = new Label(advLowerPart, SWT.NONE);
        spacer.setText(" ");

        Button editHeuristicsButton = new Button(advLowerPart, SWT.PUSH);
        editHeuristicsButton.setText("Custom weighers...");
        editHeuristicsButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        buttons.put(ButtonName.EDIT_HEURISTICS, editHeuristicsButton);

        preconfigButton = new Button(advLowerPart, SWT.PUSH);
        preconfigButton.setText("Standard configurations...");
        preconfigButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        debugButton = new Button(advLowerPart, SWT.PUSH);
        debugButton.setText("Debug settings...");
        debugButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        buttonSelectionListeners = new HashMap<>();
        for (ButtonName bn : ButtonName.values()) {
            buttonSelectionListeners.put(bn, new HashSet<Runnable>());
        }
        selectEdgeWeightAggregatorListeners = new HashSet<>();
        selectTraversalMethodChangedListeners = new HashSet<>();
        setAutoRecomputeListeners = new HashSet<>();
        setMaxRecursionDepthListeners = new HashSet<>();
        setInitialRecursionDepthListeners = new HashSet<>();
        setTraversalDepthThresholdListeners = new HashSet<>();
        selectPreconfigListeners = new HashSet<>();
        setLoggingFileRequestListeners = new HashSet<>();
        toggleAutostopModeRequestListeners = new HashSet<>();

        setupListeners();
        setupDebugButton();
        setupPreconfigButton();
    }

    /**
     * Sets up the widget listeners. Most of them call all listeners in their
     * corresponding listener set.
     */
    private void setupListeners() {
        for (final ButtonName bn : ButtonName.values()) {
            buttons.get(bn).addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent ev) {
                    for (Runnable listener : buttonSelectionListeners.get(bn)) {
                        listener.run();
                    }
                }
            });
        }

        maxRecursionDepthSpinner.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent arg0) {
                for (GenericListener<Integer> listener : setMaxRecursionDepthListeners) {
                    listener.onFiredEvent(maxRecursionDepthSpinner.getSelection());
                }
            }
        });

        initRecursionDepthSpinner.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent arg0) {
                for (GenericListener<Integer> listener : setInitialRecursionDepthListeners) {
                    listener.onFiredEvent(initRecursionDepthSpinner.getSelection());
                }
            }
        });

        traversalDepthChangeThreshSpinner.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent arg0) {
                for (GenericListener<Integer> listener : setTraversalDepthThresholdListeners) {
                    listener.onFiredEvent(traversalDepthChangeThreshSpinner.getSelection());
                }
            }
        });

        edgeWeightAggregatorCombo.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent arg0) {
                for (GenericListener<Integer> listener : selectEdgeWeightAggregatorListeners) {
                    listener.onFiredEvent(edgeWeightAggregatorCombo.getSelectionIndex());
                }
            }
        });
    }

    /**
     * Sets up the debug button's menu and corresponding listeners.
     */
    private void setupDebugButton() {
        /* When the debug button is clicked, a debug open menu pops up. */

        final Menu debugMenu = new Menu(debugButton);
        debugButton.setMenu(debugMenu);
        debugButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                Rectangle buttonRect = debugButton.getBounds();
                Point menuBase = debugButton.getParent().toDisplay(
                        new Point(buttonRect.x, buttonRect.y));
                debugMenu.setLocation(menuBase.x, menuBase.y + buttonRect.height);
                debugMenu.setVisible(true);
            }
        });

        MenuItem selectVisitor = new MenuItem(debugMenu, SWT.CASCADE);
        selectVisitor.setText("Graph traversal method");

        dbgSetAutorecompute = new MenuItem(debugMenu, SWT.CHECK);
        dbgSetAutorecompute.setText("Automatically compute new test trees");
        dbgSetAutorecompute.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (GenericListener<Boolean> listener : setAutoRecomputeListeners) {
                    listener.onFiredEvent(dbgSetAutorecompute.getSelection());
                }
            }
        });

        dbgSetAutostop = new MenuItem(debugMenu, SWT.CHECK);
        dbgSetAutostop.setText("Stop when all test objectives have been discharged");
        dbgSetAutostop.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (Runnable listener : toggleAutostopModeRequestListeners) {
                    listener.run();
                }
            }
        });

        dbgSetLogfile = new MenuItem(debugMenu, SWT.PUSH);
        dbgSetLogfile.setText("Set LazyOTF logfile...");
        dbgSetLogfile.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (Runnable listener : setLoggingFileRequestListeners) {
                    listener.run();
                }
            }
        });

        final Menu graphTraversalMenu = new Menu(selectVisitor);
        selectVisitor.setMenu(graphTraversalMenu);

        /* Add graph traversal methods when opening the debug menu. */
        debugMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuShown(MenuEvent arg0) {
                /* Clear the graph traversal method menu */
                for (MenuItem mi : graphTraversalMenu.getItems()) {
                    mi.dispose();
                }

                /* Add a menu item for each visitor in the visitor list. */
                for (int i = 0; i < dbgGraphTraversalMethods.size(); i++) {
                    final int index = i;
                    MenuItem newMenuItem = new MenuItem(graphTraversalMenu, SWT.RADIO);
                    newMenuItem.setSelection(index == dbgCurrentGraphTraversalMethod);
                    newMenuItem.setText(dbgGraphTraversalMethods.get(i).getName());
                    newMenuItem.addSelectionListener(new SelectionAdapter() {
                        @Override
                        public void widgetSelected(SelectionEvent ev) {
                            for (GenericListener<Integer> listener : selectTraversalMethodChangedListeners) {
                                listener.onFiredEvent(index);
                            }
                        }
                    });
                }

                /* If no graph traversal method is specified, say so. */
                if (dbgGraphTraversalMethods.size() == 0) {
                    MenuItem noneMenuItem = new MenuItem(graphTraversalMenu, SWT.PUSH);
                    noneMenuItem.setEnabled(false);
                    noneMenuItem.setText("(none available)");
                }
            }
        });
    }

    private boolean displayYesNoMsgBox(String caption, String question) {
        MessageBox mb = new MessageBox(parent.getShell(), SWT.ICON_WARNING | SWT.YES | SWT.NO);
        mb.setText(caption);
        mb.setMessage(question);
        return mb.open() == SWT.YES;
    }

    /**
     * Set up the "Standard configurations..." button.
     */
    public void setupPreconfigButton() {
        /* When the preconfig button is clicked, the preconfig menu pops up. */
        final Menu preconfigMenu = new Menu(preconfigButton);
        preconfigButton.setMenu(preconfigMenu);
        for (final Preconfiguration pcfg : Preconfigurations.ALL_PRECONFIGS) {
            MenuItem pcfgMenuItem = new MenuItem(preconfigMenu, SWT.PUSH);
            pcfgMenuItem.setText(pcfg.getName());
            pcfgMenuItem.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent ev) {
                    if (displayYesNoMsgBox(
                            "Warning",
                            "Warning: Using a standard configuration overrides your current configuration. "
                                    + "Do you want to proceed anyway?")) {
                        for (GenericListener<Preconfiguration> l : selectPreconfigListeners) {
                            l.onFiredEvent(pcfg);
                        }
                    }
                }
            });
        }

        preconfigButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                Rectangle buttonRect = preconfigButton.getBounds();
                Composite parent = preconfigButton.getParent();
                Point menuBase = parent.toDisplay(new Point(buttonRect.x, buttonRect.y));
                preconfigMenu.setLocation(menuBase.x, menuBase.y + buttonRect.height);
                preconfigMenu.setVisible(true);
            }
        });
    }

    /**
     * @return The embedded test type editor view's parent widget.
     */
    protected Composite getTestTypeAssignmentViewParent() {
        return testTypeGroupView;
    }

    /**
     * Updates the embedded test type editor view's parent widget (i.e., calls
     * its layout routine).
     */
    protected void updateTestTypeAssignmentViewParent() {
        testTypeGroupView.layout();
    }

    /**
     * Iff set to <tt>true</tt>, all controls within the pane get grayed out.
     * The enabledness of the buttons gets preserved.
     * 
     * @param enabledness
     */
    public void setEnabled(boolean enabledness) {
        for (Button b : buttons.values()) {
            boolean canBeUngrayed = true;
            if (b.getData("isEnabled") != null) {
                canBeUngrayed = (Boolean) b.getData("isEnabled");
            }
            b.setEnabled(canBeUngrayed && enabledness);
        }

        boolean preconfigButtonCanBeUngrayed = true;
        if (preconfigButton.getData("isEnabled") != null) {
            preconfigButtonCanBeUngrayed = (Boolean) preconfigButton.getData("isEnabled");
        }
        preconfigButton.setEnabled(enabledness && preconfigButtonCanBeUngrayed);

        debugButton.setEnabled(enabledness);
        maxRecursionDepthSpinner.setEnabled(enabledness);
        initRecursionDepthSpinner.setEnabled(enabledness);
        traversalDepthChangeThreshSpinner.setEnabled(enabledness);
        edgeWeightAggregatorCombo.setEnabled(enabledness);
        if (testTypeGroupView != null && !testTypeGroupView.isDisposed()) {
            testTypeGroupView.setEnabled(enabledness);
            for (Control c : testTypeGroupView.getChildren()) {
                c.setEnabled(enabledness);
            }
        }
    }

    /**
     * Sets the grayed-outness of non-menu buttons.
     * 
     * @param button
     *            The name of the button to be enabled/disabled.
     * @param enabledness
     *            <tt>true</tt> if the button should be enabled.
     */
    public void setButtonEnabled(ButtonName button, boolean enabledness) {
        Button b = buttons.get(button);
        b.setEnabled(enabledness);
        b.setData("isEnabled", enabledness);
    }

    /**
     * Sets the grayed-outness of the standard config button.
     * 
     * @param enabledness
     *            <tt>true</tt> if the button should be enabled.
     */
    public void setPreconfigButtonEnabled(boolean enabledness) {
        preconfigButton.setEnabled(enabledness);
        preconfigButton.setData("isEnabled", enabledness);
    }

    /**
     * Removes the embedded test type group editor widget.
     * 
     * @param drawDefaultControl
     *            If <tt>true</tt>, a default {@link Tree} control is drawn.
     *            Otherwise, the area is left empty.
     */
    protected void clearTestTypeAssignmentViewParent(boolean drawDefaultControl) {
        for (Control c : testTypeGroupView.getChildren()) {
            if (!c.isDisposed()) {
                c.dispose();
            }
        }

        if (drawDefaultControl) {
            Tree defaultTree = new Tree(testTypeGroupView, SWT.NONE);
            defaultTree.setToolTipText("You need to load a model before assigning test types.");
            testTypeGroupView.layout();
        }
    }

    /**
     * Sets the list of available model traversal methods.
     * 
     * @param methods
     *            The new list of available model traversal methods.
     */
    public void setGraphTraversalMethods(List<TraversalMethod> methods) {
        assert (methods != null);
        dbgGraphTraversalMethods = methods;
        dbgCurrentGraphTraversalMethod = -1;
    }

    /**
     * Sets the currently selected model traversal method.
     * 
     * @param index
     *            The index of the new model traversal method within the list of
     *            model traversal methods provided to
     *            <i>setGraphTraversalMethods</i>.
     */
    public void setCurrentGraphTraversalMethod(int index) {
        dbgCurrentGraphTraversalMethod = index;
    }

    /**
     * Sets the current max. model traversal depth.
     * 
     * @param depth
     *            The new model traversal depth, which must be a positive
     *            integer.
     */
    public void setMaxTraversalRecursionDepth(int depth) {
        assert (depth >= 0);
        maxRecursionDepthSpinner.setSelection(depth);
    }

    /**
     * @return The currently selected maximum model traversal depth.
     */
    public int getSelectedMaxTraversalRecursionDepth() {
        return maxRecursionDepthSpinner.getSelection();
    }

    /**
     * Sets the current initial model traversal depth.
     * 
     * @param depth
     *            The new initial model traversal depth, which must be a
     *            positive integer.
     */
    public void setInitialTraversalRecursionDepth(int depth) {
        assert (depth >= 0);
        initRecursionDepthSpinner.setSelection(depth);
    }

    /**
     * @return The currently selected initial model traversal depth.
     */
    public int getSelectedInitialTraversalDepth() {
        return initRecursionDepthSpinner.getSelection();
    }

    /**
     * Sets the current traversal depth change threshold.
     * 
     * @param value
     *            The new traversal depth change threshold.
     */
    public void setTraversalDepthThreshold(int value) {
        assert (value >= 0);
        traversalDepthChangeThreshSpinner.setSelection(value);
    }

    /**
     * @return The currently selected traversal depth change threshold.
     */
    public int getSelectedTraversalDepthThreshold() {
        return traversalDepthChangeThreshSpinner.getSelection();
    }

    /**
     * Sets the auto-recompute flag.
     * 
     * @param enabledness
     *            <tt>true</tt> iff the auto-recompute flag should be set.
     */
    public void setAutorecomputeEnabled(boolean enabledness) {
        dbgSetAutorecompute.setSelection(enabledness);
    }

    /**
     * @return <tt>true</tt> if the auto-recompute flag is currently set.
     */
    public boolean isAutorecomputeEnabled() {
        return dbgSetAutorecompute.getSelection();
    }

    /**
     * Sets the auto-stop flag.
     * 
     * @param enabledness
     *            <tt>true</tt> iff the auto-stop flag should be set.
     */
    public void setAutostopEnabled(boolean enabledness) {
        dbgSetAutostop.setSelection(enabledness);
    }

    /**
     * @return <tt>true</tt> if the auto-stop flag is currently set.
     */
    public boolean isAutostopEnabled() {
        return dbgSetAutostop.getSelection();
    }

    /**
     * Sets the list of available edge weight aggregators.
     * 
     * @param aggregators
     *            The list of currently available edge weight aggregators.
     */
    public void setEdgeWeightAggregators(List<TestTree2Weight> aggregators) {
        edgeWeightAggregatorCombo.removeAll();
        for (TestTree2Weight ewa : aggregators) {
            edgeWeightAggregatorCombo.add(ewa.getName());
        }
    }

    /**
     * Sets the minimal value of the max. model traversal depth spin control.
     * 
     * @param minRecDepth
     *            The new minimal value of the model traversal depth spin
     *            control.
     */
    public void setMaxRecursionDepthMinValue(int minRecDepth) {
        maxRecursionDepthSpinner.setMinimum(minRecDepth);
    }

    /**
     * Sets the maximal value of the model traversal depth spin control.
     * 
     * @param maxRecDepth
     *            The new maximal value of the model traversal depth spin
     *            control.
     */
    public void setMaxRecursionDepthMaxValue(int maxRecDepth) {
        maxRecursionDepthSpinner.setMaximum(maxRecDepth);
    }

    /**
     * Sets the minimal value of the initial model traversal depth spin control.
     * 
     * @param minRecDepth
     *            The new minimal value of the model traversal spin control.
     */
    public void setInitialRecursionDepthMinValue(int minRecDepth) {
        initRecursionDepthSpinner.setMinimum(minRecDepth);
    }

    /**
     * Sets the maximal value of the initial model traversal depth spin control.
     * 
     * @param maxRecDepth
     *            The new maximal value of the model traversal spin control.
     */
    public void setInitialRecursionDepthMaxValue(int maxRecDepth) {
        initRecursionDepthSpinner.setMaximum(maxRecDepth);
    }

    /**
     * Sets the minimal value of the model traversal depth modification
     * threshold spin control.
     * 
     * @param min
     *            The new minimal value.
     */
    public void setTraversalDepthThresholdMinValue(int min) {
        traversalDepthChangeThreshSpinner.setMinimum(min);
    }

    /**
     * Sets the maximal value of the model traversal depth modification
     * threshold spin control.
     * 
     * @param max
     *            The new maximal value.
     */
    public void setTraversalDepthThresholdMaxValue(int max) {
        traversalDepthChangeThreshSpinner.setMaximum(max);
    }

    /**
     * @return The index of the edge weight aggregator selection within the list
     *         of edge weight aggregators provided to
     *         <i>setEdgeWeightAggregators()</i>.
     */
    public int getSelectedEdgeWeightAggregator() {
        return edgeWeightAggregatorCombo.getSelectionIndex();
    }

    /**
     * Sets the currently selected edge weight aggregator.
     * 
     * @param index
     *            The index of the new selected edge weight aggregator within
     *            the list of edge weight aggregators supplied via
     *            <i>setEdgeWeightAggregators</i>.
     */
    public void setEdgeWeightAggregatorSelection(int index) {
        edgeWeightAggregatorCombo.select(index);
    }

    /**
     * Display an error message box with an OK button.
     * 
     * @param message
     *            The message to be displayed.
     */
    public void displayErrorMessageBox(String message) {
        if (message != null) {
            MessageBox msg = new MessageBox(parent.getShell(), SWT.ERROR);
            msg.setMessage(message);
            msg.setText("JTorX/LazyOTF");
            msg.open();
        }
        else {
            throw new IllegalArgumentException("message: Argument cannot be null");
        }
    }

    /**
     * Add a listener getting fired when a non-menu button is clicked.
     * 
     * @param button
     *            The button's name.
     * @param listener
     *            The listener.
     */
    public void addSelectButtonListener(ButtonName button, Runnable listener) {
        buttonSelectionListeners.get(button).add(listener);
    }

    /**
     * Add a listener getting fired when the user requests to use a preconfig.
     * 
     * @it The new listener.
     */
    public void addSelectPreconfigListener(GenericListener<Preconfiguration> it) {
        selectPreconfigListeners.add(it);
    }

    /**
     * Add a listener getting fired when the edge weight aggregator selection
     * changes.
     * 
     * @param listener
     *            The listener, taking the edge weight aggregator index as an
     *            argument.
     */
    public void addSelectEdgeWeightAggregatorListener(GenericListener<Integer> listener) {
        selectEdgeWeightAggregatorListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the model traversal method selection
     * changes.
     * 
     * @param listener
     *            The listener, taking the model traversal method index as an
     *            argument.
     */
    public void addSelectTraversalMethodChangedListener(GenericListener<Integer> listener) {
        selectTraversalMethodChangedListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the auto-recompute flag is changed.
     * 
     * @param listener
     *            The listener, taking the flag's new value as an argument.
     */
    public void addSetAutoRecomputeListener(GenericListener<Boolean> listener) {
        setAutoRecomputeListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the max. model traversal depth is
     * changed.
     * 
     * @param listener
     *            The listener, taking the new depth as an argument.
     */
    public void addSetMaxRecursionDepthListener(GenericListener<Integer> listener) {
        setMaxRecursionDepthListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the initial model traversal depth is
     * changed.
     * 
     * @param listener
     *            The listener, taking the new initial depth as an argument.
     */
    public void addSetInitialRecursionDepthListener(GenericListener<Integer> listener) {
        setInitialRecursionDepthListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the model traversal depth change
     * threshold is changed.
     * 
     * @param listener
     *            The listener, taking the new threshold as an argument.
     */
    public void addSetTraversalDepthThresholdListener(GenericListener<Integer> listener) {
        setTraversalDepthThresholdListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the user toggles the autostep autostop
     * mode.
     * 
     * @param it
     *            The listener to be added.
     */
    public void addToggleAutostopModeListener(Runnable it) {
        toggleAutostopModeRequestListeners.add(it);
    }

    /**
     * Add a listener getting fired when the user wishes to set the logging
     * file.
     * 
     * @param it
     *            The listener to be added.
     */
    public void addSetLoggingFileRequestedListener(Runnable it) {
        setLoggingFileRequestListeners.add(it);
    }
}
