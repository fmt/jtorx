package utwente.fmt.jtorx.testgui.lazyotf.config;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;

public final class LogFileSelector {
    public static String open() {
        return open(null);
    }

    public static String open(String currentFile) {
        FileDialog fd = new FileDialog(Display.getDefault().getActiveShell(), SWT.SAVE);
        fd.setText("Set LazyOTF log file");
        if (currentFile != null) {
            fd.setFileName(currentFile);
        }
        return fd.open();
    }

    /** Utility class - cannot be instantiated. */
    private LogFileSelector() {

    }
}
