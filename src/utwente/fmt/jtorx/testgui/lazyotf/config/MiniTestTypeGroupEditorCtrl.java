package utwente.fmt.jtorx.testgui.lazyotf.config;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.TestTypeGroupCtrl;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.TestTypeGroupView;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * A controller for the miniature test type group editor. This class is
 * responsible for keeping track which test type group is currently displayed
 * and for updating the view e.g. after changes to the set of location sets.
 */
public class MiniTestTypeGroupEditorCtrl {
    /** The controlled view. */
    private final MiniTestTypeGroupEditorView view;

    /**
     * {@link MiniTestTypeGroupEditorView} contains a {@link TestTypeGroupView}
     * instance, which is controlled by this {@link TestTypeGroupCtrl} object.
     */
    private final TestTypeGroupCtrl ctrl;

    /**
     * The current list of test type groups. Used fot the test type group combo
     * control.
     */
    private final List<UITestTypeGroup> currentTTGs;

    /**
     * Constructs a new {@link MiniTestTypeGroupEditorCtrl} instance using the
     * given <i>view</i> and corresponding test type group controller.
     * 
     * @param view
     *            The view which should be controlled.
     * @param ctrl
     *            The controller of the test type group view contained within
     *            the given view.
     */
    public MiniTestTypeGroupEditorCtrl(MiniTestTypeGroupEditorView view,
            TestTypeGroupCtrl ctrl) {
        this.view = view;
        this.ctrl = ctrl;
        currentTTGs = new LinkedList<>();

        view.addComboSelectionListener(new GenericListener<Integer>() {
            @Override
            public void onFiredEvent(Integer idx) {
                TestTypeGroupCtrl ctrl = MiniTestTypeGroupEditorCtrl.this.ctrl;
                ctrl.setTestTypeGroup(currentTTGs.get(idx));
            }
        });
    }

    /**
     * Sets the current test type group.
     * 
     * @param it
     *            The new current test type group.
     */
    public void setTestTypeGroup(UITestTypeGroup it) {
        ctrl.setTestTypeGroup(it);
    }

    /**
     * Updates the view to reflect changes in the set of available test type
     * groups.
     * 
     * @param newGroups
     *            The new set of test type groups.
     * @param defaultType
     *            If the currently displayed test type group is not contained
     *            with in <i>newGroups</i>, this test type group will be
     *            displayed instead. Must be an element of <i>newGroups</i>.
     */
    public void update(Set<UITestTypeGroup> newGroups, UITestTypeGroup defaultType) {
        view.clearTTGCombo();
        currentTTGs.clear();
        for (UITestTypeGroup ttg : newGroups) {
            int idx = view.addToCombo(ttg.getDisplayName());
            currentTTGs.add(idx, ttg);
        }

        if (!newGroups.contains(ctrl.getTestTypeGroup())) {
            ctrl.setTestTypeGroup(defaultType);
            view.setComboText(defaultType.getDisplayName());
        }
        else {
            view.redraw();
            view.setComboText(ctrl.getTestTypeGroup().getDisplayName());
        }
    }
}
