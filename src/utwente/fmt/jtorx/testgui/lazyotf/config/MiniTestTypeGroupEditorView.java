package utwente.fmt.jtorx.testgui.lazyotf.config;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.TestTypeGroupView;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.TestTypeGroupView.CtrlInterface;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * <p>
 * The miniature test type group editor view. This view contains a
 * {@link TestTypeGroupView} and a {@link Combo} control for test type group
 * selection.
 * </p>
 * 
 * <p>
 * Extending SWT's {@link Composite}, this view is intended to be used like an
 * SWT control.
 * </p>
 * 
 * <p>
 * See also {@link TestTypeGroupView}.
 * </p>
 */
public class MiniTestTypeGroupEditorView extends Composite {
    /** The test type group view. */
    private final TestTypeGroupView ttgView;

    /** The test type group selection {@link Combo} control. */
    private final Combo ttgSel;

    /**
     * The set of listeners getting fired when the test type group selection
     * combo control selection changes.
     */
    private final Set<GenericListener<Integer>> comboSelectionListeners;

    /**
     * Constructs a new {@link MiniTestTypeGroupEditorView} instance within the
     * given <i>parent</i> composite.
     * 
     * @param parent
     *            The view's <i>parent</i> composite.
     * @param testTypes
     *            The set of test types known to the {@link TestTypeGroupView}
     *            instance within this control.
     * @param ctrlIf
     *            An interface allowing queries to various configuration
     *            settings. Except for {@link UITestTypeGroup} objects, this
     *            data will not be changed by the
     *            {@link MiniTestTypeGroupEditorView}.
     */
    public MiniTestTypeGroupEditorView(Composite parent, Set<TestType> testTypes,
            CtrlInterface ctrlIf) {
        super(parent, SWT.NONE);

        GridLayout ourLayout = new GridLayout();
        ourLayout.numColumns = 3;
        setLayout(ourLayout);

        Label ttgLabel = new Label(this, SWT.NONE);
        ttgLabel.setText("Test Objective:");

        GridData comboLayout = new GridData();
        comboLayout.horizontalIndent = 10;
        comboLayout.widthHint = 200;
        ttgSel = new Combo(this, SWT.READ_ONLY);
        ttgSel.setLayoutData(comboLayout);

        ttgView = new TestTypeGroupView(this, testTypes, ctrlIf);

        GridData ttgViewLayout = new GridData(GridData.FILL_BOTH);
        ttgViewLayout.horizontalSpan = 3;
        ttgView.setLayoutData(ttgViewLayout);

        comboSelectionListeners = new HashSet<>();

        ttgSel.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (GenericListener<Integer> listener : comboSelectionListeners) {
                    listener.onFiredEvent(ttgSel.getSelectionIndex());
                }
            }
        });
    }

    /**
     * Adds a listener getting fired whenever the user changes the test type
     * group combo box selection.
     * 
     * @param it
     *            The listener to be added, taking the new selection index as
     *            its argument.
     */
    public void addComboSelectionListener(GenericListener<Integer> it) {
        comboSelectionListeners.add(it);
    }

    /**
     * Gets the {@link TestTypeGroupView} control contained within this widget.
     * 
     * @return The {@link TestTypeGroupView} control.
     */
    public TestTypeGroupView getTestTypeGroupView() {
        return ttgView;
    }

    /**
     * Removes all items from the test type group combo box.
     */
    public void clearTTGCombo() {
        ttgSel.removeAll();
    }

    /**
     * Adds a name to the test type group selection combo box.
     * 
     * @param name
     *            The test type group's name.
     * @return The index of <i>name</i> within the combo box.
     */
    public int addToCombo(String name) {
        int insertionIndex = ViewUtilities.determineInsertionIndex(ttgSel.getItems(),
                name);
        ttgSel.add(name, insertionIndex);
        return insertionIndex;
    }

    /**
     * Sets the current combo box selection text.
     * 
     * @param it
     *            The new combo box selection text.
     */
    public void setComboText(String it) {
        ttgSel.setText(it);
    }

    @Override
    public void setEnabled(boolean enabledness) {
        ttgSel.setEnabled(enabledness);
        ttgView.setEnabled(enabledness);
        super.setEnabled(enabledness);
    }

    @Override
    public void redraw() {
        ttgView.redraw();
        ttgSel.redraw();
    }
}
