package utwente.fmt.jtorx.testgui.lazyotf.config;

import java.util.HashSet;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;

public class Preconfigurations {
    public static final Set<Preconfiguration> ALL_PRECONFIGS;

    static {
        ALL_PRECONFIGS = new HashSet<>();
        ALL_PRECONFIGS.add(makeCoveragePreconfig());
        ALL_PRECONFIGS.add(makeDefaultPreconfig());
    }

    public static Preconfiguration makeCoveragePreconfig() {
        return new Preconfiguration() {
            @Override
            public void install(LazyOTFConfigSessionData cfg) {
                cfg.getTestTypeGroups().clear();
                for (NamedSet<Location> l : cfg.getBasisLocationSets()) {
                    UITestTypeGroup newTTG = new UITestTypeGroup(TestType.ALL_TEST_TYPES);
                    newTTG.setDisplayName("Cover " + l.getDisplayName());
                    newTTG.associate(l, TestType.TEST_GOAL);
                    cfg.getTestTypeGroups().add(newTTG);
                }
            }

            @Override
            public String getName() {
                return "Location Coverage";
            }
        };
    }

    public static Preconfiguration makeDefaultPreconfig() {
        return new Preconfiguration() {
            @Override
            public void install(LazyOTFConfigSessionData cfg) {
                cfg.getTestTypeGroups().clear();
                UITestTypeGroup testGoalGroup = new UITestTypeGroup(TestType.ALL_TEST_TYPES);
                testGoalGroup.setDisplayName("Initial Test Objective");
                cfg.getTestTypeGroups().add(testGoalGroup);
            }

            @Override
            public String getName() {
                return "Default";
            }
        };
    }

    public interface Preconfiguration {
        void install(LazyOTFConfigSessionData cfg);

        String getName();
    }
}
