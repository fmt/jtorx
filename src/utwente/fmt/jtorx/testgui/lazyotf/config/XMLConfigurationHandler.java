package utwente.fmt.jtorx.testgui.lazyotf.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import utwente.fmt.jtorx.lazyotf.LazyOTFConfigurationOptions;
import utwente.fmt.jtorx.lazyotf.TestTree2Weight;
import utwente.fmt.jtorx.lazyotf.TraversalMethod;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2WeightImpl;
import utwente.fmt.jtorx.lazyotf.guidance.LocationValuation2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Locations2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2WeightVisitor;
import utwente.fmt.jtorx.lazyotf.guidance.SimplePath2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.SuperLocationSize2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightVisitor;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.policy.Defaults;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.Condition;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.DumontCondition;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategyVisitor;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIMappingStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UISatStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;
import utwente.fmt.jtorx.utils.Pair;

/**
 * {@link XMLConfigurationHandler} takes care of storing and loading the LazyOTF
 * configuration to rsp. from XML strings complying to the schema defined in
 * lazyotfconfig.xsd.
 * 
 * Note: The legacy name of {@link Path2Weight} is "GuidanceMethod".
 */
public class XMLConfigurationHandler {
    /** The set of test types known to the importer/exporter. */
    private Set<TestType> knownTestTypes;

    /** The set of locations known to the importer/exporter. */
    private Set<NamedSet<Location>> knownLocations;

    /** A map from location names to locations. */
    private Map<String, NamedSet<Location>> locationMap;

    /** A map from test type names to test types. */
    private Map<String, TestType> testTypeMap;

    /** JAXB object factory for lazyotfconfig.xsd. */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.ObjectFactory xmlFactory;

    /** The currently loaded document's root element. */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig rootElement = null;
    
    /** If true, unknown entities are added to the LazyOTF configuration options when loading a configuration. */
    private boolean isCompletingConfigurations;

    /**
     * Constructs a new {@link XMLConfigurationHandler} instance, constructing a
     * new set of location sets from the given set of locations.
     * 
     * @param knownLocations
     *            The {@link Location}s known to the loader.
     */
    public XMLConfigurationHandler(Set<Location> knownLocations) {
        this(TestType.ALL_TEST_TYPES, makeLocationSets(knownLocations));
    }

    /**
     * @param locations
     *            A set of {@link Location} objects.
     * @return The corresponding set of <tt>null</tt>-named {@link NamedSet}
     *         objects.
     */
    private static Set<NamedSet<Location>> makeLocationSets(Set<Location> locations) {
        Set<NamedSet<Location>> locationSets = new HashSet<>();
        for (Location l : locations) {
            locationSets.add(NamedSet.makeNamedSet(null, l));
        }
        return locationSets;
    }

    /**
     * Construct a new {@link XMLConfigurationHandler} instance.
     * 
     * @param knownTestTypes
     *            The set of test types known to the importer/exporter.
     * @param knownLocations
     *            The set of locations known to the importer/exporter.
     */
    public XMLConfigurationHandler(Set<TestType> knownTestTypes,
            Set<NamedSet<Location>> knownLocations) {
        this.knownLocations = knownLocations;
        this.knownTestTypes = knownTestTypes;

        locationMap = new HashMap<String, NamedSet<Location>>();
        testTypeMap = new HashMap<String, TestType>();

        xmlFactory = new utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.ObjectFactory();

        if (this.knownLocations != null) {
            for (NamedSet<Location> l : this.knownLocations) {
                locationMap.put(l.getDisplayName(), l);
            }
        }

        if (this.knownTestTypes != null) {
            for (TestType t : this.knownTestTypes) {
                testTypeMap.put(t.getInternalName(), t);
            }
        }
    }

    /**
     * Load the specified XML document adhering to lazyotfconfig.xsd.
     * 
     * @param fileName
     *            The file's name.
     * @throws ParseException
     *             The file does not adhere to lazyotfconfig.xsd.
     * @throws IllegalSemanticsException
     *             The file contains location or test type names not known to
     *             this importer.
     * @throws FileNotFoundException
     *             A system error occured.
     */
    @SuppressWarnings("unchecked")
    public void readConfigFromFile(String fileName) throws ParseException,
            IllegalSemanticsException, FileNotFoundException {

        try {
            JAXBContext jaxbContext = JAXBContext
                    .newInstance("utwente.fmt.jtorx.testgui.lazyotf.config.jaxb");
            Unmarshaller unm = jaxbContext.createUnmarshaller();

            if (fileName == null) {
                throw new FileNotFoundException();
            }
            File xmlFile = new File(fileName);
            if (!xmlFile.exists()) {
                throw new FileNotFoundException(fileName);
            }

            Object XMLRootObj = unm.unmarshal(xmlFile);

            if (XMLRootObj instanceof JAXBElement<?>) {
                JAXBElement<utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig> rootElement;
                rootElement = (JAXBElement<utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig>) XMLRootObj;
                this.rootElement = rootElement.getValue();
            }
            else if (XMLRootObj instanceof utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig) {
                utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig lc;
                lc = (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig) XMLRootObj;
                rootElement = lc;
            }
            else {
                System.err.println("Internal error: XML root element of unexpected type "
                        + XMLRootObj.getClass().getName());
                throw new ParseException();
            }
        }
        catch (JAXBException e) {
            throw new ParseException();
        }
    }

    @SuppressWarnings("unchecked")
    public void readConfigFromString(String cfg) throws ParseException {
        try {
            JAXBContext jaxbContext = JAXBContext
                    .newInstance("utwente.fmt.jtorx.testgui.lazyotf.config.jaxb");
            Unmarshaller unm = jaxbContext.createUnmarshaller();

            StringReader strr = new StringReader(cfg);

            Object XMLRootObj = unm.unmarshal(strr);

            if (XMLRootObj instanceof JAXBElement<?>) {
                JAXBElement<utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig> rootElement;
                rootElement = (JAXBElement<utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig>) XMLRootObj;
                this.rootElement = rootElement.getValue();
            }
            else if (XMLRootObj instanceof utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig) {
                utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig lc;
                lc = (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig) XMLRootObj;
                rootElement = lc;
            }
            else {
                System.err.println("Internal error: XML root element of unexpected type "
                        + XMLRootObj.getClass().getName());
                throw new ParseException();
            }
        }
        catch (JAXBException e) {
            throw new ParseException();
        }

    }

    /**
     * Make a UIDecisionStrategy object corresponding to the given JAXB decision
     * strategy object.
     * 
     * @param strat
     *            The JAXB decision strategy object.
     * @param globalConditionMap
     *            An association of global decision strategy IDs to global
     *            decision strategies.
     * @return A DecisionStrategy object corresponding to the given JAXB
     *         decision strategy object.
     * @throws IllegalSemanticsException
     */
    private UIDecisionStrategy makeDecisionStrategy(
            utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.DecisionStrategy strat,
            Map<String, Condition> globalConditionMap,
            Map<Integer, NamedSet<Location>> locationSetRefMap) throws IllegalSemanticsException {
        UIDecisionStrategy result;

        if (strat.getDirectDecisionStratgegy() != null) {
            result = new UIDecisionStrategy();

            result.setDisplayName(strat.getDirectDecisionStratgegy().getName());
        }
        else if (strat.getMappingStrategy() != null) {
            UIMappingStrategy subTypedResult = new UIMappingStrategy();
            result = subTypedResult;
            utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.MappingStrategy ms = strat
                    .getMappingStrategy();
            for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationTypeMap mapItem : ms
                    .getMap()) {
                NamedSet<Location> loc;
                if (mapItem.getLocationSetId() != null
                        && locationSetRefMap.containsKey(mapItem.getLocationSetId())) {
                    loc = locationSetRefMap.get(mapItem.getLocationSetId());
                }
                else {
                    if (!locationMap.containsKey(mapItem.getLocation())) {
                        if (isCompletingConfigurations) {
                            addLocation(mapItem.getLocation());
                        }
                    }
                    loc = locationMap.get(mapItem.getLocation());
                }

                TestType type = testTypeMap.get(mapItem.getTestType().toString());

                if (loc == null) {
                    throw new IllegalSemanticsException("Unknown location " + mapItem.getLocation());
                }
                if (!testTypeMap.containsKey(mapItem.getTestType().toString())) {
                    throw new IllegalSemanticsException("Unknown test type "
                            + mapItem.getTestType().toString());
                }

                subTypedResult.setTestType(loc, type);
            }
        }
        else if (strat.getSatStrategy() != null) {
            UISatStrategy subTypedResult = new UISatStrategy(knownTestTypes);
            result = subTypedResult;
            utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SatStrategy source = strat
                    .getSatStrategy();
            Map<String, Condition> localConditionMap = makeConditionIDMap(source.getConditions()
                    .getCondition());

            for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationConditionAssociation lca : source
                    .getAssociations().getAssoc()) {
                SatStrategy.Condition cond;
                if (lca.getCondition().startsWith("g:")) {
                    cond = globalConditionMap.get(lca.getCondition());
                }
                else {
                    cond = localConditionMap.get(lca.getCondition());
                }

                if (!testTypeMap.containsKey(lca.getTestType().toString())) {
                    throw new IllegalSemanticsException("Unknown test type "
                            + lca.getTestType().toString());
                }

                NamedSet<Location> loc;
                if (lca.getLocationSetID() != null
                        && locationSetRefMap.containsKey(lca.getLocationSetID())) {
                    loc = locationSetRefMap.get(lca.getLocationSetID());
                }
                else {
                    if (!locationMap.containsKey(lca.getLocation())) {
                        if (isCompletingConfigurations) {
                            addLocation(lca.getLocation());
                        }
                    }
                    loc = locationMap.get(lca.getLocation());
                }

                if (loc == null) {
                    throw new IllegalSemanticsException("Unknown location " + lca.getLocation());
                }

                subTypedResult.associate(loc, testTypeMap.get(lca.getTestType().toString()), cond);
            }
        }
        else {
            throw new IllegalArgumentException(
                    "Argument strat must describe exactly one decision strategy.");
        }
        result.setDisplayName(strat.getMoniker());
        return result;
    }

    /**
     * Make SatStrategy.Condition objects corresponding to the given JAXB
     * SatStrategyCondition instances and put them in a map associating
     * condition IDs with their respective ID.
     * 
     * @param conds
     *            The XML condition representations to be put into the ID map.
     * @return A map associating conditoin IDs with their respective ID.
     */
    private Map<String, Condition> makeConditionIDMap(
            List<utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SatStrategyCondition> conds) {
        Map<String, Condition> conditionMap = new HashMap<String, Condition>();
        for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SatStrategyCondition cond : conds) {
            SatStrategy.DumontCondition newDC = new SatStrategy.DumontCondition(cond.getCond());
            newDC.setName(cond.getName());
            conditionMap.put(cond.getId(), newDC);
        }
        return conditionMap;
    }

    private Map<Integer, NamedSet<Location>> getNamedLocationSets(
            List<utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationSet> namedLocSets)
            throws IllegalSemanticsException {
        Map<Integer, NamedSet<Location>> result = new HashMap<>();
        for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationSet ls : namedLocSets) {
            NamedSet<Location> newNamedLocationSet = new NamedSet<Location>(ls.getName());
            for (String locationName : ls.getLocation()) {
                if (!locationMap.containsKey(locationName)) {
                    if (isCompletingConfigurations) {
                        addLocation(locationName);
                    }
                    else {
                        throw new IllegalSemanticsException("Unknown location " + locationName + ".");
                    }
                }
                newNamedLocationSet.add(locationMap.get(locationName).iterator().next());
            }
            result.put(ls.getId(), newNamedLocationSet);
        }
        return result;
    }
    
    public LazyOTFConfigSessionData getConfiguration() throws IllegalSemanticsException {
        return getConfiguration(null);
    }

    /**
     * Get the test type groups from the currently loaded XML document.
     * 
     * @param configOptions
     *            the LazyOTF configuration options item to be used for the new
     *            session data object.
     * @return The test type groups specified by the currently loaded XML
     *         document.
     * @throws IllegalSemanticsException
     *             The file contains location or test type names not known to
     *             this importer.
     */
    public LazyOTFConfigSessionData getConfiguration(LazyOTFConfigurationOptions configOptions)
            throws IllegalSemanticsException {
        if (rootElement == null) {
            throw new IllegalStateException(
                    "Cannot read test type groups from XMLConfigurationHandler without XML configuration");
        }
        
        LazyOTFConfigSessionData result;
        if (configOptions != null || !isCompletingConfigurations) {
            result = new LazyOTFConfigSessionData();
            result.setConfigOptions(configOptions);
        }
        else {
            result = new NonGuiLazyOTFConfigSessionData();
        }

        result.setLocations(knownLocations);

        Map<String, Condition> globalConditionMap = new HashMap<>();
        Map<String, UIDecisionStrategy> strategyMap = new HashMap<>();
        Map<Integer, NamedSet<Location>> locationSetMap;

        /* Read named location sets */
        if (rootElement.getLocationSets() != null) {
            locationSetMap = getNamedLocationSets(rootElement.getLocationSets().getLocationSet());
        }
        else {
            locationSetMap = new HashMap<>();
        }

        result.setLocationSets(new HashSet<>(locationSetMap.values()));

        /* Decision strategies: Read global SatStrategy conditions */
        globalConditionMap = makeConditionIDMap(rootElement.getSatStrategyGlobalConditions()
                .getCondition());

        /* Read decision strategies */
        for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.DecisionStrategy strat : rootElement
                .getDecisionStrategies().getStrategy()) {
            UIDecisionStrategy newDS = makeDecisionStrategy(strat, globalConditionMap,
                    locationSetMap);
            strategyMap.put(strat.getId(), newDS);
        }

        /* Update decision strategy info in result */
        Set<Condition> globalConditions = new HashSet<Condition>(globalConditionMap.values());
        result.setGlobalConditions(globalConditions);

        List<UIDecisionStrategy> strategies = new ArrayList<>(strategyMap.values());
        result.setStrategies(strategies);

        result.setAutomaticTestTreeComputationEnabled(rootElement.isAutorecompute());
        result.setAutoSteStopMode(rootElement.isAutostop());
        result.setLogFilename(rootElement.getLoggingFile());

        if (rootElement.getEdgeweightaggregator() != null) {
            if (!isCompletingConfigurations || configOptions != null) {
                result.setEdgeWeightAggregatorName(configOptions.tree2WeightByName(rootElement
                        .getEdgeweightaggregator()));
            }
            else {
                result.setEdgeWeightAggregatorName(new TestTree2Weight() {
                    private String name = rootElement.getEdgeweightaggregator();
                    @Override
                    public String getName() {
                        return name;
                    }

                    @Override
                    public void readFromString(String s) {
                        name = s;
                    }
                });
            }
        }
        result.setMaxRecursionDepth(rootElement.getMaxdepth());
        result.setInitialRecursionDepth(rootElement.getInitialdepth());
        result.setRecursionDepthModThreshold(rootElement.getDepththreshold());
        result.setCheckedPREDfsVisitorEnabled(true);
        if (rootElement.getVisitor() != null) {
            if (!isCompletingConfigurations || configOptions != null) {
                result.setVisitorName(configOptions.traversalMethodByName(rootElement.getVisitor()));
            }
            else {
                result.setVisitorName(new TraversalMethod() {
                    private String name = rootElement.getVisitor();
                    @Override
                    public void readFromString(String s) {
                        name = s;
                    }
                    
                    @Override
                    public String getName() {
                        return name;
                    }
                });
            }
        }

        /* Read node value weighers */
        final Map<String, NodeValue2Weight> nv2wMap = new HashMap<>();
        Set<NodeValue2Weight> nv2wList = new HashSet<>();
        if (rootElement.getNodeValueWeighers() != null) {
            for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.NodeValueWeigher nvSkel : rootElement
                    .getNodeValueWeighers().getNodeValueWeigher()) {
                if (nvSkel.getMoniker() == null || nvSkel.getId() == null) {
                    throw new IllegalSemanticsException(
                            "Node value weighers must have a name and an ID.");
                }
                NodeValue2Weight nvInstance = null;
                if (nvSkel.getLocations2Weight() != null) {
                    nvInstance = Locations2Weight.newInstance(nvSkel.getMoniker(),
                            nvSkel.getLocations2Weight().getInducingWeight(),
                            nvSkel.getLocations2Weight().getOrdinaryWeight(),
                            nvSkel.getLocations2Weight().getTestGoalWeight());
                }
                else if (nvSkel.getLocationValuation2Weight() != null) {
                    List<Pair<String, String>> exprPairs = new ArrayList<>();
                    if (nvSkel.getLocationValuation2Weight().getExprPair() == null) {
                        throw new IllegalSemanticsException("Node value weigher "
                                + nvSkel.getMoniker() + ": No expression pairs found.");
                    }
                    for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationValuation2WeightExpressionPair ep : nvSkel
                            .getLocationValuation2Weight().getExprPair()) {
                        exprPairs.add(new Pair<>(ep.getCondition(), ep.getValExpression()));
                    }
                    nvInstance = LocationValuation2Weight.newInstance(nvSkel.getMoniker(),
                            exprPairs);
                }
                else if (nvSkel.getSuperLocationSize2Weight() != null) {
                    Integer coeff = nvSkel.getSuperLocationSize2Weight().getWeightCoefficient();
                    Boolean useDefault = nvSkel.getSuperLocationSize2Weight()
                            .isUsesDefaultCoefficient();
                    String name = nvSkel.getMoniker();

                    if (coeff == null || useDefault == null) {
                        throw new IllegalSemanticsException("Node value weigher "
                                + nvSkel.getMoniker()
                                + ": Must supply coefficient and use-default value");
                    }

                    if (name == null) {
                        name = "(unnamed)";
                    }

                    SuperLocationSize2Weight tmpResult = SuperLocationSize2Weight.newInstance(name,
                            coeff);
                    tmpResult.setUsingSystemDefaultCoefficient(useDefault);
                    nvInstance = tmpResult;
                }
                nv2wMap.put(nvSkel.getId(), nvInstance);
                nv2wList.add(nvInstance);
            }
        }

        result.setCustomNodeValue2Weights(nv2wList);

        Set<Path2Weight> enabledGuidanceMethods = new HashSet<Path2Weight>();
        Set<Path2Weight> allImmutable = new HashSet<Path2Weight>();
        Set<Path2Weight> allCustom = new HashSet<Path2Weight>();

        Map<Integer, Path2Weight> id2GuidanceMethod = new HashMap<>();
        if (rootElement.getGuidanceMethods() != null) {
            for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.GuidanceMethod gm : rootElement
                    .getGuidanceMethods().getGuidanceMethod()) {
                Path2Weight internalGM = SimplePath2Weight.newInstance(
                        gm.getName(), gm.getName());
                if (gm.isEnabled()) {
                    enabledGuidanceMethods.add(internalGM);
                }
                allImmutable.add(internalGM);
                id2GuidanceMethod.put(gm.getId(), internalGM);
            }
        }

        if (rootElement.getCustomGuidanceMethods() != null) {
            for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.CustomGuidanceMethod cgm : rootElement
                    .getCustomGuidanceMethods().getGuidanceMethod()) {
                CustomPath2Weight internalGM = CustomPath2WeightImpl.newInstance(
                        cgm.getName(), cgm.getMoniker(), null);
                if (cgm.isEnabled()) {
                    enabledGuidanceMethods.add(internalGM);
                }

                final String nodeValueWeigherID = cgm.getNodevalueweigher();
                if (nodeValueWeigherID != null && nv2wMap.containsKey(nodeValueWeigherID)) {
                    internalGM.setNodeValue2Weight(nv2wMap.get(nodeValueWeigherID));
                }

                allCustom.add(internalGM);
                id2GuidanceMethod.put(cgm.getId(), internalGM);
            }
        }

        /* Read test type groups */
        Set<UITestTypeGroup> testTypeGroups = new HashSet<>();
        List<utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestTypeGroup> groups = rootElement
                .getTesttypegroups().getGroup();

        if (groups != null) {
            for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestTypeGroup t : groups) {
                testTypeGroups.add(makeTestTypeGroup(t, strategyMap, locationSetMap,
                        id2GuidanceMethod));
            }
        }

        result.setTestTypeGroups(testTypeGroups);

        Set<Path2Weight> allGuidanceMethods = new HashSet<>();
        allGuidanceMethods.addAll(allCustom);
        allGuidanceMethods.addAll(allImmutable);
        result.setPath2Weights(allGuidanceMethods);
        result.setEnabledPath2Weights(enabledGuidanceMethods);
        return result;
    }

    /**
     * Convert a given JAXB test type group representation to a LazyOTF UI test
     * type group.
     * 
     * @param jaxbTTG
     *            The JAXB test type group to be converted.
     * @param strategyMap
     *            The map of strategy ID strings to strategy instances.
     * @param locationSetMap
     *            The map of location set ID strings to location sets.
     * @param guidanceMethodMap
     *            The map of tree trace weigher ID strings to tree trace weighers.
     * @return Its LazyOTF representation.
     * @throws IllegalSemanticsException
     *             The JAXB test type group contains location or test type names
     *             not known to this importer.
     */
    private UITestTypeGroup makeTestTypeGroup(
            utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestTypeGroup jaxbTTG,
            Map<String, UIDecisionStrategy> strategyMap,
            Map<Integer, NamedSet<Location>> locationSetMap,
            Map<Integer, Path2Weight> guidanceMethodMap) throws IllegalSemanticsException {
        UITestTypeGroup result;

        Set<TestType> usedTypes = new HashSet<>();
        if (jaxbTTG.getTestTypes() == null) {
            throw new IllegalSemanticsException(
                    "Contains test Type Group without test type declaration list");
        }
        for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestType tt : jaxbTTG.getTestTypes()
                .getTestType()) {
            if (!testTypeMap.containsKey(tt.toString())) {
                throw new IllegalSemanticsException("Unknown test type " + tt);
            }
            usedTypes.add(testTypeMap.get(tt.toString()));
        }

        result = new UITestTypeGroup(usedTypes);

        if (jaxbTTG.isEnabled() != null) {
            result.setEnabled(jaxbTTG.isEnabled());
        }
        else {
            result.setEnabled(true);
        }

        result.setDisplayName(jaxbTTG.getName());

        /*
         * if(jaxbTTG.isDefaultGroup() != null) {
         * result.setDefaultTestTypeGroup(jaxbTTG.isDefaultGroup()); }
         */

        for (utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationGroup s : jaxbTTG
                .getTestTypeAssocs().getLocationgroup()) {
            if (s.getTestType() == null) {
                throw new IllegalSemanticsException("Unknown test type.");
            }

            final String type = s.getTestType().toString().toUpperCase();
            TestType usedType = testTypeMap.get(type);
            if (usedType == null) {
                throw new IllegalSemanticsException("Unknown test type " + type);
            }

            for (String locName : s.getLocation()) {
                if (!locationMap.containsKey(locName)) {
                    if (isCompletingConfigurations) {
                        addLocation(locName);
                    }
                    else {
                        throw new IllegalSemanticsException("Unknown location " + locName);    
                    }
                }
                final NamedSet<Location> location = locationMap.get(locName);
                result.associate(location, usedType);
            }

            for (Integer locSetID : s.getLocationSetID()) {
                final NamedSet<Location> locationSet = locationSetMap.get(locSetID);
                if (locationSet == null) {
                    throw new IllegalSemanticsException("Unknown location set " + locSetID);
                }
                result.associate(locationSet, usedType);
            }
        }

        /* Set strategy */
        if (jaxbTTG.getStrategyId() != null) {
            result.setStrategy(strategyMap.get(jaxbTTG.getStrategyId()));
        }

        /* Set {@link Path2Weight}s */
        for (int i : jaxbTTG.getGuidanceMethodIDs().getId()) {
            if (!guidanceMethodMap.containsKey(i)) {
                throw new IllegalSemanticsException("Unknown tree trace weigher id " + i);
            }
            result.addPath2Weight(guidanceMethodMap.get(i));
        }

        return result;
    }

    /**
     * Write the given LazyOTF configuration data to an XML file conforming to
     * lazyotfconfig.xsd.
     * 
     * @param filename
     *            The file's name.
     * @param cfg
     *            The LazyOTF configuration to be written.
     * @throws IOException
     *             A system error occurred.
     * @throws ExportException
     *             One of the given groups is incoherent/missing data.
     */
    public void writeConfigToFile(String filename, LazyOTFConfigSessionData cfg)
            throws IOException, ExportException {
        if (filename == null) {
            return;
        }

        File targetFile = new File(filename);
        if (!targetFile.exists()) {
            targetFile.createNewFile();
        }

        try {
            JAXBContext jaxbContext = JAXBContext
                    .newInstance("utwente.fmt.jtorx.testgui.lazyotf.config.jaxb");
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(constructRootElement(cfg), targetFile);
        }
        catch (JAXBException e) {
            throw new ExportException(e.toString());
        }
    }

    /**
     * Get an XML representation of the given LazyOTFConfig instance.
     * 
     * @param cfg
     *            The instance to be represented in XML.
     * @return A string containing an XML representation of cfg.
     * @throws ExportException
     *             The given configuration data was invalid.
     */
    public String getXMLConfiguration(LazyOTFConfigSessionData cfg) throws ExportException {
        try {
            JAXBContext jaxbContext = JAXBContext
                    .newInstance("utwente.fmt.jtorx.testgui.lazyotf.config.jaxb");
            Marshaller marshaller = jaxbContext.createMarshaller();
            StringWriter strw = new StringWriter();

            marshaller.marshal(constructRootElement(cfg), strw);
            return strw.toString();
        }
        catch (JAXBException e) {
            throw new ExportException(e.toString());
        }
    }

    /**
     * Construct a JAXB root element describing the given configuration.
     * 
     * @param cfg
     *            The configuration to be described by the XML structure.
     * @return The requested JAXB root element.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig constructRootElement(
            LazyOTFConfigSessionData cfg) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LazyOTFConfig result;
        result = xmlFactory.createLazyOTFConfig();

        Map<NamedSet<Location>, Integer> locationSetReferenceMap;
        locationSetReferenceMap = new HashMap<>();
        result.setLocationSets(constructLocationSetList(cfg.getLocationSets(),
                locationSetReferenceMap));

        Map<SatStrategy.Condition, String> globalCondReferenceMap;
        globalCondReferenceMap = new HashMap<SatStrategy.Condition, String>();
        result.setSatStrategyGlobalConditions(constructSatStrategyConditionList(
                cfg.getGlobalConditions(), true, globalCondReferenceMap));

        Map<UIDecisionStrategy, String> strategyReferenceMap;
        strategyReferenceMap = new HashMap<>();
        result.setDecisionStrategies(constructDecisionStrategyList(cfg.getStrategies(),
                globalCondReferenceMap, strategyReferenceMap, locationSetReferenceMap));

        result.setAutorecompute(cfg.isAutomaticTestTreeRecomputationEnabled());
        result.setEdgeweightaggregator(cfg.getEdgeWeightAggregatorName().getName());
        result.setMaxdepth(cfg.getMaxRecursionDepth());
        result.setInitialdepth(cfg.getInitialRecursionDepth());
        result.setDepththreshold(cfg.getRecursionDepthModThreshold());
        result.setVisitor(cfg.getVisitorName().getName());
        result.setAutostop(cfg.isAutoStepStopModeEnabled());
        result.setLoggingFile(cfg.getLogFilename());

        Map<NodeValue2Weight, String> nodeValue2WeightRefMap;
        nodeValue2WeightRefMap = new HashMap<>();

        Map<Path2Weight, Integer> guidanceMethodRefMap;
        guidanceMethodRefMap = new HashMap<>();

        if (cfg.getCustomNodeValue2Weights() != null) {
            Set<NodeValue2Weight> noDefaultNv2Weight = new HashSet<>(
                    cfg.getCustomNodeValue2Weights());
            noDefaultNv2Weight.remove(Defaults.defaultNodeValue2Weight());
            result.setNodeValueWeighers(constructNodeValueWeigherListElement(noDefaultNv2Weight,
                    nodeValue2WeightRefMap));
        }

        if (cfg.getPath2Weights() != null) {
            final HashSet<Path2Weight> customGMs = new HashSet<>();
            final HashSet<Path2Weight> immutableGMs = new HashSet<>();
            for (Path2Weight gm : cfg.getPath2Weights()) {
                gm.accept(new Path2WeightVisitor.Adapter() {
                    @Override
                    public void visit(SimplePath2Weight gm) {
                        immutableGMs.add(gm);
                    }

                    @Override
                    public void otherMethodCalled(Path2Weight gm) {
                        customGMs.add(gm);
                    }
                });
            }

            guidanceMethodRefMap.put(null, 0); // reference counter
            result.setCustomGuidanceMethods(constructCustomGMElement(customGMs,
                    cfg.getEnabledPath2Weights(), nodeValue2WeightRefMap, guidanceMethodRefMap));
            result.setGuidanceMethods(constructOrdinaryGMElement(immutableGMs,
                    cfg.getEnabledPath2Weights(), guidanceMethodRefMap));

            result.setTesttypegroups(constructTestTypeGroupsElement(cfg.getTestTypeGroups(),
                    strategyReferenceMap, locationSetReferenceMap, guidanceMethodRefMap));
        }
        return result;
    }

    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationSetList constructLocationSetList(
            Set<NamedSet<Location>> namedLocationSets, Map<NamedSet<Location>, Integer> referenceMap) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationSetList result;
        result = xmlFactory.createLocationSetList();

        int id = 0;
        for (NamedSet<Location> locSet : namedLocationSets) {
            referenceMap.put(locSet, id);
            utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationSet ls;
            ls = xmlFactory.createLocationSet();
            ls.setId(id++);
            ls.setName(locSet.getName());
            for (Location l : locSet) {
                ls.getLocation().add(l.getName());
            }
            result.getLocationSet().add(ls);
        }

        return result;
    }

    /**
     * Construct a JAXB list element for the given set of SatStrategy
     * conditions.
     * 
     * @param conditions
     *            The "source" set of conditions.
     * @param attachGlobalPrefix
     *            Iff true, the conditions are marked "global" by prepending the
     *            prefix "g:" to their IDs.
     * @param referenceMap
     *            Unless null, the Condition->ID associations are stored in this
     *            map.
     * @return A SatStrategyConditionList instance representing the given set of
     *         Condition objects.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SatStrategyConditionList constructSatStrategyConditionList(
            Set<Condition> conditions, boolean attachGlobalPrefix,
            Map<SatStrategy.Condition, String> referenceMap) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SatStrategyConditionList result;
        result = xmlFactory.createSatStrategyConditionList();

        int idCounter = 0;
        for (Condition cond : conditions) {
            utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SatStrategyCondition xmlCond;
            xmlCond = xmlFactory.createSatStrategyCondition();
            xmlCond.setCond(((DumontCondition) cond).getCondition());
            xmlCond.setName(cond.getName());

            String reference = (attachGlobalPrefix ? "g:" : "") + idCounter;
            xmlCond.setId(reference);

            result.getCondition().add(xmlCond);
            if (referenceMap != null) {
                referenceMap.put(cond, reference);
            }

            idCounter++;
        }

        return result;
    }

    /**
     * Construct a JAXB list element representing the given set of decision
     * strategies.
     * 
     * @param strategies
     *            The "source" set of decision strategies.
     * @param globalConditionRefMap
     *            The set of global conditions which may be used by the given
     *            decision strategies.
     * @param referenceMap
     *            Unless null, the DecisionStrategy->ID associations are stored
     *            in this map.
     * @param locationSetRefMap
     *            A reference map for location sets.
     * @return A DecisionStrategyList instance representing the given set of
     *         decision strategies.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.DecisionStrategyList constructDecisionStrategyList(
            List<UIDecisionStrategy> strategies,
            final Map<SatStrategy.Condition, String> globalConditionRefMap,
            Map<UIDecisionStrategy, String> referenceMap,
            final Map<NamedSet<Location>, Integer> locationSetRefMap) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.DecisionStrategyList result;
        result = xmlFactory.createDecisionStrategyList();

        int idCounter = 0;
        for (final UIDecisionStrategy strategy : strategies) {
            final utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.DecisionStrategy xmlStrategy;
            xmlStrategy = xmlFactory.createDecisionStrategy();

            strategy.accept(new UIDecisionStrategyVisitor() {
                @Override
                public void visit(UIDecisionStrategy it) {
                    xmlStrategy
                            .setDirectDecisionStratgegy(constructDirectDecisionStrategy(strategy));
                }

                @Override
                public void visit(UIMappingStrategy it) {
                    xmlStrategy.setMappingStrategy(constructMappingStrategy(
                            (UIMappingStrategy) strategy, locationSetRefMap));
                }

                @Override
                public void visit(UISatStrategy it) {
                    xmlStrategy.setSatStrategy(constructSatStrategy((UISatStrategy) strategy,
                            globalConditionRefMap, locationSetRefMap));
                }
            });

            xmlStrategy.setMoniker(strategy.getDisplayName());

            String reference = "" + idCounter;
            xmlStrategy.setId(reference);
            referenceMap.put(strategy, reference);
            idCounter++;

            result.getStrategy().add(xmlStrategy);
        }

        return result;
    }

    /**
     * Construct a JAXB mapping strategy element representing the given mapping
     * decision strategy.
     * 
     * @param strategy
     *            The "source" MappingStrategy instance.
     * @param locationSetRefMap
     *            A reference map for location sets.
     * @return A JAXB MappingStrategy object representing the given mapping
     *         strategy.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.MappingStrategy constructMappingStrategy(
            UIMappingStrategy strategy, Map<NamedSet<Location>, Integer> locationSetRefMap) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.MappingStrategy result;
        result = xmlFactory.createMappingStrategy();

        for (NamedSet<Location> loc : strategy.getLocationSets()) {
            utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationTypeMap mapItem;
            mapItem = xmlFactory.createLocationTypeMap();
            mapItem.setLocation(loc.getDisplayName());
            if (locationSetRefMap.containsKey(loc)) {
                mapItem.setLocationSetId(locationSetRefMap.get(loc));
            }
            mapItem.setTestType(utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestType
                    .valueOf(strategy.getTestType(loc).getInternalName()));
            result.getMap().add(mapItem);
        }

        return result;
    }

    /**
     * Construct a JAXB SatStrategy element representing the given SatStrategy
     * instance.
     * 
     * @param strategy
     *            The "source" SatStrategy instance.
     * @param globalConditionRefMap
     *            A map containing the Condition->ID association for all
     *            "global" SatStrategy conditions.
     * @param locationSetRefMap
     *            A reference map for location sets.
     * @return A JAXB SatStrategy element representing the given SatStrategy
     *         instance.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SatStrategy constructSatStrategy(
            UISatStrategy strategy, Map<SatStrategy.Condition, String> globalConditionRefMap,
            Map<NamedSet<Location>, Integer> locationSetRefMap) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SatStrategy result;
        result = xmlFactory.createSatStrategy();

        Map<SatStrategy.Condition, String> localConditionRefMap = new HashMap<SatStrategy.Condition, String>();

        Set<SatStrategy.Condition> localConditions = new HashSet<SatStrategy.Condition>();
        for (NamedSet<Location> location : strategy.getLocationSets()) {
            for (TestType type : strategy.getAssociatedTestTypes(location)) {
                SatStrategy.Condition cond = strategy.getCondition(location, type);
                if (!globalConditionRefMap.containsKey(cond)) {
                    localConditions.add(cond);
                }
            }
        }

        result.setConditions(constructSatStrategyConditionList(localConditions, false,
                localConditionRefMap));

        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SatStrategyAssociationList assocList;
        assocList = xmlFactory.createSatStrategyAssociationList();

        for (NamedSet<Location> location : strategy.getLocationSets()) {
            for (TestType type : strategy.getAssociatedTestTypes(location)) {
                utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationConditionAssociation xmlAssoc;
                xmlAssoc = xmlFactory.createLocationConditionAssociation();

                Condition assocCond = strategy.getCondition(location, type);
                if (globalConditionRefMap.containsKey(assocCond)) {
                    xmlAssoc.setCondition(globalConditionRefMap.get(assocCond));
                }
                else if (localConditionRefMap.containsKey(assocCond)) {
                    xmlAssoc.setCondition(localConditionRefMap.get(assocCond));
                }

                xmlAssoc.setLocation(location.getDisplayName());
                if (locationSetRefMap.containsKey(location)) {
                    xmlAssoc.setLocationSetID(locationSetRefMap.get(location));
                }

                xmlAssoc.setTestType(utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestType
                        .valueOf(type.getInternalName()));

                assocList.getAssoc().add(xmlAssoc);
            }
        }

        result.setAssociations(assocList);

        return result;
    }

    /**
     * Construct a JAXB DirectDecisionStrategy element representing the given
     * DecisionStrategy instance. This way, no more information than strategy
     * name and moniker are saved.
     * 
     * @param strategy
     *            The "source" DecisionStrategy instance.
     * @return A corresponding DirectDecisionStrategy element.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.DirectDecisionStrategy constructDirectDecisionStrategy(
            UIDecisionStrategy strategy) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.DirectDecisionStrategy result;
        result = xmlFactory.createDirectDecisionStrategy();

        result.setName(strategy.getDisplayName());

        return result;
    }

    /**
     * Construct a JAXB element representing the given {@link Path2Weight}s.
     * 
     * @param methods
     *            The set of {@link Path2Weight}s to be contained in the result.
     * @param allEnabledMethods
     *            The set of enabled {@link Path2Weight}s.
     * @param guidanceMethodRefMap
     *            A reference map into which new {@link Path2Weight}->ID associations
     *            are stored.
     * @return The JAXB representation of methods.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.OrdinaryGuidanceMethods constructOrdinaryGMElement(
            Set<Path2Weight> methods,
            Set<Path2Weight> allEnabledMethods,
            Map<Path2Weight, Integer> guidanceMethodRefMap) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.OrdinaryGuidanceMethods result;
        result = xmlFactory.createOrdinaryGuidanceMethods();

        if (methods == null) {
            return result;
        }

        int refCtr = guidanceMethodRefMap.get(null);

        for (Path2Weight gm : methods) {
            utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.GuidanceMethod x;
            x = xmlFactory.createGuidanceMethod();
            x.setEnabled(allEnabledMethods.contains(gm));
            x.setName(gm.getName());
            x.setId(refCtr);
            guidanceMethodRefMap.put(gm, refCtr);
            refCtr++;
            result.getGuidanceMethod().add(x);
        }

        guidanceMethodRefMap.put(null, refCtr);

        return result;
    }

    /**
     * Construct a JAXB element representing the given custom {@link Path2Weight}s.
     * 
     * @param methods
     *            The set of custom {@link Path2Weight}s to be contained in the
     *            result.
     * @param allEnabledMethods
     *            The set of enabled {@link Path2Weight}s.
     * @param nodeValueWeigherRefMap
     *            The map of node value weighers to their respective IDs.
     * @param guidanceMethodRefMap
     *            The map into which tree trace weigher IDs are stored.
     * @return The JAXB representation of methods.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.CustomGuidanceMethods constructCustomGMElement(
            Set<Path2Weight> methods,
            final Set<Path2Weight> allEnabledMethods,
            final Map<NodeValue2Weight, String> nodeValueWeigherRefMap,
            final Map<Path2Weight, Integer> guidanceMethodRefMap) {
        final utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.CustomGuidanceMethods result;
        result = xmlFactory.createCustomGuidanceMethods();

        if (methods == null) {
            return result;
        }

        for (Path2Weight gm : methods) {
            gm.accept(new Path2WeightVisitor.Adapter() {
                @Override
                public void visit(CustomPath2Weight gm) {
                    int refCtr = guidanceMethodRefMap.get(null);
                    CustomPath2Weight cgm = gm;
                    utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.CustomGuidanceMethod x;
                    x = xmlFactory.createCustomGuidanceMethod();
                    x.setEnabled(allEnabledMethods.contains(gm));
                    x.setName(cgm.getName());
                    x.setMoniker(cgm.getDisplayName());
                    if (cgm.getNodeValue2Weight() != null) {
                        String nvRef = nodeValueWeigherRefMap.get(cgm.getNodeValue2Weight());
                        if (nvRef != null) {
                            x.setNodevalueweigher(nvRef);
                        }
                    }
                    x.setId(refCtr);
                    guidanceMethodRefMap.put(gm, refCtr);
                    refCtr++;
                    guidanceMethodRefMap.put(null, refCtr);
                    result.getGuidanceMethod().add(x);
                }
            });
        }

        return result;
    }

    /**
     * Construct a JAXB element containing the given test type groups.
     * 
     * @param groups
     *            The groups to be included in the root element.
     * @param decisionStrategyRefs
     *            The decision strategy reference ID map.
     * @param locationSetRefs
     *            The location set reference ID map.
     * @param guidanceMethodRefs
     *            The tree trace weigher reference ID map.
     * @return The requested JAXB root element.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestTypeGroups constructTestTypeGroupsElement(
            Set<UITestTypeGroup> groups, Map<UIDecisionStrategy, String> decisionStrategyRefs,
            Map<NamedSet<Location>, Integer> locationSetRefs,
            Map<Path2Weight, Integer> guidanceMethodRefs) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestTypeGroups result;
        result = xmlFactory.createTestTypeGroups();

        if (groups == null) {
            return result;
        }

        for (UITestTypeGroup g : groups) {
            result.getGroup().add(
                    constructTestTypeGroupElement(g, decisionStrategyRefs, locationSetRefs,
                            guidanceMethodRefs));
        }

        return result;
    }

    /**
     * Construct a JAXB test type group element representing the given test type
     * group.
     * 
     * @param group
     *            The group to be represented as a JAXB
     * @param decisionStrategyRefs
     *            The decision strategy reference ID map.
     * @param locationSetRefs
     *            The location set reference ID map.
     * @param guidanceMethodRefs
     *            The tree trace weigher reference ID map.
     * @param decisionStrategyRefs
     * @return The requested JAXB test type group element.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestTypeGroup constructTestTypeGroupElement(
            UITestTypeGroup group, Map<UIDecisionStrategy, String> decisionStrategyRefs,
            Map<NamedSet<Location>, Integer> locationSetRefs,
            Map<Path2Weight, Integer> guidanceMethodRefs) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestTypeGroup result;
        result = xmlFactory.createTestTypeGroup();

        result.setName(group.getDisplayName());
        result.setStrategyId(decisionStrategyRefs.get(group.getStrategy()));
        result.setEnabled(group.isEnabled());

        result.setTestTypes(xmlFactory.createTestTypeGroupTestTypes());
        result.setTestTypeAssocs(xmlFactory.createTestTypeGroupTestTypeAssocs());
        for (TestType tt : group.getTestTypes()) {
            result.getTestTypes()
                    .getTestType()
                    .add(utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestType.valueOf(tt
                            .getInternalName()));
            result.getTestTypeAssocs().getLocationgroup()
                    .add(constructLocationGroup(group.getLocations(tt), tt, locationSetRefs));
        }
        result.setGuidanceMethodIDs(xmlFactory.createGuidanceMethodIDs());
        for (Path2Weight g : group.getPath2Weights()) {
            assert guidanceMethodRefs.containsKey(g);
            result.getGuidanceMethodIDs().getId().add(guidanceMethodRefs.get(g));
        }

        return result;
    }

    /**
     * Construct a JAXB location set element representing the given test type
     * and its locations.
     * 
     * @param locs
     *            The locations to be included in the set.
     * @param locationSetIDs
     *            The map into which location set IDs get stored.
     * @param type
     *            The set's test type.
     * @return The requested JAXB location set element.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationGroup constructLocationGroup(
            Set<NamedSet<Location>> locs, TestType type,
            Map<NamedSet<Location>, Integer> locationSetIDs) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationGroup result;
        result = xmlFactory.createLocationGroup();

        String xmlTestTypeName = type.getInternalName();

        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestType xmlType;
        xmlType = utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.TestType.valueOf(xmlTestTypeName);

        result.setTestType(xmlType);

        for (NamedSet<Location> l : locs) {
            if (l.getName() == null) {
                // Unnamed single location set
                if (!l.isEmpty()) {
                    result.getLocation().add(l.getDisplayName());
                }
            }
            else {
                // Named location set
                result.getLocationSetID().add(locationSetIDs.get(l));
            }
        }

        return result;
    }

    /**
     * Construct a JAXB node value weigher list element containing the given
     * node value weighers.
     * 
     * @param weighers
     *            The list of {@link NodeValueWeight} instances to be stored
     *            within the list element.
     * @param refMap
     *            A mapping from IDs to {@link NodeValueWeight} instances where
     *            automatically assigned node value weigher ids are stored for
     *            further reference within the same XML document.
     * @return The constructed JAXB node value weigher list element.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.NodeValueWeigherList constructNodeValueWeigherListElement(
            Collection<NodeValue2Weight> weighers, Map<NodeValue2Weight, String> refMap) {
        final utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.NodeValueWeigherList result;
        result = xmlFactory.createNodeValueWeigherList();

        int idCounter = 0;
        for (NodeValue2Weight item : weighers) {
            refMap.put(item, "" + idCounter);

            final utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.NodeValueWeigher itemAsXml;
            itemAsXml = xmlFactory.createNodeValueWeigher();

            itemAsXml.setMoniker(item.getDisplayName());
            itemAsXml.setImplName(item.getName());
            itemAsXml.setId("" + idCounter);

            item.accept(new NodeValue2WeightVisitor.Adapter() {
                @Override
                public void visit(Locations2Weight it) {
                    itemAsXml.setLocations2Weight(constructLocations2WeightElement(it));
                }

                @Override
                public void visit(LocationValuation2Weight it) {
                    itemAsXml
                            .setLocationValuation2Weight(constructLocationValuation2WeightElement(it));
                }

                @Override
                public void visit(SuperLocationSize2Weight it) {
                    itemAsXml
                            .setSuperLocationSize2Weight(constructSuperLocationSize2WeightElement(it));
                }
            });

            result.getNodeValueWeigher().add(itemAsXml);
            idCounter++;
        }

        return result;
    }

    /**
     * Construct a JAXB Locations2Weight element representing the given LazyOTF
     * {@link Locations2Weight} instance.
     * 
     * @param it
     *            The {@link Locations2Weight} instance to be represented.
     * @return The corresponding JAXB Locations2Weight element.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.Locations2Weight constructLocations2WeightElement(
            Locations2Weight it) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.Locations2Weight result;
        result = xmlFactory.createLocations2Weight();
        result.setInducingWeight(it.getInducingWeight());
        result.setOrdinaryWeight(it.getOrdinaryWeight());
        result.setTestGoalWeight(it.getTestGoalWeight());
        return result;
    }

    /**
     * Construct a JAXB SuperLocationSize2Weight element representing the given
     * LazyOTF {@link SuperLocationSize2Weight} instance.
     * 
     * @param it
     *            The {@link SuperLocationSize2Weight} instance to be
     *            represented.
     * @return The corresponding JAXB SuperLocationSize2Weight element.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SuperLocationSize2Weight constructSuperLocationSize2WeightElement(
            SuperLocationSize2Weight it) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.SuperLocationSize2Weight result;
        result = xmlFactory.createSuperLocationSize2Weight();
        result.setUsesDefaultCoefficient(it.isUsingSystemDefaultCoefficient());
        result.setWeightCoefficient(it.getCoefficient());
        return result;
    }

    /**
     * Construct a JAXB LocationValuation2Weight element representing the given
     * LazyOTF {@link Locations2Weight} instance.
     * 
     * @param it
     *            The {@link LocationValuation2Weight} instance to be
     *            represented.
     * @return The corresponding JAXB LocationValuation2Weight element.
     */
    private utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationValuation2Weight constructLocationValuation2WeightElement(
            LocationValuation2Weight it) {
        utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationValuation2Weight result;
        result = xmlFactory.createLocationValuation2Weight();

        for (Pair<String, String> expr : it.getExpressions()) {
            utwente.fmt.jtorx.testgui.lazyotf.config.jaxb.LocationValuation2WeightExpressionPair exprPair;
            exprPair = xmlFactory.createLocationValuation2WeightExpressionPair();
            exprPair.setCondition(expr.getLeft());
            exprPair.setValExpression(expr.getRight());
            result.getExprPair().add(exprPair);
        }

        return result;
    }

    /**
     * If true, unknown LazyOTF entities are silently added to the configuration when
     * loading a configuration file if <tt>autoComplete</tt> is <tt>true</tt>.
     * @param autoComplete
     */
    public void setAutoCompleteConfigurationOptions(boolean autoComplete) {
        isCompletingConfigurations = autoComplete;
    }
    
    /**
     * Adds a new location to the set of known locations.
     * @param name The new location's name.
     */
    private void addLocation(String name) {
        Location l = new Location(name);
        NamedSet<Location> namedL = NamedSet.makeNamedSet(null, l);
        knownLocations.add(namedL);
        locationMap.put(name, namedL);
    }
    
    @SuppressWarnings("serial")
    public class ParseException extends Exception {
    }

    @SuppressWarnings("serial")
    public class IllegalSemanticsException extends Exception {
        public IllegalSemanticsException(String msg) {
            super(msg);
        }
    }

    @SuppressWarnings("serial")
    public class ExportException extends Exception {
        public ExportException(String msg) {
            super(msg);
        }
    }
    
    private class NonGuiLazyOTFConfigSessionData extends LazyOTFConfigSessionData {
        private TestTree2Weight testTree2Weight;
        private TraversalMethod visitor;
        
        public NonGuiLazyOTFConfigSessionData() {
            setConfigOptions(new LazyOTFConfigurationOptions());
        }
        
        @Override
        public void setEdgeWeightAggregatorName(TestTree2Weight it) {
            testTree2Weight = it;
        }
        
        @Override
        public void setVisitorName(TraversalMethod it) {
            visitor = it;
        }
        
        @Override
        public TestTree2Weight getEdgeWeightAggregatorName() {
            return testTree2Weight;
        }
        
        @Override
        public TraversalMethod getVisitorName() {
            return visitor;
        }
    }
}
