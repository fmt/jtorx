package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.ArrayList;
import java.util.List;

import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.policy.Defaults;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * A controller for the {@link Path2Weight} editor, usable with
 * {@link GenericPath2WeightEditorView} view instances.
 */
class CustomPath2WeightEditorCtrl extends EditorController {
    /** The edited custom {@link Path2Weight}. */
    private final CustomPath2Weight subject;

    /** The editor's view part. */
    private final GenericPath2WeightEditorView view;

    /** The main controller of the Weights Editor window. */
    private final WeightsEditorImpl mainCtrl;

    /**
     * Construct a new {@link CustomPath2WeightEditorCtrl} instance.
     * 
     * @param subject
     *            The edited {@link Path2Weight} instance.
     * @param view
     *            The editor's view.
     * @param mainCtrl
     *            The Weights Editor's window.
     * @param readOnly
     *            true iff <tt>subject</tt> may not be altered.
     */
    public CustomPath2WeightEditorCtrl(CustomPath2Weight subject,
            GenericPath2WeightEditorView view, WeightsEditorImpl mainCtrl, boolean readOnly) {
        this.subject = subject;
        this.view = view;
        this.mainCtrl = mainCtrl;

        view.setNodeValueWeighers(new ArrayList<>(mainCtrl.getOrderedModifiedNodeValue2Weights()));
        List<String> orderedImplementationTypeNames = new ArrayList<>();
        for (Path2Weight gm : mainCtrl.getModifiedPath2WeightSet()) {
            if (!orderedImplementationTypeNames.contains(gm.getName())) {
                orderedImplementationTypeNames.add(gm.getName());
            }
        }
        view.setImplementationNames(orderedImplementationTypeNames);

        view.setDisplayName(subject.getDisplayName());
        view.setImplementationName(subject.getName());

        if (subject.getNodeValue2Weight() != null) {
            view.setNodeWeigher(subject.getNodeValue2Weight());
        }
        else {
            view.setNodeWeigher(Defaults.defaultNodeValue2Weight());
        }

        view.setDisplayNameEnabled(true);
        view.setImplementationComboEnabled(false);
        view.setNodeValueWeigherComboEnabled(true);

        setupListeners();
    }

    /**
     * Set up listeners observing the view.
     */
    private void setupListeners() {
        view.addDisplayNameChangeListener(mainCtrl.makeDefaultRenameListener(subject));
        view.addNodeValueWeigherChangedListener(new GenericListener<NodeValue2Weight>() {
            @Override
            public void onFiredEvent(NodeValue2Weight it) {
                subject.setNodeValue2Weight(it);
            }
        });
    }

    @Override
    public void update() {
    }
}
