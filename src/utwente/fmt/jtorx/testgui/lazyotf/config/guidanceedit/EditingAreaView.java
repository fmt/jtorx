package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

/**
 * A view class managing the composition of the window's editing area. This
 * class behaves like an SWT Composite.
 */
class EditingAreaView extends Composite {
    /** The client area, containing the actual editor. */
    private final Composite childArea;

    /** The text at the bottom of the editing area. */
    private final Label statusBar;

    /**
     * Construct a new {@link EditingAreaView} instance within the
     * {@link Composite} instance <tt>parent</tt>.
     * 
     * @param parent
     *            A {@link Composite} instance serving as a parent for this
     *            view.
     */
    public EditingAreaView(Composite parent) {
        super(parent, SWT.NONE);

        GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        setLayout(layout);

        childArea = new Composite(this, SWT.NONE);
        childArea.setLayoutData(new GridData(GridData.FILL_BOTH));
        childArea.setLayout(new FillLayout());
        statusBar = new Label(this, SWT.NONE);
        statusBar.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        statusBar.setText("Click on an item to edit it.");
    }

    /**
     * Set the status bar text.
     * 
     * @param text
     *            The new status bar text.
     */
    public void setStatusText(String text) {
        statusBar.setText(text);
    }

    /**
     * Clear the editing area by disposing it.
     */
    public void disposeCurrentEditor() {
        Control[] editorCtrls = childArea.getChildren();
        for (Control c : editorCtrls) {
            if (!c.isDisposed()) {
                c.dispose();
            }
        }
    }

    /**
     * @return The current editing area composite.
     */
    public Composite getEditingArea() {
        return childArea;
    }

    @Override
    public void layout() {
        super.layout();
        childArea.layout();
    }

    /**
     * Display an empty editor composite.
     * 
     * @param displayDefaultStatusText
     *            If true, a default status text is displayed, too. Otherwise,
     *            no status text is displayed.
     */
    public void setDefaultEditor(boolean displayDefaultStatusText) {
        disposeCurrentEditor();
        setStatusText(displayDefaultStatusText ? "Not editing anything." : "");
    }

    @Override
    public void setEnabled(boolean enabledness) {
        super.setEnabled(enabledness);
        Control[] editorCtrls = childArea.getChildren();
        for (Control c : editorCtrls) {
            if (!c.isDisposed()) {
                c.setEnabled(enabledness);
            }
        }
    }
}
