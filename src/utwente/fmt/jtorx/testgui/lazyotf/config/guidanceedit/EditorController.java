package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

/**
 * Superclass for editor area controllers.
 */
abstract class EditorController {
    /** The "null" editor controller. */
    private static final EditorController NULLEDITOR = new NullEditorController();

    /**
     * Update the controller and its attached view to reflect changes within the
     * edited item.
     */
    public abstract void update();

    /**
     * @return The "null" editor controller which is not attached to a view and
     *         does not modify any edited item.
     */
    public static EditorController getNullElement() {
        return EditorController.NULLEDITOR;
    }

    /** The "null" editor, ignoring updates. */
    private static class NullEditorController extends EditorController {
        @Override
        public void update() {

        }
    }
}
