package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

import com.eekboom.utils.Strings;

/**
 * A generic view for {@link Path2Weight} editors, allowing changes to the method's
 * display name, implementation name and its associated node value weigher (if
 * any).
 */
class GenericPath2WeightEditorView extends Composite {
    /** The display name text control. */
    private final Text displayNameCtrl;

    /** The implementation name combo control. */
    private final Combo implementationNameCtrl;

    /** The node value weigher combo control. */
    private final Combo nodeValueWeigherCtrl;

    /**
     * A map from implementation names to their respective index within the
     * implementation name combo control.
     */
    private final Map<String, Integer> implementations2Indices;

    /**
     * A map from node value weighers to their respective index within the node
     * value weigher combo control.
     */
    private final Map<NodeValue2Weight, Integer> nodeWeighers2Indices;

    /**
     * The list of node value weighers offered in the node value weigher combo
     * control, ordered by their respective appearance within that control.
     */
    private List<NodeValue2Weight> nodeValueWeighers;

    /** The set of listeners called for display name change requests. */
    private final Set<GenericListener<String>> onDisplayNameChangedListeners;

    /** The set of listeners called for node value weigher change requests. */
    private final Set<GenericListener<NodeValue2Weight>> onNodeValueWeigherChangedListeners;

    /** The set of listeners called for implementation type change requests. */
    private final Set<GenericListener<String>> onImplementationTypeChangedListeners;

    /**
     * Construct a new {@link GenericPath2WeightEditorView} instance.
     * 
     * @param parent
     *            The view's parent control.
     */
    public GenericPath2WeightEditorView(Composite parent) {
        super(parent, SWT.NONE);

        setLayout(new FillLayout());
        onDisplayNameChangedListeners = new HashSet<>();
        onNodeValueWeigherChangedListeners = new HashSet<>();
        onImplementationTypeChangedListeners = new HashSet<>();

        Group basicSettingsGrp = new Group(this, SWT.NONE);
        basicSettingsGrp.setText("Basic Settings");

        GridLayout bsgLayout = new GridLayout(); // basic setting group's layout
        bsgLayout.numColumns = 2;
        basicSettingsGrp.setLayout(bsgLayout);
        GridData horzFill = new GridData(GridData.FILL_HORIZONTAL);
        horzFill.grabExcessHorizontalSpace = true;

        Label displayNameLabel = new Label(basicSettingsGrp, SWT.NONE);
        displayNameLabel.setText("Name:");

        displayNameCtrl = new Text(basicSettingsGrp, SWT.NONE);
        displayNameCtrl.setLayoutData(horzFill);

        Label implementationNameLabel = new Label(basicSettingsGrp, SWT.NONE);
        implementationNameLabel.setText("Implementation:");

        implementationNameCtrl = new Combo(basicSettingsGrp, SWT.READ_ONLY);
        implementationNameCtrl.setLayoutData(horzFill);

        Label nodeValueWeigherLabel = new Label(basicSettingsGrp, SWT.NONE);
        nodeValueWeigherLabel.setText("NodeValue2Weight:");

        nodeValueWeigherCtrl = new Combo(basicSettingsGrp, SWT.READ_ONLY);
        nodeValueWeigherCtrl.setLayoutData(horzFill);

        implementations2Indices = new HashMap<>();
        nodeWeighers2Indices = new HashMap<>();

        nodeValueWeighers = new ArrayList<>();

        setupListeners();

        parent.layout();
        this.layout();
        basicSettingsGrp.layout();
    }

    /**
     * Tie the sets of listeners to the editing controls.
     */
    private void setupListeners() {
        displayNameCtrl.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent arg0) {
            }

            @Override
            public void focusLost(FocusEvent arg0) {
                String newName = displayNameCtrl.getText();
                for (GenericListener<String> listener : onDisplayNameChangedListeners) {
                    listener.onFiredEvent(newName);
                }
            }
        });

        displayNameCtrl.addTraverseListener(new TraverseListener() {
            @Override
            public void keyTraversed(TraverseEvent arg0) {
                if (arg0.detail == SWT.TRAVERSE_RETURN) {
                    String newName = displayNameCtrl.getText();
                    for (GenericListener<String> listener : onDisplayNameChangedListeners) {
                        listener.onFiredEvent(newName);
                    }
                }
            }
        });

        implementationNameCtrl.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent arg0) {
                String newName = implementationNameCtrl.getText();
                for (GenericListener<String> listener : onImplementationTypeChangedListeners) {
                    listener.onFiredEvent(newName);
                }
            }
        });

        nodeValueWeigherCtrl.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                NodeValue2Weight selection = nodeValueWeighers.get(nodeValueWeigherCtrl
                        .getSelectionIndex());
                for (GenericListener<NodeValue2Weight> listener : onNodeValueWeigherChangedListeners) {
                    listener.onFiredEvent(selection);
                }
            }
        });
    }

    /**
     * Install a listener such that it is called whenever the edited guidance
     * method's display name is requested to be changed.
     * 
     * @param listener
     *            A {@link Path2Weight} instance taking the requested
     *            new name as an argument to <tt>onFiredEvent()</tt>.
     */
    public void addDisplayNameChangeListener(GenericListener<String> listener) {
        onDisplayNameChangedListeners.add(listener);
    }

    /**
     * Install a listener such that it is called whenever the edited guidance
     * method's node value weigher is requested to be changed.
     * 
     * @param listener
     *            A {@link Path2Weight} instance taking the requested
     *            new node value weigher as an argument to
     *            <tt>onFiredEvent()</tt>.
     */
    public void addNodeValueWeigherChangedListener(GenericListener<NodeValue2Weight> listener) {
        onNodeValueWeigherChangedListeners.add(listener);
    }

    /**
     * Install a listener such that it is called whenever the edited guidance
     * method's implementation type is requested to be changed.
     * 
     * @param listener
     *            A {@link Path2Weight} instance taking the requested
     *            new implementation type name as an argument to
     *            <tt>onFiredEvent()</tt>.
     */
    public void addImplementationTypeChangedListener(GenericListener<String> listener) {
        onImplementationTypeChangedListeners.add(listener);
    }

    /**
     * Set the list of node value weighers which can be used with the given
     * {@link Path2Weight}.
     * 
     * @param weighers
     *            A {@link List} of {@link NodeValue2Weight} instances.
     */
    public void setNodeValueWeighers(List<NodeValue2Weight> weighers) {
        nodeValueWeighers = weighers;
        for (NodeValue2Weight nv : weighers) {
            int currentIdx = nodeValueWeigherCtrl.getItemCount();
            nodeWeighers2Indices.put(nv, currentIdx);
            nodeValueWeigherCtrl.add(nv.getDisplayName());
        }
    }

    /**
     * Set the list of names of the implementation types which can be used with
     * the given {@link Path2Weight}.
     * 
     * @param names
     *            A {@link List} of {@link String} instances.
     */
    public void setImplementationNames(List<String> names) {
        setComboItems(implementationNameCtrl, names, implementations2Indices);
    }

    /**
     * Set up the contents of the given {@link Combo} instance.
     * 
     * @param ctrl
     *            A {@link Combo} instance.
     * @param items
     *            A {@link List} of {@link Strings} containing the (ordered) new
     *            items of the {@link Combo} instance.
     * @param indexMap
     *            A {@link Map} instance. For each string <i>s</i> in
     *            <tt>items</tt>, the index of <i>s</i> within the combo control
     *            gets stored in this map.
     */
    private static void setComboItems(Combo ctrl, List<String> items, Map<String, Integer> indexMap) {
        ctrl.removeAll();
        indexMap.clear();
        Iterator<String> itemsIterator = items.iterator();
        for (int i = 0; i < items.size(); i++) {
            String item = itemsIterator.next();
            ctrl.add(item);
            indexMap.put(item, i);
        }
    }

    /**
     * Set the display name text control content..
     * 
     * @param name
     *            The new display name to be shown.
     */
    public void setDisplayName(String name) {
        displayNameCtrl.setText(name);
    }

    /**
     * Set the implementation type control selection.
     * 
     * @param name
     *            The implementation type to be selected.
     * @throws {@link IllegalArgumentException} when the implementation type
     *         specified by <tt>name</tt> is unknown.
     */
    public void setImplementationName(String name) {
        Integer idx = implementations2Indices.get(name);
        if (idx != null) {
            implementationNameCtrl.select(idx);
        }
        else {
            throw new IllegalArgumentException("Unknown implementation type " + name);
        }
    }

    /**
     * Set the node value weigher control selection.
     * 
     * @param weigher
     *            The node value weigher to be selected.
     * @throws {@link IllegalArgumentException} when the NodeValue2Weight
     *         specified by <tt>name</tt> is unknown.
     */
    public void setNodeWeigher(NodeValue2Weight weigher) {
        Integer idx = nodeWeighers2Indices.get(weigher);
        if (idx != null) {
            nodeValueWeigherCtrl.select(idx);
        }
        else {
            throw new IllegalArgumentException("Unknown NodeValue2Weight "
                    + weigher.getDisplayName());
        }
    }

    /**
     * Set the gray-out state of the display name text control.
     * 
     * @param enabledness
     *            true iff editing is to be enabled.
     */
    public void setDisplayNameEnabled(boolean enabledness) {
        displayNameCtrl.setEnabled(enabledness);
    }

    /**
     * Set the gray-out state of the implementation type combo control.
     * 
     * @param enabledness
     *            true iff editing is to be enabled.
     */
    public void setImplementationComboEnabled(boolean enabledness) {
        implementationNameCtrl.setEnabled(enabledness);
    }

    /**
     * Set the gray-out state of the node value weigher combo control.
     * 
     * @param enabledness
     *            true iff editing is to be enabled.
     */
    public void setNodeValueWeigherComboEnabled(boolean enabledness) {
        nodeValueWeigherCtrl.setEnabled(enabledness);
    }
}
