package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import utwente.fmt.jtorx.lazyotf.ExpressionValidator.ValidationException;
import utwente.fmt.jtorx.lazyotf.guidance.LocationValuation2Weight;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;
import utwente.fmt.jtorx.utils.Pair;

/**
 * A controller for {@link LocationValuation2Weight} view instances.
 */
class LocationValuation2WeightEditorCtrl extends EditorController {
    /** The view instance governed by this controller. */
    private final LocationValuation2WeightEditorView<Pair<String, String>> view;

    /** The main controller. */
    private final WeightsEditorImpl mainCtrl;

    /** The edited {@link LocationValuation2Weight} instance. */
    private final LocationValuation2Weight subject;

    /**
     * Construct a new {@link LocationValuation2WeightEditorCtrl} instance.
     * 
     * @param subject
     *            The edited {@link LocationValuation2Weight} instance.
     * @param view
     *            The view displaying <tt>subject</tt>.
     * @param mainCtrl
     *            The main controller.
     * @param isReadOnly
     *            Iff true, <tt>subject</tt> will not be modified by this
     *            controller.
     */
    public LocationValuation2WeightEditorCtrl(LocationValuation2Weight subject,
            LocationValuation2WeightEditorView<Pair<String, String>> view,
            WeightsEditorImpl mainCtrl, boolean isReadOnly) {
        this.view = view;
        this.mainCtrl = mainCtrl;
        this.subject = subject;
        view.setDisplayName(subject.getDisplayName());

        if (isReadOnly) {
            view.setEnabled(false);
        }

        for (Pair<String, String> p : subject.getExpressions()) {
            view.addConditionPair(p);
            checkConditionPairValidity(p);
        }

        setupListeners();
    }

    /**
     * Check a condition/value-expression pair's validity and update the view
     * accordingly.
     * 
     * @param it
     *            The condition/value-expression pair getting checked.
     */
    private void checkConditionPairValidity(Pair<String, String> it) {
        boolean conditionValid = true;
        try {
            mainCtrl.getExpressionValidator().checkCondition(it.getLeft());
        }
        catch (ValidationException e) {
            conditionValid = false;
        }

        boolean updateValid = true;
        try {
            mainCtrl.getExpressionValidator().checkUpdateExpression(it.getRight());
        }
        catch (ValidationException e) {
            updateValid = false;
        }

        String errorMsg = (!conditionValid || !updateValid) ? "Errors in: " : "";
        errorMsg += (!conditionValid) ? "condition" : "";
        if (!conditionValid && !updateValid) {
            errorMsg += ", ";
        }
        errorMsg += (!updateValid) ? "value expression" : "";

        view.setValid(it, conditionValid, 0, errorMsg);
        view.setValid(it, updateValid, 1, errorMsg);
    }

    /**
     * Set up the listeners observing the view and modifying <tt>subject</tt>
     * when appropriate.
     */
    private void setupListeners() {
        view.addNameChangedListener(mainCtrl.makeDefaultRenameListener(subject));

        view.addExpressionChangedListener(new GenericListener<LocationValuation2WeightEditorView<Pair<String, String>>.ExpressionPairChangedEvent>() {
            @Override
            public void onFiredEvent(
                    LocationValuation2WeightEditorView<Pair<String, String>>.ExpressionPairChangedEvent event) {
                Pair<String, String> expressionPair = event.expressionPair;
                if (expressionPair == null) {
                    throw new IllegalStateException("expressionPair must not be null");
                }

                subject.getExpressions().remove(expressionPair);

                Pair<String, String> newPair = new Pair<String, String>(event.newCondition,
                        event.newValueExpression);

                subject.getExpressions().add(newPair);
                view.replace(expressionPair, newPair);

                checkConditionPairValidity(newPair);
            }

        });

        view.addExpressionDeletedListener(new GenericListener<Pair<String, String>>() {

            @Override
            public void onFiredEvent(Pair<String, String> arg) {
                subject.getExpressions().remove(arg);
                if (view.contains(arg)) {
                    System.out.println("Deleting from view.");
                    view.remove(arg);
                }
            }
        });

        view.addExpressionCreatedListener(new Runnable() {
            @Override
            public void run() {
                Pair<String, String> newExpression = new Pair<>("true==true", "10 * 100");
                subject.getExpressions().add(newExpression);
                view.addConditionPair(newExpression);
            }
        });
    }

    @Override
    public void update() {
        view.setDisplayName(subject.getDisplayName());
    }
}
