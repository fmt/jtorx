package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * A view for displaying {@link LocationValuation2Weight} instances, behaving
 * like an SWT {@link Composite} instance.
 * 
 * @param <T>
 *            The condition/valuation-expression pair data type.
 */
class LocationValuation2WeightEditorView<T> extends NodeValue2WeightEditorView {
    /** The condition/valuation-expression editing table control. */
    private final Table assocTable;

    /** A {@link TableEditor} associated with the <tt>assocTable</tt>. */
    private final TableEditor assocTableEditor;

    /**
     * The set of listeners called when an expression within the
     * <tt>assocTable</tt> get changed by the user.
     */
    private final Set<GenericListener<ExpressionPairChangedEvent>> onExpressionChangedListeners;

    /**
     * The set of listeners called when a condition/valuation-expresion pair is
     * requested to be deleted.
     */
    private final Set<GenericListener<T>> onDeleteExpressionPairListeners;

    /**
     * The set of listeners called when a condition/valuation-expression pair is
     * requested to be created.
     */
    private final Set<Runnable> onCreateExpressionPairListeners;

    /**
     * A {@link Map} associating condition/valuation-expression instances with
     * their respective {@link TableItem}.
     */
    private final Map<T, TableItem> expressionPairs2TableItems;

    /** An accessor for instances of <tt>T</tt>. */
    private final IdentifierAccessor<T> identifierAccessor;

    /**
     * Construct a new {@link LocationValuation2WeightEditorView} instance.
     * 
     * @param parent
     *            The view's parent control.
     * @param identifierAccessor
     *            An accessor for instances of <tt>T</tt>.
     */
    public LocationValuation2WeightEditorView(Composite parent,
            IdentifierAccessor<T> identifierAccessor) {
        super(parent);

        this.identifierAccessor = identifierAccessor;

        Composite ourDrawingArea = getCustomControl();
        ourDrawingArea.setLayout(new FillLayout());

        Group assocGrp = new Group(ourDrawingArea, SWT.NONE);
        assocGrp.setText("Valuation Weight Expressions");
        assocGrp.setLayout(new FillLayout());

        assocTable = new Table(assocGrp, SWT.MULTI);
        assocTable.setHeaderVisible(true);

        TableColumn conditionCol = new TableColumn(assocTable, SWT.LEFT);
        TableColumn valuationCol = new TableColumn(assocTable, SWT.LEFT);
        TableColumn errorCol = new TableColumn(assocTable, SWT.LEFT);

        conditionCol.setWidth(assocTable.getBounds().width / 2);
        errorCol.setWidth(assocTable.getBounds().width / 4);

        conditionCol.setText("Condition");
        valuationCol.setText("Weight");
        errorCol.setText("Errors");

        valuationCol.setWidth(100);
        conditionCol.setWidth(200);
        errorCol.setWidth(100);

        this.onCreateExpressionPairListeners = new HashSet<>();
        this.onDeleteExpressionPairListeners = new HashSet<>();
        this.onExpressionChangedListeners = new HashSet<>();
        this.expressionPairs2TableItems = new IdentityHashMap<>();

        this.assocTableEditor = new TableEditor(this.assocTable);
        this.assocTableEditor.minimumWidth = 50;
        this.assocTableEditor.grabHorizontal = true;
        this.assocTableEditor.horizontalAlignment = SWT.LEFT;

        setupContextMenu();
        setupInlineTextEditors();

        ourDrawingArea.layout();
        assocGrp.layout();
    }

    /**
     * Set up the text editing facility within the <tt>assocTable</tt>
     * {@link Table} control.
     */
    private void setupInlineTextEditors() {
        this.assocTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDoubleClick(MouseEvent arg0) {
                Point mouseClickPt = new Point(arg0.x, arg0.y);
                TableItem matchingTableItem = assocTable.getItem(mouseClickPt);
                if (matchingTableItem == null) {
                    System.err.println("Editor setup: No matching table item");
                    return;
                }

                // Determine the table column which should be edited
                int editedColumn = -1;
                for (int i = 0; i < assocTable.getColumnCount(); i++) {
                    Rectangle rect = matchingTableItem.getBounds(i);
                    if (rect.contains(mouseClickPt)) {
                        editedColumn = i;
                    }
                }

                if (editedColumn == -1) {
                    System.err.println("Editor setup: editedColumn == -1");
                    return;
                }

                if (editedColumn < 2) {
                    displayInlineTextEditor(matchingTableItem, editedColumn);
                }
            }
        });
    }

    /**
     * Display the in-table text editor for the given {@link TableItem}
     * instance.
     * 
     * @param item
     *            The {@link TableItem} marking the row within which the editor
     *            is to be displayed.
     * @param column
     *            A non-negative integer marking the column within which the
     *            editor is to be displayed.
     */
    private void displayInlineTextEditor(final TableItem item, final int column) {
        Control formerEditor = this.assocTableEditor.getEditor();
        if (formerEditor != null && !formerEditor.isDisposed()) {
            formerEditor.dispose();
        }

        final Text txtCtrl = new Text(this.assocTable, SWT.NONE);

        txtCtrl.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent arg0) {
            }

            @Override
            public void focusLost(FocusEvent arg0) {
                onFinishedEditingTableItem(item, txtCtrl.getText(), column);
                txtCtrl.dispose();
            }

        });

        txtCtrl.addTraverseListener(new TraverseListener() {

            @Override
            public void keyTraversed(TraverseEvent arg0) {
                if (arg0.detail == SWT.TRAVERSE_RETURN) {
                    onFinishedEditingTableItem(item, txtCtrl.getText(), column);
                    txtCtrl.dispose();
                }
                else if (arg0.detail == SWT.TRAVERSE_ESCAPE) {
                    txtCtrl.dispose();
                }
            }
        });

        txtCtrl.setText(item.getText(column));
        txtCtrl.setFocus();
        txtCtrl.selectAll();

        this.assocTableEditor.setEditor(txtCtrl, item, column);
    }

    /**
     * Handler called after having finished editing a {@link TableItem} within
     * <tt>assocTable</tt>, firing the appropriate expression-change listeners.
     * 
     * @param ti
     *            The {@link TableItem} which was subject of the editor.
     * @param newText
     *            The new cell content typed by the user.
     * @param column
     *            The column index indicating which part of <tt>ti</tt> was
     *            edited.
     */
    @SuppressWarnings("unchecked")
    private void onFinishedEditingTableItem(TableItem ti, String newText, int column) {
        for (GenericListener<ExpressionPairChangedEvent> listener : onExpressionChangedListeners) {
            ExpressionPairChangedEvent ece = new ExpressionPairChangedEvent();
            ece.expressionPair = (T) ti.getData();
            ece.newCondition = this.identifierAccessor.getCondition(ece.expressionPair);
            ece.newValueExpression = this.identifierAccessor.getValueExpression(ece.expressionPair);
            if (column == 0) {
                ece.newCondition = newText;
            }
            else {
                ece.newValueExpression = newText;
            }
            listener.onFiredEvent(ece);
        }
    }

    /**
     * Set up a context menu for <tt>assocTable</tt> and establish its glue to
     * the externally provided listeners.
     */
    private void setupContextMenu() {
        final Menu contextMenu = new Menu(this.assocTable);
        this.assocTable.setMenu(contextMenu);

        final MenuItem newItem = new MenuItem(contextMenu, SWT.PUSH);
        newItem.setText("New");

        final MenuItem deleteItem = new MenuItem(contextMenu, SWT.PUSH);
        deleteItem.setText("Delete");

        newItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (Runnable r : onCreateExpressionPairListeners) {
                    r.run();
                }
            }
        });

        deleteItem.addSelectionListener(new SelectionAdapter() {
            @SuppressWarnings("unchecked")
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (TableItem ti : assocTable.getSelection()) {
                    T data = (T) ti.getData();
                    for (GenericListener<T> listener : onDeleteExpressionPairListeners) {
                        listener.onFiredEvent(data);
                    }
                }
            }
        });

        contextMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuShown(MenuEvent arg0) {
                deleteItem.setEnabled(assocTable.getSelectionCount() > 0);
            }
        });
    }

    /**
     * Add a condition/valuation-expression pair to the editor control.
     * 
     * @param identifier
     *            The condition/valuation-expression pair's handle.
     */
    public void addConditionPair(T identifier) {
        TableItem ti = new TableItem(this.assocTable, SWT.NONE);
        ti.setText(0, identifierAccessor.getCondition(identifier));
        ti.setText(1, identifierAccessor.getValueExpression(identifier));
        ti.setData(identifier);
        this.expressionPairs2TableItems.put(identifier, ti);
    }

    /**
     * Add a listener getting called whenever the user requests the creation of
     * a new condition/valuation-expression pair.
     * 
     * @param listener
     *            A {@link Runnable} instance getting called as described above.
     */
    public void addExpressionCreatedListener(final Runnable listener) {
        this.onCreateExpressionPairListeners.add(listener);
    }

    /**
     * Add a listener getting called whenever the user requests the deletion of
     * a condition/valuation-expression pair.
     * 
     * @param listener
     *            A {@link GenericListener} taking a <tt>T</tt>-typed handle of
     *            the condition/valuation-expression pair as an argument to
     *            <tt>onFiredEvent()</tt>.
     */
    public void addExpressionDeletedListener(final GenericListener<T> listener) {
        this.onDeleteExpressionPairListeners.add(listener);
    }

    /**
     * Add a listener getting called whenever the user requests to modify a
     * condition/valuation-expression pair.
     * 
     * @param listener
     *            A {@link GenericListener} taking an
     *            {@link ExpressionPairChangedEvent} instance carrying the
     *            modification information as an argument to
     *            <tt>onFiredEvent</tt>
     */
    public void addExpressionChangedListener(
            final GenericListener<ExpressionPairChangedEvent> listener) {
        this.onExpressionChangedListeners.add(listener);
    }

    /**
     * Replace a condition/valuation-expression pair within the editing table.
     * 
     * @param oldPair
     *            The old condition/valuation-expression pair.
     * @param newPair
     *            The replacement of <tt>oldPair</tt>.
     */
    public void replace(T oldPair, T newPair) {
        if (!this.expressionPairs2TableItems.containsKey(oldPair)) {
            throw new IllegalArgumentException("oldPair is not present in the table");
        }

        TableItem ti = this.expressionPairs2TableItems.get(oldPair);
        ti.setData(newPair);
        ti.setText(0, identifierAccessor.getCondition(newPair));
        ti.setText(1, identifierAccessor.getValueExpression(newPair));

        this.expressionPairs2TableItems.remove(oldPair);
        this.expressionPairs2TableItems.put(newPair, ti);
    }

    /**
     * Mark a condition or valuation expression as invalid.
     * 
     * @param pair
     *            The condition/valuation-expression pair.
     * @param valid
     *            The new validity status.
     * @param errorCol
     *            0 for condition, 1 for valuation expression.
     * @param errorMsg
     *            An error message to be displayed.
     */
    public void setValid(T pair, boolean valid, int errorCol, String errorMsg) {
        if (!this.expressionPairs2TableItems.containsKey(pair)) {
            throw new IllegalArgumentException("pair is not present in the table");
        }
        TableItem ti = this.expressionPairs2TableItems.get(pair);

        if (!valid) {
            ti.setForeground(errorCol, Display.getDefault().getSystemColor(SWT.COLOR_RED));
        }
        else {
            ti.setForeground(errorCol, null);
        }
        ti.setText(2, errorMsg);
    }

    /**
     * Remove a condition/valuation-expression pair from the editing table.
     * 
     * @param identifier The pair's identifier.
     */
    public void remove(T identifier) {
        if (!this.expressionPairs2TableItems.containsKey(identifier)) {
            throw new IllegalArgumentException("identifier is not present in the table");
        }

        TableItem ti = this.expressionPairs2TableItems.get(identifier);
        ti.dispose();
        expressionPairs2TableItems.remove(identifier);
    }

    /**
     * Check the presence of the given condition/valuation-expression pair
     * within the editing table.
     * 
     * @param identifier
     *            The handle to the condition/valuation-expression whose
     *            existence is to be determined.
     * @return true iff the specified pair is present as specified.
     */
    public boolean contains(T identifier) {
        return this.expressionPairs2TableItems.containsKey(identifier);
    }

    /**
     * A data structure representing modifications of
     * condition/valuation-expression pairs.
     */
    public class ExpressionPairChangedEvent {
        /** A handle to the modified pair. */
        public T expressionPair;

        /** The pair's new condition. */
        public String newCondition;

        /** The pair's new valuation-expression. */
        public String newValueExpression;
    }

    /**
     * An accessor for condition/valuation-expression handles.
     * 
     * @param <T>
     */
    public interface IdentifierAccessor<T> {
        /**
         * Get the pair's condition part.
         * 
         * @param identifier
         *            The handle.
         * @return A string representing the pair's condition part.
         */
        String getCondition(T identifier);

        /**
         * Get the part's valuation-expression part.
         * 
         * @param identifier
         *            The handle.
         * @return A string representing the pair's valuation-expression part.
         */
        String getValueExpression(T identifier);
    }
}
