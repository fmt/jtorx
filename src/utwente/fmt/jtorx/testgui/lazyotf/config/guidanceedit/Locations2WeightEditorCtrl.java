package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import utwente.fmt.jtorx.lazyotf.guidance.Locations2Weight;

/**
 * A controller for {@link Locations2WeightEditorView} instances.
 */
class Locations2WeightEditorCtrl extends EditorController {
    /** The view instance associated with this controller. */
    private final Locations2WeightEditorView view;

    /** The main controller. */
    private final WeightsEditorImpl mainCtrl;

    /** The edited {@link Locations2Weight} instance. */
    private final Locations2Weight subject;

    /**
     * Construct a new {@link Locations2WeightEditorCtrl} instance.
     * 
     * @param subject
     *            The edited {@link Locations2Weight} instance.
     * @param view
     *            The {@link Locations2WeightEditorView} instance governed by
     *            this controller.
     * @param mainCtrl
     *            The main controller.
     * @param readOnly
     *            Iff true, <tt>subject</tt> will not be modified by this
     *            controller.
     */
    public Locations2WeightEditorCtrl(Locations2Weight subject, Locations2WeightEditorView view,
            WeightsEditorImpl mainCtrl, boolean readOnly) {
        this.view = view;
        this.mainCtrl = mainCtrl;
        this.subject = subject;

        this.view.setMinimumWeight(0);
        this.view.setMaximumWeight(Integer.MAX_VALUE);
        this.view.setDisplayName(subject.getDisplayName());
        this.view.setOrdinaryStateWeight(subject.getOrdinaryWeight());
        this.view.setInducingStateWeight(subject.getInducingWeight());
        this.view.setTestGoalWeight(subject.getTestGoalWeight());
        this.view.setEnabled(!readOnly);

        setupListeners();
    }

    /**
     * Set up the listeners observing the view.
     */
    private void setupListeners() {
        view.addNameChangedListener(mainCtrl.makeDefaultRenameListener(subject));
        view.addWeightChangedListener(new Runnable() {
            @Override
            public void run() {
                subject.setInducingWeight(view.getInducingStateWeight());
                subject.setOrdinaryWeight(view.getOrdinaryStateWeight());
                subject.setTestGoalWeight(view.getTestGoalWeight());
            }
        });
    }

    @Override
    public void update() {
        view.setDisplayName(subject.getDisplayName());
        view.setOrdinaryStateWeight(subject.getOrdinaryWeight());
        view.setInducingStateWeight(subject.getInducingWeight());
        view.setTestGoalWeight(subject.getTestGoalWeight());
    }
}
