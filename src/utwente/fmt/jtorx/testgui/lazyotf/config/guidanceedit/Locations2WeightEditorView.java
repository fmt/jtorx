package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

/**
 * A view for displaying {@link Locations2Weight} instances, behaving like
 * {@link Composite} instances.
 */
class Locations2WeightEditorView extends NodeValue2WeightEditorView {
    /** The spinner control for setting the test goal weight. */
    private final Spinner testGoalWeightCtrl;

    /** The spinner control for setting the ordinary state weight. */
    private final Spinner ordinaryWeightCtrl;

    /** The spinner control for setting the inducing state weight. */
    private final Spinner inducingWeightCtrl;

    /** The set of listeners called when the weights are changed. */
    private final Set<Runnable> onWeightChangedListeners;

    /**
     * Construct a new {@link Locations2Weight} instance.
     * 
     * @param parent
     *            The instance's parent control.
     */
    public Locations2WeightEditorView(Composite parent) {
        super(parent);

        Composite ourDrawingArea = getCustomControl();
        ourDrawingArea.setLayout(new FillLayout());

        GridLayout twoColLayout = new GridLayout();
        twoColLayout.numColumns = 2;

        Group weightGrp = new Group(ourDrawingArea, SWT.NONE);
        weightGrp.setText("Weights");
        weightGrp.setLayout(twoColLayout);

        Label tgWeightLabel = new Label(weightGrp, SWT.NONE);
        tgWeightLabel.setText("Test goal state weight:");
        testGoalWeightCtrl = new Spinner(weightGrp, SWT.NONE);
        testGoalWeightCtrl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Label ordWeightLabel = new Label(weightGrp, SWT.NONE);
        ordWeightLabel.setText("Ordinary state weight:");
        ordinaryWeightCtrl = new Spinner(weightGrp, SWT.NONE);
        ordinaryWeightCtrl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Label indWeightLabel = new Label(weightGrp, SWT.NONE);
        indWeightLabel.setText("Inducing state weight:");
        inducingWeightCtrl = new Spinner(weightGrp, SWT.NONE);
        inducingWeightCtrl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        onWeightChangedListeners = new HashSet<>();

        setupListeners();
        ourDrawingArea.layout();
        weightGrp.layout();
    }

    /**
     * Glue the provided listeners to the SWT controls.
     */
    private void setupListeners() {
        final SelectionAdapter weightChangeListener = new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (Runnable r : onWeightChangedListeners) {
                    r.run();
                }
            }
        };

        ordinaryWeightCtrl.addSelectionListener(weightChangeListener);
        testGoalWeightCtrl.addSelectionListener(weightChangeListener);
        inducingWeightCtrl.addSelectionListener(weightChangeListener);
    }

    /**
     * Add a listener which gets called when the user requests to change the
     * weights.
     * 
     * @param listener
     *            A {@link Runnable} instance called as described.
     */
    public void addWeightChangedListener(Runnable listener) {
        onWeightChangedListeners.add(listener);
    }

    /**
     * Set the minimum weight assignable to the location test types.
     * 
     * @param minimumWeight
     *            The new minimum weight.
     */
    public void setMinimumWeight(int minimumWeight) {
        testGoalWeightCtrl.setMinimum(minimumWeight);
        ordinaryWeightCtrl.setMinimum(minimumWeight);
        inducingWeightCtrl.setMinimum(minimumWeight);
    }

    /**
     * Set the maximum weight assignable to the location test types.
     * 
     * @param maximumWeight
     *            The new maximum weight.
     */
    public void setMaximumWeight(int maximumWeight) {
        testGoalWeightCtrl.setMaximum(maximumWeight);
        ordinaryWeightCtrl.setMaximum(maximumWeight);
        inducingWeightCtrl.setMaximum(maximumWeight);
    }

    /**
     * Set the displayed test goal weight.
     * 
     * @param weight
     *            The test goal weight to be displayed.
     */
    public void setTestGoalWeight(int weight) {
        testGoalWeightCtrl.setSelection(weight);
    }

    /**
     * @return The currently selected test goal weight.
     */
    public int getTestGoalWeight() {
        return testGoalWeightCtrl.getSelection();
    }

    /**
     * Set the displayed inducing state weight.
     * 
     * @param weight
     *            The inducing state weight to be displayed.
     */
    public void setInducingStateWeight(int weight) {
        inducingWeightCtrl.setSelection(weight);
    }

    /**
     * @return The currently selected inducing state weight.
     */
    public int getInducingStateWeight() {
        return inducingWeightCtrl.getSelection();
    }

    /**
     * Set the displayed ordinary state weight.
     * 
     * @param weight
     *            The ordinary state weight to be displayed.
     */
    public void setOrdinaryStateWeight(int weight) {
        ordinaryWeightCtrl.setSelection(weight);
    }

    /**
     * @return The currently selected inducing state weight
     */
    public int getOrdinaryStateWeight() {
        return ordinaryWeightCtrl.getSelection();
    }

    @Override
    public void redraw() {
        super.redraw();

        inducingWeightCtrl.redraw();
        ordinaryWeightCtrl.redraw();
        testGoalWeightCtrl.redraw();
    }

    @Override
    public void setEnabled(boolean enabledness) {
        super.setEnabled(enabledness);
        inducingWeightCtrl.setEnabled(enabledness);
        ordinaryWeightCtrl.setEnabled(enabledness);
        testGoalWeightCtrl.setEnabled(enabledness);
    }
}
