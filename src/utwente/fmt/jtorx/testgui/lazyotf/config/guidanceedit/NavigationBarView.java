package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.TextTreeEditor;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.UnaryPredicate;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.RenameListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.SetEnabledListener;

/**
 * The navigation bar view class. Behaves like an SWT composite.
 */
class NavigationBarView extends Composite {
    /**
     * The tab folder containing the "Test Tree Trace Weighers" and
     * "Node Weighers" tabs.
     */
    private final CTabFolder register;

    /**
     * The list of {@link Path2Weight}s. Its "data" field indicates whether it
     * currently matches the test tree trace weigher list.
     */
    private final Tree treeTraceWeigherListCtrl;

    /**
     * The list of node value weighers. Its "data" field indicates whether it
     * currently matches the nodeValueWeighers list.
     */
    private final Tree nodeValueWeigherListCtrl;

    /** true iff redraw() should actually redraw the window. */
    private boolean executeRedraw = true;

    /** {@link Path2Weight} display name editor. */
    private final TextTreeEditor treeTraceWeigherListEditor;

    /** Node value weigher display name editor. */
    private final TextTreeEditor nodeValueWeigherListEditor;

    /** Listeners on the "rename test tree trace weigher" event. */
    private final Set<RenameListener<Path2Weight>> onRenamePath2WeightListeners;

    /** Listeners on the "rename node value weigher" event. */
    private final Set<RenameListener<NodeValue2Weight>> onRenameNodeValue2WeightListeners;

    /** Listeners on the "new test tree trace weigher" event. */
    private final Set<GenericListener<String>> onMakePath2WeightListeners;

    /** Listeners on the "new node value weigher" event. */
    private final Set<GenericListener<String>> onMakeNodeValueWeigherListeners;

    /** List of available {@link Path2Weight} type names. */
    private final List<String> treeTraceWeigherTypeNames;

    /** List of available node value weigher type names. */
    private final List<String> nodeValueWeigherTypeNames;

    /**
     * The set of listeners getting called when the user requests the deletion
     * of a {@link Path2Weight}.
     */
    private final Set<GenericListener<Path2Weight>> onDeletePath2WeightListeners;

    /**
     * The set of listeners getting called when the user requests the deletion
     * of a node value weigher.
     */
    private final Set<GenericListener<NodeValue2Weight>> onDeleteNodeValueWeigherListeners;

    /**
     * The set of listeners getting called when the user requests to change a
     * {@link Path2Weight}'s enabledness.
     */
    private final Set<SetEnabledListener<Path2Weight>> onSetPath2WeightEnabledListeners;

    /**
     * An enum reflecting the "Test Tree Trace Weigher" and "Node Value Weigher"
     * view modes.
     */
    public enum ViewMode {
        GUIDANCE_METHOD, NODE_VALUE_WEIGHER
    };

    /**
     * A predicate determining whether a given {@link Path2Weight} may be edited by
     * the user.
     */
    private UnaryPredicate<Path2Weight> isPath2WeightEditablePred;

    /**
     * A predicate determining whether a given node value weigher may be edited.
     */
    private UnaryPredicate<NodeValue2Weight> isNodeValueWeigherEditablePred;

    /** The tooltip for the tree trace weigher selection list. */
    private static final String GUIDANCE_METHOD_TOOL_TIP = "Check a "
            + "Path2Weight to enable it. The weight of a given path in the"
            + "test tree is computed by taking the maximal weight assigned to that"
            + "trace by the enabled weighers.";

    /**
     * Construct a new {@link NavigationBarView} instance. Initially, the lists
     * of {@link Path2Weight}s and node weighers are empty.
     * 
     * @param parent
     *            The instance's parent widget.
     * @param initialViewMode
     *            The initial register selection.
     */
    public NavigationBarView(Composite parent, ViewMode initialViewMode) {
        super(parent, SWT.NONE);

        treeTraceWeigherTypeNames = new ArrayList<>();
        nodeValueWeigherTypeNames = new ArrayList<>();
        onMakePath2WeightListeners = new HashSet<>();
        onMakeNodeValueWeigherListeners = new HashSet<>();
        onDeletePath2WeightListeners = new HashSet<>();
        onDeleteNodeValueWeigherListeners = new HashSet<>();
        onSetPath2WeightEnabledListeners = new HashSet<>();

        if (parent == null) {
            throw new IllegalArgumentException("parent must not be null");
        }

        onRenamePath2WeightListeners = new HashSet<>();
        onRenameNodeValue2WeightListeners = new HashSet<>();

        setLayout(new FillLayout());
        register = new CTabFolder(this, SWT.BORDER);

        CTabItem c1 = new CTabItem(register, SWT.NONE);
        c1.setText("Path2Weights");
        treeTraceWeigherListCtrl = new Tree(register, SWT.FLAT | SWT.CHECK);
        c1.setControl(treeTraceWeigherListCtrl);
        c1.setData(ViewMode.GUIDANCE_METHOD);

        treeTraceWeigherListCtrl.setToolTipText(GUIDANCE_METHOD_TOOL_TIP);

        CTabItem c2 = new CTabItem(register, SWT.NONE);
        c2.setText("NodeValue2Weights");
        nodeValueWeigherListCtrl = new Tree(register, SWT.FLAT);
        c2.setControl(nodeValueWeigherListCtrl);
        c2.setData(ViewMode.NODE_VALUE_WEIGHER);

        switch (initialViewMode) {
        case GUIDANCE_METHOD:
            register.setSelection(c1);
            break;
        case NODE_VALUE_WEIGHER:
            register.setSelection(c2);
            break;
        default:
            break;
        }

        treeTraceWeigherListEditor = new TextTreeEditor(treeTraceWeigherListCtrl);
        nodeValueWeigherListEditor = new TextTreeEditor(nodeValueWeigherListCtrl);

        isPath2WeightEditablePred = new UnaryPredicate<Path2Weight>() {
            @Override
            public boolean evaluate(Path2Weight argument) {
                return true;
            }
        };

        isNodeValueWeigherEditablePred = new UnaryPredicate<NodeValue2Weight>() {
            @Override
            public boolean evaluate(NodeValue2Weight argument) {
                return true;
            }
        };

        treeTraceWeigherListCtrl.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                if (ev.detail == SWT.CHECK) {
                    TreeItem ti = (TreeItem) ev.item;
                    Path2Weight gm = (Path2Weight) ti.getData();
                    for (SetEnabledListener<Path2Weight> listener : onSetPath2WeightEnabledListeners) {
                        listener.onSetEnabled(gm, ti.getChecked());
                    }
                }
            }
        });

        setupEditorListeners();
        setupTreeTraceWeigherListContextMenu();
        setupNodeValueWeigherContextMenu();
        redraw();
    }

    /**
     * Set up listeners for requests to bring up the name editor within the tree
     * controls.
     */
    private void setupEditorListeners() {
        treeTraceWeigherListCtrl.addListener(SWT.MouseDoubleClick, new Listener() {
            @Override
            public void handleEvent(Event ev) {
                renameCurrentlySelectedPath2Weight();
            }
        });

        nodeValueWeigherListCtrl.addListener(SWT.MouseDoubleClick, new Listener() {
            @Override
            public void handleEvent(Event ev) {
                renameCurrentlySelectedNodeValueWeigher();
            }
        });
    }

    /**
     * Display the name editor.
     * 
     * @param context
     *            The {@link TreeItem} at which the editor should be displayed.
     * @param editor
     *            The @{TextTreeEditor} controlling the editor.
     * @param finishedListener
     *            A {@link GenericListener<String>} to be called when the user
     *            finishes editing the name string.
     */
    private static void displayRenamingEditor(TreeItem context, TextTreeEditor editor,
            GenericListener<String> finishedListener) {
        editor.removeAllEditFinishedListeners();
        editor.addEditFinishedListener(finishedListener);
        editor.displayEditor(context, context.getText());
    }

    /**
     * Display the name editor for the currently selected {@link Path2Weight}.
     */
    public void renameCurrentlySelectedPath2Weight() {
        TreeItem[] currentSelection = treeTraceWeigherListCtrl.getSelection();
        if (currentSelection.length > 0) {
            final TreeItem currentSel = currentSelection[0];
            if (isPath2WeightEditablePred.evaluate((Path2Weight) currentSel
                    .getData())) {
                displayRenamingEditor(currentSel,
                        treeTraceWeigherListEditor,
                        new GenericListener<String>() {
                            @Override
                            public void onFiredEvent(String newName) {
                                onRenameTreeTraceWeigher(currentSel, newName);
                            }
                        });
            }
        }
    }

    /**
     * Display the name editor for the currently selected node value weigher.
     */
    public void renameCurrentlySelectedNodeValueWeigher() {
        TreeItem[] currentSelection = nodeValueWeigherListCtrl.getSelection();
        if (currentSelection.length > 0) {
            final TreeItem currentSel = currentSelection[0];
            if (isNodeValueWeigherEditablePred.evaluate((NodeValue2Weight) currentSel.getData())) {
                displayRenamingEditor(currentSel,
                        nodeValueWeigherListEditor,
                        new GenericListener<String>() {
                            @Override
                            public void onFiredEvent(String newName) {
                                onRenameNodeValueWeigher(currentSel, newName);
                            }
                        });
            }
        }
    }

    /**
     * Handler for the "rename {@link Path2Weight}" event, calling the
     * user-supplied rename handlers.
     * 
     * @param parent
     *            The TreeItem containing the {@link Path2Weight} to be renamed.
     * @param newName
     *            The method's designated new name.
     */
    private void onRenameTreeTraceWeigher(TreeItem parent, String newName) {
        Path2Weight renamedMethod = (Path2Weight) parent.getData();
        for (RenameListener<Path2Weight> gl : onRenamePath2WeightListeners) {
            gl.onRename(renamedMethod, newName);
        }
    }

    /**
     * Handler for the "rename node value weigher" event, calling the user-
     * supplied rename handlers.
     * 
     * @param parent
     *            The tree item belonging to the node value weigher i question.
     * @param newName
     *            The object's new name.
     */
    private void onRenameNodeValueWeigher(TreeItem parent, String newName) {
        NodeValue2Weight renamedNv2w = (NodeValue2Weight) parent.getData();
        for (RenameListener<NodeValue2Weight> gl : onRenameNodeValue2WeightListeners) {
            gl.onRename(renamedNv2w, newName);
        }
    }

    /**
     * Handler for requests for node value weigher deletions, calling the
     * controller-supplied deletion handlers.
     * 
     * @param it
     *            The {@link NodeValue2Weight} instance requested to be deleted.
     */
    private void onDeleteNodeValueWeigher(NodeValue2Weight it) {
        for (GenericListener<NodeValue2Weight> nvl : onDeleteNodeValueWeigherListeners) {
            nvl.onFiredEvent(it);
        }
    }

    /**
     * Handler for requests for {@link Path2Weight} deletions, calling the
     * controller-supplied deletion handlers.
     * 
     * @param it
     *            The {@link Path2Weight} instance requests to be
     *            deleted.
     */
    private void onDeleteTreeTraceWeigher(Path2Weight it) {
        for (GenericListener<Path2Weight> gm : onDeletePath2WeightListeners) {
            gm.onFiredEvent(it);
        }
    }

    @Override
    public void redraw() {
        super.redraw();

        if (!executeRedraw) {
            return;
        }

        for (TreeItem ti : treeTraceWeigherListCtrl.getItems()) {
            Path2Weight gm = (Path2Weight) ti.getData();
            ti.setText(gm.getDisplayName());
        }

        for (TreeItem ti : nodeValueWeigherListCtrl.getItems()) {
            NodeValue2Weight nv = (NodeValue2Weight) ti.getData();
            ti.setText(nv.getDisplayName());
        }

        ViewUtilities.sortTreeItemsByName(treeTraceWeigherListCtrl, true, new HashSet<String>());
        ViewUtilities.sortTreeItemsByName(nodeValueWeigherListCtrl, true, new HashSet<String>());

        treeTraceWeigherListCtrl.redraw();
        nodeValueWeigherListCtrl.redraw();
    }

    @Override
    public void setRedraw(boolean redraw) {
        super.setRedraw(redraw);
        executeRedraw = redraw;
        if (redraw) {
            redraw();
        }
    }

    /**
     * Set the list of displayed {@link Path2Weight}s. All {@link Path2Weight}
     * selection listeners are deactivated until the next <tt>redraw</tt> call.
     * 
     * @param path2Weights
     *            The new list of {@link Path2Weight}s. This list must not be null
     *            and must not contain null.
     * @param keepSelection
     *            If the currently selected {@link Path2Weight} is present in the
     *            new list of {@link Path2Weight}s, reselect it (without issuing a
     *            selection event).
     * @param enabled{@link Path2Weight}s
     *            The set of enabled {@link Path2Weight}s. These {@link Path2Weight}s
     *            will be checked initially.
     */
    public void setPath2Weights(List<Path2Weight> path2Weights,
            boolean keepSelection, Set<Path2Weight> enabledPath2Weights) {
        if (path2Weights == null) {
            throw new IllegalArgumentException("path2Weights must not be null");
        }
        else if (path2Weights.contains(null)) {
            throw new IllegalArgumentException("path2Weights must not contain null");
        }

        TreeItem[] currentSelection = treeTraceWeigherListCtrl.getSelection();
        final Path2Weight currentlySelectedGM;
        if (currentSelection.length > 0) {
            currentlySelectedGM = (Path2Weight) currentSelection[0].getData();
        }
        else {
            currentlySelectedGM = null;
        }

        treeTraceWeigherListCtrl.removeAll();
        for (Path2Weight gm : path2Weights) {
            TreeItem ti = new TreeItem(treeTraceWeigherListCtrl, SWT.NONE);
            ti.setText(gm.getDisplayName());
            ti.setData(gm);
            ti.setChecked(enabledPath2Weights.contains(gm));
            if (keepSelection && gm == currentlySelectedGM) {
                treeTraceWeigherListCtrl.setSelection(ti);
            }
        }

        redraw();
    }

    /**
     * Set the list of displayed node value weighers. All node value weigher
     * selection listeners are deactivated until the next <tt>redraw</tt> call.
     * 
     * @param nodeValueWeighers
     *            The new list of node value weighers. This list must not be
     *            null and must not contain null.
     */
    public void setNodeValueWeighers(List<NodeValue2Weight> nodeValueWeighers) {
        if (nodeValueWeighers == null) {
            throw new IllegalArgumentException("nodeValueWeighers must not be null");
        }
        else if (nodeValueWeighers.contains(null)) {
            throw new IllegalArgumentException("nodeValueWeighers must not contain null");
        }

        nodeValueWeigherListCtrl.removeAll();
        for (NodeValue2Weight nv : nodeValueWeighers) {
            TreeItem ti = new TreeItem(nodeValueWeigherListCtrl, SWT.NONE);
            ti.setText(nv.getDisplayName());
            ti.setData(nv);
        }

        redraw();
    }

    /**
     * Add a selection listener for {@link Path2Weight}s. The given listener is
     * fired whenever the {@link Path2Weight} selection changes (except for when
     * the list of {@link Path2Weight}s is cleared).
     * 
     * @param it
     *            The listener, taking the newly selected
     *            {@link Path2Weight} as an argument.
     */
    public void addTreeTraceWeigherSelectionListener(final GenericListener<Path2Weight> it) {
        NavigationBarView.addTreeSelectionListener(treeTraceWeigherListCtrl,
                it);

        register.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                if (getViewMode() == ViewMode.GUIDANCE_METHOD) {
                    Path2Weight selectedGM = getSelectedTreeTraceWeigher();
                    it.onFiredEvent(selectedGM);
                }
            }
        });
    }

    /**
     * Add a selection listener for requested changes in {@link Path2Weight}
     * enabledness. The supplied listener is fired whenever the user changes the
     * state of a {@link Path2Weight}'s check box.
     * 
     * @param it
     *            The listener.
     */
    public void addTreeTraceWeigherEnabledListener(final SetEnabledListener<Path2Weight> it) {
        onSetPath2WeightEnabledListeners.add(it);
    }

    /**
     * Add a view mode listener. The given listener is fired whenever the user
     * switches between the "Test Tree Trace Weighers" and "Node Weigher" tabs.
     * 
     * @param it
     *            The listener, taking an {@link ViewMode} argument.
     */
    public void addDisplayModeListener(final GenericListener<ViewMode> it) {
        register.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                CTabItem cti = register.getSelection();
                if (cti != null && cti.getData() != null) {
                    it.onFiredEvent((ViewMode) cti.getData());
                }
            }
        });
    }

    /**
     * Add a selection listener for node value weighers. The given listener is
     * fired whenever the node value weigher selection changes (except for when
     * the list of node value weighers is cleared).
     * 
     * @param it
     *            The listener, taking the newly selected
     *            {@link NodeValue2Weight} as an argument.
     */
    public void addNodeValueWeigherSelectionListener(final GenericListener<NodeValue2Weight> it) {
        NavigationBarView.addTreeSelectionListener(nodeValueWeigherListCtrl,
                it);

        register.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                if (getViewMode() == ViewMode.NODE_VALUE_WEIGHER) {
                    NodeValue2Weight selectedNV2W = getSelectedNodeValueWeigher();
                    it.onFiredEvent(selectedNV2W);
                }
            }
        });
    }

    /**
     * Add a listener for the "rename test tree trace weigher" event.
     * 
     * @param it
     *            The listener to be added.
     */
    public void addRenameTreeTraceWeigherListener(RenameListener<Path2Weight> it) {
        if (it == null) {
            throw new IllegalArgumentException("it must not be null");
        }
        onRenamePath2WeightListeners.add(it);
    }

    /**
     * Add a listener for the "rename node value weigher" event.
     * 
     * @param it
     *            A {@link RenameListener} instance to be added.
     */
    public void addRenameNodeValueWeigherListener(RenameListener<NodeValue2Weight> it) {
        if (it == null) {
            throw new IllegalArgumentException("it must not be null");
        }
        onRenameNodeValue2WeightListeners.add(it);
    }

    /**
     * Add a listener for requests for the deletion of a {@link Path2Weight}.
     * 
     * @param it
     *            A {@link GenericListener} taking a
     *            {@link Path2Weight} as an argument to
     *            <tt>onFiredEvent()</tt>, getting called whenever the user
     *            requests to delete a {@link Path2Weight}.
     */
    public void addDeleteTreeTraceWeigherListener(GenericListener<Path2Weight> it) {
        if (it == null) {
            throw new IllegalArgumentException("it must not be null");
        }
        onDeletePath2WeightListeners.add(it);
    }

    /**
     * Add a listener for requests for the deletion of a node value weigher.
     * 
     * @param it
     *            A {@link GenericListener} taking a {@link NodeValue2Weight} as
     *            an argument to <tt>onFiredEvent()</tt>, getting called
     *            whenever the user requests to delete a node value weigher.
     */
    public void addDeleteNodeValueWeigherListener(GenericListener<NodeValue2Weight> it) {
        if (it == null) {
            throw new IllegalArgumentException("it must not be null");
        }
        onDeleteNodeValueWeigherListeners.add(it);
    }

    /**
     * Add a listener for the "make new test tree trace weigher" event.
     * 
     * @param it
     *            The listener to be added. The requested {@link Path2Weight} type
     *            is passed to the listener as a string.
     */
    public void addMakeTreeTraceWeigherListener(GenericListener<String> it) {
        if (it == null) {
            throw new IllegalArgumentException("it must not be null");
        }
        onMakePath2WeightListeners.add(it);
    }

    /**
     * Add a listener for the "make new node value weigher" event.
     * 
     * @param it
     *            The listener to be added. The requested node value weigher
     *            type is passed to the listener as a string.
     */
    public void addMakeNodeValueWeigherListener(GenericListener<String> it) {
        if (it == null) {
            throw new IllegalArgumentException("it must not be null");
        }
        onMakeNodeValueWeigherListeners.add(it);
    }

    /**
     * Set the predicate deciding whether a given {@link Path2Weight}
     * instance may be edited by the user.
     * 
     * @param it
     *            A {@link UnaryPredicate} instance as described.
     */
    public void setTreeTraceWeigherEditablePredicate(UnaryPredicate<Path2Weight> it) {
        if (it == null) {
            throw new IllegalArgumentException("it must not be null");
        }
        isPath2WeightEditablePred = it;
    }

    /**
     * @return The {@link UnaryPredicate} deciding whether a given
     *         {@link Path2Weight} instance may be edited by the user.
     */
    public UnaryPredicate<Path2Weight> getGuidancMethodEditablePredicate() {
        return isPath2WeightEditablePred;
    }

    /**
     * Set the predicate deciding whether a given {@link NodeValue2Weight}
     * instance may be edited by the user.
     * 
     * @param it
     *            A {@link UnaryPredicate} instance as described.
     */
    public void setNodeValueWeigherEditablePredicate(UnaryPredicate<NodeValue2Weight> it) {
        if (it == null) {
            throw new IllegalArgumentException("it must not be null");
        }
        isNodeValueWeigherEditablePred = it;
    }

    /**
     * @return The {@link UnaryPredicate} deciding whether a given
     *         {@link NodeValue2Weight} instance may be edited by the user.
     */
    public UnaryPredicate<NodeValue2Weight> getNodeValueWeigherPredicate() {
        return isNodeValueWeigherEditablePred;
    }

    /**
     * Set the list of known {@link Path2Weight} types which can be instantiated.
     * 
     * @param names
     *            The described list of type names.
     */
    public void setTreeTraceWeigherTypeNames(List<String> names) {
        treeTraceWeigherTypeNames.clear();
        treeTraceWeigherTypeNames.addAll(names);
    }

    /**
     * Set the list of known node value weigher types which can be instantiated.
     * 
     * @param names
     *            The described list of type names.
     */
    public void setNodeValueWeigherTypeNames(List<String> names) {
        nodeValueWeigherTypeNames.clear();
        nodeValueWeigherTypeNames.addAll(names);
    }

    /**
     * Add a selection listener to the given SWT tree control. The listener is
     * fired whenever the selection changes and the object residing within the
     * selected item's "data" field is passed as an argument. (Note: The "data"
     * field objects therefore must be of type <tt>T</tt>.)
     * 
     * @param treeCtrl
     *            The SWT tree control.
     * @param listener
     *            The new listener.
     * @param <T>
     *            The selection listener type.
     */
    private static <T> void addTreeSelectionListener(final Tree treeCtrl,
            final GenericListener<T> listener) {
        treeCtrl.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                TreeItem[] selection = treeCtrl.getSelection();
                for (TreeItem ti : selection) {
                    if (ti.getData() == null) {
                        throw new IllegalStateException("Tree item "
                                + ti.getText() + " has null data");
                    }
                    try {
                        // This is due to SWT not using Generics.
                        @SuppressWarnings("unchecked")
                        T listenerArg = (T) ti.getData();
                        listener.onFiredEvent(listenerArg);
                    }
                    catch (ClassCastException e) {
                        throw new IllegalStateException("Tree item "
                                + ti.getText() + "'s data field contains "
                                + "an object of the wrong type: "
                                + e.toString());
                    }
                }
            }
        });
    }

    /**
     * Set up the context menu for the {@link Path2Weight} list.
     */
    private void setupTreeTraceWeigherListContextMenu() {
        final NavigationBarView localThis = this;

        setupContextMenu(treeTraceWeigherListCtrl,
                treeTraceWeigherTypeNames,
                onMakePath2WeightListeners,
                new Runnable() {
                    @Override
                    public void run() {
                        renameCurrentlySelectedPath2Weight();
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        onDeleteTreeTraceWeigher(getSelectedTreeTraceWeigher());
                    }
                },
                new UnaryPredicate<TreeItem>() {
                    @Override
                    public boolean evaluate(TreeItem arg) {
                        UnaryPredicate<Path2Weight> p = localThis
                                .getGuidancMethodEditablePredicate();
                        return p.evaluate((Path2Weight) arg.getData());
                    }
                });
    }

    /**
     * Set up the context menu for the {@link Path2Weight} list.
     */
    private void setupNodeValueWeigherContextMenu() {
        final NavigationBarView localThis = this;

        setupContextMenu(nodeValueWeigherListCtrl,
                nodeValueWeigherTypeNames,
                onMakeNodeValueWeigherListeners,
                new Runnable() {
                    @Override
                    public void run() {
                        renameCurrentlySelectedNodeValueWeigher();
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        onDeleteNodeValueWeigher(getSelectedNodeValueWeigher());
                    }
                },
                new UnaryPredicate<TreeItem>() {
                    @Override
                    public boolean evaluate(TreeItem arg) {
                        UnaryPredicate<NodeValue2Weight> p = localThis
                                .getNodeValueWeigherPredicate();
                        return p.evaluate((NodeValue2Weight) arg.getData());
                    }
                });
    }

    /**
     * Set up a navbar context menu within the given SWT {@link Tree}.
     * 
     * @param context
     *            The {@link Tree} where the context menu should be established.
     * @param newMenuItems
     *            A {@link List} of strings containing the items of the "New"
     *            sub-menu.
     * @param newMenuListeners
     *            A {@link Set} of {@link GenericListener} instances getting
     *            called when an item within the "New" sub-menu is selected.
     * @param renameListener
     *            A {@link GenericListener} getting called when the "Rename"
     *            menu item is selected.
     * @param deleteListener
     *            A {@link GenericListener} getting called when the "Delete"
     *            menu item is selected.
     * @param itemEditablePred
     *            A {@link UnaryPredicate} indicating whether a given
     *            {@link TreeItem} is editable.
     * @return The new context menu's {@link Menu} instance.
     */
    private Menu setupContextMenu(final Tree context,
            final List<String> newMenuItems,
            final Set<GenericListener<String>> newMenuListeners,
            final Runnable renameListener,
            final Runnable deleteListener,
            final UnaryPredicate<TreeItem> itemEditablePred) {
        Menu contextMenu = new Menu(context);
        context.setMenu(contextMenu);

        final MenuItem newMI = new MenuItem(contextMenu, SWT.CASCADE);
        newMI.setText("New");

        @SuppressWarnings("unused")
        final MenuItem separator = new MenuItem(contextMenu, SWT.SEPARATOR);

        final MenuItem renameMI = new MenuItem(contextMenu, SWT.PUSH);
        renameMI.setText("Rename...");
        renameMI.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                renameListener.run();
            }
        });

        final MenuItem deleteMI = new MenuItem(contextMenu, SWT.PUSH);
        deleteMI.setText("Delete");
        deleteMI.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                deleteListener.run();
            }
        });

        contextMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuShown(MenuEvent arg0) {
                TreeItem[] selection = context.getSelection();
                boolean modifiable = selection.length > 0;
                if (modifiable) {
                    modifiable &= itemEditablePred.evaluate(selection[0]);
                }
                renameMI.setEnabled(modifiable);
                deleteMI.setEnabled(modifiable);

                // Construct "new"'s submenu
                Menu newSubMenu = new Menu(newMI);
                newMI.setMenu(newSubMenu);

                for (final String typeName : newMenuItems) {
                    MenuItem subMI = new MenuItem(newSubMenu, SWT.PUSH);
                    subMI.setText(typeName);
                    subMI.addSelectionListener(new SelectionAdapter() {
                        @Override
                        public void widgetSelected(SelectionEvent ev) {
                            for (GenericListener<String> gl : newMenuListeners) {
                                gl.onFiredEvent(typeName);
                            }
                        }
                    });
                }
            }
        });

        return contextMenu;
    }

    /**
     * @return The current {@link ViewMode}, i.e. the state of the tab folder.
     */
    public ViewMode getViewMode() {
        if (register.getSelection() != null) {
            return (ViewMode) register.getSelection().getData();
        }
        return null;
    }

    /**
     * @return The currently selected {@link Path2Weight} (if any) or
     *         <tt>null</tt> if the current {@link Path2Weight} selection is empty.
     */
    public Path2Weight getSelectedTreeTraceWeigher() {
        TreeItem[] selection = treeTraceWeigherListCtrl.getSelection();
        if (selection.length > 0) {
            return (Path2Weight) selection[0].getData();
        }
        else {
            return null;
        }
    }

    /**
     * @return The currently selected {@link NodeValue2Weight} (if any) or
     *         <tt>null</tt> if the current node value weigher selection is
     *         empty.
     */
    public NodeValue2Weight getSelectedNodeValueWeigher() {
        TreeItem[] selection = nodeValueWeigherListCtrl.getSelection();
        if (selection.length > 0) {
            return (NodeValue2Weight) selection[0].getData();
        }
        else {
            return null;
        }
    }

    /**
     * Construct a new {@link NavigationBarView} instance. Initially, the lists
     * of {@link Path2Weight}s and node weighers are empty.
     * 
     * @param parent
     *            The instance's parent widget.
     * @param initialViewMode
     *            The initial register selection.
     * @return The new {@link NavigationBarView} instance.
     */
    public static NavigationBarView newInstance(Composite parent, ViewMode initialViewMode) {
        return new NavigationBarView(parent, initialViewMode);
    }
}
