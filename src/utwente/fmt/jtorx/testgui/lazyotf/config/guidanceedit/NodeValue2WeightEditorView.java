package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * A view for editing {@link NodeValue2Weight} instances, behaving like an SWT
 * {@link Composite}. This class is intended to be sub-classed; To add further
 * controls to the view, use the <tt>customControl</tt> {@link Composite}.
 */
class NodeValue2WeightEditorView extends Composite {
    /** The display name editing control. */
    private final Text nameCtrl;

    /**
     * The set of listeners getting called when the user requests a modification
     * of the edited {@link NodeValue2Weight}'s display name.
     */
    private final Set<GenericListener<String>> onNameChangedListeners;

    /** The group containing the controls governed by this view. */
    private final Group drawingArea;

    /** The composite available for the addition of further controls. */
    private final Composite customControl;

    /**
     * Construct a new {@link NodeValue2WeightEditorView} instance.
     * 
     * @param parent
     *            The instance's parent {@link Composite} control.
     */
    public NodeValue2WeightEditorView(Composite parent) {
        super(parent, SWT.NONE);

        GridLayout mainLayout = new GridLayout();
        mainLayout.numColumns = 1;
        mainLayout.marginHeight = 0;
        mainLayout.marginWidth = 0;
        setLayout(mainLayout);

        GridLayout twoColLayout = new GridLayout();
        twoColLayout.numColumns = 2;

        Group nameGrp = new Group(this, SWT.NONE);
        nameGrp.setText("Basic Settings");
        nameGrp.setLayout(twoColLayout);
        nameGrp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Label nameLabel = new Label(nameGrp, SWT.NONE);
        nameLabel.setText("Name:");
        nameCtrl = new Text(nameGrp, SWT.NONE);
        nameCtrl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        customControl = new Composite(this, SWT.NONE);
        customControl.setLayoutData(new GridData(GridData.FILL_BOTH));

        onNameChangedListeners = new HashSet<>();
        drawingArea = nameGrp;

        setupListeners();

        parent.layout();
        this.layout();
        nameGrp.layout();
    }

    /**
     * Add a listener getting called whenever the user requests a display name
     * modification.
     * 
     * @param listener
     *            A {@link GenericListener} taking the requested new name as an
     *            argument to its <tt>onFiredEvent</tt> method.
     */
    public void addNameChangedListener(GenericListener<String> listener) {
        if (listener == null) {
            throw new IllegalArgumentException("listener must not be null");
        }
        onNameChangedListeners.add(listener);
    }

    /**
     * Set up the listeners gluing the externally provided ones to the view's
     * controls.
     */
    private void setupListeners() {
        nameCtrl.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent arg0) {
            }

            @Override
            public void focusLost(FocusEvent arg0) {
                for (GenericListener<String> listener : onNameChangedListeners) {
                    listener.onFiredEvent(nameCtrl.getText());
                }
            }
        });

        Listener textCtrlListener = new Listener() {
            @Override
            public void handleEvent(Event ev) {
                switch (ev.detail) {
                case SWT.TRAVERSE_RETURN:
                    for (GenericListener<String> listener : onNameChangedListeners) {
                        listener.onFiredEvent(nameCtrl.getText());
                    }
                    break;
                default:
                    break;
                }
            }
        };

        nameCtrl.addListener(SWT.Traverse, textCtrlListener);
    }

    /**
     * @return The {@link Composite} containing the "Basic Settings"
     *         {@link Group} control.
     */
    protected Composite getBasicSettingsControl() {
        return drawingArea;
    }

    /**
     * @return The {@link Composite} which can be used to add further controls
     *         to the view.
     */
    protected Composite getCustomControl() {
        return customControl;
    }

    /**
     * @return The display name currently set within the editor.
     */
    public String getDisplayName() {
        return nameCtrl.getText();
    }

    /**
     * Set the currently displayed display name.
     * 
     * @param name
     *            The new display name.
     */
    public void setDisplayName(String name) {
        nameCtrl.setText(name);
    }

    @Override
    public void setEnabled(boolean enabledness) {
        super.setEnabled(enabledness);
        nameCtrl.setEnabled(enabledness);
    }
}
