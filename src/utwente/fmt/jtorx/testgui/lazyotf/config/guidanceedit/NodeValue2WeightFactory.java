package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.ArrayList;
import java.util.List;

import utwente.fmt.jtorx.lazyotf.guidance.LocationValuation2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Locations2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.SuperLocationSize2Weight;
import utwente.fmt.jtorx.utils.Pair;

/**
 * A GUI-specific factory class for {@link NodeValue2Weight} instances.
 */
final class NodeValue2WeightFactory {
    /** The display name prefix of newly instantiated node value weighers. */
    public static final String DEFAULT_NAME_PREFIX = "New ";

    /**
     * Make a new {@link NodeValue2Weight} instance.
     * 
     * @param name
     *            The new instance's type name. The type name must be present in
     *            the list provided by <tt>getNodeValueWeightNames</tt>.
     * @return The new instance, configured with default values.
     */
    public static NodeValue2Weight makeNodeValue2Weight(String name) {
        if (name.equals("Locations2Weight")) {
            return new Locations2Weight(DEFAULT_NAME_PREFIX + "Locations2Weight",
                    1, 10, 100);
        }
        else if (name.equals("LocationValuation2Weight")) {
            return new LocationValuation2Weight(DEFAULT_NAME_PREFIX
                    + "LocationValuation2Weight", new ArrayList<Pair<String, String>>());
        }
        else if (name.equals("SuperLocationSize2Weight")) {
            return new SuperLocationSize2Weight(DEFAULT_NAME_PREFIX
                    + "SuperLocationSize2Weight", 0);
        }
        else {
            throw new IllegalArgumentException("Unknown node value weigher type "
                    + name);
        }
    }

    /**
     * @return A {@link List} of {@link NodeValue2Weight} instances which can be
     *         made using this factory.
     */
    public static List<String> getNodeValueWeightNames() {
        List<String> result = new ArrayList<>();
        result.add("Locations2Weight");
        result.add("LocationValuation2Weight");
        result.add("SuperLocationSize2Weight");
        return result;
    }

    /** Utility class - cannot be instantiated. */
    private NodeValue2WeightFactory() {

    }
}
