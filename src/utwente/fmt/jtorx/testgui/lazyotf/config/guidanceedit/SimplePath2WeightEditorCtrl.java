package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.ArrayList;

import utwente.fmt.jtorx.lazyotf.guidance.SimplePath2Weight;
import utwente.fmt.jtorx.lazyotf.policy.Defaults;

/**
 * A controller class for the {@link SimplePath2Weight} editor.
 */
class SimplePath2WeightEditorCtrl extends EditorController {
    /** The edited {@link SimplePath2Weight} instance. */
    private final SimplePath2Weight subject;

    /** The view associated with this controller. */
    private final GenericPath2WeightEditorView view;

    /** The main controller. */
    private final WeightsEditorImpl mainCtrl;

    /**
     * Construct a new {@link SimplePath2WeightEditorCtrl} instance.
     * 
     * @param subject
     *            The edited {@link SimplePath2Weight} instance.
     * @param view
     *            The {@link GenericGuidanceMethosEditorView} to be governed by
     *            this controller.
     * @param mainCtrl
     *            The main controller.
     * @param readOnly
     *            Iff true, <tt>subject</tt> will not be modified by this
     *            controller.
     */
    public SimplePath2WeightEditorCtrl(SimplePath2Weight subject,
            GenericPath2WeightEditorView view, WeightsEditorImpl mainCtrl, boolean readOnly) {
        this.subject = subject;
        this.view = view;
        this.mainCtrl = mainCtrl;

        view.setDisplayNameEnabled(!readOnly);
        view.setImplementationComboEnabled(false);
        view.setNodeValueWeigherComboEnabled(false);
        view.setNodeValueWeighers(mainCtrl.getOrderedModifiedNodeValue2Weights());
        view.setNodeWeigher(Defaults.defaultNodeValue2Weight());

        /* dummy */
        ArrayList<String> implTypes = new ArrayList<>();
        implTypes.add(subject.getName());

        view.setImplementationNames(implTypes);

        view.setDisplayName(subject.getDisplayName());
        view.setImplementationName(subject.getName());

        setupListeners();
    }

    @Override
    public void update() {
        view.setDisplayName(subject.getDisplayName());
        view.setImplementationName(subject.getName());
    }

    /**
     * Set up the listener glue.
     */
    private void setupListeners() {
        view.addDisplayNameChangeListener(mainCtrl.makeDefaultRenameListener(subject));
    }
}
