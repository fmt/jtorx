package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import utwente.fmt.jtorx.lazyotf.guidance.SuperLocationSize2Weight;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

public class SuperLocationSize2WeightEditorCtrl extends EditorController {
    /** The view instance associated with this controller. */
    private final SuperLocationSize2WeightEditorView view;

    /** The main controller. */
    private final WeightsEditorImpl mainCtrl;

    /** The edited {@link Locations2Weight} instance. */
    private final SuperLocationSize2Weight subject;

    public SuperLocationSize2WeightEditorCtrl(SuperLocationSize2Weight subject,
            SuperLocationSize2WeightEditorView view,
            WeightsEditorImpl mainCtrl, boolean readOnly) {
        super();

        this.view = view;
        this.mainCtrl = mainCtrl;
        this.subject = subject;

        view.setEnabled(!readOnly);
        view.setDisplayName(subject.getDisplayName());
        view.setCoefficient(subject.getCoefficient());
        view.setUseDefaultCoefficient(subject.isUsingSystemDefaultCoefficient());

        setupListeners();
    }

    private void setupListeners() {
        view.addNameChangedListener(mainCtrl.makeDefaultRenameListener(subject));

        view.addChangedCoefficientListener(new GenericListener<Integer>() {
            @Override
            public void onFiredEvent(Integer coefficient) {
                subject.setCoefficient(coefficient);
            }
        });

        view.addChangedUseDefaultCoefficientListener(new GenericListener<Boolean>() {
            @Override
            public void onFiredEvent(Boolean useDefault) {
                subject.setUsingSystemDefaultCoefficient(useDefault);
            }
        });
    }

    @Override
    public void update() {
        view.setDisplayName(subject.getDisplayName());
        view.setCoefficient(subject.getCoefficient());
        view.setUseDefaultCoefficient(subject.isUsingSystemDefaultCoefficient());
    }

}
