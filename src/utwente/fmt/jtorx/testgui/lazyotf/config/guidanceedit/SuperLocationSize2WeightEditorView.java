package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

public class SuperLocationSize2WeightEditorView extends NodeValue2WeightEditorView {
    private final Button useDefaultCoeffCheckBox;
    private final Spinner coefficientSpinner;
    private final Set<GenericListener<Integer>> onChangedCoefficientListeners;
    private final Set<GenericListener<Boolean>> onChangedUseDefaultListeners;

    public SuperLocationSize2WeightEditorView(Composite parent) {
        super(parent);

        Composite ourDrawingArea = getCustomControl();
        ourDrawingArea.setLayout(new FillLayout());

        GridLayout twoColLayout = new GridLayout();
        twoColLayout.numColumns = 2;

        Group weightGrp = new Group(ourDrawingArea, SWT.NONE);
        weightGrp.setText("Weight coefficient");
        weightGrp.setLayout(twoColLayout);

        Label spinnerLabel = new Label(weightGrp, SWT.NONE);
        spinnerLabel.setText("Weight coefficient: ");
        coefficientSpinner = new Spinner(weightGrp, SWT.NONE);
        coefficientSpinner.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        coefficientSpinner.setMaximum(Integer.MAX_VALUE);
        coefficientSpinner.setMinimum(0);

        GridData checkLD = new GridData();
        checkLD.horizontalSpan = 2;

        useDefaultCoeffCheckBox = new Button(weightGrp, SWT.CHECK);
        useDefaultCoeffCheckBox.setSelection(false);
        useDefaultCoeffCheckBox.setLayoutData(checkLD);
        useDefaultCoeffCheckBox.setText("Use default weight coefficient");

        useDefaultCoeffCheckBox.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onChangedUseDefaultCoefficient();
                coefficientSpinner.setEnabled(!isUsingDefaultCoefficient());
            }
        });

        coefficientSpinner.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onChangedCoefficient();
            }
        });

        onChangedCoefficientListeners = new HashSet<>();
        onChangedUseDefaultListeners = new HashSet<>();

        ourDrawingArea.layout();
        weightGrp.layout();
    }

    public void setCoefficient(int coefficient) {
        coefficientSpinner.setSelection(coefficient);
    }

    public int getCoefficient() {
        return coefficientSpinner.getSelection();
    }

    public void setUseDefaultCoefficient(boolean useDefault) {
        useDefaultCoeffCheckBox.setSelection(useDefault);
        coefficientSpinner.setEnabled(!useDefault);
    }

    public boolean isUsingDefaultCoefficient() {
        return useDefaultCoeffCheckBox.getSelection();
    }

    public void addChangedCoefficientListener(GenericListener<Integer> it) {
        onChangedCoefficientListeners.add(it);
    }

    public void addChangedUseDefaultCoefficientListener(GenericListener<Boolean> it) {
        onChangedUseDefaultListeners.add(it);
    }

    private void onChangedCoefficient() {
        for (GenericListener<Integer> listener : onChangedCoefficientListeners) {
            listener.onFiredEvent(coefficientSpinner.getSelection());
        }
    }

    private void onChangedUseDefaultCoefficient() {
        for (GenericListener<Boolean> listener : onChangedUseDefaultListeners) {
            listener.onFiredEvent(useDefaultCoeffCheckBox.getSelection());
        }
    }

    @Override
    public void setEnabled(boolean enabledness) {
        useDefaultCoeffCheckBox.setEnabled(enabledness);
        if (enabledness == true) {
            coefficientSpinner.setEnabled(!useDefaultCoeffCheckBox.getSelection());
        }
        else {
            coefficientSpinner.setEnabled(false);
        }
    }
}
