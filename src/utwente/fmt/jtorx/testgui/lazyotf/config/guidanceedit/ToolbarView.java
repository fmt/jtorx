package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * A view class managing the guidance method editor's toolbar and the associated
 * listeners.
 */
class ToolbarView extends Composite {
    /** The tool bar control. */
    private final ToolBar toolbar;

    /** The "New" tool bar item. */
    private final ToolItem newItem;

    /** The "Rename" tool bar item. */
    private final ToolItem renameItem;

    /** The "Delete" tool bar item. */
    private final ToolItem deleteItem;

    /** The set of listeners for the "New" tool bar menu. */
    private final Set<GenericListener<String>> newItemListeners;

    /** The "New" tool bar drop-down menu. */
    private final Menu newItemDropdown;

    /**
     * Make an new toolbar view.
     * 
     * @param parent
     *            The view's parent control.
     */
    ToolbarView(Composite parent) {
        super(parent, SWT.NONE);
        setLayout(new FillLayout());
        toolbar = new ToolBar(this, SWT.FLAT | SWT.WRAP | SWT.RIGHT);

        newItem = new ToolItem(toolbar, SWT.DROP_DOWN);
        newItem.setText("New");
        newItemDropdown = new Menu(newItem.getParent().getShell());
        newItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                Rectangle tbNewRect = newItem.getBounds();
                Point pt = newItem.getParent().toDisplay(new Point(tbNewRect.x, tbNewRect.y));
                newItemDropdown.setLocation(pt.x, pt.y + tbNewRect.height);
                newItemDropdown.setVisible(true);
            }
        });

        renameItem = new ToolItem(toolbar, SWT.PUSH);
        renameItem.setText("Rename");
        deleteItem = new ToolItem(toolbar, SWT.PUSH);
        deleteItem.setText("Delete");
        newItemListeners = new HashSet<>();
    }

    /**
     * Set the enabledness of the "Rename" button.
     * 
     * @param enabledness
     *            The new enabledness of the "Rename" button.
     */
    public void setRenameEnabled(boolean enabledness) {
        renameItem.setEnabled(enabledness);
    }

    /**
     * Set the enabledness of the "Delete" button.
     * 
     * @param enabledness
     *            The new enabledness of the "Delete" button.
     */
    public void setDeleteEnabled(boolean enabledness) {
        deleteItem.setEnabled(enabledness);
    }

    /**
     * Set the list of items offered in the "New" drop-down menu.
     * 
     * @param names
     *            The described list of items.
     */
    public void setNewItemNameList(List<String> names) {
        for (MenuItem mi : newItemDropdown.getItems()) {
            mi.dispose();
        }

        for (final String name : names) {
            MenuItem newMI = new MenuItem(newItemDropdown, SWT.PUSH);
            newMI.setText(name);
            newMI.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent ev) {
                    onNewItemRequested(name);
                }
            });
        }
    }

    /**
     * The listener for selections within the "New" submenu.
     * 
     * @param name
     *            The selected menu item's name.
     */
    public void onNewItemRequested(String name) {
        for (GenericListener<String> gl : newItemListeners) {
            gl.onFiredEvent(name);
        }
    }

    /**
     * Add a listener for "make new item"-type user requests.
     * 
     * @param it
     *            The new listener, taking a String argument from the list of
     *            strings previously supplied by a call to
     *            <tt>setNewItemNameList</tt>.
     */
    public void addNewItemListener(GenericListener<String> it) {
        newItemListeners.add(it);
    }

    /**
     * Add a listener getting called when the user hit the "Rename" button.
     * 
     * @param it
     *            A {@link Runnable} listener.
     */
    public void addRenameItemListener(final Runnable it) {
        renameItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                it.run();
            }
        });
    }

    /**
     * Add a listener getting called when the user hit the "Delete" button.
     * 
     * @param it
     *            A {@link GenericListener}.
     */
    public void addDeleteItemListener(final Runnable it) {
        deleteItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                it.run();
            }
        });
    }
}
