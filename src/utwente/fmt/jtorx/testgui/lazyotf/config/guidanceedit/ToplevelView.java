package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit.NavigationBarView.ViewMode;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.SashLayout;

/**
 * The editor's main view. This view contains the toolbar and the composites
 * containing the selection tree and the main editor area. Inheriting from
 * Composite, this view behaves like an SWT widget.
 */
class ToplevelView extends Composite {
    /** The "main" editing area. */
    private final Composite lowerRightPart;

    /** The left-hand navigation bar view. */
    private final NavigationBarView navBarView;

    /** The tool bar. */
    private final ToolbarView toolBarView;

    /** The main editing area. */
    private final EditingAreaView editingAreaView;

    /**
     * Construct a new {@link ToplevelView} instance.
     * 
     * @param parent
     *            The control containing the view.
     */
    public ToplevelView(Composite parent) {
        super(parent, SWT.NONE);

        GridLayout outermostLayout = new GridLayout();
        outermostLayout.numColumns = 1;
        setLayout(outermostLayout);
        GridData fillGrid = new GridData(GridData.FILL_BOTH);

        // Make a toolbar
        toolBarView = new ToolbarView(this);

        // Enclose the lower part for a sash layout
        Composite lowerPart = new Composite(this, SWT.NONE);
        lowerPart.setLayoutData(fillGrid);

        // Install the left-hand editor part
        navBarView = NavigationBarView.newInstance(lowerPart, ViewMode.GUIDANCE_METHOD);

        // Install the right-hand editor part
        lowerRightPart = new Composite(lowerPart, SWT.NONE);
        lowerRightPart.setLayout(new FillLayout());
        editingAreaView = new EditingAreaView(lowerRightPart);

        // Install a vertical Sash layout
        SashLayout.setupVerticallySplitting(lowerPart, navBarView, lowerRightPart);
    }

    /**
     * @return The navigation bar view.
     */
    protected NavigationBarView getNavigationBarView() {
        return navBarView;
    }

    /**
     * @return The tool bar view.
     */
    protected ToolbarView getToolbarView() {
        return toolBarView;
    }

    /**
     * @return The editing area view.
     */
    protected EditingAreaView getEditingAreaView() {
        return editingAreaView;
    }

    public static class Factory {
        public ToplevelView makeInstance(Composite parent) {
            return new ToplevelView(parent);
        }
    }

}
