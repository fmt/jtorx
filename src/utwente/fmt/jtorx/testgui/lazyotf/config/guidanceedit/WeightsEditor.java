package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.Collection;

import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;

/**
 * The public interface of the weights editor. Modified {@link Path2Weight} and
 * node value weigher sets can be obtained after calling open().
 */
public interface WeightsEditor {
    /**
     * @return The edited set of {@link Path2Weight}s.
     */
    Collection<Path2Weight> getModifiedPath2WeightSet();

    /**
     * @return The edited set of node value weighers.
     */
    Collection<NodeValue2Weight> getModifiedNodeValue2Weights();

    /**
     * Display the weights editor window. This method returns when the window is
     * disposed.
     */
    void open();
}
