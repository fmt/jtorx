package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import utwente.fmt.jtorx.lazyotf.ExpressionValidator;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;

/**
 * A factory interface for {@link WeightsEditor}. {@link WeightsEditorImpl}
 * provides an implementation of this interface via <i>getFactory()</i>.
 */
public interface WeightsEditorFactory {
    /**
     * Make a new {@link WeightsEditor} instance.
     * 
     * @param sessionData
     *            The configuration session data object. This object does not
     *            get modified by the editor.
     * @param exprVal
     *            A condition/integer-expression validator.
     * @return A new {@link WeightsEditor} instance.
     */
    WeightsEditor makeWeightsEditor(LazyOTFConfigSessionData sessionData,
            ExpressionValidator exprVal);
}
