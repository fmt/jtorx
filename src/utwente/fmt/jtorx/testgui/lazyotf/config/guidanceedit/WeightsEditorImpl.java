package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.ExpressionValidator;
import utwente.fmt.jtorx.lazyotf.guidance.CustomPath2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.LocationValuation2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Locations2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2WeightVisitor;
import utwente.fmt.jtorx.lazyotf.guidance.SimplePath2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.SuperLocationSize2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2WeightVisitor;
import utwente.fmt.jtorx.lazyotf.policy.Defaults;
import utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit.LocationValuation2WeightEditorView.IdentifierAccessor;
import utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit.NavigationBarView.ViewMode;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.UnaryPredicate;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.RenameListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.SetEnabledListener;
import utwente.fmt.jtorx.utils.Pair;

/**
 * The weights editor. This is the control class.
 */
public class WeightsEditorImpl implements WeightsEditor {
    /** The class managing the editor's window. */
    private final WeightsEditorWindow window;

    /** The edited set of {@link Path2Weight}s. */
    private final Collection<Path2Weight> currentTreeTraceWeighers;

    /** The edited set of node value weighers. */
    private final Collection<NodeValue2Weight> nodeValue2Weights;

    /**
     * A map from instantiable custom {@link Path2Weight}s to their respective
     * template instances.
     */
    private final Map<String, Path2Weight> treeTraceWeigherTypeMap;

    /** A predicate deciding whether a given node value weigher can be edited. */
    private final UnaryPredicate<NodeValue2Weight> defaultPrecludingEditablePredicate;

    /** A predicate deciding whether a given {@link Path2Weight} can be edited. */
    private final UnaryPredicate<Path2Weight> defaultPrecludingTreeTraceWeigherEditablePredicate;

    /** The currently displayed editor view. */
    private EditorController currentEditorCtrl;

    /** The configuration session data. */
    private final LazyOTFConfigSessionData configSessionData;

    /** A condition/integer-expression validator. */
    private final ExpressionValidator expressionValidator;

    /**
     * Construct a new {@link WeightsEditor} instance.
     * 
     * @param sessionData
     *            The configuration's session data.
     * @param exprValidator
     *            A condition/integer-expression validator.
     */
    public WeightsEditorImpl(LazyOTFConfigSessionData sessionData,
            ExpressionValidator exprValidator) {
        configSessionData = sessionData;
        expressionValidator = exprValidator;

        treeTraceWeigherTypeMap = new LinkedHashMap<>();
        for (Path2Weight template : sessionData.getConfigOptions().configurablePath2Weights) {
            treeTraceWeigherTypeMap.put(template.getName(), template);
        }

        currentTreeTraceWeighers = new HashSet<>(sessionData.getPath2Weights());
        nodeValue2Weights = new HashSet<>(sessionData.getCustomNodeValue2Weights());

        // Make the default node value weigher uneditable since it is determined
        // by the implementation.
        defaultPrecludingEditablePredicate = new UnaryPredicate<NodeValue2Weight>() {
            @Override
            public boolean evaluate(NodeValue2Weight it) {
                return it != Defaults.defaultNodeValue2Weight();
            }
        };

        // Exclude all SimplePath2Weight instances from being edited
        // since
        // their purpose is to reflect system default {@link Path2Weight}s.
        defaultPrecludingTreeTraceWeigherEditablePredicate = new UnaryPredicate<Path2Weight>() {
            @Override
            public boolean evaluate(Path2Weight it) {
                return !(it instanceof SimplePath2Weight);
            }
        };

        window = new WeightsEditorWindow(this);
        currentEditorCtrl = EditorController.getNullElement();
    }

    @Override
    public void open() {
        window.display();
    }

    @Override
    public Collection<Path2Weight> getModifiedPath2WeightSet() {
        return currentTreeTraceWeighers;
    }

    /**
     * @return a condition/integer-expression validator.
     */
    protected ExpressionValidator getExpressionValidator() {
        return expressionValidator;
    }

    /**
     * @return The edited set of {@link Path2Weight}s, ordered alphabetically.
     */
    protected List<Path2Weight> getOrderedModifiedTreeTraceWeigherSet() {
        Path2Weight[] sortedGM = new Path2Weight[] {};
        sortedGM = currentTreeTraceWeighers.toArray(sortedGM);

        Arrays.sort(sortedGM, new Comparator<Path2Weight>() {
            @Override
            public int compare(Path2Weight arg0, Path2Weight arg1) {
                return arg0.getDisplayName().compareTo(arg1.getDisplayName());
            }
        });

        return Arrays.asList(sortedGM);
    }

    @Override
    public Collection<NodeValue2Weight> getModifiedNodeValue2Weights() {
        return nodeValue2Weights;
    }

    /**
     * @return The edited set of node value weighers, ordered alphabetically.
     */
    protected List<NodeValue2Weight> getOrderedModifiedNodeValue2Weights() {
        NodeValue2Weight[] sortedNV2W = new NodeValue2Weight[] {};
        sortedNV2W = nodeValue2Weights.toArray(sortedNV2W);

        Arrays.sort(sortedNV2W, new Comparator<NodeValue2Weight>() {

            @Override
            public int compare(NodeValue2Weight arg0, NodeValue2Weight arg1) {
                return arg0.getDisplayName().compareTo(arg1.getDisplayName());
            }
        });

        return Arrays.asList(sortedNV2W);
    }

    protected void onFinishedViewSetup() {
        List<String> tt2wTypeList = new ArrayList<>(treeTraceWeigherTypeMap.keySet());

        NavigationBarView nbv = window.getView().getNavigationBarView();
        nbv.setPath2Weights(getOrderedModifiedTreeTraceWeigherSet(), false,
                configSessionData.getEnabledPath2Weights());
        nbv.setNodeValueWeighers(getOrderedModifiedNodeValue2Weights());
        nbv.setTreeTraceWeigherTypeNames(tt2wTypeList);
        nbv.setNodeValueWeigherTypeNames(NodeValue2WeightFactory.getNodeValueWeightNames());

        nbv.setNodeValueWeigherEditablePredicate(defaultPrecludingEditablePredicate);
        nbv.setTreeTraceWeigherEditablePredicate(defaultPrecludingTreeTraceWeigherEditablePredicate);

        window.getView().getToolbarView().setNewItemNameList(tt2wTypeList);

        glueToolbarToNavbar();
        setupSelectionListeners();
        setupToolbarListeners();
        setupNavbarListeners();
    }

    /**
     * Set up the listeners observing the view for changes in the guidance
     * method and node value weigher selection within the navigation bar.
     */
    private void setupSelectionListeners() {
        final NavigationBarView nbv = window.getView().getNavigationBarView();

        nbv.addTreeTraceWeigherSelectionListener(new GenericListener<Path2Weight>() {
            @Override
            public void onFiredEvent(Path2Weight gm) {
                if (gm != null) {
                    displayEditor(gm);
                }
                else {
                    window.getView().getEditingAreaView().setDefaultEditor(true);
                }
            }
        });

        nbv.addNodeValueWeigherSelectionListener(new GenericListener<NodeValue2Weight>() {
            @Override
            public void onFiredEvent(NodeValue2Weight nv) {
                if (nv != null) {
                    displayEditor(nv);
                }
                else {
                    window.getView().getEditingAreaView().setDefaultEditor(true);
                }
            }
        });
    }

    /**
     * Set up the listeners observing the tool bar for the respective edit
     * requests.
     */
    private void setupToolbarListeners() {
        final NavigationBarView nbv = window.getView().getNavigationBarView();
        final ToolbarView tbv = window.getView().getToolbarView();

        tbv.addRenameItemListener(new Runnable() {
            @Override
            public void run() {
                switch (nbv.getViewMode()) {
                case GUIDANCE_METHOD:
                    nbv.renameCurrentlySelectedPath2Weight();
                    break;
                case NODE_VALUE_WEIGHER:
                    nbv.renameCurrentlySelectedNodeValueWeigher();
                    break;
                default:
                    break;
                }
            }
        });

        tbv.addDeleteItemListener(new Runnable() {
            @Override
            public void run() {
                switch (nbv.getViewMode()) {
                case GUIDANCE_METHOD:
                    onDeleteRequested(nbv.getSelectedTreeTraceWeigher());
                    break;
                case NODE_VALUE_WEIGHER:
                    onDeleteRequested(nbv.getSelectedNodeValueWeigher());
                    break;
                default:
                    break;
                }
            }
        });

        tbv.addNewItemListener(new GenericListener<String>() {
            @Override
            public void onFiredEvent(String typeArg) {
                switch (nbv.getViewMode()) {
                case GUIDANCE_METHOD:
                    onMakeNewTreeTraceWeigher(typeArg);
                    break;
                case NODE_VALUE_WEIGHER:
                    onMakeNewNodeValue2Weight(typeArg);
                    break;
                default:
                    break;
                }
            }
        });
    }

    /**
     * Set up the listeners observing the navigation bar's context menu for edit
     * requests.
     */
    private void setupNavbarListeners() {
        final NavigationBarView nbv = window.getView().getNavigationBarView();

        nbv.addRenameTreeTraceWeigherListener(new RenameListener<Path2Weight>() {
            @Override
            public void onRename(Path2Weight instance, String newName) {
                onRenameRequested(instance, newName);
            }
        });

        nbv.addRenameNodeValueWeigherListener(new RenameListener<NodeValue2Weight>() {
            @Override
            public void onRename(NodeValue2Weight instance, String newName) {
                onRenameRequested(instance, newName);
            }
        });

        nbv.addDeleteTreeTraceWeigherListener(new GenericListener<Path2Weight>() {
            @Override
            public void onFiredEvent(Path2Weight arg) {
                onDeleteRequested(arg);
            }
        });

        nbv.addDeleteNodeValueWeigherListener(new GenericListener<NodeValue2Weight>() {
            @Override
            public void onFiredEvent(NodeValue2Weight arg) {
                onDeleteRequested(arg);
            }
        });

        nbv.addMakeTreeTraceWeigherListener(new GenericListener<String>() {
            @Override
            public void onFiredEvent(String typeArg) {
                onMakeNewTreeTraceWeigher(typeArg);
            }
        });

        nbv.addMakeNodeValueWeigherListener(new GenericListener<String>() {
            @Override
            public void onFiredEvent(String typeArg) {
                onMakeNewNodeValue2Weight(typeArg);
            }
        });

        nbv.addTreeTraceWeigherEnabledListener(new SetEnabledListener<Path2Weight>() {
            @Override
            public void onSetEnabled(Path2Weight instance, boolean newValue) {
                onSetTreeTraceWeigherEnabled(instance, newValue);
            }
        });
    }

    /**
     * Handle {@link Path2Weight} display name change requests.
     * 
     * @param gm
     *            The {@link Path2Weight} instance whose display name
     *            is to be changed.
     * @param newName
     *            The requested new display name.
     */
    protected void onRenameRequested(Path2Weight gm, String newName) {
        if (!defaultPrecludingTreeTraceWeigherEditablePredicate.evaluate(gm)) {
            return;
        }

        gm.setDisplayName(newName);
        currentEditorCtrl.update();
        redraw();
    }

    /**
     * Handle node value weigher display name change requests.
     * 
     * @param nv
     *            The {@link NodeValue2Weight} instance whose display name is to
     *            be changed.
     * @param newName
     *            The requested new display name.
     */
    protected void onRenameRequested(NodeValue2Weight nv, String newName) {
        if (!defaultPrecludingEditablePredicate.evaluate(nv)) {
            return;
        }
        nv.setDisplayName(newName);
        currentEditorCtrl.update();
        redraw();
    }

    /**
     * Handle {@link Path2Weight} deletion requests.
     * 
     * @param gm
     *            The {@link Path2Weight} to be deleted.
     */
    protected void onDeleteRequested(Path2Weight gm) {
        if (!defaultPrecludingTreeTraceWeigherEditablePredicate.evaluate(gm)) {
            return;
        }

        currentTreeTraceWeighers.remove(gm);
        configSessionData.getEnabledPath2Weights().remove(gm);
        NavigationBarView nbv = window.getView().getNavigationBarView();
        nbv.setPath2Weights(new ArrayList<>(currentTreeTraceWeighers), true,
                configSessionData.getEnabledPath2Weights());

        redraw();

        if (nbv.getViewMode() == ViewMode.GUIDANCE_METHOD) {
            if (nbv.getSelectedTreeTraceWeigher() != null) {
                displayEditor(nbv.getSelectedTreeTraceWeigher());
            }
            else {
                window.getView().getEditingAreaView().setDefaultEditor(true);
            }
        }
    }

    /**
     * Handle node value weigher deletion requests.
     * 
     * @param nv
     *            The {@link NodeValue2Weight} to be deleted. If <tt>nv</tt> is
     *            currently in use by any {@link Path2Weight}s, the user is asked
     *            whether to cancel the operation or to proceed and replace the
     *            affected {@link Path2Weight}'s node value weighers by the default
     *            node value weiger.
     */
    protected void onDeleteRequested(final NodeValue2Weight nv) {
        if (!defaultPrecludingEditablePredicate.evaluate(nv)) {
            return;
        }

        final Set<Path2Weight> affectedPath2Weights = new HashSet<>();
        for (Path2Weight gm : currentTreeTraceWeighers) {
            gm.accept(new Path2WeightVisitor.Adapter() {
                @Override
                public void visit(CustomPath2Weight it) {
                    if (it.getNodeValue2Weight() == nv) {
                        affectedPath2Weights.add(it);
                    }
                }
            });
        }

        if (!affectedPath2Weights.isEmpty()) {
            String affectedList = "";
            for (Path2Weight gm : affectedPath2Weights) {
                affectedList += "- " + gm.getDisplayName() + "\n";
            }

            boolean confirmDelete = window
                    .displayOkCancelMessageBox(
                            "The selected node value weigher is currently used by the following Path2Weights:\n\n"
                                    + affectedList
                                    + "\nIf you choose to delete the node value weigher anyway, the affected Path2Weights' node value weighers are set to the default.",
                            "Confirm node value weigher deletion");

            if (!confirmDelete) {
                return;
            }
            else {
                // Set the affected GM's node value weighers to default
                for (Path2Weight gm : affectedPath2Weights) {
                    gm.accept(new Path2WeightVisitor.Adapter() {
                        @Override
                        public void visit(CustomPath2Weight it) {
                            it.setNodeValue2Weight(Defaults.defaultNodeValue2Weight());
                        }
                    });
                }
            }
        }

        nodeValue2Weights.remove(nv);
        NavigationBarView nbv = window.getView().getNavigationBarView();
        nbv.setNodeValueWeighers(new ArrayList<>(nodeValue2Weights));

        redraw();

        if (nbv.getViewMode() == ViewMode.NODE_VALUE_WEIGHER) {
            if (nbv.getSelectedNodeValueWeigher() != null) {
                displayEditor(nbv.getSelectedNodeValueWeigher());
            }
            else {
                window.getView().getEditingAreaView().setDefaultEditor(true);
            }
        }
    }

    /**
     * Handle requests for making new custom {@link Path2Weight}s.
     * 
     * @param type
     *            The name of the new {@link Path2Weight}'s type.
     */
    protected void onMakeNewTreeTraceWeigher(String type) {
        Path2Weight template = treeTraceWeigherTypeMap.get(type);
        if (template == null) {
            throw new IllegalStateException(
                    "Requested to make Path2Weight of unknown implementation type " + type);
        }
        Path2Weight newTT2W = template.clone();
        newTT2W.setDisplayName("New Path2Weight (" + type + ")");
        currentTreeTraceWeighers.add(newTT2W);
        window.getView()
                .getNavigationBarView()
                .setPath2Weights(new ArrayList<>(currentTreeTraceWeighers), true,
                        configSessionData.getEnabledPath2Weights());
    }

    /**
     * Handle requests for making new custom node value weighers.
     * 
     * @param type
     *            The name of the new node value weigher's type.
     */
    protected void onMakeNewNodeValue2Weight(String type) {
        NodeValue2Weight newNV2W = NodeValue2WeightFactory.makeNodeValue2Weight(type);
        nodeValue2Weights.add(newNV2W);
        window.getView().getNavigationBarView()
                .setNodeValueWeighers(new ArrayList<>(nodeValue2Weights));
    }

    /**
     * Handle requests for setting the enabledness of a {@link Path2Weight}.
     * 
     * @param gm
     *            The tree trace weigher to be modified.
     * @param enabledness
     *            {@code true} iff {@code gm} should be enabled.
     */
    protected void onSetTreeTraceWeigherEnabled(Path2Weight gm, boolean enabledness) {
        if (!enabledness) {
            configSessionData.getEnabledPath2Weights().remove(gm);
        }
        else {
            configSessionData.getEnabledPath2Weights().add(gm);
        }
    }

    /**
     * Redraw the weights editor.
     */
    private void redraw() {
        final NavigationBarView nbv = window.getView().getNavigationBarView();
        final ToolbarView tbv = window.getView().getToolbarView();

        boolean initialModEnabled = false;
        initialModEnabled |= nbv.getViewMode() == ViewMode.GUIDANCE_METHOD
                && nbv.getSelectedTreeTraceWeigher() != null
                && defaultPrecludingTreeTraceWeigherEditablePredicate.evaluate(nbv
                        .getSelectedTreeTraceWeigher());
        initialModEnabled |= nbv.getViewMode() == ViewMode.NODE_VALUE_WEIGHER
                && (nbv.getSelectedNodeValueWeigher() != null)
                && defaultPrecludingEditablePredicate.evaluate(nbv.getSelectedNodeValueWeigher());
        tbv.setRenameEnabled(initialModEnabled);
        tbv.setDeleteEnabled(initialModEnabled);

        if (!initialModEnabled) {
            window.getView().getEditingAreaView().setDefaultEditor(true);
        }

        nbv.redraw();
        tbv.redraw();

        window.getView().getEditingAreaView().redraw();
    }

    /**
     * Install navigation bar listeners to keep the tool bar up to date with the
     * {@link Path2Weight} rsp. node value weigher selection within the navigation
     * bar, i.e. updates the "New" menu and enables/disables the "Rename" and
     * "Delete" buttons.
     */
    private void glueToolbarToNavbar() {
        final NavigationBarView nbv = window.getView().getNavigationBarView();
        final ToolbarView tbv = window.getView().getToolbarView();

        nbv.addDisplayModeListener(new GenericListener<NavigationBarView.ViewMode>() {

            @Override
            public void onFiredEvent(ViewMode newMode) {
                switch (newMode) {
                case GUIDANCE_METHOD:
                    tbv.setNewItemNameList(new ArrayList<>(treeTraceWeigherTypeMap.keySet()));
                    break;
                case NODE_VALUE_WEIGHER:
                    tbv.setNewItemNameList(NodeValue2WeightFactory.getNodeValueWeightNames());
                    break;
                default:
                    break;
                }

            }
        });

        nbv.addTreeTraceWeigherSelectionListener(new GenericListener<Path2Weight>() {
            @Override
            public void onFiredEvent(Path2Weight event) {
                boolean doEnable = event != null
                        && defaultPrecludingTreeTraceWeigherEditablePredicate.evaluate(event);
                tbv.setRenameEnabled(doEnable);
                tbv.setDeleteEnabled(doEnable);
            }
        });

        nbv.addNodeValueWeigherSelectionListener(new GenericListener<NodeValue2Weight>() {

            @Override
            public void onFiredEvent(NodeValue2Weight event) {
                boolean doEnable = event != null
                        && defaultPrecludingEditablePredicate.evaluate(event);
                tbv.setRenameEnabled(doEnable);
                tbv.setDeleteEnabled(doEnable);
            }
        });

        redraw();
    }

    /**
     * Set the current {@link EditorController} instance in order to keep track
     * of the displayed {@link Path2Weight} rsp. node value weigher editor.
     * 
     * @param it
     *            The new current {@link EditorController}
     */
    private void setCurrentEditorController(EditorController it) {
        currentEditorCtrl = it;
    }

    /**
     * Make a listener calling onRenameRequested for {@link Path2Weight}s.
     * 
     * @param subject
     *            The edited {@link Path2Weight}.
     * @return A {@link GenericListener} instance as specified.
     */
    protected GenericListener<String> makeDefaultRenameListener(final Path2Weight subject) {
        return new GenericListener<String>() {
            @Override
            public void onFiredEvent(String newName) {
                onRenameRequested(subject, newName);
            }
        };
    }

    /**
     * Make a listener calling onRenameRequested for node value weighers.
     * 
     * @param subject
     *            The edited node value weigher.
     * @return A {@link GenericListener} instance as specified.
     */
    protected GenericListener<String> makeDefaultRenameListener(final NodeValue2Weight subject) {
        return new GenericListener<String>() {
            @Override
            public void onFiredEvent(String newName) {
                onRenameRequested(subject, newName);
            }
        };
    }

    /**
     * Set up and display the editing area for the given
     * {@link Path2Weight} instance, honoring the
     * <tt>defaultPrecludingTreeTraceWeigherEditablePredicate</tt>.
     * 
     * @param subject
     *            the {@link Path2Weight} instance to be displayed
     *            within an editor.
     */
    private void displayEditor(final Path2Weight subject) {
        final EditingAreaView eav = window.getView().getEditingAreaView();
        eav.disposeCurrentEditor();

        final boolean rdOnly = !defaultPrecludingTreeTraceWeigherEditablePredicate
                .evaluate(subject);

        subject.accept(new Path2WeightVisitor.Adapter() {
            @Override
            public void visit(SimplePath2Weight it) {
                eav.setStatusText("Editing " + subject.getDisplayName() + " (builtin)");
                EditorController ec = new SimplePath2WeightEditorCtrl(it,
                        new GenericPath2WeightEditorView(eav.getEditingArea()),
                        WeightsEditorImpl.this, rdOnly);
                setCurrentEditorController(ec);
            }

            @Override
            public void visit(CustomPath2Weight it) {
                eav.setStatusText("Editing " + subject.getDisplayName());
                EditorController ec = new CustomPath2WeightEditorCtrl(it,
                        new GenericPath2WeightEditorView(eav.getEditingArea()),
                        WeightsEditorImpl.this, rdOnly);
                setCurrentEditorController(ec);
            }

            @Override
            public void otherMethodCalled(Path2Weight it) {
                GenericPath2WeightEditorView child = new GenericPath2WeightEditorView(
                        eav.getEditingArea());
                child.setEnabled(false);
            }
        });
    }

    /**
     * Set up and display the editing area for the given
     * {@link NodeValue2Weight} instance, honoring the
     * <tt>defaultPrecludingEditablePredicate</tt>.
     * 
     * @param subject
     *            The {@link NodeValue2Weight} instance to be displayed within
     *            an editor.
     */
    private void displayEditor(final NodeValue2Weight subject) {
        final EditingAreaView eav = window.getView().getEditingAreaView();
        eav.disposeCurrentEditor();

        final boolean rdOnly = !WeightsEditorImpl.this.defaultPrecludingEditablePredicate
                .evaluate(subject);

        subject.accept(new NodeValue2WeightVisitor.Adapter() {
            @Override
            public void visit(Locations2Weight it) {
                eav.setStatusText("Editing " + it.getDisplayName() + ", which is a " + it.getName());

                EditorController ec = new Locations2WeightEditorCtrl(it,
                        new Locations2WeightEditorView(eav.getEditingArea()),
                        WeightsEditorImpl.this, rdOnly);
                setCurrentEditorController(ec);
            }

            @Override
            public void visit(LocationValuation2Weight it) {
                eav.setStatusText("Editing " + it.getDisplayName() + ", which is a " + it.getName());
                IdentifierAccessor<Pair<String, String>> accessor =
                        new IdentifierAccessor<Pair<String, String>>() {

                            @Override
                            public String getCondition(
                                    Pair<String, String> identifier) {
                                return identifier.getLeft();
                            }

                            @Override
                            public String getValueExpression(
                                    Pair<String, String> identifier) {
                                return identifier.getRight();
                            }
                        };
                EditorController ec = new LocationValuation2WeightEditorCtrl(it,
                        new LocationValuation2WeightEditorView<Pair<String, String>>(eav
                                .getEditingArea(), accessor), WeightsEditorImpl.this, rdOnly);
                setCurrentEditorController(ec);
            }

            @Override
            public void visit(SuperLocationSize2Weight it) {
                eav.setStatusText("Editing " + it.getDisplayName() + ", which is a " + it.getName());
                SuperLocationSize2WeightEditorView ev = new SuperLocationSize2WeightEditorView(eav
                        .getEditingArea());
                EditorController ec = new SuperLocationSize2WeightEditorCtrl(it, ev,
                        WeightsEditorImpl.this, rdOnly);
                setCurrentEditorController(ec);
            }

            @Override
            public void otherMethodCalled(NodeValue2Weight it) {
                eav.disposeCurrentEditor();
                eav.setStatusText("Editing a " + it.getDisplayName() + " (unimplemented)");
            }
        });
    }

    /**
     * @return A factory object for {@link WeightsEditorImpl}.
     */
    public static WeightsEditorFactory getFactory() {
        return new WeightsEditorFactory() {
            @Override
            public WeightsEditorImpl makeWeightsEditor(
                    LazyOTFConfigSessionData sessionData, ExpressionValidator exprVal) {
                return new WeightsEditorImpl(sessionData, exprVal);
            }
        };
    }
}