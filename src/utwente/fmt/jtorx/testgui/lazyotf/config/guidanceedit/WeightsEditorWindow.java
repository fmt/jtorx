package utwente.fmt.jtorx.testgui.lazyotf.config.guidanceedit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

/**
 * The weights editor's window managing class.
 */
class WeightsEditorWindow {
    /** The editor's controller. */
    private final WeightsEditorImpl ctrl;

    /** The editor's view. */
    private ToplevelView view;

    /** The factory making editor views. */
    private final ToplevelView.Factory viewFactory;

    /**
     * Construct a new {@link WeightsEditorWindow} instance.
     * 
     * @param ctrl
     *            The instance's main controller.
     */
    public WeightsEditorWindow(WeightsEditorImpl ctrl) {
        this.ctrl = ctrl;
        viewFactory = new ToplevelView.Factory();
    }

    /**
     * @return The editor's main view instance.
     */
    public ToplevelView getView() {
        return view;
    }

    /**
     * Create an editor window and a corresponding view instance and display the
     * window (blocking & modal).
     */
    public void display() {
        final Shell parentShell = Display.getCurrent().getActiveShell();
        final Shell ourShell = new Shell(Display.getCurrent(), SWT.APPLICATION_MODAL
                | SWT.DIALOG_TRIM | SWT.RESIZE);
        ourShell.setLayout(new FillLayout());
        ourShell.setMinimumSize(640, 480);
        ourShell.setText("Custom Weighers");
        view = viewFactory.makeInstance(ourShell);
        ctrl.onFinishedViewSetup();
        // ctrl.setView(view);
        ourShell.addListener(SWT.Close, new Listener() {
            @Override
            public void handleEvent(Event ev) {
                // ev.doit = false;
                // ourShell.setVisible(false);
                parentShell.setEnabled(true);
            }
        });
        ourShell.pack();
        ourShell.open();

        while (!ourShell.isDisposed()) {
            if (!Display.getCurrent().readAndDispatch()) {
                Display.getCurrent().sleep();
            }
        }
    }

    /**
     * Display a simple message box featuring a warning icon and both an "Ok"
     * and a "Cancel" button. The method returns when the message box is closed.
     * 
     * @param text
     *            The message box' text.
     * @param title
     *            The message box' title.
     * @return true iff the user closed the message box by clicking on "Ok".
     */
    public boolean displayOkCancelMessageBox(String text, String title) {
        MessageBox mb = new MessageBox(view.getShell(),
                SWT.ICON_WARNING | SWT.OK | SWT.CANCEL);
        mb.setMessage(text);
        mb.setText(title);
        int result = mb.open();
        return result == SWT.OK;
    }
}
