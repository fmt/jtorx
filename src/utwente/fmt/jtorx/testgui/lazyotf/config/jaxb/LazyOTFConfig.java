//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.09.16 at 02:08:08 PM CEST 
//


package utwente.fmt.jtorx.testgui.lazyotf.config.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LazyOTFConfig complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LazyOTFConfig">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="testtypegroups" type="{}TestTypeGroups"/>
 *         &lt;element name="autorecompute" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="edgeweightaggregator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="maxdepth" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="initialdepth" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="depththreshold" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="visitor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="guidance-methods" type="{}OrdinaryGuidanceMethods"/>
 *         &lt;element name="custom-guidance-methods" type="{}CustomGuidanceMethods"/>
 *         &lt;element name="sat-strategy-global-conditions" type="{}SatStrategyConditionList"/>
 *         &lt;element name="decision-strategies" type="{}DecisionStrategyList"/>
 *         &lt;element name="node-value-weighers" type="{}NodeValueWeigherList"/>
 *         &lt;element name="location-sets" type="{}LocationSetList"/>
 *         &lt;element name="logging-file" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="autostop" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LazyOTFConfig", propOrder = {

})
@XmlRootElement
public class LazyOTFConfig {

    @XmlElement(required = true)
    protected TestTypeGroups testtypegroups;
    protected boolean autorecompute;
    @XmlElement(required = true)
    protected String edgeweightaggregator;
    protected int maxdepth;
    protected int initialdepth;
    protected int depththreshold;
    @XmlElement(required = true)
    protected String visitor;
    @XmlElement(name = "guidance-methods", required = true)
    protected OrdinaryGuidanceMethods guidanceMethods;
    @XmlElement(name = "custom-guidance-methods", required = true)
    protected CustomGuidanceMethods customGuidanceMethods;
    @XmlElement(name = "sat-strategy-global-conditions", required = true)
    protected SatStrategyConditionList satStrategyGlobalConditions;
    @XmlElement(name = "decision-strategies", required = true)
    protected DecisionStrategyList decisionStrategies;
    @XmlElement(name = "node-value-weighers", required = true)
    protected NodeValueWeigherList nodeValueWeighers;
    @XmlElement(name = "location-sets", required = true)
    protected LocationSetList locationSets;
    @XmlElement(name = "logging-file", required = true)
    protected String loggingFile;
    protected boolean autostop;

    /**
     * Gets the value of the testtypegroups property.
     * 
     * @return
     *     possible object is
     *     {@link TestTypeGroups }
     *     
     */
    public TestTypeGroups getTesttypegroups() {
        return testtypegroups;
    }

    /**
     * Sets the value of the testtypegroups property.
     * 
     * @param value
     *     allowed object is
     *     {@link TestTypeGroups }
     *     
     */
    public void setTesttypegroups(TestTypeGroups value) {
        this.testtypegroups = value;
    }

    /**
     * Gets the value of the autorecompute property.
     * 
     */
    public boolean isAutorecompute() {
        return autorecompute;
    }

    /**
     * Sets the value of the autorecompute property.
     * 
     */
    public void setAutorecompute(boolean value) {
        this.autorecompute = value;
    }

    /**
     * Gets the value of the edgeweightaggregator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEdgeweightaggregator() {
        return edgeweightaggregator;
    }

    /**
     * Sets the value of the edgeweightaggregator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEdgeweightaggregator(String value) {
        this.edgeweightaggregator = value;
    }

    /**
     * Gets the value of the maxdepth property.
     * 
     */
    public int getMaxdepth() {
        return maxdepth;
    }

    /**
     * Sets the value of the maxdepth property.
     * 
     */
    public void setMaxdepth(int value) {
        this.maxdepth = value;
    }

    /**
     * Gets the value of the initialdepth property.
     * 
     */
    public int getInitialdepth() {
        return initialdepth;
    }

    /**
     * Sets the value of the initialdepth property.
     * 
     */
    public void setInitialdepth(int value) {
        this.initialdepth = value;
    }

    /**
     * Gets the value of the depththreshold property.
     * 
     */
    public int getDepththreshold() {
        return depththreshold;
    }

    /**
     * Sets the value of the depththreshold property.
     * 
     */
    public void setDepththreshold(int value) {
        this.depththreshold = value;
    }

    /**
     * Gets the value of the visitor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisitor() {
        return visitor;
    }

    /**
     * Sets the value of the visitor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisitor(String value) {
        this.visitor = value;
    }

    /**
     * Gets the value of the guidanceMethods property.
     * 
     * @return
     *     possible object is
     *     {@link OrdinaryGuidanceMethods }
     *     
     */
    public OrdinaryGuidanceMethods getGuidanceMethods() {
        return guidanceMethods;
    }

    /**
     * Sets the value of the guidanceMethods property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrdinaryGuidanceMethods }
     *     
     */
    public void setGuidanceMethods(OrdinaryGuidanceMethods value) {
        this.guidanceMethods = value;
    }

    /**
     * Gets the value of the customGuidanceMethods property.
     * 
     * @return
     *     possible object is
     *     {@link CustomGuidanceMethods }
     *     
     */
    public CustomGuidanceMethods getCustomGuidanceMethods() {
        return customGuidanceMethods;
    }

    /**
     * Sets the value of the customGuidanceMethods property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomGuidanceMethods }
     *     
     */
    public void setCustomGuidanceMethods(CustomGuidanceMethods value) {
        this.customGuidanceMethods = value;
    }

    /**
     * Gets the value of the satStrategyGlobalConditions property.
     * 
     * @return
     *     possible object is
     *     {@link SatStrategyConditionList }
     *     
     */
    public SatStrategyConditionList getSatStrategyGlobalConditions() {
        return satStrategyGlobalConditions;
    }

    /**
     * Sets the value of the satStrategyGlobalConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link SatStrategyConditionList }
     *     
     */
    public void setSatStrategyGlobalConditions(SatStrategyConditionList value) {
        this.satStrategyGlobalConditions = value;
    }

    /**
     * Gets the value of the decisionStrategies property.
     * 
     * @return
     *     possible object is
     *     {@link DecisionStrategyList }
     *     
     */
    public DecisionStrategyList getDecisionStrategies() {
        return decisionStrategies;
    }

    /**
     * Sets the value of the decisionStrategies property.
     * 
     * @param value
     *     allowed object is
     *     {@link DecisionStrategyList }
     *     
     */
    public void setDecisionStrategies(DecisionStrategyList value) {
        this.decisionStrategies = value;
    }

    /**
     * Gets the value of the nodeValueWeighers property.
     * 
     * @return
     *     possible object is
     *     {@link NodeValueWeigherList }
     *     
     */
    public NodeValueWeigherList getNodeValueWeighers() {
        return nodeValueWeighers;
    }

    /**
     * Sets the value of the nodeValueWeighers property.
     * 
     * @param value
     *     allowed object is
     *     {@link NodeValueWeigherList }
     *     
     */
    public void setNodeValueWeighers(NodeValueWeigherList value) {
        this.nodeValueWeighers = value;
    }

    /**
     * Gets the value of the locationSets property.
     * 
     * @return
     *     possible object is
     *     {@link LocationSetList }
     *     
     */
    public LocationSetList getLocationSets() {
        return locationSets;
    }

    /**
     * Sets the value of the locationSets property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationSetList }
     *     
     */
    public void setLocationSets(LocationSetList value) {
        this.locationSets = value;
    }

    /**
     * Gets the value of the loggingFile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoggingFile() {
        return loggingFile;
    }

    /**
     * Sets the value of the loggingFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoggingFile(String value) {
        this.loggingFile = value;
    }

    /**
     * Gets the value of the autostop property.
     * 
     */
    public boolean isAutostop() {
        return autostop;
    }

    /**
     * Sets the value of the autostop property.
     * 
     */
    public void setAutostop(boolean value) {
        this.autostop = value;
    }

}
