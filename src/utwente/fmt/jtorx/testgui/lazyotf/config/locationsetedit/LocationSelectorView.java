package utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.CheckListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * The editor's location selector view. This view mainly displays a list of
 * locations, each being equipped with a checkbox.
 */
class LocationSelectorView extends Composite {
    /** The table containing the location items. */
    private final Table locationTable;

    /** The control for the matcher text. */
    private final Text matchText;

    /** The set of listeners for changes in the matcher text control. */
    private final Set<GenericListener<String>> matchEditListeners;

    /** The set of listeners for changes in the "checkedness" of locations. */
    private final Set<CheckListener<NamedSet<Location>>> checkEditListeners;

    /**
     * Construct a new LocationSelectorView instance, initially displaying no
     * locations.
     * 
     * @param parent
     *            The view's parent control.
     */
    public LocationSelectorView(Composite parent) {
        super(parent, SWT.NONE);
        setLayout(new FillLayout());

        matchEditListeners = new HashSet<>();
        checkEditListeners = new HashSet<>();

        Group locationsGroup = new Group(this, SWT.NONE);
        locationsGroup.setText("Locations");

        GridLayout mainLayout = new GridLayout();
        mainLayout.numColumns = 2;
        locationsGroup.setLayout(mainLayout);

        GridData fillGrid = new GridData(GridData.FILL_BOTH);
        fillGrid.horizontalSpan = 2;

        locationTable = new Table(locationsGroup, SWT.CHECK | SWT.V_SCROLL | SWT.MULTI);
        locationTable.setHeaderVisible(false);
        locationTable.setLayoutData(fillGrid);

        Label matchLabel = new Label(locationsGroup, SWT.NONE);
        matchLabel.setText("Match: ");

        matchText = new Text(locationsGroup, SWT.NONE);
        matchText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        setupListeners();
        setupContextMenu();
    }

    /**
     * Set up the matcher text control and checkbox listeners.
     */
    private void setupListeners() {
        matchText.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent arg0) {
                for (GenericListener<String> listener : matchEditListeners) {
                    listener.onFiredEvent(matchText.getText());
                }
            }
        });

        locationTable.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                if (ev.detail == SWT.CHECK) {
                    TableItem selectionSrc = (TableItem) ev.item;
                    TableItem[] selectedLocations = locationTable.getSelection();
                    for (TableItem ti : selectedLocations) {
                        @SuppressWarnings("unchecked")
                        NamedSet<Location> loc = (NamedSet<Location>) ti.getData();
                        for (CheckListener<NamedSet<Location>> listener : checkEditListeners) {
                            listener.onSetChecked(loc, selectionSrc.getChecked());
                        }
                    }
                }
            }
        });
    }

    /**
     * Set up the context menu and according listeners.
     */
    private void setupContextMenu() {
        Menu contextMenu = new Menu(locationTable);
        locationTable.setMenu(contextMenu);

        final MenuItem checkCurrentSel = new MenuItem(contextMenu, SWT.PUSH);
        checkCurrentSel.setText("Select");

        final MenuItem uncheckCurrentSel = new MenuItem(contextMenu, SWT.PUSH);
        uncheckCurrentSel.setText("Deselect");

        final MenuItem checkAll = new MenuItem(contextMenu, SWT.PUSH);
        checkAll.setText("Select All");

        final MenuItem uncheckAll = new MenuItem(contextMenu, SWT.PUSH);
        uncheckAll.setText("Select None");

        contextMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuShown(MenuEvent arg0) {
                boolean enabledness = (locationTable.getSelectionCount() > 0);
                checkCurrentSel.setEnabled(enabledness);
                uncheckCurrentSel.setEnabled(enabledness);
            }
        });

        checkCurrentSel.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                fireMultipleCheckListeners(locationTable.getSelection(), true);
            }
        });

        uncheckCurrentSel.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                fireMultipleCheckListeners(locationTable.getSelection(), false);
            }
        });

        checkAll.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                fireMultipleCheckListeners(locationTable.getItems(), true);
            }
        });

        uncheckAll.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                fireMultipleCheckListeners(locationTable.getItems(), false);
            }
        });
    }

    /**
     * Fire the "checkedness" listeners for each item in a table item array.
     * 
     * @param selection
     *            An array of table items representing locations.
     * @param checked
     *            true iff the items all have a new "checked" state.
     */
    @SuppressWarnings("unchecked")
    private void fireMultipleCheckListeners(TableItem[] selection, boolean checked) {
        Set<NamedSet<Location>> checkedLocs = new HashSet<>();
        for (TableItem ti : selection) {
            ti.setChecked(checked);
            checkedLocs.add((NamedSet<Location>) ti.getData());
        }
        for (NamedSet<Location> l : checkedLocs) {
            for (CheckListener<NamedSet<Location>> listener : checkEditListeners) {
                listener.onSetChecked(l, checked);
            }
        }
    }

    /**
     * Remove all locations from the location list.
     */
    public void removeLocations() {
        locationTable.removeAll();
    }

    /**
     * Add a location to the location list.
     * 
     * @param l
     *            The location to be added.
     * @param checked
     *            true iff its checkbox is to be checked initially.
     */
    public void addLocation(NamedSet<Location> l, boolean checked) {
        int insertionIdx = ViewUtilities.determineInsertionIndex(locationTable.getItems(),
                l.getDisplayName(), 0, true);
        TableItem ti = new TableItem(locationTable, SWT.NONE, insertionIdx);
        ti.setText(l.getDisplayName());
        ti.setData(l);
        ti.setChecked(checked);
    }

    /**
     * Add a listener for changes in location "checkedness".
     * 
     * @param it
     *            A {@link CheckListener} for {@link Location}s.
     */
    public void addLocationCheckedListener(final CheckListener<NamedSet<Location>> it) {
        checkEditListeners.add(it);
    }

    /**
     * Add a listener for changes in the matcher text control.
     * 
     * @param it
     *            A listener taking the new content of the control as its
     *            argument.
     */
    public void addMatchTextEditListener(final GenericListener<String> it) {
        matchEditListeners.add(it);
    }

    @Override
    public void redraw() {
        super.redraw();
        locationTable.redraw();
        matchText.redraw();
    }

    @Override
    public void setEnabled(boolean enabledness) {
        super.setEnabled(enabledness);
        locationTable.setEnabled(enabledness);
        matchText.setEnabled(enabledness);
    }

    @Override
    public void setRedraw(boolean redraw) {
        super.setRedraw(redraw);
        locationTable.setRedraw(redraw);
        matchText.setRedraw(redraw);
    }
}
