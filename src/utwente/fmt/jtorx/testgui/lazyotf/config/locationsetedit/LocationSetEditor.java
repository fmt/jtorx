package utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit;

import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;

/**
 * The public interface of the location set editor.
 */
public interface LocationSetEditor {
    /**
     * Display the editor window. Returns when the window gets closed.
     */
    void open();

    /**
     * @return The edited set of location sets.
     */
    Set<NamedSet<Location>> getLocationSets();

    /**
     * @return The new location sets created by the user. This set does not
     *         contain new location sets which were deleted subsequently.
     */
    Set<NamedSet<Location>> getNewLocationSets();

    /**
     * @return The location sets deleted by the user. This set does not contain
     *         new location sets which were deleted subsequently.
     */
    Set<NamedSet<Location>> getDeletedLocationSets();
}
