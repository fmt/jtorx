package utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;

/**
 * A factory interface for {@link LocationSetEditor}.
 * {@link LocationSetEditorImpl} provides an implementation of this interface
 * via <i>getFactory()</i>.
 */
public interface LocationSetEditorFactory {
    /**
     * Make a location set editor object. Initially, the editor displays a
     * specified location set.
     * 
     * @param sessionData
     *            The configuration session data containing the set of named
     *            location sets as well as the locations. This object does not
     *            get modified directly.
     * @param initialSelection
     *            The editor's initial selection.
     * @return A reference to the controller object used for the editor.
     */
    LocationSetEditor makeLocationSetEditor(LazyOTFConfigSessionData sessionData,
            NamedSet<Location> initialSelection);

    /**
     * Make a location set editor object. Initially, the editor displays no
     * location set.
     * 
     * @param sessionData
     *            The configuration session data containing the set of named
     *            location sets as well as the locations. This object does not
     *            get modified directly.
     * @return A reference to the controller object used for the editor.
     */
    LocationSetEditor makeLocationSetEditor(LazyOTFConfigSessionData sessionData);
}
