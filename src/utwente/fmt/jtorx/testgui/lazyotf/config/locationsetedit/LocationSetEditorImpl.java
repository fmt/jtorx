package utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit;

import java.util.HashSet;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.CheckListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.RenameListener;

/**
 * Controller class for the Location Set Editor.
 */
public class LocationSetEditorImpl implements LocationSetEditor {
    /** Object dealing with the editor's window setup. */
    private LocationSetEditorWindow window;

    /** The window object factory. */
    private final LocationSetEditorWindow.Factory windowFactory;

    /** The "toplevel" view managing all other (sub-)views. */
    private ToplevelView view;

    /** The set of locations available for group creation. */
    private final Set<NamedSet<Location>> locations;

    /** The set of (potentially modified) location sets. */
    private final Set<NamedSet<Location>> locationSets;

    /** The current string used for location name matching. */
    private String currentMatchingString;

    /** The editor's initial selection. */
    private final NamedSet<Location> initialSelection;

    /** Deleted location sets. */
    private final Set<NamedSet<Location>> deletedLocationSets;

    /** Created location sets. */
    private final Set<NamedSet<Location>> newLocationSets;

    /**
     * Construct a new location set editor controller. Initializes the editor's
     * data structures. Does not display the window.
     * 
     * @param sessionData
     *            The configuration session data containing the set of named
     *            location sets and locations. The controller does not change
     *            this data structure or any of its elements except for
     *            already-existing named location sets.
     * @param initialSelection
     *            The editor's initial selection. May be null.
     */
    public LocationSetEditorImpl(LazyOTFConfigSessionData sessionData,
            NamedSet<Location> initialSelection) {
        windowFactory = new LocationSetEditorWindow.Factory();
        window = windowFactory.makeInstance(this);
        locations = new HashSet<>();
        newLocationSets = new HashSet<>();
        deletedLocationSets = new HashSet<>();

        locationSets = new HashSet<>();

        if (sessionData != null) {
            locations.addAll(sessionData.getBasisLocationSets());
            locationSets.addAll(sessionData.getLocationSets());
        }

        currentMatchingString = "";
        this.initialSelection = initialSelection;
    }

    @Override
    public void open() {
        window.display();
    }

    /**
     * Method for post-view-initialization work. Set up listeners, redraws
     * everything and sets the initial selection (if any).
     */
    protected void onFinishedViewSetup() {
        view = window.getView();
        setupListeners();
        redrawLocationSetList();
        redrawLocationList();
        redrawToolbar();
        if (initialSelection != null) {
            view.getSidebarView().setSelection(initialSelection);
            onSelectLocationSet(initialSelection);
        }
    }

    /**
     * Redraw the tool bar, graying out items where necessary.
     */
    private void redrawToolbar() {
        SidebarView sv = view.getSidebarView();
        NamedSet<Location> selection = sv.getSelection();
        boolean enabledness = (selection != null);

        ToolbarView toolbar = view.getToolbarView();
        toolbar.setEnabled(ToolbarView.Buttons.DELETE, enabledness);
        toolbar.setEnabled(ToolbarView.Buttons.DUPLICATE, enabledness);
        toolbar.setEnabled(ToolbarView.Buttons.RENAME, enabledness);
    }

    /**
     * Redraw the location set list.
     */
    private void redrawLocationSetList() {
        SidebarView sv = view.getSidebarView();

        NamedSet<Location> selection = sv.getSelection();

        sv.setRedraw(false);
        sv.removeLocationSets();

        for (NamedSet<Location> locset : locationSets) {
            sv.addLocationSet(locset);
        }

        if (selection != null && locationSets.contains(selection)) {
            sv.setSelection(selection);
            view.getLocationSelectorView().setEnabled(true);
        }
        else {
            view.getLocationSelectorView().setEnabled(false);
        }

        sv.setRedraw(true);
    }

    /**
     * Redraw the location list view unchecking all locations.
     */
    private void redrawLocationList() {
        redrawLocationList(new HashSet<Location>());
    }

    /**
     * Redraw the location list view.
     * 
     * @param checkedLocs
     *            The locations to be put in a "checked" state.
     */
    private void redrawLocationList(Set<Location> checkedLocs) {
        LocationSelectorView lsv = view.getLocationSelectorView();

        lsv.setRedraw(false);

        lsv.removeLocations();
        for (NamedSet<Location> location : locations) {
            if (location.getDisplayName().toLowerCase()
                    .contains(currentMatchingString.toLowerCase())) {
                lsv.addLocation(location, checkedLocs.contains(location.iterator().next()));
            }
        }

        lsv.setRedraw(true);
    }

    /**
     * Event handler for location set selection. Updates the location list view
     * accordingly.
     * 
     * @param it
     *            The selected location set.
     */
    protected void onSelectLocationSet(NamedSet<Location> it) {
        if (it != null) {
            view.getLocationSelectorView().setEnabled(true);
            redrawLocationList(it);
        }
        else {
            view.getLocationSelectorView().setEnabled(false);
            redrawLocationList();
        }
        redrawToolbar();
    }

    /**
     * Event handler for changes to the matching string.
     * 
     * @param newMatchingString
     *            The new matching string.
     */
    protected void onChangedMatchingString(String newMatchingString) {
        currentMatchingString = newMatchingString;
        redrawLocationList(view.getSidebarView().getSelection());
    }

    /**
     * Event handler for requests to make a new location set.
     */
    protected void onMakeNewLocationSet() {
        NamedSet<Location> newLocationSet = new NamedSet<Location>("New location set");
        locationSets.add(newLocationSet);
        newLocationSets.add(newLocationSet);
        redrawLocationSetList();
    }

    /**
     * Event handler for requests to delete a location set.
     * 
     * @param it
     *            The location set to be deleted.
     */
    protected void onDeleteLocationSet(NamedSet<Location> it) {
        locationSets.remove(it);
        redrawLocationSetList();

        if (newLocationSets.contains(it)) {
            newLocationSets.remove(it);
        }
        else {
            deletedLocationSets.add(it);
        }

        if (locationSets.isEmpty()) {
            redrawLocationList();
        }
        else if (view.getSidebarView().getSelection() != null) {
            redrawLocationList(view.getSidebarView().getSelection());
        }
    }

    /**
     * Event handler for requests to duplicate a location set. Adds a copy of
     * the given location set having the same name appended with " (copy)".
     * 
     * @param it
     *            The location set to be duplicated.
     */
    protected void onDuplicateLocationSet(NamedSet<Location> it) {
        NamedSet<Location> dupe = new NamedSet<Location>(it.getName() + " (copy)", it);
        locationSets.add(dupe);
        redrawLocationSetList();
    }

    /**
     * Set up the listeners gluing the sidebar and location selector views to
     * the event handlers.
     */
    private void setupListeners() {
        final LocationSelectorView locationSelView = view.getLocationSelectorView();
        final SidebarView sidebarView = view.getSidebarView();

        setupToolbarListeners();

        sidebarView.addSelectionListener(new GenericListener<NamedSet<Location>>() {
            @Override
            public void onFiredEvent(NamedSet<Location> event) {
                onSelectLocationSet(event);
            }
        });

        sidebarView.addRenameListener(new RenameListener<NamedSet<Location>>() {
            @Override
            public void onRename(NamedSet<Location> instance, String newName) {
                if (!newName.equals("")) {
                    NamedSet<Location> currentSel = sidebarView.getSelection();
                    currentSel.setName(newName);
                    locationSets.remove(currentSel);
                    locationSets.add(currentSel);
                    redrawLocationSetList();
                }
            }
        });

        locationSelView.addMatchTextEditListener(new GenericListener<String>() {
            @Override
            public void onFiredEvent(String event) {
                onChangedMatchingString(event);
            }
        });

        locationSelView.addLocationCheckedListener(new CheckListener<NamedSet<Location>>() {
            @Override
            public void onSetChecked(NamedSet<Location> instance, boolean newValue) {
                NamedSet<Location> currentSelection = sidebarView.getSelection();
                if (currentSelection == null) {
                    return;
                }

                if (newValue == true) {
                    currentSelection.add(instance.iterator().next());
                }
                else {
                    currentSelection.remove(instance.iterator().next());
                }

                redrawLocationList(currentSelection);
            }
        });
    }

    /**
     * Set up the listeners gluing the tool bar view to the event handlers.
     */
    private void setupToolbarListeners() {
        ToolbarView toolbar = view.getToolbarView();
        toolbar.addSelectionListener(ToolbarView.Buttons.NEW, new Runnable() {
            @Override
            public void run() {
                onMakeNewLocationSet();
            }
        });

        toolbar.addSelectionListener(ToolbarView.Buttons.DELETE, new Runnable() {
            @Override
            public void run() {
                onDeleteLocationSet(view.getSidebarView().getSelection());
            }
        });

        toolbar.addSelectionListener(ToolbarView.Buttons.DUPLICATE, new Runnable() {
            @Override
            public void run() {
                onDuplicateLocationSet(view.getSidebarView().getSelection());
            }
        });

        toolbar.addSelectionListener(ToolbarView.Buttons.RENAME, new Runnable() {
            @Override
            public void run() {
                view.getSidebarView().renameCurrentItem();
            }
        });
    }

    @Override
    public Set<NamedSet<Location>> getLocationSets() {
        return locationSets;
    }

    /**
     * @return A factory object for {@link LocationSetEditorImpl}.
     */
    public static LocationSetEditorFactory getFactory() {
        return new LocationSetEditorFactory() {
            @Override
            public LocationSetEditorImpl makeLocationSetEditor(
                    LazyOTFConfigSessionData sessionData,
                    NamedSet<Location> initialSelection) {
                return new LocationSetEditorImpl(sessionData, initialSelection);
            }

            @Override
            public LocationSetEditorImpl makeLocationSetEditor(
                    LazyOTFConfigSessionData sessionData) {
                return makeLocationSetEditor(sessionData, null);
            }
        };
    }

    @Override
    public Set<NamedSet<Location>> getNewLocationSets() {
        return newLocationSets;
    }

    @Override
    public Set<NamedSet<Location>> getDeletedLocationSets() {
        return deletedLocationSets;
    }
}
