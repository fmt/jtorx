package utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

/**
 * A class for managing the location set editor window.
 */
class LocationSetEditorWindow {
    /** The location set editor's controller. */
    private final LocationSetEditorImpl ctrl;

    /** The location set editor's main view. */
    private ToplevelView view;

    /** A factory for the editor's main view. */
    private final ToplevelView.Factory viewFactory;

    /**
     * Construct a new LocationSetEditorWindow instance and initialize its data
     * structures. Does not display the window.
     * 
     * @param ctrl
     *            The editor's controller.
     */
    public LocationSetEditorWindow(LocationSetEditorImpl ctrl) {
        this.ctrl = ctrl;
        viewFactory = new ToplevelView.Factory();
    }

    /**
     * Display the location set editor window and return when the window gets
     * closed.
     */
    public void display() {
        final Shell parentShell = Display.getCurrent().getActiveShell();
        final Shell ourShell = new Shell(Display.getCurrent(), SWT.APPLICATION_MODAL
                | SWT.DIALOG_TRIM | SWT.RESIZE);
        ourShell.setLayout(new FillLayout());
        ourShell.setMinimumSize(640, 480);
        ourShell.setText("Location Sets");
        view = viewFactory.makeInstance(ourShell);
        ctrl.onFinishedViewSetup();
        ourShell.addListener(SWT.Close, new Listener() {
            @Override
            public void handleEvent(Event ev) {
                parentShell.setEnabled(true);
            }
        });
        ourShell.pack();
        ourShell.open();

        while (!ourShell.isDisposed()) {
            if (!Display.getCurrent().readAndDispatch()) {
                Display.getCurrent().sleep();
            }
        }
    }

    /**
     * @return The editors "toplevel" view.
     */
    public ToplevelView getView() {
        return view;
    }

    public static class Factory {
        public LocationSetEditorWindow makeInstance(LocationSetEditorImpl ctrl) {
            return new LocationSetEditorWindow(ctrl);
        }
    }
}
