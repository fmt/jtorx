package utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.TextTreeEditor;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.RenameListener;

/**
 * The editor's side bar view displaying the location set names.
 */
class SidebarView extends Composite {
    /** The tree control containing the location set items. */
    private final Tree locationSetCtrl;

    /** A tree editor for locationSetCtrl. */
    private final TextTreeEditor locationSetEditor;

    /** The set of location set selection listeners. */
    private final Set<GenericListener<NamedSet<Location>>> selectionListeners;

    /** The set of rename action listeners. */
    private final Set<RenameListener<NamedSet<Location>>> renameListeners;

    /** A map associating location sets with tree items. */
    private final Map<NamedSet<Location>, TreeItem> sets2TreeItems;

    /**
     * Construct a new side bar view instance.
     * 
     * @param parent
     *            The view's parent control.
     */
    public SidebarView(Composite parent) {
        super(parent, SWT.NONE);
        setLayout(new FillLayout());

        locationSetCtrl = new Tree(this, SWT.V_SCROLL);
        locationSetEditor = new TextTreeEditor(locationSetCtrl);

        selectionListeners = new HashSet<>();
        renameListeners = new HashSet<>();
        sets2TreeItems = new HashMap<>();

        setupListeners();
    }

    /**
     * Set up the selection and rename action listeners.
     */
    private void setupListeners() {
        locationSetCtrl.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (GenericListener<NamedSet<Location>> listener : selectionListeners) {
                    TreeItem selectedTI = (TreeItem) ev.item;
                    @SuppressWarnings("unchecked")
                    NamedSet<Location> data = (NamedSet<Location>) selectedTI.getData();
                    listener.onFiredEvent(data);
                }
            }
        });

        locationSetCtrl.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDoubleClick(MouseEvent arg0) {
                renameCurrentItem();
            }
        });

        locationSetEditor.addEditFinishedListener(new GenericListener<String>() {
            @Override
            public void onFiredEvent(String event) {
                for (RenameListener<NamedSet<Location>> listener : renameListeners) {
                    listener.onRename(getSelection(), event);
                }
            }
        });
    }

    /**
     * Remove all location sets from the location set list.
     */
    public void removeLocationSets() {
        locationSetCtrl.removeAll();
        sets2TreeItems.clear();
    }

    /**
     * Add a location set to the list of location sets.
     * 
     * @param it
     *            The location set to be added.
     */
    public void addLocationSet(NamedSet<Location> it) {
        int insertionIdx = ViewUtilities.determineInsertionIndex(locationSetCtrl.getItems(),
                it.getDisplayName());
        TreeItem newTreeItem = new TreeItem(locationSetCtrl, SWT.NONE, insertionIdx);
        newTreeItem.setText(it.getDisplayName());
        sets2TreeItems.put(it, newTreeItem);
        newTreeItem.setData(it);
    }

    /**
     * Add a location set selection listener.
     * 
     * @param it
     *            A listener taking a location set as its argument.
     */
    public void addSelectionListener(GenericListener<NamedSet<Location>> it) {
        selectionListeners.add(it);
    }

    /**
     * Add a location set rename listener.
     * 
     * @param it
     *            A listener taking a location set as its argument.
     */
    public void addRenameListener(RenameListener<NamedSet<Location>> it) {
        renameListeners.add(it);
    }

    /**
     * @return The currently selected location set.
     */
    public NamedSet<Location> getSelection() {
        if (locationSetCtrl.getSelectionCount() > 0) {
            TreeItem[] selection = locationSetCtrl.getSelection();
            @SuppressWarnings("unchecked")
            NamedSet<Location> selectedSet = (NamedSet<Location>) selection[0].getData();
            return selectedSet;
        }
        else {
            return null;
        }
    }

    /**
     * Set the current location set selection.
     * 
     * @param selection
     *            The new selected location set.
     */
    public void setSelection(NamedSet<Location> selection) {
        TreeItem selTI = sets2TreeItems.get(selection);
        if (selTI != null) {
            locationSetCtrl.setSelection(selTI);
        }
    }

    /**
     * Display the rename editor for the currently selected location set.
     */
    public void renameCurrentItem() {
        if (locationSetCtrl.getSelectionCount() > 0) {
            locationSetEditor.displayEditor(locationSetCtrl.getSelection()[0],
                    getSelection().getName());
        }
    }

    @Override
    public void setRedraw(boolean redraw) {
        super.setRedraw(redraw);
        locationSetCtrl.setRedraw(redraw);
    }

    @Override
    public void redraw() {
        super.redraw();
        locationSetCtrl.redraw();
    }

    @Override
    public void setEnabled(boolean enabledness) {
        super.setEnabled(enabledness);
        locationSetCtrl.setEnabled(enabledness);
    }
}
