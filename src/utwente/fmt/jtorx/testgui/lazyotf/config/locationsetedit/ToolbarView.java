package utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * The location set editor's tool bar view.
 */
class ToolbarView extends Composite {
    /** The main tool bar control. */
    private final ToolBar toolBar;

    /** The "New..." tool bar button. */
    private final ToolItem newItem;

    /** The "Delete" tool bar button. */
    private final ToolItem deleteItem;

    /** The "Rename" tool bar button. */
    private final ToolItem renameItem;

    /** The "Duplicate" tool bar button. */
    private final ToolItem duplicateItem;

    /** The set of listeners for item creation requests. */
    private final Set<Runnable> newItemRequestedListeners;

    /** The set of listeners for delete actions. */
    private final Set<Runnable> deleteItemRequestedListeners;

    /** The set of listeners for rename actions. */
    private final Set<Runnable> renameItemRequestedListeners;

    /** The set of listeners for duplication actions. */
    private final Set<Runnable> duplicateItemRequestedListeners;

    /**
     * Enumeration of the buttons within the toolbar. Used for the toolItemNames
     * map.
     */
    public enum Buttons {
        NEW,
        DELETE,
        RENAME,
        DUPLICATE
    };

    /** Map from tool bar item names to concrete buttons. */
    private final Map<Buttons, ToolItem> toolItemNames;

    /** Map associating tool bar buttons with their selection listeners. */
    private final Map<ToolItem, Set<Runnable>> listenerMap;

    /**
     * Construct a new tool bar view instance.
     * 
     * @param parent
     *            The tool bar's parent control.
     */
    public ToolbarView(Composite parent) {
        super(parent, SWT.NONE);
        setLayout(new FillLayout());

        toolItemNames = new HashMap<>();
        listenerMap = new HashMap<>();

        newItemRequestedListeners = new HashSet<>();
        deleteItemRequestedListeners = new HashSet<>();
        renameItemRequestedListeners = new HashSet<>();
        duplicateItemRequestedListeners = new HashSet<>();

        toolBar = new ToolBar(this, SWT.FLAT | SWT.WRAP);
        newItem = new ToolItem(toolBar, SWT.PUSH);
        newItem.setText("New Set");
        renameItem = new ToolItem(toolBar, SWT.PUSH);
        renameItem.setText("Rename");
        duplicateItem = new ToolItem(toolBar, SWT.PUSH);
        duplicateItem.setText("Duplicate");
        deleteItem = new ToolItem(toolBar, SWT.PUSH);
        deleteItem.setText("Delete");

        toolItemNames.put(Buttons.NEW, newItem);
        toolItemNames.put(Buttons.RENAME, renameItem);
        toolItemNames.put(Buttons.DUPLICATE, duplicateItem);
        toolItemNames.put(Buttons.DELETE, deleteItem);
        listenerMap.put(newItem, newItemRequestedListeners);
        listenerMap.put(deleteItem, deleteItemRequestedListeners);
        listenerMap.put(renameItem, renameItemRequestedListeners);
        listenerMap.put(duplicateItem, duplicateItemRequestedListeners);

        setupListeners();
    }

    /**
     * Install a selection listener for the given tool bar button. The listener
     * looks up the "real" selection listener in the listener map and runs it.
     * 
     * @param button The target SWT button widget.
     */
    private void installButtonClickListener(final ToolItem button) {
        button.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (Runnable r : listenerMap.get(button)) {
                    r.run();
                }
            }
        });
    }

    /**
     * Install the button listeners.
     */
    private void setupListeners() {
        installButtonClickListener(newItem);
        installButtonClickListener(renameItem);
        installButtonClickListener(duplicateItem);
        installButtonClickListener(deleteItem);
    }

    /**
     * Add a selection listener for the given tool bar button.
     * 
     * @param button
     *            The involved tool bar button.
     * @param listener
     *            The listener to be called upon selection events.
     */
    public void addSelectionListener(Buttons button, Runnable listener) {
        listenerMap.get(toolItemNames.get(button)).add(listener);
    }

    /**
     * Set the enabledness of a tool bar button.
     * 
     * @param button
     *            The involved tool bar button.
     * @param enabledness
     *            false iff the button is to be grayed out.
     */
    public void setEnabled(Buttons button, boolean enabledness) {
        toolItemNames.get(button).setEnabled(enabledness);
    }

    @Override
    public void setEnabled(boolean enabledness) {
        super.setEnabled(enabledness);
        for (Buttons b : Buttons.values()) {
            toolItemNames.get(b).setEnabled(enabledness);
        }
    }
}
