package utwente.fmt.jtorx.testgui.lazyotf.config.locationsetedit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import utwente.fmt.jtorx.testgui.lazyotf.utilities.SashLayout;

/**
 * The editor's "toplevel" view responsible for setting up and laying out all
 * other views.
 */
class ToplevelView extends Composite {
    /** The editor's side-bar view (contains the location set names). */
    private final SidebarView sidebarView;

    /**
     * The editor's location selector view (the list of checkbox-equipped
     * locations.
     */
    private final LocationSelectorView locationSelectorView;

    /** The editor's tool bar view. */
    private final ToolbarView toolbarView;

    /**
     * Construct a new "toplevel" view instance.
     * 
     * @param parent
     *            The instance's parent control.
     */
    public ToplevelView(Composite parent) {
        super(parent, SWT.NONE);

        GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        setLayout(layout);

        GridData fillGrid = new GridData(GridData.FILL_BOTH);

        toolbarView = new ToolbarView(this);

        Composite lowerPart = new Composite(this, SWT.NONE);
        lowerPart.setLayoutData(fillGrid);
        sidebarView = new SidebarView(lowerPart);
        locationSelectorView = new LocationSelectorView(lowerPart);
        SashLayout.setupVerticallySplitting(lowerPart, sidebarView, locationSelectorView);

    }

    /**
     * @return The side bar view instance..
     */
    public SidebarView getSidebarView() {
        return sidebarView;
    }

    /**
     * @return The location selector view instance.
     */
    public LocationSelectorView getLocationSelectorView() {
        return locationSelectorView;
    }

    /**
     * @return The tool bar view instance.
     */
    public ToolbarView getToolbarView() {
        return toolbarView;
    }

    public static class Factory {
        public ToplevelView makeInstance(Composite parent) {
            return new ToplevelView(parent);
        }
    }
}
