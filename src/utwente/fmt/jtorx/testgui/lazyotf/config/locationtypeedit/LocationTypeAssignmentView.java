package utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * An SWT control for displaying and modifying Location/TestType associations.
 */
public class LocationTypeAssignmentView extends Composite {
    /** The Tree displaying the selected test type group. */
    private final Tree groupEditor;

    /**
     * A mapping from test types to their root element in groupEditor. Note: The
     * key set may contain <tt>null</tt>, which gets associated with the
     * "no type assigned" tree item.
     */
    private final Map<TestType, TreeItem> currentParentMap;

    /**
     * A sorted list of all test types known to the control. The list is sorted
     * descending with respect to the TestType priority.
     */
    private final List<TestType> allTestTypes;

    /** A map associating the displayed locations with tree items. */
    private final Map<NamedSet<Location>, TreeItem> currentItemMap;

    /** Set of listeners for setting location test types within test type groups */
    private final Set<SetTestTypeListener> onSetTestTypeListeners;

    /**
     * Set of listeners for handling requests to display the location set editor
     */
    private final Set<GenericListener<NamedSet<Location>>> onDisplayLocationSetEditorListeners;

    protected static final String ASSIGN_NONE = "(none)";
    protected static final String NO_TYPE_ASSIGNED = "(No type assigned)";

    /**
     * Construct a new LocationTypeAssignmentView instance.
     * 
     * @param parent
     *            The parent widget.
     * @param allTestTypes
     *            The set of test types known to the control. Only test types
     *            present in this set (in an object-identity sense) can be
     *            displayed by the editor. This set must not contain
     *            <tt>null</tt>.
     * @param allowNullAssignment
     *            If true, the "(none)" item is available in the assignment
     *            submenu.
     */
    public LocationTypeAssignmentView(Composite parent, Set<TestType> allTestTypes,
            boolean allowNullAssignment) {
        super(parent, SWT.NONE);

        assert (parent != null);
        assert (allTestTypes != null);
        assert (!allTestTypes.contains(null));

        onSetTestTypeListeners = new HashSet<>();
        onDisplayLocationSetEditorListeners = new HashSet<>();

        this.allTestTypes = new ArrayList<>(allTestTypes);

        /* sort descending with respect to the test type priority */
        Collections.sort(this.allTestTypes, new Comparator<TestType>() {
            @Override
            public int compare(TestType arg0, TestType arg1) {
                if (arg0 != null && arg1 != null) {
                    return -arg0.compareTo(arg1);
                }
                else {
                    return -1;
                }
            }
        });

        /* the "no test type assigned" "test type" */
        if (allowNullAssignment) {
            this.allTestTypes.add(null);
        }

        setLayout(new FillLayout());
        currentItemMap = new HashMap<>();
        currentParentMap = new HashMap<>();

        groupEditor = new Tree(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);

        setupGroupEditor();
    }

    /**
     * Initialize the groupEditor control by constructing its context menu and
     * appropriate listeners.
     */
    private void setupGroupEditor() {
        /* Setup context menu */
        Menu gedMenu = new Menu(groupEditor);
        groupEditor.setMenu(gedMenu);
        MenuItem assignItem = new MenuItem(gedMenu, SWT.CASCADE);
        Menu assignSubMenu = new Menu(assignItem);
        assignItem.setMenu(assignSubMenu);
        assignItem.setText("Assign type");

        MenuItem lseItem = new MenuItem(gedMenu, SWT.PUSH);
        lseItem.setText("Edit location sets...");
        lseItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onDisplayLocationSetEditor(null);
            }
        });

        /* Fill the "Assign" submenu */
        final Set<MenuItem> assignSubmenuItems = new HashSet<MenuItem>();
        assignSubmenuItems.addAll(setupGroupEditorAssignMenuTypeItems(assignSubMenu));

        /* Gray out the "Assign" submenu when no assignable item is selected */
        groupEditor.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                boolean grayOut = true;
                for (TreeItem ti : groupEditor.getSelection()) {
                    if (ti.getData() != null && ti.getData() instanceof NamedSet<?>) {
                        grayOut = false;
                    }
                }
                for (MenuItem mi : assignSubmenuItems) {
                    boolean isAssignable = true;
                    if (mi.getData() != null) {
                        /* has test type */
                        TestType menuTestType = (TestType) mi.getData();
                        if (!currentParentMap.keySet().contains(menuTestType)) {
                            isAssignable = false;
                        }
                    }

                    if (mi.getData() == null && !currentParentMap.keySet().contains(null)) {
                        /*
                         * "assign none" menu item, but group enforces
                         * assignment
                         */
                        isAssignable = false;
                    }

                    mi.setEnabled(!grayOut && isAssignable);
                }
            }
        });

        /* Double-click on named location set -> Open location set editor */
        groupEditor.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDoubleClick(MouseEvent arg0) {
                TreeItem target = groupEditor.getItem(new Point(arg0.x, arg0.y));
                if (target != null && currentItemMap.containsValue(target)) {
                    @SuppressWarnings("unchecked")
                    NamedSet<Location> locSet = (NamedSet<Location>) target.getData();
                    if (locSet != null && locSet.hasName()) {
                        onDisplayLocationSetEditor(locSet);
                    }
                }
            }
        });
    }

    public void onDisplayLocationSetEditor(NamedSet<Location> locSet) {
        for (GenericListener<NamedSet<Location>> listener : LocationTypeAssignmentView.this.onDisplayLocationSetEditorListeners) {
            listener.onFiredEvent(locSet);
        }
    }

    /**
     * Set up an "assign" submenu item and the according listener.
     * 
     * @param parentMenu
     *            The menu into which the item should be inserted.
     * @param targetTestType
     *            The test type associated with the menu item. If null, the item
     *            will have the label LocationTyeEditorView.NONE.
     * @return The created menu item.
     */
    private MenuItem setupAssignSubmenuItem(Menu parentMenu, final TestType targetTestType) {
        final MenuItem typeItem = new MenuItem(parentMenu, SWT.PUSH);

        if (targetTestType != null) {
            typeItem.setText(targetTestType.getDisplayName());
            typeItem.setData(targetTestType);
        }
        else {
            typeItem.setText(ASSIGN_NONE);
        }
        typeItem.setEnabled(false);

        typeItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {

                Set<NamedSet<Location>> updateMap = new HashSet<>();

                for (TreeItem ti : groupEditor.getSelection()) {
                    if (ti.isDisposed()) {
                        System.err.println("ERROR: Encountered disposed item within selection.");
                        continue;
                    }
                    if (ti.getParentItem() == null) {
                        // root item, leave unchanged
                        continue;
                    }
                    TestType origTestType = (TestType) ti.getParentItem().getData();
                    if (origTestType != targetTestType) {
                        @SuppressWarnings("unchecked")
                        NamedSet<Location> locSet = (NamedSet<Location>) ti.getData();
                        updateMap.add(locSet);
                    }
                }

                for (SetTestTypeListener listener : onSetTestTypeListeners) {
                    for (NamedSet<Location> nls : updateMap) {
                        listener.setTestType(nls, targetTestType);
                    }
                }
            }

        });

        return typeItem;
    }

    /**
     * Construct the "Assign" submenu for the groupEditor.
     * 
     * @param parentMenu
     *            The "Assign" submenu's Menu instance.
     * @return The set of MenuItem instances constructed by this method.
     */
    private Set<MenuItem> setupGroupEditorAssignMenuTypeItems(Menu parentMenu) {
        Set<MenuItem> result = new HashSet<MenuItem>();
        for (final TestType targetTestType : allTestTypes) {
            result.add(setupAssignSubmenuItem(parentMenu, targetTestType));
        }
        return result;
    }

    /**
     * Remove all displayed location sets and test types.
     */
    public void reset() {
        groupEditor.removeAll();
        currentItemMap.clear();
        currentParentMap.clear();
    }

    /**
     * Display all test types known to the LocationTypeAssignmentView instance.
     */
    public void useAllTestTypes() {
        useTestTypes(allTestTypes);
    }

    /**
     * Display a subset of the test types known to the
     * LocationTypeAssignmentView instance.
     * 
     * @param types
     *            A subset (in an object-identity sense) of the test types
     *            passed to the instance during construction.
     */
    public void useTestTypes(Collection<TestType> types) {
        /* Maintain the order of test types given by allTestTypes */
        for (TestType tt : allTestTypes) {
            if (!types.contains(tt)) {
                continue;
            }
            if (!currentParentMap.containsKey(types)) {
                TreeItem parentItem = new TreeItem(groupEditor, SWT.NONE);
                if (tt != null) {
                    parentItem.setText(tt.getDisplayName());
                }
                else {
                    parentItem.setText(NO_TYPE_ASSIGNED);
                }
                parentItem.setData(tt);
                currentParentMap.put(tt, parentItem);
            }
        }
    }

    /**
     * Adds a location set to the view.
     * 
     * @param locationSet
     *            The location set.
     * @param type
     *            The test type associated with the location.
     * @throws {@link IllegalArgumentException} when the test type is not among
     *         those supplied to the instance during construction.
     */
    public void addLocationSet(NamedSet<Location> locationSet, TestType type) {
        String itemName;
        itemName = locationSet.getDisplayName();

        TreeItem parent = currentParentMap.get(type);

        /* Only allow known test types */
        if (parent == null) {
            String typeName;
            if (type == null) {
                typeName = "(none)";
            }
            else {
                typeName = type.getInternalName();
            }
            throw new IllegalArgumentException("Unknown test type " + typeName);
        }

        /*
         * If already present, either no change at all is necessary or the item
         * has to be removed first.
         */
        if (currentItemMap.containsKey(locationSet)) {
            if (currentItemMap.get(locationSet).getParentItem() == currentParentMap.get(type)) {
                return;
            }
            removeLocationSet(locationSet);
        }

        TreeItem locItem = new TreeItem(parent, SWT.NONE, ViewUtilities.determineInsertionIndex(
                parent, itemName));
        locItem.setText(itemName);
        locItem.setData(locationSet);

        if (locationSet.hasName()) {
            locItem.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_BLUE));
        }

        currentItemMap.put(locationSet, locItem);
    }

    /**
     * Remove a location set from the view.
     * 
     * @param locationSet
     *            The location set to be removed.
     * @throws {@link IllegalArgumentException} when the location set to be
     *         removed is not among the ones currently displayed.
     */
    public void removeLocationSet(NamedSet<Location> locationSet) {
        if (!currentItemMap.containsKey(locationSet)) {
            throw new IllegalArgumentException("Unknown item " + locationSet);
        }
        else {
            TreeItem matchingTreeItem = currentItemMap.get(locationSet);
            matchingTreeItem.dispose();
            currentItemMap.remove(locationSet);
        }
    }

    /**
     * Add a listener getting fired when the user requests to set a location's
     * test type within a test type group.
     * 
     * @param listener
     *            The listener to be be added.
     */
    public void addSetTestTypeListener(SetTestTypeListener listener) {
        onSetTestTypeListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the user requests to use the location
     * set editor.
     * 
     * @param listener
     *            The listener to be added, taking a generic set as an argument
     *            which should be selected by the location set editor. The
     *            listener's argument may be null.
     */
    public void addDisplayLocationSetEditorListener(GenericListener<NamedSet<Location>> listener) {
        onDisplayLocationSetEditorListeners.add(listener);
    }

    /**
     * @return The set of currently displayed locations sets.
     */
    protected Set<NamedSet<Location>> getLocationSets() {
        return currentItemMap.keySet();
    }

    /**
     * Expands or collapses a test type within the assignment tree.
     * 
     * @param type
     *            The {@link TestType} whose item needs to be expanded or
     *            collapsed.
     * @param expanded
     *            <tt>true</tt> iff the item should be expanded.
     */
    public void setExpanded(TestType type, boolean expanded) {
        if (currentParentMap.containsKey(type)) {
            currentParentMap.get(type).setExpanded(expanded);
        }
    }

    /**
     * @param locationSet
     *            A {@link NamedSet} of locations.
     * @return <tt>true</tt> iff <i>locationSet</i> is currently displayed.
     */
    public boolean isDisplaying(NamedSet<Location> locationSet) {
        return currentItemMap.containsKey(locationSet);
    }

    public static interface SetTestTypeListener {
        public void setTestType(NamedSet<Location> locationSet, TestType targetTestType);
    }

    @Override
    public void setEnabled(boolean enabledness) {
        super.setEnabled(enabledness);
        groupEditor.setEnabled(enabledness);
    }

    @Override
    public void setRedraw(boolean redraw) {
        super.setRedraw(redraw);
        groupEditor.setRedraw(redraw);
    }

    @Override
    public void redraw() {
        super.redraw();
        groupEditor.redraw();
    }
}
