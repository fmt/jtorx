package utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit;

import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;

/**
 * The public interface of the location type editor.
 */
public interface LocationTypeEditor {
    /**
     * @return The current set of named location sets.
     */
    Set<NamedSet<Location>> getLocationSets();

    /**
     * @return The set of test type groups.
     */
    Set<UITestTypeGroup> getTestTypeGroups();

    /**
     * @return The set of available locations.
     */
    Set<NamedSet<Location>> getLocations();

    /**
     * Displays the (modal) location type editor window.
     */
    void open(Runnable onCloseCallback);
}
