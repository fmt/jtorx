package utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit;

import utwente.fmt.jtorx.testgui.lazyotf.config.shared.EditorInvocation;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;

/**
 * A factory interface for {@link LocationTypeEditor}.
 * {@link LocationTypeEditorImpl} provides an implementation of this interface
 * via <i>getFactory()</i>.
 */
public interface LocationTypeEditorFactory {
    /**
     * Make a new {@link LocationTypeEditor} instance.
     * 
     * @param sessionData
     *            The configuration session data. This object does not get
     *            modified by the editor.
     * @param editors
     *            An object allowing the invocation of the other editors.
     * @return A new {@link LocationTypeEditor} instance.
     */
    LocationTypeEditor makeLocationTypeEditor(LazyOTFConfigSessionData sessionData,
            EditorInvocation editors);
}
