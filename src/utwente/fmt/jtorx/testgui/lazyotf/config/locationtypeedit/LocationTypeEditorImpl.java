package utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.LocationTypeEditorView.SetStrategyListener;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.LocationTypeEditorView.SetPath2WeightEnabledListener;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.EditorInvocation;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.RenameListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.SetEnabledListener;

/**
 * Controller class for the Location Type Editor. Instances of this class are
 * the glue between the configuration state and the concrete GUI.
 */
public class LocationTypeEditorImpl implements LocationTypeEditor {
    /** The set of available test types. */
    private final Set<TestType> testTypes;

    /** The set of test type groups. */
    private final Set<UITestTypeGroup> groups;

    /** The "toplevel" part of the view. */
    private LocationTypeEditorView view;

    /** The window object. */
    private final LocationTypeEditorWindow window;

    /** The control for test type group modification. */
    private TestTypeGroupView testTypeGroupView;

    /** The sub-controller handling test type group modification. */
    private TestTypeGroupCtrl testTypeGroupController;

    /** Methods for invoking the other editors. */
    private final EditorInvocation editors;

    /** The configuration session data. */
    private final LazyOTFConfigSessionData sessionData;

    /**
     * Construct a new LocationTypeEditor instance.
     * 
     * @param sessionData
     *            The configuration's session data.
     * @param editors
     *            An object allowing the invocation of the other editors.
     */
    public LocationTypeEditorImpl(LazyOTFConfigSessionData sessionData, EditorInvocation editors) {
        groups = new HashSet<>(sessionData.getTestTypeGroups());
        testTypes = new HashSet<>(sessionData.getTestTypes());
        this.sessionData = sessionData;
        this.editors = editors;
        window = (new LocationTypeEditorWindow.Factory()).makeInstance(this);
    }

    /**
     * Set the controller's view and initialize its listeners.
     * 
     * @param view
     *            The new view instance.
     */
    protected void setView(LocationTypeEditorView view) {
        this.view = view;
        testTypeGroupView = view.getTestTypeGroupView();
        testTypeGroupController = new TestTypeGroupCtrl(testTypeGroupView,
                new GenericListener<NamedSet<Location>>() {
                    @Override
                    public void onFiredEvent(NamedSet<Location> initialSelection) {
                        editors.displayLocationSetEditor(initialSelection);
                        testTypeGroupView.redraw();
                    }
                });

        view.addDisplayDecisionStrategyListener(new Runnable() {
            @Override
            public void run() {
                editors.displayDecisionStrategyEditor();
            }
        });

        view.addRenameTestTypeGroupListener(new RenameListener<UITestTypeGroup>() {
            @Override
            public void onRename(UITestTypeGroup instance, String newName) {
                instance.setDisplayName(newName);
            }
        });

        view.addRemoveTestTypeGroupListener(new GenericListener<UITestTypeGroup>() {
            @Override
            public void onFiredEvent(UITestTypeGroup g) {
                removeTestTypeGroup(g);
            }
        });

        /*
         * view.addLoadTestTypeGroupsListener(new GenericListener<String>() {
         * 
         * @Override public void onFiredEvent(String filename) { try {
         * loadTestTypeGroupsFromFile(filename); } catch (FileNotFoundException
         * | ParseException | IllegalSemanticsException e) {
         * LocationTypeEditor.this.view.displayErrorMessage(e.toString()); } }
         * });
         * 
         * view.addStoreTestTypeGroupsListener(new GenericListener<String>() {
         * 
         * @Override public void onFiredEvent(String filename) { try {
         * storeTestTypeGroupsToFile(filename); } catch (IOException |
         * ExportException e) {
         * LocationTypeEditor.this.view.displayErrorMessage(e.toString()); } }
         * });
         */

        view.addSetTestTypeGroupEnabledListener(new SetEnabledListener<UITestTypeGroup>() {
            @Override
            public void onSetEnabled(UITestTypeGroup instance, boolean newValue) {
                instance.setEnabled(newValue);
            }
        });

        view.addMakeTestTypeGroupListener(new Runnable() {
            @Override
            public void run() {
                UITestTypeGroup newTTG = LocationTypeEditorImpl.this.makeTestTypeGroup();
                LocationTypeEditorImpl.this.view.addTestTypeGroupToSelector(newTTG);
            }
        });

        view.addSetStrategyListener(new SetStrategyListener() {
            @Override
            public void setStrategy(UITestTypeGroup ttg, UIDecisionStrategy strategy) {
                LocationTypeEditorImpl.this.setStrategy(ttg, strategy);
            }
        });

        view.addSelectTestTypeGroupListener(new GenericListener<UITestTypeGroup>() {
            @Override
            public void onFiredEvent(UITestTypeGroup event) {
                selectTestTypeGroup(event);
            }
        });

        view.addSetPath2WeightEnabledListener(new SetPath2WeightEnabledListener() {
            @Override
            public void setPath2WeightEnabled(UITestTypeGroup ttg, Path2Weight gm,
                    boolean enabled) {
                if (enabled) {
                    ttg.addPath2Weight(gm);
                }
                else {
                    ttg.removePath2Weight(gm);
                }
            }
        });
    }

    /**
     * @return the list of decision strategies managed by the main controller.
     */
    public List<UIDecisionStrategy> getDecisionStrategies() {
        return sessionData.getStrategies();
    }

    @Override
    public Set<NamedSet<Location>> getLocationSets() {
        return sessionData.getLocationSets();
    }

    @Override
    public Set<UITestTypeGroup> getTestTypeGroups() {
        return groups;
    }

    @Override
    public Set<NamedSet<Location>> getLocations() {
        return sessionData.getBasisLocationSets();
    }

    /**
     * @return The set of available test types.
     */
    protected Set<TestType> getTestTypes() {
        return testTypes;
    }

    /**
     * Make a new test type group and add it to the set of enabled test type
     * groups. The new group's name is set to "New Group" and no associations of
     * location sets and test types are set. All known test types are passed to
     * the new group.
     * 
     * @return a fresh test type group with no location assignments.
     */
    private UITestTypeGroup makeTestTypeGroup() {
        TestTypeGroup g = new TestTypeGroup(testTypes);
        g.setName("New Group");
        UITestTypeGroup uig = new UITestTypeGroup(g, sessionData);
        groups.add(uig);
        return uig;
    }

    /**
     * Removes a test type group from the sets of test type groups.
     * 
     * @param g
     *            The test type group to be removed.
     */
    private void removeTestTypeGroup(UITestTypeGroup g) {
        if (groups.contains(g)) {
            groups.remove(g);
        }
    }

    /**
     * Display a test type group in the test type group editor control.
     * 
     * @param g
     *            The test type group which should be displayed.
     */
    private void selectTestTypeGroup(UITestTypeGroup g) {
        if (g != null) {
            testTypeGroupController.setTestTypeGroup(g);
        }
        else {
            testTypeGroupController.clear();
        }
    }

    /**
     * Set a test type group's strategy.
     * 
     * @param group
     *            The target test type group.
     * @param strategy
     *            The group's new strategy.
     */
    private void setStrategy(UITestTypeGroup group, UIDecisionStrategy strategy) {
        if (group != null) {
            group.setStrategy(strategy);
        }
    }

    /**
     * @return The configuration session data currently used by the test type
     *         group editor.
     */
    protected LazyOTFConfigSessionData getConfigSessionData() {
        return sessionData;
    }

    /**
     * @return a factory object for {@link LocationTypeEditorImpl}.
     */
    public static LocationTypeEditorFactory getFactory() {
        return new LocationTypeEditorFactory() {
            @Override
            public LocationTypeEditorImpl makeLocationTypeEditor(
                    LazyOTFConfigSessionData sessionData,
                    EditorInvocation editors) {
                return new LocationTypeEditorImpl(sessionData, editors);
            }
        };
    }

    @Override
    public void open(Runnable onCloseCallback) {
        window.open(onCloseCallback);
    }
}
