package utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.RenameListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.SetEnabledListener;

/**
 * The Location Type Editor's view component. This class mainly deals with test
 * type group selection and group properties and uses a
 * {@link LocationTypeAssignmentView} control for the actual test type
 * associations.
 */
class LocationTypeEditorView extends Composite {
    /** The Tree displaying test type group list. */
    private final Tree groupSelector;

    /** The TreeEditor used for renaming test type groups. */
    private final TreeEditor groupNameEditor;

    /** The window's tool bar. */
    private final ToolBar toolBar;

    /**
     * A mapping from test type groups to their corresponding tree item in
     * groupSelector.
     */
    private Map<UITestTypeGroup, TreeItem> groupsToTreeItems = new HashMap<>();

    /** The "Rename" toolbar button. */
    private ToolItem renameGroupTBI = null;

    /** The "Remove" toolbar button. */
    private ToolItem removeGroupTBI = null;

    /** The test type group editor control. */
    private final TestTypeGroupView testTypeGroupEditor;

    /** Set of listeners for making new test type groups. */
    private final Set<Runnable> onMakeTestTypeGroupListeners;

    /** Set of listeners for removing test type groups. */
    private final Set<GenericListener<UITestTypeGroup>> onRemoveTestTypeGroupListeners;

    /** Set of listeners for setting the enabledness of test type groups. */
    private final Set<SetEnabledListener<UITestTypeGroup>> onSetTestTypeGroupEnabledListeners;

    /** Set of listeners for renaming test type groups. */
    private final Set<RenameListener<UITestTypeGroup>> onRenameTestTypeGroupListeners;

    /** Set of listeners for changing test type group strategies. */
    private final Set<SetStrategyListener> onSetStrategyListeners;

    /** Set of listeners for changes in the {@link Path2Weight} selection. */
    private final Set<SetPath2WeightEnabledListener> onSetPath2WeightEnabledListeners;

    /**
     * Set of listeners for handling requests to display the decision strategy
     * editor.
     */
    private final Set<Runnable> onDisplayDecisionStrategyEditorListeners;

    /** Set of listeners for test type group selection. */
    private final Set<GenericListener<UITestTypeGroup>> onSelectTestTypeGroupListeners;

    /** An interface to certain query-type functions of the controller. */
    private final CtrlInterface ctrlIf;

    private static final String DEFAULT_STRATEGY_NAME = "(default)";

    /**
     * Construct a new LocationTypeEditorView instance.
     *       Label nodeValueWeigherLabel = new Label(basicSettingsGrp, SWT.NONE);
        nodeValueWeigherLabel.setText("NodeValue2Weight:");
     * @param parentWidget
     *            The editor's parent widget.
     * @param ctrlIf
     *            A {@link CtrlInterface} instance for controller access.
     */
    public LocationTypeEditorView(Composite parentWidget, final CtrlInterface ctrlIf) {
        super(parentWidget, SWT.NONE);

        this.ctrlIf = ctrlIf;

        GridLayout grid = new GridLayout();
        grid.numColumns = 1;
        setLayout(grid);
        GridData editorLayoutData = new GridData(GridData.FILL_BOTH);

        toolBar = new ToolBar(this, SWT.FLAT | SWT.WRAP | SWT.RIGHT);
        Composite editorPart = new Composite(this, SWT.NONE);
        editorPart.setLayoutData(editorLayoutData);
        groupSelector = new Tree(editorPart, SWT.BORDER | SWT.CHECK | SWT.V_SCROLL);
        groupNameEditor = new TreeEditor(groupSelector);
        testTypeGroupEditor = new TestTypeGroupView(editorPart, ctrlIf.getTestTypes(),
                new TestTypeGroupView.CtrlInterface() {

                    @Override
                    public Set<NamedSet<Location>> getNamedLocationSets() {
                        return ctrlIf.getNamedLocationSets();
                    }

                    @Override
                    public Set<NamedSet<Location>> getLocations() {
                        return ctrlIf.getLocations();
                    }
                });

        onMakeTestTypeGroupListeners = new HashSet<>();
        onRemoveTestTypeGroupListeners = new HashSet<>();
        onRenameTestTypeGroupListeners = new HashSet<>();
        onSetTestTypeGroupEnabledListeners = new HashSet<>();
        onSetStrategyListeners = new HashSet<>();
        onDisplayDecisionStrategyEditorListeners = new HashSet<>();
        onSelectTestTypeGroupListeners = new HashSet<>();
        onSetPath2WeightEnabledListeners = new HashSet<>();

        setupToolbar();
        setupGroupSelector();
        setupGroupNameEditor();

        setupSashAreaLayout(editorPart);

        this.pack();
    }

    /**
     * Initialize the control's toolbar and set up its buttons.
     */
    private void setupToolbar() {

        final ToolItem newItem = new ToolItem(toolBar, SWT.PUSH);
        newItem.setText("Add");
        newItem.setToolTipText("Add a new test objective");
        final ToolItem remItem = new ToolItem(toolBar, SWT.PUSH);
        remItem.setText("Remove");
        remItem.setToolTipText("Remove the selected test objective");
        final ToolItem renItem = new ToolItem(toolBar, SWT.PUSH);
        renItem.setText("Rename");
        renItem.setToolTipText("Rename the selected test objective");

        toolBar.pack();

        newItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onMakeTestTypeGroup();
            }
        });

        remItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onRemoveTestTypeGroup();
            }
        });

        renItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onRenameTestTypeGroup();
            }
        });

        groupSelector.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                boolean enabled = (groupSelector.getSelectionCount() > 0);
                boolean deleteEnabled = enabled & isDeletable(groupSelector.getSelection());
                renItem.setEnabled(enabled);
                remItem.setEnabled(deleteEnabled);
            }
        });

        renItem.setEnabled(false);
        remItem.setEnabled(false);

        removeGroupTBI = remItem;
        renameGroupTBI = renItem;
    }

    /**
     * Determine whether the given array of {@link TreeItem} instances contains
     * one associated with an non-removable test type group. A test type group
     * is deemed removable iff it is not the only existing test type group.
     * 
     * @param selection
     *            The tree items associated with the test type groups to be
     *            deleted.
     * @return true iff the {@link TreeItem} instances may be removed from the
     *         editor.
     */
    private boolean isDeletable(TreeItem[] selection) {
        return groupSelector.getItemCount() > 1;
    }

    /**
     * Set up the sash area's layout. Currently, it uses a Sash to divide the
     * drawing area into two parts, each containing a Tree widget - hence the
     * FormLayout.
     * 
     * @param sashAreaParent
     *            the sash area's parent.
     */
    private void setupSashAreaLayout(final Composite sashAreaParent) {
        Sash vSash = new Sash(sashAreaParent, SWT.BORDER | SWT.VERTICAL);
        sashAreaParent.setLayout(new FormLayout());

        FormData gSelFD = new FormData();
        gSelFD.left = new FormAttachment(0, 0);
        gSelFD.right = new FormAttachment(vSash, 0);
        gSelFD.top = new FormAttachment(0, 0);
        gSelFD.bottom = new FormAttachment(100, 0);
        groupSelector.setLayoutData(gSelFD);

        FormData gEditFD = new FormData();
        gEditFD.left = new FormAttachment(vSash, 0);
        gEditFD.right = new FormAttachment(100, 0);
        gEditFD.top = new FormAttachment(0, 0);
        gEditFD.bottom = new FormAttachment(100, 0);
        testTypeGroupEditor.setLayoutData(gEditFD);

        final FormData sashFD = new FormData();
        sashFD.left = new FormAttachment(50, 0);
        sashFD.top = new FormAttachment(0, 0);
        sashFD.bottom = new FormAttachment(100, 0);
        vSash.setLayoutData(sashFD);

        vSash.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                sashFD.left = new FormAttachment(0, ev.x);
                sashAreaParent.layout();
            }
        });

        sashAreaParent.pack();
    }

    /**
     * Initialize the groupSelector tree control by populating it with test type
     * groups, adding selection listeners and constructing a context menu.
     */
    private void setupGroupSelector() {
        /* Add all known test type groups to the group selector */
        for (UITestTypeGroup g : ctrlIf.getTestTypeGroups()) {
            addTestTypeGroupToSelector(g);
        }

        /* Test type group selected ==> Display it in the editor */
        groupSelector.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                TreeItem item = (TreeItem) ev.item;
                UITestTypeGroup g = (UITestTypeGroup) item.getData();

                if (ev.detail == SWT.CHECK) {
                    onToggleTestTypeGroupEnabledness();
                }

                onSelectTestTypeGroup(g);
            }
        });

        /* Set up a context menu */
        Menu gselMenu = new Menu(groupSelector);
        groupSelector.setMenu(gselMenu);
        setupGroupSelectorMenu(gselMenu);
    }

    /**
     * Set up the "Use Strategy" submenu.
     * 
     * @param useStrategySubmenu
     *            The parent menu.
     * @param strategyMenuMap
     *            An empty map getting filled with DecisionStrategy -> MenuItem
     *            associations.
     */
    private void setupUseStrategySubmenu(final Menu useStrategySubmenu,
            final Map<UIDecisionStrategy, MenuItem> strategyMenuMap) {
        strategyMenuMap.clear();
        for (MenuItem mi : useStrategySubmenu.getItems()) {
            mi.dispose();
        }

        for (final UIDecisionStrategy strategy : ctrlIf.getDecisionStrategies()) {
            final MenuItem strategyMI = new MenuItem(useStrategySubmenu, SWT.RADIO);
            strategyMI.setText(strategy.getDisplayName());
            strategyMI.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent ev) {
                    if (strategyMI.getSelection() == true) {
                        for (SetStrategyListener listener : onSetStrategyListeners) {
                            listener.setStrategy(getSelectedTestTypeGroup(), strategy);
                        }
                    }
                }
            });
            strategyMenuMap.put(strategy, strategyMI);
        }

        final MenuItem noStrategyEntry = new MenuItem(useStrategySubmenu, SWT.RADIO);
        noStrategyEntry.setText(DEFAULT_STRATEGY_NAME);
        noStrategyEntry.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (SetStrategyListener listener : onSetStrategyListeners) {
                    listener.setStrategy(getSelectedTestTypeGroup(), null);
                }
            }
        });
        strategyMenuMap.put(null, noStrategyEntry);

        @SuppressWarnings("unused")
        final MenuItem separator = new MenuItem(useStrategySubmenu, SWT.SEPARATOR);
        final MenuItem displayStrategyEditorMI = new MenuItem(useStrategySubmenu, SWT.PUSH);
        displayStrategyEditorMI.setText("Edit Decision Strategies...");
        displayStrategyEditorMI.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                for (Runnable listener : onDisplayDecisionStrategyEditorListeners) {
                    listener.run();
                }
                setupUseStrategySubmenu(useStrategySubmenu, strategyMenuMap);
            }
        });

    }

    /**
     * Set up the "Use {@link Path2Weight}" submenu.
     * 
     * @param menu
     *            The supermenu.
     * @param path2WeightMenuMap
     *            A map into which the associations between tree trace weighers
     *            and menu items get stored.
     */
    private void setupUsePath2WeightSubmenu(Menu menu,
            Map<Path2Weight, MenuItem> path2WeightMenuMap) {
        for (final Path2Weight gm : ctrlIf.getTestTreeTraceWeighers()) {
            final MenuItem newMI = new MenuItem(menu, SWT.CHECK);
            newMI.setText(gm.getDisplayName());
            newMI.setSelection(false);
            path2WeightMenuMap.put(gm, newMI);

            newMI.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent ev) {
                    for (SetPath2WeightEnabledListener l : onSetPath2WeightEnabledListeners) {
                        l.setPath2WeightEnabled(getSelectedTestTypeGroup(), gm,
                                newMI.getSelection());
                    }
                }
            });
        }
    }

    /**
     * Construct the groupSelector control's context menu.
     * 
     * @param parentMenu
     *            The context menu's parent Menu instance.
     */
    private void setupGroupSelectorMenu(Menu parentMenu) {
        final MenuItem addMI = new MenuItem(parentMenu, SWT.PUSH);
        addMI.setText("Add test objective");
        final MenuItem removeMI = new MenuItem(parentMenu, SWT.PUSH);
        removeMI.setText("Remove test objective");
        final MenuItem renameMI = new MenuItem(parentMenu, SWT.PUSH);
        renameMI.setText("Rename test objective...");
        final MenuItem enableMI = new MenuItem(parentMenu, SWT.CHECK);
        enableMI.setText("Enabled");
        final MenuItem useStrategyMI = new MenuItem(parentMenu, SWT.CASCADE);
        useStrategyMI.setText("Use Decision Strategy");
        final MenuItem useGuidanceMI = new MenuItem(parentMenu, SWT.CASCADE);
        useGuidanceMI.setText("Use Path2Weight");

        /* Construct strategy submenu */
        final Menu useStrategySubmenu = new Menu(useStrategyMI);
        useStrategyMI.setMenu(useStrategySubmenu);
        final Map<UIDecisionStrategy, MenuItem> strategyMenuMap = new HashMap<>();
        setupUseStrategySubmenu(useStrategySubmenu, strategyMenuMap);

        /* Construct {@link Path2Weight} submenu */
        final Menu usePath2WeightSubmenu = new Menu(useGuidanceMI);
        useGuidanceMI.setMenu(usePath2WeightSubmenu);
        final Map<Path2Weight, MenuItem> path2WeightMenuMap = new HashMap<>();
        setupUsePath2WeightSubmenu(usePath2WeightSubmenu, path2WeightMenuMap);

        /* New test group PUSH listener */
        addMI.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onMakeTestTypeGroup();
            }
        });

        /* Delete test group PUSH listener */
        removeMI.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onRemoveTestTypeGroup();
            }
        });

        /* Rename test group PUSH listener */
        renameMI.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onRenameTestTypeGroup();
            }
        });

        /* Set active/inactive listener */
        enableMI.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onToggleTestTypeGroupEnabledness();
            }
        });

        /* Gray-out and ttg-specific menu item setup */
        parentMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuShown(MenuEvent ev) {
                boolean enabled = (groupSelector.getSelectionCount() > 0);
                addMI.setEnabled(enabled);
                removeMI.setEnabled(enabled && isDeletable(groupSelector.getSelection()));
                renameMI.setEnabled(enabled);
                enableMI.setEnabled(enabled);
                useStrategyMI.setEnabled(enabled);
                useGuidanceMI.setEnabled(enabled);

                if (enabled) {
                    TreeItem currentSelection = groupSelector.getSelection()[0];
                    UITestTypeGroup ttg = (UITestTypeGroup) currentSelection.getData();
                    enableMI.setSelection(ttg.isEnabled());

                    for (MenuItem it : strategyMenuMap.values()) {
                        it.setSelection(false);
                    }
                    if (ttg.getStrategy() == null) {
                        strategyMenuMap.get(null).setSelection(true);
                    }
                    else {
                        if (strategyMenuMap.containsKey(ttg.getStrategy())) {
                            strategyMenuMap.get(ttg.getStrategy()).setSelection(true);
                        }
                    }

                    for (Path2Weight gm : path2WeightMenuMap.keySet()) {
                        MenuItem mi = path2WeightMenuMap.get(gm);
                        mi.setSelection(ttg.getPath2Weights().contains(gm));
                    }
                }
                else {
                    enableMI.setSelection(false);
                }
            }
        });
    }

    /**
     * Set up the test type group name editing mechanism.
     */
    private void setupGroupNameEditor() {
        groupNameEditor.horizontalAlignment = SWT.LEFT;
        groupNameEditor.grabHorizontal = true;
        groupNameEditor.minimumWidth = 100;

        groupSelector.addListener(SWT.MouseDoubleClick, new Listener() {
            @Override
            public void handleEvent(Event ev) {

                TreeItem[] currentSelection = groupSelector.getSelection();
                if (currentSelection.length == 1) {
                    TreeItem selectedItem = currentSelection[0];
                    displayNameEditor(selectedItem);
                }

            }
        });

        groupSelector.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                hideNameEditor();
            }
        });
    }

    /**
     * Event handler: Make a new test type group. Issues the instantiation of a
     * new test type group and adds it to the group selector control.
     */
    private void onMakeTestTypeGroup() {
        for (Runnable listener : onMakeTestTypeGroupListeners) {
            listener.run();
        }
    }

    /**
     * Event handler: Remove the currently selected test type group.
     */
    private void onRemoveTestTypeGroup() {
        TreeItem[] currentSelection = groupSelector.getSelection();
        assert (currentSelection.length == 1);
        TreeItem selectedItem = currentSelection[0];
        UITestTypeGroup toBeDeleted = (UITestTypeGroup) selectedItem.getData();

        for (GenericListener<UITestTypeGroup> listener : onRemoveTestTypeGroupListeners) {
            listener.onFiredEvent(toBeDeleted);
        }

        selectedItem.dispose();
        groupsToTreeItems.remove(toBeDeleted);

        if (groupSelector.getSelectionCount() == 0) {
            onNoGroupSelected();
        }
    }

    /**
     * Event handler: Toggle the enabledness of the currently selected test type
     * group. A new value is assigned to the selected item's checkbox.
     */
    private void onToggleTestTypeGroupEnabledness() {
        TreeItem[] currentSelection = groupSelector.getSelection();
        assert (currentSelection.length == 1);
        TreeItem selectedItem = currentSelection[0];
        UITestTypeGroup toBeSwitched = (UITestTypeGroup) selectedItem.getData();

        for (SetEnabledListener<UITestTypeGroup> listener : onSetTestTypeGroupEnabledListeners) {
            listener.onSetEnabled(toBeSwitched, !toBeSwitched.isEnabled());
        }

        groupsToTreeItems.get(toBeSwitched).setChecked(toBeSwitched.isEnabled());
    }

    /**
     * Event handler: No group selected ~> disable buttons and clear right pane.
     */
    private void onNoGroupSelected() {
        renameGroupTBI.setEnabled(false);
        removeGroupTBI.setEnabled(false);
        onSelectTestTypeGroup(null);
    }

    /**
     * Event handler: Display the test type group name editing control for the
     * currently selected test type group.
     */
    private void onRenameTestTypeGroup() {
        TreeItem[] currentSelection = groupSelector.getSelection();
        assert (currentSelection.length == 1);
        TreeItem selectedItem = currentSelection[0];
        hideNameEditor();
        displayNameEditor(selectedItem);
    }

    /**
     * Add a test type group to the groupSelector control. Updates the
     * groupsToTreeItems map.
     * 
     * @param group
     *            The test type group to be added.
     */
    public void addTestTypeGroupToSelector(UITestTypeGroup group) {
        TreeItem groupsItem = new TreeItem(groupSelector, SWT.NONE);
        groupsItem.setText(group.getDisplayName());
        groupsItem.setData(group);
        groupsItem.setChecked(group.isEnabled());
        groupsToTreeItems.put(group, groupsItem);
    }

    private void onSelectTestTypeGroup(UITestTypeGroup group) {
        for (GenericListener<UITestTypeGroup> listener : onSelectTestTypeGroupListeners) {
            listener.onFiredEvent(group);
        }
    }

    /**
     * @return Return the currently selected test type group.
     */
    private UITestTypeGroup getSelectedTestTypeGroup() {
        if (groupSelector.getSelectionCount() > 0) {
            TreeItem ti = groupSelector.getSelection()[0];
            return (UITestTypeGroup) ti.getData();
        }
        else {
            return null;
        }
    }

    /**
     * Display the test type group name editor control above the given tree
     * item.
     * 
     * @param item
     *            The tree item above which the editor control should be
     *            displayed.
     */
    private void displayNameEditor(final TreeItem item) {
        final UITestTypeGroup ttg = (UITestTypeGroup) item.getData();

        if (groupNameEditor.getEditor() != null) {
            groupNameEditor.getEditor().dispose();
        }

        Text newEditor = new Text(groupSelector, SWT.NONE);
        newEditor.setText(item.getText());
        newEditor.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent ev) {
                Text currentText = (Text) groupNameEditor.getEditor();
                String currentTextContent = currentText.getText();
                for (RenameListener<UITestTypeGroup> listener : onRenameTestTypeGroupListeners) {
                    listener.onRename(ttg, currentTextContent);
                }
                groupNameEditor.getItem().setText(currentText.getText());
            }
        });
        newEditor.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent arg0) {
            }

            @Override
            public void focusLost(FocusEvent arg0) {
                hideNameEditor();
            }
        });
        newEditor.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
                hideNameEditor();
            }
        });
        newEditor.selectAll();
        newEditor.setFocus();
        groupNameEditor.setEditor(newEditor, item);
    }

    /**
     * Hide the test type group name editor.
     */
    private void hideNameEditor() {
        if (groupNameEditor.getEditor() != null) {
            groupNameEditor.getEditor().dispose();
        }
    }

    /**
     * Add a listener getting fired when the user requests to make a new test
     * type group.
     * 
     * @param listener
     *            The listener to be added.
     */
    public void addMakeTestTypeGroupListener(Runnable listener) {
        onMakeTestTypeGroupListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the user requests to remove a test type
     * group.
     * 
     * @param listener
     *            A listener taking the test type group to be removed as an
     *            argument.
     */
    public void addRemoveTestTypeGroupListener(GenericListener<UITestTypeGroup> listener) {
        onRemoveTestTypeGroupListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the user requests to change the
     * enabledness of a test type group.
     * 
     * @param listener
     *            The listener to be added.
     */
    public void addSetTestTypeGroupEnabledListener(SetEnabledListener<UITestTypeGroup> listener) {
        onSetTestTypeGroupEnabledListeners.add(listener);
    }

    /**
     * Adds a listener getting fired when the user changes the {@link Path2Weight}od
     * selection for the currently selected test type group.
     * 
     * @param it
     *            The listener to be added.
     */
    public void addSetPath2WeightEnabledListener(SetPath2WeightEnabledListener it) {
        onSetPath2WeightEnabledListeners.add(it);
    }

    /**
     * Add a listener getting fired when the user requests to rename a test type
     * group.
     * 
     * @param listener
     *            The renaming listener to be added.
     */
    public void addRenameTestTypeGroupListener(RenameListener<UITestTypeGroup> listener) {
        onRenameTestTypeGroupListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the user requests to use the decision
     * strategy editor.
     * 
     * @param listener
     *            The listener to be added.
     */
    public void addDisplayDecisionStrategyListener(Runnable listener) {
        onDisplayDecisionStrategyEditorListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the user requests to change a test
     * type's decision strategy.
     * 
     * @param listener
     *            The listener to be added.
     */
    public void addSetStrategyListener(SetStrategyListener listener) {
        onSetStrategyListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the user selects a currently unselected
     * test type group.
     * 
     * @param listener
     *            The listener to be added.
     */
    public void addSelectTestTypeGroupListener(GenericListener<UITestTypeGroup> listener) {
        onSelectTestTypeGroupListeners.add(listener);
    }

    /**
     * Display an error message box with an "okay" button.
     * 
     * @param msg
     *            The message to be displayed.
     */
    public void displayErrorMessage(String msg) {
        MessageBox mb = new MessageBox(getShell(), SWT.ERROR);
        mb.setMessage(msg);
        mb.setText("Error");
        mb.open();
    }

    /**
     * Get the view's locationset-testtype association control.
     * 
     * @return The locationset-testtype association control.
     */
    protected TestTypeGroupView getTestTypeGroupView() {
        return testTypeGroupEditor;
    }

    public static interface SetStrategyListener {
        public void setStrategy(UITestTypeGroup ttg, UIDecisionStrategy strategy);
    }

    public static interface SetPath2WeightEnabledListener {
        public void setPath2WeightEnabled(UITestTypeGroup ttg, Path2Weight gm,
                boolean enabled);
    }

    public static interface ChangeNamedLocationSetTestTypeListener {
        public void changeTestType(UITestTypeGroup ttg, NamedSet<Location> nls,
                TestType oldTestType, TestType newTestType);
    }

    public static class Factory {
        public LocationTypeEditorView makeInstance(Composite parentWidget, CtrlInterface ctrlIf) {
            return new LocationTypeEditorView(parentWidget, ctrlIf);
        }
    }

    /**
     * Interface to the controller for query-type calls.
     */
    public static interface CtrlInterface {
        List<UIDecisionStrategy> getDecisionStrategies();

        Set<NamedSet<Location>> getLocations();

        Set<TestType> getTestTypes();

        Set<UITestTypeGroup> getTestTypeGroups();

        Set<NamedSet<Location>> getNamedLocationSets();

        NamedSet<Location> getLocationSet(Location loc);

        Set<Path2Weight> getTestTreeTraceWeighers();
    }
}
