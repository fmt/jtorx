package utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit;

import java.util.List;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;

/**
 * The class responsible for managing the Location Type Editor window.
 */
class LocationTypeEditorWindow {
	/** The new shell. */
	private Shell ourShell;

	/** The current Location Type Editor controller. */
	private LocationTypeEditorImpl ctrl;

	/** The current Location Type Editor view. */
	private LocationTypeEditorView view;

	/** The view's factory. */
	private final LocationTypeEditorView.Factory viewFact;

	/**
	 * Construct a new LocationTypeEditorWindow instance.
	 * @param ctrl The location type editor's controller instance. A location
	 *   type editor view will be created and passed to the controller via 
	 *   a call to {@link LocationTypeEditor.setView()}.
	 */
	public LocationTypeEditorWindow(LocationTypeEditorImpl ctrl) {
		this(ctrl, null);
	}

	/**
	 * Construct a new LocationTypeEditorWindow instance.
	 * @param ctrl The controller to be used initially.
	 * @param viewFact The LocationTypeEditorView factory to be used (null for
	 *   default).
	 */
	public LocationTypeEditorWindow(final LocationTypeEditorImpl ctrl, LocationTypeEditorView.Factory viewFact) {
		this.ctrl = ctrl;
		this.viewFact = (viewFact != null) ? viewFact : new LocationTypeEditorView.Factory();

		final Shell parentShell = Display.getCurrent().getActiveShell();

		ourShell = new Shell(Display.getCurrent(), SWT.APPLICATION_MODAL | SWT.DIALOG_TRIM | SWT.RESIZE);
		ourShell.setLayout(new FillLayout());
		ourShell.setMinimumSize(480, 300);
		ourShell.setText("Test Objective Editor");

		ourShell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event ev) {
				ev.doit = false;
				ourShell.setVisible(false);
				parentShell.setEnabled(true);
			}
		});

		parentShell.setEnabled(false);
		this.view = this.viewFact.makeInstance(ourShell, new LocationTypeEditorView.CtrlInterface() {

			@Override
			public Set<TestType> getTestTypes() {
				return ctrl.getTestTypes();
			}

			@Override
			public Set<UITestTypeGroup> getTestTypeGroups() {
				return ctrl.getTestTypeGroups();
			}

			@Override
			public Set<NamedSet<Location>> getNamedLocationSets() {
				return ctrl.getLocationSets();
			}

			@Override
			public Set<NamedSet<Location>> getLocations() {
				return ctrl.getLocations();
			}

			@Override
			public List<UIDecisionStrategy> getDecisionStrategies() {
				return ctrl.getDecisionStrategies();
			}

			@Override
			public NamedSet<Location> getLocationSet(Location loc) {
				return ctrl.getConfigSessionData().getLocationSet(loc);
			}

			@Override
			public Set<Path2Weight> getTestTreeTraceWeighers() {
				return ctrl.getConfigSessionData().getPath2Weights();
			}
		});
		ctrl.setView(this.view);
	}

	public void open(final Runnable disposeListener) {
		if(disposeListener != null) {
			ourShell.addListener(SWT.Close, new Listener() {
				@Override
				public void handleEvent(Event ev) {
					disposeListener.run();
				}
			});
		}
		ourShell.pack();
		ourShell.open();	
	}

	/**
	 * Set the window's visibility.
	 * @param visibility true iff the window is to be made visible.
	 */
	public void setVisible(boolean visibility) {
		ourShell.setVisible(visibility);
	}

	/**
	 * @return true iff the window is visible.
	 */
	public boolean isVisible() {
		return ourShell.isVisible();
	}

	/**
	 * Equips the editor with a new controller (possibly containing other test
	 * type groups, locations and test types). The view is reset in any case.
	 * @param ctrl The new controller.
	 */
	public void setController(final LocationTypeEditorImpl ctrl) {
		view.dispose();
		this.ctrl = ctrl;
		view = viewFact.makeInstance(ourShell, new LocationTypeEditorView.CtrlInterface() {

			@Override
			public Set<TestType> getTestTypes() {
				return ctrl.getTestTypes();
			}

			@Override
			public Set<UITestTypeGroup> getTestTypeGroups() {
				return ctrl.getTestTypeGroups();
			}

			@Override
			public Set<NamedSet<Location>> getNamedLocationSets() {
				return ctrl.getLocationSets();
			}

			@Override
			public Set<NamedSet<Location>> getLocations() {
				return ctrl.getLocations();
			}

			@Override
			public List<UIDecisionStrategy> getDecisionStrategies() {
				return ctrl.getDecisionStrategies();
			}

			@Override
			public NamedSet<Location> getLocationSet(Location loc) {
				return ctrl.getConfigSessionData().getLocationSet(loc);
			}

			@Override
			public Set<Path2Weight> getTestTreeTraceWeighers() {
				return ctrl.getConfigSessionData().getPath2Weights();
			}
		});
		ctrl.setView(view);
	}

	/**
	 * @return The current Location Type Editor controller.
	 */
	public LocationTypeEditorImpl getController() {
		return this.ctrl;
	}

	/**
	 * @return The current Location Type Editor view.
	 */
	public LocationTypeEditorView getView() {
		return this.view;
	}

	public static class Factory {
		public LocationTypeEditorWindow makeInstance(LocationTypeEditorImpl ctrl) {
			return new LocationTypeEditorWindow(ctrl);
		}
	}
}
