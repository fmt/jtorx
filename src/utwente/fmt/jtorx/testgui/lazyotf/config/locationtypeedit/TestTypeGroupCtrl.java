package utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.LocationTypeAssignmentView.SetTestTypeListener;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * A controller class for handling the test type group operations and the test
 * type group location-set/test-type association view.
 */
public class TestTypeGroupCtrl {
    /** The test type group view. */
    private final TestTypeGroupView view;

    /** The currently edited test type group. */
    private UITestTypeGroup currentGroup;

    /**
     * A listener getting fired when the user requests to enter the location set
     * editor. If the user wishes to edit a concrete named location set, it is
     * passed as an argument (which is null otherwise).
     */
    private final GenericListener<NamedSet<Location>> locationSetEditor;

    /**
     * Construct a new {@link TestTypeGroupCtrl} instance.
     * 
     * @param view
     *            The view which needs to be controlled.
     * @param locationSetEditor
     *            A listener getting fired when the user requests to enter the
     *            location set editor. If the user wishes to edit a concrete
     *            named location set, it is passed as an argument (which is null
     *            otherwise).
     */
    public TestTypeGroupCtrl(TestTypeGroupView view,
            GenericListener<NamedSet<Location>> locationSetEditor) {
        this.view = view;
        currentGroup = null;
        this.locationSetEditor = locationSetEditor;
        setupListeners();
    }

    /**
     * Sets the currently displayed test type group.
     * 
     * @param group
     *            The new displayed test type group.
     */
    public void setTestTypeGroup(UITestTypeGroup group) {
        clear();
        currentGroup = group;
        view.displayTestTypeGroup(group);
    }

    /**
     * Gets the currently displayed test type group.
     * 
     * @return The currently displayed test type group.
     */
    public UITestTypeGroup getTestTypeGroup() {
        return currentGroup;
    }

    /**
     * Clear the associated view and set the currently edited test type group to
     * null.
     */
    public void clear() {
        view.reset();
        currentGroup = null;
    }

    /**
     * Handler for requests to enter the named location set editor.
     * 
     * @param it
     *            The location which the user wishes to edit; may be null.
     */
    private void onDisplayLocationSetEditor(NamedSet<Location> it) {
        if (currentGroup == null) {
            throw new IllegalStateException("currentGroup == null");
        }
        locationSetEditor.onFiredEvent(it);
    }

    /**
     * Handler for requests to set the test type of a location set.
     * 
     * @param locationSet
     *            The location set whose test type the user wishes to modify.
     * @param targetTestType
     *            The target {@link TestType} or null if no test type should be
     *            associated with the given location set.
     */
    private void onSetTestType(NamedSet<Location> locationSet, TestType targetTestType) {
        if (currentGroup == null) {
            throw new IllegalStateException("currentGroup == null");
        }

        if (targetTestType != null && !targetTestType.equals(TestType.ORDINARY)) {
            currentGroup.associate(locationSet, targetTestType);
        }
        else {
            currentGroup.removeAssociation(locationSet);
        }

        view.redraw();
    }

    /**
     * Glue the controller to the view.
     */
    private void setupListeners() {
        view.addDisplayLocationSetEditorListener(new GenericListener<NamedSet<Location>>() {
            @Override
            public void onFiredEvent(NamedSet<Location> event) {
                onDisplayLocationSetEditor(event);
            }
        });

        view.addSetTestTypeListener(new SetTestTypeListener() {

            @Override
            public void setTestType(NamedSet<Location> locationSet,
                    TestType targetTestType) {
                onSetTestType(locationSet, targetTestType);
            }
        });
    }
}
