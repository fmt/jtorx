package utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.widgets.Composite;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UITestTypeGroup;

/**
 * A location type assignment view geared towards test type groups.
 */
public class TestTypeGroupView extends LocationTypeAssignmentView {
    /** Indirect access to the view's controller. */
    private final CtrlInterface ctrlIf;

    /** The currently displayed test type group. */
    private UITestTypeGroup currentGroup;

    /**
     * Construct a new {@link TestTypeGroupView} instance.
     * 
     * @param parent
     *            The parent widget.
     * @param allTestTypes
     *            The set of test types available for editing.
     * @param ctrlIf
     *            An {@link CtrlInterface} instance providing indirect access to
     *            the view's controller.
     */
    public TestTypeGroupView(Composite parent, Set<TestType> allTestTypes, CtrlInterface ctrlIf) {
        super(parent, allTestTypes, false);
        this.ctrlIf = ctrlIf;
        currentGroup = null;
    }

    /**
     * Reset the view and display the given test type group.
     * 
     * @param group
     *            The new displayed test type group.
     */
    public void displayTestTypeGroup(UITestTypeGroup group) {
        reset();

        Set<TestType> usedTestTypes = new HashSet<>(group.getTestTypes());

        /*
         * if(!group.isEnforcingAssignment()) { usedTestTypes.add(null); }
         */

        currentGroup = group;

        useTestTypes(usedTestTypes);

        redraw();
    }

    @Override
    public void reset() {
        currentGroup = null;
        super.reset();
    }

    @Override
    public void redraw() {
        if (currentGroup != null) {
            Set<NamedSet<Location>> unseenLocs = new HashSet<>(ctrlIf.getLocations());

            /* The set of "dead", ie removed named location sets */
            unseenLocs.addAll(ctrlIf.getNamedLocationSets());

            Set<NamedSet<Location>> currentLocs = new HashSet<>(getLocationSets());

            // Remove dead location sets
            for (NamedSet<Location> removalCandidate : currentLocs) {
                if (!currentGroup.getLocations().contains(removalCandidate)) {
                    removeLocationSet(removalCandidate);
                    if (ctrlIf.getLocations().contains(removalCandidate)
                            || ctrlIf.getNamedLocationSets().contains(removalCandidate)) {
                        addLocationSet(removalCandidate, TestType.ORDINARY);
                        unseenLocs.remove(removalCandidate);
                    }
                }
                else {
                    unseenLocs.remove(removalCandidate);
                }
            }

            // Add previously unknown location sets
            for (NamedSet<Location> addCandidate : currentGroup.getLocations()) {
                addLocationSet(addCandidate, currentGroup.getTestType(addCandidate));
                if (unseenLocs.contains(addCandidate)) {
                    unseenLocs.remove(addCandidate);
                }
            }

            for (NamedSet<Location> unseenLoc : unseenLocs) {
                addLocationSet(unseenLoc, TestType.ORDINARY);
            }
        }
        super.redraw();
    }

    public static interface CtrlInterface {
        Set<NamedSet<Location>> getLocations();

        Set<NamedSet<Location>> getNamedLocationSets();
    }
}
