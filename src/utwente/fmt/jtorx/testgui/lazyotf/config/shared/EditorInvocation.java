package utwente.fmt.jtorx.testgui.lazyotf.config.shared;

import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;

public interface EditorInvocation {
	public void displayDecisionStrategyEditor();
	public void displayWeightsEditor();
	public void displayTestObjectiveEditor();

	public LocationSetEditingResults displayLocationSetEditor();
	public LocationSetEditingResults displayLocationSetEditor(NamedSet<Location> initialSelection);

	public interface LocationSetEditingResults {
		Set<NamedSet<Location>> newLocationSets();
		Set<NamedSet<Location>> deletedLocationSets();
	}
}
