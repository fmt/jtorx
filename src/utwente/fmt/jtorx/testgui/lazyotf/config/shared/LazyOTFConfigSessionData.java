package utwente.fmt.jtorx.testgui.lazyotf.config.shared;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.LazyOTFConfigurationOptions;
import utwente.fmt.jtorx.lazyotf.TestTree2Weight;
import utwente.fmt.jtorx.lazyotf.TraversalMethod;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.policy.Defaults;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.Condition;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.CollectionUtilities;

/**
 * A representation of the current LazyOTF configuration geared towards the GUI.
 * Most importantly, this configuration representation deals with location sets,
 * which are only implemented in the GUI layer of LazyOTF.
 */
public class LazyOTFConfigSessionData {
	/** The set of custom {@link Path2Weight}s. */
	private Set<Path2Weight> path2Weights;

	/** {@link Path2Weight} enabledness map */
	private Set<Path2Weight> enabledPath2Weights;

	/** The set of custom node value weighers. */
	private Set<NodeValue2Weight> customNodeValue2Weights;

	/** LazyOTF's configuration options. */
	private LazyOTFConfigurationOptions configOptions;

	/** The current model's test type groups. */
	private Set<UITestTypeGroup> testTypeGroups;

	/** The current model's test types. */
	private Set<TestType> testTypes;

	/** The current model's locations. */
	private Set<NamedSet<Location>> locations;

	/** The current model's location sets. */
	private Set<NamedSet<Location>> locationSets;

	/** The current model's file name. */
	private String currentFileName = null;

	/** The list of current decision strategies. */
	private List<UIDecisionStrategy> strategies = 
			new ArrayList<UIDecisionStrategy>();

	/** The automatic test tree recomputation flag. */
	private boolean isAutomaticTestTreeRecomputationEnabled;

	/** The edge weight aggregator index within the array of edge weight aggr.
	 * names in the configuration options. */
	private int treeWeigherIndex = -1;

	/** The maximum recursion depth. */
	private int maxRecursionRepth;

	/** The initial recursion depth. */
	private int initialRecursionDepth;

	/** The recursion depth modification threshold. */
	private int recursionDepthModThreshold;

	/** The CheckedPREDfsVisitor visitor wrapper enable flag. */
	private boolean isCheckedPREDfsVisitorEnabled;

	/** The index of the visitor name within the respective array contained
	 * in the configuration options. */
	private int traversalMethodIndex = -1;

	/** A mapping from model locations to their rsp. location sets */
	private final Map<Location, NamedSet<Location>> locations2LocationSets = new HashMap<>();

	/** The set of SatStrategy conditions visible to all SatStrategy 
	 * instances. */
	private Set<SatStrategy.Condition> globalConditions = 
			new HashSet<SatStrategy.Condition>();

	/** Filename of the LazyOTF logfile. */
	private String logFilename = null;

	/** true iff the autostep mode should stop when all test objectives are met. */
	private boolean autoStepStopMode = false;

	/** true iff LazyOTF debug message logging is enabled */
	private boolean isCommunicationLoggingEnabled = true;

	public LazyOTFConfigSessionData() {
		// This is only included temporarily.
		String logProperty = System.getProperty("lazyotf.debug.logCommByDefault");
		if (logProperty != null) {
			isCommunicationLoggingEnabled = Boolean.valueOf(logProperty);
		}
	}

	/**
	 * Sets the LazyOTF logging filename.
	 * @param filename The LazyOTF logging filename.
	 */
	public void setLogFilename(String filename) {
		this.logFilename = filename;
	}

	/**
	 * Gets the LazyOTF logging filename.
	 * @return The LazyOTF logging filename or <tt>null</tt> if none was
	 *   specified.
	 */
	public String getLogFilename() {
		return this.logFilename;
	}

	/**
	 * Sets the autostep autostop mode. If true, the autostep mode gets halted
	 * when the last test objective is met.
	 * @param autoStopEnabled The new autostep autostop mode.
	 */
	public void setAutoSteStopMode(boolean autoStopEnabled) {
		this.autoStepStopMode = autoStopEnabled;
	}

	/**
	 * Gets the autostep autostop mode.
	 * @return <tt>true</tt> iff the autostep autostop mode is enabled.
	 */
	public boolean isAutoStepStopModeEnabled() {
		return this.autoStepStopMode;
	}

	/** 
	 * Get the location set associated with modelLocation. Between calls to
	 * clearLocationSetAssociations(), this mapping is well-defined and
	 * injective (both in an object-identity sense).
	 * @param modelLocation The {@link Location} instance.
	 * @return The corresponding location set.
	 */
	public NamedSet<Location> getLocationSet(Location modelLocation) {
		if(!this.locations2LocationSets.containsKey(modelLocation)) {
			this.locations2LocationSets.put(modelLocation,
					NamedSet.makeNamedSet(null, modelLocation));
		}
		return this.locations2LocationSets.get(modelLocation);
	}

	/**
	 * Clears the session's location-to-location-set associations. 
	 */
	public void clearLocationSetAssociations() {
		this.locations2LocationSets.clear();
	}

	/**
	 * Sets the enabled {@link Path2Weight}s.
	 * @param enabledPath2Weights The set of the new enabled guidance 
	 *   methods. In order to pass <i>validate()</i>, this must be a subset of
	 *   the set of {@link Path2Weight}s.
	 */
	public void setEnabledPath2Weights(
			Set<Path2Weight> enabledPath2Weights) {

		this.enabledPath2Weights = enabledPath2Weights;
	}

	/**
	 * Gets the set of enabled {@link Path2Weight}s.
	 * @return The set of enabled {@link Path2Weight}s.
	 */
	public Set<Path2Weight> getEnabledPath2Weights() {
		return this.enabledPath2Weights;
	}

	/**
	 * Sets the automatic test tree recomputation flag.
	 * @param enabledness The flag's new value.
	 */
	public void setAutomaticTestTreeComputationEnabled(
			boolean enabledness) {
		this.isAutomaticTestTreeRecomputationEnabled = enabledness;
	}

	/**
	 * @return <tt>true</tt> iff the automatic test tree recomputation flag is
	 *   set.
	 */
	public boolean isAutomaticTestTreeRecomputationEnabled() {
		return this.isAutomaticTestTreeRecomputationEnabled;
	}

	/**
	 * Sets the edge weight aggregator by name.
	 * @param name The aggregator's name.
	 * @throws {@link IllegalArgumentException} when the supplied name is not
	 *   among the edge weight aggregators offered by the LazyOTF
	 *   implementation.
	 */
	public void setEdgeWeightAggregatorName(TestTree2Weight it) {
	    if (!configOptions.testTreeWeighers.contains(it)) {
	        throw new IllegalArgumentException("Unknown TestTree2Weight " + it.getName());
	    }
		int idx = this.configOptions.testTreeWeighers.indexOf(it);
		if(idx >= 0) {
			this.treeWeigherIndex = idx;
		}
		else {
			throw new IllegalArgumentException(it.getName() + 
				": Illegal edge weight aggregator");
		}
	}

	/**
	 * Gets the edge weight aggregator's name.
	 * @return The edge weight aggregator's name or <tt>null</tt> if none is 
	 *   set.
	 */
	public TestTree2Weight getEdgeWeightAggregatorName() {
		if(this.treeWeigherIndex < 0) {
			return null;
		}
		else {
			return this.configOptions.testTreeWeighers.get(
					this.treeWeigherIndex);
		}
	}

	/**
	 * Sets the UseCheckedPREDfsVisitorEnabled flag.
	 * @param enabledness The flag's new value.
	 */
	public void setCheckedPREDfsVisitorEnabled(boolean enabledness) {
		this.isCheckedPREDfsVisitorEnabled = enabledness;
	}

	/**
	 * @return <tt>true</tt> iff the UseCheckedPREDfsVisitor flag is set.
	 */
	public boolean isCheckedPREDfsVisitorEnabled() {
		return this.isCheckedPREDfsVisitorEnabled;
	}

	/**
	 * Sets the maximum traversal depth parameter for the LazyOTF algorithm.
	 * This value must be >= 0 in order to pass <i>validate()</i>.
	 * @param depth
	 */
	public void setMaxRecursionDepth(int depth) {
		this.maxRecursionRepth = depth;
	}

	/**
	 * @return The currently set maximum traversal depth parameter.
	 */
	public int getMaxRecursionDepth() {
		return this.maxRecursionRepth;
	}

	/**
	 * Sets the graph traversal method name.
	 * @param name The new graph traversal method name. In order to pass
	 *   <i>validate()</i>, this value must be contained in the list of
	 *   graph traversal methods supplied by the LazyOTF implementation.
	 */
	public void setVisitorName(TraversalMethod it) {
	    if (!configOptions.traversalMethods.contains(it)) {
	        throw new IllegalArgumentException("Unknown TraversalMethod " + it.getName());
	    }
		int idx = this.configOptions.traversalMethods.indexOf(it);
		if(idx >= 0) {
			this.traversalMethodIndex = idx;
		}
		else {
			throw new IllegalArgumentException(it.getName() + ": Illegal visitor name");
		}
	}

	/**
	 * @return The current graph traversal method name or <tt>null</tt> if none
	 *   is set.
	 */
	public TraversalMethod getVisitorName() {
		if(this.traversalMethodIndex < 0) {
			return null;
		}
		else {
			return this.configOptions.traversalMethods.get(this.traversalMethodIndex);
		}
	}

	/**
	 * Sets the LazyOTF configuration options.
	 * @param configOptions The new LazyOTF configuration options object.
	 */
	public void setConfigOptions(LazyOTFConfigurationOptions configOptions) {
		this.configOptions = configOptions;
	}

	/**
	 * @return The current LazyOTF configuration options object.
	 */
	public LazyOTFConfigurationOptions getConfigOptions() {
		return this.configOptions;
	}

	/**
	 * Sets the current model file name.
	 * @param fileName The new model file name.
	 */
	public void setCurrentFileName(String fileName) {
		this.currentFileName = fileName;
	}

	/**
	 * @return The current model file name.
	 */
	public String getCurrentFileName() {
		return this.currentFileName;
	}

	/**
	 * Sets the current custom NodeValue2Weight set.
	 * @param customNodeValue2Weights The new {@link NodeValue2Weight} set. In
	 *   order to pass <i>validate()</i>, this set must contain the default node
	 *   value weigher. This set must not contain <tt>null</tt>.
	 */
	public void setCustomNodeValue2Weights(
			Set<NodeValue2Weight> customNodeValue2Weights) {
		this.customNodeValue2Weights = customNodeValue2Weights;
	}

	/**
	 * @return The current custom NodeValue2Weight set.
	 */
	public Set<NodeValue2Weight> getCustomNodeValue2Weights() {
		return this.customNodeValue2Weights;
	}

	/**
	 * Sets the global {@link SatStrategy} condition set.
	 * @param globalConditions The new set of globally available {@link
	 *   SatStrategy} condition objects. This set may not contain <tt>null</tt>.
	 */
	public void setGlobalConditions(Set<Condition> globalConditions) {
		this.globalConditions = globalConditions;
	}

	/**
	 * @return The current set of global {@link SatStrategy} conditions.
	 */
	public Set<Condition> getGlobalConditions() {
		return this.globalConditions;
	}

	/**
	 * Sets the current set of {@link Path2Weight}s.
	 * @param path2Weights The new set of current {@link Path2Weight}s.
	 *   This set must not contain <tt>null</tt>.
	 */
	public void setPath2Weights(Set<Path2Weight> path2Weights) {
		this.path2Weights = path2Weights;
	}

	/**
	 * @return The current set of {@link Path2Weight}s.
	 */
	public Set<Path2Weight> getPath2Weights() {
		return this.path2Weights;
	}

	/**
	 * Sets the current set of locations.
	 * @param locations The new current set of locations, each contained in a
	 *   {@link NamedSet} instance. In order to pass <i>validate()</i>, this
	 *   set must not contain <tt>null</tt> and each location set within it
	 *   must contain exactly one {@link Location} instance. Also, each
	 *   {@link Location} instance may only be contained in one such sets
	 *   and its name must be set to <tt>null</tt>.
	 */
	public void setLocations(Set<NamedSet<Location>> locations) {
		this.locations = locations;
	}

	/**
	 * @return The current set of locations. Each of these sets contains
	 *   exactly one location, does not intersect with any other location
	 *   set within the set and for each location in the model, one
	 *   corresponding location set is contained within the returned set.
	 *   Each NamedSet instance's name is <tt>null</tt>.
	 */
	public Set<NamedSet<Location>> getBasisLocationSets() {
		return this.locations;
	}

	/**
	 * Sets the current set of named location sets.
	 * @param locationSets The new current set of named location sets. In order
	 *   to pass <i>validate()</i>, this set must not contain <tt>null</tt> and
	 *   each set within it must contain at least one {@link Location} instance
	 *   which must also be included in the set provided to <i>setLocations</i>. 
	 *   Also, no named location set may have a <tt>null</tt> name.
	 */
	public void setLocationSets(Set<NamedSet<Location>> locationSets) {
		this.locationSets = locationSets;
	}

	/**
	 * @return The current set of named location sets.
	 */
	public Set<NamedSet<Location>> getLocationSets() {
		return this.locationSets;
	}

	/**
	 * Sets the current set of {@link UIDecisionStrategy} objects available for
	 * use with the {@link UITestTypeGroup} objects.
	 * @param strategies The new current set of {@link UIDecisionStrategy}
	 *   objects. In order to pass <i>validate()</i>, this set must not contain
	 *   <tt>null</tt>, each strategy must have a name and each used location
	 *   set must either be a named location set or a location stored in the
	 *   session object. Also, all used {@link TestType} objects must be present
	 *   in the session object's set of test types.
	 */
	public void setStrategies(List<UIDecisionStrategy> strategies) {
		this.strategies = strategies;
	}

	/**
	 * @return The current set of {@link UIDecisionStrategy} objects available
	 * for use with the {@link UITestTypeGroup} objects.
	 */
	public List<UIDecisionStrategy> getStrategies() {
		return this.strategies;
	}

	/**
	 * Sets the current set of {@link UITestTypeGroup} objects.
	 * @param testTypeGroups The new current set of {@link UITestTypeGroup}
	 *   objects. In order to pass <i>validate()</i>, this set must not contain
	 *   <tt>null</tt>, each test type group must have a name  and each used 
	 *   location set must either be a named location set or a location stored
	 *   in the session object. Also, all used {@link TestType} objects must be
	 *   present in the session object's set of test types.
	 */
	public void setTestTypeGroups(Set<UITestTypeGroup> testTypeGroups) {
		this.testTypeGroups = testTypeGroups;
	}

	/**
	 * @return The current set of {@link UITestTypeGroup} objects.
	 */
	public Set<UITestTypeGroup> getTestTypeGroups() {
		return this.testTypeGroups;
	}

	/**
	 * Sets the current set of {@link TestType} objects used by the 
	 * configuration.
	 * @param testTypes The new current set of {@link TestType} objects used
	 *   by the configuration. In order to pass <i>validate()</i>, this set
	 *   must not contain <tt>null</tt>.
	 */
	public void setTestTypes(Set<TestType> testTypes) {
		this.testTypes = testTypes;
	}

	/**
	 * @return The current set of {@link TestType} objects.
	 */
	public Set<TestType> getTestTypes() {
		return this.testTypes;
	}

	/**
	 * Sets the graph traversal method by index within the LazyOTF configuration
	 * object's list of graph traversal methods.
	 * @param i The new graph traversal method's index. This value must be a
	 *   valid index to the list of graph traversal methods within the 
	 *   configuration options object.
	 */
	public void setVisitor(int i) {
		if(i < 0 || i >= this.configOptions.traversalMethods.size()) {
			throw new IllegalArgumentException("Illegal visitor index " + i);
		}
		else {
			this.traversalMethodIndex = i;
		}
	}

	/**
	 * @return The index of the currently selected graph traversal method within
	 *   the list of graph traversal methods provided by the LazyOTF
	 *   configuration options object.
	 */
	public int getTraversalMethodIndex() {
		return this.traversalMethodIndex;
	}

	/**
	 * Sets the edge weight aggregator by index within the LazyOTF configuration
	 * object's list of edge weight aggregators.
	 * @param i The new edge weight aggregator's index. This value must be a
	 *   valid index to the list of edge weight aggregators within the 
	 *   configuration options object.
	 */
	public void setTreeWeigherIndex(int i) {
		if(i < 0 || i >= this.configOptions.testTreeWeighers.size()) {
			throw new IllegalArgumentException("Illegal edge weight " + "" +
					"aggregator " + i);
		}
		else {
			this.treeWeigherIndex = i;
		}
	}

	/**
	 * @return The index of the currently selected edge weight aggregator within
	 *   the list of edge weight aggregators provided by the LazyOTF
	 *   configuration options object.
	 */
	public int getTreeWeigherIndex() {
		return this.treeWeigherIndex;
	}

	/**
	 * Validates the configuration session data object. See the setter javadocs
	 * for more details.
	 * @throws ValidationException when the session data failed validation. The
	 *   exception's message contains details about why it did not pass
	 *   validation.
	 */
	public void validate() throws ValidationException {
		if(configOptions == null) {
			throw new IllegalStateException("Configuration options must be " +
					"attached to the session data before validation.");
		}

		if(testTypes == null) {
			throw new ValidationException("testTypes must not be null.");
		}

		/*if(this.defaultTestGoalGroup == null) {
			throw new ValidationException("defaultTestGoalGroup must not be" +
					" null.");
		}*/

		if(customNodeValue2Weights == null) {
			throw new ValidationException("customNodeValue2Weights must not be"+
					" null.");
		}


		if(!customNodeValue2Weights.contains(Defaults.defaultNodeValue2Weight())) {
			throw new ValidationException("customNodeValue2Weights must " +
					"contain defaultNodeValue2Weight.");
		}

		int edgeWeightAggregatorsSize = 
				configOptions.testTreeWeighers.size();
		if(treeWeigherIndex < 0 ||
				treeWeigherIndex >= edgeWeightAggregatorsSize) {
			throw new ValidationException("Illegal test tree weigher " +
					"index " + treeWeigherIndex);
		}

		if(enabledPath2Weights == null) {
			throw new ValidationException("enabledPath2Weights must not be"+
					" null.");
		}

		if(path2Weights == null) {
			throw new ValidationException("path2Weights must not be null.");
		}

		if(path2Weights.contains(null)) {
			throw new ValidationException("path2Weights must not contain"+
					" null.");
		}

		for(Path2Weight gm : enabledPath2Weights) {
			if(!path2Weights.contains(gm)) {
				throw new ValidationException("enabledPath2Weights must be"+
						"a subset of path2Weights.");
			}
		}

		if(globalConditions == null) {
			throw new ValidationException("globalConditions must not be null.");
		}

		if(locations == null) {
			throw new ValidationException("locations must not be null.");
		}


		Set<Location> bareLocations = new HashSet<>();
		for(NamedSet<Location> ls : locations) {
			if(ls == null) {
				throw new ValidationException("locations must not contain" + 
						"null.");
			}
			else {
				if(ls.getName() != null) {
					throw new ValidationException("locations contains " + 
							ls.getDisplayName() + ", which illegally has a set"+
							"name.");
				}
				if(ls.size() != 1) {
					throw new ValidationException("locations contains " +
							ls.getDisplayName() + ", which illegally does not "+
							"contain exactly one location.");
				}
				bareLocations.add(ls.iterator().next());
			}
		}

		if(locationSets == null) {
			throw new ValidationException("locationSets must not be null.");
		}

		for(NamedSet<Location> nls : locationSets) {
			if(nls.getName() == null) {
				throw new ValidationException("locationSets illegally contains"+
						"a location set without a name.");
			}
			for(Location l : nls) {
				if(!bareLocations.contains(l)) {
					throw new ValidationException("locationSets contains " 
							+ nls.getDisplayName() + ", which illegally"+
							" contains an unknown location " + l.getName());
				}
			}
		}

		if(maxRecursionRepth < 0) {
			throw new ValidationException("maxRecursionDepth must be >= 0.");
		}

		if(logFilename == null) {
			throw new ValidationException("logFilename must not be null.");
		}

		for(UIDecisionStrategy ds : strategies) {
			if(ds.getDisplayName() == null) {
				throw new ValidationException("strategies illegally contains"+
						" a nameless strategy.");
			}
			/*if(ds.getInternalName() == null) {
				throw new ValidationException("strategies illegally contains a"+
						" strategy without an implementation name.");
			}
			if(!configOptions.decisionStrategies.contains(
					ds.getInternalName())) {
				throw new ValidationException(ds.getDisplayName() + 
						": Unknown strategy implementation " + 
						ds.getInternalName());
			}*/

			ds.accept(new UIDecisionStrategyVisitorAdapter() {
				@Override
				public void visit(UIMappingStrategy strategy) {
					for(NamedSet<Location> nls : strategy.getLocationSets()) {
						if(!locations.contains(nls) && 
								!locationSets.contains(nls)) {
							throw new ValidationException(
									strategy.getDisplayName() + ": Unknown " +
											"location set " + 
											nls.getDisplayName());
						}
						if(!testTypes.contains(strategy.getTestType(nls))) {
							throw new ValidationException(
									strategy.getDisplayName() + 
									": Unknown test type " + 
									strategy.getTestType(nls));
						}
					}
				}

				@Override
				public void visit(UISatStrategy strategy) {
					for(NamedSet<Location> nls : strategy.getLocationSets()) {
						if(!locations.contains(nls) && 
								!locationSets.contains(nls)) {
							throw new ValidationException(
									strategy.getDisplayName() + ": Unknown " +
											"location set " + 
											nls.getDisplayName());
						}
						for(TestType tt : strategy.getAssociatedTestTypes(nls)){
							if(!testTypes.contains(tt)) {
								throw new ValidationException(
										strategy.getDisplayName() +
										": Unknown test type " + tt);
							}
							if(strategy.getCondition(nls, tt) == null) {
								throw new ValidationException(
										strategy.getDisplayName() + 
										": null Condition");
							}
						}
					}
				}
			});
		}

		for(UITestTypeGroup uiTTG : testTypeGroups) {
			if(uiTTG.getDisplayName() == null) {
				throw new ValidationException("testTypeGroups contains an " +
						"unnamed test type group.");
			}
			String name = "Test type group " + uiTTG.getDisplayName();
			if(uiTTG.getStrategy() != null && 
					!strategies.contains(uiTTG.getStrategy())) {
				throw new ValidationException(name + ": Unknown strategy");
			}
			for(TestType tt : uiTTG.getTestTypes()) {
				if(!testTypes.contains(tt)) {
					throw new ValidationException(name + "contains an unknown" +
							" test type");
				}
				for(NamedSet<Location> ls : uiTTG.getLocations(tt)) {
					if(!locations.contains(ls) && !locationSets.contains(ls)) {
						throw new ValidationException(name + " contains an " + 
								"unknown location/location set");
					}
				}
			}
		}
	}

	/**
	 * @return The unmodifiable unification of getBasisLocationSets() and
	 *   getLocationSets().
	 */
	public Collection<NamedSet<Location>> getAllLocations()  {
		return CollectionUtilities.unmodifiableUnionView(getLocationSets(), getBasisLocationSets());
	}

	/**
	 * @return The initial model traversal depth.
	 */
	public int getInitialRecursionDepth() {
		return initialRecursionDepth;
	}

	/**
	 * Sets the initial model traversal depth.
	 * @param initialRecursionDepth The new initial model traversal depth.
	 */
	public void setInitialRecursionDepth(int initialRecursionDepth) {
		this.initialRecursionDepth = initialRecursionDepth;
	}

	/**
	 * @return The model traversal depth modification threshold.
	 */
	public int getRecursionDepthModThreshold() {
		return recursionDepthModThreshold;
	}

	/**
	 * Sets the model traversal depth modification threshold.
	 * @param recursionDepthModThreshold The new model traversal depth
	 *   modification threshold.
	 */
	public void setRecursionDepthModThreshold(int recursionDepthModThreshold) {
		this.recursionDepthModThreshold = recursionDepthModThreshold;
	}

	@SuppressWarnings("serial")
	public class ValidationException extends RuntimeException {
		public ValidationException(String msg) {
			super(msg);
		}
	}

	public boolean isCommunicationLoggingEnabled() {
		return isCommunicationLoggingEnabled;
	}

	public void setCommunicationLoggingEnabled(boolean isDebugLoggingEnabled) {
		this.isCommunicationLoggingEnabled = isDebugLoggingEnabled;
	}
}
