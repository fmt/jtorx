package utwente.fmt.jtorx.testgui.lazyotf.config.shared;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.NamedEntity;

/**
 * A decorator for {@link Set} adding a name. Its <i>display name</i> is
 * computed from the set name if it is not <tt>null</tt> and from a (random)
 * set element otherwise, if existent.
 * @param <T> The type of the objects within the set.
 */
public class NamedSet<T extends NamedEntity> implements Set<T> {
	/** The set's name. */
	private String name;

	/** The decorated set. */
	private final Set<T> backendSet;

	private final static String ANONYMOUS_NAME = "Anonymous Location Set";

	/**
	 * Construct a new {@link NamedSet} instance.
	 * @param name The set's name (may be null).
	 * @param decoratedSet The decorated set.
	 */
	public NamedSet(String name, Set<T> decoratedSet) {
		assert decoratedSet != null;

		this.name = name;
		this.backendSet = new HashSet<>(decoratedSet);
	}

	/**
	 * Construct a new {@link NamedSet} instance, copying the contents of
	 * the <i>initialContents</i> {@link Collection} into a new {@link HashSet}. 
	 * @param displayName The set's name (may be null).
	 * @param initialContents The decorated set.
	 */
	public NamedSet(String name, Collection<T> initialContents) {
		this(name, new HashSet<T>(initialContents));
	}

	/**
	 * Construct a new {@link NamedSet} instance using {@link HashSet} as
	 * the underlying set implementation.
	 * @param displayName The set's name (may be null).
	 */
	public NamedSet(String displayName) {
		this(displayName, new HashSet<T>());
	}

	/**
	 * Get the set's <i>display name</i>, which is a name intended for the
	 * GUI display. If the set has a name <tt>foo</tt>, its display name is
	 * <tt>{@literal <foo>}</tt>. Otherwise, if it is not empty, its display
	 * name is the name of one of its elements. If neither case applies, its
	 * display name is <tt>NamedSet.ANONYMOUS_NAME</tt>. 
	 * @return
	 */
	public String getDisplayName() {
		if(this.name == null) {
			if(isEmpty()) {
				return ANONYMOUS_NAME;
			}
			else {
				return iterator().next().getName();
			}
		}
		else {
			return "<" + this.name + ">";
		}
	}

	/**
	 * Determines whether the set has a name.
	 * @return <tt>true</tt> iff it has a name.
	 */
	public boolean hasName() {
		return this.name != null;
	}

	/**
	 * Getter for the set name.
	 * @return The set's name (<tt>null</tt> if it has none).
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Set the set's name to a new value. This may change its display name.
	 * @param displayName The set's new name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean add(T arg0) {
		return this.backendSet.add(arg0);
	}

	@Override
	public boolean addAll(Collection<? extends T> arg0) {
		return this.backendSet.addAll(arg0);
	}

	@Override
	public void clear() {
		this.backendSet.clear();
	}

	@Override
	public boolean contains(Object arg0) {
		return this.backendSet.contains(arg0);
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		return this.backendSet.containsAll(arg0);
	}

	@Override
	public boolean isEmpty() {
		return this.backendSet.isEmpty();
	}

	@Override
	public Iterator<T> iterator() {
		return this.backendSet.iterator();
	}

	@Override
	public boolean remove(Object arg0) {
		return this.backendSet.remove(arg0);
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		return this.backendSet.removeAll(arg0);
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		return this.backendSet.retainAll(arg0);
	}

	@Override
	public int size() {
		return this.backendSet.size();
	}

	@Override
	public Object[] toArray() {
		return this.backendSet.toArray();
	}

	@Override
	public <N> N[] toArray(N[] arg0) {
		return this.backendSet.toArray(arg0);
	}

	/**
	 * Make a new named set, backed by a {@link HashSet}, containing one initial
	 * item.
	 * @param name The set's name.
	 * @param initialItem The set's initial item.
	 * @return The new set.
	 */
	public static <T extends NamedEntity> NamedSet<T> makeNamedSet(String name, T initialItem) {
		NamedSet<T> newSet = new NamedSet<T>(name);
		newSet.add(initialItem);
		return newSet;
	}
}
