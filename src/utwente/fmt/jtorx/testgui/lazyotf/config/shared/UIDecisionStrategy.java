package utwente.fmt.jtorx.testgui.lazyotf.config.shared;

import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategy;

/**
 * A representation of {@link DecisionStrategy} objects geared towards the GUI.
 */
public class UIDecisionStrategy {
	/** The strategy's display name. */
	private String displayName;

	/** The strategy's internal name. */
	private String internalName;

	/** Makes a {@link DecisionStrategy} object reflecting the {@link
	 * UIDecisionStrategy} instance. 
	 * @return A new {@link DecisionStrategy} object.
	 */
	public DecisionStrategy makeDecisionStrategy() {
		return new DecisionStrategy();
	}

	/**
	 * Sets the strategy's internal name.
	 * @param internalName The new internal name.
	 */
	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}

	/**
	 * Gets the strategy's internal name.
	 * @return The strategy's internal name.
	 */
	public String getInternalName() {
		return this.internalName;
	}

	/**
	 * Sets the strategy's display name.
	 * @param displayName The new display name.
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Gets the strategy's display name.
	 * @return The display name.
	 */
	public String getDisplayName() {
		return this.displayName;
	}

	/**
	 * Accept a {@link UIDecisionStrategyVisitor} by calling its appropriate
	 * visit method.
	 * @param visitor The visitor object.
	 */
	public void accept(UIDecisionStrategyVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * Determine whether the strategy is usable within the given context of
	 * location sets and test types. 
	 * @param locationSets The set of location sets on which the strategy
	 *   must operate.
	 * @param testTypes The set of test types on which the strategy must
	 *   operate.
	 * @return <tt>true</tt> iff the strategy is usable within the given
	 *   context.
	 */
	public boolean isUsableInContext(Set<NamedSet<Location>> locationSets, Set<TestType> testTypes) {
		return true;
	}
}
