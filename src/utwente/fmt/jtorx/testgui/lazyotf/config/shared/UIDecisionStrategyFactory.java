package utwente.fmt.jtorx.testgui.lazyotf.config.shared;

import java.util.ArrayList;
import java.util.List;

/**
 * A factory class for UIDecisionStrategy objects.
 */
public class UIDecisionStrategyFactory {
	/**
	 * @return A list of available DecisionStrategy classes.
	 */
	public static List<Class<? extends UIDecisionStrategy>> getStrategyTypes() {
		ArrayList<Class<? extends UIDecisionStrategy>> result = new ArrayList<Class<? extends UIDecisionStrategy>>();

		result.add(UIMappingStrategy.class);
		result.add(UISatStrategy.class);
		result.add(UIDecisionStrategy.class);

		return result;
	}
}
