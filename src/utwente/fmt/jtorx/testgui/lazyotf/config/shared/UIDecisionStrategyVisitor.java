package utwente.fmt.jtorx.testgui.lazyotf.config.shared;

/**
 * A visitor interface for {@link UIDecisionStrategy} objects.
 */
public interface UIDecisionStrategyVisitor {
	/**
	 * Visit the given {@link UIDecisionStrategy} instance.
	 * @param strategy The instance to be visited.
	 */
	void visit(UIDecisionStrategy strategy);

	/**
	 * Visit the given {@link UISatStrategy} instance.
	 * @param strategy The instance to be visited.
	 */
	void visit(UISatStrategy strategy);

	/**
	 * Visit the given {@link UIMappingStrategy} instance.
	 * @param strategy The instance to be visited.
	 */
	void visit(UIMappingStrategy strategy);
}
