package utwente.fmt.jtorx.testgui.lazyotf.config.shared;

/**
 * An adapter class for the {@link UIDecisionStrategyVisitor} interface.
 */
public class UIDecisionStrategyVisitorAdapter implements UIDecisionStrategyVisitor {

	@Override
	public void visit(UIDecisionStrategy strategy) {
		otherMethodCalled(strategy);
	}

	@Override
	public void visit(UISatStrategy strategy) {
		otherMethodCalled(strategy);	
	}

	@Override
	public void visit(UIMappingStrategy strategy) {
		otherMethodCalled(strategy);
	}

	/**
	 * The default method. Gets called when a non-overridden visit method
	 * visits an object.
	 * @param strategy The visited object.
	 */
	public void otherMethodCalled(UIDecisionStrategy strategy) {

	}
}
