package utwente.fmt.jtorx.testgui.lazyotf.config.shared;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.MappingStrategy;

/**
 * A representation of {@link MappingStrategy} objects geared towards the GUI.
 */
public class UIMappingStrategy extends UIDecisionStrategy {
	/** The association map linking location sets with their corresponding
	 * test types.
	 */
	private final Map<NamedSet<Location>, TestType> associations;

	/**
	 * Constructs a new {@link UIMappingStrategy} instance initially having
	 * no associations.
	 */
	public UIMappingStrategy() {
		this.associations = new HashMap<>();
	}

	/**
	 * Associates a location set with a test type.
	 * @param locSet The location set.
	 * @param testType The test type.
	 */
	public void setTestType(NamedSet<Location> locSet, TestType testType) {
		if(testType != null) {
			this.associations.put(locSet, testType);
		}
		else {
			this.associations.remove(locSet);
		}
	}

	/**
	 * Gets all location sets associated with a test type by this instance.
	 * @return A set of location sets as specified above.
	 */
	public Set<NamedSet<Location>> getLocationSets() {
		return associations.keySet();
	}

	/**
	 * Get the test type of a location set according to this mapping strategy
	 * instance.
	 * @param locSet A location set.
	 * @return The location set's test type as specified by this mapping
	 *   strategy instance or <tt>null</tt> if it is not associated with
	 *   any test type by this instance.
	 */
	public TestType getTestType(NamedSet<Location> locSet) {
		return associations.get(locSet);
	}

	/**
	 * Convert the {@link UIMappingStrategy} instance to a new {@link
	 * MappingStrategy} instance. Conflicts between location sets are
	 * resolved by assigning the highest-possible test type associated
	 * with the respective {@link Location}.
	 * @return a new {@link MappingStrategy} instance reflecting the
	 *   associations defined by {@link UIMappingStrategy} instance.
	 */
	@Override
	public MappingStrategy makeDecisionStrategy() {
		MappingStrategy result = new MappingStrategy();

		for(NamedSet<Location> locSet : associations.keySet()) {
			TestType currentType = associations.get(locSet);
			for(Location loc : locSet) {
				if(!result.isAssigningTestType(loc)
						|| result.getType(loc).compareTo(currentType) < 0) {
					result.associate(loc, currentType);
				}
			}
		}

		result.setDisplayName(getDisplayName());
		return result;
	}

	@Override
	public void accept(UIDecisionStrategyVisitor visitor) {
		visitor.visit(this);
	}
}
