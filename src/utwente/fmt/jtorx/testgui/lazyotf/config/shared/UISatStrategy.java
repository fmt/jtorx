package utwente.fmt.jtorx.testgui.lazyotf.config.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.Condition;

/**
 * A representation of {@link SatStrategy} objects geared towards the GUI.
 * Currently, this is an adaption of SatStrategy for named location sets,
 * so the documentation of SatStrategy applies, too. 
 */
public class UISatStrategy extends UIDecisionStrategy {
	/** An association between location sets and mappings from test types to
	 * conditions representing the sat strategy data.
	 */
	private final Map<NamedSet<Location>, Map<TestType, Condition>> associationMap;

	/** The set of test types known to the strategy instance. */
	private final Set<TestType> testTypes;

	/**
	 * Constructs a new {@link UISatStrategy} instance, initially having
	 * no associations.
	 * @param testTypes The set of test types known to the strategy instance.
	 */
	public UISatStrategy(Set<TestType> testTypes) {
		this.associationMap = new HashMap<>();
		this.testTypes = testTypes;
	}

	/**
	 * Constructs a new {@link UISatStrategy} instance, initially having
	 * neither knowledge about available test types nor any associations.
	 */
	public UISatStrategy() {
		this(new HashSet<TestType>());
	}

	/**
	 * Associates a location set with a test type and a {@link SatStrategy}
	 * condition. The association is functional in the sense that locationSets
	 * are mapped to <i>functions</i> associating test types with conditions.
	 * (Thus, it is impossible to have two different conditions for the same
	 * location set and test type combination.) 
	 * @param locationSet The location set to be associated with the 
	 *   <tt>type</tt>-<tt>cond</tt> pair.
	 * @param type The test type of the association.
	 * @param cond The condition of the association.
	 */
	public void associate(NamedSet<Location> locationSet, TestType type, Condition cond) {
		this.testTypes.add(type);
		if(!associationMap.containsKey(locationSet)) {
			associationMap.put(locationSet, new HashMap<TestType, Condition>());
		}
		Map<TestType, Condition> conditionMap = associationMap.get(locationSet);
		conditionMap.put(type, cond);		
	}

	/**
	 * Removes the given <tt>locationset</tt>-<tt>testType</tt> association from 
	 * the strategy. If the association does not exist, nothing happens.
	 * @param locationSet The involved location set.
	 * @param type The involved test type.
	 */
	public void removeAssociation(NamedSet<Location> locationSet, TestType type) {
		if(associationMap.containsKey(locationSet)) {
			Map<TestType, Condition> conditionMap = associationMap.get(locationSet);
			conditionMap.remove(type);
			if(conditionMap.isEmpty()) {
				associationMap.remove(locationSet);
			}
		}
	}

	/**
	 * Gets the condition for the association of a given location set with a
	 * given test type.
	 * @param locationSet The involved location set.
	 * @param type The involved test type.
	 * @return The associated condition or <tt>null</tt> if no such condition
	 *   exists.
	 */
	public Condition getCondition(NamedSet<Location> locationSet, TestType type) {
		if(associationMap.containsKey(locationSet)) {
			Map<TestType, Condition> conditionMap = associationMap.get(locationSet);
			return conditionMap.get(type);
		}
		else {
			return null;
		}
	}

	/**
	 * Gets the set of locations for which the strategy object contains any
	 * associations.
	 * @return A set of locations as specified above.
	 */
	public Set<NamedSet<Location>> getLocationSets() {
		return associationMap.keySet();
	}

	/**
	 * Makes a new {@link SatStrategy} from the {@link UISatStrategy} instance.
	 * Conflicting condition assignments are resolved by OR-type concatenations.  
	 * @return A new {@link SatStrategy} object.
	 */
	@Override
	public SatStrategy makeDecisionStrategy() {
		SatStrategy result = new SatStrategy();

		for(NamedSet<Location> locSet : associationMap.keySet()) {
			for(TestType type : testTypes) {
				Condition cond = getCondition(locSet, type);
				if(cond == null) {
					continue;
				}

				for(Location loc : locSet) {
					Condition existingCondition = null;
					if(result.getLocations().contains(loc)
							&& result.getAssociatedTestTypes(loc).contains(type)) {
						existingCondition = result.getCondition(loc, type);
					}		

					if(existingCondition == null) {
						result.associate(loc, type, cond);
					}
					else {
						ArrayList<Condition> subConds = new ArrayList<>();
						subConds.add(existingCondition);
						subConds.add(cond);
						result.associate(loc, type, new SatStrategy.AnyMetaCondition(subConds));
					}
				}
			}
		}
		result.setDisplayName(getDisplayName());
		return result;
	}

	/**
	 * Gets the test types this strategy associates with the given location set. 
	 * @param locationSet The involved location set.
	 * @return The set of test types as specified above. May be empty.
	 */
	public Set<TestType> getAssociatedTestTypes(NamedSet<Location> locationSet) {
		if(associationMap.containsKey(locationSet)) {
			return associationMap.get(locationSet).keySet();
		}
		else {
			return new HashSet<>();
		}
	}

	@Override
	public void accept(UIDecisionStrategyVisitor visitor) {
		visitor.visit(this);
	}
}
