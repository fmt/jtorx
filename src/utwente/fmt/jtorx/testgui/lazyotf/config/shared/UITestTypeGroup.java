package utwente.fmt.jtorx.testgui.lazyotf.config.shared;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.guidance.Path2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategy;

/**
 * A representation of {@link TestTypeGroup} objects geared towards the GUI.
 * See {@link TestTypeGroup}.
 */
public class UITestTypeGroup {
	/** The group's display name. */
	private String displayName;

	/** The group's locationSet-testtype associations. */
	private final Map<NamedSet<Location>, TestType> locations2TestTypes;

	/** The group's testtype-locationSet associations. */
	private final Map<TestType, Set<NamedSet<Location>>> testTypes2Locations;

	/** The {@link Path2Weight}s associated active for this test type group */
	private final Set<Path2Weight> path2Weights;

	/** <tt>true</tt> iff the group is enabled at the beginning of the test 
	 * run. 
	 */
	private boolean isEnabled;

	/** The group's {@link UIDecisionStrategy} for DECIDED_BY_STRATEGY test
	 * type resolution.
	 */ 
	private UIDecisionStrategy strategy;

	/**
	 * Constructs a new {@link UITestTypeGroup} instance using the given set of
	 * location test types.
	 * @param testTypes The set of location test types known to the test type
	 *   group instance.
	 */
	public UITestTypeGroup(Set<TestType> testTypes) {
		this(new TestTypeGroup(testTypes), null);
	}

	/**
	 * Constructs a new {@link UITestTypeGroup} instance reflecting the given
	 * {@link TestTypeGroup} instance. Location sets are taken from the given
	 * {@link LazyOTFConfigSessionData} object.
	 * @param ttg The involved test type group.
	 * @param sessionData The current configuration session data.
	 */
	public UITestTypeGroup(TestTypeGroup ttg, LazyOTFConfigSessionData sessionData) {
		this.displayName = ttg.getName();
		this.locations2TestTypes = new HashMap<>();
		this.testTypes2Locations = new HashMap<>();
		for(TestType t : ttg.getTestTypes()) {
			this.testTypes2Locations.put(t, new HashSet<NamedSet<Location>>());
			for(Location loc : ttg.getStates(t)) {
				if(sessionData != null) {
					associate(sessionData.getLocationSet(loc), t);
				}
			}
		}
		this.isEnabled = ttg.isEnabled();
		this.path2Weights = new HashSet<>(ttg.getPath2Weights());
	}

	/**
	 * Associates the given location set with the given test type.
	 * @param location The involved location set.
	 * @param type The involved test type.
	 */
	public void associate(NamedSet<Location> location, TestType type) {
		if(!testTypes2Locations.containsKey(type)) {
			throw new IllegalArgumentException("Illegal type " + type.getInternalName());	
		}
		if(locations2TestTypes.containsKey(location)) {
			removeAssociation(location);
		}
		this.locations2TestTypes.put(location, type);
		this.testTypes2Locations.get(type).add(location);
	}

	/**
	 * Removes the given location set from the test type group.
	 * @param location The involved location set.
	 */
	public void removeAssociation(NamedSet<Location> location) {
		if(this.locations2TestTypes.containsKey(location)) {
			this.testTypes2Locations.get(this.locations2TestTypes.get(location)).remove(location);
			this.locations2TestTypes.remove(location);
		}
	}

	/**
	 * Gets the set of all locations having the given test type assigned by
	 * this group.
	 * @param type The involved test type.
	 * @return A set of location sets as described above.
	 */
	public Set<NamedSet<Location>> getLocations(TestType type) {
		return this.testTypes2Locations.get(type);
	}

	/**
	 * Gets the test type for the given location set.
	 * @param locSet The involved location set.
	 * @return The location set's test type according to this test type group
	 *   or <tt>null</tt> if it does not associate a test type with the given
	 *   location set.
	 */
	public TestType getTestType(NamedSet<Location> locSet) {
		return this.locations2TestTypes.get(locSet);
	}

	/** 
	 * Gets the set of location test types known to this test type group. 
	 * @return A set of location test types as described above.
	 */
	public Set<TestType> getTestTypes() {
		return this.testTypes2Locations.keySet();
	}

	/**
	 * Gets the set of all location sets having any test type association by
	 * this test type group.
	 * @return A set of location sets as described above.
	 */
	public Set<NamedSet<Location>> getLocations() {
		return this.locations2TestTypes.keySet();
	}

	/**
	 * Makes a new {@link TestTypeGroup} instance reflecting this
	 * {@link UITestTypeGroup} instance. Conflicts are resolved as follows:
	 * If the UITestTypeGroup assigns two test types <i>t1</i> and <i>t2</i>
	 * to a location <i>loc</i>, the result will assign the higher-priority
	 * test type to <i>loc</i> with DECIDED_BY_STRATEGY being treated as
	 * the highest-priority test type.
	 * @return A new {@link TestTypeGroup} instance as specified above.
	 */
	public TestTypeGroup makeTestTypeGroup(Map<UIDecisionStrategy, DecisionStrategy> strategyMap) {
		TestTypeGroup result = new TestTypeGroup(this.testTypes2Locations.keySet());
		for(NamedSet<Location> loc : this.locations2TestTypes.keySet()) {
			for(Location l : loc) {
				if(result.contains(l)) {
					TestType currentTT = result.getTestType(l);
					TestType ourTT = getTestType(loc);

					if(ourTT.compareTo(currentTT) > 0) {
						if(!currentTT.isDecidedByStrategy()) {
							result.dissociate(l);
							result.associate(l, ourTT);
						}
					}
					else if(ourTT.isDecidedByStrategy()) {
						result.dissociate(l);
						result.associate(l, ourTT);
					}
				}
				else {
					result.associate(l, getTestType(loc));
				}
			}
		}
		result.setEnabled(isEnabled());
		if(getStrategy() != null) {
			result.setStrategy(strategyMap.get(getStrategy()));
		}
		result.setName(getDisplayName());
		result.setPath2Weights(path2Weights);
		return result;
	}

	/** 
	 * Sets the group's display name.
	 * @param displayName The group's new display name.
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Gets the group's display name.
	 * @return The group's display name.
	 */
	public String getDisplayName() {
		return this.displayName;
	}

	/**
	 * Determines whether the group is enabled or not.
	 * @return <tt>true</tt> iff the group is enabled explicitly or if it is a
	 *   default test type group. 
	 */
	public boolean isEnabled() {
		return this.isEnabled;
	}

	/**
	 * Sets the group's enabledness.
	 * @param enabledness <tt>true</tt> if the group should be enabled.
	 */
	public void setEnabled(boolean enabledness) {
		this.isEnabled = enabledness;
	}

	/**
	 * Gets the group's {@link UIDecisionStrategy} object.
	 * @return The group's {@link DecisionStrategy} object or <tt>null</tt> if
	 *   it has none.
	 */
	public UIDecisionStrategy getStrategy() {
		return this.strategy;
	}

	/**
	 * Sets the group's {@link UIDecisionStrategy} object.
	 * @param d The group's new {@link UIDecisionStrategy}.
	 */
	public void setStrategy(UIDecisionStrategy d) {
		this.strategy = d;
	}

	/**
	 * Adds a {@link Path2Weight} to the test type group. {@link Path2Weight}s
	 * initially referenced by any test type group are active iff any
	 * active test type group refers to them.
	 * @param it The new {@link Path2Weight}.
	 */
	public void addPath2Weight(Path2Weight it) {
		this.path2Weights.add(it);
	}

	/**
	 * Removes the given {@link Path2Weight} from the test type group.
	 * @param it The {@link Path2Weight} which should be removed.
	 */
	public void removePath2Weight(Path2Weight it) {
		this.path2Weights.remove(it);
	}

	/**
	 * @return the set of {@link Path2Weight}s associated with the test type group. 
	 */
	public Set<Path2Weight> getPath2Weights() {
		return Collections.unmodifiableSet(this.path2Weights);
	}
}
