package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * An "empty" editor pane displaying a message.
 */
class EmptyPaneView extends Composite {
    /**
     * Construct a new EmptyPaneView instance.
     * 
     * @param parent
     *            The pane's parent widget.
     * @param message
     *            The message to be displayed.
     */
    public EmptyPaneView(Composite parent, String message) {
        super(parent, SWT.NONE);
        Label l1 = new Label(this, SWT.NONE);
        l1.setText(message);

        setLayout(new FillLayout());
    }
}
