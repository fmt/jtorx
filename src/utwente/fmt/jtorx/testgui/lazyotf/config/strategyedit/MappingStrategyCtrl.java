package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.LocationTypeAssignmentView.SetTestTypeListener;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.EditorInvocation;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.EditorInvocation.LocationSetEditingResults;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIMappingStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * A controller for the MappingStrategy editor.
 */
class MappingStrategyCtrl {
    /** The decision strategy governed by this controller. */
    private final UIMappingStrategy subject;

    /**
     * Construct a new MappingStrategyCtrl instance.
     * 
     * @param subject
     *            The decision strategy to be governed by this controller.
     * @param view
     *            the MappingStrategyPaneView object to be governed by this
     *            controller.
     * @param sessionData
     *            The configuration session data object.
     * @param editors
     *            An object allowing the invocation of the various editors.
     */
    public MappingStrategyCtrl(UIMappingStrategy subject, MappingStrategyPaneView view,
            LazyOTFConfigSessionData sessionData, final EditorInvocation editors) {
        this.subject = subject;

        final MappingStrategyPaneView _view = view;

        view.addSetTestTypeListener(new SetTestTypeListener() {
            @Override
            public void setTestType(NamedSet<Location> locationSet,
                    TestType targetTestType) {
                onChangeAssignment(locationSet, targetTestType);
                _view.redraw(locationSet);
            }
        });

        view.addDisplayLocationSetEditorListener(new GenericListener<NamedSet<Location>>() {
            @Override
            public void onFiredEvent(NamedSet<Location> selectedLocSet) {
                LocationSetEditingResults result;
                if (selectedLocSet != null) {
                    result = editors.displayLocationSetEditor(selectedLocSet);
                }
                else {
                    result = editors.displayLocationSetEditor();
                }

                /*
                 * Deleted location sets get removed from mapping strategies
                 * during the displayLocationSetEditor call, so they can be
                 * safely removed from the editor.
                 */
                for (NamedSet<Location> loc : result.deletedLocationSets()) {
                    _view.removeLocationSet(loc);
                }

                _view.redraw();
            }
        });
    }

    /**
     * Change the test type of a given location.
     * 
     * @param loc
     *            The location whose test type is to be changed.
     * @param newType
     *            The location's new test type.
     */
    public void onChangeAssignment(NamedSet<Location> loc, TestType newType) {
        if (subject.getLocationSets().contains(loc)) {
            subject.setTestType(loc, null);
        }
        if (newType != null) {
            subject.setTestType(loc, newType);
        }
    }
}
