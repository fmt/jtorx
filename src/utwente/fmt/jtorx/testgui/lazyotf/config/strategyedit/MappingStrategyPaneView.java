package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import org.eclipse.swt.widgets.Composite;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.testgui.lazyotf.config.locationtypeedit.LocationTypeAssignmentView;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIMappingStrategy;

/**
 * The MappingStrategy editor control.
 */
class MappingStrategyPaneView extends LocationTypeAssignmentView {
    /** The edited mapping strategy. */
    private final UIMappingStrategy subject;

    /** The configuration session data object. */
    private final LazyOTFConfigSessionData sessionData;

    /**
     * Construct a new MappingStrategyPaneView instance.
     * 
     * @param parent
     *            The view's parent widget.
     * @param sessionData
     *            The main controller session data.
     * @param subject
     *            The strategy to be displayed.
     */
    public MappingStrategyPaneView(Composite parent, LazyOTFConfigSessionData sessionData,
            UIMappingStrategy subject) {
        super(parent, sessionData.getTestTypes(), true);
        this.subject = subject;
        this.sessionData = sessionData;

        useAllTestTypes();
        redraw();
    }

    /**
     * Redraw a specific location set.
     * 
     * @param locationSet
     *            The location set to be redrawn.
     */
    public void redraw(NamedSet<Location> locationSet) {
        removeLocationSet(locationSet);
        addLocationSet(locationSet, subject.getTestType(locationSet));
    }

    @Override
    public void redraw() {
        super.redraw();
        for (NamedSet<Location> locationSet : sessionData.getAllLocations()) {
            if (isDisplaying(locationSet)) {
                removeLocationSet(locationSet);
            }
            addLocationSet(locationSet, subject.getTestType(locationSet));
        }
    }
}
