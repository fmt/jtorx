package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.Condition;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.DumontCondition;
import utwente.fmt.jtorx.lazyotf.utilities.Utils.IndirectReference;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UISatStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.Pair;

/**
 * A widget for editing test-type/condition associations.
 */
class SatStrategyConditionEditorView extends Composite {
    /** The color used for distinguishing "global" conditions. */
    private final Color globalConditionTextColor = Display.getCurrent().getSystemColor(
            SWT.COLOR_DARK_GREEN);

    /** The (right-hand) table displaying the named conditions. */
    private Table namedConditionTable;

    /** The renaming and condition editor for the namedConditionTable. */
    private TableEditor namedConditionTableEditor;

    /**
     * The (left-hand) table displaying the mapping of conditions to test types.
     */
    private Table locationMapTable;

    /** The condition editor for the locationMapTable. */
    private TableEditor locationMapTableEditor;

    /** The edited decision strategy. */
    private final UISatStrategy subject;

    /** The named conditions, ordered for user-interface consistency. */
    private final List<Condition> orderedNamedConditions;

    /** The "global" conditions currently displayed by this view. */
    private final List<Condition> globalConditions = new ArrayList<Condition>();

    /** The currently edited location. */
    private NamedSet<Location> currentLocationSet;

    /** "Make Named Condition" context menu item. */
    private MenuItem makeNamedConditionMenuItem;

    /** "Delete Condition" context menu item. */
    private MenuItem deleteConditionMenuItem;

    /** "Make Condition Global" menu item. */
    private MenuItem globalConditionMenuItem;

    /** Listener called when a condition modification is requested by the user. */
    private final Set<GenericListener<Pair<Condition, String>>> modifyConditionListeners;

    /** Listener called when a condition renaming is requested by the user. */
    private final Set<GenericListener<Pair<Condition, String>>> renameConditionListeners;

    /**
     * Listener called when the use of a named condition is requested by the
     * user.
     */
    private final Set<GenericListener<Condition>> setConditionListeners;

    /**
     * An association of test types and menu items within the "Set Test Type"
     * context submenu.
     */
    private final Map<TestType, MenuItem> setTestTypeMenuMap = new HashMap<TestType, MenuItem>();

    /**
     * An association of test types and menu items within the "New Condition"
     * context submenu.
     */
    private final Map<TestType, MenuItem> newConditionTypeMenuMap = new HashMap<TestType, MenuItem>();

    /** true iff redrawing is enabled. */
    private boolean isRedrawEnabled = true;

    /** The set of conditions refused by the condition validator. */
    private final Set<Condition> badConditions;

    /**
     * Add a "global" condition to the list of named conditions. This condition
     * does not need to be part of a test-type->condition association within the
     * currently displayed strategy.
     * 
     * @param it
     *            The condition to be added.
     */
    public void addGlobalCondition(Condition it) {
        globalConditions.add(it);
        if (!orderedNamedConditions.contains(it)) {
            orderedNamedConditions.add(it);
        }
        redraw();
    }

    /**
     * Add a set of "global" conditions to the list of named conditions. These
     * conditions do not need to be part of a test-type->condition association
     * within the currently displayed strategy.
     * 
     * @param conditions
     *            The conditions to be added.
     */
    public void addGlobalConditions(Set<Condition> conditions) {
        for (Condition c : conditions) {
            globalConditions.add(c);
            if (!orderedNamedConditions.contains(c)) {
                orderedNamedConditions.add(c);
            }
        }
        redraw();
    }

    /**
     * Remove a "global" condition from the list of named conditions.
     * 
     * @param it
     *            The condition to be
     */
    public void removeGlobalCondition(Condition it) {
        globalConditions.remove(it);
        redraw();
    }

    /**
     * Add a condition to the list of named conditions.
     * 
     * @param it
     *            The named condition to be added.
     */
    public void addNamedCondition(Condition it) {
        if (!orderedNamedConditions.contains(it)) {
            orderedNamedConditions.add(it);
            redraw();
        }
    }

    /**
     * Add a listener handling requests to add a default condition to the given
     * test type.
     * 
     * @param listener
     *            A listener accepting TestType arguments.
     */
    public void addAddDefaultConditionRequestListener(final GenericListener<TestType> listener) {
        for (final TestType t : newConditionTypeMenuMap.keySet()) {
            final MenuItem menuItem = newConditionTypeMenuMap.get(t);
            menuItem.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent ev) {
                    listener.onFiredEvent(t);
                    redraw();
                }
            });
        }
    }

    /**
     * Add a listener handling requests to clone the given condition and make it
     * nameable.
     * 
     * @param listener
     *            A listener accepting Condition arguments.
     */
    public void addCloneToNamedConditionRequestListener(final GenericListener<Condition> listener) {
        makeNamedConditionMenuItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                if (locationMapTable.getSelectionCount() > 0) {
                    TableItem currentTI = locationMapTable.getSelection()[0];
                    TestType selectedTI = (TestType) currentTI.getData();
                    listener.onFiredEvent(subject.getCondition(currentLocationSet, selectedTI));
                    redraw();
                }
            }
        });
    }

    /**
     * Add a listener handling requests to delete a test-type/condition
     * association within the currently selected location.
     * 
     * @param listener
     *            A listener accepting TestType arguments. The TestType object
     *            passed to the listener is the one whose association is
     *            requested to be deleted.
     */
    public void addDeleteConditionRequestListener(final GenericListener<TestType> listener) {
        deleteConditionMenuItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                if (locationMapTable.getSelectionCount() > 0) {
                    TableItem currentSelection = locationMapTable.getSelection()[0];
                    listener.onFiredEvent((TestType) currentSelection.getData());
                    redraw();
                }
                deleteConditionMenuItem.setEnabled(false);
            }
        });
    }

    /**
     * Add a listener handling requests to modify a condition.
     * 
     * @param listener
     *            A listener accepting Pair<Condition, String> arguments
     *            supplying the condition instance and its requested new value.
     */
    public void addModifyConditionRequestListener(GenericListener<Pair<Condition, String>> listener) {
        modifyConditionListeners.add(listener);
    }

    /**
     * Add a listener handling requests to move a condition from one
     * test-type/condition association within the current location to another.
     * 
     * @param listener
     *            A listener accepting TestType instances. The TestType object
     *            passed to the listener is the new designated test type for the
     *            condition currently associated with the selected test type.
     */
    public void addChangeTargetTypeRequestListener(final GenericListener<TestType> listener) {
        for (final TestType t : setTestTypeMenuMap.keySet()) {
            MenuItem typeMenuItem = setTestTypeMenuMap.get(t);
            typeMenuItem.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent ev) {
                    listener.onFiredEvent(t);
                    redraw();
                }
            });
        }
    }

    /**
     * Add a listener handling requests to rename a named condition.
     * 
     * @param listener
     *            A listener accepting Pair<Condition, String> arguments. The
     *            Pair<Condition, String> argument passed to the listener
     *            contains the condition to be modified and its designated name.
     */
    public void addRenameConditionRequestListener(GenericListener<Pair<Condition, String>> listener) {
        renameConditionListeners.add(listener);
    }

    /**
     * Add a listener handling requests to modify conditions' "globality" state.
     * 
     * @param listener
     *            A listener accepting Pair<Condition, Boolean> arguments. The
     *            supplied condition is requested to be "global" iff the
     *            supplied boolean is true.
     */
    public void addMakeConditionGlobalRequestListener(
            final GenericListener<Pair<Condition, Boolean>> listener) {
        globalConditionMenuItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                if (namedConditionTable.getSelectionCount() > 0) {
                    TableItem currentSelection = namedConditionTable.getSelection()[0];
                    Condition c = (Condition) currentSelection.getData();
                    listener.onFiredEvent(new Pair<Condition, Boolean>(c, globalConditionMenuItem
                            .getSelection()));
                    redraw();
                }
            }
        });
    }

    /**
     * Add a listener handling requests to set a test type's associated
     * condition.
     * 
     * @param listener
     *            A listener accepting Condition arguments. A
     *            test-type/condition is requested to be installed into the
     *            currently selected location with the test type being the
     *            currently selected one.
     */
    public void addSetConditionRequestListener(GenericListener<Condition> listener) {
        setConditionListeners.add(listener);
    }

    /**
     * Construct a SatStrategyConditionEditorView instance.
     * 
     * @param parent
     *            The view's parent widget.
     * @param subject
     *            The edited strategy.
     * @param testTypes
     *            The set of test types to be made available to the user.
     */
    public SatStrategyConditionEditorView(Composite parent, UISatStrategy subject,
            Set<TestType> testTypes) {
        super(parent, SWT.NONE);
        this.subject = subject;
        orderedNamedConditions = new ArrayList<Condition>();
        badConditions = new HashSet<>();

        setConditionListeners = new HashSet<>();
        renameConditionListeners = new HashSet<>();
        modifyConditionListeners = new HashSet<>();

        setLayout(new FillLayout());
        CTabFolder tabFolder = new CTabFolder(this, SWT.BORDER);
        CTabItem mapEd = new CTabItem(tabFolder, SWT.NONE);
        mapEd.setText("Location Map");
        CTabItem condEd = new CTabItem(tabFolder, SWT.NONE);
        condEd.setText("Named Conditions");
        condEd.setControl(setupNamedConditionTable(tabFolder));
        mapEd.setControl(setupLocationMapTable(tabFolder, testTypes));

        redraw();
        this.pack();
    }

    /**
     * Marks a condition valid rsp. invalid.
     * 
     * @param c
     *            The condition which should be marked.
     * @param error
     *            Iff <tt>true</tt>, the condition is marked as invalid.
     */
    public void setConditionError(Condition c, boolean error) {
        if (error == true) {
            badConditions.add(c);
        }
        else {
            badConditions.remove(c);
        }
    }

    @Override
    public void setEnabled(boolean enabledness) {
        locationMapTable.setEnabled(enabledness);
        namedConditionTable.setEnabled(enabledness);
    }

    @Override
    public void setRedraw(boolean arg0) {
        isRedrawEnabled = arg0;
        super.setRedraw(arg0);

        if (arg0 == true) {
            redraw();
        }
    }

    /**
     * @param it
     *            The location to be edited.
     */
    public void setLocation(NamedSet<Location> it) {
        currentLocationSet = it;
        redraw();
        setEnabled(true);
    }

    @Override
    public void redraw() {
        super.redraw();

        if (!isRedrawEnabled) {
            return;
        }

        locationMapTable.setRedraw(false);
        namedConditionTable.setRedraw(false);

        locationMapTable.removeAll();
        namedConditionTable.removeAll();

        /* Update location map */
        for (TestType type : subject.getAssociatedTestTypes(currentLocationSet)) {
            Condition assocCond = subject.getCondition(currentLocationSet, type);
            TableItem matchingTableItem = new TableItem(locationMapTable, SWT.NONE);

            matchingTableItem.setText(1, type.getInternalName());

            String conditionText = "";
            if (assocCond.getName() != null) {
                conditionText = "" + assocCond.getName() + ":" + "\n";
            }
            conditionText += assocCond.toString();

            matchingTableItem.setText(0, conditionText);
            matchingTableItem.setData(type);

            if (badConditions.contains(assocCond)) {
                matchingTableItem.setForeground(0,
                        Display.getDefault().getSystemColor(SWT.COLOR_RED));
                matchingTableItem.setText(2, "Faulty Condition");
            }
        }

        /* Update named conditions */
        for (Condition c : orderedNamedConditions) {
            TableItem condTI = new TableItem(namedConditionTable, SWT.NONE);
            condTI.setText(0, getConditionRepresentation(c));
            condTI.setText(1, c.toString());
            condTI.setData(c);

            if (globalConditions.contains(c)) {
                condTI.setForeground(0, globalConditionTextColor);
            }
            else {
                condTI.setForeground(0, Display.getCurrent().getSystemColor(SWT.DEFAULT));
            }
        }

        locationMapTable.setRedraw(true);
        namedConditionTable.setRedraw(true);

    }

    /**
     * @param c
     *            A Condition object.
     * @return A textual representation of the given condition object.
     */
    private String getConditionRepresentation(Condition c) {
        return c.getName() == null ? c.toString() : c.getName();
    }

    /**
     * Display a TableEditor in the given table.
     * 
     * @param ctrl
     *            The table with which the editor is associated.
     * @param editor
     *            The TableEditor to be displayed.
     * @param item
     *            The TableItem above which the editor is to be displayed.
     * @param initialText
     *            The editor's initial content, in text form.
     * @param col
     *            The index of the table column in which the editor is to be
     *            displayed.
     * @param onEditingFinished
     *            Runnable object called on closing the editor.
     */
    private void displayEditor(final Table ctrl, final TableEditor editor, final TableItem item,
            String initialText, final int col, final Runnable onEditingFinished) {
        if (editor.getEditor() != null) {
            editor.getEditor().dispose();
        }
        final Text newEditor = new Text(ctrl, SWT.NONE);
        newEditor.setText(initialText);
        newEditor.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent arg0) {
            }

            @Override
            public void focusLost(FocusEvent arg0) {
                onEditingFinished.run();
                newEditor.dispose();
            }
        });
        newEditor.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetDefaultSelected(SelectionEvent ev) {
                onEditingFinished.run();
                newEditor.dispose();
            }
        });
        newEditor.selectAll();
        newEditor.setFocus();
        editor.setEditor(newEditor, item, col);
    }

    /**
     * Display a condition editor for a given condition within the
     * test-type/condition association table.
     * 
     * @param item
     *            The condition's table item.
     */
    private void displayConcreteConditionEditor(final TableItem item) {
        // System.out.println("Displaying conc ed for " +
        // this.currentLocation.getName());
        final Condition cond = subject.getCondition(currentLocationSet, (TestType) item.getData());
        final IndirectReference<String> initialTextIndirect = new IndirectReference<String>();

        cond.accept(new SatStrategy.ConditionVisitorAdapter() {
            @Override
            public void visit(DumontCondition it) {
                initialTextIndirect.referencedObject = ((DumontCondition) cond).getCondition();
            }

            @Override
            public void otherMethodCalled(Condition it) {
                initialTextIndirect.referencedObject = it.toString();
            }
        });

        String initialText = initialTextIndirect.referencedObject;

        displayEditor(locationMapTable, locationMapTableEditor, item, initialText, 0,
                new Runnable() {
                    @Override
                    public void run() {
                        onHideConcreteConditionEditor();
                    }
                });
    }

    /**
     * Display a condition content editor within the named condition table.
     * 
     * @param item
     *            The condition's table item.
     */
    private void displayConditionContentEditor(final TableItem item) {
        final Condition cond = (Condition) item.getData();
        final IndirectReference<String> initialTextIndirect = new IndirectReference<String>();

        cond.accept(new SatStrategy.ConditionVisitorAdapter() {
            @Override
            public void visit(DumontCondition it) {
                initialTextIndirect.referencedObject = ((DumontCondition) cond).getCondition();
            }

            @Override
            public void otherMethodCalled(Condition it) {
                initialTextIndirect.referencedObject = it.toString();
            }
        });

        String initialText = initialTextIndirect.referencedObject;
        displayEditor(namedConditionTable, namedConditionTableEditor, item, initialText, 1,
                new Runnable() {
                    @Override
                    public void run() {
                        onHideNamedConditionEditor();
                    }
                });
    }

    /**
     * Display a condition name editor within the named condition table.
     * 
     * @param item
     *            The condition's table item.
     */
    private void displayConditionNameEditor(final TableItem item) {
        final Condition cond = (Condition) item.getData();
        final SatStrategyConditionEditorView _this = this;
        displayEditor(namedConditionTable, namedConditionTableEditor, item, cond.getName(), 0,
                new Runnable() {
                    @Override
                    public void run() {
                        for (GenericListener<Pair<Condition, String>> listener : _this.renameConditionListeners) {
                            listener.onFiredEvent(new Pair<Condition, String>(cond,
                                    ((Text) namedConditionTableEditor.getEditor()).getText()));
                        }
                        redraw();
                    }
                });
    }

    /**
     * Hide the test-type/condition association table's condition editor.
     */
    private void onHideConcreteConditionEditor() {
        if (locationMapTableEditor.getEditor() != null) {
            if (!locationMapTableEditor.getEditor().isDisposed()) {
                TestType currentType = (TestType) locationMapTableEditor.getItem().getData();
                Condition c = subject.getCondition(currentLocationSet, currentType);
                String newCond = ((Text) locationMapTableEditor.getEditor()).getText();
                String parenNewCond = "(" + newCond + ")";
                if (!parenNewCond.trim().equals(c.toString().trim())) {
                    for (GenericListener<Pair<Condition, String>> listener : modifyConditionListeners) {
                        listener.onFiredEvent(new Pair<Condition, String>(c, newCond));
                    }
                    redraw();
                }
            }
        }
    }

    /**
     * Hide the named condition table's condition editor.
     */
    private void onHideNamedConditionEditor() {
        if (namedConditionTableEditor.getEditor() != null) {
            if (!namedConditionTableEditor.getEditor().isDisposed()) {
                Text t = (Text) namedConditionTableEditor.getEditor();
                Condition c = (Condition) namedConditionTableEditor.getItem().getData();
                for (GenericListener<Pair<Condition, String>> listener : modifyConditionListeners) {
                    listener.onFiredEvent(new Pair<Condition, String>(c, t.getText()));
                }
                redraw();
            }
        }
    }

    /**
     * Get all named conditions used by the given SatStrategy object.
     * 
     * @param source
     *            The SatStrategy object whose named conditions are to be
     *            gathered.
     */
    private void gatherNamedConditions(UISatStrategy source) {
        Set<NamedSet<Location>> stratLocations = source.getLocationSets();
        for (NamedSet<Location> l : stratLocations) {
            for (TestType t : source.getAssociatedTestTypes(l)) {
                Condition c = source.getCondition(l, t);
                if (c.getName() == null) {
                    continue;
                }

                if (c.getName() != null) {
                    if (!orderedNamedConditions.contains(c)) {
                        orderedNamedConditions.add(c);
                    }
                }
            }
        }
        for (Condition c : globalConditions) {
            if (!orderedNamedConditions.contains(c)) {
                orderedNamedConditions.add(c);
            }
        }
    }

    /**
     * Set up the named condition table's context menu.
     */
    private void setupNamedConditionContextMenu() {
        final Menu contextMenu = new Menu(namedConditionTable);
        namedConditionTable.setMenu(contextMenu);
        globalConditionMenuItem = new MenuItem(contextMenu, SWT.CHECK);
        globalConditionMenuItem.setText("Globally Available");

        namedConditionTable.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                if (namedConditionTable.getSelectionCount() > 0) {
                    TableItem selected = namedConditionTable.getSelection()[0];
                    Condition c = (Condition) selected.getData();
                    globalConditionMenuItem.setSelection(globalConditions.contains(c));
                }
            }
        });

        contextMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuShown(MenuEvent arg0) {
                globalConditionMenuItem.setEnabled(namedConditionTable.getSelectionCount() > 0);
            }
        });
    }

    /**
     * Set up the named condition table.
     * 
     * @param parent
     *            The table's parent widget.
     * @return The newly created table widget.
     */
    private Composite setupNamedConditionTable(Composite parent) {
        gatherNamedConditions(subject);
        namedConditionTable = new Table(parent, SWT.V_SCROLL | SWT.BORDER);
        namedConditionTable.setLayout(new FillLayout());

        final TableColumn nameCol = new TableColumn(namedConditionTable, SWT.LEFT);
        nameCol.setText("Name");
        final TableColumn condCol = new TableColumn(namedConditionTable, SWT.LEFT);
        condCol.setText("Condition");

        namedConditionTable.setHeaderVisible(true);

        nameCol.setWidth(150);
        condCol.setWidth(200);

        /*
         * for(Condition c : this.orderedNamedConditions) {
         * addNamedCondition(c); }
         */

        namedConditionTableEditor = new TableEditor(namedConditionTable);
        namedConditionTableEditor.minimumWidth = 150;
        namedConditionTableEditor.grabHorizontal = true;

        namedConditionTable.addListener(SWT.MouseDoubleClick, new Listener() {
            @Override
            public void handleEvent(Event arg0) {
                if (namedConditionTable.getSelectionCount() > 0) {
                    if (arg0.x < nameCol.getWidth()) {
                        displayConditionNameEditor(namedConditionTable.getSelection()[0]);
                    }
                    else {
                        displayConditionContentEditor(namedConditionTable.getSelection()[0]);
                    }
                }
            }
        });

        namedConditionTable.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onHideNamedConditionEditor();
            }
        });

        setupNamedConditionContextMenu();
        return namedConditionTable;
    }

    /**
     * Set up the test-type/condition association table's context menu.
     * 
     * @param testTypes
     *            The test types available for selection.
     */
    private void setupLocationMapContextMenu(Set<TestType> testTypes) {
        final SatStrategyConditionEditorView _this = this;

        Menu contextMenu = new Menu(locationMapTable);
        locationMapTable.setMenu(contextMenu);

        final MenuItem addTestTypes = new MenuItem(contextMenu, SWT.CASCADE);
        addTestTypes.setText("Add condition for");
        final Menu addTestTypesSub = new Menu(addTestTypes);
        addTestTypes.setMenu(addTestTypesSub);
        for (final TestType type : testTypes) {
            MenuItem typeMenuItem = new MenuItem(addTestTypesSub, SWT.PUSH);
            typeMenuItem.setText(type.getInternalName());
            newConditionTypeMenuMap.put(type, typeMenuItem);
            typeMenuItem.setData(type);
        }

        final MenuItem setTestTypes = new MenuItem(contextMenu, SWT.CASCADE);
        setTestTypes.setText("Set test type");
        final Menu setTestTypesSub = new Menu(setTestTypes);
        setTestTypes.setMenu(setTestTypesSub);
        for (final TestType type : testTypes) {
            MenuItem typeMenuItem = new MenuItem(setTestTypesSub, SWT.PUSH);
            typeMenuItem.setText(type.getInternalName());
            typeMenuItem.setData(type);
            setTestTypeMenuMap.put(type, typeMenuItem);

        }

        deleteConditionMenuItem = new MenuItem(contextMenu, SWT.PUSH);
        deleteConditionMenuItem.setText("Delete condition");
        deleteConditionMenuItem.setEnabled(false);

        @SuppressWarnings("unused")
        MenuItem sep1 = new MenuItem(contextMenu, SWT.SEPARATOR);

        final MenuItem useNamedCondition = new MenuItem(contextMenu, SWT.CASCADE);
        final Menu useNamedConditionMenu = new Menu(useNamedCondition);
        useNamedCondition.setMenu(useNamedConditionMenu);
        useNamedCondition.setText("Use named condition");
        useNamedCondition.setEnabled(false);

        makeNamedConditionMenuItem = new MenuItem(contextMenu, SWT.PUSH);
        makeNamedConditionMenuItem.setText("Make named condition...");
        makeNamedConditionMenuItem.setEnabled(false);

        locationMapTable.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                deleteConditionMenuItem.setEnabled(locationMapTable.getSelectionCount() > 0);
            }
        });

        contextMenu.addMenuListener(new MenuListener() {
            @Override
            public void menuHidden(MenuEvent arg0) {

            }

            @Override
            public void menuShown(MenuEvent arg0) {
                /* Update the "add conditon" menu */
                for (MenuItem mi : addTestTypesSub.getItems()) {
                    TestType associatedType = (TestType) mi.getData();
                    boolean enabled = !subject.getAssociatedTestTypes(currentLocationSet).contains(
                            associatedType);
                    mi.setEnabled(enabled);
                }

                boolean hasSelection = (locationMapTable.getSelectionCount() > 0);
                useNamedCondition.setEnabled(hasSelection);
                makeNamedConditionMenuItem.setEnabled(hasSelection);
                deleteConditionMenuItem.setEnabled(hasSelection);
                setTestTypes.setEnabled(hasSelection);

                if (hasSelection) {

                    System.out.println("Associations for " + currentLocationSet.getDisplayName()
                            + ":");
                    for (TestType t : subject.getAssociatedTestTypes(currentLocationSet)) {
                        System.out.println(" " + t.getInternalName());
                    }

                    /* Update the "set type" menu */
                    for (MenuItem mi : setTestTypesSub.getItems()) {
                        TestType associatedType = (TestType) mi.getData();
                        boolean enabled = !subject.getAssociatedTestTypes(currentLocationSet)
                                .contains(associatedType);
                        mi.setEnabled(enabled);
                    }

                    /* Reconstruct "use named condition" context menu */
                    for (MenuItem mi : useNamedConditionMenu.getItems()) {
                        mi.dispose();
                    }

                    for (final Condition cond : orderedNamedConditions) {
                        final MenuItem condMenuItem = new MenuItem(useNamedConditionMenu, SWT.PUSH);
                        condMenuItem.setText(cond.getName());

                        condMenuItem.addSelectionListener(new SelectionAdapter() {
                            @Override
                            public void widgetSelected(SelectionEvent ev) {
                                for (GenericListener<Condition> listener : _this.setConditionListeners) {
                                    listener.onFiredEvent(cond);
                                }
                                redraw();
                            }
                        });
                    }

                }
            }

        });
    }

    /**
     * Set up the test-type/condition association table.
     * 
     * @param parent
     *            The table's parent widget.
     * @param testTypes
     *            The test types available for selection within its context
     *            menu.
     * @return The location map widget.
     */
    private Composite setupLocationMapTable(Composite parent, Set<TestType> testTypes) {
        locationMapTable = new Table(parent, SWT.V_SCROLL | SWT.BORDER);
        locationMapTable.setHeaderVisible(true);
        locationMapTable.setLayout(new FillLayout());
        TableColumn condCol = new TableColumn(locationMapTable, SWT.LEFT);
        condCol.setText("Condition");
        condCol.setWidth(200);
        TableColumn typeCol = new TableColumn(locationMapTable, SWT.LEFT);
        typeCol.setText("Test Type");
        typeCol.setWidth(100);
        TableColumn errorCol = new TableColumn(locationMapTable, SWT.LEFT);
        errorCol.setWidth(100);
        errorCol.setText("Errors");
        locationMapTable.setEnabled(false);

        /* Set up text editor */
        locationMapTableEditor = new TableEditor(locationMapTable);
        locationMapTableEditor.grabHorizontal = true;
        locationMapTableEditor.minimumWidth = 150;

        locationMapTable.addListener(SWT.MouseDoubleClick, new Listener() {
            @Override
            public void handleEvent(Event arg0) {
                TableItem[] selectedItems = locationMapTable.getSelection();
                if (selectedItems.length == 1) {
                    TableItem selectedItem = selectedItems[0];
                    displayConcreteConditionEditor(selectedItem);
                }
            }
        });

        locationMapTable.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onHideConcreteConditionEditor();
            }
        });

        setupLocationMapContextMenu(testTypes);

        return locationMapTable;
    }

    /**
     * @return The test type currently selected in the test-type/condition
     *         association or null if no such element is selected.
     */
    public TestType getSelectedTestType() {
        if (locationMapTable.getSelectionCount() == 0) {
            return null;
        }
        else {
            return (TestType) locationMapTable.getSelection()[0].getData();
        }
    }

    /**
     * @return The condition currently selected in the named condition table or
     *         null if no such element is selected.s
     */
    public Condition getSelectedNamedCondition() {
        if (namedConditionTable.getSelectionCount() == 0) {
            return null;
        }
        else {
            return (Condition) namedConditionTable.getSelection()[0].getData();
        }
    }

}
