package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import java.util.Set;

import utwente.fmt.jtorx.lazyotf.ExpressionValidator.ValidationException;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.Condition;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.DumontCondition;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategyVisitorAdapter;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UISatStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.Pair;

/**
 * The controller part for the SatStrategy editor.
 */
class SatStrategyCtrl {
    /** The edited strategy. */
    private final UISatStrategy strategy;

    /** The condition-editor view. */
    private final SatStrategyConditionEditorView conditionEditorView;

    /** The condition-list view. */
    private final SatStrategyLocationListView conditionListView;

    /** The set of conditions marked "global". */
    private final Set<Condition> globalConditions;

    /** The decision strategy editor's main controller. */
    private final StrategyEditorImpl mainCtrl;

    /** The currently edited location. */
    private NamedSet<Location> currentLocationSet;

    /**
     * Construct a new SatStrategyCtrl instance.
     * 
     * @param subject
     *            The governed SatStrategy instance.
     * @param globalConditions
     *            The set of conditions marked "global".
     * @param mainCtrl
     *            The decision strategy editor's main controller.
     * @param view
     *            The view to be governed by this controller.
     */
    public SatStrategyCtrl(UISatStrategy subject, Set<Condition> globalConditions,
            StrategyEditorImpl mainCtrl, SatStrategyPaneView view) {
        strategy = subject;
        this.globalConditions = globalConditions;
        this.mainCtrl = mainCtrl;
        conditionEditorView = view.getEditorView();
        conditionListView = view.getSelectionView();

        conditionEditorView.addGlobalConditions(globalConditions);

        setupListeners();
        initView();
    }

    private void initView() {
        for (NamedSet<Location> loc : strategy.getLocationSets()) {
            for (TestType t : strategy.getAssociatedTestTypes(loc)) {
                Condition cond = strategy.getCondition(loc, t);
                try {
                    mainCtrl.getConditionValidator().checkCondition(cond.toString());
                }
                catch (ValidationException e) {
                    conditionEditorView.setConditionError(cond, true);
                }
            }
        }
        conditionEditorView.redraw();
    }

    /**
     * This method handles requests to associate a Location/TestType pair with a
     * SatStrategy condition c.
     * 
     * @param loc
     *            The target location.
     * @param type
     *            The target test type.
     * @param c
     *            The target condition.
     */
    private void onSetCondition(NamedSet<Location> loc, TestType type, Condition c) {
        if (strategy.getAssociatedTestTypes(loc).contains(type)) {
            strategy.removeAssociation(loc, type);
        }
        strategy.associate(loc, type, c);

        if (conditionListView != null) {
            conditionListView.redraw();
        }
    }

    /**
     * This method handles requests to associate a Location/TestType pair with a
     * condition represented by the given condition string.
     * 
     * @param loc
     *            The target location.
     * @param type
     *            The target test type.
     * @param condition
     *            The target condition represented as a String.
     */
    @SuppressWarnings("unused")
    private void onSetCondition(NamedSet<Location> loc, TestType type, String condition) {
        if (strategy.getAssociatedTestTypes(loc).contains(type)) {
            strategy.removeAssociation(loc, type);
        }
        strategy.associate(loc, type, new SatStrategy.DumontCondition(condition));

        if (conditionListView != null) {
            conditionListView.redraw();
        }
    }

    /**
     * This method handles requests to change a condition.
     * 
     * @param c
     *            The condition to be changed.
     * @param newCondition
     *            The new meaning of the condition in text form.
     */
    private void onModifyCondition(Condition c, String newCondition) {
        DumontCondition dc = (DumontCondition) c;
        dc.setCondition(newCondition);

        if (conditionListView != null) {
            conditionListView.redraw();
        }

        try {
            mainCtrl.getConditionValidator().checkCondition(newCondition);
            conditionEditorView.setConditionError(dc, false);
        }
        catch (ValidationException e) {
            conditionEditorView.setConditionError(dc, true);
        }
    }

    /**
     * This method handles requests to remove the association of a
     * Location/TestType pair with its condition.
     * 
     * @param l
     *            The target location.
     * @param t
     *            The target test type.
     */
    private void onDeleteCondition(NamedSet<Location> l, TestType t) {
        strategy.removeAssociation(l, t);

        if (conditionListView != null) {
            conditionListView.redraw();
        }
    }

    /**
     * This method handles requests to remove all test-type/condition
     * associations from the given location.
     * 
     * @param it
     *            The location in which the deletion takes place.
     */
    private void onDeleteLocation(NamedSet<Location> it) {
        for (TestType t : strategy.getAssociatedTestTypes(it)) {
            strategy.removeAssociation(it, t);
        }
        conditionListView.remove(it);
        conditionEditorView.setEnabled(false);
        currentLocationSet = it;
    }

    /**
     * This method handles requests to associate the given test type with a
     * "vanilla" condition (always evaluating to "true").
     * 
     * @param type
     *            The test type with which the new condition is to be
     *            associated.
     */
    private void onInsertDefaultCondition(TestType type) {
        Condition newCondition = new DumontCondition("true == true");
        onSetCondition(currentLocationSet, type, newCondition);
    }

    /**
     * This method handles requests to rename conditions.
     * 
     * @param c
     *            The target condition.
     * @param newName
     *            The condition's new name.
     */
    private void onRenameCondition(Condition c, String newName) {
        c.setName(newName);

        if (conditionListView != null) {
            conditionListView.redraw();
        }
    }

    /**
     * This method handles requests to retarget conditions to other test types.
     * 
     * @param loc
     *            The target location.
     * @param oldType
     *            The "old" type whose associated condition is to be removed.
     * @param newType
     *            The "new" type which will be associated with the condition
     *            formerly associated with the "old" type.
     */
    private void onRetargetCondition(NamedSet<Location> loc, TestType oldType, TestType newType) {
        Condition c = strategy.getCondition(loc, oldType);
        strategy.removeAssociation(loc, oldType);
        strategy.associate(loc, newType, c);

        if (conditionListView != null) {
            conditionListView.redraw();
        }
    }

    /**
     * @return A list of conditions marked "global".
     */
    protected Set<Condition> getGlobalConditions() {
        return globalConditions;
    }

    /**
     * This method handles requests to make conditions "global".
     * 
     * @param c
     *            The target condition.
     * @param globality
     *            If true, the condition will be made "global"; otherwise, it
     *            will be marked as "not global" and be replaced by equivalent
     *            new conditions in all other SatStrategy instances.
     */
    private void onMakeConditionGlobal(final Condition c, boolean globality) {
        System.out.println("Setting globality to " + globality);
        if (globality == true) {
            if (!globalConditions.contains(c)) {
                conditionEditorView.addGlobalCondition(c);
            }
            globalConditions.add(c);
        }
        else if (globalConditions.contains(c)) {
            globalConditions.remove(c);
            conditionEditorView.removeGlobalCondition(c);
            for (final UIDecisionStrategy s : mainCtrl.getModifiedStrategySet()) {
                s.accept(new UIDecisionStrategyVisitorAdapter() {
                    @Override
                    public void visit(UISatStrategy it) {
                        Condition clone = null;

                        UISatStrategy satStrategy = (UISatStrategy) s;
                        for (NamedSet<Location> loc : satStrategy.getLocationSets()) {
                            for (TestType type : satStrategy.getAssociatedTestTypes(loc)) {
                                if (satStrategy.getCondition(loc, type) == c) {
                                    if (clone == null) {
                                        clone = c.clone();
                                    }
                                    satStrategy.associate(loc, type, clone);
                                }
                            }
                        }
                    }
                });
            }
        }
    }

    private void onDisplayLocationSetEditor(NamedSet<Location> arg) {
        if (arg == null) {
            mainCtrl.getEditorInvocationObj().displayLocationSetEditor();
        }
        else {
            mainCtrl.getEditorInvocationObj().displayLocationSetEditor(arg);
        }
        conditionEditorView.redraw();
        conditionListView.redraw();
        conditionListView.setLocationSets(mainCtrl.getSessionData().getAllLocations());
        conditionEditorView.setLocation((NamedSet<Location>) null);
    }

    /**
     * Set up the glue to the view.
     */
    private void setupListeners() {
        final SatStrategyCtrl _this = this;

        conditionListView.addLocationSelectionListener(new GenericListener<NamedSet<Location>>() {
            @Override
            public void onFiredEvent(NamedSet<Location> loc) {
                _this.currentLocationSet = loc;
                _this.conditionEditorView.setLocation(loc);
            }
        });

        conditionListView
                .addRemoveLocationRequestListener(new GenericListener<NamedSet<Location>>() {
                    @Override
                    public void onFiredEvent(NamedSet<Location> loc) {
                        _this.onDeleteLocation(loc);
                    }
                });

        conditionListView.addDisplayLocationSetListener(new GenericListener<NamedSet<Location>>() {
            @Override
            public void onFiredEvent(NamedSet<Location> arg) {
                _this.onDisplayLocationSetEditor(arg);
            }
        });

        conditionEditorView.addAddDefaultConditionRequestListener(new GenericListener<TestType>() {
            @Override
            public void onFiredEvent(TestType type) {
                _this.onInsertDefaultCondition(type);
            }
        });

        conditionEditorView.addChangeTargetTypeRequestListener(new GenericListener<TestType>() {
            @Override
            public void onFiredEvent(TestType newType) {
                _this.onRetargetCondition(currentLocationSet,
                        _this.conditionEditorView.getSelectedTestType(), newType);
            }
        });

        conditionEditorView
                .addCloneToNamedConditionRequestListener(new GenericListener<Condition>() {
                    @Override
                    public void onFiredEvent(Condition cond) {
                        TestType type = conditionEditorView.getSelectedTestType();
                        Condition clone = cond.clone();
                        clone.setName("New unnamed condition");
                        strategy.associate(_this.currentLocationSet, type, clone);
                        conditionEditorView.addNamedCondition(clone);
                        conditionListView.redraw();
                    }
                });

        conditionEditorView.addDeleteConditionRequestListener(new GenericListener<TestType>() {
            @Override
            public void onFiredEvent(TestType type) {
                _this.onDeleteCondition(currentLocationSet, type);
            }
        });

        conditionEditorView
                .addMakeConditionGlobalRequestListener(new GenericListener<Pair<Condition, Boolean>>() {
                    @Override
                    public void onFiredEvent(Pair<Condition, Boolean> event) {
                        _this.onMakeConditionGlobal(event.getCar(), event.getCdr());
                    }
                });

        conditionEditorView
                .addModifyConditionRequestListener(new GenericListener<Pair<Condition, String>>() {
                    @Override
                    public void onFiredEvent(Pair<Condition, String> event) {
                        _this.onModifyCondition(event.getCar(), event.getCdr());
                    }
                });

        conditionEditorView
                .addRenameConditionRequestListener(new GenericListener<Pair<Condition, String>>() {
                    @Override
                    public void onFiredEvent(Pair<Condition, String> event) {
                        _this.onRenameCondition(event.getCar(), event.getCdr());
                    }
                });

        conditionEditorView.addSetConditionRequestListener(new GenericListener<Condition>() {
            @Override
            public void onFiredEvent(Condition condition) {
                _this.onSetCondition(_this.currentLocationSet,
                        _this.conditionEditorView.getSelectedTestType(), condition);
            }
        });
    }
}
