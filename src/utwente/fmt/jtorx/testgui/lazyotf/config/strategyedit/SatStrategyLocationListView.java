package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MenuListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UISatStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * A view displaying a list of conditions.
 */
class SatStrategyLocationListView extends Composite {
    /** The table representing the list of locations and associated conditions. */
    private final Table conditionTable;

    /** An association of locations and table items. */
    private final Map<NamedSet<Location>, TableItem> locations;

    /** The displayed SatStrategy instance. */
    private final UISatStrategy subject;

    /** The table's currenet selection. */
    private TableItem currentSelection = null;

    /** The "Remove Location" context menu item. */
    private MenuItem removeLocationMenuItem;

    /** The listener called when the location selection changed. */
    private final Set<GenericListener<NamedSet<Location>>> locationSetSelectionListeners;

    /**
     * The set of listeners called when the user requests to use the location
     * set editor.
     */
    private Set<GenericListener<NamedSet<Location>>> displayLocationSetEditorListeners;

    /** The value of the last call to setRedraw(boolean). */
    private boolean isRedrawEnabled = true;

    /**
     * Add a listener for "location selected" events.
     * 
     * @param listener
     *            A listener accepting Location objects.
     */
    public void addLocationSelectionListener(GenericListener<NamedSet<Location>> listener) {
        locationSetSelectionListeners.add(listener);
    }

    /**
     * Add a listener getting fired when the user requests to use the location
     * set editor.
     * 
     * @param listener
     *            The listener to be added.
     */
    public void addDisplayLocationSetListener(GenericListener<NamedSet<Location>> listener) {
        displayLocationSetEditorListeners.add(listener);
    }

    /**
     * Add a listener handling location removal requests.
     * 
     * @param listener
     *            A listener accepting Location objects.
     */
    public void addRemoveLocationRequestListener(final GenericListener<NamedSet<Location>> listener) {
        removeLocationMenuItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                if (conditionTable.getSelectionCount() > 0) {
                    TableItem locationTI = conditionTable.getSelection()[0];
                    @SuppressWarnings("unchecked")
                    NamedSet<Location> loc = (NamedSet<Location>) locationTI.getData();
                    listener.onFiredEvent(loc);
                    redraw();
                }
            }
        });
    }

    /**
     * Construct a new SatStrategyLocationListView instance.
     * 
     * @param parent
     *            The view's parent widget.
     * @param subject
     *            The decision strategy to be displayed by this editor.
     * @param locationSets
     *            The set of locations to be displayed.
     */
    public SatStrategyLocationListView(Composite parent, UISatStrategy subject,
            Collection<NamedSet<Location>> locationSets) {
        super(parent, SWT.NONE);

        locations = new HashMap<NamedSet<Location>, TableItem>();
        displayLocationSetEditorListeners = new HashSet<>();
        locationSetSelectionListeners = new HashSet<>();

        this.subject = subject;
        conditionTable = new Table(this, SWT.BORDER | SWT.V_SCROLL);
        conditionTable.setHeaderVisible(true);

        TableColumn conditionNameCol = new TableColumn(conditionTable, SWT.LEFT);
        conditionNameCol.setText("Location");
        conditionNameCol.setWidth(120);

        TableColumn conditionCol = new TableColumn(conditionTable, SWT.LEFT);
        conditionCol.setText("Conditions");
        conditionCol.setWidth(240);

        final SatStrategyLocationListView _this = this;
        conditionTable.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                TableItem newSelection = (TableItem) ev.item;
                if (newSelection != getCurrentSelection()) {
                    setCurrentSelection(newSelection);
                    @SuppressWarnings("unchecked")
                    final NamedSet<Location> loc = (NamedSet<Location>) newSelection.getData();
                    for (GenericListener<NamedSet<Location>> listener : _this.locationSetSelectionListeners) {
                        listener.onFiredEvent(loc);
                    }
                }
            }
        });

        for (NamedSet<Location> loc : lexSortedLocations(this.subject.getLocationSets())) {
            addLocation(loc);
        }

        setLocationSets(locationSets);

        setLayout(new FillLayout());
        this.pack();
    }

    /**
     * Set the location sets available for selection by the user.
     * 
     * @param locationSets
     *            The new collection of available location sets.
     */
    public void setLocationSets(Collection<NamedSet<Location>> locationSets) {
        setupContextMenu(locationSets);
    }

    /**
     * Set up the tree's context menu.
     * 
     * @param locationSets
     *            The locations to be displayed in the "Add Location" context
     *            submenu.
     */
    private void setupContextMenu(Collection<NamedSet<Location>> locationSets) {
        Listener[] removeSelectionListeners;
        if (conditionTable.getMenu() != null
                && !conditionTable.getMenu().isDisposed()) {
            removeSelectionListeners = removeLocationMenuItem.getListeners(SWT.Selection);
            conditionTable.getMenu().dispose();
        }
        else {
            removeSelectionListeners = new Listener[] {};
        }

        final Menu contextMenu = new Menu(conditionTable);
        final SatStrategyLocationListView _this = this;

        conditionTable.setMenu(contextMenu);

        final MenuItem addLocationItem = new MenuItem(contextMenu, SWT.CASCADE);
        addLocationItem.setText("Add Location");

        final Menu addLocationSub = new Menu(addLocationItem);
        addLocationItem.setMenu(addLocationSub);

        for (final NamedSet<Location> l : lexSortedLocations(locationSets)) {
            MenuItem locMenuItem = new MenuItem(addLocationSub, SWT.PUSH);
            locMenuItem.setText(l.getDisplayName());
            locMenuItem.setData(l);
            locMenuItem.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent ev) {
                    addLocation(l);
                }
            });
        }

        final MenuItem displayLocationSetEditor = new MenuItem(contextMenu, SWT.PUSH);
        displayLocationSetEditor.setText("Edit location sets...");
        displayLocationSetEditor.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onDisplayLocationSetEditor();
            }
        });

        removeLocationMenuItem = new MenuItem(contextMenu, SWT.PUSH);
        removeLocationMenuItem.setText("Delete Location");

        for (Listener listener : removeSelectionListeners) {
            removeLocationMenuItem.addListener(SWT.Selection, listener);
        }

        final MenuItem fRemoveLocationItem = removeLocationMenuItem;

        contextMenu.addMenuListener(new MenuListener() {
            @Override
            public void menuHidden(MenuEvent arg0) {
            }

            @Override
            public void menuShown(MenuEvent arg0) {
                fRemoveLocationItem.setEnabled(conditionTable.getSelectionCount() > 0);
                for (MenuItem locMenuItem : addLocationSub.getItems()) {
                    locMenuItem.setEnabled(!_this.locations.containsKey(locMenuItem.getData()));
                }
            }
        });
    }

    /**
     * @return The currently selected location/condition table item (as
     *         remembered using setCurrentSelection())
     */
    private TableItem getCurrentSelection() {
        return currentSelection;
    }

    /**
     * Remember a new currently selected table item.
     * 
     * @param it
     *            The currently selected table item.
     */
    private void setCurrentSelection(TableItem it) {
        currentSelection = it;
    }

    /**
     * Add a location to the location/condition table and redraw the new item.
     * 
     * @param it
     *            The location to be added.
     */
    public void addLocation(NamedSet<Location> it) {
        TableItem newTableItem = new TableItem(conditionTable, SWT.NONE);
        newTableItem.setData(it);
        redraw(newTableItem);
        locations.put(it, newTableItem);
    }

    /**
     * Update the given location's representation.
     * 
     * @param locationSet
     *            The target location set.
     */
    public void redraw(NamedSet<Location> locationSet) {
        if (locations.containsKey(locationSet)) {
            redraw(locations.get(locationSet));
        }
    }

    /**
     * Remove a location from the location/condition list.
     * 
     * @param locationSet
     *            The location set to be removed.
     */
    public void remove(NamedSet<Location> locationSet) {
        for (TableItem ti : conditionTable.getItems()) {
            if (ti.getData().equals(locationSet)) {
                if (currentSelection == ti) {
                    currentSelection = null;
                }
                ti.dispose();
                locations.remove(locationSet);
            }
        }
    }

    /**
     * Redraw a row within the location/condition table.
     * 
     * @param it
     *            The TableItem belonging to the row to be redrawn.
     */
    private void redraw(TableItem it) {
        if (!isRedrawEnabled) {
            return;
        }

        @SuppressWarnings("unchecked")
        NamedSet<Location> associatedLocationSet = (NamedSet<Location>) it.getData();
        it.setText(0, associatedLocationSet.getDisplayName());

        String associations = "";
        Iterator<TestType> ttIterator;
        ttIterator = subject.getAssociatedTestTypes(associatedLocationSet).iterator();

        while (ttIterator.hasNext()) {
            TestType type = ttIterator.next();
            SatStrategy.Condition cond = subject.getCondition(associatedLocationSet, type);

            if (cond.getName() != null) {
                associations += cond.getName() + " -> " + type.getInternalName();
            }
            else {
                associations += cond.toString() + " -> " + type.getInternalName();
            }

            if (ttIterator.hasNext()) {
                associations += "\n";
            }
        }

        it.setText(1, associations);
    }

    @Override
    public void setRedraw(boolean arg0) {
        isRedrawEnabled = arg0;
        super.setRedraw(arg0);

        if (arg0 == true) {
            redraw();
        }
    }

    /**
     * Redraw the complete widget.
     */
    @Override
    public void redraw() {
        super.redraw();

        if (!isRedrawEnabled) {
            return;
        }

        conditionTable.setRedraw(false);

        for (TableItem ti : conditionTable.getItems()) {
            ti.dispose();
        }
        locations.clear();
        currentSelection = null;

        for (NamedSet<Location> loc : lexSortedLocations(subject.getLocationSets())) {
            addLocation(loc);
        }

        conditionTable.setRedraw(true);
    }

    /**
     * @param locations
     *            A set of named locations.
     * @return A lexicographically sorted list containing exactly the locations
     *         given in locations.
     */
    private static List<NamedSet<Location>> lexSortedLocations(
            Collection<NamedSet<Location>> locations) {
        Map<String, NamedSet<Location>> name2loc = new HashMap<>();
        String[] names = new String[locations.size()];
        Iterator<NamedSet<Location>> locIt = locations.iterator();
        for (int i = 0; i < locations.size(); i++) {
            NamedSet<Location> l = locIt.next();
            name2loc.put(l.getDisplayName(), l);
            names[i] = l.getDisplayName();
        }
        java.util.Arrays.sort(names);
        ArrayList<NamedSet<Location>> result = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            result.add(name2loc.get(names[i]));
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private void onDisplayLocationSetEditor() {
        NamedSet<Location> selectedSet = null;
        TableItem ti = getCurrentSelection();
        if (ti != null) {
            selectedSet = (NamedSet<Location>) ti.getData();
            if (selectedSet != null && !selectedSet.hasName()) {
                selectedSet = null;
            }
        }

        for (GenericListener<NamedSet<Location>> listener : displayLocationSetEditorListeners) {
            listener.onFiredEvent(selectedSet);
        }
    }
}
