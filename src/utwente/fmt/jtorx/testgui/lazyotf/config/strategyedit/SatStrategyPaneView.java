package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Sash;

import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UISatStrategy;

/**
 * The SatStrategy editor control.
 */
class SatStrategyPaneView extends Composite {
    /** The main view's selection area. */
    private SatStrategyLocationListView mainSelectionArea;

    /** The main view's editor area. */
    private SatStrategyConditionEditorView mainEditorArea;

    /**
     * Construct a new SatStrategyPaneView instance.
     * 
     * @param parent
     *            The SatStrategy pane's parent widget.
     * @param ctrl
     *            The decision strategy's main controller.
     * @param subject
     *            The decision strategy to be edited.
     */
    public SatStrategyPaneView(Composite parent, StrategyEditorImpl ctrl, UISatStrategy subject) {
        super(parent, SWT.NONE);

        mainSelectionArea = new SatStrategyLocationListView(this, subject, ctrl.getLocations());
        mainEditorArea = new SatStrategyConditionEditorView(this, subject, ctrl.getTestTypes());

        setupVerticalSashLayout(this);
    }

    /**
     * Set up the sash-based layout.
     * 
     * @param theControl
     *            The widget containing the selection area as well as the editor
     *            area.
     */
    private void setupVerticalSashLayout(final Composite theControl) {
        Sash vSash = new Sash(theControl, SWT.BORDER | SWT.HORIZONTAL);
        theControl.setLayout(new FormLayout());

        FormData gSelFD = new FormData();
        gSelFD.left = new FormAttachment(0, 0);
        gSelFD.right = new FormAttachment(100, 0);
        gSelFD.top = new FormAttachment(0, 0);
        gSelFD.bottom = new FormAttachment(vSash, 0);
        mainSelectionArea.setLayoutData(gSelFD);

        FormData gEditFD = new FormData();
        gEditFD.left = new FormAttachment(0, 0);
        gEditFD.right = new FormAttachment(100, 0);
        gEditFD.top = new FormAttachment(vSash, 0);
        gEditFD.bottom = new FormAttachment(100, 0);
        mainEditorArea.setLayoutData(gEditFD);

        final FormData sashFD = new FormData();
        sashFD.left = new FormAttachment(0, 0);
        sashFD.top = new FormAttachment(0, 200);
        sashFD.right = new FormAttachment(100, 0);
        vSash.setLayoutData(sashFD);

        vSash.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                sashFD.top = new FormAttachment(0, ev.y);
                theControl.layout();
            }
        });

        theControl.pack();
    }

    /**
     * @return The location selection view.
     */
    public SatStrategyLocationListView getSelectionView() {
        return mainSelectionArea;
    }

    /**
     * @return The test-type/condition association editor.
     */
    public SatStrategyConditionEditorView getEditorView() {
        return mainEditorArea;
    }
}
