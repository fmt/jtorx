package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import utwente.fmt.jtorx.lazyotf.strategy.DecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategyFactory;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * ViewUtilities class for the decision strategy list sidebar.
 */
class Sidebar extends Composite {
    /** The tree control displaying the decision strategy names. */
    private final Tree sidebarTree;

    /** Association of decision strategies and sidebarTree items. */
    private Map<UIDecisionStrategy, TreeItem> strategies;

    /** The strategy editor's main controller. */
    private final StrategyEditorImpl ctrl;

    /** The tree editor used for decision strategy renaming. */
    private final TreeEditor nameEditor;

    /**
     * The set of listeners getting fired when a rename operation is requested.
     */
    private final Set<GenericListener<String>> onRenameFinishedListeners;

    /** The currently selected decision strategy. */
    private UIDecisionStrategy currentStrategy = null;

    /** The strategy addition menu item. */
    MenuItem addStrategyMenuItem;

    /** The strategy removal menu item. */
    MenuItem deleteStrategyMenuItem;

    /** The strategy rename menu item. */
    MenuItem renameStrategyMenuItem;

    /** The strategy addition submenu. */
    Menu addStrategySubmenu;

    /**
     * A map from the strategy addition submenu items to the respective strategy
     * classes.
     */
    private Map<MenuItem, Class<? extends UIDecisionStrategy>> addSubmenuMap;

    /**
     * Construct a new Sidebar instance as a child of the given parent widget.
     * 
     * @param parent
     *            The sidebar's parent widget.
     * @param ctrl
     *            The decision strategy editor's main controller.
     */
    public Sidebar(Composite parent, StrategyEditorImpl ctrl) {
        super(parent, SWT.BORDER);

        onRenameFinishedListeners = new HashSet<>();
        this.ctrl = ctrl;

        setLayout(new FillLayout());

        Composite groupComposite = new Composite(this, SWT.NONE);
        GridLayout innerLayout = new GridLayout();
        innerLayout.numColumns = 1;
        innerLayout.marginRight = 1;
        innerLayout.marginTop = 1;
        innerLayout.marginBottom = 1;
        innerLayout.marginLeft = 1;
        groupComposite.setLayout(innerLayout);

        Label sidebarCaption = new Label(groupComposite, SWT.NONE);
        sidebarCaption.setText("Decision Strategies");
        GridData sidebarGD = new GridData();

        sidebarCaption.setLayoutData(sidebarGD);

        sidebarTree = new Tree(groupComposite, SWT.NONE);
        sidebarTree.setLayoutData(new GridData(GridData.FILL_BOTH));
        nameEditor = new TreeEditor(sidebarTree);

        strategies = new HashMap<UIDecisionStrategy, TreeItem>();

        setupNameEditor();
        setupContextMenu();

        reset();

        this.pack();
    }

    public void addRenameFinishedListener(GenericListener<String> listener) {
        onRenameFinishedListeners.add(listener);
    }

    public void addDeleteStrategyRequestListener(final GenericListener<UIDecisionStrategy> listener) {
        deleteStrategyMenuItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                listener.onFiredEvent(getSelectedStrategy());
                redraw();
            }
        });
    }

    public void addAddStrategyRequestListener(
            final GenericListener<Class<? extends UIDecisionStrategy>> listener) {
        final Sidebar _this = this;
        for (final MenuItem mi : addStrategySubmenu.getItems()) {
            mi.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent ev) {
                    listener.onFiredEvent(_this.addSubmenuMap.get(mi));
                    redraw();
                }
            });
        }
    }

    public void addStrategySelectionListener(final GenericListener<UIDecisionStrategy> listener) {
        sidebarTree.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                TreeItem ti = (TreeItem) ev.item;
                UIDecisionStrategy it = (UIDecisionStrategy) ti.getData();
                if (it != currentStrategy) {
                    currentStrategy = it;
                    listener.onFiredEvent(it);
                }
            }
        });
    }

    /**
     * Reset the view. Redraws the strategy list.
     */
    public void reset() {
        strategies.clear();
        sidebarTree.removeAll();

        List<UIDecisionStrategy> currentStrategies = ctrl.getModifiedStrategySet();
        for (UIDecisionStrategy strat : currentStrategies) {
            addStrategy(strat);
        }

        if (strategies.containsKey(currentStrategy)) {
            sidebarTree.select(strategies.get(currentStrategy));
        }
        else {
            sidebarTree.deselectAll();
        }
    }

    /**
     * @return The selected decision strategy or null is no strategy was
     *         selected.
     */
    public UIDecisionStrategy getSelectedStrategy() {
        if (sidebarTree.getSelectionCount() > 0) {
            return (UIDecisionStrategy) sidebarTree.getSelection()[0].getData();
        }
        else {
            return null;
        }
    }

    /**
     * Add a new decision strategy to the list of decision strategies.
     * 
     * @param it
     *            The strategy to be added.
     */
    public void addStrategy(UIDecisionStrategy it) {
        if (strategies.containsKey(it)) {
            redrawStrategy(it);
        }
        else {
            TreeItem dsItem = new TreeItem(sidebarTree, SWT.NONE);
            dsItem.setData(it);
            strategies.put(it, dsItem);
            redrawStrategy(it);
        }
    }

    /**
     * Redraw the given decision strategy's entry.
     * 
     * @param it
     *            The decision strategy whose entry is to be redrawn.
     */
    public void redrawStrategy(UIDecisionStrategy it) {
        if (strategies.containsKey(it)) {
            TreeItem matchingTreeItem = strategies.get(it);
            matchingTreeItem.setText(it.getDisplayName());
        }
        else {
            throw new IllegalArgumentException("Tried to update unknown decision strategy.");
        }
    }

    /**
     * Remove a decision strategy from the displayed list of decision
     * strategies.
     * 
     * @param it
     *            The decision strategy to be removed.
     */
    public void removeStrategy(DecisionStrategy it) {
        TreeItem matchingTreeItem = strategies.get(it);
        strategies.remove(it);
        matchingTreeItem.dispose();
    }

    /**
     * Display the strategy renaming text field for the currently selected
     * strategy. If no strategy is currently selected, calling this method has
     * no effect.
     */
    public void displayRenamingEditor() {
        UIDecisionStrategy ds = getSelectedStrategy();
        if (ds != null) {
            displayNameEditor(strategies.get(ds));
        }
    }

    /**
     * Initialize the decision strategy name editor.
     */
    private void setupNameEditor() {
        nameEditor.horizontalAlignment = SWT.LEFT;
        nameEditor.grabHorizontal = true;
        nameEditor.minimumWidth = 100;

        sidebarTree.addListener(SWT.MouseDoubleClick, new Listener() {
            @Override
            public void handleEvent(Event ev) {

                TreeItem[] currentSelection = sidebarTree.getSelection();
                if (currentSelection.length == 1) {
                    TreeItem selectedItem = currentSelection[0];
                    displayNameEditor(selectedItem);
                }

            }
        });

        sidebarTree.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                hideNameEditor();
            }
        });
    }

    private void setupContextMenu() {
        final Sidebar _this = this;
        addSubmenuMap = new HashMap<MenuItem, Class<? extends UIDecisionStrategy>>();

        final Menu contextMenu = new Menu(sidebarTree);
        sidebarTree.setMenu(contextMenu);

        addStrategyMenuItem = new MenuItem(contextMenu, SWT.CASCADE);
        addStrategyMenuItem.setText("Add Strategy");

        addStrategySubmenu = new Menu(addStrategyMenuItem);
        addStrategyMenuItem.setMenu(addStrategySubmenu);
        for (final Class<? extends UIDecisionStrategy> dsClass : UIDecisionStrategyFactory
                .getStrategyTypes()) {
            MenuItem mi = new MenuItem(addStrategySubmenu, SWT.PUSH);
            mi.setText(dsClass.getSimpleName());
            addSubmenuMap.put(mi, dsClass);
        }

        deleteStrategyMenuItem = new MenuItem(contextMenu, SWT.PUSH);
        deleteStrategyMenuItem.setText("Delete Strategy");

        renameStrategyMenuItem = new MenuItem(contextMenu, SWT.PUSH);
        renameStrategyMenuItem.setText("Rename Strategy");
        renameStrategyMenuItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                _this.displayRenamingEditor();
            }
        });

        contextMenu.addMenuListener(new MenuAdapter() {
            @Override
            public void menuShown(MenuEvent arg0) {
                boolean grayOut = _this.sidebarTree.getSelectionCount() == 0;
                _this.renameStrategyMenuItem.setEnabled(!grayOut);
                _this.deleteStrategyMenuItem.setEnabled(!grayOut);
            }
        });
    }

    /**
     * Display the strategy name editor control above the given tree item.
     * 
     * @param item
     *            The tree item above which the editor control should be
     *            displayed.
     */
    private void displayNameEditor(final TreeItem item) {
        if (nameEditor.getEditor() != null) {
            nameEditor.getEditor().dispose();
        }

        Text newEditor = new Text(sidebarTree, SWT.NONE);
        newEditor.setText(item.getText());
        newEditor.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent ev) {
                Text currentText = (Text) nameEditor.getEditor();
                for (GenericListener<String> listener : onRenameFinishedListeners) {
                    listener.onFiredEvent(currentText.getText());
                }
            }

        });
        newEditor.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent arg0) {
            }

            @Override
            public void focusLost(FocusEvent arg0) {
                hideNameEditor();
            }
        });
        newEditor.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetDefaultSelected(SelectionEvent arg0) {
                hideNameEditor();
            }
        });
        newEditor.selectAll();
        newEditor.setFocus();
        nameEditor.setEditor(newEditor, item);
    }

    /**
     * Hide the strategy name editor.
     */
    private void hideNameEditor() {
        if (nameEditor.getEditor() != null) {
            nameEditor.getEditor().dispose();
        }
    }

}
