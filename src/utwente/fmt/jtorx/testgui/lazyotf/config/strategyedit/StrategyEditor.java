package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import java.util.List;

import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;

public interface StrategyEditor {
    /**
     * @return The list of decision strategies resulting from the editing
     *         session.
     */
    List<UIDecisionStrategy> getModifiedStrategySet();

    /**
     * Display the (modal) strategy editor window.
     */
    void open();
}
