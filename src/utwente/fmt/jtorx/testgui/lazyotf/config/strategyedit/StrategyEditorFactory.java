package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import utwente.fmt.jtorx.lazyotf.ExpressionValidator;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.EditorInvocation;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;

/**
 * A factory interface for {@link StrategyEditor}. {@link StrategyEditorImpl}
 * provides an implementation of this interface via <i>getFactory()</i>.
 */
public interface StrategyEditorFactory {
    /**
     * Make a new {@link StrategyEditor} instance.
     * 
     * @param sessionData
     *            The configuration session data. This object does not get
     *            modified by the editor.
     * @param editors
     *            An object allowing the invocation of the other editors.
     * @param conditionValidator
     *            A validator for SatStrategy conditions.
     * @return A new {@link StrategyEditor} instance.
     */
    StrategyEditor makeStrategyEditor(LazyOTFConfigSessionData sessionData,
            EditorInvocation editors,
            ExpressionValidator conditionValidator);
}
