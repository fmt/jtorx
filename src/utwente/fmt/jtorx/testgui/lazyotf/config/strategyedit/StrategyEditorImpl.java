package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.ExpressionValidator;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.strategy.SatStrategy.Condition;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.EditorInvocation;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.NamedSet;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategyVisitor;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIMappingStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UISatStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.CollectionUtilities;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * An editor for decision strategies. This class acts as a top-level controller
 * capable of creating and initializing the corresponding view classes.
 */
public class StrategyEditorImpl implements StrategyEditor {
    /**
     * The "current" list of decision strategies, reflecting the changes made
     * during the editor's run.
     */
    private final List<UIDecisionStrategy> modifiedStrategies;

    /** The editor's main window. */
    private final StrategyEditorWindow wnd;

    /** The editor's "toplevel" view. */
    private StrategyEditorView view;

    /** The configuration session data. */
    private final LazyOTFConfigSessionData sessionData;

    /** The set of assignable test types. */
    private final Set<TestType> assignableTestTypes;

    /** An object allowing the invocation of the various editors. */
    private final EditorInvocation editors;

    /** A validator for conditions. */
    private final ExpressionValidator conditionValidator;

    /**
     * Construct a new StrategyEditor instance.
     * 
     * @param sessionData
     *            The configuration's session data.
     * @param editors
     *            An object allowing the invocation of the various editors.
     * @param conditionValidator
     *            A validator for SatStrategy conditions.
     */
    public StrategyEditorImpl(LazyOTFConfigSessionData sessionData, EditorInvocation editors,
            ExpressionValidator conditionValidator) {
        modifiedStrategies = new ArrayList<UIDecisionStrategy>(sessionData.getStrategies());
        this.sessionData = sessionData;
        this.editors = editors;
        this.conditionValidator = conditionValidator;

        assignableTestTypes = new HashSet<>(sessionData.getTestTypes());
        TestType stratType = null;
        for (TestType tt : assignableTestTypes) {
            if (tt.isDecidedByStrategy()) {
                stratType = tt;
            }
        }
        if (stratType != null) {
            assignableTestTypes.remove(stratType);
        }

        wnd = new StrategyEditorWindow(this);
    }

    /**
     * @return A validator for conditions.
     */
    protected ExpressionValidator getConditionValidator() {
        return conditionValidator;
    }

    /**
     * @return The set of locations known to this editor instance.
     */
    protected Collection<NamedSet<Location>> getLocations() {
        return CollectionUtilities.unmodifiableUnionView(sessionData.getLocationSets(),
                sessionData.getBasisLocationSets());
    }

    /**
     * @return The set of test types known to this editor instance.
     */
    protected Set<TestType> getTestTypes() {
        return assignableTestTypes;

    }

    @Override
    public List<UIDecisionStrategy> getModifiedStrategySet() {
        return modifiedStrategies;
    }

    /**
     * Rename a decision strategy.
     * 
     * @param it
     *            The decision strategy to be renamed.
     * @param newName
     *            The decision strategy's new name.
     */
    protected void onRenameStrategy(UIDecisionStrategy it, String newName) {
        it.setDisplayName(newName);
        wnd.getView().updateStrategy(it);
    }

    /**
     * Delete a decision strategy.
     * 
     * @param it
     *            The decision strategy to be deleted.
     */
    protected void onDeleteStrategy(UIDecisionStrategy it) {
        modifiedStrategies.remove(it);
        wnd.getView().updateStrategyList();
    }

    protected void onSelectStrategy(UIDecisionStrategy it) {
        it.accept(new UIDecisionStrategyVisitor() {
            @Override
            public void visit(UIDecisionStrategy it) {
                view.displayEditor(it);
            }

            @Override
            public void visit(UIMappingStrategy it) {
                installStrategyCtrl(it, view.displayEditor(it));
            }

            @Override
            public void visit(UISatStrategy it) {
                installStrategyCtrl(it, view.displayEditor(it));
            }
        });
    }

    /**
     * Get a controller for the SatStrategy editor view.
     * 
     * @param subject
     *            The decision strategy which is to be modified by the
     *            controller.
     * @param view
     *            The associated view.
     * @return A controller for the SatStrategy editor view.
     */
    protected SatStrategyCtrl installStrategyCtrl(UISatStrategy subject, SatStrategyPaneView view) {
        return new SatStrategyCtrl(subject, sessionData.getGlobalConditions(), this, view);
    }

    /**
     * Get a controller for the MappingStrategy editor view.
     * 
     * @param subject
     *            The decision strategy which is to be modified by the
     *            controller.
     * @param view
     *            The associated view.
     * @return A controller for the MappingStrategy editor view.
     */
    protected MappingStrategyCtrl installStrategyCtrl(UIMappingStrategy subject,
            MappingStrategyPaneView view) {
        return new MappingStrategyCtrl(subject, view, sessionData, editors);
    }

    /**
     * Make a new decision strategy and insert it into the list of modified
     * decision strategies. This method assigns the name "New Strategy ($TYPE)"
     * to the new strategy where $TYPE is the strategy's type name.
     * 
     * @param type
     *            The new decision strategy's type.
     */
    protected void onMakeNewStrategy(Class<? extends UIDecisionStrategy> type) {
        try {
            UIDecisionStrategy newDS = type.newInstance();
            modifiedStrategies.add(newDS);
            newDS.setDisplayName("New Strategy (" + type.getSimpleName() + ")");
            wnd.getView().updateStrategyList();
        }
        catch (InstantiationException e) {
            throw new IllegalStateException(e.toString());
        }
        catch (IllegalAccessException e) {
            throw new IllegalStateException(e.toString());
        }
    }

    /**
     * @return A list of conditions marked "global".
     */
    protected Set<Condition> getGlobalConditions() {
        return sessionData.getGlobalConditions();
    }

    protected void setView(StrategyEditorView view) {
        this.view = view;

        final StrategyEditorView _view = view;

        this.view.addDeleteStrategyRequestListener(new GenericListener<UIDecisionStrategy>() {
            @Override
            public void onFiredEvent(UIDecisionStrategy strategy) {
                onDeleteStrategy(strategy);
            }
        });

        this.view
                .addMakeStrategyRequestListener(new GenericListener<Class<? extends UIDecisionStrategy>>() {
                    @Override
                    public void onFiredEvent(Class<? extends UIDecisionStrategy> classType) {
                        onMakeNewStrategy(classType);
                    }
                });

        this.view.addRenameStrategyRequestListener(new GenericListener<String>() {
            @Override
            public void onFiredEvent(String newName) {
                onRenameStrategy(_view.getSelectedStrategy(), newName);
            }
        });

        this.view.addStrategySelectionListener(new GenericListener<UIDecisionStrategy>() {
            @Override
            public void onFiredEvent(UIDecisionStrategy strategy) {
                onSelectStrategy(strategy);
            }
        });
    }

    /**
     * @return The current configuration session data object.
     */
    protected LazyOTFConfigSessionData getSessionData() {
        return sessionData;
    }

    /**
     * @return The editor invocation object.
     */
    protected EditorInvocation getEditorInvocationObj() {
        return editors;
    }

    /**
     * Make a factory object for {@link StrategyEditorImpl}.
     * 
     * @return
     */
    public static StrategyEditorFactory getFactory() {
        return new StrategyEditorFactory() {
            @Override
            public StrategyEditorImpl makeStrategyEditor(
                    LazyOTFConfigSessionData sessionData,
                    EditorInvocation editors,
                    ExpressionValidator conditionValidator) {
                return new StrategyEditorImpl(sessionData, editors, conditionValidator);
            }
        };
    }

    @Override
    public void open() {
        wnd.display();
    }
}
