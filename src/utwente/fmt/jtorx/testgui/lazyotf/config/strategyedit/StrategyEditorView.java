package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIDecisionStrategyFactory;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UIMappingStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.UISatStrategy;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

/**
 * Main view class for the decision strategy editor.
 */
class StrategyEditorView extends Composite {
    /** The editor's main controller. */
    private final StrategyEditorImpl ctrl;

    /** The window's tool bar. */
    private ToolBar toolbar;

    /** The sidebar's composite. */
    private Sidebar sidebar;

    /** The main view's enclosing composite. */
    private Composite mainAreaEnclosure;

    /** The main view's composite. */
    private Composite mainArea;

    /** The main view's actual editor control. */
    private Composite mainAreaEditor;

    /** The strategy name and type label. */
    private Label strategyNameAndTypeLabel;

    private GenericListener<Class<? extends UIDecisionStrategy>> makeStrategyListener;
    private GenericListener<UIDecisionStrategy> deleteStrategyListener;

    public void addStrategySelectionListener(GenericListener<UIDecisionStrategy> listener) {
        sidebar.addStrategySelectionListener(listener);
    }

    public void addRenameStrategyRequestListener(GenericListener<String> listener) {
        sidebar.addRenameFinishedListener(listener);
    }

    public void addMakeStrategyRequestListener(
            GenericListener<Class<? extends UIDecisionStrategy>> listener) {
        makeStrategyListener = listener;
        sidebar.addAddStrategyRequestListener(listener);
    }

    public void addDeleteStrategyRequestListener(GenericListener<UIDecisionStrategy> listener) {
        deleteStrategyListener = listener;
        sidebar.addDeleteStrategyRequestListener(listener);
    }

    /**
     * Construct a new StrategyEditorView instance. The strategy editor behaves
     * like a widget drawn into the parent widget.
     * 
     * @param parentWidget
     *            The strategy editor view's parent widget.
     * @param ctrl
     *            The strategy editor's main controller.
     */
    public StrategyEditorView(Composite parentWidget, StrategyEditorImpl ctrl) {
        super(parentWidget, SWT.NONE);

        this.ctrl = ctrl;

        /* Outermost layout (between toolbar and trunk */
        GridLayout outerLayout = new GridLayout(); // gets reused, see (1)
        outerLayout.numColumns = 1;
        setLayout(outerLayout);
        GridData editorLayoutData = new GridData(GridData.FILL_BOTH); // gets
                                                                      // reused,
                                                                      // see (2)

        /* Horizontally splitted structure */
        setupToolbar();
        Composite lowerPart = new Composite(this, SWT.NONE);
        lowerPart.setLayoutData(editorLayoutData);
        sidebar = new Sidebar(lowerPart, ctrl);

        mainAreaEnclosure = new Composite(lowerPart, SWT.NONE);
        mainAreaEnclosure.setLayout(outerLayout); // (1): layout reused from
                                                  // above

        mainArea = new Composite(mainAreaEnclosure, SWT.NONE);
        mainArea.setLayoutData(editorLayoutData); // (2): layout data reused
                                                  // from above
        mainArea.setLayout(new FillLayout());

        mainAreaEditor = new EmptyPaneView(mainArea, getDefaultEditorMessage());

        strategyNameAndTypeLabel = new Label(mainAreaEnclosure, SWT.NONE);
        strategyNameAndTypeLabel.setText("");
        GridData labelGridData = new GridData(GridData.FILL_HORIZONTAL);
        strategyNameAndTypeLabel.setLayoutData(labelGridData);

        setupHorizontalSashLayout(lowerPart);

        this.pack();
    }

    /**
     * @return The message displayed in the editing pane when no strategy is
     *         currently being edited. (This happens either when no strategy is
     *         selected or when the strategy editor does not implement an
     *         editing control for the currently selected message.
     */
    private String getDefaultEditorMessage() {
        String emptyPaneMsg;
        if (ctrl.getModifiedStrategySet().size() > 0) {
            emptyPaneMsg = "Select a decision strategy to edit it.";
        }
        else {
            emptyPaneMsg = "Click on \"New Strategy\" to set up new decision strategies.";
        }
        return emptyPaneMsg;
    }

    /**
     * Inform all relevant views that the strategy list was changed.
     */
    public void updateStrategyList() {
        sidebar.reset();
        updateStrategyLabel();
        updateToolbar();
    }

    /**
     * Inform all relevant views that the given strategy was changed.
     * 
     * @param it
     *            The changed decision strategy.
     */
    public void updateStrategy(UIDecisionStrategy it) {
        sidebar.redrawStrategy(it);
        updateStrategyLabel();
    }

    /**
     * Set up the sash control between the decision strategy list and the editor
     * part.
     * 
     * @param theControl
     *            The parent widget of the list and the editor part.
     */
    private void setupHorizontalSashLayout(final Composite theControl) {
        Sash vSash = new Sash(theControl, SWT.BORDER | SWT.VERTICAL);
        theControl.setLayout(new FormLayout());

        FormData gSelFD = new FormData();
        gSelFD.left = new FormAttachment(0, 0);
        gSelFD.right = new FormAttachment(vSash, 0);
        gSelFD.top = new FormAttachment(0, 0);
        gSelFD.bottom = new FormAttachment(100, 0);
        sidebar.setLayoutData(gSelFD);

        FormData gEditFD = new FormData();
        gEditFD.left = new FormAttachment(vSash, 0);
        gEditFD.right = new FormAttachment(100, 0);
        gEditFD.top = new FormAttachment(0, 0);
        gEditFD.bottom = new FormAttachment(100, 0);
        mainAreaEnclosure.setLayoutData(gEditFD);

        final FormData sashFD = new FormData();
        sashFD.left = new FormAttachment(50, -100);
        sashFD.top = new FormAttachment(0, 0);
        sashFD.bottom = new FormAttachment(100, 0);
        vSash.setLayoutData(sashFD);

        vSash.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                sashFD.left = new FormAttachment(0, ev.x);
                theControl.layout();
            }
        });

        theControl.pack();
    }

    /** The "New" toolbar item */
    private ToolItem tbNewItem;

    /** The "Delete" toolbar item */
    private ToolItem tbDeleteItem;

    /** The "Rename" toolbar item */
    private ToolItem tbRenameItem;

    /**
     * Set up the view's toolbar and the respective listeners..
     */
    private void setupToolbar() {
        toolbar = new ToolBar(this, SWT.FLAT | SWT.WRAP | SWT.RIGHT);
        tbNewItem = new ToolItem(toolbar, SWT.DROP_DOWN);
        tbNewItem.setText("Add Strategy");
        tbDeleteItem = new ToolItem(toolbar, SWT.PUSH);
        tbDeleteItem.setText("Delete");
        tbRenameItem = new ToolItem(toolbar, SWT.PUSH);
        tbRenameItem.setText("Rename");

        final StrategyEditorView _this = this;

        tbDeleteItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                UIDecisionStrategy ds = _this.sidebar.getSelectedStrategy();
                if (ds != null) {
                    // _this.ctrl.onDeleteStrategy(ds);
                    _this.deleteStrategyListener.onFiredEvent(ds);
                }
            }
        });

        tbRenameItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                UIDecisionStrategy ds = _this.sidebar.getSelectedStrategy();
                if (ds != null) {
                    _this.sidebar.displayRenamingEditor();
                }
            }
        });

        setupToolbarDropdowns();
        updateToolbar();
    }

    /**
     * Set up the drop-down menus associated with toolbar buttons.
     */
    private void setupToolbarDropdowns() {
        final Menu newItemDropdown = new Menu(tbNewItem.getParent().getShell());
        final StrategyEditorView _this = this;
        for (final Class<? extends UIDecisionStrategy> dsClass : UIDecisionStrategyFactory
                .getStrategyTypes()) {
            MenuItem dsMenuItem = new MenuItem(newItemDropdown, SWT.PUSH);
            dsMenuItem.setText(dsClass.getSimpleName());
            dsMenuItem.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent ev) {
                    // _this.ctrl.onMakeNewStrategy(dsClass);
                    _this.makeStrategyListener.onFiredEvent(dsClass);
                }
            });
        }

        tbNewItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                Rectangle tbNewRect = tbNewItem.getBounds();
                Point pt = tbNewItem.getParent().toDisplay(new Point(tbNewRect.x, tbNewRect.y));
                newItemDropdown.setLocation(pt.x, pt.y + tbNewRect.height);
                newItemDropdown.setVisible(true);
            }
        });
    }

    /**
     * Update the toolbar, graying out buttons when necessary.
     */
    private void updateToolbar() {
        boolean enableContextButtons = (sidebar != null && sidebar.getSelectedStrategy() != null);
        tbDeleteItem.setEnabled(enableContextButtons);
        tbRenameItem.setEnabled(enableContextButtons);
    }

    /**
     * A factory class for StrategyEditorView.
     */
    public static class Factory {
        /**
         * Make a new StrategyEditorView instance.
         * 
         * @param parent
         *            The strategy editor view's parent shell.
         * @param ctrl
         *            The strategy editor's main controller.
         * @return A new StrategyEditorView instance.
         */
        public StrategyEditorView makeInstance(Shell parent, StrategyEditorImpl ctrl) {
            return new StrategyEditorView(parent, ctrl);
        }
    }

    public MappingStrategyPaneView displayEditor(UIMappingStrategy it) {
        displayEditorPre();
        MappingStrategyPaneView result = new MappingStrategyPaneView(mainArea,
                ctrl.getSessionData(), it);
        mainAreaEditor = result;
        displayEditorPost();
        return result;
    }

    public SatStrategyPaneView displayEditor(UISatStrategy it) {
        displayEditorPre();
        SatStrategyPaneView result = new SatStrategyPaneView(mainArea, ctrl, it);
        mainAreaEditor = result;
        displayEditorPost();
        return result;
    }

    public EmptyPaneView displayEditor(UIDecisionStrategy it) {
        displayEditorPre();
        EmptyPaneView result = new EmptyPaneView(mainArea,
                "No editor implemented yet for this type of decision strategies.");
        mainAreaEditor = result;
        displayEditorPost();
        return result;
    }

    private void displayEditorPre() {
        if (mainAreaEditor != null) {
            mainAreaEditor.dispose();
        }
    }

    private void displayEditorPost() {
        mainArea.layout();
        updateToolbar();
        updateStrategyLabel();
    }

    /**
     * Update the label containing information about the currently selected
     * decision strategy.
     */
    private void updateStrategyLabel() {
        if (sidebar != null) {
            UIDecisionStrategy it = sidebar.getSelectedStrategy();

            if (it != null) {
                strategyNameAndTypeLabel.setText(it.getDisplayName() + " ("
                        + it.getClass().getSimpleName() + " instance)");
            }
            else {
                strategyNameAndTypeLabel.setText("");
            }
        }
    }

    public UIDecisionStrategy getSelectedStrategy() {
        return sidebar.getSelectedStrategy();
    }
}
