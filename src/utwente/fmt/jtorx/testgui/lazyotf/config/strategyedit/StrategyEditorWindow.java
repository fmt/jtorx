package utwente.fmt.jtorx.testgui.lazyotf.config.strategyedit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

/**
 * A class responsible for setting up and managing the decision strategy editor
 * window.
 */
class StrategyEditorWindow {
    /** The decision strategy editor's main controller. */
    private final StrategyEditorImpl ctrl;

    /** The decision strategy editor view's factory. */
    private final StrategyEditorView.Factory viewFactory;

    /** The decision strategy editor view currently used. */
    private StrategyEditorView view;

    /**
     * Construct a new StrategyEditorWindow instance.
     * 
     * @param ctrl
     *            The decision strategy editor's main controller.
     */
    public StrategyEditorWindow(StrategyEditorImpl ctrl) {
        this.ctrl = ctrl;
        viewFactory = new StrategyEditorView.Factory();
        view = null;
    }

    /**
     * @return The decision strategy editor view associated with the window.
     */
    public StrategyEditorView getView() {
        return view;
    }

    /**
     * Modally display the decision strategy editor window.
     */
    public void display() {
        final Shell parentShell = Display.getCurrent().getActiveShell();
        final Shell ourShell = new Shell(Display.getCurrent(), SWT.APPLICATION_MODAL
                | SWT.DIALOG_TRIM | SWT.RESIZE);
        ourShell.setLayout(new FillLayout());
        ourShell.setMinimumSize(640, 480);
        ourShell.setText("Decision Strategy Editor");
        view = viewFactory.makeInstance(ourShell, ctrl);
        ctrl.setView(view);
        ourShell.addListener(SWT.Close, new Listener() {
            @Override
            public void handleEvent(Event ev) {
                parentShell.setEnabled(true);
            }
        });
        ourShell.pack();
        ourShell.open();

        while (!ourShell.isDisposed()) {
            if (!Display.getCurrent().readAndDispatch()) {
                Display.getCurrent().sleep();
            }
        }
    }
}
