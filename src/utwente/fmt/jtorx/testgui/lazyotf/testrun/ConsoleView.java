package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import utwente.fmt.jtorx.lazyotf.remoteimpl.Message;

/**
 * A widget displaying the LazyOTF console. Listeners for events such as user
 * input can be registered with the {@link ConsoleView.EventManager} obtainable
 * via <i>getEventManager()</i>, while LazyOTF communication can be displayed
 * using the <i>displayMessage()</i> method.
 */
public class ConsoleView extends Composite {
    /** Return key code. */
    private static final int RETURN_KEY_CODE = 13;

    /**
     * The tree control displaying the interactions between JTorX and the
     * LazyOTF instance.
     */
    private final Tree output;

    /** The class logger. */
    private static final Logger LOGGER = Logger.getLogger(ConsoleView.class.getName());

    /** The input text control. */
    private final Text input;

    /** The "Send" button. */
    private final Button sendButton;

    /** The "PrettyPrint" button. */
    private final Button prettyPrintButton;

    /** This instance's observable facility. */
    private final EventManager eventMgr;

    /** Controls pretty-printing of messages. */
    private boolean doPrettyPrintMessages = false;

    /**
     * Set up the TraversalVizConsoleView instance.
     * 
     * @param parent
     *            The view's parent control
     */
    public ConsoleView(Composite parent) {
        super(parent, SWT.NONE);

        /* Set up the event observer */
        eventMgr = new EventManager();

        /* Set up this control's layout */
        GridLayout gl = new GridLayout();
        gl.marginHeight = 3;
        gl.marginWidth = 3;
        gl.numColumns = 4;
        setLayout(gl);

        /* Set up the output control */
        final GridData inpGD = new GridData(GridData.FILL_BOTH);
        inpGD.horizontalSpan = 4;
        output = new Tree(this, SWT.BORDER);
        output.setLayoutData(inpGD);

        /* Bottom line */
        Label cmdLabel = new Label(this, SWT.NONE);
        cmdLabel.setText("Command:");

        /* Set up the input control */
        GridData outpGD = new GridData(GridData.FILL_HORIZONTAL);
        outpGD.horizontalSpan = 1;
        input = new Text(this, SWT.NONE);
        input.setLayoutData(outpGD);

        /* Set up buttons */
        sendButton = new Button(this, SWT.PUSH);
        sendButton.setText("Send");
        sendButton.setToolTipText("Send the message to the LazyOTF backend.");

        prettyPrintButton = new Button(this, SWT.CHECK);
        prettyPrintButton.setText("PrettyPrint");
        prettyPrintButton.setToolTipText("Pretty-print the messages.");

        setupButtonListeners();

        /*
         * Listen for Return key presses, issuing LazyOTF commands accordingly
         */
        setupInputListener();

        this.pack();
        this.layout();
        parent.layout();
    }

    /**
     * Notifies the observers that the user requests to send a command to the
     * LazyOTF backend.
     */
    private void onSendRequested() {
        eventMgr.onCommandSumbitted(input.getText());
        input.setText("");
    }

    /**
     * Attach a listener to the input control listening for the user pressing
     * the Return key. Upon detecting such an event, issue an according LazyOTF
     * command and clear the input field.
     */
    private void setupInputListener() {
        input.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                switch (arg0.keyCode) {
                case RETURN_KEY_CODE:
                    // Return key was pressed
                    onSendRequested();
                    break;
                default:
                    break;
                }
            }
        });
    }

    /**
     * Sets up the listeners for the buttons contained in this view.
     */
    private void setupButtonListeners() {
        sendButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                onSendRequested();
            }
        });

        prettyPrintButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                doPrettyPrintMessages = prettyPrintButton.getSelection();
                redrawMessages();
            }
        });
    }

    /**
     * Redraws the messages displayed in the console.
     */
    public void redrawMessages() {
        TreeItem[] treeItems = output.getItems();
        for (TreeItem ti : treeItems) {
            Message tiMessage = (Message) ti.getData("message");
            if (tiMessage != null) {
                setupConsoleItem(ti, tiMessage);
            }
        }
    }

    /**
     * Sets up a console item for a message.
     * 
     * @param it
     *            The TreeItem displaying the message.
     * @param msg
     *            The message to be displayed.
     */
    private void setupConsoleItem(TreeItem it, Message msg) {
        String msgString;
        if (doPrettyPrintMessages) {
            msgString = msg.toPrettyString();
        }
        else {
            msgString = msg.toString();
        }
        if (msgString.endsWith("\n")) {
            /* Cut off trailing newline characters */
            msgString = msgString.substring(0, msgString.length() - 1);
        }

        /* Color the message */
        Display curDsp = Display.getCurrent();
        if (msg.getKind() == Message.Kind.RECEIVED) {

            if (msg.isErrorMessage()) {
                it.setForeground(curDsp.getSystemColor(SWT.COLOR_DARK_RED));
            }
            else {
                it.setForeground(curDsp.getSystemColor(SWT.COLOR_DARK_GREEN));
            }
        }
        else {
            it.setForeground(curDsp.getSystemColor(SWT.COLOR_BLACK));
        }
        it.setText(msgString);

        /*
         * Keep the message for future reference, i.e. sorting/filtering
         * mechanisms
         */
        it.setData("message", msg);
    }

    /**
     * Display a LazyOTF message in the console.
     * 
     * @param msg
     *            The received or sent LazyOTF message.
     */
    public synchronized void displayMessage(Message msg) {
        LOGGER.log(Level.FINE, "Displaying message no. "
                + msg.getSequenceNumber());
        if (output.getItemCount() == msg.getSequenceNumber()) {
            LOGGER.log(Level.FINE, "Directly appending message no. "
                    + msg.getSequenceNumber());
            TreeItem msgTreeItem = new TreeItem(output, SWT.NONE);
            setupConsoleItem(msgTreeItem, msg);
            output.setSelection(msgTreeItem);
        }
        else if (output.getItemCount() > msg.getSequenceNumber()) {
            LOGGER.log(Level.FINE, "Directly inserting message no. "
                    + msg.getSequenceNumber() + " since there are "
                    + output.getItemCount() + " message items present.");
            setupConsoleItem(output.getItem(msg.getSequenceNumber()), msg);
        }
        else {
            /*
             * Messages between last one and msg are currently missing, draw
             * dummies
             */
            int dummies = msg.getSequenceNumber() - output.getItemCount();
            LOGGER.log(Level.FINE, "Inserting " + dummies
                    + " dummy items since there are only "
                    + output.getItemCount() + " items present.");
            int base = output.getItemCount();
            for (int i = 0; i < dummies; i++) {
                TreeItem dummyItem = new TreeItem(output, SWT.NONE);
                dummyItem.setText("Message no. " + (base + i)
                        + " remains to be drawn");
                Display curDsp = Display.getCurrent();
                Color magenta = curDsp.getSystemColor(SWT.COLOR_DARK_MAGENTA);
                dummyItem.setForeground(magenta);
            }
            TreeItem msgTreeItem = new TreeItem(output, SWT.NONE);
            setupConsoleItem(msgTreeItem, msg);
            output.setSelection(msgTreeItem);
        }
    }

    /**
     * Get this instance's EventManager object. Attach observers to this object
     * in order to observe this instance.
     * 
     * @return The EventManager object associated with this instance.
     */
    public Observable getEventManager() {
        return eventMgr;
    }

    /**
     * The EventManager class for TraversalVizConsoleView.
     * 
     */
    public class EventManager extends Observable {
        /**
         * Call observers for a command submission.
         * 
         * @param cmd
         *            The command requested to be sent to the LazyOTF backend.
         */
        public void onCommandSumbitted(String cmd) {
            setChanged();
            notifyObservers(cmd);
        }

        /**
         * Get the instance of TraversalVizConsoleView which generates the
         * events handled by this EventManager instance.
         * 
         * @return A TraversalVizView instance.
         */
        public ConsoleView getOwner() {
            return ConsoleView.this;
        }
    }

    /**
     * A factory class for {@link ConsoleView}.
     */
    public static class Factory {
        /**
         * Makes a new {@link ConsoleView} instance.
         * 
         * @param parent
         *            The view's parent widget.
         * @return The newly created {@link ConsoleView} instance.
         */
        public ConsoleView makeInstance(Composite parent) {
            return new ConsoleView(parent);
        }
    }
}
