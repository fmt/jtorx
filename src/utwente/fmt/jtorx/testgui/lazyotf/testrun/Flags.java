package utwente.fmt.jtorx.testgui.lazyotf.testrun;

/**
 * A class representing the flags visible in the trace.
 */
public class Flags {
    /** Flag constants. */
    private enum Flag {
        /** The test goal flag. */
        TESTGOAL_HIT("T", "T = A Test goal was reached."),

        /** The inducing state flag. */
        INDUCING_HIT("I", "I = An inducing state was reached."),

        /** The first-in-test-tree flag. */
        FIRST_IN_TT("F", "F = First transition in the respective test tree."),

        /** The last-in-test-tree flag. */
        LAST_IN_TT("L", "L = Last transition in the respective test tree."),
        
        /** The cycle detection flag. */
        HAS_DETECTED_A_CYCLE("C", "C = A test execution cycle has been detected.");

        /** The flag's string representation. */
        private final String flagAbbrev;

        /** The flag's long string representation. */
        private final String longFlag;

        private Flag(String flagAbbrev, String longFlag) {
            this.flagAbbrev = flagAbbrev;
            this.longFlag = longFlag;
        }

        public String toLongString() {
            return this.longFlag;
        }

        @Override
        public String toString() {
            return this.flagAbbrev;
        }
    }

    /** <tt>true</tt> iff a test goal was hit by the step. */
    private final boolean isTestGoalHit;

    /** <tt>true</tt> iff an inducing state was hit by the step. */
    private final boolean isInducingStateHit;

    /** <tt>true</tt> iff the transition was the first in the test tree. */
    private final boolean isFirstTransitionInTestTree;

    /** <tt>true</tt> iff the transition was the last in the test tree. */
    private final boolean isLastTransitionInTestTree;
    
    /** <tt>true</tt> iff a test execution cycle has been detected at the transition. */
    private final boolean isStepWithCycleWarning;

    /**
     * Constructs a new {@link Flags} instance, extracting the flag info from
     * the given trace item.
     * 
     * @param tti
     *            A {@link TestTraceItem}.
     */
    public Flags(TestTraceItem tti) {
        isFirstTransitionInTestTree = tti.isFirstTransitionInTestTree();
        isInducingStateHit = tti.isTestInducingStateReaching();
        isLastTransitionInTestTree = tti.isLastTransitionInTestTree();
        isTestGoalHit = tti.isTestGoalReaching();
        isStepWithCycleWarning = tti.isStepWithCycleWarning();
    }

    /**
     * Constructs a new {@link Flags} instance, using explicitly given flags.
     * 
     * @param isTestGoalHit
     *            Set to {@link true} if the T flag should be set.
     * @param isInducingStateHit
     *            Set to {@link true} if the I flag should be set.
     * @param isFirstTransitionInTestTree
     *            Set to {@link true} if the F flag should be set.
     * @param isLastTransitionInTestTree
     *            Set to {@link true} if the L flag should be set.
     */
    public Flags(boolean isTestGoalHit, boolean isInducingStateHit,
            boolean isFirstTransitionInTestTree, boolean isLastTransitionInTestTree,
            boolean isStepWithCycleWarning) {
        this.isTestGoalHit = isTestGoalHit;
        this.isInducingStateHit = isInducingStateHit;
        this.isFirstTransitionInTestTree = isFirstTransitionInTestTree;
        this.isLastTransitionInTestTree = isLastTransitionInTestTree;
        this.isStepWithCycleWarning = isStepWithCycleWarning;
    }

    /**
     * Produces a concatenated string representation of the flags.
     * 
     * @param abbreviate
     *            If {@link true}, only abbreviated flags are returned.
     *            Otherwise, the flags are explained (one line for each flag).
     * @return The described string representation of the flags.
     */
    public String toString(boolean abbreviate) {
        String result = "";
        if (isTestGoalHit) {
            result += abbreviate ? Flag.TESTGOAL_HIT.toString() : Flag.TESTGOAL_HIT.toLongString()
                    + "\n";
        }
        if (isInducingStateHit) {
            result += abbreviate ? Flag.INDUCING_HIT.toString() : Flag.INDUCING_HIT.toLongString()
                    + "\n";
        }
        if (isFirstTransitionInTestTree) {
            result += abbreviate ? Flag.FIRST_IN_TT.toString() : Flag.FIRST_IN_TT.toLongString()
                    + "\n";
        }
        if (isLastTransitionInTestTree) {
            result += abbreviate ? Flag.LAST_IN_TT.toString() : Flag.LAST_IN_TT.toLongString()
                    + "\n";
        }
        if (isStepWithCycleWarning) {
        	result += abbreviate ? Flag.HAS_DETECTED_A_CYCLE.toString() : Flag.HAS_DETECTED_A_CYCLE.toLongString()
        			+ "\n";
        }

        if(result.endsWith("\n")) {
            result = result.substring(0, result.length()-1);
        }
 
        return result;
    }

    @Override
    public String toString() {
        return toString(true);
    }
}
