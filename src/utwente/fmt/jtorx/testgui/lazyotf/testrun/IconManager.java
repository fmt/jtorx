package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

/**
 * The Icon Manager allows easy access to the various icons images used
 * throughout this of the GUI, necessitated by the lack of garbage collection
 * within SWT.
 */
final class IconManager {
    /** A black flag icon. */
    public final Image flagBlack;

    /** A green flag icon. */
    public final Image flagGreen;

    /** A blue flag icon. */
    public final Image flagBlue;

    /** A yellow flag icon. */
    public final Image flagYellow;

    /** A star icon. */
    public final Image star;

    /** Singleton {@link IconManager} instance. */
    private static IconManager instance = new IconManager();

    /**
     * Constructs a new {@link IconManager} instance.
     */
    private IconManager() {
        Display curDsp = Display.getCurrent();
        Class<? extends IconManager> cl = this.getClass();
        flagGreen = new Image(curDsp, cl.getResourceAsStream("flag-green.png"));
        flagBlack = new Image(curDsp, cl.getResourceAsStream("flag-black.png"));
        flagBlue = new Image(curDsp, cl.getResourceAsStream("flag-blue.png"));
        flagYellow = new Image(curDsp, cl.getResourceAsStream("flag-yellow.png"));
        star = new Image(curDsp, cl.getResourceAsStream("star.png"));
    }

    /**
     * @return This singleton's instance
     */
    private static IconManager getInstance() {
        return instance;
    }

    /**
     * @return A 16x16 green flag image
     */
    public static Image getGreenFlag() {
        return IconManager.getInstance().flagGreen;
    }

    /**
     * @return A 16x16 yellow flag image
     */
    public static Image getYellowFlag() {
        return IconManager.getInstance().flagYellow;
    }

    /**
     * @return A 16x16 black flag image
     */
    public static Image getBlackFlag() {
        return IconManager.getInstance().flagBlack;
    }

    /**
     * @return A 16x16 blue flag image
     */
    public static Image getBlueFlag() {
        return IconManager.getInstance().flagBlue;
    }

    /**
     * @return A 16x16 star image
     */
    public static Image getStar() {
        return IconManager.getInstance().star;
    }
}
