package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.ExtendedInteractionResult;

/**
 * The main interface for the LazyOTF introspection window. An implementation of
 * this interface is provided by {@link LazyOTFIntrospectionImpl}.
 */
public interface LazyOTFIntrospection {
    /**
     * Register a STS stimulation or observation event with the LazyOTF
     * introspection.
     * 
     * @param res
     *            The LazyOTF-enabled JTorX driver's
     *            {@link ExtendedInteractionResult} instance for the current
     *            test step.
     */
    void registerInteractionResult(ExtendedInteractionResult res);

    /**
     * Initialize the LazyOTF introspection window.
     */
    void init();

    /**
     * Destroy the LazyOTF introspection window.
     */
    void finish();
}
