package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.ExtendedInteractionResult;

/**
 * The "main" class for the LazyOTF introspection window. To make a new window,
 * it suffices to call <i>makeInstance</i> and <i>init</i>. <i>makeInstance</i>
 * takes care of creating the window's controller and top-level view objects as
 * well as of creating a new SWT window for the view.
 */
public class LazyOTFIntrospectionImpl implements LazyOTFIntrospection {

    /** The window's main controller. */
    private final TraversalViz controller;

    /**
     * Constructs a new {@link LazyOTFIntrospectionImpl} instance. This
     * constructor is private; Instead, you should use the <i>makeInstance</i>
     * factory method to create new instances.
     * 
     * @param controller
     *            The instance's controller.
     */
    private LazyOTFIntrospectionImpl(TraversalViz controller) {
        this.controller = controller;
    }

    @Override
    public void registerInteractionResult(ExtendedInteractionResult res) {
        controller.registerInteractionResult(res);
    }

    @Override
    public void init() {
        controller.init();
    }

    @Override
    public void finish() {
        controller.finish();
    }

    /**
     * Makes a new {@link LazyOTFIntrospection} instance, creates the LazyOTF
     * introspection SWT window and sets up both controller and view objects.
     * 
     * @param lazyOTFInstance
     *            The test run's {@link LazyOTF} instance.
     * @param drv
     *            The test run's LazyOTF-aware driver.
     * @param testGuiIf
     *            An accessor to various aspects of the JTorX GUI.
     * @param torxIfExt
     *            An accessor to various aspects of the test.
     * @return A new {@link LazyOTFIntrospection} instance.
     */
    public static LazyOTFIntrospection makeInstance(LazyOTF lazyOTFInstance,
            LazyOTFAwareSimOnlineTestingDriver drv,
            TestguiAccessor testGuiIf,
            TorXInterfaceExtensions torxIfExt) {

        final TraversalViz ctrl = new TraversalViz(lazyOTFInstance,
                drv, testGuiIf, torxIfExt);

        Runnable vizInit = new Runnable() {
            @Override
            public void run() {
                Display disp = Display.getDefault();
                Shell sh = new Shell(disp);
                TestExecutionDataAccessor tada = ctrl.getTestExecutionDataAccessor();
                TraversalVizView view = new TraversalVizView(sh, tada);
                ctrl.setView(view);
                sh.setLayout(new FillLayout());
                sh.setSize(512, 320);
                sh.setText("LazyOTF Introspection");
                sh.layout();
                sh.open();
            }
        };

        Display.getDefault().syncExec(vizInit);

        return new LazyOTFIntrospectionImpl(ctrl);
    }
}
