package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities;

/**
 * <p>
 * A widget displaying test type group states. It consists mainly of a
 * {@link Tree} widget having one column for group names and another for group
 * states. Inactive test type groups are drawn with a green font.
 * </p>
 * 
 * <p>
 * The widget loads name and state data from {@link TestTypeGroup} instances
 * itself. However, it does not detect changes to those instances, necessitating
 * according calls to redraw().
 * </p>
 */
class TTGStateView extends Composite {
    /** The string used to describe active test objectives. */
    private static final String TTG_STATE_ACTIVE = "Active";

    /** The string used to describe reached test objectives. */
    private static final String TTG_STATE_FINISHED = "Reached Test Goal";

    /** The string used to represent anonymous test type groups. */
    private static final String ANONYMOUS_TTG_NAME = "[Anonymous]";

    /*
     * These constants are used to address data within the SWT {@link Table}
     * control. The addressed objects are of type {@link Boolean}.
     */
    /** {@link Tree getData(String)} key to the sort order of column 0. */
    private static final String SLOT_SORT0ASCENDING = "sort0asc";

    /** {@link Tree getData(String)} key to the sort order of column 1. */
    private static final String SLOT_SORT1ASCENDING = "sort1asc";

    /**
     * An enum describing the current sorting column.
     */
    private enum SortOrder {
        /** Sort by name. */
        BY_NAME(0),

        /** Sort by state. */
        BY_STATE(1);

        private final int col;

        private SortOrder(int col) {
            this.col = col;
        }

        /**
         * @return The current sorted column's index.
         */
        public int getColumn() {
            return col;
        }
    }

    /** The current sorting column. */
    private SortOrder currentSortOrder;

    /** The main SWT {@link Table} control. */
    private final Table ttgStateTable;

    /**
     * A map associating test type groups with their respective
     * {@link TableItem}.
     */
    private final Map<TestTypeGroup, TableItem> ttg2TableItems;

    /**
     * Constructs a new {@link TTGStateView} instance.
     * 
     * @param parent
     *            The widget's parent widget.
     */
    public TTGStateView(Composite parent) {
        super(parent, SWT.NONE);

        setLayout(new FillLayout());

        ttgStateTable = new Table(this, SWT.NONE);
        ttgStateTable.setHeaderVisible(true);
        int tableWidth = ttgStateTable.getClientArea().width;

        final TableColumn ttgNameCol = new TableColumn(ttgStateTable, SWT.NONE);
        ttgNameCol.setText("Test Objective");
        ttgNameCol.setWidth(tableWidth / 2);

        final TableColumn ttgStateCol = new TableColumn(ttgStateTable, SWT.NONE);
        ttgStateCol.setText("State");
        ttgStateCol.setWidth(tableWidth / 2);

        SelectionListener sortListener = new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                boolean ascending;
                if (ev.widget == ttgNameCol) {
                    onClickedNameHeader();
                    ttgStateTable.setSortColumn(ttgNameCol);
                    ascending = (Boolean) ttgStateTable.getData(SLOT_SORT0ASCENDING);
                }
                else if (ev.widget == ttgStateCol) {
                    onClickedStateHeader();
                    ttgStateTable.setSortColumn(ttgStateCol);
                    ascending = (Boolean) ttgStateTable.getData(SLOT_SORT1ASCENDING);
                }
                else {
                    return;
                }
                ttgStateTable.setSortDirection(ascending ? SWT.DOWN : SWT.UP);
            }
        };
        ttgNameCol.addSelectionListener(sortListener);
        ttgStateCol.addSelectionListener(sortListener);

        ttgNameCol.pack();
        ttgStateCol.pack();

        /* Initialize the sort order to "ascending" */
        ttgStateTable.setData(SLOT_SORT0ASCENDING, Boolean.TRUE);
        ttgStateTable.setData(SLOT_SORT1ASCENDING, Boolean.TRUE);

        ttg2TableItems = new HashMap<>();
        currentSortOrder = SortOrder.BY_NAME;
        int sortColIdx = currentSortOrder.getColumn();
        ttgStateTable.setSortColumn(ttgStateTable.getColumn(sortColIdx));
    }

    /**
     * @return The current sorted column's index.
     */
    private int getSortCol() {
        return currentSortOrder.getColumn();
    }

    /**
     * @return <tt>true</tt> iff the current sorted column's sort order is
     *         ascending.
     */
    private boolean isSortedAscending() {
        if (currentSortOrder == SortOrder.BY_NAME) {
            return (Boolean) ttgStateTable.getData(SLOT_SORT0ASCENDING);
        }
        else {
            return (Boolean) ttgStateTable.getData(SLOT_SORT1ASCENDING);
        }
    }

    /**
     * @param group
     *            A {@link TestTypeGroup}.
     * @return The test type group's name, if it has any;
     *         <tt>ANONYMOUS_TTG_NAME</tt> otherwise.
     */
    private String getGroupName(TestTypeGroup group) {
        return (group.getName() != null) ? group.getName() : ANONYMOUS_TTG_NAME;
    }

    /**
     * Adds a {@link TestTypeGroup} to the groups displayed by the widget.
     * 
     * @param group
     *            The test type group to be added. Must not be currently
     *            displayed by the widget.
     * @throws {@link IllegalArgumentException} when the group is already
     *         displayed.
     */
    public void add(TestTypeGroup group) {
        String groupName = getGroupName(group);

        if (ttg2TableItems.containsKey(group)) {
            throw new IllegalArgumentException(groupName + ": already displayed");
        }

        int insertionIdx = ViewUtilities.determineInsertionIndex(
                ttgStateTable.getItems(),
                groupName, getSortCol(), isSortedAscending());

        TableItem newTI = new TableItem(ttgStateTable, SWT.NONE, insertionIdx);
        ttg2TableItems.put(group, newTI);
        redrawWithoutSorting(group);
    }

    /**
     * Removes a {@link TestTypeGroup} from the groups displayed by the widget.
     * 
     * @param group
     *            The test type group to be removed. Must be currently displayed
     *            by the widget.
     * @throws {@link IllegalArgumentException} when the group is not among the
     *         groups displayed by the widget.
     */
    public void remove(TestTypeGroup group) {
        if (!ttg2TableItems.containsKey(group)) {
            throw new IllegalArgumentException(getGroupName(group)
                    + ": not among the displayed groups");
        }
        else {
            ttg2TableItems.get(group).dispose();
            ttg2TableItems.remove(group);
        }
    }

    /**
     * Clears the widget.
     */
    public void removeAll() {
        ttgStateTable.removeAll();
        ttg2TableItems.clear();
    }

    /**
     * Adds all {@link TestTypeGroup} objects within the given set to the groups
     * displayed by the widget.
     * 
     * @param groups
     *            A set of {@link TestTypeGroup} items, none of which currently
     *            being displayed by the widget.
     * @throws {@link IllegalArgumentException} when a test type group within
     *         <i>groups</i> is already displayed.
     */
    public void addAll(Collection<TestTypeGroup> groups) {
        for (TestTypeGroup g : groups) {
            add(g);
        }
    }

    /**
     * Redraws the given {@link TestTypeGroup} without moving it within the
     * table.
     * 
     * @param group
     *            The {@link TestTypeGroup} to be redrawn.
     * @throws {@link IllegalArgumentException} when <i>group</i> is not among
     *         the groups currently displayed by the widget.
     */
    public void redrawWithoutSorting(final TestTypeGroup group) {
        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                if (!ttg2TableItems.containsKey(group)) {
                    throw new IllegalArgumentException(getGroupName(group)
                            + ": not currently displayed.");
                }

                TableItem groupTI = ttg2TableItems.get(group);
                groupTI.setText(0, group.getName());
                groupTI.setText(1,
                        group.isActive() ? TTG_STATE_ACTIVE : TTG_STATE_FINISHED);
                if (!group.isActive()) {
                    groupTI.setForeground(Display.getDefault().
                            getSystemColor(SWT.COLOR_DARK_GREEN));
                }
            }
        });
    }

    /**
     * Redraws the given {@link TestTypeGroup}, moving it within the table if
     * necessary. If the given group has not been added previously, it gets
     * added.
     * 
     * @param g
     *            The {@link TestTypeGroup} to be redrawn.
     */
    public void redraw(final TestTypeGroup g) {
        if (Display.getDefault().isDisposed()) {
            return;
        }
        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                if (!ttgStateTable.isDisposed()) {
                    if(ttg2TableItems.containsKey(g)) {
                        remove(g);
                    }
                    add(g);
                }
            }
        });
    }

    /**
     * Removes and add all groups.
     */
    private void reinsertAll() {
        Set<TestTypeGroup> displayedGroups = new HashSet<>(ttg2TableItems.keySet());
        removeAll();
        addAll(displayedGroups);
    }

    /**
     * Sorts the table by name.
     * 
     * @param ascending
     *            Set to {@code true} for ascending sort order.
     */
    public void sortByName(boolean ascending) {
        currentSortOrder = SortOrder.BY_NAME;
        int sortColIdx = currentSortOrder.getColumn();
        ttgStateTable.setSortColumn(ttgStateTable.getColumn(sortColIdx));
        ttgStateTable.setData(SLOT_SORT0ASCENDING, ascending);
        reinsertAll();
    }

    /**
     * Sorts the table by state.
     * 
     * @param ascending
     *            Set to {@code true} for ascending sort order.
     */
    public void sortByState(boolean ascending) {
        currentSortOrder = SortOrder.BY_STATE;
        int sortColIdx = currentSortOrder.getColumn();
        ttgStateTable.setSortColumn(ttgStateTable.getColumn(sortColIdx));
        ttgStateTable.setData(SLOT_SORT1ASCENDING, ascending);
        reinsertAll();
    }

    /**
     * Event handler for clicks on the name column's header. If necessary, this
     * handler calls the appropriate sorting methods.
     */
    private void onClickedNameHeader() {
        boolean sortAscending = true;
        if (currentSortOrder == SortOrder.BY_NAME) {
            sortAscending = !(Boolean) ttgStateTable.getData(SLOT_SORT0ASCENDING);
        }
        sortByName(sortAscending);
    }

    /**
     * Event handler for clicks on the state column's header. If necessary, this
     * handler calls the appropriate sorting methods.
     */
    private void onClickedStateHeader() {
        boolean sortAscending = true;
        if (currentSortOrder == SortOrder.BY_STATE) {
            sortAscending = !(Boolean) ttgStateTable.getData(SLOT_SORT1ASCENDING);
        }
        sortByState(sortAscending);
    }

    /**
     * A factory for {@link TTGStateView} instances.
     */
    public static class Factory {
        /**
         * Make a new {@link TTGStateView} instance.
         * 
         * @param parent
         *            The instance's parent widget.
         * @return The new {@link TTGStateView} instance.
         */
        public TTGStateView makeInstance(Composite parent) {
            return new TTGStateView(parent);
        }
    }

    @Override
    public void setRedraw(final boolean doRedraw) {
        final Table stateTable = ttgStateTable;

        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                if (!TTGStateView.super.isDisposed()) {
                    TTGStateView.super.setRedraw(doRedraw);
                }
                if (!stateTable.isDisposed()) {
                    stateTable.setRedraw(doRedraw);
                }
            }
        });
    }
}
