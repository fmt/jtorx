package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.TestTreeNode;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;

/**
 * An accessor interface for various test run properties.
 */
public interface TestExecutionDataAccessor {
    /**
     * @return The current test trace.
     */
    List<TestTraceItem> getTestTrace();

    /**
     * Retrieves a LazyOTF test tree node.
     * 
     * @param id
     *            The node's ID. (-1 for root)
     * @return The retrieved test tree node or <tt>null</tt> if no node exists
     *         having the specified <i>id</i>.
     * @throws LazyOTF.InactiveException
     *             when LazyOTF is currently inactive.
     */
    TestTreeNode getTestTreeNode(int id) throws LazyOTF.InactiveException;

    /**
     * Gets the inputs currently offered by the STS.
     * 
     * @return A Vector of input Labels.
     */
    Vector<Label> getOrdinaryModelInputs();

    /**
     * Gets the outputs currently offered by the STS.
     * 
     * @return A Vector of output Labels.
     */
    Vector<Label> getModelOutputs();

    /**
     * @return The current LazyOTF proposal (i.e. inputs and expected outputs)
     */
    Set<Label> getCurrentLazyOTFProposal();

    /**
     * @return The current label interpretation.
     */
    IOLTSInterpretation getInterpretation();

    /**
     * @return <tt>true</tt> if LazyOTF is currently active.
     */
    boolean isLazyOTFActive();

    /**
     * @return The current LazyOTF-proposed model inputs.
     */
    Vector<Label> getLazyOTFModelInputs();

    /**
     * @return The current driver state's valuation.
     */
    Map<State, Map<String, String>> getCurrentValuation();
}
