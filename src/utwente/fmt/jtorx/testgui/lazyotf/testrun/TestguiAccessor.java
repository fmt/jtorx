package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import utwente.fmt.lts.Label;

/**
 * An accessor interface for various methods of the JTorX GUI.
 */
public interface TestguiAccessor {
    /**
     * Updates the main model stimuli menu.
     */
    void updateModelMenu();

    /**
     * Requests the GUI to issue the observation of the model.
     */
    void handleObserveEvent();

    /**
     * Requests the GUI to issue the stimulation of the model using the given
     * label.
     * 
     * @param stim
     *            The label with which the model is stimulated.
     */
    void handleGivenStimulusEvent(Label stim);
}
