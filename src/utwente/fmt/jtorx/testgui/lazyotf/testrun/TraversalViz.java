package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.Vector;

import org.eclipse.swt.widgets.Display;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTF.InactiveException;
import utwente.fmt.jtorx.lazyotf.LazyOTF.LazyOTFEvents;
import utwente.fmt.jtorx.lazyotf.LocationMgr;
import utwente.fmt.jtorx.lazyotf.TestTreeNode;
import utwente.fmt.jtorx.lazyotf.TestTypeGroup;
import utwente.fmt.jtorx.lazyotf.TorXInterfaceExtensions;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver.ExtendedInteractionResult;
import utwente.fmt.jtorx.lazyotf.remoteimpl.RemoteLazyOTF;
import utwente.fmt.jtorx.lazyotf.utilities.ObserverMethod;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;

/**
 * The traversal visualisation window's main controller. This class manages user
 * requests coming up within {@link ConsoleView}, {@link TreeExplorerView},
 * {@link TTGStateView} and {@link ValuationExplorerView}.
 */
public class TraversalViz {
    /** The test trace. */
    private final List<TestTraceItem> testTraceItems = new ArrayList<>();

    /** The LazyOTF instance whose test tree is visualised. */
    private final LazyOTF lazyOTFInstance;

    /** The LazyOTF JTorX driver. */
    private final LazyOTFAwareSimOnlineTestingDriver lazyOTFDriver;

    /** The view associated with this controller. */
    private TraversalVizView view;

    /** The JTorX view controller. */
    private final TestguiAccessor owner;

    /** Accessor to various JTorX bits. */
    private final TorXInterfaceExtensions torxExts;

    /**
     * Set up the traversal visualisation controller.
     * 
     * @param lazyOTFInstance
     *            The LazyOTF instance whose test tree is to be visualised.
     * @param drv
     *            The JTorX driver belonging to the LazyOTF instance.
     * @param testGuiIf
     *            An accessor to various functions of the JTorX GUI.
     * @param torxIfExt
     *            An accessor to various aspects of the test run.
     */
    public TraversalViz(LazyOTF lazyOTFInstance,
            LazyOTFAwareSimOnlineTestingDriver drv,
            TestguiAccessor testGuiIf,
            TorXInterfaceExtensions torxIfExt) {
        this.lazyOTFInstance = lazyOTFInstance;
        lazyOTFDriver = drv;
        owner = testGuiIf;
        torxExts = torxIfExt;
    }

    /**
     * Create and display the "LazyOTF Introspection" window.
     */
    public void init() {
        setupConsoleGlue();
        setupEventObservers();

        /* Add test type groups to the TTG view */
        LocationMgr locMgr = lazyOTFInstance.getLocationManager();
        view.getTTGStateView().addAll(locMgr.getTestTypeGroupMgr().getTestTypeGroups());

        /* Update console */
        ((RemoteLazyOTF) lazyOTFInstance).flushMessageLog();
    }

    /**
     * Set the MVC view instance.
     * 
     * @param view
     *            The view instance.
     */
    protected void setView(TraversalVizView view) {
        this.view = view;
    }

    /**
     * Inform the console view about changes in the LazyOTF instance and the
     * LazyOTF instance about commands issued via the console.
     */
    protected void setupConsoleGlue() {
        /* Glue LazyOTF to the console view */
        final ConsoleView cons = view.getConsoleView();
        lazyOTFInstance.addObserver(new utwente.fmt.jtorx.lazyotf.utilities.Observer() {
            @ObserverMethod
            public void update(final LazyOTFEvents.MessageExchangedEvent msg) {
                try {
                    Display.getDefault().asyncExec(new Runnable() {
                        @Override
                        public void run() {
                            if (!cons.isDisposed()) {
                                cons.displayMessage(msg.message);
                            }
                        }
                    });
                }
                catch (Exception e) {
                    System.err.println("Got exception " + e
                            + " while trying to write to the LazyOTF console");
                    e.printStackTrace();
                }
            }
        });

        view.getConsoleView().getEventManager().addObserver(new Observer() {
            @Override
            public void update(Observable source, Object data) {
                String cmd = (String) data;
                try {
                    lazyOTFInstance.getInstancePort().send(cmd.trim());
                }
                catch (UnsupportedOperationException e) {
                    throw new IllegalStateException("Impl. error: " + e);
                }
            }
        });
    }

    /**
     * Set up view event handlers.
     */
    protected void setupEventObservers() {
        final TreeExplorerView texpView = view.getTreeExplorerView();
        texpView.getEventManager().addObserver(new Observer() {
            @Override
            public void update(Observable source, Object arg) {
                TreeExplorerView view = texpView;

                if (arg == TreeExplorerView.EventKind.ABANDON_TEST_TREE_REQUESTED) {
                    lazyOTFInstance.setInactive();
                    owner.updateModelMenu();
                    view.updateTraversalTree();
                }
                else if (arg == TreeExplorerView.EventKind.RECOMPUTE_TEST_TREE_REQUESTED) {
                    lazyOTFDriver.recomputeTestTree();
                    owner.updateModelMenu();
                    view.updateTraversalTree();
                }
                else if (arg == TreeExplorerView.EventKind.STIMULATE_REQUESTED) {
                    Vector<Label> input = getLazyOTFModelInputs();
                    if (input.size() > 0) {
                        owner.handleGivenStimulusEvent(input.get(0));
                    }
                    view.updateTraversalTree();
                }
                else if (arg == TreeExplorerView.EventKind.OBSERVE_REQUESTED) {
                    owner.handleObserveEvent();
                    view.updateTraversalTree();
                }
            }
        });
    }

    /**
     * Issue a traversal tree update, to be called after having registered a
     * stimulation or an observation.
     */
    private void updateVisTreeAfterLabelRegistration() {
        if (view != null) {
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!view.isDisposed()) {
                        view.getTreeExplorerView().updateTraversalTree();
                    }
                }
            });
        }
    }

    /**
     * Register a STS stimulation or observation event with the LazyOTF
     * introspection.
     * 
     * @param res
     *            The LazyOTF-enabled JTorX driver's
     *            {@link ExtendedInteractionResult} instance for the current
     *            test step.
     */
    public void registerInteractionResult(ExtendedInteractionResult res) {
        if (view.isDisposed()) {
            return;
        }
        TestTraceItem tti = new TestTraceItem(res, torxExts);
        testTraceItems.add(tti);
        updateVisTreeAfterLabelRegistration();

        view.getTTGStateView().setRedraw(false);
        for (TestTypeGroup g : lazyOTFInstance.getLocationManager().getTestTypeGroupMgr()
                .getTestTypeGroups()) {
            view.getTTGStateView().redraw(g);
        }
        view.getTTGStateView().setRedraw(true);
    }

    /**
     * Destroy the LazyOTF introspection window.
     */
    public void finish() {
        if (view != null) {
            Runnable destroyWnd = new Runnable() {
                @Override
                public void run() {
                    if (!view.isDisposed()) {
                        view.getShell().dispose();
                    }
                }
            };

            Display.getDefault().syncExec(destroyWnd);
        }
    }

    /**
     * Get the inputs currently offered by the test tree.
     * 
     * @return A Vector of input Labels.
     */
    private Vector<Label> getLazyOTFModelInputs() {
        return lazyOTFDriver.getModelInputs(true, false);
    }

    /**
     * @return The current driver state's valuation.
     */
    private Map<State, Map<String, String>> getCurrentValuation() {
        Map<State, Map<String, String>> result = new HashMap<>();
        Iterator<? extends State> stateIt =
                lazyOTFDriver.getTesterState().getSubStates();
        while (stateIt.hasNext()) {
            State s = stateIt.next();
            result.put(s, torxExts.getValuation(s.getID()));
        }
        return result;
    }

    /**
     * Make a {@link TestExecutionDataAccessor} instance allowing access to
     * various test run data.
     * 
     * @return A new {@link TestExecutionDataAccessor} instance.
     */
    protected TestExecutionDataAccessor getTestExecutionDataAccessor() {
        return new TestExecutionDataAccessor() {

            @Override
            public List<TestTraceItem> getTestTrace() {
                return testTraceItems;
            }

            @Override
            public TestTreeNode getTestTreeNode(int id) throws InactiveException {
                return lazyOTFInstance.getTestTreeNode(id);
            }

            @Override
            public Vector<Label> getOrdinaryModelInputs() {
                return lazyOTFDriver.getModelInputsClean();
            }

            @Override
            public Vector<Label> getModelOutputs() {
                return lazyOTFDriver.getModelOutputs();
            }

            @Override
            public Set<Label> getCurrentLazyOTFProposal() {
                return lazyOTFDriver.getCurrentLazyOTFProposal();
            }

            @Override
            public IOLTSInterpretation getInterpretation() {
                return lazyOTFDriver.getInterpretation();
            }

            @Override
            public boolean isLazyOTFActive() {
                return lazyOTFInstance.isActive();
            }

            @Override
            public Vector<Label> getLazyOTFModelInputs() {
                return lazyOTFDriver.getModelInputs(true, false);
            }

            @Override
            public Map<State, Map<String, String>> getCurrentValuation() {
                return TraversalViz.this.getCurrentValuation();
            }
        };
    }
}
