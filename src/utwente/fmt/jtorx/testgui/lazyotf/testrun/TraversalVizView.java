package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * <p>
 * The introspection window's toplevel view. This class sets up the sub-views
 * contained in the tab control, such as {@link TreeExplorerView},
 * {@link ValuationExplorerView}, {@link TTGStateView} and {@link ConsoleView}.
 * </p>
 * 
 * <p>
 * This view can be used just like an SWT widget.
 * </p>
 */
public class TraversalVizView extends Composite {
    /** The LazyOTF console view. */
    private final ConsoleView console;

    /** The Valuation Explorer view. */
    private final ValuationExplorerView valuationExplorer;

    /** The Test Type Group State view. */
    private final TTGStateView ttgStateView;

    /** The Tree Explorer view. */
    private final TreeExplorerView treeExplorerView;

    /**
     * Sets up a TraversalVizView instance.
     * 
     * @param parent
     *            The view's parent control.
     * @param testDataAcc
     *            An accessor to various aspects of the test run.
     */
    public TraversalVizView(Composite parent, TestExecutionDataAccessor testDataAcc) {
        this(parent, testDataAcc, null, null, null, null);
    }

    /**
     * Constructs a new {@link TraversalVizView} object.
     * 
     * @param parent
     *            The view's parent SWT widget.
     * @param testDataAcc
     *            An accessor to various aspects of the test run.
     * @param trevFact
     *            A factory for {@link TreeExplorerView} objects.
     * @param consFact
     *            A factory for {@link ConsoleView} objects.
     * @param valExpFact
     *            A factory for {@link ValuationExplorerView} objects.
     * @param ttgStateViewFact
     *            A factory for {@link TTGStateView} objects.
     */
    public TraversalVizView(Composite parent,
            TestExecutionDataAccessor testDataAcc,
            TreeExplorerView.Factory trevFact,
            ConsoleView.Factory consFact,
            ValuationExplorerView.Factory valExpFact,
            TTGStateView.Factory ttgStateViewFact) {

        super(parent, SWT.NONE);

        /* Set up the tab folder control */
        CTabFolder tabs = new CTabFolder(this, SWT.BORDER);

        if (trevFact == null) {
            treeExplorerView = new TreeExplorerView.Factory().makeInstance(tabs, testDataAcc);
        }
        else {
            treeExplorerView = trevFact.makeInstance(tabs, testDataAcc);
        }

        CTabItem ttree = new CTabItem(tabs, SWT.NONE);
        ttree.setControl(treeExplorerView);
        ttree.setText("Trace/Tree Explorer");

        /* Set up the valuation inspector view */
        if (valExpFact == null) {
            valuationExplorer = new ValuationExplorerView.Factory().makeInstance(tabs);
        }
        else {
            valuationExplorer = valExpFact.makeInstance(tabs);
        }

        CTabItem tstat = new CTabItem(tabs, SWT.NONE);
        tstat.setControl(valuationExplorer);
        tstat.setText("Valuation");
        treeExplorerView.setValuationExplorerView(valuationExplorer);

        /* Set up the test type group state view */
        if (ttgStateViewFact == null) {
            ttgStateView = new TTGStateView.Factory().makeInstance(tabs);
        }
        else {
            ttgStateView = ttgStateViewFact.makeInstance(tabs);
        }

        CTabItem ttgs = new CTabItem(tabs, SWT.NONE);
        ttgs.setControl(ttgStateView);
        ttgs.setText("Test Objectives");

        /* Set up the console view */
        if (consFact == null) {
            console = new ConsoleView.Factory().makeInstance(tabs);
        }
        else {
            console = consFact.makeInstance(tabs);
        }

        CTabItem dbg = new CTabItem(tabs, SWT.NONE);
        dbg.setText("LazyOTF debug console");
        dbg.setControl(console);

        this.setLayout(new FillLayout());
        this.layout();
    }

    /**
     * Return a reference to the console view attached to this view.
     * 
     * @return A {@link ConsoleView} instance.
     */
    protected ConsoleView getConsoleView() {
        return console;
    }

    /**
     * Returns a reference to the test type group state view attached to this
     * view.
     * 
     * @return A {@link TTGStateView} instance.
     */
    protected TTGStateView getTTGStateView() {
        return ttgStateView;
    }

    /**
     * Returns a reference to the valuation explorer view attached to this view.
     * 
     * @return A {@link ValuationExplorerView} instance.
     */
    protected ValuationExplorerView getValuationExplorerView() {
        return valuationExplorer;
    }

    /**
     * Returns a reference to the test tree explorer view attached to this view.
     * 
     * @return A {@link TreeExplorerView} instance.
     */
    protected TreeExplorerView getTreeExplorerView() {
        return treeExplorerView;
    }
}
