package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import utwente.fmt.jtorx.lazyotf.LazyOTF.InactiveException;
import utwente.fmt.jtorx.lazyotf.TestTreeNode;
import utwente.fmt.jtorx.lazyotf.location.InstantiatedLocation;
import utwente.fmt.jtorx.lazyotf.utilities.Utils;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.StateSetRepresentation;
import utwente.fmt.jtorx.testgui.lazyotf.utilities.TreeToolTip;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Variable;

/**
 * <p>
 * A widget implementing the Tree/Trace explorer view. Listeners for events such
 * as equests for stimulation or observation and test tree manipulation can be
 * registered with the {@link TreeExplorerView.EventManager} object obtainable
 * via <i>getEventManager()</i>. This view automatically loads the displayed
 * data using the provided interfaces.
 * </p>
 * 
 * <p>
 * This view can also drive the {@link ValuationExplorerView}.
 * </p>
 */
public class TreeExplorerView extends Composite {

    /**
     * The tree control displaying the current trace along with the test tree
     * (depending on LazyOTF's activeness.
     */
    private final Tree traversalTree;

    /** The traversal tree's context menu control. */
    private final Menu traversalTreeContextMenu;

    /** The "Recompute Test Tree" menu item control. */
    private final MenuItem recomputeTestTreeMenuItem;

    /** The "Abandon Test Tree" menu item control. */
    private final MenuItem abandonTestTreeMenuItem;

    /** The "Stimulate with LazyOTF-proposed input" menu item control. */
    private final MenuItem stimulateMenuItem;

    /** The "Observe" menu item control. */
    private final MenuItem observeMenuItem;

    /** This instance's observable facility. */
    private final EventManager eventMgr;

    /** The valuation explorer view. */
    private ValuationExplorerView valExpView;

    /** An accessor to various information about the test run. */
    private final TestExecutionDataAccessor testExecutionData;

    /**
     * A tree item slot name for the {@link TestTreeNode} item associated with
     * the respective tree item. If a tree item is associated with a test tree
     * node, it becomes the root item of the respective test sub-tree. (i.e.,
     * the "rest" of the test tree is drawn below this node.)
     */
    private static final String SLOT_TTREE = "lazyotf-test-tree";

    /**
     * A tree item slot name for the "current step" item marker. If a tree
     * item's respective slot contains non-null data, it is the "current step"
     * item.
     */
    private static final String SLOT_CURSTEP = "cs-root";

    /**
     * A tree item slot name for the {@link TestTraceItem} associated with the
     * node, if any. (Otherwise <tt>null</tt>.)
     */
    private static final String SLOT_TRACE = "trace-item";

    /**
     * A tree item slot name containing an object printing the flags and their
     * tooltip.
     */
    private static final String SLOT_FLAGS = "trace-flags";

    private static final int COL_TRANSITION = 0;
    private static final int COL_TARGETS = 1;
    private static final int COL_WEIGHT = 2;
    private static final int COL_STEPNR = 3;
    private static final int COL_FLAGS = 4;

    public enum TraversalTreeItemKind {
        TEST_GOAL,
        TEST_INDUCING,
        ORDINARY,
        FIRST_TESTTREE_ITEM,
        ABANDON_ITEM,
        ACTIVE_CURRENTSTEP
    }

    /**
     * Constructs a new {@link TreeExplorerView} instance.
     * 
     * @param parent
     *            The parent widget.
     * @param testExecData
     *            An accessor to various information about the test run.
     */
    public TreeExplorerView(Composite parent, TestExecutionDataAccessor testExecData) {
        super(parent, SWT.NONE);

        testExecutionData = testExecData;
        setLayout(new FillLayout());

        /* Set up the event manager */
        eventMgr = new EventManager(this);

        /* Set up the traversal inspector view */
        traversalTree = new Tree(this, SWT.BORDER | SWT.VIRTUAL);
        setupTraversalTree();

        traversalTreeContextMenu = new Menu(traversalTree);
        traversalTree.setMenu(traversalTreeContextMenu);
        stimulateMenuItem = new MenuItem(traversalTreeContextMenu, SWT.PUSH);
        observeMenuItem = new MenuItem(traversalTreeContextMenu, SWT.PUSH);
        recomputeTestTreeMenuItem = new MenuItem(traversalTreeContextMenu, SWT.PUSH);
        abandonTestTreeMenuItem = new MenuItem(traversalTreeContextMenu, SWT.PUSH);
        setupTraversalTreeMenuItems();

        setupTraversalTreeListeners();
        setupTraversalTreeItemDataHandler();
        setupFlagToolTipHandler();
    }

    /**
     * Sets up the tooltips for the flags column.
     */
    private void setupFlagToolTipHandler() {
        TreeToolTip.installToolTipHandler(traversalTree, new TreeToolTip.ToolTipGenerator() {
            @Override
            public String getToolTip(TreeItem ti, TreeColumn column) {
                if (column == traversalTree.getColumn(COL_FLAGS)) {

                    String result = "";
                    if (ti.getData(SLOT_CURSTEP) != null) {
                        if (testExecutionData.isLazyOTFActive()) {
                            result += "LazyOTF is active.";
                        }
                    }

                    if (ti.getData(SLOT_FLAGS) != null) {
                        Flags f = (Flags) ti.getData(SLOT_FLAGS);
                        result += f.toString(false);
                    }

                    return result;
                }
                else {
                    return null;
                }
            }
        });
    }

    /**
     * Set up the traversal tree menu items.
     */
    private void setupTraversalTreeMenuItems() {
        recomputeTestTreeMenuItem.setText("Recompute Test Tree");
        abandonTestTreeMenuItem.setText("Abandon Test Tree");
        stimulateMenuItem.setText("Stimulate with LazyOTF-proposed input");
        observeMenuItem.setText("Observe");
    }

    /**
     * Set up the traversal tree's presentation.
     */
    private void setupTraversalTree() {
        TreeColumn tcNames = new TreeColumn(traversalTree, SWT.NONE);
        tcNames.setText("Transition");
        tcNames.setWidth(180);
        TreeColumn tcStates = new TreeColumn(traversalTree, SWT.NONE);
        tcStates.setText("Target states");
        tcStates.setWidth(132);

        TreeColumn tcWeight = new TreeColumn(traversalTree, SWT.NONE);
        tcWeight.setText("Weight");
        tcWeight.setWidth(40);
        traversalTree.setHeaderVisible(true);

        TreeColumn tcStepNr = new TreeColumn(traversalTree, SWT.NONE);
        tcStepNr.setText("Step");
        tcStepNr.setWidth(40);

        TreeColumn tcFlags = new TreeColumn(traversalTree, SWT.NONE);
        tcFlags.setText("Flags");
        tcFlags.setWidth(20);

        traversalTree.setItemCount(testExecutionData.getTestTrace().size() + 1);
    }

    /**
     * Set up the traversal tree's context menu's event listeners
     */
    private void setupTraversalTreeListeners() {
        recomputeTestTreeMenuItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                eventMgr.recomputeTestTreeRequested();
            }
        });

        abandonTestTreeMenuItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                eventMgr.abandonTestTreeRequested();
            }
        });

        stimulateMenuItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                eventMgr.stimulateRequested();
            }
        });

        observeMenuItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                eventMgr.observeRequested();
            }
        });

        traversalTree.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent ev) {
                if (valExpView == null) {
                    return; // no valuation explorer configured
                }

                TreeItem it = (TreeItem) ev.item;
                Object dt = it.getData(SLOT_TTREE);
                valExpView.clear();

                if (dt != null && (it.getData(SLOT_CURSTEP) == null)) {
                    /* Test tree node selected */
                    TestTreeNode treeNode = (TestTreeNode) dt;
                    for (InstantiatedLocation location : treeNode.getLocations()) {
                        for (Variable var : location.getLocationVariables()) {
                            valExpView.add(location.getLocation().getName(),
                                    location.getID(), var.getName(),
                                    location.getValue(var).toString());
                        }
                    }
                }
                else if (it.getData(SLOT_TRACE) != null) {
                    /* Trace item selected */
                    TestTraceItem tti = (TestTraceItem) it.getData(SLOT_TRACE);
                    Map<State, Map<String, String>> val = tti.getValuation();
                    for (State key : val.keySet()) {
                        for (String var : val.get(key).keySet()) {
                            valExpView.add(StateSetRepresentation.pureNodeName(key),
                                    "T" + key.getID(), var, val.get(key).get(var));
                        }
                    }
                }
                else if (it.getData(SLOT_CURSTEP) != null) {
                    /* "Current Step" selected */
                    Map<State, Map<String, String>> val =
                            testExecutionData.getCurrentValuation();
                    for (State key : val.keySet()) {
                        for (String var : val.get(key).keySet()) {
                            valExpView.add(StateSetRepresentation.pureNodeName(key),
                                    "T" + key.getID(), var, val.get(key).get(var));
                        }
                    }
                }
            }
        });
    }

    /**
     * Install a traversal tree listener responsible for computing traversal
     * tree items on demand (Since the tree control is virtual, it is not
     * possible to add static tree items).
     */
    private void setupTraversalTreeItemDataHandler() {
        traversalTree.addListener(SWT.SetData, new Listener() {
            @Override
            public void handleEvent(Event e) {
                List<TestTraceItem> testTraceItems = testExecutionData.getTestTrace();
                TreeItem item = (TreeItem) e.item;

                int index = e.index;
                if (item.getParentItem() == null) {
                    if (index < testTraceItems.size()) {
                        /*
                         * The item is located above the "Current Step" item,
                         * thus it is a trace item.
                         */
                        drawTraceItem(index, item);
                    }
                    else {
                        /* The item is the "Current Step" item. */
                        drawCurrentStepItem(index, item);
                    }
                }
                else {
                    /* The item is in the "Current Step" subtree. */
                    drawCurrentStepChild(index, item);
                }
            }
        });
    }

    /**
     * Recompute the traversal tree's contents.
     */
    public void updateTraversalTree() {
        boolean reselectCurrent = false;
        List<TestTraceItem> testTraceItems = testExecutionData.getTestTrace();
        traversalTree.setItemCount(testTraceItems.size() + 1);
        if (reselectCurrent) {
            traversalTree.select(traversalTree.getItem(testTraceItems.size()));
            traversalTree.setTopItem(traversalTree.getItem(testTraceItems.size()));
        }
        traversalTree.clearAll(true);
        traversalTree.setItemCount(0);
        traversalTree.setItemCount(testTraceItems.size() + 1);

        /*
         * You can only compute a new test tree if there are inputs at the
         * current step. Thus, gray out the menu item accordingly.
         */
        boolean recEnabled = (testExecutionData.getOrdinaryModelInputs().size() > 0);
        recomputeTestTreeMenuItem.setEnabled(recEnabled);

        abandonTestTreeMenuItem.setEnabled(testExecutionData.isLazyOTFActive());
        stimulateMenuItem.setEnabled(testExecutionData.isLazyOTFActive()
                && testExecutionData.getLazyOTFModelInputs().size() > 0);
    }

    /**
     * Set up the given tree item to represent the Nth test trace item with N =
     * index.
     * 
     * @param index
     *            The test trace item's index.
     * @param it
     *            The tree item to be modified.
     */
    private void drawTraceItem(int index, TreeItem it) {
        List<TestTraceItem> testTraceItems = testExecutionData.getTestTrace();
        TestTraceItem tti = testTraceItems.get(index);
        it.setText(COL_STEPNR, "" + (index + 1));
        it.setText(COL_TRANSITION, tti.getName());
        it.setText(COL_TARGETS, tti.getLocations());
        it.setData(SLOT_TRACE, tti);

        Flags flags = new Flags(tti);
        it.setData(SLOT_FLAGS, new Flags(tti));
        it.setItemCount(0);

        it.setText(COL_FLAGS, flags.toString(true));
    }

    /**
     * Set up the given tree item to represent the "Current Step" node.
     * 
     * @param index
     *            unused.
     * @param it
     *            The tree item to be modified.
     */
    private void drawCurrentStepItem(int index, TreeItem it) {
        it.setText(COL_TRANSITION, "Current step");
        Vector<Label> ordinaryInputs = testExecutionData.getOrdinaryModelInputs();
        Vector<Label> ordinaryOutputs = testExecutionData.getModelOutputs();
        /*
         * Current Step children: ordinary (i.e. given by the model) inputs,
         * then the model outputs. If LazyOTF is active, prepend the "Current
         * Test Tree" and append the LazyOTF input, if any.
         */
        int items = ordinaryInputs.size() + ordinaryOutputs.size();
        if (testExecutionData.isLazyOTFActive()) {
            items++;
            Set<Label> currentProposal = testExecutionData.getCurrentLazyOTFProposal();
            Set<Label> inputs = Utils.filterLabels(currentProposal,
                    testExecutionData.getInterpretation().isInput());

            it.setData("lazyotf-inputs", new Vector<Label>(inputs));

            try {
                TestTreeNode root = testExecutionData.getTestTreeNode(-1);
                if (inputs.size() > 0) {
                    items += root.getChildren().size();
                }
                it.setData(SLOT_TTREE, root);
                it.setText(COL_WEIGHT, "" + root.getWeight());
            }
            catch (InactiveException e) {
                throw new IllegalStateException("Internal error: Tried to display"
                        + " an inactive instance's test tree.");
            }
        }
        it.setItemCount(items);

        /* This marks this tree item as the "Current Step" item */
        it.setData(SLOT_CURSTEP, "dummy");

        if (testExecutionData.isLazyOTFActive()) {
            setTraversalTreeIcon(it, TraversalTreeItemKind.ACTIVE_CURRENTSTEP);
        }

        /* Display the last trace item's valuation for this item */
        List<TestTraceItem> trace = testExecutionData.getTestTrace();
        if (trace.size() > 0) {
            TestTraceItem last = trace.get(trace.size() - 1);
            it.setData(SLOT_TRACE, last);
        }
    }

    /**
     * Set up the given tree item to represent the "Current Test Tree" node.
     * 
     * @param it
     *            The tree item to be modified.
     * @param lazyOTFNodeID
     *            LazyOTF's test tree node ID of the root node.
     */
    private void drawTestTreeRoot(TreeItem it, int lazyOTFNodeID) {
        try {
            TestTreeNode tn = testExecutionData.getTestTreeNode(lazyOTFNodeID);
            it.setData(SLOT_TTREE, tn);
            it.setItemCount(tn.getChildren().size());
        }
        catch (Exception lex) {
            /* Implementation or I/O error => Refuse further retrieval */
            it.setText(COL_TRANSITION, "No LazyOTF Test Tree available.");
            it.setItemCount(0);
        }
    }

    /**
     * Set up the given tree item to represent a child of the "Current Step"
     * node.
     * 
     * @param index
     *            The given tree item's index within its parent's children.
     * @param it
     *            The tree item to be modified.
     */
    private void drawCurrentStepChild(int index, TreeItem it) {

        if (it.getParentItem().getData(SLOT_CURSTEP) != null) {
            /* This is a direct child to the "Current Step" node */
            if (index == 0 && testExecutionData.isLazyOTFActive()) {
                /* it is the "Current Test Tree" node. */
                it.setText(COL_TRANSITION, "Current Test Tree");
                drawTestTreeRoot(it, -1);
            }
            else {
                /* it represents an input or output label. */
                if (testExecutionData.isLazyOTFActive()) {
                    /* Account for the missing "Current Test Tree" node */
                    index--;
                }

                Vector<Label> inputs = testExecutionData.getOrdinaryModelInputs();
                Vector<Label> outputs = testExecutionData.getModelOutputs();

                if (index < inputs.size()) {
                    /* it represents an input label */
                    it.setText(COL_TRANSITION, inputs.get(index).getString());
                }
                else if (index < (inputs.size() + outputs.size())) {
                    /* it represents an output label */
                    it.setText(COL_TRANSITION, outputs.get(index - inputs.size()).getString());
                }
                else {
                    /* it is a LazyOTF-proposed label */
                    if (it.getParentItem().getData("lazyotf-inputs") != null) {
                        TreeItem pt = it.getParentItem();
                        TestTreeNode parent = (TestTreeNode) pt.getData(SLOT_TTREE);
                        assert (parent != null);

                        TestTreeNode ourNode;
                        try {
                            int ourIdx = index - (inputs.size() + outputs.size());
                            /* TODO: ArrayOutOfBounds may happen here */
                            ourNode = parent.getChildren().get(ourIdx);
                        }
                        catch (InactiveException e) {
                            updateTraversalTree();
                            return;
                        }
                        String labelName = ourNode.getLabel().getString();
                        it.setText(COL_TRANSITION, labelName);

                        /* Display a modified test tree below this node */
                        drawTestTreeRoot(it, ourNode.getNodeID());

                        Display curDsp = Display.getCurrent();
                        it.setForeground(0, curDsp.getSystemColor(SWT.COLOR_DARK_GREEN));
                    }
                }
            }
        }
        else {
            /* The given item resides in the test tree */
            TestTreeNode parent = (TestTreeNode) it.getParentItem().getData(SLOT_TTREE);
            assert (parent != null);

            TestTreeNode ourNode;
            try {
                ourNode = parent.getChildren().get(index);
            }
            catch (InactiveException e) {
                updateTraversalTree();
                return;
            }

            String ourName = ourNode.getLabel().getString();
            it.setText(COL_TRANSITION, ourName);
            it.setText(COL_TARGETS,
                    StateSetRepresentation.makeStateSetRepresentation(ourNode.getLocations()));

            /* Set the flags */

            Flags flags = new Flags(!ourNode.getTestGoals().isEmpty(), !ourNode.getInducingStates()
                    .isEmpty(), false, false, false);
            it.setData(SLOT_FLAGS, flags);
            it.setText(COL_FLAGS, flags.toString(true));

            /* Display subtree weight */
            it.setText(COL_WEIGHT, "" + ourNode.getWeight());

            try {
                it.setItemCount(ourNode.getChildren().size());
            }
            catch (InactiveException e) {
                updateTraversalTree();
                return;
            }

            it.setData(SLOT_TTREE, ourNode);
        }
    }

    /**
     * Equip the given tree item with a flag icon.
     * 
     * @param it
     *            The tree item.
     * @param kind
     *            The tree item kind.
     */
    private void setTraversalTreeIcon(TreeItem it, TraversalTreeItemKind kind) {
        Image icon = null;

        if (kind == TraversalTreeItemKind.ACTIVE_CURRENTSTEP) {
            icon = IconManager.getStar();
        }

        if (icon != null) {
            it.setImage(COL_FLAGS, icon);
        }
    }

    /**
     * Expand the "Current Step" item and scroll to its last child.
     */
    public void selectAndExpandCurrentStepItem() {
        if (traversalTree.getItemCount() > 0) {
            final int lastIdx = traversalTree.getItemCount() - 1;
            final TreeItem currentStep = traversalTree.getItem(lastIdx);

            /*
             * TODO: Determine & remove superfluous update/redraw calls and fix
             * the weird scrolling behaviour occurring under QT4
             */

            /* Expand the current test tree */
            getDisplay().update();
            getShell().update();
            getShell().redraw();
            currentStep.setExpanded(true);

            /* Scroll down */
            if (currentStep.getItemCount() > 0) {
                traversalTree.showItem(currentStep.getItem(lastIdx));
            }
            else {
                traversalTree.showItem(currentStep);
            }
        }
    }

    /**
     * Get this instance's event-managing Observable object. Attach observers to
     * this object in order to observe this instance.
     * 
     * @return The Observable object associated with this instance.
     */
    public Observable getEventManager() {
        return eventMgr;
    }

    /**
     * Sets the valuation explorer view. As long as the valuation explorer view
     * of this view is not <tt>null</tt> (which it is initially), this view
     * automatically drives the valuation explorer view.
     * 
     * @param it
     *            The new {@link ValuationExplorerView} associated with this
     *            view.
     */
    public void setValuationExplorerView(ValuationExplorerView it) {
        valExpView = it;
    }

    enum EventKind {
        RECOMPUTE_TEST_TREE_REQUESTED,
        ABANDON_TEST_TREE_REQUESTED,
        STIMULATE_REQUESTED,
        OBSERVE_REQUESTED
    };

    /**
     * The EventManager class for TraversalVizView.
     */
    public static class EventManager extends Observable {

        private final TreeExplorerView owner;

        /**
         * Set up the EventManager object.
         * 
         * @param owner
         *            The TraversalVizView object governing this EventManager.
         */
        public EventManager(TreeExplorerView owner) {
            this.owner = owner;
        }

        /**
         * Get the instance of TraversalVizView which generates the events
         * handled by this EventManager instance.
         * 
         * @return A TraversalVizView instance.
         */
        public TreeExplorerView getOwner() {
            return owner;
        }

        /**
         * Call observers for a RECOMPUTE_TEST_TREE_REQUESTED event.
         */
        public void recomputeTestTreeRequested() {
            setChanged();
            notifyObservers(EventKind.RECOMPUTE_TEST_TREE_REQUESTED);
        }

        /**
         * Call observers for a ABANDON_TEST_TREE event.
         */
        public void abandonTestTreeRequested() {
            setChanged();
            notifyObservers(EventKind.ABANDON_TEST_TREE_REQUESTED);
        }

        /**
         * Call observers for a STIMULATE_REQUESTED event.
         */
        public void stimulateRequested() {
            setChanged();
            notifyObservers(EventKind.STIMULATE_REQUESTED);
        }

        /**
         * Call observers for a OBSERVE_REQUESTED event.
         */
        public void observeRequested() {
            setChanged();
            notifyObservers(EventKind.OBSERVE_REQUESTED);
        }
    }

    /** A factory clas for {@link TreeExplorerView}. */
    public static class Factory {
        /**
         * Makes a new {@link TreeExplorerView} instance.
         * 
         * @param parent
         *            The parent widget.
         * @param testExecData
         *            An accessor to various information about the test run.
         * @return The new {@link TreeExplorerView} instance.
         */
        public TreeExplorerView makeInstance(Composite parent,
                TestExecutionDataAccessor testExecData) {
            return new TreeExplorerView(parent, testExecData);
        }
    }
}
