package utwente.fmt.jtorx.testgui.lazyotf.testrun;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities;

/**
 * A widget implementing the valuation explorer view. This view does not load
 * any data on its own; instead, it must be supplied with the data to be
 * displayed by using the <i>add()</i> and <i>clear()</i> methods.
 */
class ValuationExplorerView extends Composite {
    /** The table control displaying the valuation. */
    private final Table valuationTable;

    /** The "Value" column. */
    private final TableColumn valuationCol;

    /** The "Variable" column. */
    private final TableColumn variableCol;

    /** The "Location column. */
    private final TableColumn locationCol;

    /** The "Loc. ID" column. */
    private final TableColumn idCol;

    /** The location column number. */
    private static final int LOCATION_COL = 0;

    /** The location id column number. */
    private static final int LOCID_COL = 1;

    /** The variable name column number. */
    private static final int VARIABLE_COL = 2;

    /** The variable value column number. */
    private static final int VALUE_COL = 3;

    /**
     * Constructs a ValuationExplorerView object.
     * 
     * @param parent
     *            The view's parent control.
     */
    public ValuationExplorerView(Composite parent) {
        super(parent, SWT.NONE);

        valuationTable = new Table(this, SWT.BORDER | SWT.MULTI);
        setLayout(new FillLayout());
        this.layout();
        valuationTable.setHeaderVisible(true);
        int tableWidth = valuationTable.getClientArea().width;

        locationCol = new TableColumn(valuationTable, SWT.NONE);
        locationCol.setText("State");
        locationCol.setWidth(tableWidth / 4);

        idCol = new TableColumn(valuationTable, SWT.NONE);
        idCol.setText("State ID");
        idCol.setWidth(tableWidth / 4);

        variableCol = new TableColumn(valuationTable, SWT.NONE);
        variableCol.setText("Variable");
        variableCol.setWidth(tableWidth / 4);

        valuationCol = new TableColumn(valuationTable, SWT.NONE);
        valuationCol.setText("Valuation");
        valuationCol.setWidth(tableWidth / 4);

        locationCol.pack();
        idCol.pack();
        variableCol.pack();
        valuationCol.pack();

        valuationTable.setSortColumn(locationCol);
        valuationTable.setSortDirection(SWT.DOWN);
        valuationTable.redraw();

        SelectionAdapter sortListener = new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                TableColumn selectedCol = (TableColumn) ev.widget;

                if (selectedCol == valuationTable.getSortColumn()) {
                    if (valuationTable.getSortDirection() == SWT.UP) {
                        valuationTable.setSortDirection(SWT.DOWN);
                    }
                    else {
                        valuationTable.setSortDirection(SWT.UP);
                    }
                }
                else {
                    valuationTable.setSortDirection(SWT.DOWN);
                }

                valuationTable.setSortColumn(selectedCol);
                sortItems();
            }
        };

        locationCol.addSelectionListener(sortListener);
        idCol.addSelectionListener(sortListener);
        variableCol.addSelectionListener(sortListener);
        valuationCol.addSelectionListener(sortListener);
    }

    /** Remove all items from the table. */
    public void clear() {
        valuationTable.removeAll();
    }

    /**
     * Add a location-locationID-variable-value quadruple to the table.
     * 
     * @param location
     *            The location.
     * @param locid
     *            The location ID.
     * @param variable
     *            The variable name.
     * @param value
     *            The variable value.
     */
    public void add(String location, String locid, String variable, String value) {
        String[] text = { location, locid, variable, value };
        TableColumn currentSortCol = valuationTable.getSortColumn();
        int currentSortIdx = valuationTable.indexOf(currentSortCol);
        int insertionIdx = ViewUtilities.determineInsertionIndex(valuationTable.getItems(),
                text[currentSortIdx], currentSortIdx,
                (valuationTable.getSortDirection() == SWT.DOWN) ? true : false);

        TableItem it = new TableItem(valuationTable, SWT.NONE, insertionIdx);
        it.setData(text);
        it.setText(LOCATION_COL, location);
        it.setText(LOCID_COL, locid);
        it.setText(VARIABLE_COL, variable);
        it.setText(VALUE_COL, value);
        locationCol.pack();
        variableCol.pack();
        valuationCol.pack();
        valuationTable.redraw();
    }

    /**
     * Sort the items within the valuation table.
     */
    private void sortItems() {
        /* Gather table item data */
        List<String[]> items = new ArrayList<>();
        for (TableItem ti : valuationTable.getItems()) {
            items.add((String[]) ti.getData());
        }

        clear();

        for (String[] item : items) {
            add(item[0], item[1], item[2], item[3]);
        }
    }

    /** A factory class for {@link ValuationExplorerView}. */
    public static class Factory {
        /**
         * Makes a new {@link ValuationExplorerView} instance.
         * 
         * @param parent
         *            the instance's parent widget.
         * @return The new {@link ValuationExplorerView} instance.
         */
        public ValuationExplorerView makeInstance(Composite parent) {
            return new ValuationExplorerView(parent);
        }
    }
}
