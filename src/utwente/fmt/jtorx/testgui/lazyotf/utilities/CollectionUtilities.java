package utwente.fmt.jtorx.testgui.lazyotf.utilities;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

public final class CollectionUtilities {
    public static <T> Collection<T> unmodifiableUnionView(final Collection<T> c1,
            final Collection<T> c2) {
        return new AbstractCollection<T>() {
            @Override
            public Iterator<T> iterator() {

                final Iterator<T> iterator1 = c1.iterator();
                final Iterator<T> iterator2 = c2.iterator();

                return new Iterator<T>() {

                    @Override
                    public boolean hasNext() {
                        return iterator1.hasNext() || iterator2.hasNext();
                    }

                    @Override
                    public T next() {
                        if (iterator1.hasNext()) {
                            return iterator1.next();
                        }
                        else {
                            return iterator2.next();
                        }
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException(
                                "Cannot remove from a read-only iterator.");
                    }
                };
            }

            @Override
            public int size() {
                return c1.size() + c2.size();
            }
        };
    }

    /** Utility class - cannot instantiate. */
    private CollectionUtilities() {

    }
}
