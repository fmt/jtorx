package utwente.fmt.jtorx.testgui.lazyotf.utilities;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Sash;

/**
 * Utilities for setting up Sash layouts.
 */
public final class SashLayout {
    /**
     * Set up a vertically splitting Sash-based layout within
     * <tt>enclosingCtrl</tt>.
     * 
     * @param enclosingCtrl
     *            The widget containing the two widgets which are to be
     *            separated by a Sash.
     * @param left
     *            The left-hand child of <tt>enclosingCtrl</tt>.
     * @param right
     *            The right-hand child of <tt>enclosingCtrl</tt>.
     */
    public static void setupVerticallySplitting(final Composite enclosingCtrl, Composite left,
            Composite right) {
        Sash vSash = new Sash(enclosingCtrl, SWT.BORDER | SWT.VERTICAL);
        enclosingCtrl.setLayout(new FormLayout());

        FormData gSelFD = new FormData();
        gSelFD.left = new FormAttachment(0, 0);
        gSelFD.right = new FormAttachment(vSash, 0);
        gSelFD.top = new FormAttachment(0, 0);
        gSelFD.bottom = new FormAttachment(100, 0);
        left.setLayoutData(gSelFD);

        FormData gEditFD = new FormData();
        gEditFD.left = new FormAttachment(vSash, 0);
        gEditFD.right = new FormAttachment(100, 0);
        gEditFD.top = new FormAttachment(0, 0);
        gEditFD.bottom = new FormAttachment(100, 0);
        right.setLayoutData(gEditFD);

        final FormData sashFD = new FormData();
        sashFD.left = new FormAttachment(50, -100);
        sashFD.top = new FormAttachment(0, 0);
        sashFD.bottom = new FormAttachment(100, 0);
        vSash.setLayoutData(sashFD);

        vSash.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent ev) {
                sashFD.left = new FormAttachment(0, ev.x);
                enclosingCtrl.layout();
            }
        });

        enclosingCtrl.pack();
    }

    /** Utility class - cannot instantiate.*/
    private SashLayout() {
        
    }
}
