package utwente.fmt.jtorx.testgui.lazyotf.utilities;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import utwente.fmt.jtorx.lazyotf.location.InstantiatedLocation;
import utwente.fmt.lts.State;

public final class StateSetRepresentation {
    /**
     * Pretty-print a set of state names.
     * 
     * @param stateNames
     *            The state names to be pretty-printed.
     * @return A string containing a pretty representation of the given state
     *         names.
     */
    public static String makeStateSetRepresentation(final Set<InstantiatedLocation> stateNames) {
        String result = "{";
        for (final Iterator<InstantiatedLocation> it = stateNames.iterator(); it.hasNext();) {
            result += it.next().getLocation().getName();
            if (it.hasNext()) {
                result += ", ";
            }
        }
        return result + "}";
    }

    /**
     * Generate a string representation of the given compound state.
     * 
     * @param state
     *            The compound state to be represented as a String.
     * @param removeJTorXConvention
     *            If true, remove the leading "n" from the state names.
     * @return An according String.
     */
    public static String makeStateSetRepresentation(final Iterator<? extends State> state,
            final boolean removeJTorXConvention) {
        Set<String> includedNames = new HashSet<String>();
        Set<State> includedStates = new HashSet<State>();
        while (state.hasNext()) {
            final State st = state.next();
            if (!includedNames.contains(st.getNodeName())) {
                includedNames.add(st.getNodeName());
                includedStates.add(st);
            }
        }

        String result = "{";
        Iterator<State> stateIt = includedStates.iterator();
        while (stateIt.hasNext()) {
            final State st = stateIt.next();
            result += removeJTorXConvention ? pureNodeName(st) : st.getNodeName();

            if (stateIt.hasNext()) {
                result += ", ";
            }
        }
        return result + "}";
    }

    /**
     * @param s
     *            A state name.
     * @return The STSimulator state name.
     */
    public static String pureNodeName(final State s) {
        final String nname = s.getNodeName();
        return nname.length() > 1 ? nname.substring(1) : nname;
    }

    /** Utility class - cannot instantiate. */
    private StateSetRepresentation() {

    }
}
