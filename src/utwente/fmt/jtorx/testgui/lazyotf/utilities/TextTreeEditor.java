package utwente.fmt.jtorx.testgui.lazyotf.utilities;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import utwente.fmt.jtorx.testgui.lazyotf.utilities.ViewUtilities.GenericListener;

public class TextTreeEditor {
    final Tree context;
    final TreeEditor editor;
    final Set<GenericListener<String>> editModifyListeners;
    final Set<GenericListener<String>> editFinishedListeners;

    public TextTreeEditor(Tree context) {
        this.context = context;
        editor = new TreeEditor(context);
        editModifyListeners = new HashSet<>();
        editFinishedListeners = new HashSet<>();

        editor.horizontalAlignment = SWT.LEFT;
        editor.grabHorizontal = true;
        editor.minimumWidth = 100;
    }

    public void displayEditor(TreeItem editedItem, String initialText) {
        if (editor.getEditor() != null) {
            editor.getEditor().dispose();
        }

        final Text editorCtrl = new Text(context, SWT.NONE);
        editorCtrl.setText(initialText);
        editorCtrl.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent arg0) {
                for (GenericListener<String> listener : editModifyListeners) {
                    listener.onFiredEvent(editorCtrl.getText());
                }
            }
        });
        editorCtrl.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent arg0) {
            }

            @Override
            public void focusLost(FocusEvent arg0) {
                hideEditor();
            }
        });
        editorCtrl.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetDefaultSelected(SelectionEvent ev) {
                hideEditor();
            }
        });

        editorCtrl.selectAll();
        editorCtrl.setFocus();
        editor.setEditor(editorCtrl, editedItem);
    }

    public void hideEditor() {
        if (editor.getEditor() != null && !editor.getEditor().isDisposed()) {
            Text editorCtrl = (Text) editor.getEditor();
            for (GenericListener<String> listener : editFinishedListeners) {
                listener.onFiredEvent(editorCtrl.getText());
            }
            editorCtrl.dispose();
            editor.setEditor(null);
        }
    }

    public void addModifyListener(GenericListener<String> it) {
        editModifyListeners.add(it);
    }

    public void addEditFinishedListener(GenericListener<String> it) {
        editFinishedListeners.add(it);
    }

    public void removeAllModifyListeners() {
        editModifyListeners.clear();
    }

    public void removeAllEditFinishedListeners() {
        editFinishedListeners.clear();
    }
}
