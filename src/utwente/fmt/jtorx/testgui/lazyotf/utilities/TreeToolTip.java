package utwente.fmt.jtorx.testgui.lazyotf.utilities;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

/** An utility class for displaying SWT {@link TreeItem} tooltips. */
public final class TreeToolTip {
    /** An interface for determining {@link TreeItem} tooltips. */
    public interface ToolTipGenerator {
        /**
         * Gets the tooltip for the specified {@link TreeItem}.
         * 
         * @param ti
         *            The currently hovered-over {@link TreeItem}.
         * @param column
         *            The currently hovered-over {@link TreeColumn}.
         * @return The according tooltip or {@code null} if no tooltip should be
         *         displayed.
         */
        String getToolTip(TreeItem ti, TreeColumn column);
    }

    /**
     * Adds a tooltip handler to the given {@link Tree}.
     * 
     * @param widget
     *            The target {@link Tree}.
     * @param tipGen
     *            The {@link ToolTipGenerator} generating the tooltips.
     */
    public static void installToolTipHandler(final Tree widget, final ToolTipGenerator tipGen) {
        widget.addListener(SWT.MouseMove, new Listener() {
            @Override
            public void handleEvent(Event event) {
                Point mousePos = new Point(event.x, event.y);
                TreeItem ti = widget.getItem(mousePos);

                if (ti == null) {
                    widget.setToolTipText("");
                }
                else {
                    int hoverColIdx = -1;
                    for (int i = 0; i < widget.getColumnCount(); i++) {
                        Rectangle rect = ti.getBounds(i);
                        if (rect.contains(mousePos)) {
                            hoverColIdx = i;
                        }
                    }
                    if (hoverColIdx != -1) {
                        widget.setToolTipText(tipGen.getToolTip(ti, widget.getColumn(hoverColIdx)));
                    }
                }
            }
        });
    }

    /** Utility class - cannot be instantiated. */
    private TreeToolTip() {

    }
}
