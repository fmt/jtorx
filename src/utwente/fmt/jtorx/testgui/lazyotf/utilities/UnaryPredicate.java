package utwente.fmt.jtorx.testgui.lazyotf.utilities;

public interface UnaryPredicate<T> {
    boolean evaluate(T argument);
}
