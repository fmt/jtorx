package utwente.fmt.jtorx.testgui.lazyotf.utilities;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public final class ViewUtilities {

    public interface GenericListener<T> {
        public void onFiredEvent(T event);
    }

    public interface RenameListener<T> {
        public void onRename(T instance, String newName);
    }

    public interface CheckListener<T> {
        public void onSetChecked(T instance, boolean newValue);
    }

    public interface SetEnabledListener<T> {
        public void onSetEnabled(T instance, boolean newValue);
    }

    /* TODO: Use JTorX's own Pair class */
    public static class Pair<A, B> {
        A car;
        B cdr;

        public Pair(A car, B cdr) {
            this.car = car;
            this.cdr = cdr;
        }

        public A getCar() {
            return car;
        }

        public B getCdr() {
            return cdr;
        }
    }

    public static void sortTreeItemsByName(Tree context, boolean ascending, Set<String> dataKeys) {
        TreeItem[] origSelection = context.getSelection();
        int origSelectionIdx = 0;
        TreeItem[] items = context.getItems();

        Comparator<TreeItem> treeItemComparator = new Comparator<TreeItem>() {
            @Override
            public int compare(TreeItem arg0, TreeItem arg1) {
                return arg0.getText().compareTo(arg1.getText());
            }
        };

        Arrays.sort(items, treeItemComparator);
        Arrays.sort(origSelection, treeItemComparator);

        context.setRedraw(false);

        TreeItem[] orderedSelection = new TreeItem[origSelection.length];
        for (TreeItem ti : items) {
            TreeItem orderedInsertTI = new TreeItem(context, ti.getStyle());
            orderedInsertTI.setText(ti.getText());
            orderedInsertTI.setBackground(ti.getBackground());
            orderedInsertTI.setChecked(ti.getChecked());
            orderedInsertTI.setData(ti.getData());
            orderedInsertTI.setFont(ti.getFont());
            orderedInsertTI.setForeground(ti.getForeground());
            orderedInsertTI.setGrayed(ti.getGrayed());
            orderedInsertTI.setImage(ti.getImage());
            for (String key : dataKeys) {
                orderedInsertTI.setData(key, ti.getData(key));
            }
            for (Listener l : ti.getListeners(SWT.Dispose)) {
                orderedInsertTI.addListener(SWT.Dispose, l);
            }

            if (origSelectionIdx < origSelection.length &&
                    origSelection[origSelectionIdx] == ti) {
                assert (origSelectionIdx < origSelection.length);
                orderedSelection[origSelectionIdx] = orderedInsertTI;
                origSelectionIdx++;
            }

            ti.dispose();
        }

        assert origSelectionIdx == origSelection.length;
        context.setSelection(orderedSelection);

        context.setRedraw(true);
    }

    public static int determineInsertionIndex(TreeItem parent, String name) {
        /* Binary search on parent's children */
        TreeItem[] children = parent.getItems();
        return determineInsertionIndex(children, name);
    }

    public static int determineInsertionIndex(final TreeItem[] items, String name) {
        return binSearch(new BinSearchAccessor<String>() {

            @Override
            public String getData(int idx) {
                return items[idx].getText();
            }

            @Override
            public int getLength() {
                return items.length;
            }

            @Override
            public int compare(String first, String second) {
                return first.compareTo(second);
            }

        }, name);
    }

    public static int determineInsertionIndex(final TableItem[] items, String name, final int col,
            final boolean ascending) {
        return binSearch(new BinSearchAccessor<String>() {

            @Override
            public String getData(int idx) {
                return items[idx].getText(col);
            }

            @Override
            public int getLength() {
                return items.length;
            }

            @Override
            public int compare(String first, String second) {
                return (ascending ? 1 : -1) * first.compareTo(second);
            }

        }, name);
    }

    public static int determineInsertionIndex(final String[] items, String name) {
        return binSearch(new BinSearchAccessor<String>() {
            @Override
            public String getData(int idx) {
                return items[idx];
            }

            @Override
            public int getLength() {
                return items.length;
            }

            @Override
            public int compare(String first, String second) {
                return first.compareTo(second);
            }
        }, name);
    }

    public static <T> int binSearch(BinSearchAccessor<T> items, T query) {
        /* Binary search */

        int idx = items.getLength() / 2;
        int searchRegionStart = 0;
        int searchRegionEnd = items.getLength();

        while (searchRegionStart != searchRegionEnd) {
            int direction = items.compare(items.getData(idx), query);
            if (direction > 0) {
                /* array item is larger */
                searchRegionEnd = idx;
            }
            else if (direction < 0) {
                /* array item is smaller */
                searchRegionStart = idx + 1;
            }
            else {
                return idx;
            }
            idx = (searchRegionEnd + searchRegionStart) / 2;
        }

        return idx;
    }

    public interface BinSearchAccessor<T> {
        T getData(int idx);

        int getLength();

        int compare(T first, T second);
    };

    /** Utility class - cannot instantiate. */
    private ViewUtilities() {
        
    }
}
