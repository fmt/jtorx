package utwente.fmt.jtorx.testnongui;

import java.util.HashSet;

import utwente.fmt.jtorx.lazyotf.LazyOTF.InactiveException;
import utwente.fmt.jtorx.lazyotf.guidance.NodeValue2Weight;
import utwente.fmt.jtorx.lazyotf.location.Location;
import utwente.fmt.jtorx.lazyotf.location.TestType;
import utwente.fmt.jtorx.lazyotf.policy.Defaults;
import utwente.fmt.jtorx.lazyotf.remoteimpl.StandaloneLazyOTF;
import utwente.fmt.jtorx.lazyotf.remoteimpl.StandaloneLazyOTF.ModelNotFoundException;
import utwente.fmt.jtorx.testgui.lazyotf.config.XMLConfigurationHandler;
import utwente.fmt.jtorx.testgui.lazyotf.config.XMLConfigurationHandler.IllegalSemanticsException;
import utwente.fmt.jtorx.testgui.lazyotf.config.XMLConfigurationHandler.ParseException;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;

/**
 * An XML-LazyOTF-configuration -> LazyOTFConfigSessionData converter using
 * {@link XMLConfigurationHandler}.
 * 
 * When no longer used, call <i>quit()</i> in order to clean up external
 * resources used by this converter. 
 */
public class LazyOTFConfigLoader {
	/** A standalone LazyOTF instance used for verifying the configuration. */
	//private final StandaloneLazyOTF standaloneInst;

	/**
	 * Constructs a new {@link LazyOTFConfigLoader} instance.
	 */
	public LazyOTFConfigLoader() {
		//standaloneInst = new StandaloneLazyOTF(new LazyOTFPolicyFactoryImpl(new LazyOTFConfig()));
	}

	/** 
	 * Constructs a new {@link LazyOTFConfigLoader} instance using the
	 * given {@link StandaloneLazyOTF} instance to verify configurations.
	 */
	public LazyOTFConfigLoader(StandaloneLazyOTF standaloneInst) {
		//this.standaloneInst = standaloneInst;
	}

	/**
	 * Reads a LazyOTF configuration.
	 * @param modelName The filename of the model with which the configuration
	 *   is associated.
	 * @param lazyOTFXMLConfig The LazyOTF XML configuration string.
	 * @return
	 * @throws ModelNotFoundException when the given model file does not exist.
	 * @throws ParseException when the configuration contains syntax errors.
	 * @throws InactiveException when the standalone LazyOTF instance refuses to
	 *   work.
	 * @throws IllegalSemanticsException when the configuration contains semantic
	 *   errors (e.g. refers to inexisting locations, has bad references...)
	 */
	public LazyOTFConfigSessionData loadConfig(String modelName, String lazyOTFXMLConfig) throws ModelNotFoundException, ParseException, InactiveException, IllegalSemanticsException {
		//standaloneInst.resetAndRead(modelName);
		XMLConfigurationHandler confHandler = new XMLConfigurationHandler(new HashSet<Location>());
		confHandler.setAutoCompleteConfigurationOptions(true);
		confHandler.readConfigFromString(lazyOTFXMLConfig);

		LazyOTFConfigSessionData cfgSessionData;
		//LazyOTFConfigurationOptions cfgOptions = standaloneInst.getLazyOTFInstance().getConfigurationOptions();
		cfgSessionData = confHandler.getConfiguration();

		cfgSessionData.setTestTypes(TestType.ALL_TEST_TYPES);

		NodeValue2Weight def = Defaults.defaultNodeValue2Weight();
		cfgSessionData.getCustomNodeValue2Weights().add(def);

		//TODO: validate() can't deal with missing config options yet
		//cfgSessionData.validate();
		
		return cfgSessionData;
	}

	/**
	 * Frees any external resources associated with this converter.
	 */
	public void quit() {
		//standaloneInst.quit();
	}
}
