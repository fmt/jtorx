package utwente.fmt.jtorx.testnongui;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import utwente.fmt.jtorx.lazyotf.LazyOTF;
import utwente.fmt.jtorx.lazyotf.LazyOTF.InactiveException;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFObservatory;
import utwente.fmt.jtorx.lazyotf.remoteimpl.StandaloneLazyOTF.ModelNotFoundException;
import utwente.fmt.jtorx.testgui.Config;
import utwente.fmt.jtorx.testgui.lazyotf.config.ConfigSessionData2LazyOTFConfig;
import utwente.fmt.jtorx.testgui.lazyotf.config.XMLConfigurationHandler.IllegalSemanticsException;
import utwente.fmt.jtorx.testgui.lazyotf.config.XMLConfigurationHandler.ParseException;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData;
import utwente.fmt.jtorx.testgui.lazyotf.config.shared.LazyOTFConfigSessionData.ValidationException;
import utwente.fmt.jtorx.ui.ErrorReporter;

/**
 * A controller for the LazyOTF subsystem. This class is used by {@link Main}
 * to load the configuration and to attach the {@link LazyOTFObservatory} to
 * the LazyOTF instance.
 */
public class LazyOTFCtrl {
	/** LazyOTF can only be used if it has a valid configuration. This flag
	 * is <tt>true</tt> iff such a configuration (i.e. carrying a LazyOTF
	 * configuration) has been loaded.
	 */
	private boolean isConfigured = false;

	/** The loaded LazyOTF configuration. If <i>isConfigured</i> is
	 * <tt>false</tt>, this reference is <tt>null</tt>.
	 */
	private LazyOTFConfig cfg;

	/** The LazyOTF-enabled driver. */
	private LazyOTFAwareSimOnlineTestingDriver driver;

	/** The LazyOTF log filename. If <tt>""</tt> or <tt>null</tt>, no log
	 * gets recorded by LazyOTF. */
	private String logFileName;

	/**
	 * @return the LazyOTF configuration.
	 * @throws {@link IllegalStateException} if no configuration has been loaded yet. 
	 */
	public LazyOTFConfig getConfiguration() {
		if(!isConfigured) {
			throw new IllegalStateException("Cannot get a LazyOTF configuration from an unconfigured LazyOTF controller.");
		}
		return cfg;
	}

	/**
	 * Loads the LazyOTF configuration from the given JTorX config. If no
	 * LazyOTF configuration is present within, this method does nothing.
	 * @param it The JTorX configuration object.
	 * @param err The JTorX error reporter.
	 */
	public void loadConfig(Config it, ErrorReporter err) {
	    System.out.println("Loading configuration...");
		if(it.getString("LAZYOTF") != null && it.getString("LAZYOTF").equals("true")) {
			if(it.getString("MODELTEXT") != null && it.getString("LAZYOTFCFG") != null) {
				LazyOTFConfigLoader loader = new LazyOTFConfigLoader();
				LazyOTFConfigSessionData highLevelConfig = null;
				try {
					highLevelConfig = loader.loadConfig(it.getString("MODELTEXT"), it.getString("LAZYOTFCFG"));
				} catch (InactiveException | ModelNotFoundException
						| ParseException | IllegalSemanticsException | ValidationException e) {
					err.report("lazyotf: Could not activate LazyOTF because of an error:");
					System.err.println(e);
					return;
				}
				finally {
					loader.quit();
				}
				cfg = ConfigSessionData2LazyOTFConfig.makeLazyOTFConfig(highLevelConfig);
				
				logFileName = highLevelConfig.getLogFilename();
				isConfigured = true;
			}
			else {
				if(it.getString("MODELTEXT") == null) {
					err.report("lazyotf: fatal: Missing model filename in config.");
				}
				else {
					err.report("lazyotf: fatal: Missing lazyotf configuration in config.");
				}
			}
		}
	}

	/**
	 * Clears the LazyOTF configuration from this controller.
	 */
	public void clearConfig() {
		isConfigured = false;
	}

	/**
	 * @return <tt>true</tt> iff a valid LazyOTF configuration has been loaded.
	 */
	public boolean isConfigured() {
		return isConfigured;
	}

	/**
	 * Sets the driver. If a log file was specified by the LazyOTF
	 * configuration, this method installs a {@link LazyOTFObservatory} printing
	 * its output to that file.
	 * @param it The LazyOTF-aware JTorX driver.
	 * @param lazyOTF The LazyOTF instance.
	 */
	public void setDriver(LazyOTFAwareSimOnlineTestingDriver it, LazyOTF lazyOTF)  {
		driver = it;
		if(logFileName != null && !logFileName.equals("")) {
			File observerFile = new File(logFileName);
			try {
				observerFile.createNewFile();
				new LazyOTFObservatory(it, lazyOTF, new PrintStream(observerFile));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Informs the LazyOTF controller that the test run is about to begin.
	 */
	public void runBegins() {
		if(isConfigured && driver == null) {
			throw new IllegalStateException("Needs a LazyOTF-enabled driver to begin run");
		}
	}

	/**
	 * Informs the LazyOTF controller that the test run ended.
	 */
	public void runEnds() {

	}
	
	public LazyOTFCtrl() {
		
	}
}
