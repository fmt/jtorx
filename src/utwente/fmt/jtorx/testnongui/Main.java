package utwente.fmt.jtorx.testnongui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.Version;
import utwente.fmt.engine.Testrun.RunState;
import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.InterpKind;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.engine.Engine;
import utwente.fmt.jtorx.interpretation.ActionListInterpretation;
import utwente.fmt.jtorx.interpretation.PostfixDeltaInterpretation;
import utwente.fmt.jtorx.interpretation.PostfixInterpretation;
import utwente.fmt.jtorx.interpretation.PrefixDeltaInterpretation;
import utwente.fmt.jtorx.interpretation.PrefixInterpretation;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.casestudy.CaseStudyOnLineTestingDriverWrapper;
import utwente.fmt.jtorx.lazyotf.driver.LazyOTFAwareSimOnlineTestingDriver;
import utwente.fmt.jtorx.logger.coverage.CoverageData;
import utwente.fmt.jtorx.logger.logfile.LogWriter;
import utwente.fmt.jtorx.logger.mux.LogMux;
import utwente.fmt.jtorx.logger.mux.LogServices;
import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.testgui.Config;
import utwente.fmt.jtorx.testgui.RunItemData;
import utwente.fmt.jtorx.testgui.RunItemData.AdapterKind;
import utwente.fmt.jtorx.testgui.RunSessionData;
import utwente.fmt.jtorx.testgui.StateStatHandler;
import utwente.fmt.jtorx.testnongui.casestudy.CaseStudyIO;
import utwente.fmt.jtorx.testnongui.casestudy.CaseStudyMainWrapper;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.torx.DriverInterActionResultConsumer;
import utwente.fmt.jtorx.torx.InterActionResult;
import utwente.fmt.jtorx.torx.LabelPlusVerdict;
import utwente.fmt.jtorx.torx.OnLineTestingDriver;
import utwente.fmt.jtorx.torx.driver.VerdictImpl;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.ui.StepHighlighter;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.test.Verdict;

public class Main {
	
	public static final String SYSPROP_MAX_SHUTDOWN_WAIT_MS = "testnongui.maxShutdownWait";
	public static final long DEFAULT_MAX_SHUTDOWN_WAIT_MS=60000l;
	private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

	// ============================================
	private String formatLabel(String kind, String p, String s, String c) {
		String chan = null;
		String pco = p;
		String lbl = s;
		String chanPco = "";
		String cstrnt = "";
		if (s.equalsIgnoreCase("delta"))
			lbl = "(Quiescence)";
		if (kind.equals("input"))
			chan = "in";
		else if (kind.equals("output") || kind.equals("expected"))
			chan = "out";
		if ((kind.equals("input") || kind.equals("output")) && pco == null && !s.equals("delta")) {
			if (s.length() > 3)
				pco = s.substring(0, 3);
			else
				pco = s;
		}
		if (c != null && !c.trim().equals(""))
			cstrnt = " [" + c + "]";
		if (chan != null)
			chanPco += chan;
		if (pco != null && !pco.equals(""))
			chanPco += ":" + pco;
		if (chan != null || pco != null)
			chanPco = "(" + chanPco + ")";
		return kind + chanPco + ": " + lbl + cstrnt;
	}

	public class Testrun implements utwente.fmt.engine.Testrun, utwente.fmt.configuration.Testrun {

		private void loadConfig(RunSessionData d, String fname, boolean loadingLogForReplay) {
			if (fname == null || fname.trim().equals(""))
				return;
			if (d == null)
				return;

			FileInputStream fin;
			try {
				fin = new FileInputStream(fname);
				BufferedReader r = new BufferedReader(new InputStreamReader(fin));
				cfg.read(r);
				fin.close();
				if (loadingLogForReplay) { // tweak log settings to use log as
					// SUT
					cfg.add("ADAPTERKIND", AdapterKind.LOGADAPTER);
					cfg.add("IMPLTEXT", fname);
					// should we disable things like impl instantiator?
				}
				addStatusMsg("read configuration from: " + fname);
				lazyOTF.loadConfig(cfg, err);
			}
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				addStatusMsg("cannot read configuration: " + e.getMessage());
				e.printStackTrace();
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				addStatusMsg("cannot read configuration: " + e.getMessage());
				e.printStackTrace();
			}
		}

		// ====================================
		// To implement interface engine.Testrun

		public void addStatusMsg(String s) {
			System.err.println("Info: " + s);
		}

		public ProgressReporter getStateSpaceProgressReporter() {
			return null;
		}

		public void runSetup() {
		}

		public long parseSeed(String s, String err) {
			long n = 0;
			if (!s.trim().equals("")) {
				try {
					n = Long.parseLong(s);
				}
				catch (NumberFormatException nfe) {
					addStatusMsg(err + ": " + s);
				}
			}
			return n;

		}

		public long initSeed(String s) {
			long n = 0;
			if (!s.trim().equals("")) {
				try {
					n = Long.parseLong(s);
				}
				catch (NumberFormatException nfe) {
					addStatusMsg("seed is not a number: " + s);
				}
			}
			if (n == 0) {
				Random rand = new Random();
				n = rand.nextInt();
				if (n < 0)
					n = -n;
			}
			return n;
		}

		public long initSimSeed(String s, long rs) {
			long n = 0;
			if (!s.trim().equals("")) {
				try {
					n = Long.parseLong(s);
				}
				catch (NumberFormatException nfe) {
					addStatusMsg("sim seed is not a number: " + s);
				}
			}
			if (n == 0) {
				n = rs;
			}
			return n;
		}

		public void prepareLogMuxForSession() {
			logMux.prepareForSession();
		}

		public void startLogMuxSession(RunSessionData rsd) {
			logMux.startSession(rsd, null);
		}

		public void abortTestrun() {
			System.err.println("== ABORTING TESTRUN ==");
		}

		// called from non-gui thread

		public void runBegins() {
			System.err.println("== TESTRUN STARTS { ++");
			if (lazyOTF.isConfigured()) {
				lazyOTF.setDriver((LazyOTFAwareSimOnlineTestingDriver) runState.myDriver,
						runData.getLazyOTFInstance());
				lazyOTF.runBegins();
			}
		}

		public void stopTestrun() {
			if (runState.myDriver != null /* && teststepPane.isAutoRunning() */) {
				runState.myDriver.stopAuto();
				// no need to invoke the following here:
				// teststepPane.resetAutoRunning();
				// it will be invoked by
				// AutoConsumer.end()
			}
			System.err.println("stopTestRun(): done myDriver.stopAuto();");

			if (runState.myDriver != null) {
				DriverFinalizationResult f = runState.myDriver.finish();
				System.err.println("stopTestRun(): done myDriver.finish();");
				runData.setFinResult(f);
				System.err.println("stopTestRun(): done runData.setFinResult(f);");
				if (runState.verdict == null) {
					runState.verdict = f.getVerdict();
					System.err.println("stopTestRun(): done f.getVerdict();");
				}
			}
			System.err.println("stopTestRun(): about to test if verdict==null");
			if (runState.verdict == null)
				runState.verdict = new VerdictImpl("error");
			cfgRun.showVerdict(runState.verdict);

			System.err.println("stopTestRun(): done testrunPane.setVerdict(verdict="
					+ runState.verdict.getString() + ");");

			logMux.stopSession();

			runState.myDriver = null;

			System.err.println("stopTestRun(): done logMux.stopSession();");

			if (lazyOTF.isConfigured()) {
				lazyOTF.runEnds();
			}

			System.err.println("== TESTRUN ENDS } ++");
		}

		public void addLogger(final boolean inWindow, final String t) {
		}

		public void addCoverageLogger(CoverageData coverage) {
			// TODO need to do something for coverage,
			// because without doing something,
			// coverage numbers will not be counted
			logMux.addAnimator(coverage);
		}

		public void showMenu() {
			printModelMenu(true);
		}

		public void showVerdict(final Verdict verdict) {
			CaseStudyIO.err.println("Verdict: " + verdict.getString());
		}

		public void showCoverage(final CoverageData c) {
			System.err.println("Coverage: " + c.getReport());
		}

		// ====================================
		// To implement interface configuration.Testrun
		public IOLTSInterpretation getInterpretation() {
			InterpKind k = getInterpKind();
			if (getReconDelta()) {
				if (k == InterpKind.POSTFIX)
					return new PostfixDeltaInterpretation();
				else if (k == InterpKind.PREFIX)
					return new PrefixDeltaInterpretation();
				else if (k == InterpKind.ACTIONLIST)
					return new ActionListInterpretation(getInterpInputs(), getInterpOutputs());
				else
					return null;
			}
			else {
				if (k == InterpKind.POSTFIX)
					return new PostfixInterpretation();
				else if (k == InterpKind.PREFIX)
					return new PrefixInterpretation();
				else if (k == InterpKind.ACTIONLIST)
					return new ActionListInterpretation(getInterpInputs(), getInterpOutputs());
				else
					return null;
			}
		}

		public boolean useGuide() {
			return cfg.getBool("USEGUIDE");
		}

		public boolean useCoverage() {
			return cfg.getBool("USECOVERAGE");
		}

		public boolean useModelInstantiator() {
			return cfg.getBool("USEMODELINST");
		}

		public String getInstModelFilename() {
			return cfg.getString("INSTMODELTEXT");
		}

		public boolean useImplInstantiator() {
			return cfg.getBool("USEIMPLINST");
		}

		public String getInstImplFilename() {
			return cfg.getString("INSTIMPLTEXT");
		}

		public String getModelFilename() {
			return cfg.getString("MODELTEXT");
		}

		public String getGuideFilename() {
			return cfg.getString("GUIDETEXT");
		}

		public String getImplFilename() {
			return cfg.getString("IMPLTEXT");
		}

		public RunItemData.LTSKind getGuideKind() {
			return cfg.getLTSKind("GUIDEKIND");
		}

		public RunItemData.AdapterKind getImplKind() {
			return cfg.getAdapterKind("ADAPTERKIND");
		}

		public TraceKind getTraceKind() {
			return cfg.getTraceKind("TRACEKIND");
		}

		public int getTimeoutValue() {
			return cfg.getInt("TIMEOUTVAL");
		}

		public TimeUnit getTimeoutUnit() {
			return cfg.getTimeUnit("TIMEOUTUNIT");
		}

		public LTS getSelectedLTS() {
			return null;
		}

		public LTS getCombinedLTS() {
			return null;
		}

		public boolean useImplRealTime() {
			return cfg.getBool("USEREALTIME");
		}

		public boolean getStripAddLabelAnno() {
			return cfg.getBool("ADDRMLABELANNO");
		}

		public InterpKind getInterpKind() {
			return cfg.getInterpKind("INTERPKIND");
		}

		public boolean getReconDelta() {
			return cfg.getBool("RECONDELTA");
		}

		public boolean getSynthDelta() {
			return cfg.getBool("SYNTHDELTA");
		}

		public boolean getAngelicCompletion() {
			return cfg.getBool("ANGELCOMPL");
		}

		public boolean getDivergence() {
			return cfg.getBool("DIVERGENCE");
		}

		public String getInterpInputs() {
			return cfg.getString("INTERPINPUTS");
		}

		public String getInterpOutputs() {
			return cfg.getString("INTERPOUTPUTS");
		}

		public String getSeed() {
			return cfg.getString("SEED");
		}

		public void setSeed(String s) {
			cfg.add("SEED", s);
		}

		public String getSimSeed() {
			return cfg.getString("SIMSEED");
		}

		public void setSimSeed(String s) {
			cfg.add("SIMSEED", s);
		}

		public boolean vizModel() {
			return false;
		}

		public boolean vizMsc() {
			return false;
		}

		public boolean vizRun() {
			return false;
		}

		public boolean vizLogPane() {
			return false;
		}

		public boolean vizSuspAutom() {
			return false;
		}

		public boolean vizGuide() {
			return false;
		}

		public boolean vizCoverage() { // TODO: is this what we really want???
				return cfg.getBool("VIZCOVERAGE");
		}

		public boolean vizImpl() {
			return false;
		}

		// ========= constructor and private data
		public Testrun() {
		}

		private Config cfg = new Config();
		private LazyOTFCtrl lazyOTF = new LazyOTFCtrl();

		@Override
		public boolean isLazyOTFEnabled() {
			return lazyOTF.isConfigured();
		}

		@Override
		public LazyOTFConfig getLazyOTFConfig() {
			return lazyOTF.getConfiguration();
		}

	}

	private void printModelMenu(boolean enableButtons) {
		// need check in next line, because we unset myDriver at end of test run
		// (in stopTestrun),
		// and this method may be called (much later) via
		// handleAutoRunStartEvent.end()
		if (runState.myDriver == null)
			return;
		Iterator<Label> it;
		it = runState.myDriver.getModelInputs().iterator();
		Boolean haveInputs = it.hasNext();
		while (it.hasNext()) {
			Label l = it.next();
			String s = formatLabel("input", "", l.getString(),
					l.getConstraint() != null ? l.getConstraint().getString() : null);
			menu.addInput(s, l);
		}
		menu.finalizeInputs();

		Boolean haveOutputs;
		Verdict pv = runState.myModel.getPositiveDefaultVerdict();
		String pvs = (pv != null ? "(" + pv.getString() + ")" : "");
		Collection<LabelPlusVerdict> lvcol = runState.myDriver.getModelOutputVerdicts();
		// TODO change the code below such that we assume that
		// defining the LabelPlusVerdict iterator will always succeed
		// if lvcol is non-null?
		Iterator<LabelPlusVerdict> lvit = null;
		if (lvcol != null)
			lvit = lvcol.iterator();
		if (lvit != null) {
			haveOutputs = lvit.hasNext();
			while (lvit.hasNext()) {
				LabelPlusVerdict lv = lvit.next();
				Label l = lv.getLabel();
				Verdict v = lv.getVerdict();
				if (v != null)
					menu.addOutput(formatLabel("output", "", l.getString(),
							l.getConstraint() != null ? l.getConstraint().getString() : null),
							v.getString());
				else
					menu.addOutput(formatLabel("output", "", l.getString(),
							l.getConstraint() != null ? l.getConstraint().getString() : null), pvs);
			}
		}
		else {
			it = runState.myDriver.getModelOutputs().iterator();
			haveOutputs = it.hasNext();
			while (it.hasNext()) {
				Label l = it.next();
				menu.addOutput(formatLabel("output", "", l.getString(),
						l.getConstraint() != null ? l.getConstraint().getString() : null), pvs);
			}
		}
		menu.finalizeOutputs();

		Verdict nv = runState.myModel.getNegativeDefaultVerdict();
		if (nv != null)
			menu.addOutput("*", nv.getString());

		// if (enableButtons && runState.verdict == null) {
		// teststepPane.enableStimObs(haveInputs, haveOutputs);
		// } else {
		// teststepPane.enableStimObs(false, false);
		// }
	}

	public Thread startAutoRun(final int inc) {
		class AutoConsumer implements DriverInterActionResultConsumer {
			private Boolean goOn;

			public boolean consume(final DriverInterActionResult ll) {
				if (debug)
					System.err.println("AutoConsumer: consume: " + ll);
				goOn = false;
				goOn = handleInterAction(ll);
				if (goOn) {
					// if (teststepPane.getAutoPrintModelMenu())
					// printModelMenu(false);
					// else
					// clearModelMenu(); // OPTIMIZABLE: not needed if already
					// cleared
				}
				return goOn;
			}

			public void end() {
				try {
					// printModelMenu(true);
				}
				catch (Exception e) {
					System.err.println("caught exception in handleAutoRunStartEvent.end(): "
							+ e.getMessage());
				}
			}
		}

		Thread res = new Thread("Testgui-handleAutoRunStartEvent") {
			public void run() {
				OnLineTestingDriver drv;
				if (System.getProperty(CaseStudyMainWrapper.SYSPROP_USEDRIVERWRAPPER) != null) {
					drv = new CaseStudyOnLineTestingDriverWrapper(runState.myDriver);
				}
				else {
					drv = runState.myDriver;
				}
				drv.autoStep(inc, new AutoConsumer());
			}
		};
		res.start();
		return res;
	}

	private String formatInterAction(DriverInterActionResult l, String p) {
		String kind = "";
		if (l.getKind() == InterActionResult.Kind.INPUT)
			kind = "input";
		if (l.getKind() == InterActionResult.Kind.OUTPUT)
			kind = "output";
		return formatLabel(kind, p, l.getLabel().getString(),
				l.getLabel().getConstraint() != null ? l.getLabel().getConstraint().getString()
						: null);
	}

	private Boolean handleInterAction(DriverInterActionResult ll) {
		// TODO better fix than this sympton fixing
		if (ll == null) {
			System.out.println("oops handleInterAction: DriverInterActionResult==null");
			cfgRun.stopTestrun();
			return false;
		}

		stateStats.step(ll);

		cfgRun.addStatusMsg(ll.getStepNr() + " " + formatInterAction(ll, null));

		logMux.addInterActionResult(ll, null, null);

		if (ll.hasVerdict()) {
			runState.verdict = ll.getVerdict();
			cfgRun.showVerdict(runState.verdict);
			if (runState.verdict.isFail()) {
				// menuPane.prepareToShowFailure();
				// printModelMenu(false);
				System.out.println("spec disagrees with iut");
				Iterator<Label> exp = runState.myDriver.getModelOutputs().iterator();
				if (!exp.hasNext())
					System.out.println("(oops internal error? no outputs, no delta)");
				else {
					System.out.println("outputs expected by spec:");
					while (exp.hasNext()) {
						Label l = exp.next();
						System.out.println(formatLabel("expected", null, l.getString(),
								l.getConstraint() != null ? l.getConstraint().getString() : null));
					}
					System.out.println("(end of expected outputs)");
				}
				System.out.println("state according to spec:");
				System.out.println("(end of state)");
			}
			cfgRun.stopTestrun();
			return false;
		}
		return true;
	}
	
	   
    private static void setupLogging(Level level) {
        Logger root = Logger.getLogger("");
        root.setLevel(Level.ALL);
        for (Handler handler : root.getHandlers()) {
            if (handler instanceof ConsoleHandler) {
                handler.setLevel(level);
            }
        }
    }

	public static void main(String[] args) {
	    
	    
		if (args.length > 0 && args[0].equals("-V")) {
			System.err.println("JTorX version " + Version.getVersionAndDate());
			System.exit(0);
		}

		int j = 0;
		String logFile = null;
		String seed = null;
		String simSeed = null;
		String cfgFile = null;
		String countString = null;
		int countNumber = 0;
		boolean rerunLogFile = false;
		while (j + 1 < args.length) {
			if (args[j].equals("-c")) {
				countString = args[j + 1];
				countNumber = Integer.parseInt(countString);
				j += 2;
			}
			else if (args[j].equals("-l")) {
				logFile = args[j + 1];
				j += 2;
			}
			else if (args[j].equals("-r")) {
				rerunLogFile = true;
				j += 1;
			}
			else if (args[j].equals("-s")) {
				seed = args[j + 1];
				j += 2;
			}
			else if (args[j].equals("-S")) {
				simSeed = args[j + 1];
				j += 2;
			}
			else {
				System.err
				.println("usage: jtorxcmd [-c count] [-l logFile] [-r] [-s seed] [-S simseed] configFile");
				System.err.println("usage: jtorxcmd -V");
				System.exit(1);
			}
		}
		if (j + 1 == args.length) {
			cfgFile = args[j];
			Main m = new Main(cfgFile, countNumber, logFile, rerunLogFile, seed, simSeed);
		}
		else {
			System.err
			.println("usage: jtorxcmd [-c count] [-l logFile] [-r] [-s seed] [-S simseed] configFile");
			System.err.println("usage: jtorxcmd -V");
			System.exit(1);
		}

	}

	public Main(String configFile, int count, String logFile, boolean reRunLog, String seed,
			String simSeed) {
		cfgRun = new Testrun();

		// logServices = new LogServices(err);
		// logServices.start();

		logMux = new LogMux(err, null);
		logMuxList.add(logMux);

		Appendable lf = System.out;
		BufferedWriter bw = null;
		if (logFile != null) {
			try {
				FileOutputStream fout = new FileOutputStream(logFile);
				bw = new BufferedWriter(new OutputStreamWriter(fout));
				lf = bw;
			}
			catch (FileNotFoundException e) {
				cfgRun.addStatusMsg("cannot open logfile for writing: " + e.getMessage());
			}
			catch (IOException e) {
				cfgRun.addStatusMsg("cannot open logfile for writing: " + e.getMessage());
			}
		}
		LogWriter logWriter = new LogWriter(lf);
		logMux.addAnimator(logWriter);

		cfgRun.loadConfig(runData, configFile, reRunLog);

		if (seed != null) {
			cfgRun.setSeed(seed);
		}
		if (simSeed != null) {
			cfgRun.setSimSeed(simSeed);
		}

		/* SymToSim.jar needs to know whether LazyOTF is used or not */
		modelData.setUsingLazyOTF(cfgRun.isLazyOTFEnabled());
		implData.setUsingLazyOTF(cfgRun.isLazyOTFEnabled());

		Engine e = new Engine();
		e.startTestRun(cfgRun, cfgRun, runState,
				runData, modelData, modelPrimerData, guideData, guidePrimerData, combinatorData,
				implData,
				err, progress, stateStats, highlighter);

		try {
			runState.testStartThread.join();
		}
		catch (InterruptedException e2) {
			System.err.println("Internal error when joining Engine thread: " + e2.toString());
		}

		Thread r = startAutoRun(count);
		try {
			r.join();
		}
		catch (InterruptedException e1) {
			System.err.println("Internal error when joining auto run thread: " + e1.toString());
		}
		
        Thread wd = new Thread(new Runnable() {        	
            public void run() {
            	long maxWaitBeforeShutdown = DEFAULT_MAX_SHUTDOWN_WAIT_MS;
            	
            	String userMaxWaitBeforeShutdown = System.getProperty(SYSPROP_MAX_SHUTDOWN_WAIT_MS);
            	if (userMaxWaitBeforeShutdown != null) {
            		try {
            			maxWaitBeforeShutdown = Long.parseLong(userMaxWaitBeforeShutdown);
            		}
            		catch (NumberFormatException e) {
            			LOGGER.severe("Could not parse property " + SYSPROP_MAX_SHUTDOWN_WAIT_MS
            					+ "=" +userMaxWaitBeforeShutdown + ":\n" + e + "Using default value.");
            		}
            	}
            	
            	LOGGER.info("Waiting max. " + maxWaitBeforeShutdown + "ms before killing the JVM.");
                try {
                    Thread.sleep(maxWaitBeforeShutdown);
                }
                catch (InterruptedException e) {
                    // gets interrupted when main() is about to exit.
                    System.err.println("Regular exit from main()");
                    return;
                }
                LOGGER.info("Finished sleeping before forcefully killing the JVM.");
                System.err.println("Warning: irregular exit, check log");
                System.exit(1);
            }
        });
        wd.start();
        
        cfgRun.stopTestrun();

        if (bw != null) {
			try {
				bw.flush();
				bw.close();
			}
			catch (IOException e1) {
				System.err.println("Internal error when closing logfile: " + e1.toString());
			}
		}
        wd.interrupt();
	}

	class MyErrorReporter implements ErrorReporter {

		public void report(final String s) {
			System.err.println("errreporter: " + s);
		}

		// TODO stop the test run
		public void endOfTest(final String s) {
			System.err.println("errreporter: eot: " + s);
			// stop the test run
		}

		public void log(long t, final String src, final String s) {
			// System.err.println("errreporter: ("+src+") "+fmt.format(t)+" "+s);
			logMux.addLogMessage(t, src, s);
		}

		private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	}

	private Testrun cfgRun;

	private RunItemData guideData = new RunItemData();
	private RunItemData guidePrimerData = new RunItemData();
	private RunItemData modelData = new RunItemData();
	private RunItemData modelPrimerData = new RunItemData();
	private RunItemData combinatorData = new RunItemData();
	private RunItemData implData = new RunItemData();
	private RunSessionData runData = new RunSessionData();
	private RunState runState = new RunState();

	private Menu menu = new Menu();

	private LogServices logServices = null;
	private LogMux logMux = null;
	private Collection<LogMux> logMuxList = new Vector<LogMux>();

	private StepHighlighter highlighter = null;
	private ErrorReporter err = new MyErrorReporter();
	private ProgressReporter progress = new NullProgressReporter();
	private StateStatHandler stateStats = new StateStatHandler();

	private boolean debug = false;
	private boolean measureAutostepCPUTime = true;
}
