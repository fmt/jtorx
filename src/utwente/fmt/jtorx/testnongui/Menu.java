package utwente.fmt.jtorx.testnongui;

import java.util.ArrayList;
import java.util.HashMap;





import utwente.fmt.lts.Label;

public class Menu {
	
	public class LabelVerdict {
		private String label;
		private String verdict;
		
		public LabelVerdict(String l, String v) {
			label = l;
			verdict = v;
		}
	}
	public void clear() {
		inputsList.clear();
		outputsList.clear();
		inputsMap.clear();
		selectedInput = -1;
		//System.err.println("---------------");
		//System.err.println("Possible stimuli");
	}
	public void addInput(String s, Label l) {
		inputsList.add(s);
		inputsMap.put(s, l);
		System.err.println(s);
	}
	public void addOutput(String l, String v) {
		LabelVerdict item = new LabelVerdict (l, v);
		outputsList.add(item);
		System.err.println(l+" : "+v);
	}
	
	public void finalizeInputs() {
		System.err.println("---------------");
		System.err.println("Expected outputs : associated verdict");
	}
	public void finalizeOutputs() {
		//System.err.println("---------------");
	}
	
	private ArrayList<String> inputsList = new ArrayList<String>();;
	private ArrayList<LabelVerdict> outputsList = new ArrayList<LabelVerdict>();
	private int selectedInput = -1; // we use this to be able to deselect input; needs change if we allow multiple selections
	private HashMap<String,Label> inputsMap = new HashMap<String,Label>();
	

}
