package utwente.fmt.jtorx.testnongui;


import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import utwente.fmt.jtorx.interpretation.HardcodedInterpretation;
import utwente.fmt.jtorx.torx.Model;
import utwente.fmt.jtorx.torx.VoidConsumer;
import utwente.fmt.jtorx.torx.driver.OffLineRandomDerivationDriver;
import utwente.fmt.jtorx.torx.explorer.aut.AutLTS;
import utwente.fmt.jtorx.torx.model.ModelImpl;
import utwente.fmt.jtorx.torx.primer.Primer;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.utils.cmdint.Command;
import utwente.fmt.jtorx.utils.cmdint.CommandInterpreter;
import utwente.fmt.jtorx.writers.AutWriter;
import utwente.fmt.jtorx.writers.DotWriter;
import utwente.fmt.lts.LTS;

import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;
import utwente.fmt.test.Testcase;



public class RandomOffLine {

	class MyErrorReporter implements ErrorReporter {
		public void report(final String s) {
			System.err.println("errreporter: "+s);
			// TODO implement status line or so
		}
		public void log(long stmp, final String src, final String s) {
			System.err.println("errreporter: ("+src+"): "+sdf.format(stmp)+" "+s);
		}
		public void endOfTest(final String s) {
			System.err.println("errreporter: eot: "+s);
			// TODO integrate a way to stop the test run
		}
	}

	private void unfold(HashSet<State> states, State s) {
		if (states.contains(s))
			return;
		states.add(s);
		Iterator<? extends Transition<?extends State>> tit;
		tit = s.menu(interp.isAny());
		System.out.println("state "+s.getID());
		while(tit.hasNext()) {
			Transition<?extends State> t = tit.next();
			System.out.println("\t"+t.getLabel().getString()+" -> "+t.getDestination().getID());
		}
		tit = s.menu(interp.isAny());
		while(tit.hasNext()) {
			unfold(states, tit.next().getDestination());
		}
	}

	class CmdAuto implements Command, VoidConsumer {
		class CmdStop implements Command {
			public Boolean compute(String arg) {
				myDriver.stopAuto();
				return false;
			}
		}
		class CmdAutoSubCommands extends Commands {	
			CmdAutoSubCommands () {
				cmdTable = new HashMap<String,Command>();
				cmdTable.put("stop", new CmdStop());
			}
		}

		public Boolean compute(String arg) {
			if (arg != null) {
				try {
					int inc = Integer.parseInt(arg);
					return myDriver.autoStep(inc, this);				
				}
				catch(NumberFormatException nfe) {
					System.out.println("not a number: "+arg);
					return true;
				}
			} else
				return myDriver.autoStep(this);

		}
		public boolean consume() {
			cmdInterp.peek(sub);
			return true;
		}
		public void end() {
		}
		private utwente.fmt.jtorx.utils.cmdint.Commands sub = new CmdAutoSubCommands();

	}

	private void printTxt(Testcase t) {
		HashSet<State> states = new HashSet<State>();
		Iterator<?extends State> sit = null;
		try {
			sit = testcase.getLTS().init();
		} catch (LTSException e) {
			System.err.println("caught LTSException: "+e.getMessage());
			return;
		}
		while(sit.hasNext())
			unfold(states, sit.next());
	}

	class CmdAddLevel implements Command {
		public Boolean compute(String arg) {
			myDriver.addLevel();
			return true;
		}
	}
	class CmdPrintTxt implements Command {
		public Boolean compute(String arg) {
			printTxt(testcase);
			return true;
		}
	}
	class CmdPrintAut implements Command {
		public Boolean compute(String arg) {
			try {
				OutputStreamWriter w = new OutputStreamWriter(System.out);
				new AutWriter(testcase.getLTS(), interp, w).write();
				w.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
	}
	class CmdPrintDot implements Command {
		public Boolean compute(String arg) {
			try {
				OutputStreamWriter w = new OutputStreamWriter(System.out);
				new DotWriter(testcase.getLTS(), interp, w).write();
				w.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
	}

	class CmdQuit implements Command {
		public Boolean compute(String arg) {
			return false;
		}
	}
	class ToplevelCommands extends Commands {
		ToplevelCommands () {
			cmdTable = new HashMap<String,Command>();
			cmdTable.put("quit", new CmdQuit());
			cmdTable.put("q", new CmdQuit());
			cmdTable.put("exit", new CmdQuit());
			cmdTable.put("x", new CmdQuit());
			cmdTable.put("n", new CmdAddLevel());
			cmdTable.put("a", new CmdAuto());
			cmdTable.put("t", new CmdPrintTxt());
			cmdTable.put("p", new CmdPrintAut());
			cmdTable.put("d", new CmdPrintDot());
		}
	}

	abstract class Commands implements utwente.fmt.jtorx.utils.cmdint.Commands {

		public Command lookup(String s) {
			return cmdTable.get(s);
		}

		protected HashMap<String, Command> cmdTable;
	}


	public void run (String m) {

		cmds = new ToplevelCommands();
		cmdInterp = new CommandInterpreter();

		// here we might want something that looks at the file
		// type and does the right thing:
		// .aut -> use AutLTS

		try {
			myModelLTS = new AutLTS(m);
		} catch (IOException e) {
			String msg = "cannot read "+m;
			if (e.getMessage() != null)
				msg += ": "+e.getMessage();
			System.err.println(msg);
			return;
		}

		interp = new HardcodedInterpretation();

		myPrimer = new Primer(myModelLTS, interp, false /*tracesOnly*/, err);
		model = new ModelImpl(myPrimer, interp, err);

		myDriver = new OffLineRandomDerivationDriver(model);

		testcase = myDriver.init();

		cmdInterp.loop("tester> ", cmds);


	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args.length != 1) {
			System.err.println("usage: ltsim/sim model.aut");
			return;
		}
		
			
		// here we might want something that looks at the file
		// type and does the right thing:
		// .aut -> use AutLTS
		
		RandomOffLine t = new RandomOffLine();
		t.run(args[0]);
		
	}

	// private  State testerState;
	private  Commands  cmds;
	private  Model model;
	private LTS myModelLTS;
	private CompoundLTS myPrimer;
	private HardcodedInterpretation interp;
	private OffLineRandomDerivationDriver myDriver;
	private CommandInterpreter cmdInterp;
	private Testcase testcase;
	
	private ErrorReporter err = new MyErrorReporter();
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

}
