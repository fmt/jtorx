package utwente.fmt.jtorx.testnongui;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;



import utwente.fmt.jtorx.interpretation.HardcodedInterpretation;
import utwente.fmt.jtorx.torx.Model;
import utwente.fmt.jtorx.torx.adapter.lts.AdapterImpl;
import utwente.fmt.jtorx.torx.explorer.aut.AutLTS;
import utwente.fmt.jtorx.torx.model.ModelImpl;
import utwente.fmt.jtorx.torx.primer.Primer;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.utils.cmdint.Command;
import utwente.fmt.jtorx.utils.cmdint.CommandInterpreter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.LTS.LTSException;

// This is an early version.
// This effectively implements the Driver functionality,
// together with the user-interface to it.
// It does not use a separate Driver implementation.
// It is not able to deal with test-purposes.


public class Tester {
	
	class MyErrorReporter implements ErrorReporter {
		public void report(final String s) {
			System.err.println("errreporter: "+s);
			// TODO implement status line or so
		}
		public void log(long stmp, final String src, final String s) {
			System.err.println("errreporter: ("+src+"): "+sdf.format(stmp)+" "+s);
		}
		public void endOfTest(final String s) {
			System.err.println("errreporter: eot: "+s);
			// TODO implement a way to end the test run
		}
	}

	private  void printState(State s, String pfx) {
		//System.out.println("state: " + s.getID()+" "+s.toString());
		System.out.println(pfx+"state: " + s.getID());

	}
	// note: use pco="" for menu entries
	// note: use pco="bla" or pco=null for input/output results
	// because: when pco=null we fake it
	// because: when pco!=null we do not fake it (put omit "")
	private String formatLabel(String kind, String p, String s){
		String chan = null;
		String pco = p;
		String lbl = s;
		String chanPco = "";
		if (s.equalsIgnoreCase("delta"))
			lbl = "(Quiescence)";
		if (kind.equals("input"))
			chan = "in";
		else if (kind.equals("output") || kind.equals("expected"))
			chan = "out";
		if ((kind.equals("input") || kind.equals("output")) && pco == null && !s.equals("delta")) {
			if (s.length() > 3)
				pco = s.substring(0, 3);
			else
				pco = s;
		}
		if (chan != null)
			chanPco += chan;
		if (pco != null && !pco.equals(""))
			chanPco += ":"+pco;
		if (chan != null || pco != null)
			chanPco = "("+chanPco+ ")";
		return kind+chanPco+": "+lbl;
	}
	private  void printModelMenu(CompoundState s) {
		Iterator<Label> it;
		it = model.getInputs(s).iterator();
		while (it.hasNext())
			System.out.println(formatLabel("input", "", it.next().getString()));
		it = model.getOutputs(s).iterator();
		while (it.hasNext())
			System.out.println(formatLabel("output", "", it.next().getString()));
	}

	
	
	
	//static class CommandInterpreter {

	//class Commands implements cmdint.Commands {
		
		class CmdAuto implements Command {
			public Boolean compute(String arg) {
				int stepMax = 0;
				if (arg != null)
					try {
						int inc = Integer.parseInt(arg);
						stepMax = stepNr + inc;
					}
					catch(NumberFormatException nfe) {
					}
				//Command m = new CmdMenu();
				Command i = new CmdRandomInput();
				Command o = new CmdCheckOutput();
				Boolean goOn = true;
				Boolean nextIsInput;
				while(goOn && (stepMax <= 0 || stepNr < stepMax)) {
					//goOn = m.compute(arg);
					int inSize = model.getInputs(testerState).size();
					int outSize = model.getOutputs(testerState).size();
					if (lastWasDelta && (inSize > 0))
						nextIsInput = true;
					else if (inSize > 0 && outSize > 0) {
						double d = Math.random();
						nextIsInput = (d < .5);	
					} else if (inSize > 0)
						nextIsInput = true;
					else
						nextIsInput = false;
						
					if (nextIsInput) {
						goOn = i.compute(null);
					} else {
						goOn = o.compute(null);
					}
				}
				return goOn;
			}
		}
			
			class CmdMenu implements Command {
				public Boolean compute(String arg) {
					printModelMenu(testerState);
					return true;
				}
			}
			class CmdState implements Command {
				public Boolean compute(String arg) {
					System.out.println("tester:");
					printState(testerState, "\t");
					System.out.println("impl:");
					//printState(implState, "\t");
					myAdapter.printState();
				return true;
				}
			}
			class CmdApplyInput implements Command {
				public Boolean compute(String arg) {
					if (arg == null) {
						// System.out.println("(no input given, trying random input)");
						return new CmdRandomInput().compute(arg);
					} else {
						String s = arg.trim();
						Label l = new TesterLabel(s);
						stepNr++;
						CompoundTransition<? extends CompoundState> nextTS = model.next(testerState, l);
						//State nextIS = labelInMenu(implState, s);
						myAdapter.applyStimulus(l);
						if (nextTS != null) {
							System.out.println(stepNr+" "+formatLabel("input", null, s));
							lastWasDelta = false;
							testerState = nextTS.getDestination();
						} else {
							System.out.println("(not in spec)");
						}
					}
					return true;
				}
			}
			class CmdRandomInput implements Command {
				public Boolean compute(String arg) {
					Vector<Label> in = model.getInputs(testerState);
					if (in.size() > 0) {
						double d = Math.random();
						int i = ((int) ( in.size() * d));
						return new CmdApplyInput().compute(in.elementAt(i).getString());
					} else
						System.out.println("(sorry, input menu is empty)");	
					return true;
				}
			}
			
			class CmdCheckOutput implements Command {
				public Boolean compute(String arg) {
					stepNr++;
					Label l = myAdapter.getObservation().getLabel();
					CompoundTransition<? extends CompoundState> nextTS = model.next(testerState, l);
					System.out.println(stepNr+" "+formatLabel("output", null, l.getString()));
					lastWasDelta = (l.getString().equals("delta"));
					if (nextTS !=  null) {
						// System.out.println("ok");
						testerState = nextTS.getDestination();
					} else {
						System.out.println("spec disagrees with iut");
						verdict = "fail";
						Iterator<Label> exp = model.getOutputs(testerState).iterator();
						if (!exp.hasNext())
							System.out.println("(oops internal error? no outputs, no delta)");
						else {
							System.out.println("outputs expected by spec:");
							while(exp.hasNext())
								System.out.println(formatLabel("expected", null, exp.next().getString()));
							System.out.println("(end of expected outputs)");
						}
						System.out.println("state according to spec:");
						System.out.println("(end of state)");
						return false;
					}
					return true;
				}
			}
			class CmdQuit implements Command {
				public Boolean compute(String arg) {
					return false;
				}
			}
			
	class Commands implements utwente.fmt.jtorx.utils.cmdint.Commands {
				public Command lookup(String s) {
		
				return cmdTable.get(s);
			}
			
			Commands () {
				cmdTable = new HashMap<String,Command>();
				cmdTable.put("auto", new CmdAuto());
				cmdTable.put("quit", new CmdQuit());
				cmdTable.put("q", new CmdQuit());
				cmdTable.put("exit", new CmdQuit());
				cmdTable.put("x", new CmdQuit());
				cmdTable.put("menu", new CmdMenu());
				cmdTable.put("m", new CmdMenu());
				cmdTable.put("state", new CmdState());
				cmdTable.put("input", new CmdApplyInput());
				cmdTable.put("i", new CmdApplyInput());
				cmdTable.put("output", new CmdCheckOutput());
				cmdTable.put("o", new CmdCheckOutput());
			}
			private HashMap<String, Command> cmdTable;
		}
		
	State getTesterState() {
		return testerState;
	}
	
	
	
	public void run (String m, String i) {
		
		cmds = new Commands();
		CommandInterpreter cmdInterp = new CommandInterpreter();
	
		// here we might want something that looks at the file
		// type and does the right thing:
		// .aut -> use AutLTS

			try {
				myModelLTS = new AutLTS(m);
			} catch (IOException e) {
				String msg = "cannot read "+m;
				if (e.getMessage() != null)
					msg += ": "+e.getMessage();
				System.err.println(msg);
				return;
			}
			try {
				myImplLTS = new AutLTS(i);
			} catch (IOException e) {
				String msg = "cannot read "+i;
				if (e.getMessage() != null)
					msg += ": "+e.getMessage();
				System.err.println(msg);
				return;
			}
			
			interp = new HardcodedInterpretation();
			
			myPrimer = new Primer(myModelLTS, interp, false /*tracesOnly*/, err);
			model = new ModelImpl(myPrimer, interp, err);
			
			myAdapter = new AdapterImpl(myImplLTS, interp, seed, true /*synthDelta*/);

			try {
				testerState = model.start();
			} catch (LTSException e) {
				System.err.println("caught LTSException: "+e.getMessage());
				testerState = null;
			}
			if (testerState==null) {
				verdict = "error";
			}else {
				myAdapter.start();
			
				verdict = "inconclusive";
				cmdInterp.loop("tester> ", cmds);
			}
			System.out.println("End of Test");
			if (verdict != null)
				System.out.println("Verdict: "+stepNr+" "+verdict);
			
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.err.println("usage: ltsim/sim model.aut impl.aut");
			return;
		}
		
			
		// here we might want something that looks at the file
		// type and does the right thing:
		// .aut -> use AutLTS
		
		Tester t = new Tester();
		t.run(args[0], args[1]);
		
	}

	private  CompoundState testerState;
	private  Commands  cmds;
	private Boolean lastWasDelta = false;
	private LTS myModelLTS;
	private LTS myImplLTS;
	private CompoundLTS myPrimer;
	private Model model;
	private AdapterImpl myAdapter;
	private HardcodedInterpretation interp;
	private int stepNr = 0;
	private String verdict;
	private long seed = 0;

	private ErrorReporter err = new MyErrorReporter();
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
}
