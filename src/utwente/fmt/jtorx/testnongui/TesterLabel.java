package utwente.fmt.jtorx.testnongui;

import java.util.ArrayList;
import java.util.Iterator;

import utwente.fmt.jtorx.utils.EmptyIterator;
import utwente.fmt.lts.Action;
import utwente.fmt.lts.Constraint;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Variable;

public class TesterLabel implements Label, Action {

	// this only works as long is we have basic labels
	// if we have complex actions we need to split
	// action from other label parameters
	public Action getAction() {
		return this;
	}
	public String getString() {
		return label;
	}

	public Boolean isObservable() {
		return true;
	}
	
	public TesterLabel (String s) {
		label = s;
	}
	
	public Iterator<Variable> getVariables() {
		if (variables==null)
			return EmptyIterator.iterator();
		return variables.iterator();
	}
	
	public Constraint getConstraint() {
		return null;
	}



	public boolean equals(Object o) {
		if (o instanceof Label) {
	         Label l = (Label) o;
	         return (l.getString().equals(this.getString()));
		}
		return false;
	}
	
	public Boolean eq(Label l) {
		return (l.getString().equals(this.getString()));
	}

	private String label;
	private ArrayList<Variable> variables = null;

}
