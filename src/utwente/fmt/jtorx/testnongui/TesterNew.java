package utwente.fmt.jtorx.testnongui;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;

import utwente.fmt.jtorx.interpretation.HardcodedInterpretation;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResultConsumer;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.torx.InterActionResult;
import utwente.fmt.jtorx.torx.Model;
import utwente.fmt.jtorx.torx.adapter.lts.AdapterImpl;
import utwente.fmt.jtorx.torx.driver.OnLineTestingDriver;
import utwente.fmt.jtorx.torx.explorer.aut.AutLTS;
import utwente.fmt.jtorx.torx.model.ModelImpl;
import utwente.fmt.jtorx.torx.primer.Primer;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.utils.cmdint.Command;
import utwente.fmt.jtorx.utils.cmdint.CommandInterpreter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.test.Verdict;


// this is more recent than Tester
// it uses OnLineTestingDriver to do the Driver work
//
// However, it does contain formatting of the interaction results
// that we also have elsewhere.

public class TesterNew {
	
	class MyErrorReporter implements ErrorReporter {
		public void report(final String s) {
			System.err.println("errreporter: "+s);
			// TODO implement status line or so
		}
		public void log(long stmp, final String src, final String s) {
			System.err.println("errreporter: ("+src+"): "+sdf.format(stmp)+" "+s);
		}
		public void endOfTest(final String s) {
			System.err.println("errreporter: eot: "+s);
			// TODO implement a way to end the test run
		}
	}

	/*
	private  void printState(State s, String pfx) {
		//System.out.println("state: " + s.getID()+" "+s.toString());
		System.out.println(pfx+"state: " + s.getID());

	}
	*/
	// note: use pco="" for menu entries
	// note: use pco="bla" or pco=null for input/output results
	// because: when pco=null we fake it
	// because: when pco!=null we do not fake it (put omit "")
	private String formatLabel(String kind, String p, String s){
		String chan = null;
		String pco = p;
		String lbl = s;
		String chanPco = "";
		if (s.equalsIgnoreCase("delta"))
			lbl = "(Quiescence)";
		if (kind.equals("input"))
			chan = "in";
		else if (kind.equals("output") || kind.equals("expected"))
			chan = "out";
		if ((kind.equals("input") || kind.equals("output")) && pco == null && !s.equals("delta")) {
			if (s.length() > 3)
				pco = s.substring(0, 3);
			else
				pco = s;
		}
		if (chan != null)
			chanPco += chan;
		if (pco != null && !pco.equals(""))
			chanPco += ":"+pco;
		if (chan != null || pco != null)
			chanPco = "("+chanPco+ ")";
		return kind+chanPco+": "+lbl;
	}
	private String formatInterAction(DriverInterActionResult l, String p) {
		String kind ="";
		if (l.getKind() == InterActionResult.Kind.INPUT)
			kind = "input";
		if (l.getKind() == InterActionResult.Kind.OUTPUT)
			kind = "output";
		return formatLabel(kind, p, l.getLabel().getString());
		
	}
	private  void printModelMenu() {
		Iterator<Label> it;
		it = myDriver.getModelInputs().iterator();
		while (it.hasNext())
			System.out.println(formatLabel("input", "", it.next().getString()));
		it = myDriver.getModelOutputs().iterator();
		while (it.hasNext())
			System.out.println(formatLabel("output", "", it.next().getString()));
	}
	
	private Boolean handleInterAction(DriverInterActionResult ll) {
		System.out.println(ll.getStepNr()+" "+formatInterAction(ll, null));
		stepNr = ll.getStepNr();
		//lastWasDelta = (ll.getLabel().getString().equals("delta"));
		if (ll.hasVerdict()) {
			verdict = ll.getVerdict();
			if (verdict.isFail()){
				System.out.println("spec disagrees with iut");
				verdict = ll.getVerdict();
				Iterator<Label> exp = myDriver.getModelOutputs().iterator();
				if (!exp.hasNext())
					System.out.println("(oops internal error? no outputs, no delta)");
				else {
					System.out.println("outputs expected by spec:");
					while(exp.hasNext())
						System.out.println(formatLabel("expected", null, exp.next().getString()));
					System.out.println("(end of expected outputs)");
				}
				System.out.println("state according to spec:");
				System.out.println("(end of state)");
			}
			return false;
		}
		return true;
	}



	class CmdAuto implements Command, DriverInterActionResultConsumer {
		public Boolean compute(String arg) {
			if (arg != null) {
				try {
					int inc = Integer.parseInt(arg);
					return myDriver.autoStep(inc, this);				
				}
				catch(NumberFormatException nfe) {
					System.out.println("not a number: "+arg);
					return true;
				}
			} else
				return myDriver.autoStep(this);	
			
		}
		public boolean consume(DriverInterActionResult ll) {
			Boolean goOn = handleInterAction(ll);
			//cmdInterp.checkSubCommands();
			return goOn;
		}
		public void end() {
		}
		
	}
		
		class CmdMenu implements Command {
			public Boolean compute(String arg) {
				printModelMenu();
				return true;
			}
		}
		class CmdState implements Command {
			public Boolean compute(String arg) {
				System.out.println("tester:");
				//printState(testerState, "\t");
				System.out.println("impl:");
				//printState(implState, "\t");
				myAdapter.printState();
			return true;
			}
		}
		class CmdApplyInput implements Command {
			public Boolean compute(String arg) {
				if (arg == null) {
					// System.out.println("(no input given, trying random input)");
					return new CmdRandomInput().compute(arg);
				} else {
					String s = arg.trim();
					Label l = new TesterLabel(s);
					DriverInterActionResult ll = myDriver.stimulate(l);
					if (ll != null) {
						return handleInterAction(ll);
					} else {
						System.out.println("(not in spec)");
					}
				}
				return true;
			}
		}
		class CmdRandomInput implements Command {
			public Boolean compute(String arg) {
				DriverInterActionResult ll = myDriver.stimulate();
				if (ll == null) {
					System.out.println("(sorry, input menu is empty)");
					return true;
				} else
					return handleInterAction(ll);
			}
		}
		
		class CmdCheckOutput implements Command {
			public Boolean compute(String arg) {
				DriverInterActionResult o = myDriver.observe();
				return handleInterAction(o);
			}
		}
		class CmdQuit implements Command {
			public Boolean compute(String arg) {
				return false;
			}
		}
		
class Commands implements utwente.fmt.jtorx.utils.cmdint.Commands {
			public Command lookup(String s) {
	
			return cmdTable.get(s);
		}
		
		Commands () {
			cmdTable = new HashMap<String,Command>();
			cmdTable.put("auto", new CmdAuto());
			cmdTable.put("quit", new CmdQuit());
			cmdTable.put("q", new CmdQuit());
			cmdTable.put("exit", new CmdQuit());
			cmdTable.put("x", new CmdQuit());
			cmdTable.put("menu", new CmdMenu());
			cmdTable.put("m", new CmdMenu());
			cmdTable.put("state", new CmdState());
			cmdTable.put("input", new CmdApplyInput());
			cmdTable.put("i", new CmdApplyInput());
			cmdTable.put("output", new CmdCheckOutput());
			cmdTable.put("o", new CmdCheckOutput());
		}
		private HashMap<String, Command> cmdTable;
	}

	public void run (String m, String i) {

		cmds = new Commands();
		cmdInterp = new CommandInterpreter();

		// here we might want something that looks at the file
		// type and does the right thing:
		// .aut -> use AutLTS

		try {
			myModelLTS = new AutLTS(m);
		} catch (IOException e) {
			String msg = "cannot read "+m;
			if (e.getMessage() != null)
				msg += ": "+e.getMessage();
			System.err.println(msg);
			return;
		}
		try {
			myImplLTS = new AutLTS(i);
		} catch (IOException e) {
			String msg = "cannot read "+i;
			if (e.getMessage() != null)
				msg += ": "+e.getMessage();
			System.err.println(msg);
			return;
		}


		interp = new HardcodedInterpretation();

		myPrimer = new Primer(myModelLTS, interp, false /*tracesOnly*/, err);
		model = new ModelImpl(myPrimer, interp, err);

		myAdapter = new AdapterImpl(myImplLTS, interp, seed, true/*synthDelta*/);

		myDriver = new OnLineTestingDriver(model, myAdapter, seed);

		DriverInitializationResult r = myDriver.init();

		if (r.hasVerdict())
			verdict = r.getVerdict();
		cmdInterp.loop("tester> ", cmds);
		System.out.println("End of Test");
		if (verdict != null)
			System.out.println("Verdict: "+stepNr+" "+verdict.getString());
		// TODO: do we not give a verdict when we did not fail?

	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.err.println("usage: ltsim/sim model.aut impl.aut");
			return;
		}
		
			
		// here we might want something that looks at the file
		// type and does the right thing:
		// .aut -> use AutLTS
		
		TesterNew t = new TesterNew();
		t.run(args[0], args[1]);
		
	}


	// private  State testerState;
	private  Commands  cmds;
	//private Boolean lastWasDelta = false;
	private LTS myModelLTS;
	private LTS myImplLTS;
	private CompoundLTS myPrimer;
	private  Model model;
	private AdapterImpl myAdapter;
	private HardcodedInterpretation interp;
	private Verdict verdict;
	private int stepNr;
	private OnLineTestingDriver myDriver;
	private CommandInterpreter cmdInterp;
	private long seed  = 0;

	private ErrorReporter err = new MyErrorReporter();
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
}
