package utwente.fmt.jtorx.testnongui.casestudy;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * A tokenizer for {@link CaseStudyMainWrapper} arguments, i.e. argument lists
 * of the form NoParamArg1;UnaryArg2;ParamToArg2;...
 */
public class CaseStudyArgTokenizer {
	/** The tokenized arguments, mapping argument identifiers to complete arguments
	 * (i.e. identifiers with instantiated parameters)
	 */
	private final Map<CaseStudyArgumentIdentifier, CaseStudyArgument> arguments;
	
	/**
	 * Constructs a new {@link CaseStudyArgTokenizer} instance, tokenizing the
	 * given argument list <tt>csArgString</tt>.
	 * @param csArgString The semicolon-separated argument list.
	 */
	public CaseStudyArgTokenizer(String csArgString) {
		this.arguments = new HashMap<>();

		String[] argSegments = csArgString.split(";");
		int currentPos = 0;
		while (currentPos < argSegments.length) {
			currentPos = consumeFurther(argSegments, currentPos);
		}
	}
	
	/**
	 * Retrieves an argument instantiation from the tokenizer. If no matching
	 * argument was given in the argument list, {@link NoSuchElementException}
	 * is thrown.
	 * @param identifier The identifier of the argument to be retrieved.
	 * @return The retrieved argument.
	 */
	public CaseStudyArgument getArgument(CaseStudyArgumentIdentifier identifier) {
		if (!arguments.containsKey(identifier)) {
			throw new NoSuchElementException(identifier.toString());
		}
		else {
			return arguments.get(identifier);
		}
	}
	
	/**
	 * Checks whether the argument list contains an argument matching the given
	 * argument identifier.
	 * @param identifier An argument identifier.
	 * @return <tt>true</tt> iff the argument list contains an argument matching
	 *   <tt>identifier</tt>.
	 */
	public boolean containsArgument(CaseStudyArgumentIdentifier identifier) {
		return arguments.containsKey(identifier);
	}
	
	/**
	 * Parses the next argument in the argument list.
	 * @param argumentSegments The argument list, splittet by semicolons.
	 * @param currentPosition The position of the first element of the argument
	 *   to be parsed in <tt>argumentSegments</tt>.
	 * @return The amount of element of <tt>argumentSegments</tt> parsed, i.e.
	 *   the starting position of the next argument.
	 */
	private int consumeFurther(String[] argumentSegments, int currentPosition) {
		assert argumentSegments.length > currentPosition;
		int pos = currentPosition;
		
		CaseStudyArgumentIdentifier id = CaseStudyArgumentIdentifier.parse(argumentSegments[pos]);
		pos++;
		
		CaseStudyArgument arg = new CaseStudyArgument(id, argumentSegments, pos);
		arguments.put(id, arg);
		
		return pos + id.getArgumentCount();
	}

	/**
	 * An instantiated CaseStudy argument, consisting of an argument identifier
	 * and (if applicable) the instantiated parameters.
	 */
	public static class CaseStudyArgument {
		/** The argument's identifier. */
		private final CaseStudyArgumentIdentifier identifier;
		
		/** Reference to the (splitted) original case study arguments. */
		private final String[] globalArgArray;
		
		/** Index of the first item of this argument in <tt>globalArgArray</tt>. */
		private final int begin;
		
		/**
		 * Constructs a {@link CaseStudyArgument} instance.
		 * @param id The argument's identifier.
		 * @param argArray The splitted case study arguments.
		 * @param begin Index of the first item of this arguments in
		 *   <tt>argArray</tt>.
		 */
		private CaseStudyArgument(CaseStudyArgumentIdentifier id, String[] argArray, int begin) {
			if (id.getArgumentCount() > 0) {
				assert begin >= 0 && argArray.length > begin;
				assert argArray.length >= begin + id.getArgumentCount();
			}
			
			this.identifier = id;
			this.globalArgArray = argArray;
			this.begin = begin;
		}
		
		/**
		 * Retrieves an argument parameter.
		 * @param idx The parameter's index. Must be nonnegative and legal
		 *   wrt. the parameter count of the argument.
		 * @return The argument parameter instantiation.
		 */
		public String getArgument(int idx) {
			if (idx >= 0 && idx < identifier.getArgumentCount()) {
				return globalArgArray[idx + begin];
			}
			else {
				throw new IllegalArgumentException(idx + ": Out of bounds");
			}
		}
		
		/**
		 * @return The argument's identifier.
		 */
		public CaseStudyArgumentIdentifier getIdentifier() {
			return identifier;
		}
	}
}
