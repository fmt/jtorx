package utwente.fmt.jtorx.testnongui.casestudy;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * The argument identifiers.
 */
public enum CaseStudyArgumentIdentifier {
	/** The null argument. */
	NOARGS("noargs",0),

	/** Use the MonotonouslyDecreaseLicenseCount LazyOTF test objective. */
	USE_LAZYOTF_TO_DECREASE("decrease-lazyotfto", 1),

	/** Use the DetLicenseCountCoverage LazyOTF test objective. */
	USE_LAZYOTF_TO_LICCOV("liccov-lazyotfto", 2),

	/** Do not force the JVM exit at the end of the execution. */
	NOFORCEEXIT("no-force-exit", 0),

	/** Use the mersenne twister RNG in the JTorX testing driver. */
	USE_MERSENNE_TWISTER("use-mt", 0),

	/** Use LazyOTF distribution via UDP multicast. */
	USE_DIST_MUDP("use-dist-mudp", 0),

	/** Switch for MUDP loopback */
	DIST_MUDP_LOOPBACK("mudp-loopback", 0),

	/** Use LazyOTF distribution via UDP broadcast. */
	USE_DIST_BUDP("use-dist-budp", 0),

	/** Use LazyOTF distribution via Hazelcast. */
	USE_DIST_HAZEL("use-dist-hazel", 2),

	/** Use the ReachShowMoreLicenses JTorX exit criterion. */
	DRVARG_TO_FINDSHOWMORELICENSES("findshowmorelicenses-simpleto", 0),

	/** Use the Coverage JTorX exit criterion. */
	DRVARG_TO_COVERAGE("coverage-simpleto",0),

	/** Use the MonotonouslyDecreaseLicenseCount JTorX exit criterion. */
	DRVARG_TO_DECREASELICENSECOUNT("decrease-simpleto",1),

	/** Use the DetLicenseCountCoverage JTorX exit criterion. */
	DRVARG_TO_JTORXLICCOV("liccov-jtorxto", 2),

	/** Use JTorX exit criteria. */
	DRVARG_JTORX_MODE("jtorx-mode",0),

	/** Enable performance measurements for guides. */
	DRVARG_GUIDED_MODE("guided-mode",0),

	/** Enable cycle detection warnings, with the warning threshold being the argument. */
	ENABLE_CYCLE_DETECTION_WARNINGS("enable-cycle-detection", 1),

	/** Disable most JTorX output. */
	QUIET_MODE("quiet", 0),

	/** The Java logger log-level. */
	VERBOSE("log-level", 1),

	/** Disable JTorX<->LazyOTF message logging. */
	DISABLE_LOTF_COMM_LOGGING("disable-lotf-comm-logging", 0),

	/** Sleep during LazyOTF-HazelCast cleanup before shutting down HazelCast.
	 * This value is a minimum value for randomization, see
	 * SLEEP_BEFORE_HC_SHUTDOWN_MAX_RATIO.
	 */
	SLEEP_BEFORE_HC_SHUTDOWN("sleep-before-hc-shutdown", 1),

	/** Ratio of SLEEP_BEFORE_HC_SHUTDOWN value determining the interval from
	 * which a random value is taken as the actual sleep time during
	 * LazyOTF-Hazelcast cleanup.
	 */
	SLEEP_BEFORE_HC_SHUTDOWN_MAX_RATIO("sleep-before-hc-shutdown-max-ratio", 1),

	/** Max amount of time the shutdown watchdog waits before killing the JVM. */
	MAX_WAIT_BEFORE_JVM_FORCED_SHUTDOWN("max-wait-before-forcing-jvm-shutdown", 1),

	/** Sleep between teststeps. */
	SLEEP_BETWEEN_TESTSTEPS("sleep-between-teststeps", 1);


	/** The argument's parameter count. */
	private final int argumentCount;

	/** The argument's name. */
	private final String argumentName;

	/** A mapping from argument identifier names to argument identifiers. */
	private static final Map<String, CaseStudyArgumentIdentifier> names2Ids = new HashMap<>();

	/**
	 * Constructs a {@link CaseStudyArgumentIdentifier}.
	 * @param argumentName The argument's name.
	 * @param argumentCount The argument's parameter count.
	 */
	private CaseStudyArgumentIdentifier(String argumentName, int argumentCount) {
		this.argumentCount = argumentCount;
		this.argumentName = argumentName;
	}

	/**
	 * Returns the argument identifier associated with the given argument
	 * name. Throws {@link NoSuchElementException} if the name is not associated
	 * with any argument identifier.
	 * @param s The argument name.
	 * @return The matching argument identifier.
	 */
	public static CaseStudyArgumentIdentifier parse(String s) {
		synchronized(names2Ids) {

			/* If names2Ids has not been initialized yet, do so by populating it. */
			if (names2Ids.isEmpty()) {
				for (CaseStudyArgumentIdentifier id : values()) {
					names2Ids.put(id.getArgumentName(), id);
				}
			}

			if (!names2Ids.containsKey(s)) {
				throw new NoSuchElementException();
			}

			return names2Ids.get(s);
		}
	}

	/**
	 * @return The argument's parameter count.
	 */
	public int getArgumentCount() {
		return this.argumentCount;
	}

	/**
	 * @return The argument's name.
	 */
	public String getArgumentName() {
		return this.argumentName;
	}

	@Override
	public String toString() {
		return getArgumentName();
	}
}