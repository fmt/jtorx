package utwente.fmt.jtorx.testnongui.casestudy;

import java.io.PrintStream;

/**
 * Alternative output channels for case-study relevant outputs. This class
 * was introduce in order to be able to shut down all output not relevant
 * to the measurements conducted during the case study without having to
 * rewrite large parts of JTorX (since System.{err,out} gets used directly
 * quite often).
 * 
 * These streams get replaced by null streams when using the quiet
 * mode.
 */
public class CaseStudyIO {
	/** The error output stream. */
    public static PrintStream err = System.err;
    
    /** The standard output stream. */
    public static PrintStream out = System.out;
}
