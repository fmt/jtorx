package utwente.fmt.jtorx.testnongui.casestudy;

import java.io.PrintStream;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfigurationHooks;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfigurationHooks.LazyOTFConfigurationHook;
import utwente.fmt.jtorx.lazyotf.casestudy.CaseStudyOnLineTestingDriverWrapper;
import utwente.fmt.jtorx.lazyotf.casestudy.CaseStudyOnLineTestingDriverWrapper.InstantiationHooks.InstantiationHook;
import utwente.fmt.jtorx.lazyotf.casestudy.CoverageSimpleTO;
import utwente.fmt.jtorx.lazyotf.casestudy.DecreaseLicenseCountSimpleTO;
import utwente.fmt.jtorx.lazyotf.casestudy.FindShowMoreLicensesSimpleTO;
import utwente.fmt.jtorx.lazyotf.casestudy.LicenseCountCoveragePassiveTO;
import utwente.fmt.jtorx.lazyotf.casestudy.LicenseCountCoverageSimpleTO;
import utwente.fmt.jtorx.lazyotf.casestudy.MeasureTime;
import utwente.fmt.jtorx.lazyotf.casestudy.RemoveWithoutGeneratePassiveTO;
import utwente.fmt.jtorx.lazyotf.distributed.BUDPDistributeLazyOTFFactory;
import utwente.fmt.jtorx.lazyotf.distributed.HazelDistributeLazyOTFFactory;
import utwente.fmt.jtorx.lazyotf.distributed.MUDPDistributeLazyOTFFactory;
import utwente.fmt.jtorx.lazyotf.driver.PolicyObserverFactoryRegistry;
import utwente.fmt.jtorx.testnongui.Main;
import utwente.fmt.jtorx.testnongui.casestudy.CaseStudyArgTokenizer.CaseStudyArgument;
import utwente.fmt.jtorx.torx.driver.CompoundSimOnLineTestingDriver;

/**
 * A class wrapping {@link Main # main(String[])}, measuring and printing
 * its running time as well as processing case-study-relevant arguments.
 */
public class CaseStudyMainWrapper {
	/** The class logger. */
	private static final Logger LOGGER = Logger.getLogger(CaseStudyMainWrapper.class.getCanonicalName());

	/** The system property telling JTorX whether to use the 
	 * {@link CaseStudyOnLineTestingDriverWrapper} or not.
	 */
	public static final String SYSPROP_USEDRIVERWRAPPER = "casestudy.usedriverwrapper";

	/**
	 * The <tt>main</tt>-wrapping mehtod.
	 * @param args The program arguments.
	 */
	public static void main(String[] args) {
		LOGGER.severe("[progress] Started JTorX.");

		long wallclockStartTime = System.currentTimeMillis();

		if (args.length == 0) {
			return;
		}

		// Enable the case study driver wrapper.
		System.setProperty(SYSPROP_USEDRIVERWRAPPER, Boolean.TRUE.toString());

		String caseStudyArgs = args[0];

		boolean forceExit = true;
		boolean quietMode = false;

		// Process casestudy-relevant arguments and install corresponding hooks.
		final CaseStudyArgTokenizer tokenizer = new CaseStudyArgTokenizer(caseStudyArgs);

		if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.VERBOSE)) {
			CaseStudyArgument arg = tokenizer.getArgument(CaseStudyArgumentIdentifier.VERBOSE);
			try {
				setupLogging(Level.parse(arg.getArgument(0)));
			}
			catch (IllegalArgumentException e) {
				LOGGER.severe("Unknown log level " + arg.getArgument(0));
			}
		}
		else {
			setupLogging(Level.OFF);
		}

		// Clear old TOs, e.g. from previously executed JUnit tests.
		PolicyObserverFactoryRegistry.getInstance().clear();

		if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.USE_LAZYOTF_TO_DECREASE)) {
			CaseStudyArgument arg = tokenizer.getArgument(CaseStudyArgumentIdentifier.USE_LAZYOTF_TO_DECREASE);
			int maxPortion = Integer.parseInt(arg.getArgument(0));
			RemoveWithoutGeneratePassiveTO.registerWithDriverPrePolicyObserverRegistry(maxPortion+1);
			LOGGER.info("[config] Added LazyOTF TO: MonotonouslyDecreaseLicenseCount for maxPortion=" + maxPortion);
		}

		if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.USE_LAZYOTF_TO_LICCOV)) {
			CaseStudyArgument arg = tokenizer.getArgument(CaseStudyArgumentIdentifier.USE_LAZYOTF_TO_LICCOV);
			int minLicenseCount = Integer.parseInt(arg.getArgument(0));
			int maxLicenseCount = Integer.parseInt(arg.getArgument(1));
			LicenseCountCoveragePassiveTO.registerWithPrePolicyObserverFactory(minLicenseCount, maxLicenseCount);
			LOGGER.info("[config] Added LazyOTF TO: DetLicenseCountCoverage for minLicenseCount=" + minLicenseCount 
					+ ", maxLicenseCount=" + maxLicenseCount);
		}

		if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.NOFORCEEXIT)) {
			forceExit = false;
			LOGGER.info("[config] Not forcing JVM Exit");
		}

		if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.USE_MERSENNE_TWISTER)) {
			CompoundSimOnLineTestingDriver.setUseInternalRand(false);
			LOGGER.info("[config] Using Mersenne Twister in JTorX genTC");
		}

		if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.QUIET_MODE)) {
			quietMode = true;
			LOGGER.info("[config] Enabling Quiet Mode");
		}

		if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.MAX_WAIT_BEFORE_JVM_FORCED_SHUTDOWN)) {
			CaseStudyArgument arg = tokenizer.getArgument(CaseStudyArgumentIdentifier.MAX_WAIT_BEFORE_JVM_FORCED_SHUTDOWN);
			long maxWait;
			try {
				maxWait = Long.parseLong(arg.getArgument(0));
			}
			catch (NumberFormatException e) {
				throw new IllegalArgumentException(CaseStudyArgumentIdentifier.MAX_WAIT_BEFORE_JVM_FORCED_SHUTDOWN.toString()
						+ ": Could not parse argument " + arg.getArgument(0));
			}
			System.setProperty(Main.SYSPROP_MAX_SHUTDOWN_WAIT_MS, ""+maxWait);
			LOGGER.info("[config] Setting max. wait before forcing JVM shutdown after the test run execution to " + maxWait);
		}

		// Enforce a clean hooks environment.
		LazyOTFConfigurationHooks.getInstance().reset();
		CaseStudyOnLineTestingDriverWrapper.InstantiationHooks.getInstance().reset();        

		LazyOTFConfigurationHooks.getInstance().addHook(new LazyOTFConfigurationHook() {
			@Override
			public LazyOTFConfig applyHook(LazyOTFConfig cfg) {
				if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.USE_DIST_MUDP)) {
					boolean useNoLoopback = !tokenizer.containsArgument(CaseStudyArgumentIdentifier.DIST_MUDP_LOOPBACK);
					cfg.setDistributeLazyOTFFactory(new MUDPDistributeLazyOTFFactory(useNoLoopback));
					LOGGER.info("[config] Using MUDPDistributeLazyOTF, useNoLoopback=" + useNoLoopback);
				}
				else if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.USE_DIST_BUDP)) {
					cfg.setDistributeLazyOTFFactory(new BUDPDistributeLazyOTFFactory());
					LOGGER.info("[config] Using BUDPDistributeLazyOTF.");
				}
				else if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.USE_DIST_HAZEL)) {
					try {
						CaseStudyArgument arg = tokenizer.getArgument(CaseStudyArgumentIdentifier.USE_DIST_HAZEL);
						int initialMinCluster = Integer.parseInt(arg.getArgument(0));
						HazelDistributeLazyOTFFactory factory = new HazelDistributeLazyOTFFactory();
						factory.setInitialMinCluster(initialMinCluster);
						String clusterName = arg.getArgument(1);
						factory.setClusterName(clusterName);

						long minSleepPeriod = factory.getMinSleepBeforeShutdownMs();
						double maxRatio = factory.getMaxSleepBeforeShutdownRatio();
						boolean modifiedSleepTime = false;
						if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.SLEEP_BEFORE_HC_SHUTDOWN)) {
							CaseStudyArgument sleepArg = tokenizer.getArgument(CaseStudyArgumentIdentifier.SLEEP_BEFORE_HC_SHUTDOWN);
							try {
								minSleepPeriod = Long.parseLong(sleepArg.getArgument(0));
							}
							catch (NumberFormatException e) {
								throw new IllegalArgumentException(CaseStudyArgumentIdentifier.SLEEP_BEFORE_HC_SHUTDOWN.toString()
										+ " expects anargument parseable as a long value.");
							}
							factory.setMinSleepBeforeShutdownMs(minSleepPeriod);
							modifiedSleepTime = true;
							LOGGER.info("[config] Using a minimum pre-Hazelcast-shutdown sleep time of " + minSleepPeriod + "ms.");
						}

						if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.SLEEP_BEFORE_HC_SHUTDOWN_MAX_RATIO)) {
							CaseStudyArgument sleepRatioArg= tokenizer.getArgument(CaseStudyArgumentIdentifier.SLEEP_BEFORE_HC_SHUTDOWN_MAX_RATIO);
							try {
								maxRatio = Double.parseDouble(sleepRatioArg.getArgument(0));
							}
							catch (NumberFormatException e) {
								throw new IllegalArgumentException(CaseStudyArgumentIdentifier.SLEEP_BEFORE_HC_SHUTDOWN_MAX_RATIO.toString()
										+ " expects an argument parseable as a double value.");
							}
							factory.setMaxSleepBeforeShutdownRatio(maxRatio);
							modifiedSleepTime = true;
							LOGGER.info("[config] pre-Hazelcast-shutdown sleep time interval ratio: " + maxRatio);
						}

						if (modifiedSleepTime) {
							long currentMaxWaitBeforeShutdown;
							if (System.getProperty(Main.SYSPROP_MAX_SHUTDOWN_WAIT_MS) != null) {
								String maxWait = System.getProperty(Main.SYSPROP_MAX_SHUTDOWN_WAIT_MS);
								currentMaxWaitBeforeShutdown = Long.parseLong(maxWait);
							}
							else {
								currentMaxWaitBeforeShutdown = Main.DEFAULT_MAX_SHUTDOWN_WAIT_MS;
							}

							long newMaxWait = currentMaxWaitBeforeShutdown + (long)(maxRatio * (double)minSleepPeriod);

							LOGGER.info("Adjusting max. wait before forcing JVM shutdown to "
									+ newMaxWait);
							System.setProperty(Main.SYSPROP_MAX_SHUTDOWN_WAIT_MS, newMaxWait+"");
						}

						cfg.setDistributeLazyOTFFactory(factory);
						LOGGER.info("[config] Using HazelDistributeLazyOTF, initialMinCluster=" + initialMinCluster + ", clusterName=" + clusterName);
					}
					catch (NumberFormatException exception) {
						throw new IllegalArgumentException(CaseStudyArgumentIdentifier.USE_DIST_HAZEL.toString()
								+ " expects an integer argument (initialMinCluster).");
					}
				}

				if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.USE_LAZYOTF_TO_LICCOV)) {
					cfg.setInhibitNondetTestGoals(true);
					LOGGER.info("[config] Not recognizing nondet. superstates as test goals.");
				}

				if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.ENABLE_CYCLE_DETECTION_WARNINGS)) {
					try {
						CaseStudyArgument arg = tokenizer.getArgument(CaseStudyArgumentIdentifier.ENABLE_CYCLE_DETECTION_WARNINGS);
						int cwThreshold = Integer.parseInt(arg.getArgument(0));
						cfg.setCycleDetectionEnabled(true);
						cfg.setCycleDetectionThreshold(cwThreshold);
						LOGGER.info("[config] Using cycle detection with a threshold of " + cwThreshold);
					}
					catch (NumberFormatException e) {
						throw new IllegalArgumentException(CaseStudyArgumentIdentifier.ENABLE_CYCLE_DETECTION_WARNINGS.toString()
								+ " expects an integer argument (threshold).");
					}
				}

				if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.DISABLE_LOTF_COMM_LOGGING)) {
					cfg.setCommunicationLoggingEnabled(false);
				}

				return cfg;
			}
		});

		CaseStudyOnLineTestingDriverWrapper.InstantiationHooks.getInstance().addHook(new InstantiationHook() {
			@Override
			public void applyHook(CaseStudyOnLineTestingDriverWrapper target) {

				if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.DRVARG_JTORX_MODE)) {
					target.setPureJTorXModeEnabled(true);
					LOGGER.info("[config] Enabling pure JTorX mode.");
				}

				if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.DRVARG_TO_FINDSHOWMORELICENSES)) {
					target.addJTorXTestObjective(new FindShowMoreLicensesSimpleTO());
					LOGGER.info("[config] Added FindShowMoreLicensesSimpleTO.");
				}

				if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.DRVARG_TO_COVERAGE)) {
					target.addJTorXTestObjective(new CoverageSimpleTO());
					LOGGER.info("[config] Added CoverageSimpleTO.");
				}

				if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.DRVARG_TO_DECREASELICENSECOUNT)) {
					CaseStudyArgument arg = tokenizer.getArgument(CaseStudyArgumentIdentifier.DRVARG_TO_DECREASELICENSECOUNT);
					int maxPort = Integer.parseInt(arg.getArgument(0));
					target.addJTorXTestObjective(new DecreaseLicenseCountSimpleTO(maxPort+1));
					LOGGER.info("[config] Added DecreaseLicenseCountSimpleTO for maxPortion=" + maxPort);
				}

				if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.DRVARG_TO_JTORXLICCOV)) {
					CaseStudyArgument arg = tokenizer.getArgument(CaseStudyArgumentIdentifier.DRVARG_TO_JTORXLICCOV);
					int minLC = Integer.parseInt(arg.getArgument(0));
					int maxLC = Integer.parseInt(arg.getArgument(1));

					target.addJTorXTestObjective(new LicenseCountCoverageSimpleTO(minLC, maxLC));
					LOGGER.info("[config] Added LicenseCountCoverageSimpleTO for minLC=" + minLC + ",maxLC=" + maxLC);
				}

				if (tokenizer.containsArgument(CaseStudyArgumentIdentifier.SLEEP_BETWEEN_TESTSTEPS)) {
					CaseStudyArgument arg = tokenizer.getArgument(CaseStudyArgumentIdentifier.SLEEP_BETWEEN_TESTSTEPS);
					long sleepDuration;
					try {
						sleepDuration = Long.parseLong(arg.getArgument(0));
					}
					catch (NumberFormatException e) {
						throw new IllegalArgumentException(CaseStudyArgumentIdentifier.SLEEP_BETWEEN_TESTSTEPS.toString()
								+ " expects a long argument. Could not parse " + arg.getArgument(0));
					}
					target.setDelayAfterTeststepMs(sleepDuration);
					LOGGER.info("[config] Setting delay between teststeps to " + sleepDuration);
				}
			}

		});

		CaseStudyIO.err = System.err;
		CaseStudyIO.out = System.out;

		if (quietMode) {
			System.setErr(new NullPrintStream());
			System.setOut(new NullPrintStream());
		}

		CaseStudyIO.out.println("CaseStudyMainWrapper Args: " + caseStudyArgs);

		// Create a new argument list for JTorX not containing the case study
		// arguments.
		String[] jtorxArgs = new String[args.length - 1];
		for (int i = 1; i < args.length; i++) {
			jtorxArgs[i - 1] = args[i];
		}

		Main.main(jtorxArgs);

		printTotalCPUTimeFin();

		long wallClockStopTime = System.currentTimeMillis();
		CaseStudyIO.out.print("[time] JTorX_Fin_TotalElapsedWallclockTime: "
				+ (wallClockStopTime - wallclockStartTime) + "\n");

		// Clean up the hooks environment.
		LazyOTFConfigurationHooks.getInstance().reset();
		CaseStudyOnLineTestingDriverWrapper.InstantiationHooks.getInstance().reset();

		if (forceExit) {
			try {
				Thread.sleep(1000);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.err.println("Forcing JVM exit.");
			System.exit(0);
		}
	}

	/**
	 * Prints the process-wide total spent CPU time.
	 */
	public static void printTotalCPUTime(String prefix) {
		long totalCPUTimeSigar = MeasureTime.getCPUTime();
		CaseStudyIO.out.print("[time] JTorX_" + prefix + "_TotalElapsedUntilMainCompleted_Sigar: " + totalCPUTimeSigar + "\n");
		long totalCPUTimeLinux = MeasureTime.getLinuxCPUTime(MeasureTime.getPID());
		CaseStudyIO.out.print("[time] JTorX_" + prefix + "_TotalElapsedUntilMainCompleted_Linux: " + totalCPUTimeLinux + "\n");
		MeasureTime.printLinuxStat(MeasureTime.getPID(), "[procstatus] JTorX_" + prefix + ": ");
	}

	/**
	 * Prints the process-wide total spent CPU time, with prefix <tt>Fin_</tt>.
	 */
	public static void printTotalCPUTimeFin() {
		printTotalCPUTime("Fin");
	}


	/**
	 * Prints the process-wide total spent CPU time, with prefix <tt>Setup_</tt>.
	 */
	public static void printTotalCPUTimeSetup() {
		printTotalCPUTime("Setup");
	}

	/**
	 * A null {@link PrintStream} discarding all printed strings.
	 */
	private static class NullPrintStream extends PrintStream {
		public NullPrintStream() {
			super(System.err);
		}

		@Override
		public void println(String line) {
		}

		@Override
		public void print(String line) {
		}
	}

	/**
	 * Set up Java logging.
	 * @param level The desired log level.
	 */
	private static void setupLogging(Level level) {
		Logger root = Logger.getLogger("");
		root.setLevel(Level.ALL);
		for (Handler handler : root.getHandlers()) {
			if (handler instanceof ConsoleHandler) {
				// java.util.logging.ConsoleHandler.level = ALL
				handler.setLevel(level);
			}
		}
	}
}
