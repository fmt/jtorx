package utwente.fmt.jtorx.torx;

import utwente.fmt.lts.Label;

public interface Adapter {
	AdapterInitializationBasicResult start(); // returns false if starting failed; true if starting succeeded
	void done();
	AdapterInteractionLabelResult applyStimulus(Label l); // returns either l, or label of observation
	AdapterInteractionLabelResult getObservation();
}
