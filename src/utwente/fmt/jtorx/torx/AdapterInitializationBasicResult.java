package utwente.fmt.jtorx.torx;

public interface AdapterInitializationBasicResult {
	Boolean startedWithoutErrors();
	long getStartTime();
	long getEndTime();
}
