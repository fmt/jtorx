package utwente.fmt.jtorx.torx;

import utwente.fmt.lts.SuspensionAutomatonState;

public interface AdapterInitializationExtendedResult extends AdapterInitializationBasicResult {
	SuspensionAutomatonState getAdapterState();
}
