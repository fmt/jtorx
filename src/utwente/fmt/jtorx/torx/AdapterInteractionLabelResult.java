package utwente.fmt.jtorx.torx;

import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.lts.Label;

public interface AdapterInteractionLabelResult {
	Label getLabel();
	Kind getKind();
	long getStartTime();
	long getEndTime();
	long getTimeStamp();
	String getConcrete();
}
