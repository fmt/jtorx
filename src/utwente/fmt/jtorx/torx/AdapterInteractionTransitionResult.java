package utwente.fmt.jtorx.torx;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;

public interface AdapterInteractionTransitionResult<S extends CompoundState>  extends
		AdapterInteractionLabelResult {
	CompoundTransition<S> getTransition();

}
