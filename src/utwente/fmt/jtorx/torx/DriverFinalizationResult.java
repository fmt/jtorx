package utwente.fmt.jtorx.torx;

import java.util.Iterator;

import utwente.fmt.lts.Label;
import utwente.fmt.test.Verdict;

public interface DriverFinalizationResult {
	// to be able to deliver final verdict
	// needed when all test steps succeed, such that
	// no DriverInterActionResult has a verdict.
	public Boolean hasVerdict();
	public Verdict getVerdict();
	public long getStartTime();
	public long getEndTime();
	
	// to be able to give associated verdict for expected outputs for which verdict is not given
	public Verdict getPositiveDefaultVerdict();
	
	public Iterator<? extends Label> getExpected();

	
	public int getStepNr(); // this is currently not used
}
