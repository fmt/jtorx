package utwente.fmt.jtorx.torx;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.test.Verdict;

public interface DriverInitializationResult {
	public CompoundState getState();

	// to be able to deliver initial verdict (errors, or immediate hit or miss in guided setting)
	public Boolean hasVerdict();
	public Verdict getVerdict();
	
	long getStartTime();
	long getEndTime();
}
