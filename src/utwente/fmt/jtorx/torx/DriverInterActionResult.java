package utwente.fmt.jtorx.torx;

import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.test.Verdict;


public interface DriverInterActionResult {

	public Label getLabel();
	public String getConcrete();
	public Boolean hasVerdict();
	public Verdict getVerdict();
	public int getStepNr();
	public Kind getKind();
	public String getChan();
	public String getPCO();
	
	public State getSrc();
	public State getDst();
	public CompoundTransition<? extends CompoundState> getTransition();
	// public CompoundTransition<? extends State> getCompoundTransition();
	
	long getStartTime();
	long getEndTime();
	long getTimeStamp();
}
