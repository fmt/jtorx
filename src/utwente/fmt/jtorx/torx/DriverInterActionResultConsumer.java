package utwente.fmt.jtorx.torx;

public interface DriverInterActionResultConsumer {
	boolean consume(DriverInterActionResult i);
	void end();
}
