package utwente.fmt.jtorx.torx;

import utwente.fmt.lts.CompoundState;

public interface DriverSimInitializationResult extends DriverInitializationResult {
	public CompoundState getAdapterState();
}
