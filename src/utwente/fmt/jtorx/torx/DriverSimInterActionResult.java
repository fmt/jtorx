package utwente.fmt.jtorx.torx;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;

public interface DriverSimInterActionResult extends DriverInterActionResult {

	public CompoundTransition<? extends CompoundState> getAdapterTransition();

}
