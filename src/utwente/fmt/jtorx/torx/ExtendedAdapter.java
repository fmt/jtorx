package utwente.fmt.jtorx.torx;

import utwente.fmt.lts.Label;

public interface ExtendedAdapter extends Adapter {
	Label randomStimulus();
	void printMenu();
	void printState();

}
