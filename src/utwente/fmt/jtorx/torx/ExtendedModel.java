package utwente.fmt.jtorx.torx;

import java.util.Vector;

import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;

public interface ExtendedModel extends Model {
	Vector<Label> getVerdicts(State s);
	Vector<LabelPlusVerdict> getOutputVerdicts(State s);

}
