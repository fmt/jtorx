package utwente.fmt.jtorx.torx;

import java.util.ArrayList;

import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Term;
import utwente.fmt.lts.UnificationBinding;

public class InstantiationImpl implements Instantiation {

	public Label getLabel() {
		return label;
	}
	
	public Term getTerm() {
		return term;
	}

	public InstantiationImpl(Label l, Term t, ArrayList<UnificationBinding> bl) {
		label = l;
		term = t;
		bindings = bl;
	}
	
	private Label label;
	private Term term;
	private ArrayList<UnificationBinding> bindings;
}
