package utwente.fmt.jtorx.torx;

import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public interface Instantiator {
	boolean start(); // returns false if starting failed; true if starting succeeded
	void done();

	Label getInstantiation(Transition<? extends State> t); // returns either null, or instantiation label
}
