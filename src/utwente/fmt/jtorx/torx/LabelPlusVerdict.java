package utwente.fmt.jtorx.torx;

import java.util.ArrayList;
import java.util.Iterator;

import utwente.fmt.jtorx.utils.EmptyIterator;
import utwente.fmt.lts.Action;
import utwente.fmt.lts.Constraint;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Variable;
import utwente.fmt.test.Verdict;

public class LabelPlusVerdict implements Label {
	
	public Label getLabel() {
		return label;
	}
	public Verdict getVerdict() {
		return verdict;
	}

	public LabelPlusVerdict(Label l, Verdict v) {
		label = l;
		verdict = v;
	}
	
	public Boolean eq(Label l) {
		return label.eq(l);
	}
	public Action getAction() {
		return label.getAction();
	}
	public String getString() {
		if (label!=null && verdict!=null)
			return label.getString()+" : "+verdict.getString();
		else if (label!=null)
			return label.getString();
		else if (verdict!=null)
			return "(null) : "+verdict.getString();
		else
			return "(null)";
	}
	public Boolean isObservable() {
		return label.isObservable();
	}
	public Iterator<Variable> getVariables() {
		if (variables==null)
			return EmptyIterator.iterator();
		return variables.iterator();
	}
	
	public Constraint getConstraint() {
		if (label==null)
			return null;
		return label.getConstraint();
	}


	
	private Label label;
	private Verdict verdict;
	private ArrayList<Variable> variables = null;

}
