package utwente.fmt.jtorx.torx;

import java.util.Vector;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.test.Verdict;

// ModelAccess is probably a better name
public interface Model {
	CompoundState start() throws LTS.LTSException;
	void done();
	
	Vector<Label> menu(CompoundState s);
	CompoundTransition<? extends CompoundState> next(CompoundState s, Label l);
	
	Transition<? extends State> findTransition(CompoundState s, Label l);
	
	Vector<Label> getInputs(CompoundState s);
	Vector<Label> getOutputs(CompoundState s);
	
	Verdict getNegativeDefaultVerdict();
	Verdict getPositiveDefaultVerdict();
	
}
