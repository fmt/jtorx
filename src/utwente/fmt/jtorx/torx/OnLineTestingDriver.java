package utwente.fmt.jtorx.torx;

import java.util.Vector;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.Label;

public interface OnLineTestingDriver {
	DriverInitializationResult init();
	DriverFinalizationResult finish();
	
	DriverInterActionResult stimulate(Label l);
	DriverInterActionResult stimulate();
	DriverInterActionResult observe();
	
	Vector<Label> getModelOutputs();
	Vector<LabelPlusVerdict> getModelOutputVerdicts();
	Vector<Label> getModelInputs();
	CompoundState getTesterState();
	
	DriverInterActionResult randomStep();
	boolean autoStep(int n, DriverInterActionResultConsumer i);
	boolean autoStep(DriverInterActionResultConsumer i);
	
	void stopAuto();

}
