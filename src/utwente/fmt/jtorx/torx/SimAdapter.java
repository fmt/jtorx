package utwente.fmt.jtorx.torx;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.Label;

public interface SimAdapter extends Adapter {
	AdapterInitializationExtendedResult start();
	void done();
	AdapterInteractionTransitionResult<? extends CompoundState> applyStimulus(Label l);
	AdapterInteractionTransitionResult<? extends CompoundState> getObservation();
}
