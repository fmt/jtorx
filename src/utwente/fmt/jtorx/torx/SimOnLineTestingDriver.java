package utwente.fmt.jtorx.torx;

import utwente.fmt.lts.Label;

public interface SimOnLineTestingDriver extends OnLineTestingDriver {
	DriverSimInitializationResult init();
	DriverSimInterActionResult stimulate(Label l);
	DriverSimInterActionResult stimulate();
	DriverSimInterActionResult observe();

}
