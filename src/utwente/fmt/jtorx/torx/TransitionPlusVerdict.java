package utwente.fmt.jtorx.torx;

import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.test.Verdict;

// This currently unused!

public class TransitionPlusVerdict<S extends CompoundState> implements CompoundTransition<S > {
	
	public CompoundTransition<? extends CompoundState> getTransition() {
		return trans;
	}
	public Verdict getVerdict() {
		return verdict;
	}

	public TransitionPlusVerdict(CompoundTransition<S> l, Verdict v) {
		trans = l;
		verdict = v;
	}
	
	public String getString() {
		if (trans!=null && verdict!=null)
			return trans.getLabel().getString()+" : "+verdict.getString();
		else if (trans!=null)
			return trans.getLabel().getString();
		else if (verdict!=null)
			return "(null) : "+verdict.getString();
		else
			return "(null)";
	}
	
	private CompoundTransition<S> trans;
	private Verdict verdict;
	
	public S getDestination() {
		return trans.getDestination();
	}
	public S getSource() {
		return trans.getSource();
	}
	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return trans.getSubTransitions();
	}
	public CompoundTransition<S> instantiate(Instantiation i) {
		return trans.instantiate(i);
	}
	public Label getSolution() {
		return trans.getSolution();
	}
	public String getEdgeName() {
		return trans.getEdgeName();
	}
	public Label getLabel() {
		return trans.getLabel();
	}
	public Position getLabelPosition() {
		return trans.getLabelPosition();
	}
	public Vector<Position> getPosition() {
		return trans.getPosition();
	}

}
