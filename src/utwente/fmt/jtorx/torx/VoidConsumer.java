package utwente.fmt.jtorx.torx;

public interface VoidConsumer {
	boolean consume();
	void end();

}
