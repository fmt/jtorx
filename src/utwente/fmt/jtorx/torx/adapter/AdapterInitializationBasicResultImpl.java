package utwente.fmt.jtorx.torx.adapter;

import utwente.fmt.jtorx.torx.AdapterInitializationBasicResult;

public class AdapterInitializationBasicResultImpl implements
		AdapterInitializationBasicResult {

	public Boolean startedWithoutErrors() {
		return startedOk;
	}
	
	public long getStartTime() {
		return startTime;
	}
	public long getEndTime() {
		return endTime;
	}

	public AdapterInitializationBasicResultImpl(long st, long et, Boolean ok) {
		startedOk = ok;
		startTime = st;
		endTime = et;
	}

	private boolean startedOk;
	private long startTime;
	private long endTime;
}
