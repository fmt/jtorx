package utwente.fmt.jtorx.torx.adapter;

import utwente.fmt.jtorx.torx.AdapterInitializationExtendedResult;
import utwente.fmt.lts.SuspensionAutomatonState;

public class AdapterInitializationExtendedResultImpl implements
		AdapterInitializationExtendedResult {

	public SuspensionAutomatonState getAdapterState() {
		return state;
	}

	public Boolean startedWithoutErrors() {
		System.err.println("AdapterInitializationExtendedResult: returning start result: "+startedOk);
		return startedOk;
	}
	
	public long getStartTime() {
		return startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	
	public AdapterInitializationExtendedResultImpl(long st, long et, Boolean ok) {
		this(st, et, ok, null);
	}
	public AdapterInitializationExtendedResultImpl(long st, long et, Boolean ok, SuspensionAutomatonState s) {
		state = s;
		startTime = st;
		endTime = et;
		startedOk = ok;
	}
	
	private SuspensionAutomatonState state;
	private boolean startedOk;
	private long startTime;
	private long endTime;
}
