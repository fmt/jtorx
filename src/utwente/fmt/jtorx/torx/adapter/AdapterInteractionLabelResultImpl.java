package utwente.fmt.jtorx.torx.adapter;

import utwente.fmt.jtorx.torx.AdapterInteractionLabelResult;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.lts.Label;

public class AdapterInteractionLabelResultImpl implements
		AdapterInteractionLabelResult {

	public Label getLabel() {
		return label;
	}
	
	public String getConcrete() {
		return concrete;
	}
	
	public Kind getKind() {
		return kind;
	}
	
	public long getStartTime() {
		return startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	public long getTimeStamp() {
		return timeStamp;
	}


	public AdapterInteractionLabelResultImpl(long startTime, long endTime, long timeStamp, Label l, Kind k, String c) {
		label = l;
		kind = k;
		this.startTime = startTime;
		this.endTime = endTime;
		this.timeStamp = timeStamp;
		concrete = c;
	}

	private Label label;
	private Kind kind;
	private long startTime;
	private long endTime;
	private long timeStamp;
	private String concrete;
}
