package utwente.fmt.jtorx.torx.adapter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.SuspensionAutomatonState;
import utwente.fmt.lts.Transition;
import utwente.fmt.test.Verdict;

// We now use AdaptState also internally.
// It is a wrapper around an underlying LTS state.
// With the AdaptState we keep the internal transitions and
// the internal states that we walk through in an RandomWalk.


public class AdapterState implements SuspensionAutomatonState {
	
	public AdapterState(State s) {
		if (s==null)
			System.err.println("oops...AdaptState: s = null");
		s.getID(); // just to force use, to trigger bug
		states.add(s);
		cur = s;
	}
	
	public State cur() {
		if (cur==null)
			System.err.println("oops...AdaptState.cur: cur = null");
		return cur;
	}

	private Set<State> states = new HashSet<State>();
	private Set<State> indStates = new HashSet<State>();
	private State cur =  null;

	public Iterator<? extends State> getSubStates() {
		return states.iterator();
	}
	public Iterator<? extends State> getSubStates(LTS l) {
		return getSubStates();
	}
	public Iterator<? extends State> getDirectSubStates() {
		return getSubStates();
	}
	public Iterator<? extends State> getIndirectSubStates() {
		return indStates.iterator();
	}


	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return trans.iterator();
	}
	public String getSubEdgeName() {
		return null;
	}


	public String getID() {
		if (cur==null)
			System.err.println("oops...AdaptState.getID: cur = null");

		return cur.getID();
	}
	public AdapterState getCanonical() {
		return this;
	}
	public Position getPosition() {
		return null;
	}
	public String getNodeName() {
		return null;
	}
	
	// TODO check whether this is enough
	public String getLabel(boolean quote) {
		String s = "";
		s += "[ ";
		for (State p: states)
			s+= p.getLabel(true)+" ";
		for (State p: indStates)
			s+= p.getLabel(true)+" ";
		s += "]";
		return s;
	}

	public CompoundLTS getLTS() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Iterator<? extends CompoundTransition<? extends CompoundState>> menu(
			LabelConstraint l) {
		if (cur==null)
			System.err.println("oops...AdaptState.menu: cur = null");
	// TODO Auto-generated method stub
		return null;
	}
	public CompoundTransition<? extends CompoundState> next(Label l) {
		return null;
	}
	public void disposeTransitions() {
	}
	
	public void copySubTransitions(AdapterState a) {
		Iterator<?extends Transition<?extends State>> sub_it = a.getSubTransitions();
		while(sub_it.hasNext()) {
			Transition<?extends State> sub_t = sub_it.next();
			addTrans(sub_t);
			dbgMsg("adapt-sim-impl: copying-sub-trans ("+sub_t.getSource().getID()+" "+sub_t.getLabel().getString()+" "+sub_t.getDestination().getID()+")");
		}
	}
	
	public void copySubState(AdapterState a) {
		Iterator<?extends State> sub_it = a.getSubStates();
		while(sub_it.hasNext()) {
			State sub_s = sub_it.next();
			addIndirectState(sub_s);
			dbgMsg("adapt-sim-impl: copying-sub-state ("+sub_s.getID()+")");
		}
	}
	
	public void addTrans(Transition<?extends State> t) {
		trans.add(t);
		dbgMsg("adapt-sim-impl: addTrans("+getID()+") ("+t.getSource().getID()+" "+t.getLabel().getString()+" "+t.getDestination().getID()+")");

	}
	private void addIndirectState(State s) {
		indStates.add(s);
	}
	private Collection<Transition<?extends State>> trans = new Vector<Transition<?extends State>>();

	public Verdict getVerdict() {
		// TODO Auto-generated method stub
		return null;
	}


	private void dbgMsg(String s) {
		if (debug)
			System.out.println(s);
	}

	private boolean debug = false;

}