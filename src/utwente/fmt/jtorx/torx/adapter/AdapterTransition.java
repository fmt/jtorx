package utwente.fmt.jtorx.torx.adapter;

import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class  AdapterTransition<S extends AdapterState> implements CompoundTransition<AdapterState>, AdapterInteractionTransitionResult<AdapterState> {

		public Iterator<? extends Transition<? extends State>> getSubTransitions() {
			return trans.iterator();
		}

		public AdapterState getDestination() {
			return dst;
		}
		public Label getLabel() {
			return lbl;
		}
		public String getConcrete() {
			if (src==null && dst==null)
				return concrete;
			if (lbl==null || lbl.getString().equals("delta"))
				return null;
			return lbl.getString();
		}

		public Kind getKind() {
			return kind;
		}

		public AdapterState getSource() {
			return src;
		}
		
		public void add(Transition<?extends State> t) {
			trans.add(t);
		}
		
		public long getStartTime() {
			return startTime;
		}
		public long getEndTime() {
			return endTime;
		}
		public long getTimeStamp() {
			return timeStamp;
		}
		
		public AdapterTransition<S> instantiate(Instantiation i) {
			return this;
		}
		public Label getSolution() {
			return null;
		}
		
		public Vector<Position> getPosition() {
			return null;
		}
		public Position getLabelPosition() {
			return null;
		}
		public String getEdgeName() {
			return null;
		}
		
		public CompoundTransition<AdapterState> getTransition() {
			return this;
		}
		public AdapterTransition(long st, long et, long ts, Label l, Kind k, String c) {
			this(st, et, ts, null, l, k, null);
			concrete = c;
		}


		public AdapterTransition(long st, long et, long ts, AdapterState s, Label l, Kind k, AdapterState d) {
			src = s;
			lbl = l;
			dst = d;
			kind = k;
			startTime = st;
			endTime = et;
			timeStamp = ts;
			concrete = null;
		}
		private Vector<Transition<?extends State>> trans = new Vector<Transition<?extends State>>();
		private AdapterState src;
		private Label lbl;
		private Kind kind;
		private AdapterState dst;
		private long startTime;
		private long endTime;
		private long timeStamp;
		private String concrete;

	}
