package utwente.fmt.jtorx.torx.adapter;

public class Observation<D>{
	  public Observation(long t, D d){ 
	    timeStamp = t;
	    data = d;   
	  }
	  public Observation(){ 
	  }
	  public void fill(long t, D d){ 
		    timeStamp = t;
		    data = d;   
	  }

	  public long getTimeStamp(){
	    return timeStamp;
	  }

	  public D getData()   {
	    return data;
	  }

	  public String toString()  { 
	    return "(" + timeStamp + ", " + data.toString() + ")"; 
	  }
	 
	  private  long timeStamp;
	  private  D data;
	}
