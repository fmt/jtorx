package utwente.fmt.jtorx.torx.adapter;

import java.io.IOException;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.concurrent.BlockingQueue;

import utwente.fmt.jtorx.ui.ErrorReporter;


public class StreamReader extends Thread {
	private Reader is;
	final int ARRAYSIZE=1024;
	private String buf = "";
	private String name;
	private BlockingQueue<Observation<String>> obsQueue;
	private ErrorReporter errorReporter;
	private long obsCount = 0;

	public StreamReader(String n, Reader is, BlockingQueue<Observation<String>> obsQueue, ErrorReporter errorReporter) {
		this.is = is;
		this.name = n;
		this.obsQueue = obsQueue;
		this.errorReporter = errorReporter;
	}

	public void run() {
		try {
			int n;
			char[] cbuf = new char[ARRAYSIZE];
			Observation<String> p;
			p = new Observation<String>();
			long obsTime;
			long timeStamp;
			while (true /*err.ready()*/){	
				n  = is.read(cbuf, 0, ARRAYSIZE);
				if (n <= 0)
					return;
				obsTime = System.currentTimeMillis();
				timeStamp = TimeSource.getCurrentMonotonicTime();
				buf += new String(cbuf,0,n);
				int i = buf.indexOf("\n");
				while (i >= 0) {
					obsCount++;
					// NOTE for strings that we see in two or more parts,
					// the timestamp is for the _last_ part that we see
					String s = buf.substring(0,i);
					// TODO check that we do not spent too much time here;
					// possible optimizations:
					// - preallocate+reuse Pair objects
					// - pre-fill/empty the obsQueu upto some given capacity, to pre-allocate link blocks (or will that work against us?)
					// - more efficient bug string operations
					p.fill(timeStamp,s);
					obsQueue.put(p);
					//Thread.yield();
					report(obsTime, "streamreader "+obsCount+" : "+s);
					buf = buf.substring(i+1);
					i = buf.indexOf("\n");
					p = new Observation<String>();

				}
			}
		} catch (IOException e) {
			report("streamreader io exception: "+e.getMessage());
			if (!e.getMessage().equalsIgnoreCase("Socket closed"))
				e.printStackTrace();
			try {
				is.close();
			} catch (IOException e1) {
				System.err.println(name+" stdinout-adapter streamreader close io exception: "+e1.getMessage());
				e1.printStackTrace();
			}

		} catch (InterruptedException e) {
			report("streamreader io exception: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public long getObsCount() {
		return obsCount;
	}
	
	private void report(String s) {
		long now = System.currentTimeMillis();
		System.out.println(format.format(now)+" "+s);
		errorReporter.log(now, name, s);
	}
	private void report(long t, String s) {
		long now = System.currentTimeMillis();
		String n = format.format(now);
		System.out.println(n+" "+s);
		errorReporter.log(t, name, s+" ("+n+")");
	}
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");


}
