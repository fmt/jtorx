package utwente.fmt.jtorx.torx.adapter;

public class TimeSource {
	static public long getCurrentMonotonicTime() {
		// return System.currentTimeMillis()
		return System.nanoTime()/1000;
	}
}
