package utwente.fmt.jtorx.torx.adapter.iocolts;

import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.torx.AdapterInitializationExtendedResult;
import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.ExtendedModel;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.adapter.AdapterInitializationExtendedResultImpl;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.torx.model.ExtendedModelImpl;
import utwente.fmt.jtorx.torx.primer.Primer;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.lts.Action;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.LTS.LTSException;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.Variable;

/**
 * An adapter for simulating ioco tests without having
 * <i>inconclusive</i> test results, i.e. the adapter maintains compound
 * states and can effectively be "forced" into a state by the tester when
 * the current state is nondeterministic and the test would otherwise
 * be inconclusive. For example, if the current menu contains ?a stemming
 * from state A and ?b stemming from state B and the inputs of A and B
 * are disjoint, stimulating with ?a forces this adapter into a compound
 * state containing A. This situation might lead to an <i>inconclusive</i>
 * result when testing a real SUT, which might be in state B and might not
 * accept ?a. This way, we can simulate <i>hit</i> test runs without
 * using angelic completion.
 * 
 * To this end, a primer is used to implement the adapter.
 */
public class IocoAdapterSimImpl implements SimAdapter {
    /** The label interpretation. */
    private final IOLTSInterpretation interp;
    
    /** Output-or-quiescence label constraint. */
    private final LabelConstraint outputOrQuiescenceLabelConstr;
    
    /** The simulation's primer. */
    private final Primer primer;
    
    /** The simulation's model. */
    private final ExtendedModel model;
    
    /** The system's error reporter. */
    private final ErrorReporter err;
    
    /** The system's progress reporter. */
    private final ProgressReporter progress;
    
    /** The random number generator used to choose observations. */
    private final RandomGenerator random;
    
    /** The current adapter state. */
    private CompoundState currentState;
    
    /** The initial adapter state. */
    private final CompoundState initState;
    
    /** Set this to {@code true} to enable debug outputs. */
    private final boolean debug = false;
    
    private boolean synthDelta;
    
    /**
     * Constructs a new {@link IocoAdapterSimImpl} instance.
     * @param lts The simulated LTS.
     * @param interp The label interpretation.
     * @param seed The random number generator seed.
     * @param synthDelta Synthesize delta transitions.
     * @param err The system's error reporter.
     * @param progress The system's progress reporter.
     */
    public IocoAdapterSimImpl(LTS lts, IOLTSInterpretation interp, long seed,
            boolean synthDelta, ErrorReporter err, ProgressReporter progress) {
        
        if (lts == null) {
            throw new IllegalArgumentException("Must supply an LTS.");
        }
        
        this.interp = interp;
        this.err = err;
        
        if (progress != null) {
            this.progress = progress;
        }
        else {
            this.progress = new NullProgressReporter();
        }
        
        this.primer = new Primer(lts, interp,
                false, /* no traces only */ 
                false, /* no remember traces */
                TraceKind.STRACES, /* This adapter is only useful when using straces */
                false, /* no angelic completion */
                false, /* no delta for tau transitions */
                this.err, this.progress);
        
        if (seed != 0) {
            this.random = new MersenneTwister(seed);
        }
        else {
            this.random = new MersenneTwister();
        }
        
        this.model = new ExtendedModelImpl(primer, interp, err);
        
        try {
            currentState = model.start();
        }
        catch (LTSException e) {
            String msg = "The adapter was equipped with a bad LTS:\n" + e.toString();
            err.log(System.currentTimeMillis(), getClass().getName(), msg);
            throw new IllegalStateException(msg);
        }
        
        this.initState = currentState;
        this.outputOrQuiescenceLabelConstr = new OutputOrQuiescenceConstraint(interp.isOutput());
        this.synthDelta = synthDelta;
    }
    
    @Override
    public AdapterInitializationExtendedResult start() {
        long startTime = System.currentTimeMillis();
        return new AdapterInitializationExtendedResultImpl(startTime, startTime, 
                (initState != null), new IocoAdapterState(initState));
    }

    @Override
    public void done() {
        primer.done();
    }

    /**
     * Process a fully instantiated interaction.
     * @param l The interaction label.
     * @param trans The interaction transition.
     * @return The resulting {@link IocoAdapterTransition}.
     */
    private AdapterInteractionTransitionResult<? extends CompoundState> processInteraction(
            Label l, CompoundTransition<? extends CompoundState> trans) {
        long startTime = System.currentTimeMillis();
        currentState = trans.getDestination();
        long stopTime = System.currentTimeMillis();
        
        Kind transKind;
        if (interp.isInput().isSatisfiedBy(l)) {
            transKind = Kind.INPUT;
        }
        else {
            transKind = Kind.OUTPUT;
        }
        
        return new IocoAdapterTransition<>(l, transKind, startTime, stopTime, 0, null, trans);        
    }
    
    @Override
    public AdapterInteractionTransitionResult<? extends CompoundState> applyStimulus(Label l) {
        if (debug) {
            System.err.println("ADAPTER applying stimulus " + l.getString());
        }
        CompoundTransition<? extends CompoundState> next = model.next(currentState, l);
        return processInteraction(l, next);
    }

    /**
     * @param vector A nonempty vector.
     * @param <T> The element type.
     * @return a random element from {@link vector}.
     */
    private <T> T getRandom(Vector<T> vector) {
        if (vector == null || vector.size() == 0) {
            return null;
        }
        else {
            int idx = random.nextInt(vector.size());
            return vector.get(idx);
        }
    }
    
    @Override
    public AdapterInteractionTransitionResult<? extends CompoundState> getObservation() {
        if (debug) {
            System.err.println("ADAPTER Getting observation...");
        }
        //Vector<Label> outputLabels = model.getOutputs(currentState);
        //Label targetCandidate = getRandom(outputLabels);
        
        /* Select state at random */
        Iterator<? extends State> subStates = currentState.getSubStates();
        Vector<State> subStateVec = new Vector<>();
        while (subStates.hasNext()) {
            subStateVec.add(subStates.next());
        }
        State randomState = getRandom(subStateVec);
        
        /* Get the current state's output transitions. */
        Iterator<? extends Transition<? extends State>> transIt = randomState.menu(interp.isOutput());
        Vector<Transition<? extends State>> transVec = new Vector<>();
        while (transIt.hasNext()) {
            Transition<? extends State> nextTrans = transIt.next();
            transVec.add(nextTrans);
        }
        
        /* Select a transition at random, or delta if synthDelta and
         * outputs are empty.
         */
        Transition<? extends State> randomTrans = null;
        Label targetCandidate;
        if (transVec.size() == 0 && synthDelta) {
            targetCandidate = new LibLabel("delta");
        }
        else {
            randomTrans = getRandom(transVec);
            targetCandidate = randomTrans.getLabel();
        }
        
        if (debug) {
            System.err.println("ADAPTER Observation candidate: " + targetCandidate.getString());
            if (targetCandidate.getConstraint() != null)
                System.err.println("ADAPTER Obs constraint: " + targetCandidate.getConstraint().getString());
        }
        
        Label target;
        Iterator<Variable> variables = targetCandidate.getVariables();
        if (variables == null || !variables.hasNext()) {
            target = targetCandidate;
        }
        else {
            target = randomTrans.getSolution();
        }
        
        if (debug) {
            System.err.println("ADAPTER Observation: " + target.getString());
        }
        
        CompoundTransition<? extends CompoundState> next = model.next(currentState, target);
        return processInteraction(target, next);
    }

    /**
     * A {@link LabelConstraint} implementation for output or quiescence
     * labels.
     */
    private static class OutputOrQuiescenceConstraint implements LabelConstraint {
        private final LabelConstraint outputConstraint;
        
        public OutputOrQuiescenceConstraint(LabelConstraint outputConstraint) {
            this.outputConstraint = outputConstraint;
        }
        
        @Override
        public Boolean isSatisfiedBy(Label l) {
            Action a = l.getAction();
            return outputConstraint.isSatisfiedBy(l) || a.getString().equalsIgnoreCase("delta");
        }
        
    }
}
