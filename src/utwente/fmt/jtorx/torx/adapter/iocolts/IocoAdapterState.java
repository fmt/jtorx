package utwente.fmt.jtorx.torx.adapter.iocolts;

import java.util.HashSet;
import java.util.Iterator;

import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.SuspensionAutomatonState;
import utwente.fmt.lts.Transition;
import utwente.fmt.test.Verdict;

public class IocoAdapterState implements SuspensionAutomatonState {

    private final CompoundState decoratedState;
    
    public IocoAdapterState(CompoundState s) {
        this.decoratedState = s;
    }
    
    @Override
    public Iterator<? extends CompoundTransition<? extends CompoundState>> menu(LabelConstraint l) {
        return decoratedState.menu(l);
    }

    @Override
    public CompoundTransition<? extends CompoundState> next(Label l) {
        return decoratedState.next(l);
    }

    @Override
    public CompoundLTS getLTS() {
        return decoratedState.getLTS();
    }

    @Override
    public Iterator<? extends State> getSubStates() {
        return decoratedState.getSubStates();
    }

    @Override
    public Iterator<? extends State> getSubStates(LTS l) {
        return decoratedState.getSubStates(l);
    }

    @Override
    public Iterator<? extends Transition<? extends State>> getSubTransitions() {
        return decoratedState.getSubTransitions();
    }

    @Override
    public String getSubEdgeName() {
        return decoratedState.getSubEdgeName();
    }

    @Override
    public void disposeTransitions() {
    }

    @Override
    public Verdict getVerdict() {
        return decoratedState.getVerdict();
    }

    @Override
    public String getID() {
        return decoratedState.getID();
    }

    @Override
    public String getLabel(boolean quote) {
        return decoratedState.getLabel(quote);
    }

    @Override
    public Position getPosition() {
        return decoratedState.getPosition();
    }

    @Override
    public String getNodeName() {
        return decoratedState.getNodeName();
    }

    @Override
    public Iterator<? extends State> getDirectSubStates() {
        return decoratedState.getSubStates();
    }

    @Override
    public Iterator<? extends State> getIndirectSubStates() {
        return new HashSet<State>().iterator();
    }

    @Override
    public SuspensionAutomatonState getCanonical() {
        return this;
    }

}
