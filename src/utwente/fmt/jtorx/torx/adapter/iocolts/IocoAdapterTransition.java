package utwente.fmt.jtorx.torx.adapter.iocolts;

import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Label;

public class IocoAdapterTransition<S extends CompoundState>
    implements AdapterInteractionTransitionResult<S> {

    private final Label label;
    private final Kind kind;
    private final long startTime;
    private final long endTime;
    private final long timeStamp;
    private final String concrete;
    private final CompoundTransition<S> transition;
    
    public IocoAdapterTransition(Label label, Kind kind, long startTime,
            long endTime, long timeStamp, String concrete,
            CompoundTransition<S> transition) {
        this.label = label;
        this.kind = kind;
        this.startTime = startTime;
        this.endTime = endTime;
        this.timeStamp = timeStamp;
        this.concrete = concrete;
        this.transition = transition;
    }
    
    @Override
    public Label getLabel() {
        return label;
    }

    @Override
    public Kind getKind() {
        return kind;
    }

    @Override
    public long getStartTime() {
        return startTime;
    }

    @Override
    public long getEndTime() {
        return endTime;
    }

    @Override
    public long getTimeStamp() {
        return timeStamp;
    }

    @Override
    public String getConcrete() {
        if(transition.getSource() == null || transition.getDestination() == null) {
            return concrete;
        }
        if(label == null || label.getString().equals("delta")) {
            return null;
        }
        return label.getString();
    }

    @Override
    public CompoundTransition<S> getTransition() {
        return transition;
    }

}
