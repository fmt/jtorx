package utwente.fmt.jtorx.torx.adapter.log;


import java.util.Iterator;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.torx.AdapterInitializationExtendedResult;
import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.jtorx.torx.adapter.AdapterInitializationExtendedResultImpl;
import utwente.fmt.jtorx.torx.adapter.AdapterState;
import utwente.fmt.jtorx.torx.adapter.AdapterTransition;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;



// TODO keep track of time of last interaction,
// such that we compute time-to-wait as
// timeout - (timecur - timelast)

public class LogAdapter implements SimAdapter{
	
	
	public AdapterInteractionTransitionResult<? extends CompoundState> applyStimulus(Label l) {
		return doStep();
	}
	public void done() {
		lts.done();
	}
	public AdapterInteractionTransitionResult<? extends CompoundState> getObservation() {
		return doStep();
	}
	public AdapterInitializationExtendedResult start() {
		System.err.println("LogAdapter.start");
		long startTime = System.currentTimeMillis();
		curState = initState;
		System.err.println("LogAdapter.started");
		return new AdapterInitializationExtendedResultImpl(startTime, startTime, (initState!=null), initState);
	}

	
	public LogAdapter (LTS l, IOLTSInterpretation i, long s, boolean sd) {
		lts = l;
		interp = i;

		System.err.println("LogAdapter initializing lts="+lts);
		Iterator<?extends State> it = null;
		try {
			it = lts.init();
		} catch (LTSException e) {
			System.err.println("LogAdapter: caught LTSException: "+e.getMessage());
			it = null;
		}
		System.err.println("LogAdapter initializing it="+it);
		if(it!=null && it.hasNext()) {
			System.err.println("LogAdapter initializing it has next");
			initState = new AdapterState(it.next());
			System.err.println("LogAdapter initializing used it next");
		} else
			System.err.println("LogAdapter: cannot initialize (lts.init returns 0 states)");
		System.err.println("LogAdapter initialized");
	}
	
	private AdapterInteractionTransitionResult<? extends CompoundState> doStep() {
		long startTime = System.currentTimeMillis();
		Iterator<? extends Transition<? extends State>> tit = curState.cur().menu(interp.isAny());
		if (tit.hasNext()) {
			Transition<? extends State> t = tit.next();
			AdapterState src = curState;
			AdapterState dst = new AdapterState(t.getDestination());
			curState = dst;
			System.out.println("\t"+t.getLabel().getString()+ " -> "+t.getDestination().getID());
			Label l = t.getLabel();
			Kind k;
			if (interp.isInput().isSatisfiedBy(l))
				k = Kind.INPUT;
			else
				k = Kind.OUTPUT;
			return new AdapterTransition<AdapterState>(startTime, System.currentTimeMillis(), 0, src, l, k, dst);
		} else
			return null;
	}


	
	private LTS lts;
	private IOLTSInterpretation interp;
	private AdapterState curState;
	private AdapterState initState;


}
