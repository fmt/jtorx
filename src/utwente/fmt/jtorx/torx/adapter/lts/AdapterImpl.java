package utwente.fmt.jtorx.torx.adapter.lts;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.torx.AdapterInitializationBasicResult;
import utwente.fmt.jtorx.torx.AdapterInteractionLabelResult;
import utwente.fmt.jtorx.torx.ExtendedAdapter;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;


public class AdapterImpl implements ExtendedAdapter {
	
	public AdapterInteractionLabelResult applyStimulus(Label l) {
		return adapter.applyStimulus(l);
	}
	public void done() {
		adapter.done();
	}
	
	public AdapterInteractionLabelResult getObservation() {
		return adapter.getObservation();
	}
	
	public AdapterInitializationBasicResult start() {
		return adapter.start();
	}
	
	
	public void printMenu() {
		adapter.printMenu();
		
	}
	public void printState() {
		adapter.printState();
		
	}
	public Label randomStimulus() {
		return adapter.randomStimulus().getLabel();
	}

	
	public AdapterImpl (LTS l, IOLTSInterpretation i, long s, boolean sd) {
		adapter = new AdapterSimImpl(l, i, s, sd);
	}
	
	private AdapterSimImpl adapter;

}
