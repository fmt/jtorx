package utwente.fmt.jtorx.torx.adapter.lts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.Vector;


import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.torx.AdapterInitializationExtendedResult;
import utwente.fmt.jtorx.torx.InstantiationImpl;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.adapter.AdapterState;
import utwente.fmt.jtorx.torx.adapter.AdapterTransition;
import utwente.fmt.jtorx.torx.adapter.AdapterInitializationExtendedResultImpl;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.jtorx.utils.term.TermParser;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Term;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.UnificationResult;
import utwente.fmt.lts.Variable;
import utwente.fmt.lts.LTS.LTSException;

public class AdapterSimImpl implements SimAdapter {
	
	// This implements more than basic adapter functionality:
	// it also allows printing menu and state.
	// Probably we should have an extended Adapter interface
	// that has this functionality.
	
	// the functionality here is ok to work as
	// implementation simulator with which the tester interacts
	// using applyStimulus and getObservation
	
	// the interactive features are less OK:
	// menu gives the plain lts menu of a state
	//  - includes internal actions
	//  - does not synthesize quiescence (delta) actions
	
	// an attempt is made to make goRandom and goLabel general
	// but that does not completely work right now:
	//  - goLabel does not know about quiescence (delta) actions
	
	// alternative approach (not implemented here)
	//  - global setting: lts or ioco (in case of ioco synth delta)
	//  - whenever we 'stay' in a state, compute menu using global setting
	//  - goLabel and goRandom use the menu, instead of computing their own
	// as currently done, at end of goRandom and goLabel we may do some
	// random walking through internal steps before we end up in a state
	// where we compute the menu
	
	// what is menu? two possibilities
	//  - 3 lists of transitions (input, output, internal)
	//     same label may be on multiple transitions;
	//     -> pick transition: have label and dest
	//  - 3 mappings from label to set of destinations
	//     same label occurs at most once in map
	//     -> first choose label, then destination
	
	// difference between this alternative approach and the Primer:
	// here we only look/do for a single state, not a set of states
	//
	// We now use AdaptState also internally.
	// It is a wrapper around an underlying LTS state.
	// With the AdaptState we keep the internal transitions and
	// the internal states that we walk through in an RandomWalk.
	
		
	public AdapterInitializationExtendedResult start() {
		long startTime = System.currentTimeMillis();
		return new AdapterInitializationExtendedResultImpl(startTime, startTime, (initState!=null), initState);
	}

	public void done() {
		lts.done();
	}
	

	
	// this gives the plain menu, does not add quiescence
	public void printMenu () {
		Iterator<? extends Transition<? extends State>> tit = curState.cur().menu(interp.isAny());
		while (tit.hasNext()) {
			Transition<? extends State> t = tit.next();
			System.out.println("\t"+t.getLabel().getString()+ " -> "+t.getDestination().getID());
		}
	}
	public void printState() {
		System.out.println("\t"+curState.getID());
	}

	
	public AdapterTransition<?extends State> applyStimulus(Label l) {
		if(debug) System.out.println("adaptSimImpl.applyStimulus start...");
		long startTime = System.currentTimeMillis();
		AdapterTransition<?extends State> res;
		int choice = 0;
		if (outputsPending())
			choice = rand.nextInt(2);
		if (choice==1) {
			if(debug) System.out.println("adaptSimImpl.applyStimulus... sut already produced output");
			res = goRandom(startTime, "output", interp.isOutput(), Kind.OUTPUT, synthDelta, false);
		} else {
			if(debug) System.out.println("adaptSimImpl.applyStimulus... sut accepts input");
			res = goLabel(startTime, l, "input", interp.isInput(), Kind.INPUT);
		}
		if(debug) System.out.println("adaptSimImpl.applyStimulus... done");
		return res;
	}
	public AdapterTransition<?extends State> randomStimulus() {
		if(debug) System.out.println("adaptSimImpl.randomStimulus start...");
		long startTime = System.currentTimeMillis();
		AdapterTransition<?extends State> res;
		int choice = 0;
		if (outputsPending())
			choice = rand.nextInt(2);
		if (choice==1) {
			if(debug) System.out.println("adaptSimImpl.randomStimulus... sut already produced output");
			res = goRandom(startTime, "output", interp.isOutput(), Kind.OUTPUT, synthDelta, false);
		} else {
			if(debug) System.out.println("adaptSimImpl.randomStimulus... sut accepts input");
			res =  goRandom(startTime, "input", interp.isInput(), Kind.INPUT, false, false);
		}
		if(debug) System.out.println("adaptSimImpl.randomStimulus... done");
		return res;
	}
	public AdapterTransition<?extends State> getObservation() {
		if(debug) System.out.println("adaptSimImpl.getObservation start...");
		long startTime = System.currentTimeMillis();
		AdapterTransition<?extends State> res =  goRandom(startTime, "output", interp.isOutput(), Kind.OUTPUT, synthDelta, true);
		if(debug) System.out.println("adaptSimImpl.getObservation... done");
		return res;
	}
	
	public AdapterSimImpl (LTS l, IOLTSInterpretation i, long s, boolean sd) {
		lts = l;
		interp = i;
		seed = s;
		synthDelta = sd;
		
		if (seed != 0)
			rand = new Random(seed);
		else
			rand = new Random();
		Iterator<?extends State> it = null;
		try {
			it = lts.init();
		} catch (LTSException e) {
			System.err.println("AdapterSimImpl: caught LTSException: "+e.getMessage());
			it = null;
		}
		Vector<State> states = new Vector<State> ();
		if (it!=null)
			while(it.hasNext())
				states.add(it.next());
		if (states.size() > 0) {
			initState = new AdapterState(randomItem(states));
			curState = randomInternalWalk(initState);
			initState.copySubTransitions(curState);
			initState.copySubState(curState);
		} else
			System.err.println("AdapterSimImpl: cannot initialize (lts.init returns 0 states)");
	}
	
	private boolean outputsPending() {
		// TODO also look if there is output pending via tau transition
		Iterator<?extends Transition<?extends State>> tit = curState.cur().menu(interp.isOutput());
		if (tit==null || !tit.hasNext()) {
			// should look for tau transition that enables output
			if(debug) System.out.println("AdapterSimImpl.outputsPending: FALSE: tit==null or no next: "+tit);
			return false;
		}
			
		while(tit.hasNext()) {
			Transition<? extends State> t = tit.next();
			Label l = t.getLabel();
			String l_nm = l.getString().toLowerCase();
			if (! l_nm.startsWith("delta")) {
				if(debug)System.out.println("AdapterSimImpl.outputsPending: TRUE: label does not start with delta: "+l_nm);
				return true;
			} else {
				if(debug)System.out.println("AdapterSimImpl.outputsPending: ... label starts with delta: "+l_nm);
			}
		}
		if(debug)System.out.println("AdapterSimImpl.outputsPending: FALSE no non-delta label encountered");
		return false;
	}
	
	// NOTE: goLabel does not compute whether a state is quiescent
	// start in curState
	// if enabled in curState, walk a transition with given label
	private AdapterTransition<?extends State> goLabel(long startTime, Label l, String nm, LabelConstraint lc, Kind k) {
		AdapterState a_src = curState;
		
		// clear out traversed transition info of a_src by creating fresh AdaptState
		if (a_src.getSubTransitions().hasNext())
			a_src = new AdapterState(a_src.cur());

		AdapterTransition<? extends State> a_t =  doGoLabel(startTime, a_src, l, nm, lc, k, new HashSet<State>() );
		curState = a_t.getDestination();
		return a_t;
	}
	private AdapterTransition<?extends State> doGoLabel(long startTime, AdapterState a_src, Label l, String nm, LabelConstraint lc, Kind k, Set<State> traversed) {
		AdapterTransition<? extends State> a_t;
		
		if (traversed.contains(a_src.cur())) 
			return new AdapterTransition<AdapterState>(startTime, System.currentTimeMillis(), 0, a_src, new LibLabel("tauloop"), k, a_src);
		traversed.add(a_src.cur());

		Iterator<?extends Transition<?extends State>> tit = a_src.cur().menu(lc);
		Vector<State> dsts = new Vector<State>();
		HashMap<State,Vector<Transition<? extends State>>> transMap = new HashMap<State,Vector<Transition<? extends State>>>();
		if(debug)System.out.println("adapt-sim-impl: go-label "+l.getString());
		State dst = null;
		Transition<? extends State> res = null;
		while (tit.hasNext()) {
			Transition<? extends State> t = tit.next();
			Label t_l = t.getLabel();
			Term t_tm = TermParser.parse(t_l);
			Term l_tm = TermParser.parse(l);
			UnificationResult uni_res = t_tm.unify(l_tm);
			if (uni_res.isUnifyable()) {
				ArrayList<UnificationBinding> b = uni_res.getBindings();
				if (b==null || b.isEmpty()) {
					// no work to do: isUnifyable, no bindings involved
					// should we check whether there are free variables remaining?
					dbgMsgln("");
					res = t;
					dst = t.getDestination();
					if(debug)System.out.println("adapt-sim-impl: adding label "+res.getLabel().getString());
					dsts.add(dst);
					if (transMap.get(dst) == null)
						transMap.put(dst, new Vector<Transition<? extends State>>());
					transMap.get(dst).add(res);

				} else {
					// this is where we have instantiate the transition
					Transition<?extends State> t_inst = t.instantiate(new InstantiationImpl(l, l_tm, b));
					dbgMsgln("Model.next: inst-result="+t_inst);
					if (t_inst != null) { // i.e. instantiation was succesful
						res = t_inst;
						dst = t_inst.getDestination();
						if(debug)System.out.println("adapt-sim-impl: adding label "+res.getLabel().getString());
						dsts.add(dst);
						if (transMap.get(dst) == null)
							transMap.put(dst, new Vector<Transition<? extends State>>());
						transMap.get(dst).add(res);

					} else {
						dbgMsgln("");
						System.err.println("ModelImpl next: instantiation unsuccesful: "+t_l.getString()+" with "+l.getString());
					}
				}
			} else
				if(debug)System.out.println("adapt-sim-impl: skipping label "+t.getLabel().getString());
		}

		if(debug) System.out.println("adapt-sim-impl: s.size: "+dsts.size());
		dbgMsg(nm+": "+a_src.getID()+ " -- "+l.getString());
		AdapterState intermediate;
		if (dsts.size() > 0){
			intermediate = new AdapterState(randomItem(dsts));
		} else {
			intermediate = a_src;

			Iterator<?extends Transition<?extends State>> intIt = a_src.cur().menu(interp.isInternal());
			if (intIt.hasNext()) {
				dbgMsg("(not-enabled-ff: "+l.getString()+")");
				dbgMsg("(adapt-sim-impl: no "+nm+" but: "+a_src.getID()+ " -- "+"i");
				intermediate = randomInternalStep(a_src, intIt);
				dbgMsgln(" --> "+intermediate.getID());
				return doGoLabel(startTime, intermediate, l, nm, lc, k, traversed);
			} else {
				dbgMsg("(adapt-sim-impl: not-enabled-oops: "+l.getString()+")");
			}
		}
		dbgMsg(" --> "+intermediate.getID());
		AdapterState a_dst = randomInternalWalk(intermediate);
		a_t = new AdapterTransition<AdapterState>(startTime, System.currentTimeMillis(), 0, a_src, l, k, a_dst);
		if (dsts.size() > 0)
			a_t.add(randomItem(transMap.get(intermediate.cur())));
		dbgMsgln(" --> "+a_dst.getID());
		a_dst.copySubTransitions(a_src);
		return a_t;
	}
	
	// this can be used to obtain random output that produces delta
	// delta is only allowed if allowDelta (do not allow delta to overrule attempt to apply stimulus)
	// updates curState
	private AdapterTransition<?extends State> goRandom(long startTime, String nm, LabelConstraint lc, Kind k, Boolean synthDelta, boolean allowDelta) {
		AdapterState a_src = curState;
		
		// clear out traversed transition info of a_src by creating fresh AdaptState
		if (a_src.getSubTransitions().hasNext())
			a_src = new AdapterState(a_src.cur());

		AdapterTransition<?extends State> a_t = doGoRandom(startTime, a_src, nm, lc, k, synthDelta, allowDelta, new HashSet<State>());
		curState = a_t.getDestination();
		return a_t;
	}
	private AdapterTransition<?extends State> doGoRandom(long startTime, AdapterState a_src, String nm, LabelConstraint lc, Kind k, Boolean synthDelta, boolean allowDelta, Set<State> traversed) {
		if (traversed.contains(a_src.cur())) 
			return new AdapterTransition<AdapterState>(startTime, System.currentTimeMillis(), 0, a_src, new LibLabel("tauloop"), k, a_src);
		traversed.add(a_src.cur());
		
		if(debug) System.out.println("adapt-sim-impl: doGoRandom -- get internal menu....");
		Iterator<?extends Transition<?extends State>> intIt = a_src.cur().menu(interp.isInternal());
		if(debug) System.out.println("adapt-sim-impl: doGoRandom -- done get internal menu");
		if(debug) System.out.println("adapt-sim-impl: doGoRandom -- get visible menu....");
		Iterator<?extends Transition<?extends State>> tit = a_src.cur().menu(lc);
		if(debug) System.out.println("adapt-sim-impl: doGoRandom -- done get visible menu");
		if (! tit.hasNext()) {
			dbgMsg("adapt-sim-impl: no succ for tit in doGoRandom");
			if (intIt.hasNext()) {
				dbgMsg("adapt-sim-impl: no "+nm+" but: "+a_src.getID()+ " -- "+"i");
				AdapterState intermediate = randomInternalStep(a_src, intIt);
				dbgMsgln(" --> "+intermediate.getID());
				return doGoRandom(startTime, intermediate, nm, lc, k, synthDelta, allowDelta, traversed);
			} else if (synthDelta) {
				dbgMsg("adapt-sim-impl: "+nm+" got: "+a_src.getID()+ " -- "+"delta");
				dbgMsgln(" --> "+a_src.getID());
				return new AdapterTransition<AdapterState>(startTime, System.currentTimeMillis(), 0, a_src, new LibLabel("delta"), k, a_src);
			} else {
				dbgMsg("adapt-sim-impl: "+nm+" nosynthdelta");
			}
		}
		// pick 1 of n transitions -> dest automatically chosen, or
		//      1 of n labels
		//         -> pick 1 of set of transitions
		// Implemented below:
		//	first pick label from set of enabled labels,
		//  then pick transition from set of transitions for that label,
		//  the transition gives us the destination
		
		HashMap<Label,Vector<Transition<? extends State>>> tMap = new HashMap<Label,Vector<Transition<? extends State>>>();
		while (tit.hasNext()) {
			Transition<? extends State> t = tit.next();
			Label l = t.getLabel();
			if (l.getString().startsWith("delta") && ! allowDelta)
				continue;
			if (tMap.get(l) == null)
				tMap.put(l, new Vector<Transition<? extends State>>());
			tMap.get(l).add(t);
		}
		Label l = randomItem(new Vector<Label>(tMap.keySet()));
		dbgMsgln("adapt-sim-impl: "+nm+" got: "+a_src.getID()+ " -- "+l.getString());
		Transition<? extends State> t = randomItem(tMap.get(l));
		Iterator<Variable> var_it = l.getVariables();
		State dst = null;
		Transition<?extends State> res = null;
		if (var_it!=null && var_it.hasNext()) {
			while (var_it.hasNext()) {
				Variable v = var_it.next();
				dbgMsgln("adapt-sim-impl: "+nm+" need to instantiate: "+v.getName()+" of "+v.getType()+" ("+v.getOriginalName()+")");
			}
			Label l_inst = t.getSolution();
				if (l_inst!=null) {
					dbgMsgln("adapt-sim-impl: inst-result label="+l_inst.getString());
					Term l_tm = TermParser.parse(l);
					Term l_inst_tm = TermParser.parse(l_inst);
					UnificationResult uni_res = l_tm.unify(l_inst_tm);
					if (uni_res.isUnifyable()) {
						Transition<?extends State> t_inst = t.instantiate(new InstantiationImpl(l_inst, l_inst_tm, uni_res.getBindings()));
						dbgMsgln("adapt-sim-impl: inst-result="+t_inst);
						if (t_inst != null) { // i.e. instantiation was successful
							res = t_inst;
							dst = t_inst.getDestination();
						}
					} else 
						System.err.println("adapt-sim-impl: unable to obtain unifyable instantiation: "+l_inst.getString());
				} else
					System.err.println("adapt-sim-impl: instantiation=null");
		} else {
			res = t;
			dst  = t.getDestination();
		}
		// TODO do a better job at making this robust
		if (res==null && dst==null) {
			res = t;
			dst  = t.getDestination();
			return new AdapterTransition<AdapterState>(startTime, System.currentTimeMillis(), 0, null, k, "unable to instantiate");
		}
		dbgMsg("adapt-sim-impl: "+nm+" finally-got: "+a_src.getID()+ " -- "+res.getLabel().getString());
		AdapterState intermediate = new AdapterState(dst);
		dbgMsg(" --> "+intermediate.cur().getID());
		AdapterState a_dst = randomInternalWalk(intermediate);
		dbgMsgln(" --> "+a_dst.cur().getID());
		AdapterTransition<?extends State> a_t = new AdapterTransition<AdapterState>(startTime, System.currentTimeMillis(), 0, a_src, res.getLabel(), k, a_dst);
		a_t.add(res);

		if (debug) { // TODO is this still correct after merge between symbolic and master?
			Iterator<?extends Transition<?extends State>> sub_it = a_src.getSubTransitions();
			while(sub_it.hasNext()) {
				Transition<?extends State> sub_t = sub_it.next();
				dbgMsg("adapt-sim-impl: looking at-sub-trans ("+a_src.getID()+") ("+sub_t.getSource().getID()+" "+sub_t.getLabel().getString()+" "+sub_t.getDestination().getID()+")");
			}
		}
		return a_t;
	}


	private AdapterState randomInternalStep(AdapterState a_src, Iterator<?extends Transition<?extends State>> intIt) {
		Vector<AdapterState> succ = new Vector<AdapterState>();
		while(intIt.hasNext()) {
			Transition<?extends State> t = intIt.next();
			AdapterState a_dst = new AdapterState(t.getDestination());
			a_dst.copySubTransitions(a_src);
			a_dst.addTrans(t);
			succ.add(a_dst);
		}
		return randomItem(succ);
	}
	
	private AdapterState randomInternalWalk(AdapterState a_src) {
		Vector<AdapterState> succ = internalSucc(a_src);
		if (succ.size() > 0) {
			AdapterState a_dst = randomItem(succ);
			return a_dst;
		}
		return a_src;
	}
	
	private Vector<AdapterState> internalSucc(AdapterState s)  {
		State dst;
		Vector<AdapterState> work = new Vector<AdapterState>();
		HashSet<State> seen = new HashSet<State>();
		// clear out traversed transition info of s by creating fresh AdaptState
		work.add(new AdapterState(s.cur()));
		seen.add(s.cur()); 
		for (int i=0; i < work.size(); i++) {
			AdapterState a_src = work.elementAt(i);
			Iterator<?extends Transition<?extends State>> it = a_src.cur().menu(interp.isInternal());
			while(it.hasNext()) {
				Transition<?extends State> t = it.next();
				dst = t.getDestination();
				if (!seen.contains(dst)) {
					AdapterState a_dst = new AdapterState(dst);
					work.add(a_dst);
					a_dst.copySubTransitions(a_src);
					a_dst.addTrans(t);
					seen.add(dst);
				}
			}
		}
		// do not remove where we started from work,
		//because we may decide to stay there
		return work;
	}
	
	private <T> T randomItem(Vector<T> s) {
		int sz = s.size();
		dbgMsgln("(adapt-sim-impl: randomItem sz="+sz+")");
		if (sz ==1)
			return s.firstElement();
		if (sz > 1) {
			int idx = rand.nextInt(sz);
			dbgMsgln("\t(adapt-sim-impl: randomItem sz="+sz+" chosen idx="+idx+")");
			//double d = Math.random();
			//int idx = (int)(d*sz);
			//dbgMsgln("   randomItem sz="+sz+" d="+d+" idx="+idx);
			return s.elementAt(idx);
		}
		if (debug) System.err.println("(adapt-sim-impl: randomItem sz="+sz+")"+" returning null!");
		return null;
	}
	
	private void dbgMsg(String s) {
		if (debug)
			System.out.print(s);
	}
	private void dbgMsgln(String s) {
		if (debug)
			System.out.println(s);
	}
	
	private AdapterState curState;
	private AdapterState initState;
	private LTS lts;
	private IOLTSInterpretation interp;
	private boolean debug = false;
	private long seed;
	private Random rand;
	
	private boolean synthDelta;

}
