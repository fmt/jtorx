package utwente.fmt.jtorx.torx.adapter.stdinout;

import java.io.IOException;
import java.util.logging.Logger;

import utwente.fmt.jtorx.testnongui.casestudy.CaseStudyIO;
import utwente.fmt.jtorx.torx.AdapterInitializationExtendedResult;
import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.adapter.Observation;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.Label;

/**
 * A TorX stdio test adapter wrapper querying the SUT for performance data after
 * the test execution. The performance data is written to stdout.
 */
public class PerformanceMeasuringStdInOutAdapterWrapper implements SimAdapter {
	private final TimedStdInOutAdapter wrappedAdapter;
	private static final Logger LOGGER = Logger.getLogger(PerformanceMeasuringStdInOutAdapterWrapper.class.getCanonicalName());
	
	/** The string sent to the SUT for performance queries */
	private static final String CMD_PRINT_PERFORMANCE_STATS = "#print-performance-stats";
	
	/** The string expected from the SUT after having transmitted the performance data. */
	private static final String RESPONSE_STATS_FINISHED = "#finished";
		
	public PerformanceMeasuringStdInOutAdapterWrapper(TimedStdInOutAdapter wrappedAdapter) {
		this.wrappedAdapter = wrappedAdapter;
	}

	@Override
	public AdapterInitializationExtendedResult start() {
		return wrappedAdapter.start();
	}

	@Override
	public void done() {
		sendPerformanceStatsRequest();
		readAndDisplayPerformanceStats();
		wrappedAdapter.done();
	}
	
	private void sendPerformanceStatsRequest() {
		try {
			wrappedAdapter.sendRequest(CMD_PRINT_PERFORMANCE_STATS + "\n");
		}
		catch (IOException | IllegalStateException e) {
			LOGGER.severe("Unable to issue the printing of performance statistics by the SUT:\n" + e);
		}
	}
	
	private void readAndDisplayPerformanceStats() {
		String line = "";
		while (!line.trim().equals(RESPONSE_STATS_FINISHED)) {
			try {
				Observation<String> obs = wrappedAdapter.getObservationLineFromSUT();
				if (obs != null) {
					line = obs.getData();
					if (!line.trim().equals(RESPONSE_STATS_FINISHED)) {
						CaseStudyIO.out.print(line + "\n");
					}
				}
				else {
					LOGGER.severe("Unable to read performance statistics from the SUT, giving up");
					return;
				}
			}
			catch (InterruptedException e) {
				LOGGER.severe("Unable to read performance statistics from the SUT:\n" + e);
			}
		}
	}

	@Override
	public AdapterInteractionTransitionResult<? extends CompoundState> applyStimulus(
			Label l) {
		return wrappedAdapter.applyStimulus(l);
	}

	@Override
	public AdapterInteractionTransitionResult<? extends CompoundState> getObservation() {
		return wrappedAdapter.getObservation();
	}
}
