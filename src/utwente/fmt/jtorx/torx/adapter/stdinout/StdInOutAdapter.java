package utwente.fmt.jtorx.torx.adapter.stdinout;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.torx.AdapterInitializationExtendedResult;
import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.InterActionResult;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.adapter.AdapterTransition;
import utwente.fmt.jtorx.torx.adapter.AdapterInitializationExtendedResultImpl;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.utils.ChildStarter;
import utwente.fmt.lts.Label;

// TODO keep track of time of last interaction,
// such that we compute time-to-wait as
// timeout - (timecur - timelast)

public class StdInOutAdapter implements SimAdapter{
	
	class StreamReader extends Thread {
		private Reader is;
		final int ARRAYSIZE=1024;
		private String buf = "";
		private String name;
 
		StreamReader(String n, Reader is) {
			this.is = is;
			this.name = n;
		}
 
		public void run() {
			try {
				int n;
				char[] cbuf = new char[ARRAYSIZE];
				while (true /*err.ready()*/){
					n  = is.read(cbuf, 0, ARRAYSIZE);
					if (n <= 0)
						return;
					buf += new String(cbuf,0,n);
					int i = buf.indexOf("\n");
					while (i >= 0) {
						String s = buf.substring(0,i).replaceAll("\r", "").trim();
						obsQueue.put(s);
						report("streamreader: "+s);
						buf = buf.substring(i+1);
						i = buf.indexOf("\n");
					}
				}
			} catch (IOException e) {
				report("streamreader io exception: "+e.getMessage());
				e.printStackTrace();
				try {
					is.close();
				} catch (IOException e1) {
					System.err.println(name+" stdinout-adapter streamreader close io exception: "+e1.getMessage());
					e1.printStackTrace();
				}

			} catch (InterruptedException e) {
				report("streamreader io exception: "+e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	public class Reporter implements Appendable {

		public Appendable append(CharSequence csq) throws IOException {
			if (csq==null)
				csq = "null";
			doAppend(csq.toString());
			return this;
		}

		public Appendable append(char c) throws IOException {
			doAppend(""+c);
			return this;
		}

		public Appendable append(CharSequence csq, int start, int end) throws IOException {
			if (csq==null)
				csq = "null";
			if (start<0 || end<0 || start>end || end>csq.length())
				throw new IndexOutOfBoundsException();
			doAppend(csq.subSequence(start,end).toString());
			return this;
		}
		
		private void doAppend(String s) {
			report(s);
		}
		
	}


	public AdapterInteractionTransitionResult applyStimulus(Label l) {
		long startTime = System.currentTimeMillis();
		if(debug) report("applyStim: "+l.getString());
		String obs = obsQueue.poll();
		if (obs!=null) {
			Label l2 = new LibLabel(obs);
			report("wanted applyStim ("+l.getString()+") but pending: "+l2.getString());
			return new AdapterTransition(startTime, System.currentTimeMillis(), 0, l2, InterActionResult.Kind.OUTPUT, obs);
		} else {
			try {
				String cmd = l.getString()+"\n";
				sendRequest(cmd);
				if(debug) report("applied Stim: "+l.getString());
				return new AdapterTransition(startTime, System.currentTimeMillis(), 0, l, InterActionResult.Kind.INPUT, l.getString());
			} catch (IOException e) {
				report("applyStim io exception: "+e.getMessage());
				e.printStackTrace();
				errorReporter.report(adaptStimErr);
				running = false;
				return null;
			}
		}
	}
	
	public AdapterInteractionTransitionResult getObservation() {
		long startTime = System.currentTimeMillis();
		if(debug) report("getObs");
		AdapterTransition result = null;
		progress.setActivity("Observing...");
		progress.startUndeterminate();
		try {
			String obs = obsQueue.poll(timeVal, timeUnit);
			
			Label l;
			String c = null;
			if (obs != null) {
				progress.setActivity("Observation");
				l = new LibLabel(obs);
				c = obs;
			} else {
				progress.setActivity("Timed out");
				l = new LibLabel("delta");
			}
			result = new AdapterTransition(startTime, System.currentTimeMillis(), 0, l, InterActionResult.Kind.OUTPUT, c);
			if(debug) report("gotObs: "+l.getString());
		} catch (InterruptedException e) {
			progress.setActivity("ObsError");
			report("getObs interrupted: "+e.getMessage());
			e.printStackTrace();
			errorReporter.report(adaptStimErr);
			running = false;
			return null;
		}
		progress.stopUndeterminate();
		return result;
	}

	public void done() {
		report("in done: running="+(running?"f":"f")+" cleanedUp="+(cleanedUp?"t":"f"));

		if (! running)
			return;

		if (cleanedUp)
			return;
		cleanedUp = true;
		
		if (in == null)
			return;
		
		dbgMsgln("stdinout-adapter-impl: in done: closing subprocess stdin");
		try {
			in.close();
		} catch (IOException e) {
			System.err.println("Adapter stdinout-adapt-impl: done: io exception: "+e.getMessage());
			e.printStackTrace();
			errorReporter.report("done: io exception: "+e.getMessage());
			running = false;
		}
		dbgMsgln("stdinout-adapter-impl: in done: closed subprocess stdin");
		
		// TODO make sleep amount configurable
		cs.destroy(500);
	}

	public AdapterInitializationExtendedResult start() {
		long startTime = System.currentTimeMillis();
		report("start running="+(running?"t":"f")+" in="+in+" out="+out);
		if (running && in != null && out !=  null) {
			return new AdapterInitializationExtendedResultImpl(startTime, startTime, true);
		} else
			return new AdapterInitializationExtendedResultImpl(startTime, startTime, false);

	}

	
	public StdInOutAdapter(ErrorReporter r, String command, String[] settings, String dirName, String[] envVars) /*throws IOException*/ {
		this(r, command, dirName, envVars, new NullProgressReporter());
	}
	
	public StdInOutAdapter(ErrorReporter r, String command, String dirName, String[] envVars, ProgressReporter p) {
		this(r, command, dirName, envVars, p, 10L, TimeUnit.SECONDS);
	}

	public StdInOutAdapter(ErrorReporter r, String command, String dirName, String[] envVars, ProgressReporter p, long tov, TimeUnit tou) /*throws IOException*/ {
		
		//String[] commands = {"python", "3DVis.py", "-passive"};
		// String[] settings = {"path=%path%;C:\\python25"};
		
		System.err.println("adapter-impl  "+this.toString()+" constructor begin");
		
		progress = p;
		errorReporter = r;
		running = false;
		name = "StdInOut-Adapter";
		
		timeVal = tov;
		timeUnit = tou;
		
		cs = new ChildStarter(name, r, command, dirName, p, new Reporter(), envVars);
		cs.run();
		running = cs.isRunning();
		in = cs.getIn();
		out = cs.getOut();
		if (running && in!=null && out != null) {
				StreamReader reader = new StreamReader(name, out);
				reader.start();
		}

		running = cs.isRunning();
		
	}
	
	public void dbgMsgln(String s) {
		if (debug)
			System.out.println(name+" dbg: "+s);
	}

	
	private void sendRequest(String s) throws IOException {
		in.write(s.getBytes());
		in.flush();
	}
	
	private void report(String s) {
		long now = System.currentTimeMillis();
		if(debug) System.out.println(format.format(now)+" "+s);
		errorReporter.log(now, name, s);
	}
	
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	private boolean running = false;
	private ErrorReporter errorReporter;
	private String name;

	private OutputStream in;
	private Reader out;

	private ProgressReporter progress = null;
	
	private Boolean debug = false;
	private Boolean cleanedUp = false;
	

	private ChildStarter cs;
	
	final String adaptStimErr = "Adapter stdinout-adapt-impl: could not apply stimulus";
	final String adaptObsErr = "Adapter stdinout-adapt-impl: could not obtain observation";


	private BlockingQueue<String> obsQueue = new LinkedBlockingQueue<String>();
	private long timeVal = 10L;
	private TimeUnit timeUnit = TimeUnit.SECONDS;

}
