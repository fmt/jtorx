package utwente.fmt.jtorx.torx.adapter.stdinout;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.torx.AdapterInitializationExtendedResult;
import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.InterActionResult;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.adapter.AdapterInitializationExtendedResultImpl;
import utwente.fmt.jtorx.torx.adapter.AdapterTransition;
import utwente.fmt.jtorx.torx.adapter.Observation;
import utwente.fmt.jtorx.torx.adapter.StreamReader;
import utwente.fmt.jtorx.torx.adapter.TimeSource;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.utils.ChildStarter;
import utwente.fmt.jtorx.utils.term.ConstantTerm;
import utwente.fmt.jtorx.utils.term.OperatorTerm;
import utwente.fmt.jtorx.utils.term.TermParser;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Term;

// TODO keep track of time of last interaction,
// such that we compute time-to-wait as
// timeout - (timecur - timelast)

public class TimedStdInOutAdapter implements SimAdapter{
	
	public class Reporter implements Appendable {

		public Appendable append(CharSequence csq) throws IOException {
			if (csq==null)
				csq = "null";
			doAppend(csq.toString());
			return this;
		}

		public Appendable append(char c) throws IOException {
			doAppend(""+c);
			return this;
		}

		public Appendable append(CharSequence csq, int start, int end) throws IOException {
			if (csq==null)
				csq = "null";
			if (start<0 || end<0 || start>end || end>csq.length())
				throw new IndexOutOfBoundsException();
			doAppend(csq.subSequence(start,end).toString());
			return this;
		}
		
		private void doAppend(String s) {
			report(s);
		}
		
	}


	public AdapterInteractionTransitionResult<? extends CompoundState> applyStimulus(Label l) {
		long startTime = System.currentTimeMillis();
		if(debug) report("applyStim: "+l.getString());
		long timeToWait = 0;
		long timeValue = 0;
		Label new_l = l;
		ArrayList<Term> new_subs = null;
		Term l_tm = null;
		if (realTime) {
			l_tm = TermParser.parse(l);
			ArrayList<Term> l_tm_subs = l_tm.subTerms();
			if (l_tm_subs==null || l_tm_subs.size() <= 0) {
				report("no time information present in label: "+l);
				return new AdapterTransition(startTime, System.currentTimeMillis(), 0, null, null, l.getString());
			}
			Term l_tm_timeValue = l_tm_subs.get(l_tm_subs.size()-1);
			String timeString = l_tm_timeValue.unparse();
			Double timeDouble;
			try {
				timeDouble = java.lang.Double.parseDouble(timeString);
			} catch (NumberFormatException e) {
				report("could not parse timestamp ("+timeString+") : "+e.getMessage());
				return new AdapterTransition(startTime, System.currentTimeMillis(), 0, null, null, l.getString());
			}
			// TODO be more specific about the conversion?
			//  - eg check whether we have the special case of sometime.0 ?
			timeValue = timeDouble.longValue();
			long timeToApply = adapterStartTime + timeValue;
			new_subs = new ArrayList<Term>();
			for(int i=0; i < l_tm_subs.size() -1; i++)
				new_subs.add(l_tm_subs.get(i));
			Term new_term = new OperatorTerm(l_tm.getOperator(), new_subs);
			String new_l_s = new_term.unparse();
			new_l = new LibLabel(new_l_s);
			timeToWait = timeToApply-TimeSource.getCurrentMonotonicTime();
		}
		System.out.println("TimedStdInOutAdapter.applyStimulus: "+l.getString()+" at "+timeValue+
				" since adapter start (cur="+timeToWait+")");
		if (timeToWait < 0) {
			report("Warning: too late ("+timeToWait+") for applying stimulus: "+new_l.getString());
			timeToWait = 0;
		}
		Observation<String> obs = null;
		// TODO should we add a yield (or so) here, to give the observation thread
		// some time to do its thing in case timeToWait==0 ???
		int i = 0;
		int max = 100;
		if(debug) System.out.println("before yield+poll reader.getObsCount()="+reader.getObsCount()+" obsProcessed="+obsProcessed+" i="+i+" obs="+obs);
		while (obs==null && reader.getObsCount() > obsProcessed && i < max) {
			Thread.yield();
			try {
				obs = obsQueue.poll(timeToWait, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e1) {
				report("interrupted while waiting for applying stimulus: "+e1.getMessage());
			}
			System.out.println("after yield+poll i="+i+" obs="+obs);
			i++;
		}
		if (obs!=null) {
			// TODO report observation time in obsQueue
			// 
			obsProcessed ++;
			String c = obs.getData().replaceAll("\r", "").trim();
			long obsTime = obs.getTimeStamp()-adapterStartTime;
			String new_obs = annoToAddPre + c;
			if (realTime)
				new_obs += " ! "+ obsTime;
			new_obs += annoToAddPost;
			Label l2 = new LibLabel(new_obs);
			if(debug) report("wanted applyStim ("+l.getString()+") but pending: "+l2.getString());
			if(debug) report("obsDelay: "+(TimeSource.getCurrentMonotonicTime() - obs.getTimeStamp()));
			return new AdapterTransition(startTime, System.currentTimeMillis(), obsTime, l2, InterActionResult.Kind.OUTPUT, c);
		} else {
			try {
				String s = new_l.getString();
				if (addRmPrefix && (s.charAt(0) == '!' || s.charAt(0) == '?')) {
					s = s.substring(1);
				} else if (addRmSuffix && (s.endsWith("!") || s.endsWith("?"))) {
					s = s.substring(0, s.length()-1);
				}

				String cmd = s+"\n";
				long ts_now_pre = TimeSource.getCurrentMonotonicTime();
				long now_pre = System.currentTimeMillis();
				long stimTime = ts_now_pre-adapterStartTime;
				sendRequest(cmd);
				long ts_now_post = TimeSource.getCurrentMonotonicTime();
				long now_post = System.currentTimeMillis();
				Label l_app;
				if (realTime) {
					new_subs.add(new ConstantTerm(""+stimTime));
					Term tm_applied_stim = new OperatorTerm(l_tm.getOperator(), new_subs);
					l_app = new LibLabel(tm_applied_stim.unparse());
				} else {
					l_app = new_l;
				}
				report(now_pre, "applied Stim: "+l_app.getString());
				if (ts_now_pre!=ts_now_post)
					report(now_post, "stimDelay: "+(ts_now_post-ts_now_pre));
				return new AdapterTransition(startTime, System.currentTimeMillis(), stimTime, l_app, InterActionResult.Kind.INPUT, s);
			} catch (IOException e) {
				report("applyStim io exception: "+e.getMessage());
				e.printStackTrace();
				errorReporter.report(adaptStimErr);
				running = false;
				return null;
			}
		}
	}
	
	public AdapterInteractionTransitionResult<? extends CompoundState> getObservation() {
		long startTime = System.currentTimeMillis();
		if(debug) report("getObs");
		AdapterInteractionTransitionResult result = null;
		progress.setActivity("Observing...");
		progress.startUndeterminate();
		try {
			Observation<String> obs = obsQueue.poll(timeVal, timeUnit);
			
			Label l;
			String c = null;
			long obsTime;
			if (obs != null) {
				progress.setActivity("Observation");
				obsProcessed ++;
				obsTime = obs.getTimeStamp()-adapterStartTime;
				c = obs.getData().replaceAll("\r", "").trim();
				String new_obs = annoToAddPre + c;
				if (realTime)
					new_obs += " ! "+obsTime;
				new_obs += annoToAddPost;
				l = new LibLabel(new_obs);
				if(debug) report("obsDelay: "+(TimeSource.getCurrentMonotonicTime() - obs.getTimeStamp()));
			} else {
				obsTime = TimeSource.getCurrentMonotonicTime()-adapterStartTime;
				progress.setActivity("Timed out");
				String new_obs;
				if (realTime) {
					new_obs = "Delta"+" ! "+obsTime;
				} else {
					new_obs = "delta";
				}
				l = new LibLabel(new_obs);
			}
			result = new AdapterTransition(startTime, System.currentTimeMillis(), obsTime, l, InterActionResult.Kind.OUTPUT, c);
			if(debug) report("gotObs: "+l.getString());
		} catch (InterruptedException e) {
			progress.setActivity("ObsError");
			report("getObs interrupted: "+e.getMessage());
			e.printStackTrace();
			errorReporter.report(adaptObsErr);
			running = false;
			return null;
		}
		progress.stopUndeterminate();
		return result;
	}
	
	protected Observation<String> getObservationLineFromSUT() throws InterruptedException {
		return obsQueue.poll(timeVal, timeUnit);
	}

	public void done() {
		report("in done: running="+(running?"f":"f")+" cleanedUp="+(cleanedUp?"t":"f"));

		if (! running)
			return;

		if (cleanedUp)
			return;
		cleanedUp = true;
		
		if (in == null)
			return;
		
		dbgMsgln("stdinout-adapter-impl: in done: closing subprocess stdin");
		try {
			in.close();
		} catch (IOException e) {
			System.err.println("Adapter stdinout-adapt-impl: done: io exception: "+e.getMessage());
			e.printStackTrace();
			// this might kill main program...
			// errorReporter.report("done: io exception: "+e.getMessage());
			running = false;
		}
		dbgMsgln("stdinout-adapter-impl: in done: closed subprocess stdin");
		
		// TODO make sleep amount configurable
		cs.destroy(500);
	}

	public AdapterInitializationExtendedResult start() {
		long startTime = System.currentTimeMillis();
		report("start running="+(running?"t":"f")+" in="+in+" out="+out);
		if (running && in != null && out !=  null) {
			return new AdapterInitializationExtendedResultImpl(startTime, startTime, true);
		} else
			return new AdapterInitializationExtendedResultImpl(startTime, startTime, false);

	}

	
	public TimedStdInOutAdapter(ErrorReporter r, String command, String[] settings, String dirName, String[] envVars, boolean realTime, boolean addRmPre, boolean addRmPost) /*throws IOException*/ {
		this(r, command, dirName, envVars, realTime, addRmPre, addRmPost, new NullProgressReporter());
	}
	
	public TimedStdInOutAdapter(ErrorReporter r, String command, String dirName, String[] envVars, boolean realTime, boolean addRmPre, boolean addRmPost, ProgressReporter p) {
		this(r, command, dirName, envVars, realTime, addRmPre, addRmPost, p, 10L, TimeUnit.SECONDS);
	}

	public TimedStdInOutAdapter(ErrorReporter r, String command, String dirName, String[] envVars, boolean realTime, boolean addRmPre, boolean addRmPost, ProgressReporter p, long tov, TimeUnit tou) /*throws IOException*/ {
		
		//String[] commands = {"python", "3DVis.py", "-passive"};
		// String[] settings = {"path=%path%;C:\\python25"};
		
		System.err.println("adapter-impl  "+this.toString()+" constructor begin");
		
		progress = p;
		errorReporter = r;
		running = false;
		name = "StdInOut-Adapter";
		
		timeVal = tov;
		timeUnit = tou;
		
		if (addRmPre && !addRmPost) {
			annoToAddPre = "!";
			addRmPrefix =  addRmPre;
		}
		if (addRmPost && !addRmPre) {
			annoToAddPost = "!";
			addRmSuffix =  addRmPost;
		}

		
		cs = new ChildStarter(name, r, command, dirName, p, new Reporter(), envVars);
		cs.run();
		running = cs.isRunning();
		in = cs.getIn();
		out = cs.getOut();
		if (running && in!=null && out != null) {
				reader = new StreamReader(name, out, obsQueue, errorReporter);
				reader.start();
		}

		running = cs.isRunning();
		adapterStartTime = TimeSource.getCurrentMonotonicTime();
		this.realTime = realTime;
		
	}
	
	public void dbgMsgln(String s) {
		if (debug)
			System.out.println(name+" dbg: "+s);
	}

	
	protected void sendRequest(String s) throws IOException {
		if (in != null) {
			in.write(s.getBytes());
			in.flush();
		}
		else {
			throw new IllegalStateException("Adapter i/o channel not set up. Adapter program not running?");
		}
	}
	
	private void report(String s) {
		long now = System.currentTimeMillis();
		if(debug) System.out.println(format.format(now)+" "+s);
		errorReporter.log(now, name, s);
	}
	private void report(long t, String s) {
		long now = System.currentTimeMillis();
		String n = format.format(now);
		if(debug) System.out.println(n+" "+s);
		errorReporter.log(t, name, s+" ("+n+")");
	}
	
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	private boolean running = false;
	private ErrorReporter errorReporter;
	private String name;

	private OutputStream in;
	private Reader out;
	private StreamReader reader = null;

	private ProgressReporter progress = null;
	
	private Boolean debug = false;
	private Boolean cleanedUp = false;
	

	private ChildStarter cs;
	
	final String adaptStimErr = "Adapter stdinout-adapt-impl: could not apply stimulus";
	final String adaptObsErr = "Adapter stdinout-adapt-impl: could not obtain observation";
	final String adaptRaceErr = "Adapter stdinout-adapt-impl: interaction race detected";


	private BlockingQueue<Observation<String>> obsQueue = new LinkedBlockingQueue<Observation<String>>();
	private long timeVal = 10L; // timeout value???
	private long adapterStartTime = 0L;
	private TimeUnit timeUnit = TimeUnit.SECONDS;
	private boolean realTime = true;
	private boolean addRmPrefix = false;
	private boolean addRmSuffix = false;
	private String annoToAddPre = "";
	private String annoToAddPost = "";
	private long obsProcessed = 0;

}
