package utwente.fmt.jtorx.torx.adapter.tcplabels;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import utwente.fmt.jtorx.torx.AdapterInitializationExtendedResult;
import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.InterActionResult;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.adapter.AdapterInitializationExtendedResultImpl;
import utwente.fmt.jtorx.torx.adapter.AdapterTransition;
import utwente.fmt.jtorx.torx.adapter.Observation;
import utwente.fmt.jtorx.torx.adapter.StreamReader;
import utwente.fmt.jtorx.torx.adapter.TimeSource;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.lts.Label;

public class TcpServerLabelAdapter implements SimAdapter {

	
	public AdapterInitializationExtendedResult start() {
		long startTime = System.currentTimeMillis();
		System.err.println("tcplabel-adapt-impl "+this.toString()+" start running="+(running?"t":"f")+" in="+in+" out="+out);
		adapterStartTime = TimeSource.getCurrentMonotonicTime();

		System.err.println("waiting until ListenThread reports connection");
		progress.setActivity("Waiting for connect...");
		progress.startUndeterminate();
		try {
			// we need to give a timout value here, for two reasons:
			// - safety guard (currently one day... better too high than too low)
			// - to allow catching InterruptedException
			//   (without the timeout value in poll, the try/catch gives compilation error
			Boolean r = connQueue.poll(60*60*24, TimeUnit.SECONDS);
			if (r!=null) {
				progress.setActivity("Connection");
				running = r;
				System.err.println("ListenThread reported connection: "+running);
			} else {
				progress.setActivity("Timed out");
				running = false;
				System.err.println("ListenThread timed out: "+running);
			}

		} catch (InterruptedException e) {
			progress.setActivity("Interrupted");
			System.err.println("ListenThread did not report connection in time");
			running = false;
		}
		progress.stopUndeterminate();
		if (running && in != null && out !=  null) {
			return new AdapterInitializationExtendedResultImpl(startTime, startTime, true);
		} else
			return new AdapterInitializationExtendedResultImpl(startTime, startTime, false);

	}
	
	public void done() {
		System.err.println("tcplabel-adapt-impl "+this.toString()+"done()");
		
		if (listenSocket!=null) {
			try {
				listenSocket.close();
				listenThread.join();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		
		if (! running)
			return;
		
		if (in == null)
			return;
		

		running =  false;
		try {
			//out.close();
			in.close();
			//sock.close();
		} catch (IOException e) {
			System.err.println("Adapter tcplabel-adapt-impl: done: io exception: "+e.getMessage());
			// e.printStackTrace();
			// errorReporter.report("Adapter tcplabel-adapt-impl: done: io exception: "+e.getMessage());
		}
	}


	public AdapterInteractionTransitionResult applyStimulus(Label l) {
		long startTime = System.currentTimeMillis();
		System.err.println("Adapter tcplabel-adapt-impl applyStim: "+l.getString());
		Observation<String> obs = obsQueue.poll();
		if (obs!=null) {
			String c = obs.getData().replaceAll("\r", "").trim();
			String new_obs = annoToAddPre + c;
			new_obs += annoToAddPost;
			long obsTime = obs.getTimeStamp()-adapterStartTime;
			Label l2 = new LibLabel(new_obs);
			return new AdapterTransition(startTime, System.currentTimeMillis(), obsTime, l2, InterActionResult.Kind.OUTPUT, c);
		} else {
			try {
				String s = l.getString();
				if (addRmPrefix && (s.charAt(0) == '!' || s.charAt(0) == '?')) {
					s = s.substring(1);
				} else if (addRmSuffix && (s.endsWith("!") || s.endsWith("?"))) {
					s = s.substring(0, s.length()-1);
				}

				String cmd = s+"\n";
				long stimTime = TimeSource.getCurrentMonotonicTime()-adapterStartTime;
				sendRequest(cmd);
				return new AdapterTransition(startTime, System.currentTimeMillis(), stimTime, l, InterActionResult.Kind.INPUT, s);
			} catch (IOException e) {
				System.err.println("Adapter tcplabel-adapt-impl applyStim io exception: "+e.getMessage());
				e.printStackTrace();
				errorReporter.report(adaptStimErr);
				running = false;
				return null;
			}
		}
	}

	public AdapterInteractionTransitionResult getObservation() {
		long startTime = System.currentTimeMillis();
		System.err.println("Adapter tcplabel-adapt-impl getObs");
		AdapterInteractionTransitionResult result = null;
		progress.setActivity("Observing...");
		progress.startUndeterminate();
		try {
			Observation<String> obs = obsQueue.poll(timeVal, timeUnit);
			Label l;
			long obsTime;
			String c = "";
			if (obs != null) {
				progress.setActivity("Observation");
				c = obs.getData().replaceAll("\r", "").trim();
				String new_obs = annoToAddPre + c;
				new_obs += annoToAddPost;
				obsTime = obs.getTimeStamp()-adapterStartTime;
				l = new LibLabel(new_obs);
			} else {
				progress.setActivity("Timed out");
				obsTime = TimeSource.getCurrentMonotonicTime()-adapterStartTime;
				l = new LibLabel("delta");
			}
			result = new AdapterTransition(startTime, System.currentTimeMillis(), obsTime, l, InterActionResult.Kind.OUTPUT, c);
		} catch (InterruptedException e) {
			progress.setActivity("ObsError");
			System.err.println("Adapter tcplabel-adapt-impl getObs interrupted: "+e.getMessage());
			e.printStackTrace();
			errorReporter.report(adaptStimErr);
			running = false;
			return null;
		}
		progress.stopUndeterminate();
		return result;
	}

	public class ListenThread extends Thread {
		public ListenThread(ServerSocket listenSocket) {
			super("TcpServerLabelAdapter-Listenthread");
		}
		public void run() {
			try {
				Socket sock = listenSocket.accept();
				listenSocket.close();
				listenSocket = null;
				sock.setSoLinger(false, 0);
				sock.setSoTimeout(0);

				System.err.println(name+" "+"ListenThread got connection, closed listen sock, setting up StreamReader");
				
				out = new BufferedReader(new InputStreamReader(sock.getInputStream()));
				StreamReader reader = new StreamReader(name, out, obsQueue, errorReporter);
				reader.start();
				in = new DataOutputStream(sock.getOutputStream());
				running = true;
				connQueue.put(running);
				System.err.println(name+" "+"ListenThread got connection");
			} catch (IOException e) {
				System.err.println(name+" "+" ListenThread io exception "+e.getMessage());
				if (!e.getMessage().equalsIgnoreCase("Socket closed"))
					e.printStackTrace();
			} catch (InterruptedException e) {
				System.err.println(name+" "+" ListenThread interrupted exception "+e.getMessage());
				e.printStackTrace();
			}

		}
	}
	

	
	public TcpServerLabelAdapter(ErrorReporter r, String h, int p, boolean addRmPre, boolean addRmPost, ProgressReporter pr, long tov, TimeUnit tou) {
		host = h;
		port = p;
		
		progress = pr;
		errorReporter = r;
		running = false;
		name = "tcpserverlabel-Adapter";
		
		timeVal = tov;
		timeUnit = tou;
		
		if (addRmPre && !addRmPost) {
			annoToAddPre = "!";
			addRmPrefix =  addRmPre;
		}
		if (addRmPost && !addRmPre) {
			annoToAddPost = "!";
			addRmSuffix =  addRmPost;
		}

		try {
			listenSocket = new ServerSocket(); // port, 0, java.net.InetAddress.getByName(null));
			listenSocket.setReuseAddress(true);
			listenSocket.setSoTimeout(0);
			listenSocket.bind(new InetSocketAddress(java.net.InetAddress.getByName(null), port), 1);
			listenThread = new ListenThread(listenSocket);
			listenThread.start();
		} catch (IllegalArgumentException e) {
			r.report(name+" "+"could not connect: "+e.getMessage());
			// e.printStackTrace();
		} catch (UnknownHostException e) {
			r.report(name+" "+"unknown host \""+host+"\": "+e.getMessage());
			// e.printStackTrace();
		} catch (IOException e) {
			r.report(name+" "+"error connecting socket: "+e.getMessage());
			// e.printStackTrace();
		}
	}
	
	private void sendRequest(String s) throws IOException {
		in.write(s.getBytes());
		in.flush();
	}

	private void report(String s) {
		long now = System.currentTimeMillis();
		System.out.println(format.format(now)+" "+s);
		errorReporter.log(now, name, s);
	}


	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	private boolean running = false;
	private ErrorReporter errorReporter;
	private String name;

	private Thread listenThread = null;
	private ServerSocket listenSocket;
	private DataOutputStream in;
	private BufferedReader out;

	private ProgressReporter progress = null;

	final String adaptStimErr = "Adapter tcplabel-adapt-impl: could not apply stimulus";
	final String adaptObsErr = "Adapter tcplabel-adapt-impl: could not obtain observation";

	private String host;
	private int port;
	private long adapterStartTime = 0L;
	
	private BlockingQueue<Observation<String>> obsQueue = new LinkedBlockingQueue<Observation<String>>();
	private BlockingQueue<Boolean> connQueue = new LinkedBlockingQueue<Boolean>();
	private long timeVal = 10L;
	private TimeUnit timeUnit = TimeUnit.SECONDS;

	private boolean addRmPrefix = false;
	private boolean addRmSuffix = false;
	private String annoToAddPre = "";
	private String annoToAddPost = "";


}
