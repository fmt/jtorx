package utwente.fmt.jtorx.torx.adapter.torx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;

import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.torx.AdapterInitializationExtendedResult;
import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.InterActionResult;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.jtorx.torx.adapter.AdapterInitializationExtendedResultImpl;
import utwente.fmt.jtorx.torx.adapter.AdapterTransition;
import utwente.fmt.jtorx.torx.adapter.TimeSource;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.utils.ChildStarter;
import utwente.fmt.lts.Label;



public class AdapterImpl implements SimAdapter {
	
	public AdapterInteractionTransitionResult applyStimulus(Label l) {

		// C_INPUT event=CFSAP_IN !CF1 !JOIN (UT_A, CI_ONE)
		// A_INPUT event=CFSAP_IN !CF1 !JOIN (UT_A, CI_ONE)        channel=        pco=cf1 suspension=     concrete=SENDHEX 01006161706161706161700065656e65656e65656e\n   timestamp=1226011934 <Thu Nov 06 23:52:14 CET 2008>
		long startTime = System.currentTimeMillis();
		String pfx = "C_INPUT";
		Label result = null;
		String c = null;
		Kind ioKind = null;
		try {
			String cmd = pfx+" event="+l.getString()+"\n";
			sendRequest(cmd);
			long stimTime = TimeSource.getCurrentMonotonicTime()-adapterStartTime;
			String s = readUntil(pfx, inputAnswers);
			if (s==null)
				return null;
			System.err.println("reply:"+s);
			if (s.startsWith("A_I ")) {
				ioKind = InterActionResult.Kind.INPUT;
				s = s.substring("A_I ".length());
			} else if (s.startsWith("A_O ")) {
				ioKind = InterActionResult.Kind.OUTPUT;
				s = s.substring("A_O ".length());
			} else {
				System.err.println("response string has no exected prefix: "+s);
				return null;
			}
			String[] fields = s.split("[\t]");
			System.err.println("fieldcound:"+fields.length);
			for (int i=0; i < fields.length; i++) {
				System.err.println("field["+i+"]:"+fields[i]);
				if (fields[i].startsWith("event=")) {
					String label = fields[i].substring(6);
					result = new LibLabel(label);
				}
				if (fields[i].startsWith("suspension=1")) {
					String label = "delta";
					result =  new LibLabel(label);
				}
				if (fields[i].startsWith("concrete=")) {
					c = fields[i].replace("concrete=","");
				}

			}

			consumePendingLines(pfx);

			return new AdapterTransition(startTime, System.currentTimeMillis(), stimTime, result, ioKind, c);
		} catch (IOException e) {
			System.err.println("Adapter torx-adapt-impl: "+pfx+" io exception: "+e.getMessage());
			e.printStackTrace();
			errorReporter.report(adaptStimErr);
			running = false;
			return null;
		}
	}
	
	public AdapterInteractionTransitionResult getObservation() {
		// TODO Auto-generated method stub
		// C_OUTPUT
		// nogci_iut_get_output-result: output udp3 {udp_out {udp3 {udp_dataind( udp1, pdu_j( UT_A, CI_ONE ) )}}}
		// A_DEBUG Output output udp3 {udp_out {udp3 {udp_dataind( udp1, pdu_j( UT_A, CI_ONE ) )}}}
		// A_OUTPUT event=udp_out ! udp3 ! udp_dataind( udp1, pdu_j( UT_A, CI_ONE ) )      channel=        pco=udp3        suspension=0
		long startTime = System.currentTimeMillis();
		Label result = null;
		progress.setActivity("Observing...");
		progress.startUndeterminate();
		String pfx = "C_OUTPUT";
		String c = null;
		Kind ioKind = null;
		long obsTime = 0L;
		try {
			String cmd = pfx+"\n";
			sendRequest(cmd);
			String s = readUntil(pfx, outputAnswers);
			obsTime = TimeSource.getCurrentMonotonicTime()-adapterStartTime; // TODO: should take time from TorXAdapter
			if (s==null) {
				progress.setActivity("ObsError");
				progress.stopUndeterminate();
				return null;
			}
			System.err.println("reply:"+s);
			if (s.startsWith("A_I ")) {
				ioKind = InterActionResult.Kind.INPUT;
				s = s.substring("A_I ".length());
			} else if (s.startsWith("A_O ")) {
				ioKind = InterActionResult.Kind.OUTPUT;
				s = s.substring("A_O ".length());
			} else {
				System.err.println("response string has no exected prefix: "+s);
				return null;
			}

			String[] fields = s.split("[\t]");
			System.err.println("fieldcound:"+fields.length);
			for (int i=0; i < fields.length; i++) {
				System.err.println("field["+i+"]:"+fields[i]);
				if (fields[i].startsWith("event=")) {
					String label = fields[i].substring(6);
					result = new LibLabel(label);
				}
				if (fields[i].startsWith("suspension=1")) {
					String label = "delta";
					result =  new LibLabel(label);
				}
				if (fields[i].startsWith("concrete=")) {
					c = fields[i].replace("concrete=","");
				}

			}
			consumePendingLines(pfx);
		} catch (IOException e) {
			System.err.println("Adapter torx-adapt-impl: "+pfx+" io exception: "+e.getMessage());
			e.printStackTrace();
			errorReporter.report(adaptObsErr);
			running = false;
		}
		if (result!=null && result.getString().equals("delta"))
			progress.setActivity("Timed out");
		else if (result!=null && !result.getString().equals("delta"))
			progress.setActivity("Observation");
		progress.stopUndeterminate();
		return new AdapterTransition(startTime, System.currentTimeMillis(), obsTime, result, ioKind, c);
	}

	private String readUntil(String cmd, String ans[]) throws IOException {
		String line;
		boolean readAnswer = false;
		while(! readAnswer) {
			line = out.readLine();
			if (line == null) {
				running = false;
				return line;
			}
			System.err.println("");
			System.err.println("readUntil read:"+line);

			for (String s: ans) {
				if (line.startsWith(s+" ") || line.startsWith(s+"\t") || line.startsWith(s+"\n")) {
					// actually, we should extract channel,  pco, concrete, timestamp
					// return a line that starts with   'A_I '  or  'A_O '
					String tmp = line.replaceFirst(s+"[ \t]+", s.substring(0, 3)+" ").trim();
					return tmp;
				}
			}
			if (line.startsWith("A_ERROR")) {
				System.err.println("fatal: adapter error:"+line);
				return null;
			} else if (line.startsWith("A_DEBUG")) {
				subprocessDbgMsgln(line);
			} else if (line.startsWith("A_LOG")) {
				long now = System.currentTimeMillis();
				errorReporter.log(now, name, line.replaceFirst("A_LOG[ \t]*",""));
				System.err.println("readUntil logged:"+line);
			} else if (line.startsWith("A_INPUT_ERROR")) {
				System.err.println("non-fatal: adapter error:"+line);
				return null;
			} else if (line.startsWith("A_OUTPUT_ERROR")) {
				System.err.println("non-fatal: adapter error:"+line);
				return null;
			} else {
				System.err.println("unexpected "+cmd+" response:"+line);
			}
		}
		return null;
	}
	
	private void consumePendingLines(String cmd) throws IOException {
		String line;
		while(out.ready()) {
			line = out.readLine();
			System.err.println("");
			System.err.println("applyStimulus read:"+line);
			if (line.startsWith("A_ERROR")) {
				System.err.println("fatal: adapter error:"+line);
				// should not happen
			} else if (line.startsWith("A_DEBUG")) {
				subprocessDbgMsgln(line);
			} else if (line.startsWith("A_LOG")) {
				long now = System.currentTimeMillis();
				errorReporter.log(now, name, line.replaceFirst("A_LOG[ \t]*",""));
			} else {
				System.err.println("unexpected "+cmd+" response:"+line);
			}
		}	
	}

	public void done() {
		dbgMsgln("adapter-impl: in done: running="+(running?"f":"f")+" cleanedUp="+(cleanedUp?"t":"f"));

		if (! running)
			return;

		if (cleanedUp)
			return;
		cleanedUp = true;
		
		if (in == null || out == null)
			return;
		
		String pfx = "C_QUIT";
		try {
			String cmd = pfx+"\n";
			sendRequest(cmd);
			String s = readUntil(pfx, quitAnswers);
			if (s==null)
				return;
			consumePendingLines(pfx);
			in.close();
			out.close();
		} catch (IOException e) {
			System.err.println("Adapter torx-adapt-impl: "+pfx+" io exception: "+e.getMessage());
			e.printStackTrace();
			// this may kill the main program
			// errorReporter.report("Adapter torx-adapt-impl: "+pfx+" io exception: "+e.getMessage());
			running = false;
		}
		
		
		// TODO make sleep amount configurable
		cs.destroy(500);

	}



	public AdapterInitializationExtendedResult start() {
		long startTime = System.currentTimeMillis();
		System.err.println("adapt-impl "+this.toString()+" start running="+(running?"t":"f")+" in="+in+" out="+out);
		if (running && in != null && out !=  null) {
			// check that indeed the adapter is up and running:
			String pfx = "C_IOKIND";
			try {
				String cmd = pfx+"\n";
				sendRequest(cmd);
				String s = readUntil(pfx, iokindAnswers);
				if (s==null)
					return new AdapterInitializationExtendedResultImpl(startTime, System.currentTimeMillis(), false);
				consumePendingLines(pfx);
			} catch (IOException e) {
				System.err.println("Adapter torx-adapt-impl: start: caught io exception: "+e.getMessage());
				// e.printStackTrace();
				// errorReporter.report("Adapter torx-adapt-impl: start: caught io exception: "+e.getMessage());
				running = false;
				return new AdapterInitializationExtendedResultImpl(startTime, System.currentTimeMillis(), false);
			}	
			return new AdapterInitializationExtendedResultImpl(startTime, System.currentTimeMillis(), true);
		} else
			return new AdapterInitializationExtendedResultImpl(startTime, System.currentTimeMillis(), false);
	}
	
	public void subprocessDbgMsgln(String s) {
		if (showSubProcessDebug)
			System.out.println(name+" subdbg: "+s);
	}
	public void dbgMsgln(String s) {
		if (debug)
			System.out.println(name+" dbg: "+s);
	}


	public AdapterImpl(ErrorReporter r, String command, String dirName, String[] envVars) /*throws IOException*/ {
		this(r, command, dirName, envVars, new NullProgressReporter());
	}

	public AdapterImpl(ErrorReporter r, String command, String dirName, String[] envVars, ProgressReporter p) /*throws IOException*/ {
		
		//String[] commands = {"python", "3DVis.py", "-passive"};
		// String[] settings = {"path=%path%;C:\\python25"};
		
		System.err.println("adapter-impl  "+this.toString()+" constructor begin");
		
		errorReporter = r;	
		progress = p;
		running = false;
		name = "Torx-Adapter";
		
		cs = new ChildStarter(name, r, command, dirName, p, null, envVars);
		cs.run();
		running = cs.isRunning();
		in = cs.getIn();
		Reader csout = cs.getOut();
		if (running && in!=null && csout != null)
			out = new BufferedReader(csout);
		else
			out = null;
		running = cs.isRunning();
		adapterStartTime = TimeSource.getCurrentMonotonicTime();
		String pfx = "DUMMY-THAT-DOES-NOT-EXIST";
		try {
			consumePendingLines(pfx);
		} catch (IOException e) {
			System.err.println("Adapter torx-adapt-impl: "+pfx+" io exception: "+e.getMessage());
			e.printStackTrace();
			// this may kill the main program
			// errorReporter.report("Adapter torx-adapt-impl: "+pfx+" io exception: "+e.getMessage());
			running = false;
		}
	}
	
	public static String extractEventNr(String s) {
		int i = s.length() - 1;
		while(i >= 0 && s.charAt(i) != '.')
			i--;
		if (i>0)
			return s.substring(i+1, s.length());
		return s;
	}
	
	private void sendRequest(String s) throws IOException {
		in.write(s.getBytes());
		in.flush();
	}
	
	private String inputAnswers[] = { "A_INPUT", "A_INPUT_OK", "A_OUTPUT" };
	private String outputAnswers[] = { "A_OUTPUT" };
	private String iokindAnswers[] = { "A_IOKIND" };
	private String quitAnswers[] = { "A_QUIT" };
	
	private boolean running = false;
	private ErrorReporter errorReporter;
	private String name;

	private OutputStream in;
	private BufferedReader out;

	private ProgressReporter progress = null;
	
	private Boolean showSubProcessDebug = true;
	private Boolean debug = true;
	private Boolean cleanedUp = false;
	private long adapterStartTime = 0L;

	final String adaptStimErr = "Adapter torx-adapt-impl: could not apply stimulus";
	final String adaptObsErr = "Adapter torx-adapt-impl: could not obtain observation";
	
	private ChildStarter cs;


}
