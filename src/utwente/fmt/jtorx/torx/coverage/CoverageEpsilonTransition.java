package utwente.fmt.jtorx.torx.coverage;

import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class CoverageEpsilonTransition implements CompoundTransition<CompoundState> {

	public Label getLabel() {
		return label;
	}

	public Vector<Position> getPosition() {
		// TODO Auto-generated method stub
		return null;
	}

	public Position getLabelPosition() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getEdgeName() {
		// TODO Auto-generated method stub
		return null;
	}

	public Label getSolution() {
		// TODO Auto-generated method stub
		return null;
	}

	public CompoundState getSource() {
		return srcdst;
	}

	public CompoundState getDestination() {
		return srcdst;
	}

	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		// TODO Auto-generated method stub
		return null;
	}

	public CompoundTransition<CompoundState> instantiate(Instantiation i) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public CoverageEpsilonTransition(CompoundState srcdst) {
		this.srcdst = srcdst;
	}

	private CompoundState srcdst;
	static private Label label = new LibLabel("epsilon");
}
