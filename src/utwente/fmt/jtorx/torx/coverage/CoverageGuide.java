package utwente.fmt.jtorx.torx.coverage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.logger.coverage.CoverageData;
import utwente.fmt.jtorx.torx.driver.VerdictImpl;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.test.Verdict;

public class CoverageGuide implements CompoundLTS {

	public long getNrOfStates() {
		return -1;
	}
	
	public long getNrOfTransitions() {
		return -1;
	}

	public boolean hasPosition() {
		return false;
	}
	
	public Position getRootPosition() {
		return null;
	}

	public void cleanupPreserving(Collection<? extends State> s) {
		// TODO Auto-generated method stub
		
	}

	public Iterator<? extends CompoundState> init() throws LTSException {
		if(debug) System.out.println("coverage-guide-init: start");

		Iterator<? extends CompoundState> it = primer.init();
		ArrayList<CoverageState> states = new ArrayList<CoverageState>();
		while(it.hasNext()) {
			CompoundState s = it.next();
			CoverageState cs = factory.mkState(s);
			states.add(cs);
			if(debug) System.out.println("coverage-guide-init: add state: "+s.getID()+" (=coverageState: "+cs.getID()+")");
		}
		if(debug) System.out.println("coverage-guide-init: end");

		return states.iterator();
	}

	public void done() {
		// TODO Auto-generated method stub
		
	}

	public LTS getSubLTS() {
		return primer;
	}

	public Verdict getNegativeDefaultVerdict() {
		return new VerdictImpl("fail");
	}
	public Verdict getPositiveDefaultVerdict() {
		return new VerdictImpl("pass");
	}
	
	public CompoundLTS getPrimer() {
		return primer;
	}
	
	public CoverageStateFactory getFactory() {
		return factory;
	}
	
	public CoverageData getCoverageData() {
		return coverage;
	}
	
	public IOLTSInterpretation getInterpretation() {
		return interpretation;
	}
	
	public ErrorReporter getErrorReporter() {
		return err;
	}
	
	
	public CoverageGuide(CompoundLTS primer, CoverageData data, IOLTSInterpretation i, ErrorReporter r) {
		this.primer = primer;
		factory = new CoverageStateFactory(this);
		coverage = data;
		interpretation = i;
		err = r;
	}
	
	private CompoundLTS primer;
	private CoverageStateFactory factory;
	private CoverageData coverage;
	private IOLTSInterpretation interpretation;
	private ErrorReporter err;
	protected boolean debug = false;

}
