package utwente.fmt.jtorx.torx.coverage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.SuspensionAutomatonState;
import utwente.fmt.lts.Transition;
import utwente.fmt.test.Verdict;

public class CoverageState implements SuspensionAutomatonState {
	
	public class Path {
		public Path add(CompoundTransition<? extends CompoundState> t) {
			Path p = new Path(this, t);
			return p;
		}
		public CompoundState end() {
			if (trans == null)
				return root;
			return trans.getDestination();
		}
		public Path start() {
			if (prefix==null || (prefix.trans==null && prefix.root!=null))
				return this;
			return prefix.start();
		}
		
		public Label getLabel() {
			if (trans==null)
				return null;
			return trans.getLabel();
		}
		
		public CompoundState getRoot() {
			if (prefix!=null)
				return prefix.getRoot();
			return root;
		}
			
		
		public Path(Path prefix, CompoundTransition<? extends CompoundState> t) {
			this.prefix = prefix;
			this.trans = t;
		}
		public Path(CompoundState root) {
			this.root = root;
		}
		
		public String getString() {
			String res = "";
			if (prefix!=null)
				res += prefix.getString();
			if (trans!=null)
				res += " "+trans.getLabel().getString();
			return res;
		}
		
		@Override
		public boolean equals(Object o) {
			if(this == o)
				return true;
			//		if((o == null) || (o.getClass() != this.getClass()))
			//			return false;
			if (o instanceof Path) {
				Path p = (Path) o;
				return ((p.prefix==null && prefix==null) || (p.prefix!=null&&prefix!=null && p.prefix.equals(prefix))) &&
					   ((p.trans==null && trans==null) || (p.trans!=null&&trans!=null && p.trans.equals(trans))) &&
					   ((p.root==null && root==null) || (p.root!=null&&root!=null && p.root.equals(root)));
			}
			return false;
		}

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 31 * hash + (null == prefix ? 0 : prefix.hashCode());
			hash = 31 * hash + (null == trans ? 0 : trans.hashCode());
			hash = 31 * hash + (null == root ? 0 : root.hashCode());
			return hash;
		}
		
		private Path prefix = null;
		private CompoundTransition<? extends CompoundState> trans = null;
		private CompoundState root = null;
	}

	public Collection<Path> bfsSearchFrontier(CompoundState root) {
		Set<Path> work = new HashSet<Path>();
		work.add(new Path(root));
		Set<CompoundState> states = new HashSet<CompoundState>();
		states.add(root);

		int lvl = 0;
		while (work.size() > 0) {
			Set<Path> unVisited = new HashSet<Path>();
			Set<Path> nextLayer = new HashSet<Path>();
			for(Path p: work) {
				CompoundState pp = p.end();
				if(theLTS.debug) System.out.println("bfsSearchFrontier "+lvl+" looking at "+pp+" known: "+states.contains(pp));
				states.add(pp);
				Iterator<? extends CompoundTransition<? extends CompoundState>> it = pp.menu(any.isAny());
				while(it.hasNext()) {
					CompoundTransition<? extends CompoundState> pt = it.next();
					if (theLTS.getInterpretation().isInput().isSatisfiedBy(pt.getLabel())) {
						int c = theLTS.getCoverageData().saTransitionVisitCount(pt);
						if (c==0) {
							if(theLTS.debug) System.out.println("bfsSearchFrontier "+lvl+ /*": node "+pt.getSource().getNodeName()+*/" unseen: "+pt.getLabel().getString());
							unVisited.add(p.add(pt));
						} else if (!states.contains(pt.getDestination())) {
							if(theLTS.debug) System.out.println("bfsSearchFrontier"+lvl+/*": node "+pt.getSource().getNodeName()+*/" seen: "+pt.getLabel().getString());
							states.add(pt.getDestination());
							nextLayer.add(p.add(pt));
						}
					} else {
						int c = theLTS.getCoverageData().saTransitionVisitCount(pt);
						if (c==0) {
							if(theLTS.debug) System.out.println("bfsSearchFrontier "+lvl+ /*": node "+pt.getSource().getNodeName()+*/" unseen: "+pt.getLabel().getString());
							unVisited.add(p.add(pt));
						} else if (!states.contains(pt.getDestination())) {
							if(theLTS.debug) System.out.println("bfsSearchFrontier"+lvl+/*": node "+pt.getSource().getNodeName()+*/" seen: "+pt.getLabel().getString());
							states.add(pt.getDestination());
							nextLayer.add(p.add(pt));
						}
					}
				}
			}
			ErrorReporter r = theLTS.getErrorReporter();
			long curTime = System.currentTimeMillis();
			if (unVisited.size() > 0){
				if(theLTS.debug) System.out.println("bfsSearchFrontier"+lvl+" have unvisited: size="+unVisited.size());
				if (r!=null)
					r.log(curTime, "COVERAGEGUIDERESULT", "#unvisited "+unVisited.size()+" level "+lvl+" #workpaths "+work.size()+" #nxtlayerpaths "+nextLayer.size());
				return unVisited;
			} else if (nextLayer.size() > 0 && lvl <= theLTS.getCoverageData().saTransitionCount()) {
				if(theLTS.debug) System.out.println("bfsSearchFrontier"+lvl+" recursing... size="+nextLayer.size());
				lvl += 1;
				work = nextLayer;
			} else {
				if(theLTS.debug) System.out.println("bfsSearchFrontier"+lvl+" NO unvisited... or enough recursed size="+nextLayer.size());
				if (r!=null)
					r.log(curTime, "COVERAGEGUIDERESULT", "#unvisited "+unVisited.size()+" level "+lvl+" #workpaths "+work.size()+" #nxtlayerpaths "+nextLayer.size());
				return nextLayer;
			}
		}
		return null;
	}
	public Iterator<? extends CompoundTransition<? extends CompoundState>> menu(
			LabelConstraint l) {
		ErrorReporter r = theLTS.getErrorReporter();
		long curTime = System.currentTimeMillis();
		if (r!=null)
			r.log(curTime, "COVERAGEGUIDESTART", l.toString()+" "+this.getLabel(false));

		if(theLTS.debug) System.out.println("coverage-state-menu: start "+l.toString());
		Collection<Path> unVisitedPaths = bfsSearchFrontier(primerState);
		Collection<Label> startLabels = new HashSet<Label>();
		ArrayList<CompoundTransition<? extends CompoundState>> items = new ArrayList<CompoundTransition<? extends CompoundState>>();
		for(Path p: unVisitedPaths) {
			if(theLTS.debug) System.out.println("coverage-state-menu: path: "+p.getString());
			Label pl = p.start().getLabel();	
			if (pl!=null) {
				if(theLTS.debug) System.out.println("coverage-state-menu: found start label "+pl.getString());
				startLabels.add(pl);
			} else {
				if(theLTS.debug) System.out.println("coverage-state-menu: found start label NULL for path start: "+p.start());
			}
		}
		if(theLTS.debug) System.out.println("coverage-state-menu: found "+startLabels.size()+" labels that lead to closest unseen transitions");
		Iterator<? extends CompoundTransition<? extends CompoundState>> it = primerState.menu(l);
		while(it.hasNext()) {
			CompoundTransition<? extends CompoundState> pt = it.next();
			if (!theLTS.getInterpretation().isInput().isSatisfiedBy(pt.getLabel()) || startLabels.contains(pt.getLabel())) {
				if(theLTS.debug) System.out.println("coverage-state-menu: looking at "+pt.getLabel().getString()+" ... adding");
				items.add(new CoverageTransition(this, pt, theLTS.getFactory().mkState(pt.getDestination())));
			} else {
				if(theLTS.debug) System.out.println("coverage-state-menu: looking at "+pt.getLabel().getString()+" ... NOT adding");
			}
		}

		if (unVisitedPaths.size() == 0) {
			items.add(new CoverageEpsilonTransition(this) );
		}
		return items.iterator();
	}

	public CompoundTransition<? extends CompoundState> next(Label l) {
		CompoundTransition<? extends CompoundState> pt = primerState.next(l);
		return new CoverageTransition(this, pt, theLTS.getFactory().mkState(pt.getDestination()));
	}

	public CompoundLTS getLTS() {
		return theLTS;
	}

	public Iterator<? extends State> getSubStates() {
		return primerState.getSubStates();
	}

	public Iterator<? extends State> getSubStates(LTS l) {
		return primerState.getSubStates(l);
	}

	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return primerState.getSubTransitions();
	}
	public String getSubEdgeName() {
		return primerState.getSubEdgeName();
	}


	public void disposeTransitions() {
		// TODO Auto-generated method stub
		
	}

	public Verdict getVerdict() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getID() {
		return primerState.getID();
	}

	public String getLabel(boolean quote) {
		return primerState.getLabel(quote);
	}

	public Position getPosition() {
		return primerState.getPosition();
	}

	public String getNodeName() {
		return primerState.getNodeName();
	}

	public Iterator<? extends State> getDirectSubStates() {
		if (primerState instanceof SuspensionAutomatonState) {
			SuspensionAutomatonState s = (SuspensionAutomatonState)primerState;
			return s.getDirectSubStates();
		}
		return primerState.getSubStates();
	}

	public Iterator<? extends State> getIndirectSubStates() {
		if (primerState instanceof SuspensionAutomatonState) {
			SuspensionAutomatonState s = (SuspensionAutomatonState)primerState;
			return s.getIndirectSubStates();
		}
		return null; // TODO must be empty iterator
	}

	public SuspensionAutomatonState getCanonical() {
		return this;
	}
	
	public CoverageState(CoverageGuide lts, CompoundState s) {
		theLTS = lts;
		primerState = s ;
	}
	
	private CompoundState primerState;
	private CoverageGuide theLTS;
	
	static Interpretation any = new AnyInterpretation();

}
