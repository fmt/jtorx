package utwente.fmt.jtorx.torx.coverage;

import java.util.HashMap;
import java.util.Map;

import utwente.fmt.lts.State;
import utwente.fmt.lts.CompoundState;

public class CoverageStateFactory {

	public CoverageState mkState(CompoundState s) {
		CoverageState res = map.get(s);
		if (res == null) {
			res = new CoverageState(theLTS, s);
			map.put(s, res);
		}
		return res;
	}
	
	CoverageStateFactory(CoverageGuide lts) {
		theLTS = lts;
	}
	
	private Map<State,CoverageState> map = new HashMap<State,CoverageState>();
	private CoverageGuide theLTS;
}
