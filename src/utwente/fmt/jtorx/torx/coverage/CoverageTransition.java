package utwente.fmt.jtorx.torx.coverage;

import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class CoverageTransition  implements CompoundTransition<CompoundState> {

	public Label getLabel() {
		return underlying.getLabel();
	}

	public Vector<Position> getPosition() {
		return underlying.getPosition();
	}

	public Position getLabelPosition() {
		return underlying.getLabelPosition();
	}

	public String getEdgeName() {
		return underlying.getEdgeName();
	}

	public Label getSolution() {
		return underlying.getSolution();
	}

	public CompoundState getSource() {
		return src;
	}

	public CompoundState getDestination() {
		return dst;
	}

	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return underlying.getSubTransitions();
	}

	public CompoundTransition<CompoundState> instantiate(Instantiation i) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public CoverageTransition(CoverageState src, CompoundTransition<? extends CompoundState> underlying, CoverageState dst) {
		this.src = src;
		this.dst = dst;
		this.underlying = underlying;
	}
	
	private CompoundTransition<? extends CompoundState> underlying;
	private CoverageState src;
	private CoverageState dst;

}
