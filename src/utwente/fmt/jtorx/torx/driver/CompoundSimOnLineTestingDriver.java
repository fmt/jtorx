package utwente.fmt.jtorx.torx.driver;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import utwente.fmt.jtorx.torx.AdapterInitializationExtendedResult;
import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResultConsumer;
import utwente.fmt.jtorx.torx.DriverSimInterActionResult;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.jtorx.torx.LabelPlusVerdict;
import utwente.fmt.jtorx.torx.Model;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS.LTSException;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Variable;
import utwente.fmt.test.Verdict;





// THIS IS STILL UNFINISHED;
// work-in-progress: splitting the tester package:
//  - moving the essential, non-ui functionality here,
//    as implementation of the Driver interface,
//  - and then, changing tester to be just the ui,
//  - and then, also add a gui

// With on-line vs off-line test derivation in mind
// we should rename this, because this is clearly only
// for on-line derivation and execution
// (should also add an off-line derivation, and
//  maybe even off-line execution???)

// PROBABLY we want to make this a two-level approach:
//  - basic single-test-step driver functionality
//  - multi-step/auto-bla functionality

public class CompoundSimOnLineTestingDriver implements utwente.fmt.jtorx.torx.SimOnLineTestingDriver {

	public class HistoryItem {
		public CompoundState prev;
		public CompoundState post;
		public AdapterInteractionTransitionResult<? extends CompoundState> interaction;
		
		public HistoryItem(CompoundState src, AdapterInteractionTransitionResult<? extends CompoundState> a, CompoundState dst) {
			prev = src;
			interaction = a;
			post = dst;
		}
		public HistoryItem(AdapterInteractionTransitionResult<? extends CompoundState> a) {
			interaction  = a;
		}
	}
	public class History {
		public void addToHistory(CompoundState src, AdapterInteractionTransitionResult<? extends CompoundState> a, CompoundState dst) {
			HistoryItem h = new HistoryItem(src, a, dst);
			past.add(0, h);
			while(past.size() > 2) {
				HistoryItem p = past.remove(2);
				initialState = p.post;
			}
		}

		public void pendingInteraction(AdapterInteractionTransitionResult<? extends CompoundState> a) {
			HistoryItem h = new HistoryItem(a);
			pending.add(0, h);
			if(debug) System.out.println("adding pending interaction #past="+past.size());
			if (pending.size()!=1) {
				// error (asssertion failed)
			} else {
				boolean first = true;
				while(past.size() > 0 &&
						past.elementAt(0).interaction.getTimeStamp() > a.getTimeStamp() ) {
					if (first && a.getKind() == Kind.INPUT) {
						report("NOT reordering past for wrong-ordered ("+a.getTimeStamp()+" < "+past.elementAt(0).interaction.getTimeStamp()+")"+" INPUT interaction "+a.getLabel().getString());
						return;
					} else if (first) {
						report("reordering past for interaction "+a.getLabel().getString());
						first = false;
					}
					HistoryItem p = past.remove(0);
					report("shifting wrong-ordered interaction ("+p.interaction.getTimeStamp()+" < "+a.getTimeStamp()+")"+p.interaction.getLabel().getString());
					System.out.println("\tshifting wrong-ordered interaction");
					pending.add(1,p);
				}
			}
		}
		
		public Collection<HistoryItem> replayReordered() {
			Collection<HistoryItem> tmp = pending;
			pending = new Vector<HistoryItem>();
			if(debug) System.out.println("replayReordered: size="+tmp.size());
			return tmp;
		}
		
		public CompoundState lastState() {
			if (past==null || past.size()==0)
				return initialState;
			else
				return past.firstElement().post;
		}
		
		public History(CompoundState s) {
			initialState = s;
			past = new Vector<HistoryItem>();
			pending =  new Vector<HistoryItem>();
		}
		
		private CompoundState initialState = null;
		private Vector<HistoryItem> past = null;
		private Vector<HistoryItem> pending = null;
	}
	
	public SimInitializationResultImpl init() {
		long startTime = System.currentTimeMillis();
		initialize();
		return new SimInitializationResultImpl(startTime, System.currentTimeMillis(), testerState, adapterState, verdict);
	}

	protected void initialize() {
		if (initialized)
			return;
		initialized = true;
		
		if (seed == 0) {
		    rand = new Random();
		    randMT = new MersenneTwister();
		}
		else {
			rand = new Random(seed);
			randMT = new MersenneTwister(seed);
		}
		System.err.println("driver: using internal rand? " + useInternalRand);
		System.err.println("driver: starting model...");
		try {
			testerState = myModel.start();
		} catch (LTSException e) {
			System.err.println("driver: caught LTSException: "+e.getMessage());
			testerState = null;
		}
		if (testerState==null) {
			System.err.println("driver: obtaining model failed, setting verdict...");
			verdict =  new VerdictImpl("error");
			System.err.println("driver: obtaining model failed, verdict set");
		} else {
			System.err.println("driver: starting adapter...");
			AdapterInitializationExtendedResult adapterStartResult = myAdapter.start();
			System.err.println("driver: adapter start result: "+adapterStartResult);
			if (!adapterStartResult.startedWithoutErrors()) {
				System.err.println("driver: starting adapter failed, setting verdict...");
				verdict =  new VerdictImpl("error");
				System.err.println("driver: starting adapter failed, verdict set");
			}
			adapterState = adapterStartResult.getAdapterState();
			System.err.println("driver: adapter started, obtained adapterstate");
			recentHistory = new History(testerState);
			System.err.println("driver: initialized recentHistory");
		}
	}

	public DriverFinalizationResult finish() {
		long startTime = System.currentTimeMillis();
		Collection<?extends Label> expected = null;
		if (verdict == null)
			verdict = myModel.getPositiveDefaultVerdict();
		else if (!verdict.isError())
			expected = myModel.getOutputs(testerState);
		myModel.done();
		myAdapter.done();
		return new FinalizationResultImpl(startTime, System.currentTimeMillis(), verdict, myModel.getPositiveDefaultVerdict(), stepNr, expected);
	}


	public DriverSimInterActionResult observe() {
		if(debug) System.out.println("CompoundSimOnLineTestingDriver observe");
		stepNr++;
		AdapterInteractionTransitionResult<? extends CompoundState> a_t = myAdapter.getObservation();
		Label l = a_t.getLabel();
		CompoundTransition<? extends CompoundState> nextTS = null;
		if (l==null) {
			verdict = new VerdictImpl("error");
			nextTS = new DriverTransition<CompoundState>(testerState, new LibLabel("adapter-error"), new ErrorState(testerState.getLTS()));
			return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS,  a_t.getTransition(), a_t.getConcrete(), stepNr, verdict);
		}
		recentHistory.pendingInteraction(a_t);
		AdapterInteractionTransitionResult<? extends CompoundState> h_t;
		Label h_l2;
		Collection<HistoryItem> hist = recentHistory.replayReordered();
		for (HistoryItem h: hist) {
			h_t = h.interaction;
			testerState = recentHistory.lastState();
			h_l2 = h.interaction.getLabel();
			lastInteractionTime = h_t.getTimeStamp();
			if (hist.size() > 1) {
				report("replaying history+pending (size="+hist.size()+"): timestamp: "+lastInteractionTime+" label: "+h_l2.getString());
			}
			nextTS = myModel.next(testerState, h_l2);
			if (nextTS !=  null) {
				recentHistory.addToHistory(testerState, h_t, nextTS.getDestination());
				testerState = nextTS.getDestination();
				lastWasDelta = (h_l2.getAction().getString().equalsIgnoreCase("delta"));
			} else {
				if (h_t.getKind() == Kind.INPUT) {
					verdict = new VerdictImpl("error");
				} else {
					verdict = myModel.getNegativeDefaultVerdict();
				}
				nextTS = new DriverTransition<CompoundState>(testerState, h_l2, new ExtendedFailState(testerState.getLTS(), verdict));
				return new SimInterActionResultImpl(h_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS, a_t.getTransition(), a_t.getConcrete(), stepNr, verdict);
			}
		}
		return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS, a_t.getTransition(), a_t.getConcrete(), stepNr);
	}

	public DriverSimInterActionResult stimulate(Label l) {
		if(debug) System.out.println("CompoundSimOnLineTestingDriver stimulate model: "+l);
		Iterator<Variable> var_it = l.getVariables();
		Label l_inst = null;
		if (var_it!=null && var_it.hasNext()) {
			while (var_it.hasNext()) {
				Variable v = var_it.next();
				if(debug) System.out.println("CompoundSimOnLineTestingDriver stimulate: need to instantiate: "+v.getName()+" of "+v.getType()+" ("+v.getOriginalName()+")");
			}
			l_inst = myModel.findTransition(testerState, l).getSolution();
		}
		Label l_to_use = l_inst==null?l:l_inst;
		CompoundState s_to_use = testerState;
		CompoundTransition<? extends CompoundState> nextTS = myModel.next(testerState, l_to_use);
		// System.out.println("done stimulate model: "+l_to_use);
		if (nextTS != null) {
			stepNr++;
			// System.out.println("stimulate apply-iut: "+l_to_use);
			AdapterInteractionTransitionResult<? extends CompoundState> a_t = myAdapter.applyStimulus(l_to_use);
			Label l2 = a_t.getLabel();
			if (l2==null) {
				verdict = new VerdictImpl("error");
				nextTS = new DriverTransition<CompoundState>(testerState, new LibLabel("adapter-error"), new ErrorState(testerState.getLTS()));
				return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS, a_t.getTransition(), a_t.getConcrete(), stepNr, verdict);
			}
			recentHistory.pendingInteraction(a_t);
			AdapterInteractionTransitionResult<? extends CompoundState> h_t;
			Label h_l2;
			Collection<HistoryItem> hist = recentHistory.replayReordered();
			for (HistoryItem h: hist) {
				h_t = h.interaction;
				testerState = recentHistory.lastState();
				h_l2 = h.interaction.getLabel();
				lastInteractionTime = h_t.getTimeStamp();
				if (hist.size() > 1) {
					report("replaying history+pending (size="+hist.size()+"): timestamp: "+lastInteractionTime+" label: "+h_l2.getString());
				}
				if (!l_to_use.eq(h_l2) || !s_to_use.equals(testerState))
					nextTS = myModel.next(testerState, h_l2);
				if (nextTS !=  null) {
					recentHistory.addToHistory(testerState, h_t, nextTS.getDestination());
					testerState = nextTS.getDestination();
					lastWasDelta = (h_l2.getAction().getString().equalsIgnoreCase("delta"));
				} else {
					if (h_t.getKind() == Kind.INPUT) {
						verdict = new VerdictImpl("error");
					} else {
						verdict = myModel.getNegativeDefaultVerdict();
					}
					nextTS = new DriverTransition<CompoundState>(testerState, h_l2, new ExtendedFailState(testerState.getLTS(), verdict));
					return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS, a_t.getTransition(), a_t.getConcrete(), stepNr, verdict);
				}
			}
			return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS, a_t.getTransition(), a_t.getConcrete(), stepNr);

		} else
			return null;
	}

	public DriverSimInterActionResult stimulate() {
		if(debug) System.out.println("CompoundSimOnLineTestingDriver stimulate model: ");
		Vector<Label> in = myModel.getInputs(testerState);
		if (in.size() > 0) {
			//double d = Math.random();
			//int i = ((int) ( in.size() * d));
			Collections.sort(in, new byString());
			int i = randNextInt(in.size());
			if(debug) System.err.println("Compound-Sim-online-test:stimulate: i="+i+" elem="+in.elementAt(i).getString());
			return stimulate(in.elementAt(i));
		} else
			return null;
	}

	public Vector<Label> getModelInputs() {
		return myModel.getInputs(testerState);
	}

	public Vector<Label> getModelOutputs() {
		return myModel.getOutputs(testerState);
	}

	public Vector<LabelPlusVerdict> getModelOutputVerdicts() {
		return null;
	}

	public CompoundState getTesterState() {
		return testerState;
	}
	

	public DriverSimInterActionResult randomStep() {
		Vector<Label> in = myModel.getInputs(testerState);
		Vector<Label> out = myModel.getOutputs(testerState);
		if(debug) System.out.println("CompoundSimOnLineTestingDriver randomStep: #in="+in.size()+ " #out="+out.size()+" lastwasdelta="+lastWasDelta);
		if (lastWasDelta && in.size() > 0)
			return stimulate();
		else if (out.size() > 0 && in.size() <= 0)
			return observe();
		else if (out.size() <= 0 && in.size() > 0)
			return stimulate();
		else if (out.size() <= 0 && in.size() <= 0)
			return observe(); // attempt to do something useful in guided mode, even when there seems to be nothing to do
		else {
			//double d = Math.random();
			//if (d < .5)
			if(debug) System.out.println("CompoundSimOnLineTestingDriver randomStep throw dice: #in="+in.size()+ " #out="+out.size()+" lastwasdelta="+lastWasDelta);

			int i = randNextInt(2);
			if (i == 0)
				return stimulate();
			else
				return observe();
		}
	}

	public boolean autoStep(int n, DriverInterActionResultConsumer c) {
		Boolean goOn = true;
		Boolean forever = (n <= 0);
		int i = n;
		// System.out.println("run starting");
		stop = false;
		while (goOn && !stop && (forever || i> 0)) {
			// System.out.println("run step "+i);
			goOn = c.consume(randomStep());
			i--;
		}
		c.end();
		return goOn;
	}

	public boolean autoStep(DriverInterActionResultConsumer c) {
		return autoStep(0, c);
	}

	public void stopAuto() {
		stop = true;
	}

	public CompoundSimOnLineTestingDriver(ErrorReporter r, Model m, SimAdapter a, long s){
		myModel = m;
		myAdapter = a;
		seed = s;

		verdict = null;
		stepNr = 0;
		errorReporter = r;
		recentHistory = null; // will be set in initialize()
	}
	
	public class byString implements java.util.Comparator<Label> {
		 public int compare(Label a, Label b) {
			 return a.getString().compareToIgnoreCase(b.getString());
		 }
	}
	
	protected void report(String s) {
		long now = System.currentTimeMillis();
		if(debug) System.out.println(format.format(now)+" "+s);
		errorReporter.log(now, name, s);
	}
	
	public static void setUseInternalRand(boolean useInternal) {
	    useInternalRand = useInternal;
	}
	
	private int randNextInt(int sup) {
	    if (useInternalRand) {
	        return rand.nextInt(sup);
	    }
	    else {
	        return randMT.nextInt(sup);
	    }
	}
	
	
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	protected ErrorReporter errorReporter;
	final String errRace = "interaction-race-detected";
	private String name = "Driver";
	protected long lastInteractionTime = 0L;
	protected History recentHistory = null;
	protected CompoundState testerState;
	protected CompoundState adapterState;
	private Model myModel;
	protected SimAdapter myAdapter;
	protected Verdict verdict;
	protected int stepNr;
	private long seed;
	private Random rand;
	private RandomGenerator randMT;

	protected Boolean lastWasDelta = false;
	private Boolean stop;
	private Boolean initialized = false;
	private boolean debug = false;

	private static boolean useInternalRand = true;
}
