package utwente.fmt.jtorx.torx.driver;

import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.jtorx.utils.EmptyIterator;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

class DriverTransition<T extends CompoundState> implements CompoundTransition<T> {

	public T getDestination() {
		return dst;
	}

	public Label getLabel() {
		return lbl;
	}

	public T getSource() {
		return src;
	}
	
	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return EmptyIterator.iterator();
	}

	public DriverTransition<T> instantiate(Instantiation i) {
		return this;
	}
	public Label getSolution() {
		return null;
	}


	public Vector<Position> getPosition() {
		return null;
	}
	
	public Position getLabelPosition() {
		return null;
	}
	public String getEdgeName() {
		return null;
	}

	
	public DriverTransition(T s, Label l, T d) {
		src = s;
		lbl = l;
		dst = d; 
	}
	private T src;
	private Label lbl;
	private T dst;


}
