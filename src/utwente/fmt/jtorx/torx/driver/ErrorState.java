package utwente.fmt.jtorx.torx.driver;

import utwente.fmt.lts.CompoundLTS;

public 	class ErrorState extends FailState {

	public ErrorState(CompoundLTS lts) {
		super(lts);
	}

	public String getID() {
		return "error";
	}
}

