package utwente.fmt.jtorx.torx.driver;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import utwente.fmt.jtorx.torx.AdapterInteractionTransitionResult;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.ExtendedModel;
import utwente.fmt.jtorx.torx.LabelPlusVerdict;
import utwente.fmt.jtorx.torx.SimAdapter;
import utwente.fmt.jtorx.torx.DriverSimInterActionResult;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Variable;
import utwente.fmt.test.Verdict;





// extend CompoundSimOnLineTestingDriver
// to take verdicts from the interface to the model.
// This is used to access the extended verdicts
// that we may get with test purposes, and that are
// unequal to the default positive and negative verdicts:
// <pass,hit> and <fail,hit>

public class ExtendedCompoundSimOnLineTestingDriver extends CompoundSimOnLineTestingDriver {

	public DriverFinalizationResult finish() {
		long startTime = System.currentTimeMillis();
		System.err.println("ExtendedCompoundSimOnLineTestingDriver.finish() verdict="+verdict);
		Vector<LabelPlusVerdict> expected = null;
		if (verdict == null)
			verdict = myModel.getPositiveDefaultVerdict();
		else if (!verdict.isError())
			expected = myModel.getOutputVerdicts(testerState);
		myModel.done();
		myAdapter.done();
		return new FinalizationResultImpl(startTime, System.currentTimeMillis(), verdict, myModel.getPositiveDefaultVerdict(), stepNr, expected);
	}

	
	public DriverSimInterActionResult observe() {
		if (debug) System.out.println("ExtendedCompoundSimOnLineTestingDriver observe");
		stepNr++;
		AdapterInteractionTransitionResult<? extends CompoundState> a_t = myAdapter.getObservation();
		if (a_t==null)
			return null;
		Label l = a_t.getLabel();
		CompoundTransition<? extends CompoundState> nextTS = null;
		if (l==null) {
			verdict = new VerdictImpl("error");
			nextTS = new DriverTransition<CompoundState>(testerState, new LibLabel("adapter-error"), new ErrorState(testerState.getLTS()));
			return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS,  a_t.getTransition(), a_t.getConcrete(), stepNr, verdict);
		}
		recentHistory.pendingInteraction(a_t);
		Verdict v = null;
		AdapterInteractionTransitionResult<? extends CompoundState> h_t;
		Label h_l2;
		Collection<HistoryItem> hist = recentHistory.replayReordered();
		for (HistoryItem h: hist) {
			h_t = h.interaction;
			testerState = recentHistory.lastState();
			h_l2 = h.interaction.getLabel();
			lastInteractionTime = h_t.getTimeStamp();
			if (hist.size() > 1) {
				report("replaying history+pending (size="+hist.size()+"): timestamp: "+lastInteractionTime+" label: "+h_l2.getString());
			}
			nextTS = myModel.next(testerState, h_l2);
			if (nextTS !=  null) {
				CompoundState nextTesterState = nextTS.getDestination();
				recentHistory.addToHistory(testerState, h_t, nextTesterState);
				v = mkVerdict(nextTesterState);
				if (v != null)
					verdict = v;
				else
					testerState = nextTesterState;
				lastWasDelta = (h_l2.getAction().getString().equalsIgnoreCase("delta"));
				if (verdict!=null)
					break;
			} else {
				if (h_t.getKind() == Kind.INPUT) {
					verdict = new VerdictImpl("error");
				} else {
					verdict = myModel.getNegativeDefaultVerdict();
				}
				nextTS = new DriverTransition<CompoundState>(testerState, h_l2, new ExtendedFailState(testerState.getLTS(), verdict));
				return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS, a_t.getTransition(), a_t.getConcrete(), stepNr, verdict);
			}
		}
		return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS, a_t.getTransition(), a_t.getConcrete(), stepNr, v);
	}

	public DriverSimInterActionResult stimulate(Label l) {
		if (debug) System.out.println("ExtendedCompoundSimOnLineTestingDriver stimulate model: "+l);
		Iterator<Variable> var_it = l.getVariables();
		Label l_inst = null;
		if (var_it!=null && var_it.hasNext()) {
			while (var_it.hasNext()) {
				Variable v = var_it.next();
				if(debug) System.out.println("CompoundSimOnLineTestingDriver stimulate: need to instantiate: "+v.getName()+" of "+v.getType()+" ("+v.getOriginalName()+")");
			}
			l_inst = myModel.findTransition(testerState, l).getSolution();
		}
		Label l_to_use = l_inst==null?l:l_inst;
		CompoundTransition<? extends CompoundState> nextTS = myModel.next(testerState, l_to_use);
		if(debug) System.out.println("done stimulate model: "+l_to_use+ " "+nextTS);
		if (nextTS != null) {
			stepNr++;
			// System.out.println("stimulate apply-iut: "+l_to_use);
			AdapterInteractionTransitionResult<? extends CompoundState> a_t = myAdapter.applyStimulus(l_to_use);
			if (a_t==null)
				return null;
			Label l2 = a_t.getLabel();
			if (l2==null) {
				verdict = new VerdictImpl("error");
				nextTS = new DriverTransition<CompoundState>(testerState, new LibLabel("adapter-error"), new ErrorState(testerState.getLTS()));
				return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS,  a_t.getTransition(), a_t.getConcrete(), stepNr, verdict);
			}
			recentHistory.pendingInteraction(a_t);
			Verdict v = null;
			AdapterInteractionTransitionResult<? extends CompoundState> h_t;
			Label h_l2;
			Collection<HistoryItem> hist = recentHistory.replayReordered();
			for (HistoryItem h: hist) {
				h_t = h.interaction;
				testerState = recentHistory.lastState();
				h_l2 = h.interaction.getLabel();
				lastInteractionTime = h_t.getTimeStamp();
				if (hist.size() > 1) {
					report("replaying history+pending (size="+hist.size()+"): timestamp: "+lastInteractionTime+" label: "+h_l2.getString());
				}
				if (!l_to_use.eq(h_l2))
					nextTS = myModel.next(testerState, h_l2);
				if (nextTS !=  null) {
					recentHistory.addToHistory(testerState, h_t, nextTS.getDestination());
					testerState = nextTS.getDestination();
					v = mkVerdict(testerState);
					if (v != null)
						verdict = v;
					lastWasDelta = (h_l2.getAction().getString().equalsIgnoreCase("delta"));
					if (verdict!=null)
						break;
				} else {
					if (h_t.getKind() == Kind.INPUT) {
						verdict = new VerdictImpl("error");
					} else {
						verdict = myModel.getNegativeDefaultVerdict();
					}
					nextTS = new DriverTransition<CompoundState>(testerState, l2, new ExtendedFailState(testerState.getLTS(), verdict));
					return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS, a_t.getTransition(), a_t.getConcrete(), stepNr, verdict);
				}
			}
			return new SimInterActionResultImpl(a_t.getStartTime(), a_t.getEndTime(), a_t.getTimeStamp(), a_t.getKind(), nextTS, a_t.getTransition(), a_t.getConcrete(), stepNr, v);
		} else
			return null;
	}
	
	public Vector<LabelPlusVerdict> getModelOutputVerdicts() {
		return myModel.getOutputVerdicts(testerState);
	}

	protected void initialize() {
		System.err.println("in ExtendedCompoundSimOnLineTestingDriver.initialize()");
		super.initialize();
		if (verdict==null)
			verdict = mkVerdict(testerState);
		System.err.println("in ExtendedCompoundSimOnLineTestingDriver.initialize(): verdict="+verdict);
		stepNr = 0;
	}

	public ExtendedCompoundSimOnLineTestingDriver(ErrorReporter r, ExtendedModel m, SimAdapter a, long s){
		super(r, m, a, s);
		myModel = m;
	}
	
	
	private String extractVerdictString(String s) {
		Pattern p = Pattern.compile("^verdict[ \\t]*\\(([^ (]+)\\)$");
    	Matcher m = p.matcher(s.trim());
    	if (m.matches())
    		return m.group(1).trim();
    	else
    		return s;
	}
	
	private Verdict mkVerdict(CompoundState s) {
		if(debug) System.err.println("in ExtendedCompoundSimOnLineTestingDriver.mkVerdict(): "+s);
		if (s==null)
			return new VerdictImpl("error");
		
		String verdictString = "";
		for(Label l_v: myModel.getVerdicts(s)) {
			verdictString += " "+extractVerdictString(l_v.getString());
		}
		if (!verdictString.equals(""))
			return new VerdictImpl(verdictString.trim());
		else
			return null;
	}

	private ExtendedModel myModel;
	private boolean debug = false;
}
