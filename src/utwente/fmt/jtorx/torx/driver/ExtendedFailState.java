package utwente.fmt.jtorx.torx.driver;

import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.test.Verdict;

public class ExtendedFailState extends FailState {
	
	public String getID() {
		return verdict.getString();
	}

	public ExtendedFailState(CompoundLTS lts, Verdict v) {
		super(lts);
		verdict = v;
	}

	private Verdict verdict;
}
