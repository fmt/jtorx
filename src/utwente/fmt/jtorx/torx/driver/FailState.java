package utwente.fmt.jtorx.torx.driver;

import java.util.Iterator;

import utwente.fmt.jtorx.utils.EmptyIterator;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.test.Verdict;

public class FailState implements CompoundState {

	public String getID() {
		return "fail";
	}
	public FailState getCanonical() {
		return this;
	}
	public String getLabel(boolean quote) {
		return "fail";
	}
	public Position getPosition() {
		return null;
	}
	public String getNodeName() {
		return "(failstate)";
	}

	public CompoundLTS getLTS() {
		return theLTS;
	}
	public Iterator<? extends CompoundTransition<? extends CompoundState>> menu(
			LabelConstraint l) {
		return 	EmptyIterator.iterator();
	}
	public CompoundTransition<? extends CompoundState> next(Label l) {
		return null;
	}

	public Iterator<? extends State> getSubStates() {
		return EmptyIterator.iterator();
	}
	public Iterator<? extends State> getSubStates(LTS l) {
		return getSubStates();
	}
	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return EmptyIterator.iterator();
	}
	public String getSubEdgeName() {
		return null;
	}
	public FailState(CompoundLTS lts) {
		theLTS = lts;
	}
	public void disposeTransitions() {
	}


	private CompoundLTS theLTS;


	public Verdict getVerdict() {
		// TODO Auto-generated method stub
		return null;
	}
}
