package utwente.fmt.jtorx.torx.driver;

import java.util.Collection;
import java.util.Iterator;

import utwente.fmt.lts.Label;
import utwente.fmt.test.Verdict;


public class FinalizationResultImpl implements utwente.fmt.jtorx.torx.DriverFinalizationResult {
	public Verdict getVerdict() {
		return verdict;
	}
	public Verdict getPositiveDefaultVerdict() {
		return posDefVerdict;
	}
	public Boolean hasVerdict() {
		return (verdict != null);
	}
	public int getStepNr() {
		return stepNr;
	}
	
	public long getStartTime() {
		return startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	
	public Iterator<? extends Label> getExpected() {
		if (expected==null)
			return null;
		return expected.iterator();
	}


	public FinalizationResultImpl(long s, long e, Verdict v, Verdict pdv, int n, Collection<? extends Label> exp) {
		verdict = v;
		posDefVerdict = pdv;
		stepNr = n;
		expected = exp;
		
		startTime = s;
		endTime = e;
	}

	private Verdict verdict;
	private int stepNr;
	private long startTime;
	private long endTime;
	
	private Verdict posDefVerdict;
	
	private Collection<?extends Label> expected;

}

