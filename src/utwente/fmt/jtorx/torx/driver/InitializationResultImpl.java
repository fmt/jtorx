package utwente.fmt.jtorx.torx.driver;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.test.Verdict;

public class InitializationResultImpl implements utwente.fmt.jtorx.torx.DriverInitializationResult {
	public CompoundState getState() {
		return modelState;
	}
	public InitializationResultImpl(long st, long et, CompoundState mod) {
		this(st, et, mod, null);
	}
	public Verdict getVerdict() {
		return verdict;
	}
	public Boolean hasVerdict() {
		return (verdict != null);
	}

	public long getStartTime() {
		return startTime;
	}
	public long getEndTime() {
		return endTime;
	}

	public InitializationResultImpl(long st, long et, CompoundState mod, Verdict v) {
		modelState = mod;
		verdict = v;
		startTime = st;
		endTime = et;
	}

	private CompoundState modelState;
	private Verdict verdict;
	private long startTime;
	private long endTime;
}

