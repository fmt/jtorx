package utwente.fmt.jtorx.torx.driver;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Label;
import utwente.fmt.test.Verdict;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;

public class InterActionResultImpl implements utwente.fmt.jtorx.torx.DriverInterActionResult {
	// public enum Kind {INPUT, OUTPUT };
	public Boolean hasVerdict() {
		return (verdict != null);
	}
	public Label getLabel() {
		return trans.getLabel();
	}
	public String getConcrete() {
		return concrete;
	}
	public Verdict getVerdict() {
		return verdict;
	}
	public int getStepNr() {
		return stepNr;
	}
	
	public Kind getKind() {
		return kind;
	}
	
	public CompoundState getSrc() {
		return trans.getSource();
	}
	
	public CompoundState getDst() {
		return trans.getDestination();
	}
	
	public CompoundTransition<? extends CompoundState> getTransition() {
		return trans;
	}
	
	public String getChan() {
		return chan;
	}
	public String getPCO() {
		return pco;
	}
	
	public long getStartTime() {
		return startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	public long getTimeStamp() {
		return timeStamp;
	}


	public InterActionResultImpl(long s, long e, long ts, Kind k, CompoundTransition<? extends CompoundState> t, String c, int n, Verdict v){
		startTime = s;
		endTime = e;
		timeStamp = ts;
		stepNr = n;
		verdict = v;
		kind = k;
		trans = t;
		concrete = c;
		if (kind == Kind.INPUT)
			chan = "in";
		else if (kind == Kind.OUTPUT)
			chan = "out";
		else
			chan = "";
		if (t!=null && t.getLabel()!=null && (kind == Kind.INPUT || kind == Kind.OUTPUT) && !t.getLabel().getString().equals("delta")) {
			if (t.getLabel().getString().length() > 3)
				pco = t.getLabel().getString().substring(0, 3);
			else
				pco = t.getLabel().getString();
		} else
			pco = "";
	}
	public InterActionResultImpl(long s, long e, long ts, Kind k, CompoundTransition<? extends CompoundState> t, String c, int n){
		this(s, e, ts, k, t, c, n, null);
	}

	
	private Verdict verdict;
	//private Label label;
	private int stepNr;
	private Kind kind;
	private String chan;
	private String pco;
	//private State src;
	//private State dst;
	private CompoundTransition<? extends CompoundState> trans;
	private String concrete;
	private long startTime;
	private long endTime;
	private long timeStamp;
	
	public CompoundTransition<? extends CompoundState> getAdapterTransition() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
