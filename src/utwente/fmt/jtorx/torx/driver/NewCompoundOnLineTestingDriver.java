 package utwente.fmt.jtorx.torx.driver;

import utwente.fmt.jtorx.torx.Adapter;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.Model;

// use positiveDefaultVerdict of the model,
// instead of always using "pass".
// We need this because when we use a test purpose
// the default positive verdict, i.e. when we just stop
// testing, without failing and without reaching
// the end of the test purpose, is "pass,miss"

// this extension is currently incomplete, because
// - the initialization result is not passed through correctly
// we need to refactor more before we can create a nice hierarchy

public class NewCompoundOnLineTestingDriver extends OnLineTestingDriver {

	
	public DriverFinalizationResult finish() {
		if (verdict == null)
			verdict = myModel.getPositiveDefaultVerdict();
		return super.finish(verdict);
	}


	public NewCompoundOnLineTestingDriver(Model m, Adapter a, long s){
		super(m, a, s);
		verdict = null;
	}
}
