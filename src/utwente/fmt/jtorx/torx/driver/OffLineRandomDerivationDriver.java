package utwente.fmt.jtorx.torx.driver;

import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.torx.Model;
import utwente.fmt.jtorx.torx.VoidConsumer;
import utwente.fmt.jtorx.torx.testcase.Testcase;
import utwente.fmt.jtorx.torx.testcase.TestcaseTransition;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;
import utwente.fmt.test.Node;

public class OffLineRandomDerivationDriver {
	
	public Testcase init() {
		try {
			testcase = new Testcase(model.start());
		} catch (LTSException e) {
			System.err.println("caught LTSException: "+e.getMessage());
			testcase = null;
		}
		return testcase;
	}
	public DriverInterActionResult addLevel() {
		
		// either we get rid of init method above, and let
		// the constructor already initialize testcase --
		//  but then it also has to start the model --
		// or, as we do now, we have to check that testcase != null
		if (testcase == null) {
			System.err.println("addLevel: testcase is null. did you invoke init()?");
			return null;
		}
		testcase.startAddLevel();
		System.err.println("addlevel start");
		Iterator<?extends Node> it = testcase.getOpenLeaves();
		while(it.hasNext()) {
			Node n = it.next();
			System.err.println("working on "+n.getID());
			addOneLevel(n);
			//Testcase tc = buildOneLevel(n);
			//n.extend(tc);
		}
		testcase.doneAddLevel();
		System.err.println("addlevel done");
		return null;
	}
	
	
	public boolean autoStep(int n, VoidConsumer c) {
		Boolean goOn = true;
		Boolean forever = (n <= 0);
		int i = n;
		// System.out.println("run starting");
		stop = false;
		while (goOn && !stop && (forever || i> 0)) {
			// System.out.println("run step "+i);
			addLevel();
			goOn = c.consume();
			i--;
		}
		c.end();
		return goOn;
	}
		


	public boolean autoStep(VoidConsumer c) {
		return autoStep(0, c);
	}
	
	public void stopAuto() {
			stop = true;
	}
	

	private void addOneLevel(Node n) {
		Vector<Label> in = model.getInputs(n.getState());
		Vector<Label> out = model.getOutputs(n.getState());
		Boolean lastWasDelta = n.isReachedByQuiescence();
		if (lastWasDelta && in.size() > 0)
			addOneInputLevel(n);
		else if (out.size() > 0 && in.size() <= 0)
			addOneOutputLevel(n);
		else if (out.size() <= 0 && in.size() > 0)
			addOneInputLevel(n);
		else {
			double d = Math.random();
			if (d < .5)
				addOneInputLevel(n);
			else
				addOneOutputLevel(n);
		}

	}
	private void addOneInputLevel(Node n) {
		Vector<Label> in = model.getInputs(n.getState());
		Label l = randomLabel(in);
		// special case: avoid direct self-loop, force at least one
		// non-loop edge even when original model contains selfloop
		// (like we would typically have for delta for outputs)
		Node d = testcase.mkTestcaseNode(model.next(n.getState(), l).getDestination(), n);
		Transition<Node> t = new TestcaseTransition(n, l, d);
		n.add(t);
	}
	private void addOneOutputLevel(Node n) {
		Vector<Label> out = model.getOutputs(n.getState());
		Iterator<Label> it = out.iterator();
		Boolean isDelta;
		while(it.hasNext()){
			Label l = it.next();
			// special case: avoid direct self-loop, force at least one
			// non-loop edge even when original model contains selfloop
			// (like we would typically have for delta for outputs)
			isDelta = (l.getString().equals("delta"));
			Node d = testcase.mkTestcaseNode(model.next(n.getState(), l).getDestination(), n, isDelta);
			TestcaseTransition t = new TestcaseTransition(n, l, d);
			n.add(t);
		}
	}

	private Label randomLabel(Vector<Label> s) {
		int sz = s.size();
		//dbgMsgln("randomLabel sz="+sz);
		if (sz ==1)
			return s.firstElement();
		if (sz > 1) {
			double d = Math.random();
			int idx = (int)(d*sz);
			//dbgMsgln("   randomLabel sz="+sz+" d="+d+" idx="+idx);
			return s.elementAt(idx);
		}
		return null;
	}
	

	public OffLineRandomDerivationDriver(Model m) {
		model = m;
		//testcase = new Testcase(m.start());
	}
	
	private Testcase testcase;
	private Model model;
	// private Boolean lastWasDelta = false; // cannot use that here
	private Boolean stop = false;
}
