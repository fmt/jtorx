package utwente.fmt.jtorx.torx.driver;


import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import utwente.fmt.jtorx.torx.Adapter;
import utwente.fmt.jtorx.torx.AdapterInteractionLabelResult;
import utwente.fmt.jtorx.torx.DriverFinalizationResult;
import utwente.fmt.jtorx.torx.DriverInitializationResult;
import utwente.fmt.jtorx.torx.DriverInterActionResultConsumer;
import utwente.fmt.jtorx.torx.DriverInterActionResult;
import utwente.fmt.jtorx.torx.LabelPlusVerdict;
import utwente.fmt.jtorx.torx.Model;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Variable;
import utwente.fmt.lts.LTS.LTSException;
import utwente.fmt.test.Verdict;


// this implements basic on line testing functionality
//  - does not have the extended features that we need
//    when we have test purposes
//  - assumes that the adapter has no state (LTS) information
//    that has to be passed on to the user

public class OnLineTestingDriver implements utwente.fmt.jtorx.torx.OnLineTestingDriver {
	
	public DriverInitializationResult init() {
		long startTime = System.currentTimeMillis();
		if (seed == 0)
			rand = new Random();
		else
			rand = new Random(seed);
		System.err.println("driver: starting model...");
		try {
			testerState = myModel.start();
		} catch (LTSException e) {
			System.err.println("driver: caught LTSException: "+e.getMessage());
			testerState = null;
		}
		DriverInitializationResult result;
		if (testerState==null) {
			System.err.println("driver: obtaining model failed, setting verdict...");
			verdict =  new VerdictImpl("error");
			System.err.println("driver: obtaining model failed, verdict set");
			result = new InitializationResultImpl(startTime, System.currentTimeMillis(), testerState, verdict);
		} else {
			System.err.println("driver: starting adapter...");
			if (!myAdapter.start().startedWithoutErrors()) {
				System.err.println("driver: starting adapter failed, setting verdict...");
				verdict =  new VerdictImpl("error");
				System.err.println("driver: starting adapter failed, verdict set");
				result = new InitializationResultImpl(startTime, System.currentTimeMillis(), testerState, verdict);
			} else
				result = new InitializationResultImpl(startTime, System.currentTimeMillis(), testerState);
		}
		System.err.println("driver: init returning...");
		return result;
	}
	
	public DriverFinalizationResult finish() {
		return finish(null);
	}
	protected DriverFinalizationResult finish(Verdict v) {
		long startTime = System.currentTimeMillis();
		Collection<?extends Label> expected = myModel.getOutputs(testerState);
		myModel.done();
		myAdapter.done();
		return new FinalizationResultImpl(startTime, System.currentTimeMillis(), v!=null?v:verdict, myModel.getPositiveDefaultVerdict(), stepNr, expected);
	}


	public DriverInterActionResult observe() {
		stepNr++;
		AdapterInteractionLabelResult r = myAdapter.getObservation();
		Label l = r.getLabel();
		CompoundTransition<? extends CompoundState> nextTS;
		if (l==null) {
			verdict = new VerdictImpl("error");
			nextTS = new DriverTransition<CompoundState>(testerState, new LibLabel("adapter-error"), new ErrorState(testerState.getLTS()));
			 return new InterActionResultImpl(r.getStartTime(), r.getEndTime(), r.getTimeStamp(), r.getKind(), nextTS, r.getConcrete(), stepNr, verdict);
		}
		nextTS = myModel.next(testerState, l);
		if (nextTS !=  null) {
			testerState = nextTS.getDestination();
			lastWasDelta = (l.getAction().getString().equalsIgnoreCase("delta"));
			return new InterActionResultImpl(r.getStartTime(), r.getEndTime(), r.getTimeStamp(), r.getKind(), nextTS, r.getConcrete(), stepNr);
		} else {
			verdict = new VerdictImpl("fail");
			nextTS = new DriverTransition<CompoundState>(testerState, l, new FailState(testerState.getLTS()));
			return new InterActionResultImpl(r.getStartTime(), r.getEndTime(), r.getTimeStamp(), r.getKind(), nextTS, r.getConcrete(), stepNr, verdict);
		}
	}

	public DriverInterActionResult stimulate(Label l) {
		//CompoundTransition<? extends CompoundState> nextTS = myModel.next(testerState, l);
		System.out.println("OnLineTestingDriver stimulate model: "+l);
		Iterator<Variable> var_it = l.getVariables();
		Label l_inst = null;
		if (var_it!=null && var_it.hasNext()) {
			while (var_it.hasNext()) {
				Variable v = var_it.next();
				System.out.println("OnLineTestingDriver stimulate: need to instantiate: "+v.getName()+" of "+v.getType()+" ("+v.getOriginalName()+")");
			}
			l_inst = myModel.findTransition(testerState, l).getSolution();
		}
		Label l_to_use = l_inst==null?l:l_inst;
		CompoundTransition<? extends CompoundState> nextTS = myModel.next(testerState, l_to_use);
		System.out.println("done stimulate model: "+l_to_use+ " "+nextTS);

		if (nextTS != null) {
			stepNr++;
			AdapterInteractionLabelResult r = myAdapter.applyStimulus(l_to_use);
			Label l2  = r.getLabel();
			if (l2==null) {
				verdict = new VerdictImpl("error");
				nextTS = new DriverTransition<CompoundState>(testerState, new LibLabel("adapter-error"), new ErrorState(testerState.getLTS()));
				return new InterActionResultImpl(r.getStartTime(), r.getEndTime(), r.getTimeStamp(), r.getKind(), nextTS, r.getConcrete(), stepNr, verdict);
			}
			if (!l_to_use.eq(l2))
				nextTS = myModel.next(testerState, l2);
			if (nextTS !=  null) {
				testerState = nextTS.getDestination();
				lastWasDelta = (l2.getAction().getString().equalsIgnoreCase("delta"));
				return new InterActionResultImpl(r.getStartTime(), r.getEndTime(), r.getTimeStamp(), r.getKind(), nextTS, r.getConcrete(), stepNr);
			} else {
				verdict = new VerdictImpl("fail");
				nextTS = new DriverTransition<CompoundState>(testerState, l2, new FailState(testerState.getLTS()));
				return new InterActionResultImpl(r.getStartTime(), r.getEndTime(), r.getTimeStamp(), r.getKind(), nextTS, r.getConcrete(), stepNr, verdict);
			}
		} else
			return null;
	}

	public DriverInterActionResult stimulate() {
		Vector<Label> in = myModel.getInputs(testerState);
		if (in.size() > 0) {
			//double d = Math.random();
			//int i = ((int) ( in.size() * d));
			int i = rand.nextInt(in.size());
			return stimulate(in.elementAt(i));
		} else
			return null;
	}

	public Vector<Label> getModelInputs() {
		return myModel.getInputs(testerState);
	}

	public Vector<Label> getModelOutputs() {
		return myModel.getOutputs(testerState);
	}
	
	public Vector<LabelPlusVerdict> getModelOutputVerdicts() {
		return null;
	}
	
	public CompoundState getTesterState() {
		return testerState;
	}
	
	public DriverInterActionResult randomStep() {
		Vector<Label> in = myModel.getInputs(testerState);
		Vector<Label> out = myModel.getOutputs(testerState);
		if (lastWasDelta && in.size() > 0)
			return stimulate();
		else if (out.size() > 0 && in.size() <= 0)
			return observe();
		else if (out.size() <= 0 && in.size() > 0)
			return stimulate();
		else if (out.size() <= 0 && in.size() <= 0)
			return observe(); // attempt to do something useful in guided mode, even when there seems to be nothing to do
		else {
			//double d = Math.random();
			//if (d < .5)
			int i = rand.nextInt(2);
			if (i == 0)
				return stimulate();
			else
				return observe();
		}
	}

	public boolean autoStep(int n, DriverInterActionResultConsumer c) {
		Boolean goOn = true;
		Boolean forever = (n <= 0);
		int i = n;
		// System.out.println("run starting");
		stop = false;
		while (goOn && !stop && (forever || i> 0)) {
			// System.out.println("run step "+i);
			goOn = c.consume(randomStep());
			i--;
		}
		c.end();
		return goOn;
	}
		


	public boolean autoStep(DriverInterActionResultConsumer c) {
		return autoStep(0, c);
	}
	
	public void stopAuto() {
			stop = true;
	}

	public OnLineTestingDriver(Model m, Adapter a, long s){
		myModel = m;
		myAdapter = a;
		seed = s;
	
		verdict = new VerdictImpl("pass");
		stepNr = 0;
	}
	
	private CompoundState testerState;

	protected Model myModel;
	private Adapter myAdapter;
	protected Verdict verdict;
	protected int stepNr;
	private long seed;
	private Random rand;
	
	private Boolean lastWasDelta = false;
	private Boolean stop;

}
