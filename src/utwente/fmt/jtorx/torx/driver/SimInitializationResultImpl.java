package utwente.fmt.jtorx.torx.driver;

import utwente.fmt.jtorx.torx.DriverSimInitializationResult;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.test.Verdict;

public class SimInitializationResultImpl extends InitializationResultImpl implements DriverSimInitializationResult {
	public CompoundState getAdapterState() {
		return implState;
	}

	public SimInitializationResultImpl(long st, long et, CompoundState mod, CompoundState impl) {
		this(st, et, mod, impl, null);
	}

	public SimInitializationResultImpl(long st, long et, CompoundState mod, CompoundState impl, Verdict v) {
		super(st, et, mod,v);
		implState = impl;
	}

	private CompoundState implState;
}

