package utwente.fmt.jtorx.torx.driver;

import utwente.fmt.jtorx.torx.DriverSimInterActionResult;
import utwente.fmt.jtorx.torx.InterActionResult.Kind;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.State;
import utwente.fmt.test.Verdict;

class SimInterActionResultImpl extends InterActionResultImpl implements DriverSimInterActionResult {

	public CompoundTransition<? extends CompoundState> getAdapterTransition() {
		return adaptTrans;
	}

	public SimInterActionResultImpl(long s, long e, long ts, Kind k, CompoundTransition<? extends CompoundState> t, CompoundTransition<? extends State> a_t, String c, int n, Verdict v){
		super(s, e, ts, k, t, c, n, v);
		adaptTrans = a_t;
	}
	
	public SimInterActionResultImpl(long s, long e, long ts, Kind k, CompoundTransition<? extends CompoundState> t, CompoundTransition<? extends State> a_t, String c, int n){
		this(s, e, ts, k, t, a_t, c, n, null);
	}

	private CompoundTransition<? extends State> adaptTrans;
}
