package utwente.fmt.jtorx.torx.driver;

public class VerdictImpl implements utwente.fmt.test.Verdict {

	public String getString() {
		return verdict;
	}
	
	public Boolean isFail() {
		return verdict.equalsIgnoreCase("fail") || verdict.startsWith("fail,");
	}
	public Boolean isError() {
		return verdict.equalsIgnoreCase("error") || verdict.startsWith("error,");
	}
	
	public boolean equals(Object o) {
		if(this == o)
			return true;
		if (o instanceof VerdictImpl) {
			VerdictImpl v = (VerdictImpl) o;
			if (verdict != null)
				return verdict.equals(v);
		}
		return false;
	}

	
	//public int getStepNr() {
	//	return stepNr;
	//}
	
	public VerdictImpl(String s) {
		verdict = s;
		//stepNr = 0;
	}

	private String verdict;
	//private int stepNr;
}
