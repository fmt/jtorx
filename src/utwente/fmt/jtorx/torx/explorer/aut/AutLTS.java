package utwente.fmt.jtorx.torx.explorer.aut;

import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.torx.explorer.lib.LibState;
import utwente.fmt.jtorx.torx.explorer.lib.LibStateIterator;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class AutLTS implements utwente.fmt.lts.LTS {

	public Iterator<? extends State> init() {
		LibStateIterator it = new LibStateIterator();
		if (start!=null)
			it.add(start);
		return it;
	}
	
	public void done() {
	}
	
	public long getNrOfStates() {
		return nrOfStates;
	}
	public long getNrOfTransitions() {
		return nrOfTrans;
	}

	public boolean hasPosition() {
		return false;
	}
	
	public Position getRootPosition() {
		return null;
	}
	
	public void cleanupPreserving(Collection<?extends State> s) {
	}

	public AutLTS (String fname) throws IOException {
		filereader(fname);
	}
	
	private void setStart(String s) {
		LibState as = mkState(s);
		start = as;
	}
	
	private LibState mkState(String s) {
		LibState as = stateMap.get(s);
		if (as == null) {
			as = new LibState(this, s, s, s);
			stateMap.put(s, as);
			nrOfStates++;
		}
		return as;
	}
	
	private LibLabel mkLabel(String l) {
		LibLabel al = labelMap.get(l);
		if (al == null) {
			al = new LibLabel(l);
			labelMap.put(l, al);
		}
		return al;
	}
	
	private void mkTransition(String s, String l, String d, String id) {
		LibState as = mkState(s);
		LibState ad = mkState(d);
		LibLabel al = mkLabel(l);
		as.addSucc(al, ad, null, null, id);
		nrOfTrans++;
	}
	
	private void filereader(String fname) throws IOException{
		FileInputStream fin;
		String line;
		String src, label, dst;
		String desNrOfStatesString = "";
		String desNrOfTransString = "";

		
		nrOfStates = 0;
		nrOfTrans = 0;
		
		fin = new FileInputStream (fname);
		BufferedReader d = new BufferedReader(new InputStreamReader(fin));
		int lineNr = 0;
		if ((line = d.readLine()) != null) {
			lineNr++;
			// parse header line
			Pattern p = Pattern.compile("^des[ \\t]*\\([ \\t]*([^ \\t,]+)[ \\t]*,[ \\t]*([^ \\t,]+)[ \\t]*,[ \\t]*([^ \\t\\)]+)[ \\t]*\\)$");
			Matcher m = p.matcher(line.trim());
			if (m.matches()) {
				desStartNr = m.group(1);
				desNrOfTransString = m.group(2);
				desNrOfStatesString = m.group(3);
				setStart(desStartNr);
			} else {
				// TODO throw exception or otherwise indicate error
				// print error message and exit
				System.err.println("AutExplorer: cannot parse des line: "+line);
			}
			Pattern pdq = Pattern.compile("^\\([ \\t]*([^ \\t,]+)[ \\t]*,[ \\t]*\"([^\"]*)\"[ \\t]*,[ \\t]*([^ \\t\\)]+)[ \\t]*\\)$");
			Pattern psq = Pattern.compile("^\\([ \\t]*([^ \\t,]+)[ \\t]*,[ \\t]*\'([^\']*)\'[ \\t]*,[ \\t]*([^ \\t\\)]+)[ \\t]*\\)$");
			Pattern puq = Pattern.compile("^\\([ \\t]*([^ \\t,]+)[ \\t]*,[ \\t]*([^,]*)[ \\t]*,[ \\t]*([^ \\t\\)]+)[ \\t]*\\)$");
			Matcher [] mm = new Matcher[3];
			mm[0] = pdq.matcher("");
			mm[1] = psq.matcher("");
			mm[2] = puq.matcher("");	 
			int i;
			Boolean matched;
			while ((line = d.readLine()) != null) {
				lineNr++;
				// parse body lines
				String trimmed = line.trim();
				i = 0;
				matched = false;
				while (i < 3 && !matched) {
					mm[i].reset(trimmed);
					if (mm[i].matches()) {
						matched = true;
						src = mm[i].group(1);
						label = mm[i].group(2);
						dst = mm[i].group(3);
						mkTransition(src, label, dst, ""+lineNr);
					}
					i++;
				}
				if (!matched)
					System.err.println("AutExplorer: cannot parse line "+lineNr+": "+line);
			}
		}

		fin.close();
		
		int desNrOfTrans, desNrOfStates;
		desNrOfTrans = Integer.parseInt(desNrOfTransString);
		desNrOfStates = Integer.parseInt(desNrOfStatesString);
		String errMsg = "";
		if (desNrOfTrans!= nrOfTrans) 
			errMsg += "#transitions ("+desNrOfStates+"!="+nrOfStates+")";
		if (desNrOfStates!= nrOfStates)
			errMsg += "#states ("+desNrOfStates+"!="+nrOfStates+")";
		if (!errMsg.equals(""))
			System.err.println("AutExplorer: incorrect des line"+errMsg);
	}
	
	
	
	// AutLTS specific method that is not in the LTS interface:
	public String getStartNr() {
		return desStartNr;
	}

	private LibState start = null;
	private HashMap<String, LibState> stateMap = new HashMap<String, LibState>();
	private HashMap<String, LibLabel> labelMap = new HashMap<String, LibLabel>();
	private String desStartNr;

	private long nrOfStates = -1;
	private long nrOfTrans = -1;
	

}
