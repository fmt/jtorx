package utwente.fmt.jtorx.torx.explorer.graphml;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;

import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.torx.explorer.lib.LibPosition;
import utwente.fmt.jtorx.torx.explorer.lib.LibState;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.Position;

// TODO make the whole parsing more robust.
// right now we do not check whether src,dst node ids are set
// when we pass them to mkState and mkTransition,
// and if label is not set we just pass the empty string, instead
// of complaining.
public class GraphMLLTS  implements utwente.fmt.lts.LTS {
	
	private LibState mkState(String s, String name, boolean isTransparent, Position p) {
		if(debug)
			System.err.println("mkState: s=\""+s+"\""+
					" name=\""+name+"\""+
					" isTransparent="+isTransparent);
		LibState result = mkState(s);
		result.setLabel(name);
		if (p!=null) {
			result.setPosition(p);
			hasPosition = true;
		}
		if (name.equals("1")) {
			start.add(result);
		}
		if (isTransparent)
			transparentNodes.add(result);
		return result;
	}
	private LibState mkState(String s) {
		if(debug)
			System.err.println("mkState: s=\""+s+"\"");
		LibState as = stateMap.get(s);
		if (as == null) {
			as = new LibState(this, s, null, s);
			stateMap.put(s, as);
			nrOfStates++;
		}
		return as;
	}
		
	private LibLabel mkLabel(String l) {
		LibLabel al = labelMap.get(l);
		if (al == null) {
			al = new LibLabel(l);
			labelMap.put(l, al);
		}
		return al;
	}
	
	private void mkTransition(String s, String d, String l, Vector<Position> edgePos, Position labelOffset, String eid) {
		if (debug)
			System.err.println("mkTransition: s=\""+s+"\" d=\""+d+"\" l=\""+l+"\"");
		LibState as = mkState(s);
		LibState ad = mkState(d);
		LibLabel al = mkLabel(l);
		Position asPos = as.getPosition();
		Position labelPos = null;
		if (asPos != null && labelOffset!=null)
			labelPos = new LibPosition(
					""+(Float.parseFloat(asPos.getX())+
					    Float.parseFloat(labelOffset.getX())), 
					""+(Float.parseFloat(asPos.getY())+
						Float.parseFloat(labelOffset.getY())));	
		as.addSucc(al, ad, edgePos, labelPos, eid);
		nrOfTrans++;
	}
	
	private Position mkPosition(String x, String y, String w, String h) {
		if (x!=null && y!=null) {
			// graphml position of node and of edgeLabel is of left-upper corner,
			// whereas we want position of center
			// (and thus we  add half height + half width)
			// also: graphml unit is pixels, _current_ dot unit is inch,
			// with conversion factor: 72 pixels per inch.
			// finally, vertical orientiation differs: (dot_y:= -1*graphml_y)
			// thus:
			// scale, and fix vertical orientation 
			// factor: 72 gives a relatively small font compared to nodes
			// 84 is better
			// 96 is ok on newspaper example, but short edges for fwgc example
			double fact = 72; // 88;
			float xf = Float.parseFloat(x); 
			float yf = Float.parseFloat(y);
			float wf = (w!=null) ? Float.parseFloat(w) : 0; 
			float hf = (h!=null) ? Float.parseFloat(h) : 0;
			double rx = (xf + wf/2.0)/fact;
			double ry = -1* (yf + hf/2.0)/fact;
			return new LibPosition(""+rx,""+ry);
		} else
			return null;
	}
	
	private Position mkPosition(String x, String y) {
		return mkPosition(x, y, null, null);
	}

	private void filereader(String filename) throws ParserConfigurationException, SAXException, IOException {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		FileInputStream fin;

		DefaultHandler handler = new DefaultHandler() {

			String nodeID;
			String edgeID;
			String from;
			String to;
			Position nodePos;
			Vector<Position> edgePos;
			Position labelOff;
			StringBuilder buf = null;
			StringBuilder nodeBuf = new StringBuilder();
			StringBuilder edgeBuf = new StringBuilder();
			boolean nodeLabelSeen = false;
			boolean edgeLabelSeen = false;
			String nodeLabel;
			String edgeLabel;
			boolean isTransparent = false;
			boolean hasBorderColor = true;
			boolean hasFillColor = true;
			boolean multipleNodeLabelsSeen = false;
			boolean multipleEdgeLabelsSeen = false;

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes)
			throws SAXException {
				if (debug) {
					if ("".equals (uri))
						System.err.println("Start element: " + qName);
					else
						System.err.println("Start element: {" + uri + "}" + localName);
					for(int i=0; i<attributes.getLength(); i++) {
						System.err.println("-"+attributes.getType(i)+" "+attributes.getQName(i)+"=\""+attributes.getValue(i)+"\"");
					}
				}
				buf = null;
				if (qName.equals("node")) {
					nodeID = attributes.getValue("id");
					nodeLabel = null;
					isTransparent = false;
					hasBorderColor = true;
					hasFillColor = true;
					nodePos = null;
					nodeLabelSeen = false;
				} else if (qName.equals("edge")) {
					from = attributes.getValue("source");
					to = attributes.getValue("target");
					edgeID = attributes.getValue("id");
					edgeLabel = null;
					isTransparent = false;
					hasBorderColor = true;
					hasFillColor = true;
					edgePos = null;
					labelOff = null;
					edgeLabelSeen = false;
				} else if (qName.equals("y:NodeLabel")) {
					buf = nodeBuf;
				} else if (qName.equals("y:EdgeLabel")) {
					buf = edgeBuf;
					// actually, label position is an offset wrt what? source of edge?
					// or, wrt something else, like position on line segment as given by modelPosition?
					labelOff = mkPosition(attributes.getValue("x"), attributes.getValue("y"),
							              attributes.getValue("width"), attributes.getValue("height"));
				}
				else if (qName.equals("y:Fill")) {
					String transparent = attributes.getValue("transparent");
					if (transparent != null && transparent.equals("true"))
						isTransparent = true;
					String hasColor = attributes.getValue("hasColor");
					if(hasColor!=null && hasColor.equals("false"))
						hasFillColor = false;
				} else if (qName.equals("y:BorderStyle")) {
					String hasColor = attributes.getValue("hasColor");
					if(hasColor!=null && hasColor.equals("false"))
						hasBorderColor = false;
				} else if (qName.equals("y:Geometry"))
					nodePos = mkPosition(attributes.getValue("x"), attributes.getValue("y"),
							             attributes.getValue("width"), attributes.getValue("height"));
				else if (qName.equals("y:Path"))
					edgePos = new Vector<Position>();
				else if (qName.equals("y:Point")) {
					Position p = mkPosition(attributes.getValue("x"), attributes.getValue("y"));
					edgePos.add(p);
				}
			}
			
			@Override
			public void endElement (String uri, String localName, String qName) {
				if (debug) {
					if ("".equals (uri))
						System.err.println("End element: " + qName);
					else
						System.err.println("End element: {" + uri + "}" + localName);
				}
				if (qName.equals("node")) {
					mkState(nodeID, nodeLabel, isTransparent || ((!hasBorderColor) && !hasFillColor), nodePos);
					nodeLabel = null;
					isTransparent = false;
					hasBorderColor = true;
					hasFillColor = true;
					nodePos = null;
				} else if (qName.equals("edge")) {
					mkTransition(from, to, (edgeLabel==null?"":edgeLabel), edgePos, labelOff, edgeID);
					from = null;
					to = null;
					edgeLabel = null;
					isTransparent = false;
					hasBorderColor = true;
					hasFillColor = true;
					edgePos = null;
				} else if (qName.equals("y:NodeLabel")) {
					if (!nodeLabelSeen) {
						nodeLabel = nodeBuf.toString();
						nodeLabelSeen = true;
					} else {
						multipleNodeLabelsSeen = true;
					}
					nodeBuf.delete(0, nodeBuf.length());
					buf = null;
				} else if (qName.equals("y:EdgeLabel")) {
					if (!edgeLabelSeen) {
						edgeLabel = edgeBuf.toString();
						edgeLabelSeen = true;
					} else {
						multipleEdgeLabelsSeen = true;
					}
					edgeBuf.delete(0, edgeBuf.length());
					buf = null;
				}
			}

			@Override
			public void characters(char ch[], int start, int length)
			throws SAXException {
				if (debug) {
					System.err.print("Characters: \"");
					for (int i = start; i < start + length; i++) {
						switch (ch[i]) {
						case '\\': System.err.print("\\\\"); break;
						case '"': System.err.print("\\\""); break;
						case '\n': System.err.print("\\n"); break;
						case '\r': System.err.print("\\r"); break;
						case '\t': System.err.print("\\t"); break;
						default: System.err.print(ch[i]); break;
						}
					}
					System.err.print("\"\n");
				}

				if (buf != null) {
					// buf.append(ch, start, length);
					for (int i = start; i < start + length; i++) {
						switch (ch[i]) {
						case '\\': buf.append("\\\\"); break;
						case '"': buf.append("\\\""); break;
						case '\n': buf.append("\\n"); break;
						case '\r': buf.append("\\r"); break;
						case '\t': buf.append("\\t"); break;
						default: buf.append(ch[i]); break;
						}
					}

				}
			}
			
			@Override
			public void endDocument() {
				if (multipleNodeLabelsSeen && multipleEdgeLabelsSeen) {
					err.report("Warning: graphml file contains nodes and edges with multiple labels; for those nodes and edges the first label encountered is used as state resp. transition label");
				} else if (multipleNodeLabelsSeen) {
					err.report("Warning: graphml file contains nodes with multiple labels; for those nodes the first label encountered is used as state label");
				} else if (multipleEdgeLabelsSeen) {
					err.report("Warning: graphml file contains edges with multiple labels; for those edges the first label encountered is used as transition label");
				}
			}
			
			@Override
			public void warning(SAXParseException e) {
				err.report("XML parser warning: "+e.getMessage());
			}
			
			@Override
			public void error(SAXParseException e) {
				err.report("XML parser error: "+e.getMessage());
			}
			
			@Override
			public void fatalError(SAXParseException e) throws SAXException {
				err.report("XML parser fatal error: "+e.getMessage());
				throw new SAXException(e);
			}

		};

		fin = new FileInputStream (filename);
		saxParser.parse(fin, handler);
		fin.close();
		
	}
	
	public GraphMLLTS(String filename, ErrorReporter err) throws ParserConfigurationException, SAXException, IOException {
		// TODO report errors otherwise, e.g. via ErrorReporter, so user can see them
		if (debug)
			System.err.println("graphml trying to read "+filename);
		this.err = err;
		filereader(filename);
		// experiment to allow initial transitions to be represented by
		// transparent state with single label-less outgoing transition
		Vector<State> realStart =  new Vector<State>();
		Vector<Position> rootPositions =  new Vector<Position>();
		Interpretation i = new AnyInterpretation();
		nrOfStates = 0;
		nrOfTrans = 0;
		for(State s: transparentNodes) {
			Iterator<? extends Transition<? extends State>> it = s.menu(i.isAny());
			if (it.hasNext()) {
				Transition<?extends State> t = it.next();
				if (! it.hasNext() && t!=null && t.getLabel()!=null &&
						t.getLabel().getString()!= null && t.getLabel().getString().equals("")) {
					realStart.add(t.getDestination());
					rootPositions.add(t.getSource().getPosition());
				}
			}
		}
		if (realStart.size() > 0)
			start = realStart;
		// TODO use proper exception
		if (start.size() < 1)
			throw new IOException("error reading graphml file: no start state given");
		if (start.size() > 1)
			throw new IOException("error reading graphml file: multiple start states given");
		if (rootPositions.size() > 0)
			rootPosition = rootPositions.firstElement();
	}

	public void done() {
		// TODO Auto-generated method stub
	}
	
	public long getNrOfStates() {
		return nrOfStates;
	}
	public long getNrOfTransitions() {
		return nrOfTrans;
	}

	public boolean hasPosition() {
		return hasPosition;
	}
	
	public Position getRootPosition() {
		return rootPosition;
	}

	public Iterator<? extends State> init() {
		//LibStateIterator it = new LibStateIterator();
		//it.addAll(start);
		return start.iterator();
	}
	
	public void cleanupPreserving(Collection<?extends State> s) {
	}

	private HashMap<String, LibState> stateMap = new HashMap<String, LibState>();
	private HashMap<String, LibLabel> labelMap = new HashMap<String, LibLabel>();
	private Vector<State> start = new Vector<State>();
	private Vector<State> transparentNodes = new Vector<State>();
	
	private long nrOfStates = -1;
	private long nrOfTrans = -1;
	private boolean hasPosition = false;
	private Position rootPosition = null;

	private ErrorReporter err;
	private boolean debug = false;

}
