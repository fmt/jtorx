package utwente.fmt.jtorx.torx.explorer.guide;

import java.util.Collection;
import java.util.Iterator;

import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;

public class GuidanceLTS implements LTS {

	public void done() {
		lts.done();
	}

	public Iterator<? extends State> init() throws LTSException {
		if (lts==null)
			return null;
		Iterator<? extends State> lts_it = lts.init();
		if (lts_it==null)
			return null;
		GuidanceStateIterator it = new GuidanceStateIterator();
		while(lts_it.hasNext())
			it.add(new GuidanceState(this, lts_it.next()));
		return it;
	}
	
	public AnyInterpretation getInterpretation() {
		return interp;
	}
	
	public Label getEpsilonLabel() {
		return epsilon;
	}
	
	public long getNrOfStates() {
		return lts.getNrOfStates();
	}
	
	public boolean hasPosition() {
		return lts.hasPosition();
	}
	
	public Position getRootPosition() {
		return lts.getRootPosition();
	}
	
	public void cleanupPreserving(Collection<?extends State> s) {
	}

	// TODO note that we cheat here: we do not take into account the epsilon transitions that we add
	public long getNrOfTransitions() {
		return lts.getNrOfTransitions();
	}
	
	public GuidanceLTS(LTS l) {
		lts = l;
	}

	private LTS lts = null;
	private AnyInterpretation interp = new AnyInterpretation();
	private Label epsilon = new LibLabel("epsilon");
}
