package utwente.fmt.jtorx.torx.explorer.guide;

import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.lts.LTS;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class GuidanceState implements utwente.fmt.lts.State {
	public LTS getLTS() {
		return theLTS;
	}
	
	public String getID() {
		return state.getID();
	}
	
	public String getLabel(boolean quote) {
		return state.getLabel(quote);
	}
	
	public Position getPosition() {
		return state.getPosition();
	}
	public String getNodeName() {
		return state.getNodeName();
	}
		
	public GuidanceState getCanonical() {
		return this;
	}


	public Iterator<? extends Transition<? extends State>> menu(LabelConstraint c) {
		if (state==null)
			return null;
		if (succ == null) {
			succ = new Vector<Transition<State>>();
			Iterator<? extends Transition<? extends State>> state_it = state.menu(theLTS.getInterpretation().isAny());
			if (state_it!=null)
				while(state_it.hasNext()) {
					Transition<?extends State> t = state_it.next();
					succ.add(new GuidanceTransition(this, t, new GuidanceState(theLTS, t.getDestination())));
				}
			if (succ.size()==0)
				succ.add(new GuidanceTransition(this, theLTS.getEpsilonLabel(), this));
		}
			
		return new GuidanceTransitionIterator<State>(succ, c);
	}

	@Override
    public boolean equals(Object o) {
		if (o instanceof GuidanceState) {
			GuidanceState s = (GuidanceState) o;
			return (s.theLTS.equals(theLTS) && s.state.equals(state));
		}
        if (o instanceof State) {
        	State s = (State) o;
        	return (state.equals(s));
        }
        return false;
    }
	
	@Override
	public int hashCode() {
		return state.hashCode();
	}

	public GuidanceState (GuidanceLTS lts, State s) {
		theLTS = lts;
		state = s;
	}
	private GuidanceLTS theLTS;
	private State state;
	private Vector<Transition<State>> succ = null;
	

}
