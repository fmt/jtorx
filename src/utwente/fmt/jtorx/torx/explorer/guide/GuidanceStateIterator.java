package utwente.fmt.jtorx.torx.explorer.guide;

import java.util.Vector;

import utwente.fmt.lts.State;

public class GuidanceStateIterator implements java.util.Iterator<State> {

	public boolean hasNext() {
		return (idx < states.size());
	}

	public GuidanceState next() {
		if (idx < states.size()) {
			return states.get(idx++);
		}
		return null;
	}
	
	public void add(GuidanceState s) {
		states.add(s);
	}
	
	private Vector<GuidanceState> states;
	private int idx;
	
	public GuidanceStateIterator() {
		states = new Vector<GuidanceState>();
		idx = 0;
		
	}

	public void remove() {
		throw new UnsupportedOperationException("GuidanceStateIterator.remove unimplemented");
	}

}
