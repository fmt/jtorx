package utwente.fmt.jtorx.torx.explorer.guide;

import java.util.Vector;

import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class GuidanceTransition implements utwente.fmt.lts.Transition<State> {

	public State getDestination() {
		return dst;
	}

	public Label getLabel() {
		return label;
	}

	public State getSource() {
		return src;
	}
		
	public GuidanceTransition instantiate(Instantiation i) {
		// TODO fix this
		return this;
	}
	public Label getSolution() {
		if(transition==null)
			return null;
		return transition.getSolution();
	}


	public Vector<Position> getPosition() {
		if(transition==null)
			return null;
		else
			return transition.getPosition();
	}
	
	public Position getLabelPosition() {
		if (transition==null)
			return null;
		else
			return transition.getLabelPosition();
	}
	public String getEdgeName() {
		if (transition==null)
			return null;
		else
			return transition.getEdgeName();
	}
	
	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		//              if((o == null) || (o.getClass() != this.getClass()))
		//                      return false;
		// TODO take variables and constraints into account!!!
		if (o instanceof GuidanceTransition) {
			GuidanceTransition s = (GuidanceTransition) o;
			return (s.src.equals(src) &&
					s.label.equals(label) &&
					s.dst.equals(dst));
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == src ? 0 : src.hashCode());
		hash = 31 * hash + (null == label ? 0 : label.hashCode());
		hash = 31 * hash + (null == dst ? 0 : dst.hashCode());
		// TODO take variables and constraints into account!!!
		return hash;
	}
	
	public GuidanceTransition(State s, Label l, State d) {
		src = s;
		label = l;
		dst = d;
	}
	public GuidanceTransition(State s, Transition<?extends State> t, State d) {
		src = s;
		label = t.getLabel();
		dst = d;
		transition = t;
	}


	private State src;
	private Label label;
	private State dst;
	
	private Transition<? extends State> transition = null;


}
