package utwente.fmt.jtorx.torx.explorer.guide;

import java.util.Vector;

import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class GuidanceTransitionIterator<T extends State> implements java.util.Iterator<Transition<? extends State>> {

	public boolean hasNext() {
		return (idx < transitions.size());
	}

	public Transition<T> next() {
		if (idx < transitions.size()) {
			Transition<T> result = transitions.elementAt(idx);
			idx++;
			skipNonMatching();

			return result;
		}
		return null;
	}

	public GuidanceTransitionIterator(Vector<Transition<T>> t, LabelConstraint c) {
		transitions = t;
		constraint = c;
		idx = 0;
		skipNonMatching();
	}
	
	private void skipNonMatching() {
		if (constraint != null && transitions != null)
			while(idx < transitions.size() && !constraint.isSatisfiedBy(transitions.elementAt(idx).getLabel()))
				idx++;
	}
	
	private Vector<Transition<T>> transitions;
	private LabelConstraint constraint;
	private int idx;
	
	public void remove() {
		throw new UnsupportedOperationException("LibTransitionIterator.remove unimplemented");
	}

}
