package utwente.fmt.jtorx.torx.explorer.gv;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import com.alexmerz.graphviz.ParseException;
import com.alexmerz.graphviz.Parser;
import com.alexmerz.graphviz.objects.Edge;
import com.alexmerz.graphviz.objects.Graph;
import com.alexmerz.graphviz.objects.Node;

import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.torx.explorer.lib.LibState;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class GvLTS implements utwente.fmt.lts.LTS {

	public void cleanupPreserving(Collection<? extends State> s) {
	}

	public void done() {
	}

	public long getNrOfStates() {
		return nrOfStates;
	}

	public long getNrOfTransitions() {
		return nrOfTrans;
	}

	public Position getRootPosition() {
		return null;
	}

	public boolean hasPosition() {
		return false;
	}

	public Iterator<? extends State> init() throws LTSException {
		return start.iterator();
	}
	
	public GvLTS (String fname) throws IOException, ParseException {
		File f = new File( fname);
		FileReader in = new FileReader(f);
		Parser p = new Parser();            
		p.parse(in);
		ArrayList<Graph> graphlist = p.getGraphs();
		if (graphlist.size() != 1) {
			throw new IOException("error reading graphviz file: expected one graph, got: "+graphlist.size());
		}
		
		java.util.ArrayList<Node> nodes = graphlist.get(0).getNodes(true);
		for(Node n: nodes) {
			mkState(n.getId().getId(),
					/*n.getId().getLabel(),*/  /* doesn't work, don't understand what this label is*/
					n.getAttribute("label"),
					/*n.getAttributes().get("label"),*/ /* this works too */
					n.getAttribute("style"),
					n.getAttribute("shape"));
		}
		java.util.ArrayList<Edge> edges = graphlist.get(0).getEdges();
		for(Edge e: edges) {
			mkTransition(e.getSource().getNode().getId().getId(), e.getTarget().getNode().getId().getId(), e.getAttribute("label"), e.getAttribute("name"));
		}
		
		Vector<State> realStart =  new Vector<State>();
		Interpretation i = new AnyInterpretation();
		nrOfStates = 0;
		nrOfTrans = 0;
		for(State s: transparentNodes) {
			Iterator<? extends Transition<? extends State>> it = s.menu(i.isAny());
			if (it.hasNext()) {
				Transition<?extends State> t = it.next();
				if (! it.hasNext() && t!=null && t.getLabel()!=null &&
						t.getLabel().getString()!= null && t.getLabel().getString().equals("")) {
					realStart.add(t.getDestination());
				}
			}
		}
		if (realStart.size() > 0)
			start = realStart;
		// TODO use proper exception
		if (start.size() < 1)
			throw new IOException("error reading graphml file: no start state given");
		if (start.size() > 1)
			throw new IOException("error reading graphml file: multiple start states given");

	}
	
	private LibState mkState(String s, String name, String style, String shape) {
		if(debug)
			System.err.println("mkState: s=\""+s+"\""+
					" name=\""+name+"\""+
					" style="+(style==null?"null":("\""+style+"\""))+
					" shape="+(shape==null?"null":("\""+shape+"\"")));
		LibState result = mkState(s);
		if (name==null || name=="")
			name = s;
		result.setLabel(name);
		if (name.equals("s0")) {
			start.add(result);
		}
		if ((style!=null && style.contains("invis")) || (shape!=null && shape.contains("box")))
			transparentNodes.add(result);
		return result;
	}
	private LibState mkState(String s) {
		if(debug)
			System.err.println("mkState: s=\""+s+"\"");
		LibState as = stateMap.get(s);
		if (as == null) {
			as = new LibState(this, s, null, s);
			stateMap.put(s, as);
			nrOfStates++;
		}
		return as;
	}
		
	private LibLabel mkLabel(String l) {
		LibLabel al = labelMap.get(l);
		if (al == null) {
			al = new LibLabel(l);
			labelMap.put(l, al);
		}
		return al;
	}
	
	private void mkTransition(String s, String d, String l, String name) {
		if (debug)
			System.err.println("mkTransition: s=\""+s+"\" d=\""+d+"\" l=\""+l+"\"");
		if (l==null)
			l = "";
		LibState as = mkState(s);
		LibState ad = mkState(d);
		l = l.replace("\\n", "\n");
		
		for(String ll: l.split("\n")) {
			if (debug)
				System.err.println("mkTransition split ll=\""+ll+"\"");
			LibLabel al = mkLabel(ll);
			as.addSucc(al, ad, null, null, name);
			nrOfTrans++;
		}
		
	}

	
	private HashMap<String, LibState> stateMap = new HashMap<String, LibState>();
	private HashMap<String, LibLabel> labelMap = new HashMap<String, LibLabel>();
	private Vector<State> start = new Vector<State>();
	private Vector<State> transparentNodes = new Vector<State>();
	
	private long nrOfStates = -1;
	private long nrOfTrans = -1;

	private boolean debug = false;


}
