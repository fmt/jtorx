package utwente.fmt.jtorx.torx.explorer.lib;

public class LibAction implements utwente.fmt.lts.Action {
	
	public String getString() {
		return s;
	}

	// TODO make the use of intern configurable
	public LibAction(String a) {
		s = a.intern();
	}
	
	private String s;
}
