package utwente.fmt.jtorx.torx.explorer.lib;

public class LibConstraint implements utwente.fmt.lts.Constraint {

	public String getString() {
		return constraint;
	}
	
	public LibConstraint(String c) {
		constraint = c;
	}
	
	private String constraint;
}
