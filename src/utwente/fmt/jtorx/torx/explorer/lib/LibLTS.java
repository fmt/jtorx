package utwente.fmt.jtorx.torx.explorer.lib;

import java.util.Collection;
import java.util.Iterator;

import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;

public class LibLTS implements utwente.fmt.lts.LTS {
	public void done() {
		// TODO Auto-generated method stub
		
	}

	public Iterator<? extends State> init() {
		LibStateIterator it = new LibStateIterator();
		it.add(start);
		return it;
	}
	
	public LibState mkInitialState() {
		return mkInitialState(null);
	}
	public LibState mkInitialState(String l) {
		start = new LibState(this, ""+(nextnr++), l, l);
		return start;
	}
	
	public LibState mkState() {
		return mkState(null);
	}
	public LibState mkState(String l) {
		return new LibState(this, ""+(nextnr++), l, l);
	}

	
	public LibLabel mkLabel(String s) {
		return new LibLabel(s);
	}
	
	public long getNrOfStates() {
		return -1;
	}
	
	public long getNrOfTransitions() {
		return -1;
	}
	
	public boolean hasPosition() {
		return false;
	}

	public Position getRootPosition() {
		return null;
	}
	
	public void cleanupPreserving(Collection<?extends State> s) {
	}

	private LibState start = null;
	private long nextnr = 0;
}
