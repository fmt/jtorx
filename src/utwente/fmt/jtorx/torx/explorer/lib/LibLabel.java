package utwente.fmt.jtorx.torx.explorer.lib;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import utwente.fmt.jtorx.utils.EmptyIterator;
import utwente.fmt.lts.Constraint;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Variable;

public class LibLabel implements utwente.fmt.lts.Label {
	
	public LibAction getAction() {
		if (a==null)
			a = mkAction(label);
		return a;
	}
	
	public String getString() {
		return label;
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (label==null ? 0 : label.hashCode());
		hash = 31 * hash + (constraint==null ? 0 : constraint.getString().hashCode());
		return hash;
	}

	@Override
	public boolean equals(Object o) {
		//System.out.println("autlabel-obj");
		if (o instanceof Label) {
	         Label l = (Label) o;
	         if (!l.getString().replaceAll("[\\t ]","").equalsIgnoreCase(this.getString().replaceAll("[\\t ]","")))
	        	 return false;
	         Constraint o_c = l.getConstraint();
	         if (constraint==null && o_c==null)
	        	 return true;
	         if (constraint!=null && constraint!=null) {
	        	 String c_s = constraint.getString();
	        	 String o_c_s = o_c.getString();
		         if (c_s==null && o_c_s==null)
		        	 return true;
		         if (c_s!=null && o_c_s!=null && o_c_s.replaceAll("[\\t ]","").equalsIgnoreCase(c_s.replaceAll("[\\t ]","")))
		        	 return true;
	         }

		}
		return false;
	}
	public Boolean eq(Label l) {
		//System.out.println("autlabel-lbl");
		return equals(l);
	}
	
	public Boolean isObservable() {
		if (isObservable != null)
			return isObservable;
		LibAction a = getAction();
		if (a.getString().equalsIgnoreCase("tau") || a.getString().equalsIgnoreCase("i"))
			return false;
		else
			return true;
	}
	
	public Iterator<Variable> getVariables() {
		if (variables==null)
			return EmptyIterator.iterator();
		return variables.iterator();
	}
	
	public Constraint getConstraint() {
		return constraint;
	}


	public LibLabel (String l) {
		this (l, null, null, null);
	}

	public LibLabel (String l, Boolean isObservable) {
		this (l, isObservable, null, null);
	}

	public LibLabel (String l, Boolean isObservable, Collection<Variable> vars) {
		this (l, isObservable, vars, null);
	}
	public LibLabel (String l, Boolean isObservable, Collection<Variable> vars, Constraint c) {
		label = unifyToLotosSyntax(l).intern();
		this.isObservable = isObservable;
		if (vars!=null)
			variables = new ArrayList<Variable>(vars);
		// a = mkAction(label);
		constraint = c;
	}

	
	private LibAction mkAction(String l) {
		String tmp = l.trim();
		if (tmp.startsWith("!") || tmp.startsWith("?"))
			return new LibAction(tmp);
		else if (tmp.endsWith("!") || tmp.endsWith("?"))
			return new LibAction(tmp);
		else
			return new LibAction(tmp.split("[ \\t!]+", 2)[0]);
	}
	
	// we use this to normalize e.g. the mcrl label representation
	// assumption: we have already used 'trim()' on the argument String
	private String unifyToLotosSyntax(String l) {
		if (l.matches("^\\w+\\(.*")) {
			// System.err.println("\tExpLabel: match("+l+")");
			String tmp = l.replaceFirst("\\(", "!");
			tmp = tmp.replaceFirst("\\)$","");
			int i = tmp.indexOf("(");
			// System.err.println("\tExpLabel: tmp("+tmp+") i="+i);
			if (i >= 0)
				tmp = tmp.substring(0,i).replaceAll(",", "!").concat(tmp.substring(i));
			else
				tmp = tmp.replaceAll(",", "!");
			return tmp;
		} else
			return l;
	}
	
	private String label;

	private ArrayList<Variable> variables = null;
	private Constraint constraint;

	private Boolean isObservable =  null;
	
	// chaching the action makes a huge difference when we have
	// states with many outgoing transitions (i.e. many labels):
	// fewer calls to String.split() is a big win;
	// can we get the action without String.split?
	private LibAction a = null;


}
