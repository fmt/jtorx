package utwente.fmt.jtorx.torx.explorer.lib;

import utwente.fmt.lts.Position;

public class LibPosition implements Position {

	public String getX() {
		return x;
	}

	public String getY() {
		return y;
	}
	
	public LibPosition(String x, String y) {
		this.x = x;
		this.y = y;
	}
	
	private String x;
	private String y;

}
