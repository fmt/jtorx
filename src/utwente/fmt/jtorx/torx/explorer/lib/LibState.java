package utwente.fmt.jtorx.torx.explorer.lib;

import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.lts.LTS;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.Position;

public class LibState implements utwente.fmt.lts.State {
	public LTS getLTS() {
		return theLTS;
	}
	
	public String getID() {
		return info;
	}
	
	public LibState getCanonical() {
		return this;
	}
	
	public Position getPosition() {
		return pos;
	}
	public String getNodeName() {
		return name; // TODO? return info when name==null?
	}

	
	// TODO decide whether we return something else than info when label==null
	public String getLabel(boolean quote) {
		String l;
		if (label!=null)
			l = label;
		else if (info!=null)
			l = info;
		else
			l = "";  // or raise error?
		if (quote && l.contains(" ")) // should we check for other whitespace?
			return "("+l+")";
		else
			return l;
	}
		
	// TODO decide whether we want to really override already present label
	public void setLabel(String l) {
		if (label==null)
			label = l;
	}
	
	public void setPosition(Position p) {
		pos = p;
	}

	public Iterator<? extends Transition<? extends State>> menu(LabelConstraint c) {
		return new LibTransitionIterator<LibState>(succ, c);
	}
	
	public void addSucc(LibLabel al, LibState ad, Vector<Position> ep, Position lp, String name) {
		succ.add(new LibTransition<LibState>(this, al, ad, ep, lp, name));
	}

	@Override
    public boolean equals(Object o) {
        if (o instanceof LibState) {
         LibState s = (LibState) o;
         return (s.theLTS.equals(theLTS) && s.info.equals(info));
        }
        return false;
    }
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (info==null ? 0 : info.hashCode());
		// TODO: what about info, hash that too?
		return hash;
	}

	
	private LTS theLTS;
	private Vector<LibTransition<LibState>> succ;
	private String info; // used to define/construct state identity
	private String label; // only used to show to user
	private Position pos;
	private String name; // for NODENAME field
	
//	public LibState (LTS lts, String s) {
//		this(lts, s, null);
//	}
	public LibState (LTS lts, String s, String l, String n) {
		theLTS = lts;
		info = s;
		label = l;
		succ = new Vector<LibTransition<LibState>>();
		name = n;
	}

}
