package utwente.fmt.jtorx.torx.explorer.lib;

import java.util.Vector;

import utwente.fmt.lts.State;

public class LibStateIterator implements java.util.Iterator<State> {

	public boolean hasNext() {
		return (idx < states.size());
	}

	public LibState next() {
		if (idx < states.size()) {
			return states.get(idx++);
		}
		return null;
	}
	
	public void add(LibState s) {
		states.add(s);
	}
	
	private Vector<LibState> states;
	private int idx;
	
	public LibStateIterator() {
		states = new Vector<LibState>();
		idx = 0;
		
	}

	public void remove() {
		throw new UnsupportedOperationException("LibStateIterator.remove unimplemented");
	}

}
