package utwente.fmt.jtorx.torx.explorer.lib;

import java.util.Vector;

import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;

public class LibTransition<T extends State> implements utwente.fmt.lts.Transition<State> {

	public T getDestination() {
		return dst;
	}

	public LibLabel getLabel() {
		return label;
	}

	public T getSource() {
		return src;
	}
	

	// for now, we avoid symbolic stuff, and just return the given transition if i.label==t.label
	// otherwise, we return null
	public LibTransition<? extends T> instantiate(Instantiation i) {
		if (i==null || i.getLabel()==null)
			return null;
		if (label.equals(i.getLabel()))
			return this;
		else
			return null;
	}
	public Label getSolution() {
		return null;
	}


	public Vector<Position> getPosition() {
		return pos;
	}
	public Position getLabelPosition() {
		return lp;
	}
	public String getEdgeName() {
		return name;
	}

	
	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		//              if((o == null) || (o.getClass() != this.getClass()))
		//                      return false;
		// TODO take variables and constraints into account!!!
		if (o instanceof LibTransition<?>) {
			LibTransition<? extends State> s = (LibTransition<?>) o;
			return (s.src.equals(src) &&
					s.label.equals(label) &&
					s.dst.equals(dst));
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == src ? 0 : src.hashCode());
		hash = 31 * hash + (null == label ? 0 : label.hashCode());
		hash = 31 * hash + (null == dst ? 0 : dst.hashCode());
		// TODO take variables and constraints into account!!!
		return hash;
	}
	
	public LibTransition(T s, LibLabel l, T d, Vector<Position> ep, Position lp, String name) {
		src = s;
		label = l;
		dst = d;
		pos = ep;
		this.lp = lp;
		this.name = name;
	}

	private T src;
	private LibLabel label;
	private T dst;
	private Vector<Position> pos;
	private Position lp;
	private String name;
}
