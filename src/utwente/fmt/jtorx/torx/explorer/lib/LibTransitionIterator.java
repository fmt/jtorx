package utwente.fmt.jtorx.torx.explorer.lib;

import java.util.Vector;

import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class LibTransitionIterator<T extends State> implements java.util.Iterator<Transition<State>> {

	public boolean hasNext() {
		return (idx < transitions.size());
	}

	public LibTransition<T> next() {
		if (idx < transitions.size()) {
			LibTransition<T> result = transitions.elementAt(idx);
			idx++;
			skipNonMatching();

			return result;
		}
		return null;
	}

	public LibTransitionIterator(Vector<LibTransition<T>> t, LabelConstraint c) {
		transitions = t;
		constraint = c;
		idx = 0;
		skipNonMatching();
	}
	
	private void skipNonMatching() {
		if (constraint != null && transitions != null)
			while(idx < transitions.size() && !constraint.isSatisfiedBy(transitions.elementAt(idx).getLabel()))
				idx++;
	}
	
	private Vector<LibTransition<T>> transitions;
	private LabelConstraint constraint;
	private int idx;
	
	public void remove() {
		throw new UnsupportedOperationException("LibTransitionIterator.remove unimplemented");
	}

}
