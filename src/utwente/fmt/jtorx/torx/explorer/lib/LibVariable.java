package utwente.fmt.jtorx.torx.explorer.lib;

import utwente.fmt.lts.Variable;

public class LibVariable implements Variable {

	public String getName() {
		return name;
	}

	public String getOriginalName() {
		return origName;
	}

	public String getType() {
		return type;
	}
	
	public LibVariable(String n, String t, String o) {
		name = n;
		type = t;
		origName = o;
	}

	String name;
	String type;
	String origName;
}
