package utwente.fmt.jtorx.torx.explorer.log;

// WARNING this does NOT take reordering into account.
// The Driver may reorder items when it notices that the timestamp
// of a new interaction is before a recent earlier interaction.
// It should be possible to reconstruct this using the TIMESTAMP
// lines that are now included in logs.

import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.torx.explorer.lib.LibState;
import utwente.fmt.jtorx.torx.explorer.lib.LibStateIterator;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class LogLTS implements utwente.fmt.lts.LTS {

	public Iterator<? extends State> init() {
		LibStateIterator it = new LibStateIterator();
		if (start!=null)
			it.add(start);
		return it;
	}
	
	public void done() {
	}
	
	private void setStart(LibState s) {
		start = s;
	}
	
	public void cleanupPreserving(Collection<?extends State> s) {
	}

	private LibState mkState(String id, String lbl, String name) {
		LibState as = stateMap.get(id);
		if (as == null) {
			as = new LibState(this, id, lbl, name);
			stateMap.put(id, as);
			nrOfStates++;
		}
		return as;
	}
	
	private LibLabel mkLabel(String l) {
		LibLabel al = labelMap.get(l);
		if (al == null) {
			al = new LibLabel(l);
			labelMap.put(l, al);
		}
		return al;
	}
	
	private void mkTransition(LibState s, LibLabel l, LibState d, String name) {
		s.addSucc(l, d, null, null, name);
		nrOfTrans++;
	}
	
	private void filereader(String fname) throws IOException{
		FileInputStream fin;
		String line;
		String keyword, number, data;
		
		String stateId = null;
		String stateLabel = null;
		String nodeName = null;
		String edgeName = null;
		LibState src = null;
		LibState dst = null;
		LibLabel label = null;
		long timeStamp = 0;
		
		int prevNr = -1;
		int stepNr = -1;
		
		nrOfStates = 0;
		nrOfTrans = 0;
		
		fin = new FileInputStream (fname);
		BufferedReader d = new BufferedReader(new InputStreamReader(fin));
		// assume that we already trimmed the string we try to match
		Pattern p = Pattern.compile("^([^ \\t]+[ \\t]+)?([^ \\t]+)[ \\t]+([0-9]+)([ \\t]+(.+))?$");
		boolean eotSeen = false;
		while ((line = d.readLine()) != null) {
			Matcher m = p.matcher(line.trim());
			if (m.matches()) {
				keyword = m.group(2);
				number = m.group(3);
				data = m.group(5);
				stepNr = Integer.parseInt(number);
				//System.err.println("loglts: cur="+stepNr+" prev="+prevNr+" kw="+keyword+" nr="+number+" data=("+data+")");
				if (stepNr != prevNr || keyword.equals("EOT")) {
					if (stateId!=null)
						dst = mkState(""+prevNr, stateLabel, nodeName); // do NOT reuse original stateID, or else we will not have a trace
					if (label!=null && src!=null && dst!=null)
						mkTransition(src, label, dst, edgeName);
					else if (dst!=null && start==null)
						setStart(dst);
					src = dst;
					label = null;
					stateId = null;
					stateLabel = null;
					nodeName = null;
					edgeName = null;
					timeStamp = 0;
				}
				
				if (keyword.equals("ABSTRACT")) {
					label = mkLabel(data);
				} else if (keyword.equals("STATEID")) {
					stateId = data;
				} else if (keyword.equals("STATELABEL")) {
					stateLabel = data;
				} else if (keyword.equals("NODENAME")) {
					nodeName = data;
				} else if (keyword.equals("EDGENAME")) {
					edgeName = data;
				} else if (keyword.equals("EOT")) {
					eotSeen = true;
				} else if (keyword.equals("TIMESTAMP")) {
					timeStamp = Long.parseLong(data);
				}
				prevNr = stepNr;
			} else {
				// TODO throw exception or otherwise indicate error
				// print error message and exit
			}
		}
		if (!eotSeen) {
			if (stateId!=null)
				dst = mkState(""+prevNr, stateLabel, nodeName); // do NOT reuse original stateID, or else we will not have a trace
			if (label!=null && src!=null && dst!=null)
				mkTransition(src, label, dst, edgeName);
			else if (dst!=null && start==null)
				setStart(dst);
		}

		//System.err.println("loglts: done reading");
		// Close our input stream
		fin.close();
	}
	
	public long getNrOfStates() {
		return nrOfStates;
	}
	public long getNrOfTransitions() {
		return nrOfTrans;
	}

	public boolean hasPosition() {
		return false;
	}

	public Position getRootPosition() {
		return null;
	}
	

	public LogLTS (String fname) throws IOException {
		filereader(fname);
	}

	
	private LibState start = null;
	private HashMap<String, LibState> stateMap = new HashMap<String, LibState>();
	private HashMap<String, LibLabel> labelMap = new HashMap<String, LibLabel>();
	
	private long nrOfStates = -1;
	private long nrOfTrans = -1;

}
