package utwente.fmt.jtorx.torx.explorer.torx;

import java.util.HashMap;

public class ExpFactory {

	
	public ExpState mkState(String s, String n) {
		ExpState ps;
		ps = new ExpState(lts, s, n);
		creates++;
		return ps;
	}

	public ExpState mkUniqState(String s, String n) {
		ExpState ps;
		if (stateMap.get(s) == null) {
			ps = new ExpState(lts, s, n);
			stateMap.put(s, ps);
			creates++;
		} else {
			ps = stateMap.get(s);
			matches++;
		}
		return ps;
	}
	
	public void report() {
		System.out.println("TorX-Explorer-factory ("+creates+")("+matches+")");
	}
	
	public ExpFactory(TorxExplorer theLTS) {
		lts = theLTS;
	}
	private HashMap<String,ExpState> stateMap = new HashMap<String,ExpState>();
	private TorxExplorer lts;
	
	private int matches = 0;
	private int creates = 0;
	
}
