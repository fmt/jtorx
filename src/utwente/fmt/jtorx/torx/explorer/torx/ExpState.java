package utwente.fmt.jtorx.torx.explorer.torx;


import java.util.Iterator;

import utwente.fmt.lts.LTS;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class ExpState implements utwente.fmt.lts.State {

	public String getID() {
		return id;
	}
	
	public ExpState getCanonical() {
		return this;
	}
	
	// TODO think of alternative to return?
	public String getLabel(boolean quote) {
		if (quote && id.contains(" ")) // should we check for other whitespace?
			return "("+id+")";
		else
			return id;
	}

	public Position getPosition() {
		return null;
	}
	public String getNodeName() {
		return nodeName.replace(',', ' '); // HACK to allow highlighting of individual nodes for multiple-node states
	}

	public LTS getLTS() {
		return lts;
	}

	public Iterator<? extends Transition<? extends State>> menu(LabelConstraint l) {
		
		// now that we stopped caching the list of transitions in
		// the ExpState, we had to make it a local variable here.
		ExpTransition<ExpState>[] transitions = null;
		
		if (transitions == null)
			transitions = lts.expand(this, id);
		if (transitions==null) {
			return null;
		}
		
		return new ExpTransitionIterator(transitions, l);
	}
	
	
	@Override
    public boolean equals(Object o) {
        if (o instanceof ExpState) {
        	ExpState s = (ExpState) o;
         return (s.lts.equals(lts) && s.id.equals(id));
        }
        return false;
    }
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (id==null ? 0 : id.hashCode());
		return hash;
	}

	public ExpState(TorxExplorer t, String s, String n) {
		lts = t;
		id = s; //.intern();
		nodeName = n;
	}
	
	private String id;
	private String nodeName = null;
	private TorxExplorer lts;

	// Experimentally we stopped caching the transitions.
	// However, it seems that his does not gain us so much:
	// after 400 step, seed5 lotos confprot memory seems to
	// have gone down from 49.6 or 49.7 Mb to 48.1 Mb
	// It would be nice to be able to make this optimization
	// end-user-configurable.
//	private ExpTransition[] transitions = null;

}
