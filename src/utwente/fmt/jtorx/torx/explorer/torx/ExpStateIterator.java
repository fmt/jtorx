package utwente.fmt.jtorx.torx.explorer.torx;

import java.util.Vector;

import utwente.fmt.lts.State;

public class ExpStateIterator implements java.util.Iterator<State> {

	public boolean hasNext() {
		return (idx < states.size());
	}

	public ExpState next() {
		if (idx < states.size()) {
			return states.get(idx++);
		}
		return null;
	}
	
	public void add(ExpState s) {
		states.add(s);
	}
	
	public ExpStateIterator() {
		states = new Vector<ExpState>();
		idx = 0;
	}
	
	private Vector<ExpState> states;
	private int idx;
	public void remove() {
		throw new UnsupportedOperationException("ExpStateIterator.remove unimplemented");
	}
	
}
