package utwente.fmt.jtorx.torx.explorer.torx;

import java.util.Vector;

import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;

public class ExpTransition<T extends ExpState> implements utwente.fmt.lts.Transition<State> {

	public T getDestination() {
		return dst;
	}

	public LibLabel getLabel() {
		return label;
	}

	public T getSource() {
		return src;
	}
	
	public ExpTransition<? extends ExpState> instantiate(Instantiation i) {
		if (src==null || src.getLTS() ==null)
			return null;
		if (src.getLTS() instanceof TorxExplorer) {
			TorxExplorer texp = (TorxExplorer)src.getLTS();
			return texp.instantiate(this, i);
		}
		return null;
	}

	public Label getSolution() {
		if (src==null || src.getLTS() ==null)
			return null;
		if (src.getLTS() instanceof TorxExplorer) {
			TorxExplorer texp = (TorxExplorer)src.getLTS();
			return texp.getSolution(this);
		}
		return null;
	}

	public Vector<Position> getPosition() {
		return pos;
	}
	public Position getLabelPosition() {
		return lp;
	}
	public String getEdgeName() {
		return edgeName.replace(',', ' '); // HACK to allow highlighting of individual edges for multiple-node states

	}


	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		//              if((o == null) || (o.getClass() != this.getClass()))
		//                      return false;
		// TODO take variables and constraints into account!!!
		if (o instanceof ExpTransition<?>) {
			ExpTransition<? extends State> s = (ExpTransition<?>) o;
			return (s.src.equals(src) &&
					s.label.equals(label) &&
					s.dst.equals(dst) &&
					(s.edgeName==null || edgeName==null || s.edgeName.equals(edgeName)));
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == src ? 0 : src.hashCode());
		hash = 31 * hash + (null == label ? 0 : label.hashCode());
		hash = 31 * hash + (null == dst ? 0 : dst.hashCode());
		// TODO take variables and constraints into account!!!
		hash = 31 * hash + (null == edgeName ? 0 : edgeName.hashCode());
		return hash;
	}
	
	public ExpTransition(T s, LibLabel l, T d, String id, String n) {
		src = s;
		label = l;
		dst = d;
		this.id = id;
		edgeName = n;
	}
	
	// the following is a hack? we need access to id in TorxExplorer.java
	public String id() {
		return id;
	}

	private T src;
	private LibLabel label;
	private T dst;
	private Vector<Position> pos;
	private Position lp;
	private String edgeName = null;
	
	private String id; // transition event number, used to refer to back to it for instantiation or deletion
	
}
