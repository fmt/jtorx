package utwente.fmt.jtorx.torx.explorer.torx;


import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class ExpTransitionIterator implements java.util.Iterator<Transition<State>> {

	public boolean hasNext() {
		return (idx < transitions.length);
	}

	public ExpTransition<ExpState> next() {
		if (idx < transitions.length) {
			ExpTransition<ExpState>  result = transitions[idx];
			idx++;
			skipNonMatching();

			return result;
		}
		return null;
	}
	
	/*
	public ExpTransitionIterator(Vector<ExpTransition> t, LabelConstraint c) {
		transitions = t;
		constraint = c;
		idx = 0;
		skipNonMatching();
	}
	*/
	public ExpTransitionIterator(ExpTransition<ExpState>[] t, LabelConstraint c) {
		transitions = t;
		constraint = c;
		idx = 0;
		skipNonMatching();
	}

	
	private void skipNonMatching() {
		if (constraint != null && transitions != null)
			while(idx < transitions.length && !constraint.isSatisfiedBy(transitions[idx].getLabel()))
				idx++;
	}
	
	private ExpTransition<ExpState>[] transitions;
	private LabelConstraint constraint;
	private int idx;
	public void remove() {
		throw new UnsupportedOperationException("ExpTransitionIterator.remove unimplemented");
	}

}
