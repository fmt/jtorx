package utwente.fmt.jtorx.torx.explorer.torx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.testnongui.casestudy.CaseStudyIO;
import utwente.fmt.jtorx.torx.Instantiator;
import utwente.fmt.jtorx.torx.explorer.lib.LibConstraint;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.torx.explorer.lib.LibVariable;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.utils.ChildStarter;
import utwente.fmt.jtorx.utils.term.TermParser;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Term;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.UnificationResult;
import utwente.fmt.lts.Variable;

public class TorxExplorer implements utwente.fmt.lts.LTS {
	
	private static final Logger LOGGER = Logger.getLogger(TorxExplorer.class.getName());
	
	class Tokenizer {
		public boolean consume(String cl) {
			if (i >= line.length())
				return false;
			if (cl.indexOf(line.charAt(i)) >= 0) {
				i++;
				return true;
			}
			return false;
		}
		public boolean eol() {
			return (i >= line.length());
		}
		public String readUntil(String cl) {
			String res = "";
			while(i < line.length() &&  cl.indexOf(line.charAt(i)) < 0 ) {
				res += line.charAt(i);
				i++;
			}
			return res;
		}
		public Tokenizer(String s) {
			line = s;
			i = 0;
		}
		private String line;
		int i = 0;
	}

	public Iterator<? extends State> init() throws LTSException {
		dbgMsgln("torx-explorer in init");
		ExpStateIterator sit = new ExpStateIterator();
		
		if (!cs.isRunning() || !running)
			throw  new LTS.LTSException("torx-explorer is not running");
		
		dbgMsgln("writing "+out.toString()+" :"+"r");
		try {
			synchronized(in) {
				out.write(new String("r\n").getBytes());
				out.flush();

				String line;
				Boolean readingHeader = true;
				while(readingHeader) {
					line = in.readLine();
					dbgMsgln("r read: "+line);
					//TODO: take care: line may be null if we fail to start the explorer
					if (line==null) {
						System.err.println("fatal: explorer eof");
						running = false;
						throw  new LTS.LTSException("error initializing torx explorer");
					}

					if (line.startsWith("R0")) {
						System.err.println("fatal: explorer error:"+line);
						throw  new LTS.LTSException("error initializing torx explorer");
					} else if (line.startsWith("A_LOG")) {
						long now = System.currentTimeMillis();
						err.log(now, name, line.replaceFirst("A_LOG[ \t]*",""));
					} else if (line.startsWith("A_DEBUG")) {
						subprocessDbgMsgln(line);
					} else if (line.startsWith("R")) {
						// this is the line we want
						// R event TAB solved TAB preds TAB freevars TAB identical
						readingHeader = false;
						Tokenizer tok = new Tokenizer(line);
						if (! tok.consume("R")) {
							System.err.println("fatal: expected R");
							throw  new LTS.LTSException("error initializing torx explorer");
						}
						if (! tok.consume(" \t")) {
							System.err.println("fatal: expected tab or space");
							throw  new LTS.LTSException("error initializing torx explorer");
						}
						String ev = tok.readUntil("\t");
						if (!tok.eol() && ! tok.consume(" \t")) {
							System.err.println("fatal: expected tab before solved");
							throw  new LTS.LTSException("error initializing torx explorer");
						}
						String svd = tok.readUntil("\t");
						if (!tok.eol() && ! tok.consume(" \t")) {
							System.err.println("fatal: expected tab before preds");
							throw  new LTS.LTSException("error initializing torx explorer");
						}
						String preds = tok.readUntil("\t");
						if (!tok.eol() && ! tok.consume(" \t")) {
							System.err.println("fatal: expected tab before vars");
							throw  new LTS.LTSException("error initializing torx explorer");
						}
						String vars = tok.readUntil("\t");
						if (!tok.eol() && ! tok.consume(" \t")) {
							System.err.println("fatal: expected tab before idnt");
							throw  new LTS.LTSException("error initializing torx explorer");
						}
						String idnt = tok.readUntil("\t");
						String nr = extractEventNr(ev);
						String nn = extractNodeName(ev);
						dbgMsgln("ev=("+ev+")");
						ExpState s = factory.mkState(nr, nn);
						sit.add(s);
					} else {
						System.err.println("unexpected post 'r' response:"+line);
					}
				}

				while(in.ready()) {
					line = in.readLine();
					if (line==null) {
						System.err.println("fatal: explorer eof");
						running = false;
						throw  new LTS.LTSException("error initializing torx explorer");
					}

					if (line.startsWith("R0")) {
						System.err.println("fatal: explorer error:"+line);
						throw  new LTS.LTSException("error initializing torx explorer");
					} else if (line.startsWith("A_LOG")) {
						long now = System.currentTimeMillis();
						err.log(now, name, line.replaceFirst("A_LOG[ \t]*",""));
					} else if (line.startsWith("A_DEBUG")) {
						subprocessDbgMsgln(line);
					} else {
						System.err.println("unexpected post 'r' response:"+line);
					}
				}


				// Alternative approach using regular expresions not used.

				//Pattern p = Pattern.compile("^R\\t([^\\t])\\t([^\\t])\\t([^\\t])");
				//Matcher m = p.matcher(line);
				//if (m.matches()) {
				//	System.err.println("matched response to 'r':"+line);
				//	String startId = m.group(1);
				//	ExpState s = ExpState.mkState(this, startId);
				//	sit.add(s);
				//} else {
				//	System.err.println("fatal: so wrong response to 'r':"+line);
				//}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.err.println("caught exception explorer: "+e.getMessage());
			running = false;
			//System.err.println("fatal: explorer error:"+line);
			throw  new LTS.LTSException("error initializing torx explorer");
		}	
		return sit;
	}
	
	public ExpTransition<ExpState>[] expand(ExpState src, String id) {

		dbgMsgln("torx-explorer in expand");

		
		if (!cs.isRunning() || !running)
			return null;

		Vector<ExpTransition<ExpState>> trans;
		
		trans = new Vector<ExpTransition<ExpState>>();

		synchronized(in) {
			TorxExplorer.dbgMsgln("writing "+out.toString()+" :"+"e"+" "+id);
			try {
				out.write(new String("e"+" "+id+"\n").getBytes());
				TorxExplorer.dbgMsgln("e cmd written:"+"e"+" "+id);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.err.println("menu write eerror for:"+out.toString()+" : "+e.getMessage());
				// e.printStackTrace();
				running = false;
				return null;
			}	
			TorxExplorer.dbgMsgln("flushing "+out.toString());
			try {
				out.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.err.println("menu flush e error for:"+out.toString()+" : "+e.getMessage());
				//e.printStackTrace();
				running = false;
				return null;
			}	


			String line;
			Boolean readingHeader = true;
			while (readingHeader) {
				try {
					line = in.readLine();
					TorxExplorer.dbgMsgln("e read: "+line);
				} catch (IOException e) {
					System.err.println("explorer caught exception: "+e.getMessage());
					// e.printStackTrace();
					running = false;
					return null;
				}
				if (line==null) {
					System.err.println("fatal: explorer error eot");
					running = false;
					return null;
				}

				if (line.startsWith("E0")) {
					System.err.println("fatal: explorer error:"+line);
					return null;
				} else if (line.startsWith("A_LOG")) {
					long now = System.currentTimeMillis();
					err.log(now, name, line.replaceFirst("A_LOG[ \t]*",""));
				} else if (line.startsWith("A_DEBUG")) {
					TorxExplorer.subprocessDbgMsgln(line);
				} else if (line.startsWith("EB")) {
					// ok, this is what we want; move to next block
					readingHeader = false;
				} else {
					System.err.println("unexpected 'e' response:"+line);
				}
			}

			Boolean readingBody = true;
			while(readingBody) {
				try {
					line = in.readLine();
					TorxExplorer.dbgMsgln("e cmd rsp:"+line);
				} catch (IOException e) {
					System.err.println("explorer caught exception: "+e.getMessage());
					// e.printStackTrace();
					running = false;
					return null;
				}
				if (line==null) {
					System.err.println("fatal: explorer error eot");
					running = false;
					return null;
				}

				if (line.startsWith("E0")) {
					System.err.println("fatal: explorer error:"+line);
					return null;
				} else if (line.startsWith("EE")) {
					TorxExplorer.dbgMsgln("done:"+line);
					readingBody = false;
				} else  if (line.startsWith("Ee")) {
					TorxExplorer.dbgMsgln("transition:"+line);
					// Ee event TAB visible TAB solved TAB label TAB preds TAB freevars TAB identical
					Tokenizer tok = new Tokenizer(line);
					if (!tok.consume("E") || !tok.consume("e")) {
						System.err.println("fatal: expected Ee");
						return null;
					}

					ExpTransition<ExpState>tt = constructTransition(src, tok, false);
					if (tt==null)
						return null;
					trans.add(tt);


				} else {
					System.err.println("unexpected 'e' response:"+line);
				}

			}
			try {
				while (in.ready()) {

					line = in.readLine();
					if (line==null) {
						System.err.println("fatal: explorer error eot");
						return null;
					}

					if (line.startsWith("E0")) {
						System.err.println("fatal: explorer error:"+line);
						return null;
					} else if (line.startsWith("A_LOG")) {
						long now = System.currentTimeMillis();
						err.log(now, name, line.replaceFirst("A_LOG[ \t]*",""));
					} else if (line.startsWith("A_DEBUG")) {
						TorxExplorer.subprocessDbgMsgln(line);
					} else {
						System.err.println("unexpected post 'e' response:"+line);
					}
				}
			} catch (IOException e) {
				System.err.println("explorer caught exception "+e.getMessage());
				//e.printStackTrace();
				running = false;
				return null;
			}
		}
		ExpTransition<ExpState>[] res = new ExpTransition[trans.size()];
		trans.toArray(res);
		return res;
	}
	
	// for now, we avoid symbolic stuff, and just return the given transition if i.label==t.label
	// otherwise, we return null
	// TODO add 
	protected ExpTransition<? extends ExpState> instantiate(ExpTransition<? extends ExpState> t, Instantiation i) {
		if (t==null || i==null)
			return null;
		if (t.getSource().getLTS() != this) // not a transition from us
			return null;
		LibLabel t_l =  t.getLabel();
		Label i_l = i.getLabel();
		if (t_l==null || i_l==null)
			return null;
		if (t_l.equals(i_l))
			return t;
		
		ExpTransition<? extends ExpState> result = null;

		if (t.getSource()!=null) {
			ExpState src = t.getSource();
			Term t_tm = TermParser.parse(t_l);
			Term i_tm = i.getTerm();
			UnificationResult uni_res = t_tm.unify(i_tm);
			if (uni_res.isUnifyable() && t.id()!=null && !t.id().trim().equalsIgnoreCase("")) {
				ArrayList<UnificationBinding> bl = uni_res.getBindings();
				// construct instantiation command string
				String cmd = "i "+t.id()+" ";
				String varName = "";
				for (UnificationBinding b: bl) {
					Iterator<Variable> vars = t_l.getVariables();
					// map general variable name used in label onto orig var name
					varName = b.getVariable().getName();
					while(vars.hasNext()) {
						Variable v = vars.next();
						if (v.getName().equals(b.getVariable().getName())) {
							varName = v.getOriginalName();
						}
					}
					cmd += varName+"="+b.getTerm().unparse()+"; ";
				}
				
				if (!cs.isRunning() || !running)
					return null;

				dbgMsgln("writing "+out.toString()+" :"+cmd);
				try {
					synchronized(in) {
						out.write(new String(cmd+"\n").getBytes());
						out.flush();

						String line;
						Boolean readingHeader = true;
						while(readingHeader) {
							line = in.readLine();
							dbgMsgln("r read: "+line);
							//TODO: take care: line may be null if we fail to start the explorer
							if (line==null) {
								System.err.println("fatal: explorer error eot");
								return null;
							}

							if (line.startsWith("I0")) {
								System.err.println("fatal: explorer error:"+line);
								return null;
							} else if (line.startsWith("A_LOG")) {
								long now = System.currentTimeMillis();
								err.log(now, name, line.replaceFirst("A_LOG[ \t]*",""));
							} else if (line.startsWith("A_DEBUG")) {
								TorxExplorer.subprocessDbgMsgln(line);
							} else  if (line.startsWith("I")) {
								readingHeader = false;
								TorxExplorer.dbgMsgln("transition:"+line);
								// I event TAB visible TAB solved TAB label TAB preds TAB freevars TAB identical
								Tokenizer tok = new Tokenizer(line);
								if (!tok.consume("I")) {
									System.err.println("fatal: expected I");
									return null;
								}

								ExpTransition<ExpState> tt = constructTransition(src, tok, true);
								if (tt==null)
									return null;
								result =  tt;

							} else {
								System.err.println("unexpected 'i' response:"+line);
							}
						}


						while(in.ready()) {
							line = in.readLine();
							if (line==null) {
								System.err.println("fatal: explorer error eot");
								return null;
							}
							if (line.startsWith("I0")) {
								System.err.println("fatal: explorer error:"+line);
								return null;
							} else if (line.startsWith("A_LOG")) {
								long now = System.currentTimeMillis();
								err.log(now, name, line.replaceFirst("A_LOG[ \t]*",""));
							} else if (line.startsWith("A_DEBUG")) {
								subprocessDbgMsgln(line);
							} else {
								System.err.println("unexpected post 'i' response:"+line);
							}
						}

					}
				} catch (IOException e) {
					System.err.println("explorer caught exception "+e.getMessage());
					//e.printStackTrace();
					running = false;
				}	


			}

		}
		return result;
	}

	// instantiator method
	public Label getInstantiation(Transition<? extends State> t) {
		return t.getSolution();
	}
	// instantiator method
	public boolean start() {
		return true;
	}
	
	protected Label getSolution(ExpTransition<? extends ExpState> t) {
		if (t.getSource().getLTS() != this) // not a transition from us
			return null;
		
		if (inst!=null) // use external instantiator
			return inst.getInstantiation(t);
		
		// try to access instantiator built-in in LTS explorer
		Label result = null;
		
		String cmd = "f "+t.id()+" ";
		
		if (!cs.isRunning() || !running)
			return null;

		dbgMsgln("writing "+out.toString()+" :"+cmd);
		try {
			synchronized(in) {
				out.write(new String(cmd+"\n").getBytes());
				out.flush();

				String line;
				Boolean readingHeader = true;
				while(readingHeader) {
					line = in.readLine();
					dbgMsgln("r read: "+line);
					//TODO: take care: line may be null if we fail to start the explorer
					if (line==null) {
						System.err.println("fatal: explorer error eot");
						return null;
					}

					if (line.startsWith("F0")) {
						System.err.println("fatal: explorer error:"+line);
						return null;
					} else if (line.startsWith("A_ERROR UnknownCommand")) {
						System.err.println("fatal: explorer error:"+line);
						return null;
					} else if (line.startsWith("A_LOG")) {
						long now = System.currentTimeMillis();
						err.log(now, name, line.replaceFirst("A_LOG[ \t]*",""));
					} else if (line.startsWith("A_DEBUG")) {
						TorxExplorer.subprocessDbgMsgln(line);
					} else  if (line.startsWith("F")) {
						readingHeader = false;
						TorxExplorer.dbgMsgln("solution:"+line);
						// F label
						Tokenizer tok = new Tokenizer(line);
						if (!tok.consume("F")) {
							System.err.println("fatal: expected I");
							return null;
						}

						if (!tok.consume(" \t")) {
							System.err.println("fatal: expected tab or space before label");
							return null;
						}
						String lbl = tok.readUntil("\t");
						ArrayList<Variable> variables = new ArrayList<Variable>();
						LibConstraint constraint = null;
						result = new LibLabel(lbl, true, variables, constraint);
					} else {
						System.err.println("unexpected 'f' response:"+line);
					}
				}

				while(in.ready()) {
					line = in.readLine();
					if (line==null) {
						System.err.println("fatal: explorer error eot");
						return null;
					}
					if (line.startsWith("F0")) {
						System.err.println("fatal: explorer error:"+line);
						return null;
					} else if (line.startsWith("A_LOG")) {
						long now = System.currentTimeMillis();
						err.log(now, name, line.replaceFirst("A_LOG[ \t]*",""));
					} else if (line.startsWith("A_DEBUG")) {
						subprocessDbgMsgln(line);
					} else {
						System.err.println("unexpected post 'f' response:"+line);
					}
				}

			}
		} catch (IOException e) {
			System.err.println("explorer caught exception: "+e.getMessage());
			// e.printStackTrace();
			running = false;
		}	
		
		return result;

	}

	
	public void done() {
		dbgMsgln("in done");
		
		factory.report();
		
		if (cleanedUp)
			return;
		cleanedUp = true;

		if (in!=null && out !=null)
			try {
				synchronized(in) {
				    dbgMsgln("shutting down symtosim...");
					out.write(new String("q\n").getBytes());
					out.flush();
					String line = in.readLine();
					while(line != null) {
						/*if (line==null) {
							System.err.println("fatal: explorer eof");
							return;
						}*/
						if (line.startsWith("Q0")) {
							System.err.println("fatal: explorer error:"+line);
							return;
						} else if (line.startsWith("Q")) {
							// ok, this we expect
		                    dbgMsgln("symtosim shut down regularly.");
						} else if (line.startsWith("A_LOG")) {
							long now = System.currentTimeMillis();
							String loggedLine = line.replaceFirst("A_LOG[ \t]*","") + "\n";
							err.log(now, name, loggedLine);
							CaseStudyIO.out.print(loggedLine);
						} else if (line.startsWith("A_DEBUG")) {
							subprocessDbgMsgln(line);
						} else {
							System.err.println("unexpected post 'r' response:"+line);
						}
						line = in.readLine();
					}
                    dbgMsgln("closing comm channels with symtosim.");
					out.close();
					in.close();
				}
			} catch (IOException e) {
				System.err.println("TorxExplorer: done: caught IO exception: "+e.getMessage());
				//e.printStackTrace();
			}
		running = false;
	}
	
	public ExpFactory getFactory() {
		dbgMsgln("torx-explorer in getFactory");
		return factory;
	}
	
	public void cleanupPreserving(Collection<?extends State> s) {
		// should clean level in ExpFactory???
	}

	
	/*
	public TorxExplorer(String filename) throws IOException {
		factory = new ExpFactory(this);
		
		File file = new File(filename);
		
		String cmd[] = new String[1];
		cmd[0] = file.getAbsoluteFile().getPath();
		File dir = file.getAbsoluteFile().getParentFile();
		
		process = Runtime.getRuntime().exec( cmd, new String[0], dir );
		out = new DataOutputStream(process.getOutputStream());
        in = new BufferedReader(new InputStreamReader(process.getInputStream()));
        err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
   

	}
	*/
	
	public long getNrOfStates() {
		return -1;
	}
	
	public long getNrOfTransitions() {
		return -1;
	}

	public boolean hasPosition() {
		return false;
	}
	
	public Position getRootPosition() {
		return null;
	}
	
	public TorxExplorer(ErrorReporter r, String[]  command, String dirName, String[] envVars) throws IOException {
		this(r, command, dirName, envVars, new NullProgressReporter(), null);
	}

	public TorxExplorer(ErrorReporter r, String command[], String dirName, String[] envVars, ProgressReporter p, Instantiator i) throws IOException {
		factory = new ExpFactory(this);
		
		System.err.println("torx-explorer  "+this.toString()+" constructor begin");
		
		err = r;
		running = false;
		name = "Torx-Explorer";
		inst = i;
		LOGGER.fine("[progress] Beginning to start a SymToSim instance for a TorxExplorer.");
		for (int rc = 0; rc < CHILDSTARTER_MAX_RETRIES && !running; rc++) {
		    try {
		        cs = new ChildStarter(name, r, command, dirName, p, null, envVars); // TODO: add log reporter
		        cs.run();
		        running = cs.isRunning();
		        
	            try {
	                Thread.sleep(1500);
	            }
	            catch(InterruptedException e1) {
	                System.err.println("Thread sleep interrupted");
	            }
		        
		        out = cs.getIn();
		        Reader csout = cs.getOut();
		        if (running && out!=null && csout != null)
		            in = new BufferedReader(csout);
		        else
		            in = null;
		        running = cs.isRunning() && in != null && out != null;
		    }
		    catch (Exception e) {
		        System.err.println("Got childstarter exception " + e);
		        e.printStackTrace();
		        System.err.println("Retries left: " + (CHILDSTARTER_MAX_RETRIES - rc));
		        try {
                    Thread.sleep(CHILDSTARTER_RETRY_DELAY);
                }
                catch (InterruptedException e1) {
                    System.err.println("Thread sleep interrupted");
                }
		    }
		    if (!CHILDSTARTER_RETRY) {
		        break;
		    }
		}
		LOGGER.fine("[progress] Done starting a SymToSim instance for a TorxExplorer.");
		
		if (!running) {
		    System.err.println("Fatal: Aborting: Could not start child!");
		    System.exit(-42);
		}
		
		
		dbgMsgln("torx-explorer end of constructor: running="+cs.isRunning());
	}

	
	public OutputStream getOut() {
		dbgMsgln("torx-explorer getOut");
		return out;
	}
	public BufferedReader getIn() {
		dbgMsgln("torx-explorer getIn");
		return in;
	}

		
	public static String extractEventNr(String s) {
		if (s==null)
			return "";
		int i = s.length() - 1;
		while(i >= 0 && s.charAt(i) != '.')
			i--;
		if (i>0)
			return s.substring(i+1, s.length());
		return s;
	}
	public static String extractNodeName(String s) {
		if (s==null)
			return "";
		int i = s.length() - 1;
		while(i >= 0 && s.charAt(i) != '.')
			i--;
		if (i<0)
			return "";
		int j = i;
		// now j points to . in n_e.nr
		while(i >= 0 && s.charAt(i) != '_')
			i--;
		if (i>=0)
			// now i points to _ in n_e.nr
			return s.substring(0, i);
		// no _e part
		return s.substring(0, j);
	}
	public static String extractEdgeName(String s) {
		if (s==null)
			return "";
		int i = s.length() - 1;
		while(i >= 0 && s.charAt(i) != '.')
			i--;
		if (i<0)
			return "";
		int j = i;
		// now j points to . in n_e.nr
		while(i >= 0 && s.charAt(i) != '_')
			i--;
		if (i>=0)
			// now i points to _ in n_e.nr
			return s.substring(i+1, j);
		// no _e part
		return "";
	}

	
	static public void dbgMsgln(String s) {
		if (debug)
			System.out.println("TorX-Explorer dbg: "+s);
	}
	static public void subprocessDbgMsgln(String s) {
		if (showSubProcessDebug)
			System.out.println("TorX-Explorer subdbg: "+s);
	}
	

	// NOTE we assume that the prefix word already has been read!
	private ExpTransition<ExpState> constructTransition(ExpState src, Tokenizer t, boolean isInst) {
		ExpState dst = null;
		LibLabel label = null;
		LibConstraint constraint = null;

		// SPorTAB event TAB visible TAB solved TAB label TAB preds TAB freevars TAB identical
		if (!t.consume(" \t")) {
			System.err.println("fatal: expected tab or space before ev");
		    return null;
		}
		String ev = t.readUntil("\t");
		if (!t.consume("\t")) {
			System.err.println("fatal: expected tab before vis");
		    return null;
		}
		String vis = t.readUntil("\t");
		if (!t.consume("\t")) {
			System.err.println("fatal: expected tab before svd");
		    return null;
		}
		String svd = t.readUntil("\t");
		if (!t.consume("\t")) {
			System.err.println("fatal: expected tab before lbl");
		    return null;
		}
		String lbl = t.readUntil("\t");
		if (!t.eol() && !t.consume("\t")) {
			System.err.println("fatal: expected tab before preds");
		    return null;
		}
		String preds = t.readUntil("\t");
		if (!t.eol() && !t.consume("\t")) {
			System.err.println("fatal: expected tab before vars");
		    return null;
		}
		String vars = t.readUntil("\t");
		if (!t.eol() && !t.consume("\t")) {
			System.err.println("fatal: expected tab before idnt");
		    return null;
		}
		String idnt = t.readUntil("\t");
		
		// done parsing the fields
		TorxExplorer.dbgMsgln("ev=("+ev+") vis=("+vis+") svd=("+svd+") lbl=("+lbl+") preds=("+preds+") vars=("+vars+") idnt=("+idnt+")");

		String ev_nr = extractEventNr(ev);
		String ev_nn = extractNodeName(ev);
		String ev_en = extractEdgeName(ev);
		String idnt_nr = extractEventNr(idnt);
		String idnt_nn = extractNodeName(idnt);
		String idnt_en = extractEdgeName(idnt);
		TorxExplorer.dbgMsgln("ev_nr=("+ev_nr+") ev_nn=("+ev_nn+") ev_en=("+ev_en+") idnt_nr=("+idnt_nr+") idnt_nn=("+idnt_nn+") idnt_en=("+idnt_en+")");	

		
		ArrayList<Variable> variables = new ArrayList<Variable>();
		if (!vars.equals("")) {
			String[] vars_fields = vars.split(" ");    			
			int j = 0;
			String name = null;
			String orig = null;
			String type = null;
			while(j < vars_fields.length){
				name = vars_fields[j]; j++;
				if (j< vars_fields.length) {
					orig = vars_fields[j]; j++;
				} else
					orig = null;
				if (j< vars_fields.length) {
					type = vars_fields[j]; j++;
				} else
					type = null;
				variables.add(new LibVariable(name,type, orig));
			}
		}
		
		// we must be smarter about identical states/locations.
		// smile returns a nr for an identical _location_,
		// though it may represent a different _state_,
		// as indicated in the predicates that are also returned.
		// for now, we treat each state that results from instantiating
		// as a fresh one (to avoid erroneously treating different states as the same),
		// though we should be able to be smarter.
		// take the predicates also into account?
		// what if predicates are returned, but list of vars is empty?
		if (preds!=null && !preds.trim().equals("") && variables.size() > 0)
			constraint = new LibConstraint(preds);
		
		if (idnt != null && !idnt.equals("") && (ev_nr==null || !isInst)) {
			dst = getFactory().mkState(idnt_nr, ev_nn);
		} else {
			dst = getFactory().mkState(ev_nr, ev_nn);
		}
		
		
		

		label = null;
		if (vis != null) {
			if (vis.trim().equals("1"))
				label = new LibLabel(lbl, true, variables, constraint);
			else if (vis.trim().equals("0"))
				label = new LibLabel(lbl, false, variables, constraint);
			else
				err.report("torx-explorer: unknown visibility for label \""+lbl+"\" : \""+vis+"\"");
		} else 
			err.report("torx-explorer: visibility not given for label \""+lbl+"\"");
		if (label==null)
			label = new LibLabel(lbl, null, variables, constraint);

		TorxExplorer.dbgMsgln("found transition: "+label.getString()+ (constraint!=null?"["+preds+"]":"")+" -> "+dst.getID());
		return new ExpTransition<ExpState>(src, label, dst, ev_nr, ev_en);

	}
	
	
	private ExpFactory factory;
	
	private boolean running = false;

	private String name;

	private OutputStream out;
	private BufferedReader in;

	private Instantiator inst;
	
	static private Boolean showSubProcessDebug = false;
	static private Boolean debug = false;
	private Boolean cleanedUp = false;
	
	private ChildStarter cs;
	private ErrorReporter err;

	private static final boolean CHILDSTARTER_RETRY = true;
	private static final int CHILDSTARTER_MAX_RETRIES = 20;
	private static final long CHILDSTARTER_RETRY_DELAY = 1000L;  
}
