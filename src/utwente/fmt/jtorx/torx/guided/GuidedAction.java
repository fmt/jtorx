package utwente.fmt.jtorx.torx.guided;

import utwente.fmt.lts.Action;

public class GuidedAction implements Action {

	public String getString() {
		return action;
	}
	
	public GuidedAction(String s) {
		action =  s; 
	}

	private String action;
}
