package utwente.fmt.jtorx.torx.guided;

import utwente.fmt.interpretation.IOCOInterpretation;
import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.lts.Action;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;


public class GuidedIOCOInterpretation implements IOCOInterpretation {
	
	public class QuiescenceConstraint implements LabelConstraint {
		public Boolean isSatisfiedBy(Label l) {
			Action a = l.getAction();
			return (a.getString().equalsIgnoreCase("delta") );
		}
	}
	public class EpsilonConstraint implements LabelConstraint {
		public Boolean isSatisfiedBy(Label l) {
			Action a = l.getAction();
			return (a.getString().indexOf("epsilon") >= 0);
		}
	}

	public LabelConstraint isQuiescence() {
		return isQuiescence;
	}
	public LabelConstraint isEpsilon() {
		return isEpsilon;
	}
	public LabelConstraint isInput() {
		return interp.isInput();
	}
	public LabelConstraint isOutput() {
		return interp.isOutput();
	}
	public LabelConstraint isInternal() {
		return interp.isInternal();
	}
	public LabelConstraint isAny() {
		return interp.isAny();
	}
	public GuidedIOCOInterpretation(IOLTSInterpretation i) {
		isQuiescence = new QuiescenceConstraint();
		isEpsilon = new EpsilonConstraint();
		interp = i;
	}
	private LabelConstraint isQuiescence;
	private LabelConstraint isEpsilon;
	private IOLTSInterpretation interp;

}
