package utwente.fmt.jtorx.torx.guided;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.torx.driver.VerdictImpl;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.test.Verdict;

public class GuidedLTS implements CompoundLTS {
	
	// this works as a kind of multiplexer
	// to one (trivial case) or more Model
	// (or GuidedModel ?).
	// the inputs menu is the intersection of
	// inputs menus of the sub-models;
	// the outputs menu is the union of the
	// outputs menu of the sub-models.
	// next returns a successful next state only
	// when each sub-model next does --
	// otherwise, it goes to a next state where a
	// verdict is available;
	// if none of the sub-models reach a successful
	// next state, also GuidedModel need not reach
	// a next state (whether successful or not).
	// So, we only need a verdict when models disagree
	// on whether there is a successful next state.
	// the State that we deal with here consists
	// of the states of the submodels.
	
	// now that we have written code, we see that it actually
	// implements the LTS interface: we can rip out the
	// model related stuff, and only implement the LTS interface,
	// and just use the model implementation on top of that.
	
	// for LTS interface
	public Iterator<? extends CompoundState> init() throws LTSException {
		Vector<CompoundState> specStates = new Vector<CompoundState>();
		Vector<CompoundState> guideStates = new Vector<CompoundState>();
		int j = 0;
		j = doAdd(j, specs, specStates, "model");
		j = doAdd(j, guides, guideStates, "guide");
		initState = factory.mkState(this, specStates, guideStates);
		init.add(initState);

		return init.iterator();
	}
	
	public void done() {
		for(LTS m: specs)
			m.done();
		for(LTS m: guides)
			m.done();
	}

	public GuidedIOCOInterpretation getInterpretation() {
		return interp;
	}
	
	public Vector<CompoundLTS> getSpecs() {
		return specs;
	}
	public Vector<CompoundLTS> getGuides() {
		return guides;
	}
	
	public Verdict getNegativeDefaultVerdict() {
		return new VerdictImpl("fail,miss");
	}
	public Verdict getPositiveDefaultVerdict() {
		return new VerdictImpl("pass,miss");
	}
	public GuidedStateFactory getFactory() {
		return factory;
	}
	
	public int nSpecs() {
		return specs.size();
	}
	public int nGuides() {
		return guides.size();
	}
	public int lookupLTSindex(LTS l) {
		if (ltsIndexMap.containsKey(l))
			return ltsIndexMap.get(l);
		else
			return -1;
	}

	public long getNrOfStates() {
		return -1;
	}
	
	public long getNrOfTransitions() {
		return -1;
	}

	public boolean hasPosition() {
		return false;
	}
	
	public Position getRootPosition() {
		return null;
	}
	
	public void cleanupPreserving(Collection<?extends State> s) {
		// should clean up level in GuidedStateFactory???
	}

	public LTS getSubLTS() {
		return null; // should throw error? (this should never be used)
	}
	
	public CompoundLTS getSubLTS(LTS l) {
		int i = lookupLTSindex(l);
		if (i < 0)
			return null;
		if (i < specs.size())
			return specs.elementAt(i);
		if (i >= specs.size() && i < specs.size()+guides.size())
			return guides.elementAt(i-specs.size());
		return null;
	}

	public boolean noCache() {
		return !enableCache;
	}

	// TODO make the epsilon label used in GuidedModelState
	// a parameter of the constructor,
	// so that it can be configured
	public GuidedLTS(Set<CompoundLTS> s, Set<CompoundLTS> g, IOLTSInterpretation i, boolean enableCache, ErrorReporter r) {
		specs = new Vector<CompoundLTS>(s);
		guides = new Vector<CompoundLTS>(g);
		init = new Vector<CompoundState>();
		factory = new GuidedStateFactory();
		interp = new GuidedIOCOInterpretation(i);
		this.enableCache = enableCache;
		err = r;
	}
	
	private int doAdd(int j, Vector<CompoundLTS> l, Vector<CompoundState> v, String kind) throws LTSException {
		for(CompoundLTS m: l) {
			ltsIndexMap.put(m, j++);
			Iterator<? extends CompoundState> sit = m.init();
			if (sit.hasNext())
				v.add(sit.next());
			else {
				System.err.println("GuidedLTS warning: null initial state for "+kind+" LTS");
				v.add(null);
			}
			if (sit.hasNext())
				System.err.println("GuidedLTS internal error: assumption failure: assumed single initial state for "+kind+" LTS");
		}
		return j;
	}
	
	protected boolean debug = false;
	protected ErrorReporter err = null;
	
	private Vector<CompoundLTS> specs;
	private Vector<CompoundLTS> guides;
	private GuidedState initState;
	private GuidedIOCOInterpretation interp;
	private GuidedStateFactory factory;
	private Map<LTS,Integer> ltsIndexMap = new HashMap<LTS,Integer>();
	private boolean enableCache;
	
	private Vector<CompoundState> init;
}
