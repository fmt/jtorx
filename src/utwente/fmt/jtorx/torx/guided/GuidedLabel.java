package utwente.fmt.jtorx.torx.guided;

import java.util.ArrayList;
import java.util.Iterator;

import utwente.fmt.jtorx.utils.EmptyIterator;
import utwente.fmt.lts.Action;
import utwente.fmt.lts.Constraint;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Variable;

public class GuidedLabel implements Label {

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (label==null ? 0 : label.hashCode());
		return hash;
	}

	@Override
	public boolean equals(Object o) {
		//System.out.println("autlabel-obj");
		if (o instanceof Label) {
	         Label l = (Label) o;
	         return (l.getString().equals(this.getString()));
		}
		return false;
	}
	public Boolean eq(Label l) {
		//System.out.println("autlabel-lbl");
		return (l.getString().equals(this.getString()));
	}
	
	public Iterator<Variable> getVariables() {
		if (variables==null)
			return EmptyIterator.iterator();
		return variables.iterator();
	}
	
	public Constraint getConstraint() {
		return null;
	}



	public Action getAction() {
		return a;
	}

	public String getString() {
		return label;
	}

	public Boolean isObservable() {
		return true;
	}
	
	public GuidedLabel(String s) {
		label = s;
		a = new GuidedAction(s.split("[^a-zA-Z0-9_]+")[0]);
	}

	private String label;
	private ArrayList<Variable> variables = null;

	private Action a;
}
