package utwente.fmt.jtorx.torx.guided;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import utwente.fmt.jtorx.torx.InstantiationImpl;
import utwente.fmt.jtorx.torx.primer.PrimerState;
import utwente.fmt.jtorx.torx.primer.PrimerTransition;
import utwente.fmt.jtorx.utils.EmptyIterator;
import utwente.fmt.jtorx.utils.term.TermParser;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Term;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.UnificationResult;


public class GuidedState implements CompoundState {

	public String getFullID() {
		String s = "";
		s += "id("+nr+")";
		GuidedState ps = theLTS.getFactory().get(this.states);
		if (ps != null) {
			s += "[";
			s += ps.nr;
			s += "]";
		}
		s += " lts states: ";
		s += "spec(";
		for(int j=0; j<theLTS.nSpecs(); j++ ) {
			State st = states[j];
			if (st != null)
				s += st.getID();
			else
				s += "(null)";
			s += " ";
		}
		s += ") guide(";
		for(int j=0, k=theLTS.nSpecs(); j<theLTS.nGuides(); j++,k++) {
			State st = states[k];
			if (st != null)
				s += st.getID();
			else
				s += "(null)";
			s += " ";
		}
		s += ")";
		return s;
	}
	
	public GuidedState getCanonical() {
		return this;
	}

	private class SubStateExpander {

		// assumption: models and guides have been determinized already,
		// so each contributes at most a single instance of each
		// different label
		private void expandSubState(CompoundState s,
				Map<Label,Map<CompoundLTS,CompoundState>> inputDsts,
				Map<Label,Map<CompoundLTS,CompoundState>> outputDsts) {
			CompoundTransition<?extends CompoundState> t;
			Label l;
			Map<CompoundLTS,CompoundState> ltsDst;
			Map<CompoundLTS,CompoundTransition<?extends CompoundState>> ltsTrans;
			Iterator<?extends CompoundTransition<?extends CompoundState>> tit;

			if (s == null) {
				System.err.println("GuidedState expandSubstate s = null");
				return;
			}

			tit = s.menu(theLTS.getInterpretation().isAny());
			while (tit.hasNext()) {
				t = tit.next();
				l = t.getLabel();
				if(theLTS.debug) System.out.println("GuidedState expand: working on: "+l+" "+l.getString());
				ltsTrans = getOrCreate(transMap, l);
				ltsTrans.put(s.getLTS(), t);
				if (theLTS.getInterpretation().isInput().isSatisfiedBy(l)) {
					ltsDst = getOrCreate(inputDsts, l);
					ltsDst.put(s.getLTS(), t.getDestination());
					if(theLTS.debug) System.out.println("GuidedState expand: add to input map "+ltsDst+": "+l+" "+l.getString()+" "+t.getDestination());
				} else if (theLTS.getInterpretation().isOutput().isSatisfiedBy(l) ||
						theLTS.getInterpretation().isQuiescence().isSatisfiedBy(l) ||
						theLTS.getInterpretation().isEpsilon().isSatisfiedBy(l)) {

					ltsDst = getOrCreate(outputDsts, l);
					ltsDst.put(s.getLTS(), t.getDestination());
					if(theLTS.debug) System.out.println("GuidedState expand: add to output map "+ltsDst+": "+l+" "+l.getString()+" "+t.getDestination());
				} else {
					if(theLTS.debug) System.err.println("GuidedState: configuration error: no io kind category for label: "+l.getString());
				}
			}
		}
		
		public Iterable<Label> getInputLabels() {
			return inputDstMap.keySet();
		}
		public Iterable<Label> getOutputLabels() {
			return outputLabels;
		}
		public Map<CompoundLTS,CompoundState> getInputDstMap(Label l) {
			return inputDstMap.get(l);
		}
		public Map<CompoundLTS,CompoundTransition<?extends CompoundState>> getTransMap(Label l) {
			return transMap.get(l);
		}
		public Map<CompoundLTS,CompoundState> getOrCreateOutputGuideDstMap(Label l) {
			return getOrCreate(outputGuideDstMap, l);
		}
		public Map<CompoundLTS,CompoundState> getOrCreateOutputSpecDstMap(Label l) {
			return getOrCreate(outputSpecDstMap, l);
		}
		public boolean outputGuideDstMapContainsEpsilon() {
			for(Label ll: outputGuideDstMap.keySet()) {
				if (theLTS.getInterpretation().isEpsilon().isSatisfiedBy(ll))
					return true;
			}
			return false;
		}
		
		public SubStateExpander () {
			if(theLTS.debug) System.out.println("--GuidedState start of expand work specs");
			for(int j=0; j<theLTS.nSpecs(); j++) {
				if(theLTS.debug) System.out.println("---GuidedState start of expand work spec "+j);
				expandSubState(states[j], inputDstMap, outputSpecDstMap);
			}
			if(theLTS.debug) System.out.println("--GuidedState start of expand work guides");
			for(int j=0,k=theLTS.nSpecs(); j<theLTS.nGuides(); j++,k++) {
				if(theLTS.debug) System.out.println("---GuidedState start of expand work guide "+j);
				expandSubState(states[k], inputDstMap, outputGuideDstMap);
			}
			if(theLTS.debug) System.out.println("--GuidedState end of expand work");
			outputLabels = new HashSet<Label>(outputSpecDstMap.keySet());
			outputLabels.addAll(outputGuideDstMap.keySet());
			for(Label l: outputLabels) {
				if(theLTS.debug) System.out.println("GuidedState outputLabels label: "+l.getString()+" "+l);
			}
			if(theLTS.debug) System.out.println("GuidedState end of outputLabels");
		}

		private Map<Label,Map<CompoundLTS,CompoundState>> inputDstMap = new HashMap<Label,Map<CompoundLTS,CompoundState>>();
		private Set<Label> outputLabels;
		private Map<Label,Map<CompoundLTS,CompoundTransition<?extends CompoundState>>> transMap = new HashMap<Label,Map<CompoundLTS,CompoundTransition<?extends CompoundState>>>();
		private Map<Label,Map<CompoundLTS,CompoundState>> outputSpecDstMap = new HashMap<Label,Map<CompoundLTS,CompoundState>>();
		private Map<Label,Map<CompoundLTS,CompoundState>> outputGuideDstMap = new HashMap<Label,Map<CompoundLTS,CompoundState>>();
	}

	

	private void expand() {
		if(theLTS.debug) System.out.println("GuidedState start of expand; isExpanded: "+isExpanded);
		
		if (isExpanded && theLTS.noCache()) {
			trans = new Vector<CompoundTransition<? extends CompoundState>>();
			isExpanded = false;
		}
		
		if (isExpanded)
			return;
		
		isExpanded = true;

		// first we add all info about menus of sub states

		SubStateExpander expandedSubLTSes = new SubStateExpander();
	
		// now create the resulting menu.
		int n = theLTS.nSpecs() + theLTS.nGuides();
		Map<CompoundLTS,CompoundState> dstMap;
		Map<CompoundLTS,CompoundTransition<?extends CompoundState>> transMap;
		
		// for inputs, use the intersection.
		// because underlying models are deterministic, we thus should take
		// those inputs for which the map has number-of-underlying-models elements
		for(Label l: expandedSubLTSes.getInputLabels()) {
			if(theLTS.debug) System.out.println("GuidedState expand: start of inputMap loop "+l.getString());
			dstMap = expandedSubLTSes.getInputDstMap(l);
			if (dstMap.size() == n){
				if(theLTS.debug) System.out.println("GuidedState expand: adding inputMap item "+l.getString()+" n="+n);
				GuidedState dst = theLTS.getFactory().mkState(getLTS(), getSpecItems(dstMap), getGuideItems(dstMap));
				transMap = expandedSubLTSes.getTransMap(l);
				CompoundTransition<GuidedState> t =
					new GuidedTransition<GuidedState>(this, l, dst, getSpecItems(transMap), getGuideItems(transMap));
				trans.add(t);
			} else {
			    if(theLTS.debug) {
			        theLTS.err.log(System.currentTimeMillis(), "GuidedState", "expand: skipping inputMap item "+l.getString()+" n="+n+" mapsz="+dstMap.size());
			    }
			}
			if(theLTS.debug) System.out.println("GuidedState expand: end of inputMap loop "+l.getString());
		}
		// for outputs, use the union for the menu.
		// all outputs not in the menu will yield default verdict <fail,miss>
		//    except guides that have epsilon in menu
		// all outputs in the intersection will yield <pass,miss> if they need to yield a verdict
		// all outputs with specs in the intersection and guides epsilon yield <pass,hit>
		// all outputs with specs not in the intersection and guides epsilon yield <fail,hit>
		// all outputs with specs not in the intersection and guides not epsilon yield <fail,miss>
		// because underlying models are deterministic, we thus should take
		// those inputs for which the map has number-of-underlying-models elements

		Map<CompoundLTS,CompoundState> ltsSpecMap, ltsGuideMap;
		for(Label l: expandedSubLTSes.getOutputLabels()) {
			ltsGuideMap = expandedSubLTSes.getOrCreateOutputGuideDstMap(l);
			if(theLTS.debug) System.out.println("GuidedState expand: start of outputMap loop "+l.getString()+ " guideMap="+ltsGuideMap);
			Vector<CompoundState> guideDests = getGuideItems(ltsGuideMap);
			/*
			boolean isHit = false;
			for(CompoundState g: guideDests) {
				if (!isHit && g != null) {
					Iterator<? extends CompoundTransition<? extends CompoundState>> t =
						g.menu(theLTS.getInterpretation().isEpsilon());
					isHit |= t.hasNext();
				}
			}
			*/
			
			/* Since we need to loao 
			 */
			boolean isHit = false;
			Map<CompoundLTS, CompoundTransition<? extends CompoundState>> transmap = expandedSubLTSes.getTransMap(l);
			for(CompoundLTS lts : transmap.keySet()) {
				CompoundTransition<? extends CompoundState> trans = transmap.get(lts);
				if(trans instanceof PrimerTransition) {
					PrimerTransition pt = (PrimerTransition)trans;
					Label instantiatedLabel = pt.getSolution();
					if(instantiatedLabel != null) {
						Term termL = TermParser.parse(l);
						Term termI = TermParser.parse(instantiatedLabel);
						UnificationResult uniRes = termL.unify(termI);
						if(uniRes.isUnifyable()) {
							PrimerTransition instantiatedTransition = null;
							ArrayList<UnificationBinding> bind = uniRes.getBindings();
							if(bind == null || bind.isEmpty()) {
								/* Nothing to instantiate. */
								instantiatedTransition = pt;
							}
							else {
								Instantiation inst = new InstantiationImpl(instantiatedLabel, termI, bind);
								instantiatedTransition = pt.instantiate(inst);
							}
							
							if(instantiatedTransition != null) {
								/* instantiation successful */
								PrimerState target = (PrimerState)instantiatedTransition.getDestination();
								if(target.menu(theLTS.getInterpretation().isEpsilon()).hasNext()) {
									isHit = true;
								}
							}
						}
					}
				}
			}
			
			
			ltsSpecMap = expandedSubLTSes.getOrCreateOutputSpecDstMap(l);
			Vector<CompoundState> specDests = getSpecItems(ltsSpecMap);
			GuidedState dst = null;
			if (isHit) {
				if (ltsSpecMap.size() == theLTS.nSpecs()) {
					// this was the defPosVerdict case; should we return something special?
					GuidedVerdict v = new GuidedVerdict(GuidedVerdict.Correctness.PASS, GuidedVerdict.ObservationObjective.HIT);
					dst = theLTS.getFactory().mkState(getLTS(), specDests, guideDests, v);
				} else if (ltsSpecMap.size() < theLTS.nSpecs()) {
					GuidedVerdict v = new GuidedVerdict(GuidedVerdict.Correctness.FAIL, GuidedVerdict.ObservationObjective.HIT);
					dst = theLTS.getFactory().mkState(getLTS(), specDests, guideDests, v);
				} else {
					// we should not be able reach this: if we are here:
					// ltsSpecMap.size() > specStates.size()
					System.out.println("GuidedState expand: this should be unreachable: have epsilonLabel, GuidedState dst is null");
				}
			} else {
				if (ltsSpecMap.size() == theLTS.nSpecs() && ltsGuideMap.size() > 0) {
					dst = theLTS.getFactory().mkState(getLTS(), specDests, guideDests);
				} else if (ltsSpecMap.size() == theLTS.nSpecs() && ltsGuideMap.size() == 0) {
					GuidedVerdict v = new GuidedVerdict(GuidedVerdict.Correctness.PASS, GuidedVerdict.ObservationObjective.MISS);
					dst = theLTS.getFactory().mkState(getLTS(), specDests, guideDests, v);	
				} else if (ltsSpecMap.size() < theLTS.nSpecs()) {
					// this is the defNegVerdict case; should we return destination null in this case?
					GuidedVerdict v = new GuidedVerdict(GuidedVerdict.Correctness.FAIL, GuidedVerdict.ObservationObjective.MISS);
					dst = theLTS.getFactory().mkState(getLTS(), specDests, guideDests, v);	
				} else {
					// we should not be able to reach this: if we are here:
					// either: ltsSpecMap.size() > specStates.size()
					// or: ltsSpecMap.size() == specStates.size() && ltsGuideMap.size() < 0
					System.out.println("GuidedState expand: this should be unreachable: have normal Label, GuidedState dst is null");
				}
			}
			if (dst!=null && dst.getVerdict()!=null)
				if(theLTS.debug) System.out.println("GuidedState expand: verdict for "+l.getString()+" : "+dst.getVerdict().getString());
			if (! theLTS.getInterpretation().isEpsilon().isSatisfiedBy(l)) {
			// if (! l.eq(epsilonLabel)) {
				if(theLTS.debug) System.out.println("GuidedState expand: constructing transition for "+l.getString());
				transMap = expandedSubLTSes.getTransMap(l);
				CompoundTransition<GuidedState> t =
					new GuidedTransition<GuidedState>(this, l, dst, getSpecItems(transMap), getGuideItems(transMap));
				if(theLTS.debug) System.out.println("GuidedState expand: constructed transition for "+l.getString());
				trans.add(t);
			} else
				if(theLTS.debug) System.out.println("GuidedState expand: NOT constructing transition for "+l.getString());
			if(theLTS.debug) System.out.println("GuidedState expand: end of outputMap loop "+l.getString());
		}
		if (verdict == null && expandedSubLTSes.outputGuideDstMapContainsEpsilon()) {
			if(theLTS.debug) System.out.println("GuidedState late find of verdict due to epsilon label");
			verdict = new GuidedVerdict(GuidedVerdict.Correctness.PASS, GuidedVerdict.ObservationObjective.HIT);
		}
		if (verdict != null) {
			if(theLTS.debug) System.out.println("GuidedState start add verdict transition");
			CompoundTransition<GuidedState> t = new GuidedTransition<GuidedState>(this, new GuidedLabel("verdict("+verdict.getString()+")"), this);
			trans.add(t);
			if(theLTS.debug) System.out.println("GuidedState end add verdict transition");
		}
		if(theLTS.debug) System.out.println("GuidedState end of expand");
	}
	
	public GuidedVerdict getVerdict() {
		return verdict;
	}

	public String getID() {
		return ""+nr;
	}
	
	public String getLabel(boolean quote) {
		if (label==null) {
			String s = "";
			int j=0;
			s += "< ";
			if (j<theLTS.nSpecs()) 
				s += getSubStateLabel(states[j++]);
			while(j<theLTS.nSpecs())
				s += ", " + getSubStateLabel(states[j++]);
			s += " | ";
			int n = j;
			j = 0;
			if (j<theLTS.nGuides())
				s += getSubStateLabel(states[n+j++]);
			while(j<theLTS.nGuides())
				s += "," + getSubStateLabel(states[n+j++]);
			s += " >";
			label = s;
		}
		return label;
	}
	private String getSubStateLabel(State p) {
		if (p!=null)
			return p.getLabel(true);
		else
			return "_";
	}
	
	public Position getPosition() {
		return null;
	}
	
	public String getNodeName() {
		if (name==null) {
			String s = "";
			int j=0;
			s += "< ";
			if (j<theLTS.nSpecs()) 
				s += getSubStateNodeName(states[j++]);
			while(j<theLTS.nSpecs())
				s += ", " + getSubStateNodeName(states[j++]);
			s += " | ";
			int n = j;
			j = 0;
			if (j<theLTS.nGuides())
				s += getSubStateNodeName(states[n+j++]);
			while(j<theLTS.nGuides())
				s += "," + getSubStateNodeName(states[n+j++]);
			s += " >";
			name = s;
		}
		return name;

	}
	private String getSubStateNodeName(State p) {
		if (p!=null)
			return p.getNodeName();
		else
			return "_";
	}


	public GuidedLTS getLTS() {
		return theLTS;
	}

	public Iterator<? extends CompoundTransition<? extends CompoundState>> menu(LabelConstraint l) {
		expand();
		return new GuidedTransitionIterator(trans, l);
	}
	// TODO take care of symbolic stuff?
	// TODO what about patterns (wildcards) in guide labels?
	public CompoundTransition<? extends CompoundState> next(Label l) {
		if (trans==null || trans.size()==0)
			return null;
			
		for (CompoundTransition<? extends CompoundState> t: trans) {
			Label uninstL = t.getLabel();
			Term termL = TermParser.parse(l);
			Term termUninstL = TermParser.parse(uninstL);
			UnificationResult unificationResult = termUninstL.unify(termL);
			
			if(unificationResult.isUnifyable()) {
				GuidedVerdict gv = (GuidedVerdict)t.getDestination().getVerdict();
				boolean miss = (gv != null && gv.isMiss());
			
				Vector<CompoundTransition<? extends CompoundState>> specTrans = new Vector<CompoundTransition<? extends CompoundState>>();
				Vector<CompoundTransition<? extends CompoundState>> guideTrans = new Vector<CompoundTransition<? extends CompoundState>>();
				Vector<CompoundState> specDests = new Vector<CompoundState>();
				Vector<CompoundState> guideDests = new Vector<CompoundState>();
			
				for(int i = 0; i < getLTS().nSpecs(); i++) {
					CompoundTransition<? extends CompoundState> instTrans = this.states[i].next(l);
					if(instTrans == null) {
						continue;
					}
					specTrans.add(instTrans);
					specDests.add(instTrans.getDestination());
				}
				if(!miss) {
					for(int i = 0, off = getLTS().nSpecs(); i < getLTS().nGuides(); i++) {
						CompoundTransition<? extends CompoundState> instTrans = this.states[i+off].next(l);
						guideTrans.add(instTrans);
						if(instTrans == null) {
							miss = true;
						}
						else {
							guideDests.add(instTrans.getDestination());
						}
					}
				}
			
				GuidedState instDest = getLTS().getFactory().mkState(getLTS(), specDests, guideDests, gv);
				return new GuidedTransition<GuidedState>(this, l, instDest, specTrans, guideTrans);
			}
			
			/*if (t.getLabel().eq(l))
				return t;*/	
		}
		return null;
	}

	
	public Iterator<? extends State> getSubStates() {
		return EmptyIterator.iterator();
	}
	public Iterator<? extends State> getSubStates(LTS lts) {
		CompoundState s = getCompoundState(lts);
		if (s!=null)
			return s.getSubStates();
		if(theLTS.debug) System.out.println("guidedstate-getsubstates-did-not-match-lts "+lts);
		return getSubStates();
	}


	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return EmptyIterator.iterator();
	}
	public Iterator<? extends Transition<? extends State>> getSubTransitions(LTS lts) {
		CompoundState s = getCompoundState(lts);
		if (s!=null)
			return s.getSubTransitions();
		if(theLTS.debug) System.out.println("guidedstate-getsubtrans-did-not-match-lts "+lts);
		return getSubTransitions();
	}
	
	public String getSubEdgeName() {
		return null;
	}
	public String getSubEdgeName(LTS lts) {
		CompoundState s = getCompoundState(lts);
		if (s!=null)
			return s.getSubEdgeName();
		if(theLTS.debug) System.out.println("guidedstate-getsubedgename-did-not-match-lts "+lts);
		return getSubEdgeName();

	}

	public CompoundState getCompoundState(LTS lts) {
		int i = theLTS.lookupLTSindex(lts);
		if (i >= 0)
			return states[i];
		if(theLTS.debug) System.out.println("guidedstate-getCompoundState-did-not-match-lts "+lts);
		return null;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
//		if((o == null) || (o.getClass() != this.getClass()))
//			return false;
		if (o instanceof GuidedState) {
			GuidedState s = (GuidedState) o;
         return (s.theLTS.equals(theLTS) &&
        		 (s.states.equals(states) ));
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == states ? 0 : states.hashCode());
		return hash;
	}
	
	public void disposeTransitions() {
	}
	
	public GuidedState(GuidedLTS m, Vector<CompoundState> s, Vector<CompoundState> g, GuidedVerdict v, int nr) {
		if(m.debug) System.out.println("constructing GuidedState "+nr);
		int n = m.nSpecs() + m.nGuides();
		states = new CompoundState[n];
		int j=0;
		for(CompoundState k: s)
			states[j++] = k;
		for(CompoundState k: g)
			states[j++] = k;
		theLTS = m;
		verdict = v;
		this.nr = nr;
	}
	
	private <K,P,Q>  Map<P,Q> getOrCreate(Map<K, Map<P,Q>> m, K k) {
		Map<P, Q> v = m.get(k);
		if (v == null) {
			v = new HashMap<P,Q>();
			m.put(k, v);
		}
		return v;
	}
	
	private <T> Vector<T> getSpecItems(Map<CompoundLTS,T> m) {
		Vector<T> r = new Vector<T>();
		for(CompoundLTS lts : theLTS.getSpecs())
			r.add(m.get(lts));
		return r;
	}
	private <T> Vector<T> getGuideItems(Map<CompoundLTS,T> m) {
		Vector<T> r = new Vector<T>();
		for(CompoundLTS lts : theLTS.getGuides())
			r.add(m.get(lts));
		return r;
	}
	
	
	private GuidedLTS theLTS;
	private CompoundState states[] = new CompoundState[0];
	private GuidedVerdict verdict;
	
	private Vector<CompoundTransition<? extends CompoundState>> trans = new Vector<CompoundTransition<? extends CompoundState>>();
	private Boolean isExpanded = false;
	private int nr;
	private String label = null;
	private String name = null;

}
