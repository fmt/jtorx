package utwente.fmt.jtorx.torx.guided;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;


import utwente.fmt.lts.CompoundState;


public class GuidedStateFactory {
	
	//GuidedLTS m, Vector<State> s, Vector<State> g, GuidedVerdict v
	public  GuidedState mkState(GuidedLTS theLTS, Vector<CompoundState> s, Vector<CompoundState> g) {
		return mkState(theLTS, s, g, null);
	}

	public  GuidedState mkState(GuidedLTS theLTS, Vector<CompoundState> s, Vector<CompoundState> g, GuidedVerdict v) {
		Vector<String> ids = new Vector<String>();
		for(CompoundState it:s)
			if (it != null)
				ids.add(it.getID());
			else
				ids.add("null");
		for(CompoundState it:g)
			if (it != null)
				ids.add(it.getID());
			else
				ids.add("null");
		GuidedState ps = stateMap.get(ids);
		if (ps == null) {
			ps = new GuidedState(theLTS, s, g, v, nextnr++);
			stateMap.put(ids, ps);
			if (theLTS.debug) System.out.println("guided factory created state "+(nextnr-1)+" "+ps.getFullID()+" s: "+s.hashCode()+" g: "+g.hashCode());
			debugPrintStateDetails(theLTS.debug, s, g);

		} else {
			if (theLTS.debug) System.out.println("\tguided factory looked up state     "+ps.getFullID()+" s: "+s.hashCode()+" g: "+g.hashCode());
			debugPrintStateDetails(theLTS.debug, s, g);
		}
		return ps;
	}
	
	public GuidedState get(CompoundState[] sg) {
		Set<CompoundState> hs = new HashSet<CompoundState>();
		for(CompoundState it:sg)
			hs.add(it);
		return stateMap.get(hs);
	}	

	
	private void debugPrintStateDetails(boolean debug, Collection<CompoundState> s, Collection<CompoundState> g) {
		for (CompoundState cs:s) {
			if (cs != null) {
				if (debug) System.out.println("\ts: "+cs.hashCode()+" "+cs.getID());
			} else {
				if (debug) System.out.println("\ts: null");
			}
		}
		for (CompoundState cs:g) {
			if (cs != null) {
				if (debug) System.out.println("\tg: "+cs.hashCode()+" "+cs.getID());
			} else {
				if (debug) System.out.println("\tg: null");
			}
		}
	}
	
	private Map<Vector<String>,GuidedState> stateMap = new HashMap<Vector<String>,GuidedState>();
	private int nextnr = 0;

}
