package utwente.fmt.jtorx.torx.guided;

import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.jtorx.utils.EmptyIterator;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;


public class GuidedTransition<T extends GuidedState> implements CompoundTransition<T> {

	public T getDestination() {
		return dst;
	}

	public Label getLabel() {
		return label;
	}

	public T getSource() {
		return src;
	}
	
	public Vector<Position> getPosition() {
		return null;
	}
	public Position getLabelPosition() {
		return null;
	}
	public String getEdgeName() {
		if (name==null) {
			String s = "";
			int j=0;
			s += "< ";
			if (j<src.getLTS().nSpecs()) 
				s += getSubTransitionEdgeName(trans[j++]);
			while(j<src.getLTS().nSpecs())
				s += ", " + getSubTransitionEdgeName(trans[j++]);
			s += " | ";
			int n = j;
			j = 0;
			if (j<src.getLTS().nGuides())
				s += getSubTransitionEdgeName(trans[n+j++]);
			while(j<src.getLTS().nGuides())
				s += "," + getSubTransitionEdgeName(trans[n+j++]);
			s += " >";
			name = s;
		}
		return name;
	}
	private String getSubTransitionEdgeName(Transition<? extends State> p) {
		if (p!=null)
			return p.getEdgeName();
		else
			return "_";
	}
	
	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return EmptyIterator.iterator();
	}

	public Iterator<? extends Transition<? extends State>> getSubTransitions(LTS lts) {
		CompoundTransition<? extends CompoundState> t = getCompoundTransition(lts);
		if (t!=null)
			return t.getSubTransitions();
		if(src.getLTS().debug) System.out.println("guided-transition-getsubstransitions-did-not-match-lts "+lts);
		return getSubTransitions();
	}
	
	public CompoundTransition<? extends CompoundState> getCompoundTransition(LTS lts) {
		int i = src.getLTS().lookupLTSindex(lts);
		if (i >= 0)
			return trans[i];
		if(src.getLTS().debug) System.out.println("guided-transition-getcompoundtransitions-did-not-match-lts "+lts);
		return null;
	}
	
	// for now, we avoid symbolic stuff, and just return the given transition if i.label==t.label
	// otherwise, we return null
	public GuidedTransition<T> instantiate(Instantiation i) {
		if (i==null || i.getLabel()==null)
			return null;
		if (label.equals(i.getLabel()))
			return this;
		else
			return null;
	}
	
	// should look at all sub-lts-es?
	public Label getSolution() {
		/*TODO*/
		// Hack
		CompoundTransition<? extends CompoundState> concreteTrans = trans[0];
		CompoundTransition<? extends CompoundState> guidedTrans = trans[1];
		
		Label guidedLabel = guidedTrans.getSolution();
		if(concreteTrans.getSource().next(guidedLabel) != null) {
			return guidedLabel;
		}
		else {
			return null;
		}		
		// /Hack
		
		//return null;
	}


	// TODO add eq and hashCode methods??
	// TODO take variables and constraints into account in these methods? !!!

	public GuidedTransition(T s, Label l, T d) {
		this(s,l,d,null,null);
	}
	public GuidedTransition(T s, Label l, T d, Vector<?extends CompoundTransition<?extends CompoundState>> t_s, Vector<?extends CompoundTransition<?extends CompoundState>> t_g) {
		if(s.getLTS().debug) System.out.println("constructing GuidedTransition "+s.getID()+" -- "+l.getString()+" --> "+d.getID());
		src = s;
		label  = l;
		dst = d;
		int n = s.getLTS().nSpecs() + s.getLTS().nGuides();
		trans = new CompoundTransition<?>[n];
		
		int j=0;
		if (t_s!=null && t_s.size()==s.getLTS().nSpecs())
			for(CompoundTransition<?extends CompoundState> k: t_s)
				trans[j++] = k;
		else
			for(int i=0; i<s.getLTS().nSpecs(); i++)
				trans[j++] = null;
		if (t_g!=null && t_g.size()==s.getLTS().nGuides())
			for(CompoundTransition<?extends CompoundState> k: t_g)
				trans[j++] = k;
		else
			for(int i=0; i<s.getLTS().nGuides(); i++)
				trans[j++] = null;

	}

	CompoundTransition<?extends CompoundState> trans[];
	private T src;
	private Label label;
	private T dst;
	private String name; // TODO does this attribute break storing these transitions in sets/maps?
}

