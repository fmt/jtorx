package utwente.fmt.jtorx.torx.guided;

import java.util.Vector;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LabelConstraint;

public class GuidedTransitionIterator implements java.util.Iterator<CompoundTransition<? extends CompoundState>> {

	public boolean hasNext() {
		return (idx < transitions.size());
	}

	public CompoundTransition<? extends CompoundState> next() {
		if (idx < transitions.size()) {
			CompoundTransition<? extends CompoundState> result = transitions.elementAt(idx);
			idx++;
			skipNonMatching();

			return result;
		}
		return null;
	}
	
	public GuidedTransitionIterator(Vector<? extends CompoundTransition<? extends CompoundState>> t, LabelConstraint lc) {
		transitions = t;
		constraint = lc;
		idx = 0;
		skipNonMatching();
	}
	
	private void skipNonMatching() {
		if (constraint != null && transitions != null)
			while(idx < transitions.size() && !constraint.isSatisfiedBy(transitions.elementAt(idx).getLabel()))
				idx++;
	}
	
	private Vector<? extends CompoundTransition<? extends CompoundState>> transitions;
	private LabelConstraint constraint;
	private int idx;
	public void remove() {
		throw new UnsupportedOperationException("GuidedTransitionIterator.remove unimplemented");
	}
}
