package utwente.fmt.jtorx.torx.guided;

import utwente.fmt.test.Verdict;

public class GuidedVerdict implements Verdict {

	enum Correctness { PASS, FAIL };
	enum ObservationObjective { HIT, MISS };
	
	public String getString() {
		String c = correctnessToString();
		String o = ObjectiveToString();
		if (c != null && o != null)
			return c+","+o;
		else if (c != null)
			return c;
		else if (o != null)
			return o;
		else
			return "";
	}
	
	public Boolean isMiss() {
		return (obsobj == ObservationObjective.MISS); 
	}
	
	public Boolean isFail() {
		return (corr == Correctness.FAIL);
	}
	public Boolean isError() {
		return (getString().contains("error"));
	}

	public GuidedVerdict (Correctness c, ObservationObjective o) {
		corr = c;
		obsobj = o;
	}
	
	private String correctnessToString() {
		if (corr == Correctness.PASS)
			return "pass";
		else if (corr == Correctness.FAIL)
			return "fail";
		else
			return null;
	}
	private String ObjectiveToString() {
		if (obsobj == ObservationObjective.HIT)
			return "hit";
		else if (obsobj == ObservationObjective.MISS)
			return "miss";
		else
			return null;
	}

	
	private Correctness corr;
	private ObservationObjective obsobj;
}
