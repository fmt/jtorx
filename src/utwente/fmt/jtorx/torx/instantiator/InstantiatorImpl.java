package utwente.fmt.jtorx.torx.instantiator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;

import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.torx.Instantiator;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.utils.ChildStarter;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class InstantiatorImpl implements Instantiator {

	public Label getInstantiation(Transition<? extends State> t) {

		if (t==null) {
			System.err.println("InstantiatorImpl: cannot instantiate null transition");
			return null;
		}
		
		Label l = t.getLabel();
		if (l==null) {
			System.err.println("InstantiatorImpl: cannot instantiate null label");
			return null;
		}
		
		Label result = null;
		String pfx = "C_EVENT";
		try {
			String cmd = pfx+" "+"event="+l.getString()+"\t"+"predicates="+l.getConstraint().getString()+"\n";
			sendRequest(cmd);
			String s = readUntil(pfx, eventAnswers);
			if (s==null) {
				return null;
			}
			System.err.println("reply:"+s);
			String[] fields = s.split("[\t]");
			System.err.println("fieldcound:"+fields.length);
			for (int i=0; i < fields.length; i++) {
				System.err.println("field["+i+"]:"+fields[i]);
				if (fields[i].startsWith("event=")) {
					String label = fields[i].substring(6);
					result = new LibLabel(label);
				}
			}
			consumePendingLines(pfx);
		} catch (IOException e) {
			System.err.println("Instantiator-impl: "+pfx+" io exception: "+e.getMessage());
			e.printStackTrace();
			errorReporter.report(instErr);
			running = false;
		}

		return result;
	}

	private String readUntil(String cmd, String ans[]) throws IOException {
		String line;
		boolean readAnswer = false;
		while(! readAnswer) {
			line = out.readLine();
			if (line == null) {
				running = false;
				return line;
			}
			System.err.println("");
			System.err.println("readUntil read:"+line);

			for (String s: ans) {
				if (line.startsWith(s+" ") || line.startsWith(s+"\t") || line.startsWith(s+"\n")) {
					// actually, we should extract channel,  pco, concrete, timestamp
					String tmp = line.replaceFirst(s+"[ \t]+", "").trim();
					return tmp;
				}
			}
			if (line.startsWith("A_ERROR")) {
				System.err.println("fatal: instantiator error:"+line);
				return null;
			} else if (line.startsWith("A_DEBUG")) {
				subprocessDbgMsgln(line);
			} else if (line.startsWith("A_LOG")) {
				// skip
			} else if (line.startsWith("A_INPUT_ERROR ")) {
				System.err.println("non-fatal: instantiator error:"+line);
			} else if (line.startsWith("A_OUTPUT_ERROR ")) {
				System.err.println("non-fatal: instantiator error:"+line);
			} else {
				System.err.println("unexpected "+cmd+" response:"+line);
			}
		}
		return null;
	}
	
	private void consumePendingLines(String cmd) throws IOException {
		String line;
		while(out.ready()) {
			line = out.readLine();
			System.err.println("");
			System.err.println("consumePendingLines read:"+line);
			if (line.startsWith("A_ERROR")) {
				System.err.println("fatal: instantiator error:"+line);
				// should not happen
			} else if (line.startsWith("A_DEBUG")) {
				subprocessDbgMsgln(line);
			} else if (line.startsWith("A_LOG")) {
				// skip
			} else {
				System.err.println("unexpected "+cmd+" response:"+line);
			}
		}	
	}

	public void done() {
		dbgMsgln("instantiator-impl: in done: running="+(running?"f":"f")+" cleanedUp="+(cleanedUp?"t":"f"));

		if (! running)
			return;

		if (cleanedUp)
			return;
		cleanedUp = true;
		
		if (in == null || out == null)
			return;
		
		String pfx = "C_QUIT";
		try {
			String cmd = pfx+"\n";
			sendRequest(cmd);
			String s = readUntil(pfx, quitAnswers);
			if (s==null)
				return;
			consumePendingLines(pfx);
			in.close();
			out.close();
		} catch (IOException e) {
			System.err.println("Instantiator-impl: "+pfx+" io exception: "+e.getMessage());
			e.printStackTrace();
			errorReporter.report("Instantiator-impl: "+pfx+" io exception: "+e.getMessage());
			running = false;
		}
	}



	public boolean start() {
		System.err.println("instantiator-impl "+this.toString()+" start running="+(running?"t":"f")+" in="+in+" out="+out);
		if (running && in != null && out !=  null) {
			// check that indeed the instantiator is up and running:
			String pfx = "C_EVENT ";
			try {
				String cmd = pfx+"\n";
				sendRequest(cmd);
				String s = readUntil(pfx, eventAnswers);
				if (s==null)
					return  false;
				consumePendingLines(pfx);
			} catch (IOException e) {
				System.err.println("Instantiator: start: io exception: "+e.getMessage());
				e.printStackTrace();
				errorReporter.report("Instantiator: start: io exception: "+e.getMessage());
				running = false;
				return false;
			}	
			return true;
		} else
			return false;
	}
	
	public void subprocessDbgMsgln(String s) {
		if (showSubProcessDebug)
			System.out.println(name+" subdbg: "+s);
	}
	public void dbgMsgln(String s) {
		if (debug)
			System.out.println(name+" dbg: "+s);
	}

	public InstantiatorImpl(ErrorReporter r, String command, String dirName) /*throws IOException*/ {
		
		//String[] commands = {"python", "3DVis.py", "-passive"};
		// String[] settings = {"path=%path%;C:\\python25"};
		
		System.err.println("instantiator-impl  "+this.toString()+" constructor begin");
		
		running = false;
		name = "Torx-Instantiator";
		
		cs = new ChildStarter(name, r, command, dirName, new NullProgressReporter(), null);
		cs.run();
		running = cs.isRunning();
		in = cs.getIn();
		out = null;
		if (running && in!=null && cs.getOut() != null) {
			out = new BufferedReader(cs.getOut());
		}

		running = cs.isRunning();

		
	}


	private void sendRequest(String s) throws IOException {
		in.write(s.getBytes());
		in.flush();
	}
	
	private String eventAnswers[] = { "A_EVENT" };
	private String quitAnswers[] = { "A_QUIT" };
	
	private boolean running = false;
	private ErrorReporter errorReporter;
	private String name;

	private OutputStream in;
	private BufferedReader out;
	
	private Boolean showSubProcessDebug = true;
	private Boolean debug = true;
	private Boolean cleanedUp = false;

	final String instErr = "Instantiator-impl: could not obtain instantiation";
	
	private ChildStarter cs;
}
