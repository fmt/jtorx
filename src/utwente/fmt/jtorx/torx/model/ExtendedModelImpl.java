package utwente.fmt.jtorx.torx.model;

import java.util.Vector;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.torx.ExtendedModel;
import utwente.fmt.jtorx.torx.LabelPlusVerdict;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.State;


public class ExtendedModelImpl extends ModelImpl implements ExtendedModel {
	
	public Vector<Label> getVerdicts(State s) {
		return getMenu(s, isVerdict, false, false);
	}
	public Vector<LabelPlusVerdict> getOutputVerdicts(State s) {
		return getMenuWithVerdict(s, isOutputOrQuiescence, false, false);
	}


	public ExtendedModelImpl(CompoundLTS l, IOLTSInterpretation i, ErrorReporter r) {
		super(l, i, r);
	}

	LabelConstraint isVerdict = new VerdictConstraint();
}
