package utwente.fmt.jtorx.torx.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.torx.LabelPlusVerdict;
import utwente.fmt.jtorx.torx.guided.GuidedState;
import utwente.fmt.jtorx.torx.guided.GuidedVerdict;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.Action;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;
import utwente.fmt.test.Verdict;


public class ModelImpl implements utwente.fmt.jtorx.torx.Model {

	public class OutputQuiescenceConstraint implements LabelConstraint {

		public Boolean isSatisfiedBy(Label l) {
			Action a = l.getAction();
			return (isOutput.isSatisfiedBy(l) || a.getString().equalsIgnoreCase("delta") );
		}
		
		public OutputQuiescenceConstraint(LabelConstraint lc) {
			isOutput = lc;
		}
		
		private LabelConstraint isOutput;
	}

	
	public CompoundState start() throws LTSException {
		CompoundState s = null;
		Iterator<? extends CompoundState> sit = lts.init();
		// this breaks when we have more than a single start state
		while (sit.hasNext())
			s = sit.next();
		return s;
	}
	public void done() {
		lts.done();
	}

	public Vector<Label> menu(CompoundState s) {
		return getMenu(s, interp.isAny());
	}
	
	public Vector<Label> getInputs(CompoundState s) {
		return getMenu(s, interp.isInput(), true, false);
	}
	public Vector<Label> getOutputs(CompoundState s) {
		return getMenu(s, isOutputOrQuiescence, false, true);
	}
	
	public Transition<? extends State> findTransition(CompoundState s, Label l) {
		Iterator<? extends Transition<?extends State>> tit = cachedMenu(s, interp.isAny()); // s.menu(interp.isAny());
		while (tit.hasNext()) {
			Transition<?extends State> t = tit.next();
			if (t.getLabel().equals(l))
				return t;
		}
		return null;
	}
	
	public Verdict getNegativeDefaultVerdict() {
		return lts.getNegativeDefaultVerdict();
	}
	public Verdict getPositiveDefaultVerdict() {
		return lts.getPositiveDefaultVerdict();
	}


	// Regarding the non-symbolic case:
	// we can change the linear search lookup using while loop, below, into one
	// where we use a hashmap<Label, CompoundState> from label to destination,
	// but then we must be sure that the label that we use to lookup will find
	// the label that we put into the table.
	// We cannot guarantee that that works/happens.
	// To guarantee that to happen, independent of case or whitespace issues,
	// we should have to put a canonical representation of the (string in the label)
	// and we do not do that (yet?)
	// Thus, for now we use linear search.
	// (note-to-self: it is interesting that we nevertheless use Label.eq to compare labels)
	// NOTE: if we would somehow sort the items, could we then use some kind of binary search?
	//
	// Regarding the symbolic case:
	// 1) we check for unifyability instead of checking eq
	// 2) if unifyable, we have to instantiate the transition
	//    using the bindings obtained in the unification; this may succeed or fail
	// 3) if instantiation succeeds, we obtain an updated transition with updated
	//    (parameter-free?) label and parameter-free destination state
	// NOTE there may be multiple parameterized transitions that match a single given parameter-free label
	//
	// What to do in the case of guidance?
	// In the original TorX we had two kind of guidance labels in symbolic setting:
	//  - labels that have a wild card (*) corrresponding with each parameter
	//    (this has no effect on label shown in susp automaton, not on verdict (hit/miss))
	//  - ('recent' addition) labels that have an actual value corresponding to a parameter.
	//    - the actual value is used as instantiation of the parameter
	//    - this does change the label shown in susp automaton for inputs
	//      (because we show 'intersection' for inputs and 'union' for outputs)
	//    - for inputs we can only show the label with actual value after having checked its validity,
	//    - for outputs, correct hit/miss verdict is onluy known after having checked validity of actual value
	//
	// As a consequence, in guided setting, when instantiating:
	//  - if label in susp automaton contains parameter we must assume that corresponding guidance label contains wildcard
	//     (we should be able to match the guidance label with the instantiation using unification)
	//  - if label in susp automaton contains actual value
	//     - either both spec and guide have actual value there (which will be simply matched)
	//     - or spec has actual value and guide has wild card
	//     - or spec has parameter and guide has actual value
	// Question now is:
	//    - at which point do we use instantiation label, and
	//    - at which point do we use binding
	// It seems that we need instantiaton label in guidance, and in primer, and
	//               we use binding in explorer
	// Problem: this means that guidance/primer interface differs from explorer interface
	// (not all are same LTS)
	// Solutution attempt: abstract the intantiation parameter into interface Instantiation,
	// and obtain from it either label or bindings, depending on what is needed
	// (if that works).
	
	public class MyTransitionIterator implements java.util.Iterator<Transition<?extends State>> {

		public boolean hasNext() {
			return (idx < transitions.size());
		}

		public Transition<?extends State> next() {
			if (idx < transitions.size()) {
				Transition<?extends State> result = transitions.elementAt(idx);
				idx++;
				skipNonMatching();

				return result;
			}
			return null;
		}

		public MyTransitionIterator(Vector<CompoundTransition<?extends CompoundState>> t, LabelConstraint c) {
			transitions = t;
			constraint = c;
			idx = 0;
			skipNonMatching();
		}
		
		private void skipNonMatching() {
			if (constraint != null && transitions != null)
				while(idx < transitions.size() && !constraint.isSatisfiedBy(transitions.elementAt(idx).getLabel()))
					idx++;
		}
		
		private Vector<CompoundTransition<?extends CompoundState>> transitions;
		private LabelConstraint constraint;
		private int idx;
		
		public void remove() {
			throw new UnsupportedOperationException("LibTransitionIterator.remove unimplemented");
		}

	}

	
	private Iterator<? extends Transition<?extends State>>cachedMenu(State s, LabelConstraint lc) {
		if (!s.equals(cachedState)) {
			if(debug) System.err.println("cachedmenu: s="+s+" != "+cachedState);
			return s.menu(lc);
		} else {
			if(debug) System.err.println("cachedmenu: s="+s+" == "+cachedState);
			return new MyTransitionIterator(cachedMenu, lc);
		}
	}
	
	public CompoundTransition<? extends CompoundState> next(CompoundState s, Label l) {
		CompoundTransition<? extends CompoundState> res = s.next(l);
		CompoundState dst = null;
		if (res!=null)
			dst = res.getDestination();

		if (dst!=null && !dst.getID().equals(s.getID())) {
			Collection<CompoundState> pres = new Vector<CompoundState>();
			pres.add(s);
			s.getLTS().cleanupPreserving(pres);
		}
		
		// TODO make sure the following continues to work (force expansion; disposeTransitions)
		if (dst!=null) {
				// force expansion
			if(debug) System.out.println("ModelImpl next: forced expansion of dst: start");
			Iterator<? extends CompoundTransition<? extends CompoundState>> it = dst.menu(interp.isAny());
			long curTime = System.currentTimeMillis();
			cachedMenu.clear();
			int i = 0;
			while(it.hasNext()) {
				cachedMenu.addElement(it.next());
				i++;
			}
			cachedState = dst;
			err.log(curTime, "MODELSTATS", "#trans "+i);

			if(debug) System.out.println("ModelImpl next: forced expansion of dst: done");
		}

		if(debug) System.out.println("ModelImpl next: "+s.getID()+" "+s+" --> "+(dst!=null?dst.getID():"(null)")+" "+dst);
		if (dst!=null && dst!=s && !dst.getID().equals(s.getID())) {
			if(debug) System.out.println("\tModelImpl next: dispose "+s.getID()+" "+s);
			s.disposeTransitions();
		} else
			if(debug) System.out.println("\tModelImpl next: no dispose");
		return res;
	}

	protected Vector<Label> getMenu(State s, LabelConstraint lc) {
		return getMenu(s, lc, true, false);
	}
	

	/**
	 * @param s state of which menu is requested
	 * @param lc constraint to select items of interest
	 * @param omitLeadingToMiss when true, omit any transitions that have as target a GuidedState of which verdict contains miss
	 * @param omitLonelyLeadingToMiss when true and omitLeadingToMiss==false, omit any transitions that lead to miss (as for omitLeadingToMiss) unless we report also other labels
	 * @return a list of labels of the transitions of the menu of s, filtered by lc and the omit*LeadingToMiss parameters
	 */
	protected Vector<Label> getMenu(State s, LabelConstraint lc, boolean omitLeadingToMiss, boolean omitLonelyLeadingToMiss) {
		Iterator<? extends Transition<?extends State>> tit = cachedMenu(s, lc); //s.menu(lc);
		Vector<Label> m = new Vector<Label>();
		Vector<Label> mm = new Vector<Label>();
		while (tit.hasNext()) {
			Transition<?extends State> t = tit.next();
			State dst = t.getDestination();
			if (dst instanceof GuidedState) {
				GuidedState g_dst = (GuidedState)dst;
				GuidedVerdict v = g_dst.getVerdict();
				if (v != null && v.getString().contains("miss")) {
					mm.add(t.getLabel());
					continue;
				}
			}
			m.add(t.getLabel());
		}
		if (!omitLeadingToMiss && (!omitLonelyLeadingToMiss || m.size() > 0))
			m.addAll(mm);
		return m;
	}
	
	/**
	 * @param s state of which menu is requested
	 * @param lc constraint to select items of interest
	 * @param omitLeadingToMiss when true, omit any transitions that have as target a GuidedState of which verdict contains miss
	 * @param omitLonelyLeadingToMiss when true and omitLeadingToMiss==false, omit any transitions that lead to miss (as for omitLeadingToMiss) unless we report also other labels
	 * @return a list of labels of the transitions of the menu of s, filtered by lc and the omit*LeadingToMiss parameters
	 */
	protected Vector<LabelPlusVerdict> getMenuWithVerdict(State s, LabelConstraint lc, boolean omitLeadingToMiss, boolean omitLonelyLeadingToMiss) {
		Iterator<? extends Transition<?extends State>> tit = cachedMenu(s, lc); // s.menu(lc);
		Vector<LabelPlusVerdict> m = new Vector<LabelPlusVerdict>();
		Vector<LabelPlusVerdict> mm = new Vector<LabelPlusVerdict>();
		while (tit.hasNext()) {
			Transition<?extends State> t = tit.next();
			State dst = t.getDestination();
			if (dst instanceof GuidedState) {
				GuidedState g_dst = (GuidedState)dst;
				GuidedVerdict v = g_dst.getVerdict();
				if (v != null && v.getString().contains("miss"))
					mm.add(new LabelPlusVerdict(t.getLabel(), v));
				else
					m.add(new LabelPlusVerdict(t.getLabel(), v));
			} else
				m.add(new LabelPlusVerdict(t.getLabel(), null));
		}
		if (!omitLeadingToMiss && (!omitLonelyLeadingToMiss || m.size() > 0))
			m.addAll(mm);
		return m;
	}


	
	public ModelImpl(CompoundLTS l, IOLTSInterpretation i, ErrorReporter r){
		lts = l;
		interp = i;
		isOutputOrQuiescence = new OutputQuiescenceConstraint(i.isOutput());
		err = r;
	}
	
	
	private IOLTSInterpretation interp;
	protected LabelConstraint isOutputOrQuiescence;
	private CompoundLTS lts;
	private boolean debug=false;
	private ErrorReporter err = null;
	
	Vector<CompoundTransition<? extends CompoundState>> cachedMenu = new Vector<CompoundTransition<? extends CompoundState>>();
	CompoundState cachedState = null;
}
