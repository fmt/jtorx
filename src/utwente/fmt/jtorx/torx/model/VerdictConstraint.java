package utwente.fmt.jtorx.torx.model;

import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;

public class VerdictConstraint  implements LabelConstraint {

	public Boolean isSatisfiedBy(Label l) {
		return l.getAction().getString().equals("verdict");
	}
	
}
