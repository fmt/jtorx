package utwente.fmt.jtorx.torx.primer;

import java.util.Vector;

import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class AngelicCompletionLoopTransition<S extends State> implements Transition<S> {

    private final Label label;
    private final S state;
    private final String edgeName;
    
    public AngelicCompletionLoopTransition(S state, Label label) {
        this.label = label;
        this.state = state;
        this.edgeName = "acloop";
    }
    
    @Override
    public Label getSolution() {
        return label;
    }

    @Override
    public S getSource() {
        return state;
    }

    @Override
    public Label getLabel() {
        return label;
    }

    @Override
    public S getDestination() {
        return state;
    }

    @Override
    public Vector<Position> getPosition() {
        return null;
    }

    @Override
    public Position getLabelPosition() {
        return null;
    }

    @Override
    public String getEdgeName() {
        return edgeName;
    }

    @Override
    public Transition<S> instantiate(Instantiation i) {
        return this;
    }
}
