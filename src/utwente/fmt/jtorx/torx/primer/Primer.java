package utwente.fmt.jtorx.torx.primer;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.torx.driver.VerdictImpl;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.test.Verdict;


// We SHOULD extend the Interpretation for the users of this,
// because it now also distinguishes Quiescence as special label

// GET RID of traces-only and simulate features/parameters here?
// and then re-implement a non-determinizing simulator that does
// know about quiescence elsewhere?
// (extract that functionality from


public class Primer implements CompoundLTS {
	
	public Iterator<? extends CompoundState> init() throws LTSException {
		Iterator<? extends State> ltsIt = givenLTS.init();
		HashSet<State> ltsStates = new HashSet<State>();	
		while (ltsIt.hasNext())
			ltsStates.add(ltsIt.next());
		PrimerState s = factory.mkInitState(ltsStates);
		System.err.println("Primer-init: "+s.toString());
		return  new PrimerStateIterator(s);

	}
	
	
	public void done() {
		factory.report();
		givenLTS.done();
	}
	
	public Boolean getTracesOnly() {
		return tracesOnly;
	}
	public IOLTSInterpretation getInterpretation() {
		return interp;
	}
	public ErrorReporter getErrorReporter() {
		return errorReporter;
	}
	public PrimerStateFactory getFactory() {
		return factory;
	}
	public StateComparator getStateComparator() {
		return stateComparator;
	}

	public Verdict getNegativeDefaultVerdict() {
		return new VerdictImpl("fail");
	}
	public Verdict getPositiveDefaultVerdict() {
		return new VerdictImpl("pass");
	}
	
	public ProgressReporter getProgressReporter() {
		return progressReporter;
	}
	public boolean getStoreAll() {
		return storeAll;
	}
	public TraceKind getTracesKind() {
		return tracesKind;
	}

	protected void addUtraceOmittedInputs(CompoundState s, Collection<Transition<? extends State>> t) {
		if (omittedUtraceInputs!=null)
			omittedUtraceInputs.put(s, t);
	}
	
	public boolean angelicCompleteInputs() {
		return angelicCompleteInputs;
	}
	
	protected boolean addDeltaForDivergence() {
		return deltaForDivergence;
	}

	public Map<CompoundState,Collection<Transition<? extends State>>> getOmmittedUtraceInputs() {
		return omittedUtraceInputs;
	}

	public long getNrOfStates() {
		return -1;
	}
	
	public long getNrOfTransitions() {
		return -1;
	}

	public boolean hasPosition() {
		return false;
	}
	
	public Position getRootPosition() {
		return null;
	}
	
	public void cleanupPreserving(Collection<?extends State> ps) {
		// should clean up level in PrimerStateFactory
		factory.cleanupPreserving(ps);
	}
	
	public LTS getSubLTS() {
		return givenLTS;
	}


	public Primer(LTS lts, IOLTSInterpretation i, Boolean to, ErrorReporter r) {
		this(lts, i, to, false, TraceKind.STRACES, r, new NullProgressReporter());
	}
	public Primer(LTS lts, IOLTSInterpretation i, Boolean to, boolean remem, TraceKind tk, ErrorReporter r) {
		this(lts, i, to, remem, tk, r, new NullProgressReporter());
	}
	public Primer(LTS lts, IOLTSInterpretation i, TraceKind tk, Boolean to, ErrorReporter r, ProgressReporter p) {
		this(lts, i, to, false, tk, r, p);
	}
	
	public Primer(LTS lts, IOLTSInterpretation i, Boolean to, boolean remem, TraceKind tk, ErrorReporter r, ProgressReporter p) {
		this(lts, i, to, remem, tk, false, false, r, p);
	}

	public Primer(LTS lts, IOLTSInterpretation i, Boolean to, boolean remem, TraceKind tk, boolean aci, boolean divergence, ErrorReporter r, ProgressReporter p) {
		givenLTS = lts;
		tracesOnly = to;
		storeAll = remem;
		interp = i;
		errorReporter = r;
		progressReporter = p;
		tracesKind = tk;
		angelicCompleteInputs = aci;
		deltaForDivergence = divergence;
		factory = new PrimerStateFactory(this);
		if (tk != TraceKind.STRACES)
			omittedUtraceInputs = new HashMap<CompoundState,Collection<Transition<? extends State>>>();
	}


	class StateComparator implements Comparator<State> {

	    // Comparator interface requires defining compare method.
	    public int compare(State sa, State sb) {
	        return sa.getID().compareTo(sb.getID());
	    }
	}
	

	
	private LTS givenLTS;
	private Boolean tracesOnly;
	private IOLTSInterpretation interp;
	private ErrorReporter errorReporter;
	private PrimerStateFactory factory;

	private StateComparator stateComparator = new StateComparator();
	private ProgressReporter progressReporter;
	
	private boolean storeAll =  false;
	private TraceKind tracesKind = TraceKind.STRACES;
	
	private boolean angelicCompleteInputs =  false;
	private boolean deltaForDivergence = false;
	
	private Map<CompoundState,Collection<Transition<? extends State>>> omittedUtraceInputs = null;

}
