package utwente.fmt.jtorx.torx.primer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.eekboom.utils.Strings;

import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.torx.InstantiationImpl;
import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.utils.EmptyIterator;
import utwente.fmt.jtorx.utils.term.TermParser;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.SuspensionAutomatonState;
import utwente.fmt.lts.Term;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.UnificationResult;
import utwente.fmt.test.Verdict;

public class PrimerState implements SuspensionAutomatonState {
	


	public String getFullID() {
		String s = "";
		s += "id("+nr+")";
		PrimerState ps;
		if (canonical==null)
			ps = theLTS.getFactory().get(this.initStates);
		else
			ps = theLTS.getFactory().get(canonical.initStates);
		if (ps != null) {
			s += "[";
			s += ps.nr;
			s += "]";
		}
		s += " lts states: ";
		s += "init(";
		for(State st: initStates ) {
			s += st.getID();
			s += " ";
		}
		s += ") trans(";
		if (canonical==null)
			s+= "null";
		else {
			Set<State> tmp = new HashSet<State>(canonical.initStates);
			tmp.removeAll(initStates);
			for(State st: tmp) {
				s += st.getID();
				s += " ";
			}
		}
		s += ")";
		return s;
	}
	public String getID() {
		String s =  "";
		if (canonical == null)
			s += nr;
		else
			s += canonical.nr; //+"("+nr+")";
		return s;
	}
	
	public PrimerState getCanonical() {
		return canonical;
	}
	
	public String getLabel(boolean quote) {
		return getLabel(quote, -1);
	}
	public String getLabel(boolean quote, int maxLength) {
		if (canonical != null && canonical != this)
			return canonical.getLabel(quote, maxLength);
		
		if (label==null) {
			// TODO decide whether we already stop asking for labels
			// as soon as we get more than maxLabelsShown,
			// or, as we do now, ask for all, sort them, and only then
			// apply the limitation.
			// only asking for those we actually do show will improve performance
			Vector<String> labels = new Vector<String>();
			for(State st: initStates ) {
				String l = st.getLabel(true).trim();
				labels.add(l);
			}
			Collections.sort(labels, Strings.getNaturalComparator());

			String s = "";
			//s += "p"+getID()+":";
			s += "{ ";
			int i = 0, lastLine = 0;
			for(String l: labels ) {
				if (maxLength<0 || s.length() <= maxLength-3/*for the dots*/) {
					if (i > 0)
						s += ", ";
					// following is disabled, because ugly in utracesCheck output
					if (false && s.substring(lastLine).length() > 7) {// think of better heuristic?
						s += "\\n ";
						lastLine = s.length();
					}
					s += l;
				} else {
					s += "...\\n("+(labels.size()-i)+" more)";
					break;
				}
				i++;
			}
			s += " }";
			label = s;
		}
		return label;
	}
	
	public Position getPosition() {
		return null;
	}
	public String getNodeName() {
		if (canonical != null && canonical != this)
			return canonical.getNodeName();
		
		if (nodeName==null) {
			Vector<String> names = new Vector<String>();
			for(State st: initStates ) {
				String n = st.getNodeName();
				if (n!=null)
					n = n.trim();
				names.add(n);
			}
			Collections.sort(names, Strings.getNaturalComparator());
			
			String s = "";
			s += "{ ";
			boolean first = true;
			for(String l: names ) {
				if (first)
					first = false;
				else
					s += ", ";
				s += l;
			}
			s += " }";
			nodeName = s;
		}
		return nodeName;
	}
	
	public int getNr() {
		return nr;
	}
	public CompoundLTS getLTS() {
		return theLTS;
	}
	public Primer getPrimer() {
		return theLTS;
	}
	
	public void disposeTransitions() {
		if (debug) System.err.println("PrimerState dispose "+getID()+" "+toString()+" "+
				(canonical==null?"cnull":(canonical==this?"cthis":(canonical.getID()+" "+canonical))));
		if (transitions!=null) {
			transitions.clear();
			transitions=null;
		}
		if (subTransTransitions!=null)
			subTransTransitions = null;
		if (canonical!=null && canonical!=this) {
			canonical.disposeTransitions();
			// do NOT remove link to canonical,
			// to allow canonical ID (which we obtain via getID())
			// of cached transitions (e.g. in DotLTS)
			// to continue to work for visualization
			// (otherwise, when state.nr and state.canonical.nr differ,
			//  getID of a state suddenly gives different result)
			// canonical = null;
		} else {
		    if (debug) {
		        System.err.println("PrimerState not recursively disposing canonical "+getID()+" "+toString()+" "+
		                (canonical==null?"cnull":(canonical==this?"cthis":(canonical.getID()+" "+canonical))));
		    }
		}
	}


	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		final Transition<?>[] sub;
		if (subTransTransitions!=null)
			sub = subTransTransitions;
		else if (canonical!=null)
			sub = canonical.subTransTransitions;
		else
			sub = null;
		if (sub==null)
			return EmptyIterator.iterator();
		else
			return new Iterator<Transition<? extends State>>() {
				public boolean hasNext() {
					return i < sub.length;
				}
				public Transition<? extends State> next() {
					if (i< sub.length) {
						i++;
						return sub[i-1];
					}
					return null;
				}
				public void remove() {
					throw new UnsupportedOperationException("PrimerState.getSubTransitions.iterator.remove unimplemented"); 
				}
				private int i = 0;
			};
	}
	
	public String getSubEdgeName() {
		final Transition<?>[] sub;
		if (subTransTransitions!=null)
			sub = subTransTransitions;
		else if (canonical!=null)
			sub = canonical.subTransTransitions;
		else
			sub = null;
		if (sub==null)
			return null;
		
		return PrimerTransition.mkEdgeName(sub);
	}
	public Iterator<? extends State> getSubStates() {
		if (canonical==null)
			return initStates.iterator();
		else
			return canonical.initStates.iterator();
	}
	public Iterator<? extends State> getSubStates(LTS l) {
		return getSubStates();
	}
	public Iterator<? extends State> getDirectSubStates() {
		return initStates.iterator();
	}
	public Iterator<? extends State> getIndirectSubStates() {
		if (canonical==null || canonical==this){
			if(debug) System.err.println("getIndirectSubStates "+this.toString()+" canonical="+canonical);
			return EmptyIterator.iterator();
		} else {
			if (debug) System.err.println("getIndirectSubStates "+this.toString()+" canonical="+canonical);
			Set<State> tmp = new HashSet<State>(canonical.initStates);
			tmp.removeAll(initStates);
			if(debug) System.err.println("getIndirectSubStates "+this.toString()+
					" canonical="+canonical+
					" #caninit="+canonical.initStates.size()+
					" #init="+initStates.size()+
					" #tmp="+tmp.size());
			return tmp.iterator();
		}
	}


	public Iterator<? extends CompoundTransition<? extends CompoundState>> menu(LabelConstraint l) {
		if (transitions == null && (canonical==null || canonical.transitions==null))
			expand();
		else
			if(debug) System.err.println("PrimerState-menu-already-expanded "+this.getID());

		return new PrimerTransitionIterator(canonical.transitions, l);
	}
	
	public CompoundTransition<? extends CompoundState> next(Label l) {
		if (transitions == null && (canonical==null || canonical.transitions==null))
			expand();
		if (canonical!=null && canonical!=this)
			return canonical.next(l);
		if(debug) System.out.println("PrimerState.next: "+this.getID()+" l="+l.getString());
		if (l==null) {
			System.out.println("PrimerState.next: l==null -> returning null");
			return null;
		}
		if (transitions==null) {
			System.out.println("PrimerState.next: transitions==null -> returning null");
			return null;
		}
		Vector<CompoundState> res_dsts = new Vector<CompoundState>();
		Vector<CompoundTransition<?extends CompoundState>> res_ts =
			new Vector<CompoundTransition<?extends CompoundState>>();
		CompoundTransition<?extends CompoundState> res = null;
		if(debug) System.out.println("PrimerState.next: looping through menu: start...");
		for (CompoundTransition<?extends CompoundState> t:  transitions) {
			Label t_l = t.getLabel();
			Term t_tm = TermParser.parse(t_l);
			Term l_tm = TermParser.parse(l);
			UnificationResult uni_res = t_tm.unify(l_tm);
			if(debug) System.out.println("PrimerState.next: trying: l: ("+l.getString()+") "+l.hashCode()+" "+l.toString()+" tl:("+t_l.getString()+")"+t_l.hashCode()+" "+t_l.toString());
			if (uni_res.isUnifyable()) {
				if(debug) System.out.println(" is-unifyable");
				ArrayList<UnificationBinding> b = uni_res.getBindings();
				if (b==null || b.isEmpty()) {
					// no work to do: isUnifyable, no bindings involved
					// should we check whether there are free variables remaining?
					if(debug) System.out.println(" no work to do (no bindings)");
					res_ts.add(t);
					res_dsts.add(t.getDestination());
				} else {
					// this is where we have instantiate the transition
					CompoundTransition<?extends CompoundState> t_inst = t.instantiate(new InstantiationImpl(l, l_tm, b));
					if(debug) System.out.println("PrimerState.next: inst-result="+t_inst);
					if (t_inst != null) { // i.e. instantiation was succesful
						res_ts.add(t_inst);
						res_dsts.add(t_inst.getDestination());
						if(debug) System.out.println("PrimerState.next: instantiation YES succesful: "+t_l.getString()+
								(t_l.getConstraint()!=null?(" ["+t_l.getConstraint().getString()+"]"):"")+
										" with "+l.getString());
					} else { // continue with next round, because instantiation unsuccesful
						if(debug) System.out.println("");
						if(debug) System.out.println("PrimerState.next: instantiation unsuccesful: "+t_l.getString()+
								(t_l.getConstraint()!=null?(" ["+t_l.getConstraint().getString()+"]"):"")+
								" with "+l.getString());
					}
				}
			} else
				if(debug) System.out.println("PrimerState.next: trying result: NOT-unifyable");
		}
		if(debug) System.out.println("PrimerState.next: done looping through menu, res_ts.size()="+res_ts.size());
		if (res_ts.size()==0)
			return null;

		res = mergeTransitions(l, res_ts);

		return res;
	}

	private CompoundTransition<? extends CompoundState> mergeTransitions(Label l, Collection<CompoundTransition<?extends CompoundState>> transSet) {
		if (transSet==null || transSet.size()==0) {
			System.out.println("PrimerState.mergeTransitions: done: null or .size()==0");
			return null;
		}
		if(debug) System.out.println("PrimerState.mergeTransitions: .size() :"+transSet.size());
		if (transSet.size()==1) {
			if(debug) System.out.println("PrimerState.mergeTransitions: done:  special case: size()==1");
			return transSet.iterator().next();
		}
		else { // really merge
			if(debug) System.out.println("PrimerState.mergeTransitions: really merging");
			Set<Transition<?extends State>> u_ts =
				new HashSet<Transition<?extends State>>();
			Set<State> u_ds = new HashSet<State>();
			for(CompoundTransition<?extends CompoundState> t: transSet) {
				Iterator<? extends Transition<?extends State>> t_it = t.getSubTransitions();
				while (t_it.hasNext()) {
					Transition<?extends State> t_sub = t_it.next();
					u_ts.add(t_sub);
					if(debug) System.out.println("PrimerState.mergeTransitions: merging: \""+t_sub.toString()+"\": \""+t_sub.getEdgeName()+"\"");
				}
				CompoundState t_dst = t.getDestination();
				Iterator<? extends State> s_it = t_dst.getSubStates();
				while (s_it.hasNext())
					u_ds.add(s_it.next());
			}
			PrimerState dst = theLTS.getFactory().mkExpandedState(this, u_ds);
			PrimerTransition  res = new PrimerTransition(this, l, dst, u_ts);
			if(debug) System.out.println("PrimerState.mergeTransitions: done really merging (l=\""+l.getString()+"\", dstSz="+u_ds.size()+", "+u_ts.size()+")");
			return res;
		}
	}


	private void expand() {
		// to keep track of the underlying transitions:
		// have a mapping from (destination) state to set of incoming transitions;
		// while constructing menu and looking at a transition t:
		// incoming(dst(t)) = t union incoming(src(t))
		// when constructing the suspension automaton transition t' add to it:
		// union over d in menu.get(lbl(t')) of incoming(d)
		
		// originally, we visited states in BFS order:
		//  - newly encountered states are added to end of list work
		//  - we go through work from start to end
		
		// for tau-cycle (divergence) detection we change this:
		//  - newly encountered states are added to end of list work
		//  - we go through work from end to start
		//  - we keep a stack of states that gives trace to root
		//  - we keep a stack of brother-counters that tell how many more
		//    brothers or sisters we have to visit before we are done with a level

		theLTS.getFactory().countExpanded();

		theLTS.getFactory().storeState(this, this.initStates);

		// transStates holds for each tau-reachable state tau transitions
		Map<State,Collection<Transition<?extends State>>> transStates =  new HashMap<State,Collection<Transition<?extends State>>>();
		Collection<Transition<?extends State>> transTransitions = null;
		if (collectTransTransitions)
			transTransitions = new Vector<Transition<?extends State>>();

		if(debug) System.err.println("PrimerState-menu-expanding "+this.getID());
		Map<Label,Set<State>> destMap = new HashMap<Label,Set<State>>();
		Map<Label,Vector<Transition<? extends State>>> underlying =
			new HashMap<Label,Vector<Transition<? extends State>>>();

		// next two are for doing some kind of angelic input completipn
		Map<Label,Set<State>> sourceMap = null;
		Set<State> stableUnderlyingStates = null;
		if (theLTS.angelicCompleteInputs()) {
			sourceMap = new HashMap<Label,Set<State>>();
			stableUnderlyingStates = new HashSet<State>();
		}

		// originall,
		// we sort the work list to obtain the same order as in old TorX
		// which allows us better to compare runs
		// this now no longer works with DFS work
		ArrayList<State> work = new ArrayList<State>(initStates);
		// Already sorted by PrimerStateFactory
		//Collections.sort(work,theLTS.getStateComparator());
		if(debug) for(State sss: work)
			System.err.println("\t\tinit exp state "+sss.getID());
		
		ArrayList<Integer> sibblings = new ArrayList<Integer>();
		sibblings.add(work.size());
		ArrayList<State> stack = new ArrayList<State>();
		Set<State> onLoop = new HashSet<State>();
		Set<State> expanded = new HashSet<State>();

		Vector<Transition<? extends State>> trans;
		Iterator<? extends Transition<? extends State>> tit;
		theLTS.getProgressReporter().reset();
		theLTS.getProgressReporter().setActivity("Expanding state");
		theLTS.getProgressReporter().setMax(work.size());
		
		// split handling of observable transitions in
		// handling of input and of output
		// deal with the o
		// while we are working on the current state in the workset,
		// at least treat the outputs so we know whether the
		// current state is quiescent or not, and

		Set<Label> stableInputs = null;
		Vector<Transition<? extends State>> observableInputs = new Vector<Transition<? extends State>>();
		Vector<Transition<? extends State>> omittedUtraceInputs = null;
		if (debug) System.err.println("PrimerState expand before while ("+work.size()+", "+sibblings.size()+", "+stack.size()+")");

		while(work.size() > 0 && sibblings.size() > 0) {
			// theLTS.getProgressReporter().setLevel(idx);
			
			if (debug) {
				String sibbString = "";
				for(Integer sib: sibblings) {
					sibbString += " "+sib;
				}
				System.err.println("sibblings-pre: "+sibbString);
			
				String workString = "";
				for(State w: work) {
					workString += " "+w.getLabel(false);
				}
				System.err.println("work: "+workString);
			}

			State s = work.get(work.size()-1);
			work.remove(work.size()-1);
			if (debug) System.err.println("PrimerState expand ("+work.size()+", "+sibblings.size()+", "+stack.size()+") "+s.getID()+" "+s.getLabel(false));

			if (debug) {
				String stackString = "";
				for(State ss: stack) {
					stackString += " "+ss.getLabel(false);
				}
				System.err.println("stack pre: "+stackString);
			}
			
			if (sibblings.size() > stack.size()) {
				if (debug) System.err.println("push, start new level");
				// push, to start working on new level
				stack.add(s);
				sibblings.set(sibblings.size()-1, sibblings.get(sibblings.size()-1)-1);
			} else {
				Integer sibCount = sibblings.get(sibblings.size()-1);
				while (sibCount==0) {
					// pop from stack
					if (debug) System.err.println("pop level");
					stack.remove(stack.size()-1);
					sibblings.remove(sibblings.size()-1);
					sibCount = sibblings.get(sibblings.size()-1);
				}
				// replace top of stack by next sibbling
				if (debug) System.err.println("replace top of stack");
				stack.remove(stack.size()-1);
				stack.add(s);
				sibblings.set(sibblings.size()-1, sibblings.get(sibblings.size()-1)-1);
			}
			
			if (debug) {
				String stackString = "";
				for(State ss: stack) {
					stackString += " "+ss.getLabel(false);
				}
				System.err.println("stack-post: "+stackString);
				String sibbString = "";
				for(Integer sib: sibblings) {
					sibbString += " "+sib;
				}
				System.err.println("sibblings-post: "+sibbString);
			}
			
			if (!this.getPrimer().addDeltaForDivergence() && expanded.contains(s)) {
				if(debug) System.err.println("PrimerState.expand: skipping already expanded state: "+s.getID());
				continue;
			}
			
			boolean isQuiescent = true;
			boolean isStable = true;
			boolean isOnTauLoop = false;
			Integer tauSibblings = 0;
			
			Set<Label> inputs = new HashSet<Label>();
			
			Collection<Transition<?extends State>>tauTrans = transStates.get(s);
			if (tauTrans == null) {
				tauTrans = new HashSet<Transition<?extends State>>();
				transStates.put(s, tauTrans);
			}

			boolean expanding = true;  // indicates whether we are expanding new state or revisiting already visited state to find tau loops
			if (!expanded.contains(s)) {
				if(debug) System.err.println("\t\texpanding explorer state "+s.getID());
				tit = s.menu(theLTS.getInterpretation().isAny());
				expanded.add(s);
			} else {
				if(debug) System.err.println("\t\trevisiting tau trans of explorer state "+s.getID());
				tit = tauTrans.iterator();
				expanding = false;
			}
			if (tit != null) {
				// split transition handling:
				//  - look at all transitions;
				//     - handle internal labels immediately
				//	   - handle output labels immediately
				//     - store input transitions in array, until we have seen all
				//  - when we have seen all, we know whether we are in a stable state
				//	- now go through the stored observable transitions, and do the right thing,
				//    depending on our knowledge about whether state is stable or not
				while(tit.hasNext()) {
					Transition<? extends State> t = tit.next();
					Label lbl = t.getLabel();
					State dst = t.getDestination();
					if (theLTS.getInterpretation().isInternal().isSatisfiedBy(lbl)) {
						isQuiescent = false;
						isStable = false;
						if (expanding)
							tauTrans.add(t);
						if (!initStates.contains(dst) && !transStates.containsKey(dst)) {
							transStates.put(dst, new HashSet<Transition<?extends State>>());
							//work.add(dst);
							//theLTS.getProgressReporter().setMax(work.size());
							if(debug) System.err.println("\t\t\tadding tau dest explorer state "+dst.getID());
						}
						if (stack.contains(dst)) {
							System.err.println("cycle found for "+dst.getLabel(false));
							onLoop.add(dst);
							isOnTauLoop = true;
							int j = stack.size()-1;
							while (j>= 0) {
								State o = stack.get(j);
								onLoop.add(o);
								if (debug) System.err.println("   onloop "+o.getLabel(false));
								if (o.equals(dst))
										break;
								j--;
							}
						} else if (!expanded.contains(dst)) {
							work.add(dst);
							tauSibblings++;
						}
						theLTS.getProgressReporter().setMax(work.size());
						if (collectTransTransitions)
							transTransitions.add(t);
					} else if(theLTS.getInterpretation().isInput().isSatisfiedBy(lbl)) {
						inputs.add(lbl);
						observableInputs.add(t);
					}else if(theLTS.getInterpretation().isOutput().isSatisfiedBy(lbl)) {
						isQuiescent = false;
						trans = underlying.get(lbl);
						if (trans == null) {
							trans = new Vector<Transition<? extends State>>();
							underlying.put(lbl, trans);
						}	
						trans.add(t);
						if (destMap.get(lbl) == null)
							destMap.put(lbl, new HashSet<State>());
						destMap.get(lbl).add(dst);
					} else {
						// TODO turn into exception?
						theLTS.getErrorReporter().report("Warning: no input/output for label: "+lbl.getString());
						if(debug) System.err.println("Warning: no input/output for label: "+lbl.getString());
					}
				}
			} else
				System.err.println("\t\t\ttit==null"); // throw exception?
			if (tauSibblings > 0)
				sibblings.add(tauSibblings);
			
			if (expanding && theLTS.getTracesKind() != TraceKind.STRACES) {
				if(debug) System.err.println("\t\t\tTracesKind="+theLTS.getTracesKind()+" isStable="+isStable+" stableInputs="+stableInputs);
				if (isStable && stableInputs==null)
					stableInputs = new HashSet<Label>(inputs);
				else if (isStable && stableInputs!=null)
					stableInputs.retainAll(inputs);
			}
			
			if (expanding && theLTS.angelicCompleteInputs() && isStable)
				stableUnderlyingStates.add(s);
			
			if(debug) System.err.println("PrimerState.expand: isQuiescent:"+isQuiescent+" isOnTauLoop:"+isOnTauLoop);
			if (((expanding && isQuiescent) || (this.getPrimer().addDeltaForDivergence() && isOnTauLoop))
					&& !this.getPrimer().getTracesOnly()) {
				Label lbl = new LibLabel("delta");

				if (destMap.get(lbl) == null)
					destMap.put(lbl, new HashSet<State>());
				destMap.get(lbl).add(s);
				
				if (this.getPrimer().addDeltaForDivergence() && isOnTauLoop)
					destMap.get(lbl).addAll(onLoop);


				trans = new Vector<Transition<? extends State>>();
				underlying.put(lbl, trans);
			}
		}
		for (Transition<? extends State> t: observableInputs) {
			Label lbl = t.getLabel();
			if(debug) System.err.println("\t\tlooping observableInput: "+lbl.getString()+" stableInputs="+stableInputs);
			if (theLTS.getTracesKind()==TraceKind.STRACES || (stableInputs!=null && stableInputs.contains(lbl))) {
				if(debug) System.err.println("about to add input");
				State dst = t.getDestination();
				trans = underlying.get(lbl);
				if (trans == null) {
					trans = new Vector<Transition<? extends State>>();
					underlying.put(lbl, trans);
				}	
				trans.add(t);
				if (destMap.get(lbl) == null)
					destMap.put(lbl, new HashSet<State>());
				destMap.get(lbl).add(dst);
				if (theLTS.angelicCompleteInputs()) {
					if (sourceMap.get(lbl) == null)
						sourceMap.put(lbl, new HashSet<State>());
					sourceMap.get(lbl).add(t.getSource());
				}
			} else if (theLTS.getTracesKind()!=TraceKind.STRACES && (stableInputs==null || !stableInputs.contains(lbl))) {
				if(debug) System.err.println("about to add omitted trace: "+lbl.getString());
				if (omittedUtraceInputs==null)
					omittedUtraceInputs = new Vector<Transition<? extends State>>();
				omittedUtraceInputs.add(t);
				if(debug) System.err.println("added omitted trace: "+lbl.getString());
			} else
				System.err.println("about to skip");
		}
		if (theLTS.angelicCompleteInputs()) {
			for (Label lbl: sourceMap.keySet()) {
			    if (debug) {
			        System.err.println("angelicCompleteInputs: looking at label: "+lbl.getString());
			    }
				Set<State> srcs = sourceMap.get(lbl);
				Set<State> missing = new HashSet<State>(stableUnderlyingStates);
				if (debug) {
				    System.err.println("angelicCompleteInputs: "+missing.size()+" stable underlying states ... for: "+lbl.getString());
				    System.err.println("angelicCompleteInputs: removing "+srcs.size()+" source states ... for: "+lbl.getString());
				}
				missing.removeAll(srcs);
				if (debug) {
				    System.err.println("angelicCompleteInputs: adding "+missing.size()+" selfloop states ... for: "+lbl.getString());
				}
				for (State sss: missing) {
				    if (debug) {
				        System.err.println("\tangelicCompleteInputs: adding selfloop \""+sss.getLabel(false)+"\" --  "+lbl.getString()+" --> \""+sss.getLabel(false)+"\"");
				    }
				    destMap.get(lbl).add(sss); // this should in one go add self-loops to all stable states that do not have an outgoing transition with this label
					
					// Fix for instantiation bug, see acceptance test InstantiateCompoundStateWithAngelicCompletion
					underlying.get(lbl).add(new AngelicCompletionLoopTransition<State>(sss, lbl));
				}
				if (debug) {
				    System.err.println("angelicCompleteInputs: done adding selfloop states for: "+lbl.getString());
				}
			}
		}

		theLTS.getProgressReporter().ready();

		Set<State> allStates = new HashSet<State>();
		allStates.addAll(initStates);
		allStates.addAll(transStates.keySet());
		PrimerState as = theLTS.getFactory().mkExpandedState(this, allStates);
		if (as != this)
			theLTS.getFactory().storeState(as, as.initStates);
		if (transStates.size() == 0 && as!=this)
			theLTS.getFactory().countExpandedIsNotInitState();
		if (canonical != null) {
		    if (debug) {
		        System.err.println("Warning: canonical != null: "+this.toString()+" canon: "+canonical.toString());
		    }
			theLTS.getFactory().countCanonicalNonNull();
		}
		if(debug) System.err.println("PrimerState canonicalize result: "+as.toString()+" this: "+this.toString());
		canonical = as;
		if (as.canonical==null)
			as.canonical = as;
		if (canonical.transitions == null) {
			canonical.transitions = new Vector<PrimerTransition>();
			for(Label lbl: destMap.keySet() ) {
				PrimerState dst = theLTS.getFactory().mkInitState(destMap.get(lbl));
				canonical.transitions.add(new PrimerTransition(this, lbl, dst, underlying.get(lbl)));
				if(debug) System.err.println("New Primertransition: "+this.toString()+" -- "+lbl.getString()+" --> "+dst.toString()+" "+dst.getLabel(false));
			}
		}
		if (canonical.subTransTransitions==null) {
			canonical.subTransTransitions = transTransitions.toArray(new Transition[0]);
			if(debug) 
			for(Transition<?extends State> tr: canonical.subTransTransitions ) {
				System.err.println("New Primer-sub-transition: "+this.toString()+" -- "+tr.getLabel().getString()+" --> "+tr.getDestination().toString()+" "+tr.getDestination().getLabel(false));
			}
		}
		if (omittedUtraceInputs!=null)
			theLTS.addUtraceOmittedInputs(canonical, omittedUtraceInputs);
	}

	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		//		if((o == null) || (o.getClass() != this.getClass()))
		//			return false;
		if (o instanceof PrimerState) {
			PrimerState s = (PrimerState) o;
			if (false && canonical!=null && canonical!=this)
				return canonical.equals(s);
			if (false && s.canonical!=null && s.canonical!=s)
				return (s.theLTS.equals(theLTS) &&
						(s.canonical.initStates.equals(initStates)));
			return (s.theLTS.equals(theLTS) &&
					(s.initStates.equals(initStates)));
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (false && canonical!=null && canonical!=this)
			return canonical.hashCode();
		int hash = 7;
		hash = 31 * hash + (null == initStates ? 0 : initStates.hashCode());
		return hash;
	}
	
	public PrimerState (Primer lts, Collection<State> hs, int nr) {
		theLTS = lts;
		initStates =  hs;
		transitions = null;
		this.nr = nr;
		subTransTransitions = null;
	}

	protected PrimerStateFactory getFactory() {
		return theLTS.getFactory();
	}
	protected Collection<State> getInitStates() {
		return initStates;
	}

	class StateComparator implements Comparator<State> {
		public int compare(State sa, State sb) {
			return sa.getID().compareTo(sb.getID());

		}
	}

 	private Collection<State> initStates;
//	private State[] initStates;
	private Transition<?>[] subTransTransitions = new Transition[0];
	private Primer theLTS;

	private Vector<PrimerTransition> transitions = null;
	private int nr;
	private PrimerState canonical = null;
	
	private String label = null;
	private String nodeName = null;

	static private boolean collectTransTransitions = true;

	public Verdict getVerdict() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private boolean debug = false;
}
