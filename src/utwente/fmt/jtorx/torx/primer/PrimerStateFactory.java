package utwente.fmt.jtorx.torx.primer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import utwente.fmt.lts.State;

public class PrimerStateFactory {
	
	public  PrimerState mkInitState(Collection<State> hs) {
		ArrayList<State> as = new ArrayList<State>(hs);
		Collections.sort(as,theLTS.getStateComparator());
		// debug print:
		//for(State s: as)
		//	System.out.println("mkInitStatefactory statecollection "+hs.toString()+ " contains "+s.toString());
		PrimerState ps = initMap.get(as);
		if (ps == null) {
			ps = initTmpMap.get(as);
			if (ps == null) {
				ps = new PrimerState(theLTS, as, nextnr++);
				if(debug) System.out.println("factory created init state "+(nextnr-1)+" "+ps.getFullID()+" "+ps.toString());
				// next statement we added to be able to get nice view of newspaper example; i.e.
				// when two different states each have (an) observable(?) transition(s?) to a third state
				// however, this will in general be expensive, especially when we never
				// again visit the state (then we should get rid of it again)
				if (theLTS.getStoreAll())
					storeState(ps, as);
			} else {
				init_tmp_found++;
				if(debug) System.out.println("\tfactory found init tmp state     "+ps.getFullID()+" "+ps.toString());
			}
		} else {
			init_found++;
			if(debug) System.out.println("\tfactory found init state     "+ps.getFullID()+" "+ps.toString());
		}
		return ps;
	}
	
	public void storeState(PrimerState ps, Collection<State> hs) {
		initTmpMap.put(hs, ps);
		if(debug) System.out.println("\tfactory stored state     "+ps.getFullID()+" "+ps.toString());

	}
	public void cleanupPreserving(Collection<? extends State> ts) {
		if (theLTS.getStoreAll())
			return;
		for(State s: ts) {
			if (s instanceof PrimerState) {
				PrimerState ps = (PrimerState) s;
				initMap.put(ps.getInitStates(), ps);
				PrimerState psc = ps.getCanonical();
				if (psc!=null && psc!=ps)
					initMap.put(psc.getInitStates(), psc);
			}
		}
		// or should we just use clear?
		initTmpMap = null;
		initTmpMap = new HashMap<Collection<State>,PrimerState>();
	}
		
	public PrimerState mkExpandedState(PrimerState s, Collection<State> hs) {
		ArrayList<State> as = new ArrayList<State>(hs);
		Collections.sort(as,s.getPrimer().getStateComparator());
		PrimerState ps = initMap.get(as);
		if (ps == null) {
			ps = initTmpMap.get(as);
			if (ps == null) {
				ps = new PrimerState(s.getPrimer(), as, s.getNr());
				if(debug) System.out.println("factory created expanded state "+(s.getNr())+" "+ps.getFullID()+" "+ps.toString());
			} else {
				if(debug) System.out.println("\tfactory found expanded tmp state     "+ps.getFullID()+" "+ps.toString());
			}
		} else {
			canonical_init_found++;
			if(debug) System.out.println("\tfactory found expanded state     "+ps.getFullID()+" "+ps.toString());
		}
		return ps;
	}
	
	public PrimerState get(Collection<State> hs) {
		ArrayList<State> as = new ArrayList<State>(hs);
		Collections.sort(as,theLTS.getStateComparator());
		PrimerState ps = initMap.get(hs);
		//if (ps != null)
			return ps;
		//return expandedMap.get(hs);
	}
		
	public void countExpandedIsNotInitState() {
		init_is_not_cannonical++;
	}
	public void countExpanded() {
		expanded++;
	}
	public void countCanonicalNonNull() {
		canonical_non_null++;
	}
	
	public void report() {
		System.err.println("PrimerStateFactoryReport("+nextnr+","+init_found+","+
				expanded+","+
				init_is_not_cannonical+","+
				canonical_put+","+canonical_init_found+","+canonical_expanded_found+","+
				canonical_non_null+
				")");
	}
	
	public PrimerStateFactory(Primer p) {
		theLTS = p;
	}

	private Map<Collection<State>,PrimerState> initMap = new HashMap<Collection<State>,PrimerState>();
	//private Map<Collection<State>,PrimerState> expandedMap = new HashMap<Collection<State>,PrimerState>();
	private Map<Collection<State>,PrimerState> initTmpMap = new HashMap<Collection<State>,PrimerState>();
	private int nextnr = 0;
	private int init_found = 0;
	private int init_tmp_found = 0;
	private int canonical_init_found = 0;
	private int canonical_expanded_found = 0;
	private int canonical_put = 0;
	private int init_is_not_cannonical = 0;
	private int expanded = 0;
	private int canonical_non_null = 0;
	
	private boolean debug = false;
	
	private Primer theLTS;

}
