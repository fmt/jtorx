package utwente.fmt.jtorx.torx.primer;

import java.util.Vector;

import utwente.fmt.lts.CompoundState;

public class PrimerStateIterator implements java.util.Iterator<CompoundState> {

	public boolean hasNext() {
		return idx < states.size();
	}

	public CompoundState next() {
		if (idx < states.size()) {
			return states.get(idx++);
		}
		return null;
	}
	
	
	public PrimerStateIterator () {
		states = new Vector<PrimerState>();
	}
	
	public PrimerStateIterator (PrimerState s) {
		this();
		states.add(s);
	}

	
	private Vector<PrimerState> states;
	int idx;
	public void remove() {
		throw new UnsupportedOperationException("PrimerStateIterator.remove unimplemented");
	}

}
