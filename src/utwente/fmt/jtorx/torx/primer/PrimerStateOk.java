package utwente.fmt.jtorx.torx.primer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.utils.EmptyIterator;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.test.Verdict;

// NOTE: this class is only used if we change the PrimerStateFactory to use it.

public class PrimerStateOk implements CompoundState {

	public String getFullID() {
		String s = "";
		s += "id("+nr+")";
		State ps = theLTS.getFactory().get(allStates);
		if (ps != null) {
			s += "[";
			s += ps.getID(); // TODO: take care here; this works because we have no canonical here
			s += "]";
		}
		s += " lts states: ";
		s += "init(";
		for(Iterator<State> it = initStates.iterator(); it.hasNext(); ) {
			s += it.next().getID();
			s += " ";
		}
		s += ") trans(";
		for(Iterator<State> it = transStates.iterator(); it.hasNext(); ) {
			s += it.next().getID();
			s += " ";
		}
		s += ")";
		return s;
	}
	public String getID() {
		String s =  "";
		s += nr;
		return s;
	}
	
	public PrimerStateOk getCanonical() {
		return this;
	}
	
	public String getLabel(boolean quote) {
		String s = "";
		s += "{ ";
		for(State st: allStates ) {
			s += st.getLabel(true) + " ";
		}
		s += "}";
		return s;
	}
	
	public Position getPosition() {
		return null;
	}
	public String getNodeName() {
		return null;
	}

	public CompoundLTS getLTS() {
		return theLTS;
	}
	public Primer getPrimer() {
		return theLTS;
	}
	
	public Iterator<? extends CompoundTransition<? extends CompoundState>> menu(LabelConstraint l) {
		// to keep track of the underlying transitions:
		// have a mapping from (destination) state to set of incoming transitions;
		// while constructing menu and looking at a transition t:
		// incoming(dst(t)) = t union incoming(src(t))
		// when constructing the suspension automaton transition t' add to it:
		// union over d in menu.get(lbl(t')) of incoming(d)
		if (isExpanded == false) {
			isExpanded = true;
			Map<Label,Set<State>> menu = new HashMap<Label,Set<State>>();
			Vector<State> work = new Vector<State>(initStates);
			Iterator<? extends Transition<? extends State>> tit;
			for(int idx = 0; idx < work.size(); idx++) {
				State s = work.get(idx);
				Boolean isQuiescent = true;
				tit = s.menu(theLTS.getInterpretation().isAny());
				while(tit.hasNext()) {
					Transition<? extends State> t = tit.next();
					Label lbl = t.getLabel();
					State dst = t.getDestination();
					if (theLTS.getInterpretation().isInternal().isSatisfiedBy(lbl)) {
						isQuiescent = false;
						if (!initStates.contains(dst) && !transStates.contains(dst)) {
							transStates.add(dst);
							work.add(dst);
						}				
					} else if(theLTS.getInterpretation().isInput().isSatisfiedBy(lbl) ||
							  theLTS.getInterpretation().isOutput().isSatisfiedBy(lbl)) {
						if (theLTS.getInterpretation().isOutput().isSatisfiedBy(lbl))
							isQuiescent = false;
						if (menu.get(lbl) == null)
							menu.put(lbl, new HashSet<State>());
						menu.get(lbl).add(dst);
					}
				}
				if (isQuiescent && !this.getPrimer().getTracesOnly()) {
					Label delta = new LibLabel("delta");
					HashSet<State> deltaDst = new HashSet<State>();
					//System.out.println("add delta from "+s.getID()+" to "+s.getID());
					deltaDst.add(s);
					menu.put(delta, deltaDst);
				}
			}
			allStates = new HashSet<State>();
			allStates.addAll(initStates);
			allStates.addAll(transStates);
			theLTS.getFactory().mkInitState(allStates); // should be: mkTransState
			for(Iterator<Label> lit = menu.keySet().iterator(); lit.hasNext(); ) {
				Label lbl = lit.next();
				CompoundState dst = theLTS.getFactory().mkInitState(menu.get(lbl));
				transitions.add(new PrimerTransition(this, lbl, dst));
			}
		}
		return new PrimerTransitionIterator(transitions, l);
	}
	
	public CompoundTransition<?extends CompoundState> next(Label l) {
		// TODO implement this method
		return null;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof PrimerStateOk) {
         PrimerStateOk s = (PrimerStateOk) o;
         return (s.theLTS.equals(theLTS) &&
        		 (s.initStates.equals(initStates) ||
        		    (s.isExpanded && isExpanded && s.allStates.equals(allStates))));
		}
		return false;
	}
	
	public PrimerStateOk (Primer lts, HashSet<State> hs, int nr) {
		theLTS = lts;
		initStates =  hs;
		transStates =  new HashSet<State>();
		isExpanded = false;
		transitions = new Vector<PrimerTransition>();
		this.nr = nr;
	}
	
	private Set<State> initStates;
	private Set<State> transStates;
	private Set<State> allStates;
	private Boolean isExpanded;
	private Primer theLTS;
	private Vector<PrimerTransition> transitions;
	private int nr;
	public Iterator<? extends State> getSubStates() {
		return EmptyIterator.iterator();
	}
	public Iterator<? extends State> getSubStates(LTS l) {
		return getSubStates();
	}

	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return EmptyIterator.iterator();
	}
	public String getSubEdgeName() {
		return null;
	}
	
	public void disposeTransitions() {
	}
	public Verdict getVerdict() {
		// TODO Auto-generated method stub
		return null;
	}

}
