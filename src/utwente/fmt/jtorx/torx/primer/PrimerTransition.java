package utwente.fmt.jtorx.torx.primer;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import com.eekboom.utils.Strings;

import utwente.fmt.jtorx.utils.term.TermParser;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Term;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.UnificationResult;

public class PrimerTransition implements CompoundTransition<CompoundState> {

	public CompoundState getDestination() {
		return dst;
	}

	public Label getLabel() {
		return lbl;
	}

	public CompoundState getSource() {
		return src;
	}
		
	public PrimerTransition instantiate(Instantiation i) {
		// what should we do?
		//  - look at i.label: if incompatible: return null
		//  - if nothing to do (variables empty) return this
		//  - if work to do:
		//      construct new transition, 
		//        by instantiating each of the subtransitions,
		//        and collecting the results
		//        ??? what about tau transitions?
		//          - should we keep them?
		//          - or should we only instantiate the visible sub-transitions,
		//            and then
		//              - if the instantiation result differs, expand it
		//              - if the instantiation result is identical, copy expansion
		//            (what do we do in TorX at this point?)
		//      - the instantiation label (if more explicit) would then be the label;
		//      - the destinations of the subtransitions combined
		//        give us the dest of the new transition
		if (i==null)
			return null;
		Label i_l = i.getLabel();
		Label t_l = lbl;
		if (i_l==null || t_l==null || i.getTerm()==null)
			return null;
		if (t_l.eq(i_l))
			return this;
		Term t_tm = TermParser.parse(t_l);
		Term i_tm = i.getTerm();
		UnificationResult uni_res = t_tm.unify(i_tm);
		if(debug) System.out.println("PrimerTransition.instantiate: l: ("+i_l.getString()+") "+i_l.hashCode()+" "+i_l.toString()+" tl:("+t_l.getString()+")"+t_l.hashCode()+" "+t_l.toString());
		// TODO: use official interface (access via subTransitions)
		if (uni_res.isUnifyable()) {
			if (transitions.length == 0)
				return this;
		
			boolean isChanged = false;
			PrimerTransition res = this;
			// next line commented out, because we do not use  b
			//ArrayList<UnificationBinding> b = uni_res.getBindings();
			Vector<Transition<?>> resTransitions = new Vector<Transition<?>>();
			Set<State> dstStates = new HashSet<State>();
			for (Transition<?> st: transitions) {
				// TODO get rid of cast in next line
				Transition<?> st_inst = st.instantiate(i);
				if (st_inst==null || st_inst!=st)
					isChanged = true;
				if (st_inst!=null) {
					resTransitions.add(st_inst);
					if (st_inst.getDestination() != null)
						dstStates.add(st_inst.getDestination());
				}
			}
			if(debug) System.out.println("PrimerTransition.instantiate: result size="+resTransitions.size());
			if (resTransitions.size() <= 0) {
				System.out.println("PrimerTransition.instantiate: no results");
				return null;
			}
			if (isChanged && src!=null && src instanceof PrimerState) {
				if(debug) System.out.println("PrimerTransition.instantiate: yes results, constructing result transition");
				// construct new transition. we just computed its subtransitions;
				// we still have to compute its destination state
				PrimerState ps_src = (PrimerState) src;
				PrimerState resDst = ps_src.getFactory().mkInitState(dstStates);
				res = new PrimerTransition(src, i_l, resDst, resTransitions);
				if(debug) System.out.println("PrimerTransition.instantiate: " +
						"" +
						"" +
						"" +
						"" +
						"New Primertransition: "+src.toString()+" -- "+i_l.getString()+" --> "+resDst.toString());
			} else if (isChanged)
				System.err.println("PrimerTransition.instantiate: internal error: cannot create dst state: src==null or src not instanceof PrimerState");
			return res;
		}
		return null;
	}
	
	// return (arbitrary decision) solution for first transition
	public Label getSolution() {
		for (Transition<?> st: transitions) {
			return st.getSolution();
		}
		return null;
	}



	// TODO take variables and constraints into account!!!

	public Vector<Position> getPosition() {
		return null;
	}
	
	public Position getLabelPosition() {
		return null;
	}
	public static String mkEdgeName(Transition<?>[] transList) {
		Vector<String>names = new Vector<String>();
		boolean needsSorting = false;
		for(Transition<?> t: transList) {
			String l = t.getEdgeName();
			if (l != null && !l.trim().equals("")) {
				names.add(l);
				needsSorting = true;
			} else
				names.add("_");
		}
		if (needsSorting)
			Collections.sort(names, Strings.getNaturalComparator());

		String s = "";
		s += "{ ";
		boolean first = true;
		for(String l: names ) {
			if (first)
				first = false;
			else
				s += " ";
			s += l;
		}
		s += " }";
		return s;
	}
	public String getEdgeName() {
		return mkEdgeName(transitions);
	}

	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		//              if((o == null) || (o.getClass() != this.getClass()))
		//                      return false;
		if (o instanceof PrimerTransition) {
			PrimerTransition s = (PrimerTransition) o;
			return (s.src.equals(src) &&
					s.lbl.equals(lbl) &&
					s.dst.equals(dst));
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == src ? 0 : src.hashCode());
		hash = 31 * hash + (null == lbl ? 0 : lbl.hashCode());
		hash = 31 * hash + (null == dst ? 0 : dst.hashCode());
		// TODO take variables and constraints into account!!!
		return hash;
	}

	
	public PrimerTransition(CompoundState s, Label l, CompoundState d) {
		src = s;
		lbl = l;
		dst = d;
	}
	public PrimerTransition(CompoundState s, Label l, CompoundState d, Collection<Transition<? extends State>> allTransitions) {
		src = s;
		lbl = l;
		dst = d;
		transitions = allTransitions.toArray(transitions);
	}
	

	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return new Iterator<Transition<? extends State>>() {
			public boolean hasNext() {
				return i < transitions.length;
			}
			public Transition<? extends State> next() {
				if (i< transitions.length) {
					i++;
					return transitions[i-1];
				}
				return null;
			}
			public void remove() {
				throw new UnsupportedOperationException("PrimerState.getSubTransitions.iterator.remove unimplemented"); 
			}
			private int i = 0;
		};
	}

	private CompoundState src;
	private Label lbl;
	private CompoundState dst;
	
	private Transition<?>[] transitions = new Transition[0];
	
	private boolean debug=false;
}
