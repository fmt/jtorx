package utwente.fmt.jtorx.torx.primer;

import java.util.Vector;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LabelConstraint;

public class PrimerTransitionIterator implements java.util.Iterator<CompoundTransition<CompoundState>> {

	public boolean hasNext() {
		return (idx < transitions.size());
	}

	public CompoundTransition<CompoundState> next() {
		if (idx < transitions.size()) {
			CompoundTransition<CompoundState> result = transitions.elementAt(idx);
			idx++;
			skipNonMatching();

			return result;
		}
		return null;
	}
	
	public PrimerTransitionIterator(Vector<PrimerTransition> t, LabelConstraint lc) {
		transitions = t;
		constraint = lc;
		idx = 0;
		skipNonMatching();
	}
	
	private void skipNonMatching() {
		if (constraint != null && transitions != null)
			while(idx < transitions.size() && !constraint.isSatisfiedBy(transitions.elementAt(idx).getLabel()))
				idx++;
	}
	
	private Vector<PrimerTransition> transitions;
	private LabelConstraint constraint;
	private int idx;
	public void remove() {
		throw new UnsupportedOperationException("PrimerTransitionIterator.remove unimplemented");
	}
}
