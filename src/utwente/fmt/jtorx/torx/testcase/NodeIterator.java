package utwente.fmt.jtorx.torx.testcase;

import java.util.HashSet;
import java.util.Iterator;

import utwente.fmt.test.Node;

public class NodeIterator implements java.util.Iterator<Node> {

	public boolean hasNext() {
		return it.hasNext();
	}

	public Node next() {
		return it.next();
	}
	
	public NodeIterator(HashSet<Node> curOpenLeaves) {
		it = curOpenLeaves.iterator();
	}

	Iterator<Node> it;

	public void remove() {
		throw new UnsupportedOperationException("NodeIterator.remove unimplemented");
	}
}
