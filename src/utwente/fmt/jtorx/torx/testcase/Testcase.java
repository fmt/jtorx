package utwente.fmt.jtorx.torx.testcase;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.test.Node;


public class Testcase implements utwente.fmt.test.Testcase,utwente.fmt.lts.LTS {

	// Testcase methods:
	public Iterator<? extends Node> getOpenLeaves() {
		return new NodeIterator(curOpenLeaves);
	}
	public LTS getLTS() {
		return this;
	}

	// LTS methods:
	public Iterator<? extends State> init() {
		return new TestcaseStateIterator(root);
	}
	public void done() {
		// TODO Auto-generated method stub	
	}
	
	public void cleanupPreserving(Collection<?extends State> s) {
	}
	
	// TODO probably we know more than we say here
	public long getNrOfStates() {
		return -1;
	}
	
	// TODO probably we know more than we say here
	public long getNrOfTransitions() {
		return -1;
	}

	public boolean hasPosition() {
		return false;
	}
		
	public Position getRootPosition() {
		return null;
	}
	
	public void reportLeafClosed(TestcaseNode n) {
		System.out.println("\t\tleafclosed: "+n.getID());
		if (closedLeaves != null)
			closedLeaves.add(n);
	}
	public void reportOpenLeaf(Node n) {
		System.out.println("\tleafopen: "+n.getID());
		if (n.isLeaf() && (closedLeaves == null || !closedLeaves.contains(n)))
			addOpenLeaves.add(n);
	}
	
	public void startAddLevel() {
		for (Iterator<Node> it = curOpenLeaves.iterator(); it.hasNext(); )
			System.out.println("- "+it.next().getID());
		addOpenLeaves = new HashSet<Node>();
		closedLeaves = new HashSet<Node>();
	}
	private String pr(Node node) {
		return node.getID()+" "+node.toString();
	}
	public void doneAddLevel() {
		for (Iterator<Node> it = curOpenLeaves.iterator(); it.hasNext(); ){
			System.out.println(". "+pr(it.next()));
		}
		for (Iterator<Node> it = addOpenLeaves.iterator(); it.hasNext(); )
			System.out.println("+ "+pr(it.next()));
		for (Iterator<Node> it = addOpenLeaves.iterator(); it.hasNext(); ){
			Node t = it.next();
			if (!curOpenLeaves.contains(t))
				curOpenLeaves.add(t);
		}
		// curOpenLeaves.addAll(newOpenLeaves);
		for (Iterator<Node> it = curOpenLeaves.iterator(); it.hasNext(); )
			System.out.println("= "+pr(it.next()));
		for (Iterator<Node> it = closedLeaves.iterator(); it.hasNext(); )
			System.out.println("- "+pr(it.next()));
		curOpenLeaves.removeAll(closedLeaves);
		for (Iterator<Node> it = curOpenLeaves.iterator(); it.hasNext(); )
			System.out.println("= "+pr(it.next()));
		closedLeaves = null;
		//addOpenLeaves = null;
	}

	public Testcase(CompoundState s) {
		System.out.println("tc  s="+s.getID());
		root = mkTestcaseNode(s);
	}
	
	public Node mkTestcaseNode(CompoundState s) {
		return mkTestcaseNode(s, null);
	}
	public Node mkTestcaseNode(CompoundState s, Node sn) {
		return new TestcaseNode(this, s, false);
/*		
		if (sn != null && sn.getState().equals(s)) {
			System.out.println("mktcn 2 s="+s.getID());
			return new TestcaseNode(this, s);
		}
		TestcaseNode n = states.get(s);
		if (n != null)
			return n;
		System.out.println("mktcn 2-2 s="+s.getID());
		n = new TestcaseNode(this, s);
		states.put(s,n);
		return n;
*/
	}
	public Node mkTestcaseNode(CompoundState s, Node sn, Boolean q) {
		return new TestcaseNode(this, s, q);
	}
	
	private Node root;
	private HashSet<Node> curOpenLeaves = new HashSet<Node>();
	private HashSet<Node> addOpenLeaves = curOpenLeaves;
	private HashSet<Node> closedLeaves;
}
