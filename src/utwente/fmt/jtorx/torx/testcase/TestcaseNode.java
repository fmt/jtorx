package utwente.fmt.jtorx.torx.testcase;

import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

import utwente.fmt.test.Node;
import utwente.fmt.test.Verdict;


public class TestcaseNode implements utwente.fmt.test.Node, CompoundState {

	
	// Node (testcase) methods:
	public void extend(utwente.fmt.test.Testcase t) {
		// TODO Auto-generated method stub
		isLeaf = false;
		isOpen = false;
		testcase.reportLeafClosed(this);
	}

	public void add (CompoundTransition<? extends Node> t){
		isLeaf = false;
		isOpen = false;
		testcase.reportLeafClosed(this);
		trans.add(t);
		if (t.getDestination().isOpen())
			testcase.reportOpenLeaf(t.getDestination());
	}
	public CompoundState getState() {
		return state;
	}
	public Boolean isReachedByQuiescence() {
		return reachedByQuiescence;
	}
	public Verdict getVerdict() {
		// TODO Auto-generated method stub
		return null;
	}
	public Boolean isLeaf() {
		return isLeaf;
	}
	public Boolean isOpen() {
		return isOpen;
	}

	// both Node and State
	
	public Iterator<? extends utwente.fmt.lts.CompoundTransition<? extends CompoundState>> menu(LabelConstraint l) {
		return new TransitionIterator(trans, l);
	}

	// State methods
	
	// TODO take care of symbolic stuff?
	public CompoundTransition<? extends CompoundState> next(Label l) {
		if (trans==null || trans.size()==0)
			return null;
		for (CompoundTransition<? extends CompoundState> t: trans)
			if (t.getLabel().eq(l))
				return t;
		return null;
	}

	
	public String getID() {
		return "nrb "+nr+" "+state.getID();
	}
	
	// TODO check whether we need to do more
	public String getLabel(boolean quote) {
		return state.getLabel(quote);
	}
	
	public Position getPosition() {
		return null;
	}
	public String getNodeName() {
		return null;
	}

	public TestcaseNode getCanonical() {
		return this;
	}


	public CompoundLTS getLTS() {
		// TODO get rid of cast
		return (CompoundLTS)testcase;
	}
/*
	public int hashCode() {
		return state.hashCode() +testcase.hashCode()%31;
	}
	//@Override
	public boolean myequals(Object o) {
		if (o instanceof TestcaseNode) {
			TestcaseNode s = (TestcaseNode) o;
			System.out.println("tcn eq "+o.toString()+"    "+toString()+"  =  "+(s.getID().equals(getID()) ));
			return (s.testcase.equals(testcase) &&
        		 (s.state.equals(state) ));
		}
		return false;
	}
*/
	
	public TestcaseNode(Testcase t, CompoundState s, Boolean q) {
		System.out.println("tcn s="+s.getID());
		testcase = t;
		state = s;
		reachedByQuiescence = q;
		testcase.reportOpenLeaf(this);
		nr = nextnr++;
	}
	
	public void disposeTransitions() {
	}

	
	private Boolean isOpen = true;
	private Boolean isLeaf = true;
	private Vector<CompoundTransition<? extends Node>> trans = new Vector<CompoundTransition<? extends Node>>();
	private CompoundState state;
	private Testcase testcase;
	private Boolean reachedByQuiescence;
	private int nr;
	private static int nextnr = 0;
	public void add(Transition<? extends Node> t) {
		// TODO Auto-generated method stub
		
	}

	public Iterator<? extends State> getSubStates() {
		return state.getSubStates();
	}
	public Iterator<? extends State> getSubStates(LTS l) {
		return getSubStates();
	}


	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		return state.getSubTransitions();
	}
	
	public String getSubEdgeName() {
		return state.getSubEdgeName();
	}

}
