package utwente.fmt.jtorx.torx.testcase;

import java.util.Vector;

import utwente.fmt.lts.State;
import utwente.fmt.test.Node;

public class TestcaseStateIterator implements java.util.Iterator<State> {

	public boolean hasNext() {
		return idx < states.size();
	}

	public State next() {
		if (idx < states.size()) {
			return states.get(idx++);
		}
		return null;
	}
	
	
	public TestcaseStateIterator () {
		states = new Vector<Node>();
	}
	
	public TestcaseStateIterator (Node s) {
		this();
		states.add(s);
	}

	
	private Vector<Node> states;
	int idx;
	public void remove() {
		throw new UnsupportedOperationException("TestcaseStateIterator.remove unimplemented");
	}

}
