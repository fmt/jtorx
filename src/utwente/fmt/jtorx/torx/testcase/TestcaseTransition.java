package utwente.fmt.jtorx.torx.testcase;

import java.util.Vector;

import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.test.Node;


public class TestcaseTransition implements utwente.fmt.lts.Transition<Node> {

	public Node getDestination() {
		return dst;
	}

	public Label getLabel() {
		return lbl;
	}

	public Node getSource() {
		return src;
	}
	
	public TestcaseTransition instantiate(Instantiation i) {
		return this;
	}

	public Label getSolution() {
		return null;
	}


	// TODO add eq and hashCode methods??
	// TODO take variables and constraints into account in these methods? !!!


	public Vector<Position> getPosition() {
		return null;
	}
	
	public Position getLabelPosition() {
		return null;
	}
	public String getEdgeName() {
		return null;
	}

	
	public TestcaseTransition(Node s, Label l, Node d) {
		src = s;
		lbl = l;
		dst = d;
	}
	
	private Node src;
	private Node dst;
	private Label lbl;


}
