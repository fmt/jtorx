package utwente.fmt.jtorx.torx.testcase;

import java.util.Vector;

import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.test.Node;


public class TransitionIterator implements java.util.Iterator<CompoundTransition<? extends Node>> {

	public boolean hasNext() {
		return (idx < transitions.size());
	}

	public CompoundTransition<?extends Node> next() {
		if (idx < transitions.size()) {
			CompoundTransition<? extends Node> result = transitions.elementAt(idx);
			idx++;
			skipNonMatching();

			return result;
		}
		return null;
	}
	
	public TransitionIterator(Vector<CompoundTransition<? extends Node>> trans, LabelConstraint lc) {
		transitions = trans;
		constraint = lc;
		idx = 0;
		skipNonMatching();
	}
	
	private void skipNonMatching() {
		if (constraint != null && transitions != null)
			while(idx < transitions.size() && !constraint.isSatisfiedBy(transitions.elementAt(idx).getLabel()))
				idx++;
	}
	
	private Vector<CompoundTransition<? extends Node>> transitions;
	private LabelConstraint constraint;
	private int idx;
	public void remove() {
		throw new UnsupportedOperationException("TransitionIterator.remove unimplemented");
	}
}
