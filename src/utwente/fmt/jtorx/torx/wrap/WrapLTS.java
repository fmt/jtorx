package utwente.fmt.jtorx.torx.wrap;

import java.util.Collection;
import java.util.Iterator;

import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.test.Verdict;

public class WrapLTS implements CompoundLTS {

	public void done() {
		lts.done();
	}
	
	public Verdict getNegativeDefaultVerdict() {
		return null;
	}

	public Verdict getPositiveDefaultVerdict() {
		return null;
	}
	
	public void cleanupPreserving(Collection<?extends State> s) {
	}


	public Iterator<? extends CompoundState> init() throws LTSException {
		if(debug)
			System.err.println("WrapLTS init this.sublts="+lts+" ...");
		if (lts==null)
			return null;
		Iterator<? extends State> lts_it = lts.init();
		if(debug)
			System.err.println("WrapLTS init this.sublts="+lts+" lts_it="+lts_it);
		if (lts_it==null)
			return null;
		WrapStateIterator it = new WrapStateIterator();
		if(debug)
			System.err.println("WrapLTS init this.sublts="+lts+" it="+it);
		while(lts_it.hasNext()) {
			if(debug)
				System.err.println("WrapLTS init adding state this.sublts="+lts+" it="+it);
			it.add(new WrapState(this, lts_it.next()));
		}
		if(debug)
			System.err.println("WrapLTS init this.sublts="+lts+ "...done");
		return it;

	}
	
	public long getNrOfStates() {
		return lts.getNrOfStates();
	}

	public long getNrOfTransitions() {
		return lts.getNrOfTransitions();
	}

	public boolean hasPosition() {
		return lts.hasPosition();
	}

	public Position getRootPosition() {
		return lts.getRootPosition();
	}
	
	public AnyInterpretation getInterpretation() {
		return interp;
	}
	
	public LTS getSubLTS() {
		return lts;
	}
	
	public WrapLTS(LTS l) {
		if(debug)
			System.err.println("WrapLTS this.sublts="+lts);
		lts = l;
	}
	

	private LTS lts;
	private AnyInterpretation interp = new AnyInterpretation();
	private boolean debug = false;
}
