package utwente.fmt.jtorx.torx.wrap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.LabelConstraint;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.test.Verdict;

public class WrapState implements utwente.fmt.lts.CompoundState {
	public WrapLTS getLTS() {
		return theLTS;
	}
	
	public String getID() {
		if(debug)
			System.err.println("WrapState getID this"+this+" state="+state.getID()+" "+state.getLabel(false));
		return state.getID();
	}
	
	public String getLabel(boolean quote) {
		if(debug)
			System.err.println("WrapState getLabel this"+this+" state="+state.getID()+" "+state.getLabel(false));
		return state.getLabel(quote);
	}
	
	public Position getPosition() {
		return state.getPosition();
	}
	public String getNodeName() {
		return state.getNodeName();
	}
		
	public WrapState getCanonical() {
		if(debug)
			System.err.println("WrapState getCanonical this"+this+" state="+state.getID()+" "+state.getLabel(false));
		return this;
	}


	public Iterator<? extends CompoundTransition<? extends CompoundState>> menu(LabelConstraint c) {
		if(debug)
			System.err.println("WrapState menu this"+this+" state="+state.getID()+" "+state.getLabel(false));
		if (state==null)
			return null;
		if (succ == null) {
			succ = new Vector<CompoundTransition<CompoundState>>();
			Iterator<? extends Transition<? extends State>> state_it = state.menu(theLTS.getInterpretation().isAny());
			if (state_it!=null)
				while(state_it.hasNext()) {
					Transition<?extends State> t = state_it.next();
					WrapState ws = new WrapState(theLTS, t.getDestination());
					CompoundTransition<CompoundState> wt = new WrapTransition(this, ws, t);
					succ.add(wt);
				}
		}
			
		return new WrapTransitionIterator<CompoundState>(succ, c);
	}
	
	// TODO take care of symbolic stuff?
	public CompoundTransition<? extends CompoundState> next(Label l) {
		if (succ==null || succ.size()==0)
			return null;
		for (CompoundTransition<? extends CompoundState> t: succ)
			if (t.getLabel().eq(l))
				return t;
		return null;
	}
	
	public Iterator<? extends State> getSubStates() {
		if(debug)
			System.err.println("WrapState getSubStates this.lts="+theLTS+" "+this+" "+getID()+" "+getLabel(false));
		ArrayList<State> al = new ArrayList<State>();
		al.add(state);
		if(debug)
			System.err.println("\tWrapState getSubStates returning 1 state: this.lts="+theLTS+" state="+state.getID()+" "+state.getLabel(false));
		return al.iterator();

	}

	public Iterator<? extends State> getSubStates(LTS l) {
		if(debug)
			System.out.println("WrapState getSubStates("+l+") this.lts="+theLTS+" "+this+" "+getID()+" "+getLabel(false));
		if (l==null || l.equals(theLTS))
			return getSubStates();
		return new Iterator<State>() {
			public boolean hasNext() {
				return false;
			}
			public State next() {
				return null;
			}
			public void remove() {
				throw new UnsupportedOperationException("WrapState.getSubTransitions.remove unimplemented");
			}
		};

	}

	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		if(debug)
			System.out.println("WrapState getSubTransitions this.lts="+theLTS+" "+this+" "+getID()+" "+getLabel(false));
		return new Iterator<Transition<? extends State>>() {
			public boolean hasNext() {
				return false;
			}
			public Transition<?extends State> next() {
				return null;
			}
			public void remove() {
				throw new UnsupportedOperationException("WrapState.getSubTransitions.remove unimplemented");
			}
		};
	}
	
	public String getSubEdgeName() {
		return null;
	}

	public Verdict getVerdict() {
		if(debug)
			System.out.println("WrapState getVerdict this.sublts="+theLTS.getSubLTS());
		return null;
	}
	


	@Override
    public boolean equals(Object o) {
		if (o instanceof WrapState) {
			WrapState s = (WrapState) o;
			return (s.theLTS.equals(theLTS) && s.state.equals(state));
		}
        if (o instanceof State) {
        	State s = (State) o;
        	return (state.equals(s));
        }
        return false;
    }
	
	@Override
	public int hashCode() {
		return state.hashCode();
	}
	
	public void disposeTransitions() {
		
	}

	public WrapState (WrapLTS lts, State s) {
		if(debug)
			System.out.println("WrapState getVerdict this.sublts="+lts.getSubLTS()+" state="+s.getID()+" "+s.getLabel(false));
		theLTS = lts;
		state = s;
	}
	
	private WrapLTS theLTS;
	private State state;
	private Vector<CompoundTransition<CompoundState>> succ = null;
	private boolean debug = false;

}
