package utwente.fmt.jtorx.torx.wrap;

import java.util.Vector;

import utwente.fmt.lts.CompoundState;

public class WrapStateIterator implements java.util.Iterator<CompoundState> {

	public boolean hasNext() {
		if(debug)
			System.err.println("WrapStateIterator "+this+" hasnext ");
		return (idx < states.size());
	}

	public WrapState next() {
		if(debug)
			System.err.println("WrapStateIterator "+this+" next ");
		if (idx < states.size()) {
			return states.get(idx++);
		}
		return null;
	}
	
	public void add(WrapState s) {
		if(debug)
			System.err.println("WrapStateIterator "+this+" add "+s);
		states.add(s);
	}
	
	public WrapStateIterator() {
		states = new Vector<WrapState>();
		idx = 0;
		
	}

	public void remove() {
		throw new UnsupportedOperationException("TraceWrapStateIterator.remove unimplemented");
	}

	private Vector<WrapState> states;
	private int idx;
	private boolean debug = false;
}
