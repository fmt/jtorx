package utwente.fmt.jtorx.torx.wrap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.Instantiation;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class WrapTransition implements utwente.fmt.lts.CompoundTransition<CompoundState> {

	public CompoundState getDestination() {
		return dst;
	}

	public Label getLabel() {
		return trans.getLabel();
	}

	public CompoundState getSource() {
		return src;
	}
	
	public Vector<Position> getPosition() {
		return trans.getPosition();
	}
	
	public Position getLabelPosition() {
		return trans.getLabelPosition();
	}
	public String getEdgeName() {
		return trans.getEdgeName();
	}
	
	public Iterator<? extends Transition<? extends State>> getSubTransitions() {
		ArrayList<Transition<?extends State>> al = new ArrayList<Transition<?extends State>>();
		al.add(trans);
		return al.iterator();
	}
	
	public WrapTransition instantiate(Instantiation i) {
		Transition<?extends State> t = trans.instantiate(i);
		if (t==null)
			return null;
		WrapState ws = new WrapState(src.getLTS(), t.getDestination());
		WrapTransition wt = new WrapTransition(src, ws, t);
		return wt;
	}

	public Label getSolution() {
		return trans.getSolution();
	}


	
	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		//              if((o == null) || (o.getClass() != this.getClass()))
		//                      return false;
		if (o instanceof WrapTransition) {
			WrapTransition s = (WrapTransition) o;
			return (s.src.equals(src) &&
					s.trans.getLabel().equals(trans.getLabel()) &&
					s.dst.equals(dst));
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == src ? 0 : src.hashCode());
		hash = 31 * hash + (null == trans.getLabel() ? 0 : trans.getLabel().hashCode());
		hash = 31 * hash + (null == dst ? 0 : dst.hashCode());
		return hash;
	}

	public WrapTransition(WrapState s, WrapState d, Transition<?extends State> t) {
		src = s;
		dst = d;
		trans = t;
	}

	private WrapState src;
	private WrapState dst;
	private Transition<?extends State> trans;

}
