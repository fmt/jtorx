package utwente.fmt.jtorx.torx.wrap;

import java.util.Vector;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LabelConstraint;

public class WrapTransitionIterator<T extends CompoundState> implements java.util.Iterator<CompoundTransition<? extends CompoundState>> {

	public boolean hasNext() {
		return (idx < transitions.size());
	}

	public CompoundTransition<? extends CompoundState> next() {
		if (idx < transitions.size()) {
			CompoundTransition<T> result = transitions.elementAt(idx);
			idx++;
			skipNonMatching();

			return result;
		}
		return null;
	}

	public WrapTransitionIterator(Vector<CompoundTransition<T>> t, LabelConstraint c) {
		transitions = t;
		constraint = c;
		idx = 0;
		skipNonMatching();
	}
	
	private void skipNonMatching() {
		if (constraint != null && transitions != null)
			while(idx < transitions.size() && !constraint.isSatisfiedBy(transitions.elementAt(idx).getLabel()))
				idx++;
	}
	
	private Vector<CompoundTransition<T>> transitions;
	private LabelConstraint constraint;
	private int idx;
	
	public void remove() {
		throw new UnsupportedOperationException("LibTransitionIterator.remove unimplemented");
	}

}
