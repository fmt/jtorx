package utwente.fmt.jtorx.ui;

public interface ErrorReporter {
	void report(String s);
	void log(long timeStamp, String src, String msg);
	void endOfTest(String s);
}
