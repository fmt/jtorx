package utwente.fmt.jtorx.ui;

public interface FailureReporter {
	void notRunning();
	void isRunning();
}
