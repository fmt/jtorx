package utwente.fmt.jtorx.ui;

public interface LineHandler {
	public void handle(String s);
}
