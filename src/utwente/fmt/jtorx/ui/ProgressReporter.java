package utwente.fmt.jtorx.ui;

public interface ProgressReporter {
	void reset();
	void ready();
	void stop();
	void setActivity(String s);
	void setMax(int m);
	void setLevel(int l);
	void startUndeterminate();
	void stopUndeterminate();
}
