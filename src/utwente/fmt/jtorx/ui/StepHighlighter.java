package utwente.fmt.jtorx.ui;

public interface StepHighlighter {
	public void highlight(String s);
}
