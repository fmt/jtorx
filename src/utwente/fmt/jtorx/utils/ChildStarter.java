package utwente.fmt.jtorx.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.exec.ExecuteStreamHandler;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;
import org.apache.commons.exec.environment.EnvironmentUtil;

import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;

public class ChildStarter {
	
	class IOinterface {
		public OutputStream in;
		public Reader out;
		public Reader err;

	}
	
	class StreamReader extends Thread {
		private Reader is;
		final int ARRAYSIZE=1024;
		private String buf = "";
		private String name;
 
		StreamReader(String n, Reader err) {
			this.is = err;
			this.name = n;
		}
 
		public void run() {
			try {
				int n;
				char[] cbuf = new char[ARRAYSIZE];
				while (true /*err.ready()*/){
					n  = is.read(cbuf, 0, ARRAYSIZE);
					if (n <= 0)
						return;
					buf += new String(cbuf,0,n);
					int i = buf.indexOf("\n");
					while (i >= 0) {
						subprocessStderrln(buf.substring(0,i).replaceAll("\r", "").trim());
						buf = buf.substring(i+1);
						i = buf.indexOf("\n");
					}
				}
			} catch (IOException e) {
				System.err.println(name+" child-starter caught streamreader io exception: "+e.getMessage());
				// e.printStackTrace();
				try {
					is.close();
				} catch (IOException e1) {
					System.err.println(name+" child-starter caught streamreader close io exception: "+e1.getMessage());
					// e1.printStackTrace();
				}

			}
		}
	}

	class ResultHandler implements ExecuteResultHandler {

		public void onProcessComplete(int exitValue) {
			// TODO Auto-generated method stub
			System.err.println(name+" child-starter "+parent.toString()+" onProcessComplete: "+exitValue);
			instances.remove(parent);
			running = false;
			if (activatedDestroyer !=null)
				activatedDestroyer.interrupt();
			errorReporter.endOfTest(name+" child-starter exits: "+exitValue);
		}

		public void onProcessFailed(ExecuteException e) {
			System.err.println(name+" child-starter "+parent.toString()+" onProcessFailed: "+e.getMessage());
			running = false;
			instances.remove(parent);
			if (activatedDestroyer !=null)
				activatedDestroyer.interrupt();
			errorReporter.endOfTest(name+" child-starter exits with failure"+expectedFailureMsg+": "+e.getMessage());
			System.err.println(name+" child-starter "+parent.toString()+" reported onProcessFailed error");

		}
		
		public ResultHandler(String n, Object o) {
			parent = o;
			name = n;
		}
		
		private Object parent;
		private String name;

	}
	class StreamHandler implements ExecuteStreamHandler {

		public void setProcessErrorStream(InputStream is) throws IOException {
			err = new BufferedReader(new InputStreamReader(is));
		}
		public void setProcessInputStream(OutputStream os) throws IOException {
			in = os;
		}
		public void setProcessOutputStream(InputStream is) throws IOException {
			out = new BufferedReader(new InputStreamReader(is));
		}
		public void start() throws IOException {
			System.err.println(name+" child-starter StreamHandler "+parent.toString()+" start in="+in.toString()+" out="+out.toString()+" err="+err.toString());
			StreamReader reader = new StreamReader(name, err);
			reader.start();
			IOinterface io = new IOinterface();
			io.in = in;
			io.out = out;
			io.err = err;
			try {
				ioInterface.put(io);
			} catch (InterruptedException e) {
				System.err.println(name+" child-starter StreamHandler start: error ioInterface.put "+parent.toString()+"  in="+in.toString()+" out="+out.toString()+" err="+err.toString());
				e.printStackTrace();
			}
		}
		public void stop() {
			System.err.println(name+" child-starter StreamHandler "+parent.toString()+" stop");
		}
		public StreamHandler(String n, Object o) {
			parent = o;
			name = n;
		}
		
		private Object parent;
		String name;

		// start a separate thread to consume the error stream,
		// to avoid that the explorer program gets blocked while writing its standard error,
		// http://forums.sun.com/thread.jspa?threadID=676129&messageID=9744480
	}
	
	// brute force kill process
	public void destroy(final long delay) {
		activatedDestroyer = new Thread() {
			public void run() {
				try {
					Thread.sleep(delay);
					if (isRunning()) {
						errorReporter.report("ChildStarter: killing program (expect failure result)");
						expectedFailureMsg = " (not unexpectedly, because we had to kill the process)";
						destroyer.run();
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					System.err.println("ChildStarter: in done: activatedDestroyer was interrupted: ");
					e.printStackTrace();
				}
				// TODO register this with ChildStart, and let it interrupt us when Child dies
			}
		};
		activatedDestroyer.start();
	}
	
	public void run() {
		System.err.println(name+" child-starter "+this.toString()+" run start");
		if (dir!=null)
			exec.setWorkingDirectory(dir);
		resultHandler = new ResultHandler(name, this);
		streamHandler = new StreamHandler(name, this);
		exec.setStreamHandler(streamHandler);
		try {
			System.err.println(name+" child-starter about to start child");
			exec.execute(cmd, envMap, resultHandler);
			System.err.println(name+" child-starter started child");
			running = true;
			IOinterface io = ioInterface.poll(20000L, TimeUnit.MILLISECONDS);
			if (io != null) {
				in = io.in;
				out = io.out;
				err = io.err;
				System.err.println(name+" child-starter got io interface to child");
				instances.add(this);
			} else {
				running = false;
				System.err.println(name+" child-starter failed to get io interface to child");
			}
		} catch (ExecuteException e) {
			System.err.println(name+" child-starter execute exception: "+e.getMessage());
			e.printStackTrace();
			errorReporter.report(name+" child-starter error starting child: "+e.getMessage());
		} catch (IOException e) {
			System.err.println(name+" child-starter error starting child: io exception: "+e.getMessage());
			e.printStackTrace();
			errorReporter.report(name+" child-starter error starting child: io exception: "+e.getMessage());
		} catch (InterruptedException e) {
		}
		System.err.println(name+" child-starter "+this.toString()+" run end");

	}

	public ChildStarter(String n, ErrorReporter r, String[] command, String dirName, ProgressReporter p, Appendable err) /*throws IOException*/ {
		this(n, r, command, dirName, p, err, null);
	}
	public ChildStarter(String n, ErrorReporter r, String[] command, String dirName, ProgressReporter p, Appendable err, String[] envVars) /*throws IOException*/ {
		this(n, r, dirName, p, err, envVars);
		cmd = new CommandLine(command[0]);
		for (int i=1; i< command.length; i++)
			cmd.addArgument(command[i]);
	}

	public ChildStarter(String n, ErrorReporter r, String command, String dirName, ProgressReporter p, Appendable err) /*throws IOException*/ {
		this(n, r, command, dirName, p, err, null);
	}
	public ChildStarter(String n, ErrorReporter r, String command, String dirName, ProgressReporter p, Appendable err, String[] envVars) /*throws IOException*/ {
		this(n, r, dirName, p, err, envVars);
		cmd = CommandLine.parse(command);
	}
	
	public ChildStarter(String n, ErrorReporter r, CommandLine command, String dirName, ProgressReporter p, Appendable err) /*throws IOException*/ {
		this(n, r, command, dirName, p, err, null);
	}
	public ChildStarter(String n, ErrorReporter r, CommandLine command, String dirName, ProgressReporter p, Appendable err, String[] envVars) /*throws IOException*/ {
		this(n, r, dirName, p, err, envVars);
		cmd = command;
	}
	
	public ChildStarter(String n, ErrorReporter r, String dirName, ProgressReporter p, Appendable err, String[] envVars) /*throws IOException*/ {
		
		System.err.println("ChildStarter  "+this.toString()+" constructor begin");
		
		errorReporter = r;
		stderrReporter = err;

		name = n;
		
		// should check that this succeeds
		if (dirName!=null)
			dir = new File( dirName );
	
		if (dir == null) 
			System.out.println("oops: dir==null");
		else
			System.out.println("dir="+dirName);
		
		exec = new DefaultExecutor();
		destroyer = new ShutdownHookProcessDestroyer();
		exec.setProcessDestroyer(destroyer);
		

		if (envVars!=null) {
			try {
				envMap = EnvironmentUtil.getProcEnvironment();
				for (String s: envVars)
					EnvironmentUtil.addVariableToEnvironment(envMap, s);
			} catch (IOException e) {
				errorReporter.report(name+" child-starter could not get  environt: "+e.getMessage());
			}
		}
		
		System.err.println("ChildStarter  "+this.toString()+" constructor end");
	}

	
	public OutputStream getIn() {
		return in;
	}
	public Reader getOut() {
		return out;
	}
	public Reader getErr() {
		return err;
	}
	public boolean isRunning() {
		return running;
	}
	
	public static String extractEventNr(String s) {
		int i = s.length() - 1;
		while(i >= 0 && s.charAt(i) != '.')
			i--;
		if (i>0)
			return s.substring(i+1, s.length());
		return s;
	}
	
	public void subprocessStderrln(String s) {
		if (showSubProcessStderr)
			report(name+" suberr: "+s);
		if (stderrReporter!=null)
			try {
				stderrReporter.append(name+" suberr: "+s);
			} catch (IOException e) {
				errorReporter.report(name+": "+"failed to report stderr line: "+e.getMessage());
				//e.printStackTrace();
			}
		errorReporter.report(name+": "+s);
	}
	
	private void report(String s) {
		long now = System.currentTimeMillis();
		System.out.println(format.format(now)+" "+s);		
	}


	static public void cleanup() {
		for (ChildStarter t: instances) {
			if (t!=null) {
				System.err.println("ChildStarter cleanup: \""+t.name+"\" "+t.toString());
				if (t.out!=null) {
					try {
						t.out.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						System.err.println("ChildStarter cleanup:: \""+t.name+"\" "+t.toString()+" caught excption: "+e.getMessage());
					}
				}
				t.destroy(10);
			}
		}
	}

	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	private Executor exec;
	private ShutdownHookProcessDestroyer destroyer;
	private Thread activatedDestroyer = null;
	private File dir = null;
	private CommandLine  cmd;
	private ErrorReporter errorReporter;
	private String name;
	private String expectedFailureMsg = "";
	private Map<String,String> envMap = null;
	
	private OutputStream in = null;
	private Reader out = null;
	private Reader err = null;

	private boolean running = false;

	private Boolean showSubProcessStderr = true;


	private ExecuteResultHandler resultHandler = null;
	private ExecuteStreamHandler streamHandler = null;
	
	private BlockingQueue<IOinterface> ioInterface =
	    new ArrayBlockingQueue<IOinterface>(1);

	private Appendable stderrReporter = null;
	
	
	static private Vector<ChildStarter> instances = new Vector<ChildStarter>();
	
	

}
