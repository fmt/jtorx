package utwente.fmt.jtorx.utils;

import java.util.Iterator;

public class EmptyIterator<E> {
	static class TheEmptyIterator<E> implements Iterator<E> {

		public boolean hasNext() {
			return false;
		}

		public E next() {
			throw new UnsupportedOperationException("EmptyIterator.iterator.next unimplemented");
		}

		public void remove() {
			throw new UnsupportedOperationException("EmptyIterator.iterator.remove unimplemented");
		}
	}
	

	@SuppressWarnings("unchecked")
	public static final <T> Iterator<T> iterator() {
		return emptyIterator;
	}
	
	@SuppressWarnings("unchecked")
	static Iterator emptyIterator = new TheEmptyIterator<Object>();

}
