package utwente.fmt.jtorx.utils;

// taken from http://stackoverflow.com/questions/521171/a-java-collection-of-value-pairs-tuples
// answered Feb 6 '09 at 17:24  Paul Brinkley

public class Pair<L,R> {

	  private final L left1;
	  private final R right;

	  public Pair(L left, R right) {
	    this.left1 = left;
	    this.right = right;
	  }

	  public L getLeft() { return left1; }
	  public R getRight() { return right; }

	  public int hashCode() { return left1.hashCode() ^ right.hashCode(); }

	  public boolean equals(Object o) {
	    if (o == null) return false;
	    if (!(o instanceof Pair)) return false;
	    Pair pairo = (Pair) o;
	    return this.left1.equals(pairo.getLeft()) &&
	           this.right.equals(pairo.getRight());
	  }

	}