package utwente.fmt.jtorx.utils.cmdint;

public interface Command {
	public Boolean compute(String arg);
	//public Commands getSubcommands();
}
