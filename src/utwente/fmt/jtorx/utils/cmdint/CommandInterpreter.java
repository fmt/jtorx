package utwente.fmt.jtorx.utils.cmdint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class CommandInterpreter {
		
	private void printPrompt(String prompt) {
		System.out.print(prompt);
	}
	
	private void printWarning(String s) {
		System.out.println(s);
	}
	
	private String getCommand() {
		String tmp;
		try {
			tmp =  in.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tmp = "";
		}
		return tmp;
	}
	
	private String peekCommand() {
		String line = null;
		try {
			if (in.ready()) {
				int n = in.read(peekBuf, peekOff, peekBuf.length - peekOff);
				int i = peekOff;
				peekOff += n;
				line = null;
				while (i<peekOff && peekBuf[i] != '\n' && peekBuf[i] != '\r')
					i++;
				if (i < peekOff) {
					line = "";
					for(int j=0; j < i; j++)
						line += peekBuf[j];
					if (i+1 < peekOff)
						System.arraycopy(peekBuf, i+1, peekBuf, 0, peekOff - (i+1));
					peekOff = 0;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return line;	
	}
	
	
	private Boolean handleLine(String line, Commands cmds) {
		Boolean goOn = true;
		String [] splitted;
		String cmdStr, argStr;
		
		String trimmed = line.trim();
		splitted = trimmed.split("[\\t ]+", 2);
		if (splitted.length > 0) {
			cmdStr = splitted[0];
			if (splitted.length > 1)
				argStr = splitted[1];
			else
				argStr = null;
			Command cmd = cmds.lookup(cmdStr.trim());
			if (cmd != null) {
				goOn = cmd.compute(argStr);
			} else if (!cmdStr.equals("")) {
				printWarning("Error: unknown command: "+cmdStr);
			}
		}
		return goOn;
	}
	
	public void loop(String prompt, Commands cmds) {
		String line;
		Boolean goOn = true;
		
		while(goOn) {
			printPrompt(prompt);
			line = getCommand();
			if (line != null) 
				goOn = handleLine(line, cmds);
			else {
				//System.out.println("end of file on stdin");
				goOn = false;
			}
		}
	}
	
	public void peek(Commands cmds) {
		String line = peekCommand();
		if (line != null)
			handleLine(line, cmds);
	}
		
	public CommandInterpreter() {
		InputStreamReader converter = new InputStreamReader(System.in);
		in = new BufferedReader(converter);
		peekBuf = new char[1024];
		peekOff = 0;
	}
		
	BufferedReader in;
	private char peekBuf[];
	private int peekOff;

}
