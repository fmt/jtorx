package utwente.fmt.jtorx.utils.cmdint;

public interface Commands {
	Command lookup(String s);
}
