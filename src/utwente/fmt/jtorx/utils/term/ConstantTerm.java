package utwente.fmt.jtorx.utils.term;

import java.util.ArrayList;

import utwente.fmt.lts.Term;
import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.UnificationResult;
import utwente.fmt.lts.Variable;

public class ConstantTerm implements Term {

	public String getOperator() {
		return operator;
	}

	public Variable getVariable() {
		return null;
	}

	public boolean isVariable() {
		return false;
	}

	public ArrayList<Term> subTerms() {
		return null;
	}
	public UnificationResult unify(Term t) {
		return unify(t, new ArrayList<UnificationBinding>());
	}
	public UnificationResult unify(Term t, ArrayList<UnificationBinding> bl) {
		if (t.isVariable())
			return new UnificationResultImpl(false, null);
		String t_op = t.getOperator();
		ArrayList<Term> t_subs = t.subTerms();
		if (t_subs!=null && t_subs.size() != 0)
			return new UnificationResultImpl(false, null);
		if (operator==null || t_op==null || !operator.equals(t_op))
			// TODO check case-insensitive?
			// TODO check in special way for set?
			return new UnificationResultImpl(false, null);
		else
			return new UnificationResultImpl(true, bl);
	}
	
	public String unparse() {
		return operator;
	}
	
	public ConstantTerm(String op) {
		operator = op;
	}
	
	private String operator;

}
