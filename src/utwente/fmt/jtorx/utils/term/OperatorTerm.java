package utwente.fmt.jtorx.utils.term;

import java.util.ArrayList;

import utwente.fmt.lts.Term;
import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.UnificationResult;
import utwente.fmt.lts.Variable;

public class OperatorTerm implements Term {

	public String getOperator() {
		return operator;
	}

	public Variable getVariable() {
		return null;
	}

	public boolean isVariable() {
		return false;
	}

	public ArrayList<Term> subTerms() {
		return subTerms;
	}
	
	public UnificationResult unify(Term t) {
		return unify(t, new ArrayList<UnificationBinding>());
	}

	public UnificationResult unify(Term t, ArrayList<UnificationBinding> b) {
		if (t.isVariable()) {
			dbgMsgln("OperatorTerm.unify: this==operator("+operator+") t=variable");
			return new UnificationResultImpl(false, null);
		}
		String t_op = t.getOperator();
		ArrayList<Term> t_subTerms = t.subTerms();
		if (operator==null || t_op==null || !operator.equals(t_op)) { // TODO check case-insensitive?
			dbgMsgln("OperatorTerm.unify: operator="+operator+" t_op="+t_op);
			return new UnificationResultImpl(false, null);
		}
		int subSize = 0;
		int t_subSize = 0;
		if (subTerms!=null)
			subSize = subTerms.size();
		if (t_subTerms!=null)
			t_subSize = t_subTerms.size();
		dbgMsgln("OperatorTerm.unify: subTerms-size="+subSize+" t_subTerms-size="+t_subSize);
		if (subSize != t_subSize)
			return new UnificationResultImpl(false, null);
		if (subSize==0 && t_subSize==0){
			dbgMsgln("OperatorTerm.unify: subTerms-size="+subSize+" t_subTerms-size="+t_subSize);
			return new UnificationResultImpl(true, b);
		}
		if (subTerms!=null && t_subTerms!=null) {
			int i = 0;
			UnificationResult tmp;
			boolean ok = true;
			while (i < subTerms.size() && ok) {
				Term t_l = subTerms.get(i);
				Term t_r =  t_subTerms.get(i);
				tmp = t_l.unify(t_r,b);
				ok = tmp.isUnifyable();
				b = tmp.getBindings();
				i++;
			}
			return new UnificationResultImpl(ok, b);
		}
		
		return new UnificationResultImpl(false, null);
	}
	
	public String unparse() {
		if (subTerms==null || subTerms.size()==0)
			return operator;
		String res =  operator;
		res += "(";
		boolean first = true;
		for (Term t: subTerms) {
			if (first)
				first = false;
			else
				res += ", ";
			res += t.unparse();
		}
		res += ")";
		return res;
	}
	
	public OperatorTerm(String op, ArrayList<Term> sub) {
		operator = op;
		subTerms = sub;
	}
	
	private void dbgMsg(String s) {
		if (debug)
			System.out.print(s);
	}
	private void dbgMsgln(String s) {
		if (debug)
			System.out.println(s);
	}
	

	private String operator;
	private ArrayList<Term> subTerms;
	private boolean debug = false;

}
