package utwente.fmt.jtorx.utils.term;

import java.util.ArrayList;

import utwente.fmt.lts.Label;
import utwente.fmt.lts.Term;

public class TermParser {
	
	static public Term parse(Label l) {
		TermTokenizer tokenizer = new TermTokenizer(l);
		TermTokenizer.TokenType t = tokenizer.nextToken();
		Term parseResult = parseTerm(tokenizer, t);
		// the following assumes we have EITHER prefix OR suffix (but not both)
		ArrayList<Term> sub = new ArrayList<Term>();
		String pfx = tokenizer.getPrefix();
		String sfx = tokenizer.getSuffix();
		if (pfx!=null) {
			sub.add(new ConstantTerm(pfx));
			sub.add(parseResult);
			return new OperatorTerm("", sub);
		} else if (sfx!=null) {
			sub.add(parseResult);
			sub.add(new ConstantTerm(sfx));
			return new OperatorTerm("", sub);
		}
		return parseResult;
	}
		
	static private Term parseTerm(TermTokenizer tokenizer, TermTokenizer.TokenType t) {
		if (debug) System.out.println("parseTerm: parsing token: "+TermTokenizer.tokenName(t));
		if (t == TermTokenizer.TokenType.EOT)
			return null;
		if (t == TermTokenizer.TokenType.CONSTANT)
			return new ConstantTerm(tokenizer.tokenString());
		if (t == TermTokenizer.TokenType.VARIABLE)
			return new VariableTerm(tokenizer.tokenVariable());
		if (t == TermTokenizer.TokenType.OPERATOR) {
			String op = tokenizer.tokenString();
			ArrayList<Term> sub = new ArrayList<Term>();
		    t = tokenizer.nextToken();
		    if (t == TermTokenizer.TokenType.PAROPEN) {
		    	t = tokenizer.nextToken();
		    	parseTermList(tokenizer, t, sub);
		    } else
		    	tokenizer.pushBack(t);
	    	return new OperatorTerm(op, sub);
		}
		System.out.println("parseTerm: unexpected token: \""+tokenizer.tokenString()+"\"");
		return null;
	}
	
	static private ArrayList<Term> parseTermList(TermTokenizer tokenizer, TermTokenizer.TokenType t, ArrayList<Term> sub) {
		if (debug) System.out.println("parseTermList: parsing token: "+TermTokenizer.tokenName(t));
		if (t == TermTokenizer.TokenType.EOT)
			return null;
		while(t != TermTokenizer.TokenType.EOT && t!=TermTokenizer.TokenType.PARCLOSE) {
			Term tt = parseTerm(tokenizer, t);
			sub.add(tt);
			t = tokenizer.nextToken();
			if (t == TermTokenizer.TokenType.COMMA)
				t = tokenizer.nextToken();
		}
		if (t == TermTokenizer.TokenType.PARCLOSE) {
			return sub;
		}
		return null;
	}
	
	static private boolean debug = false;

}
