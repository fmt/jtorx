package utwente.fmt.jtorx.utils.term;

import java.util.Iterator;

import utwente.fmt.lts.Label;
import utwente.fmt.lts.Variable;

public class TermTokenizer {
	
	public enum TokenType {
		CONSTANT, // any leaf that can not have subterms
		VARIABLE, // variable (and thus has no subterms)
		OPERATOR, // operator name (may have subterms)
		PAROPEN,
		PARCLOSE,
		COMMA,
		EOT,
	}
	static public String TokenNames[] = {
		"CONSTANT",
		"VARIABLE",
		"OPERATOR",
		"PAROPEN",
		"PARCLOSE",
		"COMMA",
		"EOT",
	};
	
	static public String tokenName(TokenType t) {
		return TokenNames[t.ordinal()];
	}

	public void pushBack(TokenType t) {
		if (pushedBack != null)
			System.err.println("TermTokenizer: Internal Error: pushing onto non-empty pushBack");
		pushedBack = t;
	}
	
	public TokenType nextToken() {
		if (debug) System.out.println("termtokenizer nextToken (i="+i+", len="+s.length()+")");
		if (pushedBack !=null) {
			TokenType t = pushedBack;
			pushedBack = null;
			if (debug) System.out.println("termtokenizer returning pushed back "+t);
			return t;
		}
		variable = null;
		char c;
		while(i < s.length()) {
			c = s.charAt(i);
			i++;
			if (c != ' ' && c != '\t') {
				i--;
				break;
			}
		}
		begIndex = i;
		if (debug) System.out.println("termtokenizer nextToken skipped whitespace (i="+i+", len="+s.length()+")");
		if (i==s.length()) {
			endIndex = i;
			if (debug) System.out.println("termtokenizer returning "+TokenType.EOT);
			return TokenType.EOT;
		}
		c = s.charAt(i);
		i++;
		if (c == '(') {
			endIndex = i;
			parNestingDepth++;
			if (debug) System.out.println("termtokenizer returning "+TokenType.PAROPEN);
			return TokenType.PAROPEN;
		}
		if (c == ')') {
			endIndex = i;
			parNestingDepth--;
			if (debug) System.out.println("termtokenizer returning "+TokenType.PARCLOSE);
			return TokenType.PARCLOSE;
		}
		if (c == ',') {
			endIndex = i;
			if (debug) System.out.println("termtokenizer returning "+TokenType.COMMA);
			return TokenType.COMMA;
		}
		if (parNestingDepth == 0 && c == '!') {
			endIndex = i;
			if (! parOpenPassed) {
				parOpenPassed =  true;
				if (debug) System.out.println("termtokenizer returning "+TokenType.PAROPEN);
				return TokenType.PAROPEN;

			} else {
				if (debug) System.out.println("termtokenizer returning "+TokenType.COMMA);
				return TokenType.COMMA;
			}
		}
		if (c == '{') {
			int recLevel = 1;
			while(i<s.length()) {
				c = s.charAt(i);
				i++;
				if (c == '{') {
					recLevel ++;
				}
				if (c == '}'){
					recLevel --;
					if (recLevel == 0) {
						endIndex = i;
						if (debug) System.out.println("termtokenizer returning "+TokenType.CONSTANT);
						return TokenType.CONSTANT;
					}
				}
			}
			System.err.println("TermTokenizer: end of input in set");
		}
		if (c == '"') {
			boolean escaping = false;
			while(i<s.length()) {
				c = s.charAt(i);
				i++;
				if (escaping)
					escaping = false;
				else {
					if (c == '\\')
						escaping = true;
					if (c == '"'){
						endIndex = i;
						if (debug) System.out.println("termtokenizer returning "+TokenType.CONSTANT);
						return TokenType.CONSTANT;
					}
				}
			}
			System.err.println("TermTokenizer: end of input in quoted string");
		}
		while(i<s.length()) {
			c = s.charAt(i);
			i++;
			if (c == ' ' || c == '\t' || c == '(' || c == ')' || c == '!' || c ==',' || c == '{') {
				i--;
				break;
			}
		}
		endIndex = i;
		String tok = s.substring(begIndex, endIndex);
		Iterator<Variable> it = label.getVariables();
		while(it.hasNext()) {
			Variable v = it.next();
			if (v.getName().equals(tok)) {
				variable = v;
				if (debug) System.out.println("termtokenizer returning "+TokenType.VARIABLE+" \""+tokenString()+"\"");
				return TokenType.VARIABLE;
			}
		}
		if (debug) System.out.println("termtokenizer returning "+TokenType.OPERATOR+" \""+tokenString()+"\"");
		return TokenType.OPERATOR;

	}
	public String tokenString() {
		return s.substring(begIndex, endIndex);
	}
	public Variable tokenVariable() {
		return variable;
	}
	public String getPrefix() {
		return pfx;
	}
	public String getSuffix() {
		return sfx;
	}
	
	public TermTokenizer(Label l) {
		label = l;
		pfx = null;
		sfx = null;
		if (debug) System.out.println("termtokenizer label "+l.getString());
		// get rid of !? prefix or suffix (after trim)
		// this assumes we have EITHER prefix OR suffix (but not both)
		s = l.getString().trim();
		if (s.charAt(0) == '!' || s.charAt(0) == '?') {
			pfx = s.substring(0, 1);
			s = s.substring(1);
		} else if (s.endsWith("!") || s.endsWith("?")) {
			sfx = s.substring(s.length()-1, s.length());
			s = s.substring(0, s.length()-1);
		}
		if (debug) System.out.println("termtokenizer pfx "+pfx);
		if (debug) System.out.println("termtokenizer sfx "+sfx);
		if (debug) System.out.println("termtokenizer string "+s);
		i = 0;
		begIndex = 0;
		endIndex = 0;
		parNestingDepth = 0;
		parOpenPassed = false;
		variable = null;
		pushedBack = null;
	}
	
	private Label label;
	private String s; // label string
	private int i; // position in s
	private int begIndex;
	private int endIndex;
	private int parNestingDepth;
	private boolean parOpenPassed;
	private Variable variable;
	private TokenType pushedBack;
	private String pfx;
	private String sfx;
	private boolean debug = false;
}
