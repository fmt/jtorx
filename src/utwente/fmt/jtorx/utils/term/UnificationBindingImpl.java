package utwente.fmt.jtorx.utils.term;

import utwente.fmt.lts.Term;
import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.Variable;

public class UnificationBindingImpl implements UnificationBinding {

	public Term getTerm() {
		return term;
	}

	public Variable getVariable() {
		return variable;
	}
	
	public UnificationBindingImpl(Variable v, Term t) {
		variable = v;
		term = t;
	}
	
	private Variable variable;
	private Term term;

}
