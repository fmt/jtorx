package utwente.fmt.jtorx.utils.term;

import java.util.ArrayList;

import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.UnificationResult;

public class UnificationResultImpl implements UnificationResult {

	public ArrayList<UnificationBinding> getBindings() {
		return bindings;
	}

	public boolean isUnifyable() {
		return isUnifyable;
	}
	
	public UnificationResultImpl(boolean ok, ArrayList<UnificationBinding> bl) {
		isUnifyable = ok;
		bindings = bl;
	}
	
	private ArrayList<UnificationBinding> bindings;
	private boolean isUnifyable;

}
