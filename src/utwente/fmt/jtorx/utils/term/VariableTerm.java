package utwente.fmt.jtorx.utils.term;

import java.util.ArrayList;

import utwente.fmt.lts.Term;
import utwente.fmt.lts.UnificationBinding;
import utwente.fmt.lts.UnificationResult;
import utwente.fmt.lts.Variable;

public class VariableTerm implements Term {

	public String getOperator() {
		return null;
	}

	public Variable getVariable() {
		return variable;
	}

	public boolean isVariable() {
		return true;
	}

	public ArrayList<Term> subTerms() {
		return null;
	}
	
	public UnificationResult unify(Term t) {
		return unify(t, new ArrayList<UnificationBinding>());
	}


	public UnificationResult unify(Term t, ArrayList<UnificationBinding> bl) {
		if (!t.isVariable()) {
			// TODO check if variable is already in bl?
			bl.add(new UnificationBindingImpl(variable, t));
			return new UnificationResultImpl(true, bl);
		}
		bl.add(new UnificationBindingImpl(variable, t));
		return new UnificationResultImpl(true, bl);
	}
	
	public String unparse() {
		return variable.getName();
	}
	
	public VariableTerm(Variable var) {
		variable = var;
	}
	
	private Variable variable;

}
