package utwente.fmt.jtorx.utracechecker;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.torx.primer.Primer;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.writers.NullLTSWriter;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;

// Idea:
// given an LTS, we create a Utrace primer
// we generate its entire state space
// then we ask for the omitted utrace inputs
//   this gives us primer states + the omitted outgoing transitions (in original LTS)
// then we do a breadthfirst search in the primer for the states that have omitted transitions
// this gives us the shortest traces to problematic states in the original model
// we can present these kind of like the iocoChecker results


public class UtraceChecker {
	
	private Map<CompoundState,List<CompoundTransition<?extends CompoundState>>> findTraces(Primer primer) {
		
		// now that we have fully explored the primer LTS, obtain omitted inputs,
		// and associated primer states
		Map<CompoundState,Collection<Transition<? extends State>>> primerOmittedUtraceInputs =
			primer.getOmmittedUtraceInputs();
		if (primerOmittedUtraceInputs==null || primerOmittedUtraceInputs.size()==0)
			return null;
		
		for(CompoundState s: primerOmittedUtraceInputs.keySet()) {
			System.err.println("findTraces: primer map "+s.getLabel(true)+" "+s.getID()+" "+s+" hashcode:"+s.hashCode());
		}
		
		Map<CompoundState,Collection<Transition<? extends State>>> omittedUtraceInputs =
			new HashMap<CompoundState,Collection<Transition<? extends State>>>();
		
		// NOTE we assume that getCanonical for ALL states returns non-null,
		// because we already expanded/explored the state space!!!
		for (Entry<CompoundState, Collection<Transition<? extends State>>> e: primerOmittedUtraceInputs.entrySet()) {
			omittedUtraceInputs.put(e.getKey().getCanonical(), e.getValue());
		}
		
		for(CompoundState s: omittedUtraceInputs.keySet()) {
			System.err.println("findTraces: our map "+s.getLabel(true)+" "+s.getID()+" "+s+" hashcode:"+s.hashCode());
		}
	

		
		// now do breadth first exploration, to obtain shortest traces
		// to problematic states
		Map<CompoundState,List<CompoundTransition<?extends CompoundState>>> foundStates =
			new HashMap<CompoundState,List<CompoundTransition<?extends CompoundState>>>();
		Map<CompoundState,List<CompoundTransition<?extends CompoundState>>> work =
			new HashMap<CompoundState,List<CompoundTransition<?extends CompoundState>>>();
		Set<CompoundState> seen = new HashSet<CompoundState>();
		Iterator<? extends CompoundState> it;
		try {
			it = primer.init();
		} catch (LTSException e1) {
			System.err.println("UtraceChecker: caught LTSException: "+e1.getMessage());
			return null;
		}
		while(it.hasNext())
			work.put(it.next(), new Vector<CompoundTransition<?extends CompoundState>>());
		for(CompoundState s: work.keySet()) {
			System.err.println("findTraces: in initial for: "+s.getLabel(true)+" "+s.getID()+" "+s);
			seen.add(s);
			if (omittedUtraceInputs.keySet().contains(s)) {
				System.err.println("findTraces: in for: found "+s.getLabel(true));
				foundStates.put(s, work.get(s));
			}
		}
		while(foundStates.size() < omittedUtraceInputs.size() && work.size() > 0) {
			System.err.println("findTraces: in while: "+work.size());
			Map<CompoundState,List<CompoundTransition<?extends CompoundState>>> newWork =
				new HashMap<CompoundState,List<CompoundTransition<?extends CompoundState>>>();
			for(CompoundState s: work.keySet()) {
				Iterator<?extends CompoundTransition<?extends CompoundState>> tit = s.menu(interpAny.isAny());
				List<CompoundTransition<?extends CompoundState>> trace = work.get(s);
				System.err.println("findTraces: working on: "+s.getLabel(true)+" "+s+" hashcode:"+s.hashCode()+" trace: "+
						"[ "+printTrace(trace.iterator())+" ]");
				while(tit.hasNext()) {
					CompoundTransition<?extends CompoundState> t = tit.next();
					System.err.print("\t-- "+t.getLabel().getString()+" --> "+t.getDestination().getLabel(true)+" "+t.getDestination().getID()+" hashcode:"+t.getDestination().hashCode());
					CompoundState dst = t.getDestination().getCanonical();
					if (!seen.contains(dst)) {
						System.err.println(" : dst not seen");
						seen.add(dst);
						List<CompoundTransition<?extends CompoundState>> newTrace =
							new Vector<CompoundTransition<?extends CompoundState>>(trace);
						newTrace.add(t);
						if (omittedUtraceInputs.keySet().contains(dst)) {
							System.err.println("findTraces: in while-for-while: found "+dst.getLabel(true)+" "+dst.getID()+" "+dst);
							foundStates.put(dst, newTrace);
						} else {
							System.err.println("findTraces: in while-for-while: NOT found "+dst.getLabel(true)+" "+dst.getID()+" "+dst);
						}
						if (foundStates.size() == omittedUtraceInputs.size())
							return foundStates;
						newWork.put(dst, newTrace);
					} else {
						System.err.println(" : dst already seen");
					}
				}
			}
			work = newWork;
		}
		System.err.println("findTraces : returning #foundstates="+foundStates.size());
		return foundStates;
	}
	
	public UtraceCheckerResult check() {
		Primer primer = new Primer(lts, interp, false, true, TraceKind.UTRACES, errorReporter, progressReporter);
		
		System.err.println("UtraceChecker.check: NullLTSWriter start ... ");
		new NullLTSWriter(primer, interp, stateSpaceProgressReporter ).write();
		
		System.err.println("UtraceChecker.check: NullLTSWriter ... done ");
		
		Map<CompoundState,Collection<Transition<? extends State>>> omittedUtraceInputs =
			primer.getOmmittedUtraceInputs();
		if (omittedUtraceInputs==null || omittedUtraceInputs.size()==0) {
			System.err.println("no omitted utrace inputs found");
			return new UtraceCheckerResult(null, null, null, null);
		}

		Map<CompoundState,List<CompoundTransition<?extends CompoundState>>> result = findTraces(primer);
		for(CompoundState s: result.keySet()) {
			System.err.println("utraces check: "+s.getLabel(true)+" "+
					"[ "+printTrace(result.get(s).iterator())+" ]"+" "+
					"{ "+printLabels(omittedUtraceInputs.get(s).iterator())+" }");
		}
		System.err.println("UtraceChecker.check:  #omittedUtraceInputs="+omittedUtraceInputs.size()+" #result="+result.size());
		return new UtraceCheckerResult(lts, fileName, omittedUtraceInputs, result);
	}
	
	public UtraceChecker(LTS l, String n, IOLTSInterpretation i, ErrorReporter r, ProgressReporter p, ProgressReporter spp) {
		lts = l;
		fileName = n;
		errorReporter = r;
		progressReporter = p;
		stateSpaceProgressReporter = spp;
		interp = i;
	}
	
	private String printTrace(Iterator<CompoundTransition<?extends CompoundState>> it) {
		String s = "";
		if (it.hasNext())
			s += it.next().getLabel().getString();
		while(it.hasNext()) {
			s += " . ";
			s += it.next().getLabel().getString();
		}
		return s;
	}
	private String printLabels(Iterator<Transition<? extends State>> it) {
		String s = "";
		if (it.hasNext())
			s += it.next().getLabel().getString();
		while(it.hasNext()) {
			s += " . ";
			s += it.next().getLabel().getString();
		}
		return s;
	}

	
	private LTS lts;
	private String fileName;
	private IOLTSInterpretation interp;
	private ErrorReporter errorReporter;
	private ProgressReporter progressReporter;
	private ProgressReporter stateSpaceProgressReporter;
	private Interpretation interpAny = new AnyInterpretation();
}
