package utwente.fmt.jtorx.utracechecker;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;

public class UtraceCheckerResult {
	
	public Collection<? extends CompoundState> getStates() {
		if (omittedUtraceInputs==null)
			return null;
		return omittedUtraceInputs.keySet();
	}
	
	public Collection<? extends Transition<? extends State>> getTransitions(CompoundState s) {
		if (omittedUtraceInputs==null)
			return null;
		return omittedUtraceInputs.get(s);
	}
	
	public List<CompoundTransition<?extends CompoundState>> getTrace(CompoundState s) {
		if (tracesToOmittingStates==null)
			return null;
		return tracesToOmittingStates.get(s);
	}
	
	public LTS getLTS() {
		return lts;
	}
	
	public String getFileName() {
		return fileName;
	}


	public UtraceCheckerResult(LTS l, String n, Map<CompoundState,Collection<Transition<? extends State>>> o,
			Map<CompoundState,List<CompoundTransition<?extends CompoundState>>> t) {
		omittedUtraceInputs = o;
		tracesToOmittingStates = t;
		lts = l;
		fileName = n;
	}
	
	private Map<CompoundState,Collection<Transition<? extends State>>> omittedUtraceInputs;
	private Map<CompoundState,List<CompoundTransition<?extends CompoundState>>> tracesToOmittingStates;
	private LTS lts;
	private String fileName;
}
