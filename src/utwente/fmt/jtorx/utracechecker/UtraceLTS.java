package utwente.fmt.jtorx.utracechecker;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import utwente.fmt.jtorx.torx.explorer.lib.LibLabel;
import utwente.fmt.jtorx.torx.explorer.lib.LibState;
import utwente.fmt.jtorx.torx.explorer.lib.LibStateIterator;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Position;
import utwente.fmt.lts.State;

public class UtraceLTS implements LTS {

	public void done() {
		// TODO Auto-generated method stub

	}
	
	public long getNrOfStates() {
		return nrOfStates;
	}

	public long getNrOfTransitions() {
		return nrOfTrans;
	}
	
	public boolean hasPosition() {
		return false;
	}
	
	public Position getRootPosition() {
		return null;
	}
	
	public void cleanupPreserving(Collection<?extends State> s) {
	}

	public Iterator<? extends State> init() {
		LibStateIterator it = new LibStateIterator();
		if (start!=null)
			it.add(start);
		return it;
	}
	
	public UtraceLTS(List<CompoundTransition<?extends CompoundState>> tl) {
		theTrace = tl;
		nrOfStates = 0;
		nrOfTrans = 0;
		if (theTrace!=null && theTrace.size() > 0) {
			LibState src = null;
			LibState dst = null;
			int j = 0;
			for (int i=0; i<theTrace.size(); i++) {
				CompoundTransition<?extends CompoundState> t = theTrace.get(i);
				if (src==null && start==null) {
					src = mkState(""+j++, t.getSource().getLabel(false), t.getSource().getNodeName());
					start =  src;
				}
				dst = mkState(""+j++, t.getDestination().getLabel(false), t.getDestination().getNodeName());
				// TODO use more informative transition id in next line?
				mkTransition(src, mkLabel(t.getLabel().getString()), dst, t.getEdgeName());
				src = dst;
			}
		} else
			start = mkState(""+0, "", null);
		System.err.println("UtraceLTS nrOfStates="+nrOfStates);
	}
	
	private LibState mkState(String id, String lbl, String name) {
		LibState as = new LibState(this, id, lbl, name);
		nrOfStates++;
		return as;
	}

	private void mkTransition(LibState s, LibLabel l, LibState d, String eid) {
		s.addSucc(l, d, null, null, eid);
		nrOfTrans++;
	}
	
	private LibLabel mkLabel(String l) {
		LibLabel al = labelMap.get(l);
		if (al == null) {
			al = new LibLabel(l);
			labelMap.put(l, al);
		}
		return al;
	}

	
	private List<CompoundTransition<?extends CompoundState>> theTrace = null;
	private LibState start = null;
	
	private long nrOfStates = -1;
	private long nrOfTrans = -1;
	
	private HashMap<String, LibLabel> labelMap = new HashMap<String, LibLabel>();

}
