package utwente.fmt.jtorx.writers;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;


import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.jtorx.logger.anidot.DotLTS;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.CompoundTransition;
import utwente.fmt.lts.LTS.LTSException;

public class AniDotExtendedLTSWriter extends AniDotLTSWriter {
	
	private void doAddNode(CompoundState s) {
		System.out.println("AniDotExtendedLTSWriter doAddNode: "+s+(s!=null?" "+s.getID():""));
		if (s == null)
			return;
		if (s != null && s.getVerdict() != null) {
			System.out.println("AniDotExtendedLTSWriter: "+s+" verdict "+s.getVerdict().getString());
			dotLTS.addNode(s, s.getVerdict().getString());
		} else
			dotLTS.addNode(s);
	}
	private void unfold(Iterator<? extends CompoundTransition<?extends CompoundState>> tit) {
		if (tit == null)
			return;
		System.out.println("\tAniDotExtendedLTSWriter doing unfold of: "+tit);
		while(tit.hasNext()) {
			CompoundTransition<?extends CompoundState> t = tit.next();
			if (!t.getLabel().getString().startsWith("verdict")) {
				CompoundState d =  t.getDestination();
				System.out.println("\tAniDotExtendedLTSWriter unfold of "+tit+" finds state: "+d);
				if (!expanded.contains(d)) {
					// Force expansion of state before we add it to set and animation
					d.menu(interp.isAny());
					work.add(d);
					progress.setMax(work.size()+expanded.size());
				}
				doAddNode(d);  // should this be only done if d not aldready added?
				dotLTS.addEdge(t);
			}
		}
	}
	
	public void write() {
		System.out.println("AniDotExtendedLTSWriter ...");
		Iterator<? extends CompoundState> sit = null;
		try {
			sit = lts.init();
		} catch (LTSException e) {
			System.err.println("AniDotExtendedLTSWriter: caught LTSException: "+e.getMessage());
			sit = null;
		}
		if (sit!=null) {
			while(sit.hasNext()) {
				work.add(sit.next());
				progress.setMax(work.size()+expanded.size());
			}
			while (work.size() > 0) {
				CompoundState s = work.firstElement();
				work.remove(0);
				System.out.println("AniDotExtendedLTSWriter looking at: "+s);
				if (s != null) {

					Iterator<? extends CompoundTransition<?extends CompoundState>> tit = null;

					if (!expanded.contains(s)) {
						tit = s.menu(interp.isAny());
						System.out.println("\tAniDotExtendedLTSWriter invoking menu for: "+s+" "+tit);
						expanded.add(s); // we do this AFTER we forced expansion by requesting menu
						progress.setLevel(expanded.size()-work.size());
						doAddNode(s);
						unfold(tit);
					} else
						progress.setLevel(expanded.size()-work.size());

				}
			}
			progress.setLevel(expanded.size());
			progress.ready();
		}
		System.out.println("AniDotExtendedLTSWriter done");
	}
	public void write(DotLTS d) {
		dotLTS = d;
		write();
	}
	
	public AniDotExtendedLTSWriter(CompoundLTS l, Interpretation i, DotLTS d, ProgressReporter p) {
		super(l,i,d, p);
//		interp = i;
		lts = l;
//		dotLTS = d;
	}
	public AniDotExtendedLTSWriter(CompoundLTS l, Interpretation i, ProgressReporter p) {
		this(l,i,null, p);
	}
	
	private CompoundLTS lts;
	//private Interpretation interp;
	private Vector<CompoundState> work = new Vector<CompoundState>();
	private HashSet<CompoundState> expanded = new HashSet<CompoundState>();
	
	// DotLTS dotLTS;
}
