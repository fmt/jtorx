package utwente.fmt.jtorx.writers;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;


import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.jtorx.logger.anidot.DotLTS;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;

public class AniDotLTSWriter {
	
	private void unfold(Iterator<? extends Transition<?extends State>> tit) {
		while(tit.hasNext()) {
			Transition<?extends State> t = tit.next();
			State d = t.getDestination();
			if (!expanded.contains(d)) {
				d.menu(interp.isAny());
				work.add(d);
				progress.setMax(work.size()+expanded.size());
			}
			dotLTS.addNode(d);
			dotLTS.addEdge(t);
		}
	}
	
	public void write() {
		if (progress!=null)
			progress.reset();
		expanded = new HashSet<State>();
		work = new Vector<State>();
		Iterator<? extends State> sit = null;
		try {
			sit = lts.init();
		} catch (LTSException e) {
			System.err.println("AniDotLTSWriter: caught LTSException: "+e.getMessage());
			sit = null;
		}
		if (sit==null) {
			expanded = null;
			work = null;
			return;
		}
		while(sit.hasNext()) {
			work.add(sit.next());
			progress.setMax(work.size()+expanded.size());
		}
		while(work.size() > 0) {
			State s = work.firstElement();
			work.remove(0);
			// TODO make robust in other way? throw error?
			if (s!=null) {
				Iterator<? extends Transition<?extends State>> tit = null;
				progress.setLevel(expanded.size()-work.size());
				if (!expanded.contains(s)) {
					tit = s.menu(interp.isAny());
					expanded.add(s);
					dotLTS.addNode(s);
					unfold(tit);
				}
			}
		}
		progress.setLevel(expanded.size());
		expanded = null;
		work = null;
		progress.ready();
	}
	public void write(DotLTS d) {
		dotLTS = d;
		write();
	}
	
	public void report(String s) {
		progress.setActivity(s);
	}
	
	public AniDotLTSWriter(LTS l, Interpretation i, DotLTS d, ProgressReporter p) {
		interp = i;
		lts = l;
		dotLTS = d;
		progress = p;
	}
	public AniDotLTSWriter(LTS l, Interpretation i, ProgressReporter p) {
		this(l, i, null, p);
	}
	
	protected LTS lts;
	protected Interpretation interp;
	private HashSet<State> expanded = null;
	private Vector<State> work = null;
	protected ProgressReporter progress;
	
	DotLTS dotLTS;
}
