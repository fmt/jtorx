package utwente.fmt.jtorx.writers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.jtorx.logger.coverage.CoverageData;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;

public class AutWriter implements LTSWriter {
	
	private void unfold(Iterator<? extends State> sit) {
		HashSet<State> states = new HashSet<State>();
		ArrayList<State> work = new ArrayList<State>(states);

		while(sit.hasNext()) {
			State s = sit.next();
			// HACK force expansion
			s.menu(interp.isAny());
			State c = s.getCanonical();
			if (c!=null)
				s = c;
			if (!states.contains(s)) {
				states.add(s);
				work.add(s);
			}
		}

		while(work.size() > 0) {
			//System.out.println("  in work loop");
			State s = work.get(0);
			work.remove(0);
			Iterator<? extends Transition<?extends State>> tit;
			tit = s.menu(interp.isAny());
			//System.out.println("state "+s.getID());
			while(tit.hasNext()) {
				Transition<?extends State> t = tit.next();
				State n = t.getDestination();
				// HACK force expansion
				n.menu(interp.isAny());
				State c = n.getCanonical();
				if (c!=null)
					n = c;
				if (!states.contains(n)) {
					states.add(n);
					work.add(n);
				}
				result.add("("+toNumber(s)+", \""+t.getLabel().getString()+"\", "+toNumber(n)+")");
			}
		}
	}
	
	public void write() throws IOException {
		Iterator<? extends State> sit = null;
		try {
			sit = lts.init();
		} catch (LTSException e) {
			System.err.println("AutWriter: caught LTSException: "+e.getMessage());
			return;
		}
		unfold(sit);
		app.append("des(0"+","+result.size()+","+nextNr+")"+"\n");
		for (Iterator<String> it = result.iterator(); it.hasNext();)
			app.append(it.next()+"\n");
	}
	
	public AutWriter(LTS l, Interpretation i, Appendable a) {
		interp = i;
		lts = l;
		app = a;
	}
	
	private int toNumber(State s) {
		if (!stateMap.containsKey(s)) {
			stateMap.put(s, nextNr++);
		}
		return stateMap.get(s);
	}
	private String toNodeName(State s) {
		return CoverageData.getNodeName(s);
	}

	private LTS lts;
	private Interpretation interp;
	private Appendable app;
	private int nextNr = 0;
	private HashMap<State,Integer> stateMap = new HashMap<State,Integer>();

	private Vector<String> result = new Vector<String>();
}
