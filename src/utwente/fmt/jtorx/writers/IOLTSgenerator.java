package utwente.fmt.jtorx.writers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.Label;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;


import iolts.*;


// TODO do not use  Main.addstate and Main.addtransition
// but directly use the underlying methods
// to be able to directly use the Interpretation
// instead of having to resort to !? prefixes
public class IOLTSgenerator {
	
	private void unfold(State s) {
		if (states.contains(s))
			return;
		states.add(s);
		Iterator<? extends Transition<?extends State>> tit;
		tit = s.menu(interp.isAny());
		//System.out.println("state "+s.getID());
		while(tit.hasNext()) {
			Transition<?extends State> t = tit.next();
			trans.add(t);
		}
		tit = s.menu(interp.isAny());
		while(tit.hasNext()) {
			unfold(tit.next().getDestination());
		}
	}
	
	public IOLTS generateIOLTS()  {
		IOLTS iolts = new IOLTS();
		boolean errorsSeen = false;
		Iterator<? extends State> sit;
		try {
			sit = lts.init();
		} catch (LTSException e1) {
			System.err.println("IOLTSgenerator: caught LTSException: "+e1.getMessage());
			return null;
		}
		Set<State> inits = new HashSet<State>();
		while(sit.hasNext()) {
			State i = sit.next();
			inits.add(i);
			unfold(i);
		}
		iolts.State realinit =  null;
		if (inits.size() > 1) {
			realinit = new iolts.State("real-init-state", "real-init-state");
			iolts.setInitialState(realinit);
		}
		for (State s: states) {
			String id = Integer.toString(toNumber(s));
			iolts.State ss = new iolts.State(id, s.getLabel(false));
			iolts.addState(ss);
			if (inits.contains(s) && inits.size()==1) {
				iolts.setInitialState(ss);
				realinit = ss;
			} else if (inits.contains(s) && inits.size() > 1) {
		           iolts.Transition t = new iolts.Transition(tau, ss);
		           realinit.addOutgoingTransition(t);
			}
		}
		for (Transition<?extends State> t: trans) {
			String to = Integer.toString(toNumber(t.getDestination()));
			String from = Integer.toString(toNumber(t.getSource()));
			iolts.State target;
			iolts.State source;
			try {
				target = iolts.getStateByID(to);
				source = iolts.getStateByID(from);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				source = realinit;
				target = realinit;
			}
			Label l = t.getLabel();
			String label = l.getString();
			iolts.Action act = null;
			if (interp.isInternal().isSatisfiedBy(l))
		       act = tau;
		    else if (interp.isInput().isSatisfiedBy(l))
		       act = new InputAction(label);
		    else if (interp.isOutput().isSatisfiedBy(l))
		       act = new OutputAction(label);
		    else if (!labelsWithoutInterpretation.contains(label)) {
		    	err.report("no interpretation given for "+name+" label: "+label);
		    	labelsWithoutInterpretation.add(label);
		    }
		  
			if (act != null) {
				if (!(act instanceof UnobservableAction)) {
	        	   act = actionKnown(act);
	        	   iolts.addAction(act);
				}
				iolts.Transition tt = new iolts.Transition(act, target);
				source.addOutgoingTransition(tt);
			} else
				errorsSeen = true;
		}
		if (errorsSeen)
			iolts = null;
		return iolts;
	}
	
	public IOLTSgenerator(ErrorReporter r, String n, LTS l, IOLTSInterpretation i) {
		interp = i;
		lts = l;
		err = r;
		name = n;
	}
	
	private int toNumber(State s) {
		if (!stateMap.containsKey(s)) {
			stateMap.put(s, nextNr++);
		}
		return stateMap.get(s);
	}
	
	   private static Action actionKnown(Action act) {
	        for (Iterator<Action> it = actions.iterator(); it.hasNext();) {
	            Action a = it.next();
	            if (a.equals(act)) {
	                return a;
	            }
	        }
	        actions.add(act);
	        return act;
	    }


	private LTS lts;
	private IOLTSInterpretation interp;
	private int nextNr = 0;
	private HashMap<State,Integer> stateMap = new HashMap<State,Integer>();
	private HashSet<State> states = new HashSet<State>();
	private HashSet<Transition<?extends State>> trans = new HashSet<Transition<?extends State>>();
	static private UnobservableAction tau = new UnobservableAction();
    static Set<Action> actions = new HashSet<Action>();
    
    private Set<String> labelsWithoutInterpretation = new HashSet<String>();
    
    private ErrorReporter err = null;
    private String name = null;
}
