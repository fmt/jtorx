package utwente.fmt.jtorx.writers;

import java.io.IOException;

public interface LTSWriter {
	public void write() throws IOException;
}
