package utwente.fmt.jtorx.writers;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;


import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;

public class NullLTSWriter {
	
	private void unfold(Iterator<? extends Transition<?extends State>> tit) {
		System.err.println("NullLTSWriter unfold do... "+tit);
		if (tit==null)
			return;
		while(tit.hasNext()) {
			Transition<?extends State> t = tit.next();
			State d = t.getDestination();
			if (d!=null && !expanded.contains(d)) {
				d.menu(interp.isAny());
				work.add(d);
				progress.setMax(work.size()+expanded.size());
			}
		}
		System.err.println("NullLTSWriter unfold done "+tit);
	}
	
	public void write() {
		if (progress!=null)
			progress.reset();
		expanded = new HashSet<State>();
		work = new Vector<State>();
		Iterator<? extends State> sit = null;
		try {
			sit = lts.init();
		} catch (LTSException e) {
			System.err.println("driver: caught LTSException: "+e.getMessage());
			sit = null;
		}
		if (sit!=null) {
			while(sit.hasNext()) {
				work.add(sit.next());
				progress.setMax(work.size()+expanded.size());
			}
			while(work.size() > 0) {
				System.err.println("NullLTSWriter write in while: work size "+work.size());
				State s = work.firstElement();
				work.remove(0);
				System.err.println("NullLTSWriter write in while: work on "+s+" "+s.getLabel(false)+" "+s.getID());
				// TODO make robust in other way? throw error?
				if (s!=null) {
					Iterator<? extends Transition<?extends State>> tit = null;
					progress.setLevel(expanded.size()-work.size());
					if (!expanded.contains(s)) {
						System.err.println("NullLTSWriter write in while: expanded contains "+s);
						tit = s.menu(interp.isAny());
						System.err.println("NullLTSWriter write in while: menu for "+s+" "+tit);
						expanded.add(s);
						System.err.println("NullLTSWriter write in while: added to expanded "+s);
						unfold(tit);
						System.err.println("NullLTSWriter write in while: done unfolding for "+s+" of "+tit);
					} else 
						System.err.println("NullLTSWriter write in while: expanded does not contain "+s);
				}
			}
			System.err.println("NullLTSWriter write after while");
			progress.setLevel(expanded.size());
			expanded = null;
			work = null;
			progress.ready();
		}
		System.err.println("NullLTSWriter write done");
	}
	
	public void report(String s) {
		progress.setActivity(s);
	}
	
	public NullLTSWriter(LTS l, Interpretation i, ProgressReporter p) {
		interp = i;
		lts = l;
		progress = p;
	}
	
	protected LTS lts;
	protected Interpretation interp;
	private HashSet<State> expanded = null;
	private Vector<State> work = null;
	protected ProgressReporter progress;
	
}
