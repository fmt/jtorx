package utwente.fmt.jtorx.writers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import utwente.fmt.interpretation.Interpretation;
import utwente.fmt.lts.LTS;
import utwente.fmt.lts.State;
import utwente.fmt.lts.Transition;
import utwente.fmt.lts.LTS.LTSException;

public class TraceWriter implements LTSWriter {
		
		private void unfold(Iterator<? extends State> sit) throws IOException {
			HashSet<State> states = new HashSet<State>();
			ArrayList<State> work = new ArrayList<State>(states);

			while(sit.hasNext()) {
				State s = sit.next();
				// HACK force expansion
				s.menu(interp.isAny());
				State c = s.getCanonical();
				if (c!=null)
					s = c;
				if (!states.contains(s)) {
					states.add(s);
					work.add(s);
				}
			}

			while(work.size() > 0) {
				//System.out.println("  in work loop");
				State s = work.get(0);
				work.remove(0);
				Iterator<? extends Transition<?extends State>> tit;
				tit = s.menu(interp.isAny());
				//System.out.println("state "+s.getID());
				int i = 0;
				while(tit.hasNext()) {
					if (i>0) {
						System.err.println("TraceWriter: LTS is not a trace (contains state with multiple outgoing transitions)");
						throw new IOException("LTS is not a trace");
					}
					Transition<?extends State> t = tit.next();
					State n = t.getDestination();
					// HACK force expansion
					n.menu(interp.isAny());
					State c = n.getCanonical();
					if (c!=null)
						n = c;
					if (!states.contains(n)) {
						states.add(n);
						work.add(n);
					}
					result.add(t.getLabel().getString());
					i++;
				}
			}
		}

		
		public void write() throws IOException {
			Iterator<? extends State> sit = null;
			try {
				sit = lts.init();
			} catch (LTSException e) {
				System.err.println("TraceWriter: caught LTSException: "+e.getMessage());
				return;
			}
			unfold(sit);
			for (Iterator<String> it = result.iterator(); it.hasNext();)
				app.append(it.next()+"\n");
		}
		
		public TraceWriter(LTS l, Interpretation i, Appendable a) {
			interp = i;
			lts = l;
			app = a;
		}

		private LTS lts;
		private Interpretation interp;
		private Appendable app;
		private Vector<String> result = new Vector<String>();
}
