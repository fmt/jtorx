package utwente.fmt.lts;

public interface Action {
	String getString();
}
