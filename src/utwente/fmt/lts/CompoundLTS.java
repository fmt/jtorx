package utwente.fmt.lts;

import utwente.fmt.test.Verdict;

public interface CompoundLTS extends LTS {
	java.util.Iterator<?extends CompoundState> init() throws LTS.LTSException;
	void done();
	
	LTS getSubLTS();
	
	// should be in a derived interface
	public Verdict getNegativeDefaultVerdict();
	public Verdict getPositiveDefaultVerdict();
	
}
