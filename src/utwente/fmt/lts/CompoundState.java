package utwente.fmt.lts;

import java.util.Iterator;
import utwente.fmt.test.Verdict;

/**
 * Interface to a Compound State: to a State that contains
 * others states, of an underlying LTS.
 * The identity of a Compound State is determined by
 * the states of the underlying LTS that it contains.
 * Typically, a Compound State contains those states of the underlying LTS
 * that are directly connected via internal (tau) transitions.
 * <p>
 * The outgoing transitions of a Compound State, as given by method <code>menu()</code>,
 * consist of only the visible (observable, non-tau) transitions of the underlying LTS.
 * The internal (tau) transitions of the underlying LTS can be obtained
 * via method getSubStates().
 * <p>
 * A Compound State may choose to cache a computed list of
 * of outgoing transitions.
 * Method <code>disposeTransitions() </code> suggests to a Compound State
 * to dispose of any cached transitions.
 *
 */

public interface CompoundState extends State {
	
	CompoundState getCanonical();
	
	/**
	 * obtain outgoing transitions of this state,
	 * filtered by the given constraint.
	 * @param l the constraint
	 * @return an Iterator over the outgoing transitions
	 */

	java.util.Iterator<?extends CompoundTransition<?extends CompoundState>> menu(LabelConstraint l );
	
	CompoundTransition<?extends CompoundState> next(Label l );
	
	/**
	 * obtain the {@link CompoundLTS} to which the CompoundState belongs.
	 * Each CompoundState belongs to a CompoundLTS.
	 * @return the CompoundLTS
	 */
	CompoundLTS getLTS();
	
	/**
	 * obtain underlying states in the underlying LTS.
	 * @return an Iterator over these states
	 */
	Iterator<? extends State> getSubStates();
	
	/**
	 * obtain underlying states in the given underlying LTS.
	 * @param l the underlying lts for which the states must be given
	 * @return an Iterator over these states
	 */

	Iterator<? extends State> getSubStates(LTS l);
	/**
	 * obtain outgoing internal (tau) transitions
	 * from the underlying states in the underlying LTS.
	 * 
	 * @return an Iterator over these transitions
	 */
	Iterator<? extends Transition<? extends State>> getSubTransitions();
	
	String getSubEdgeName();

	/**
	 * free cached outgoing transitions (if any) for this state
	 */
	void disposeTransitions();

	/**
	 * obtain verdict (if any) associated with this state.
	 * @return the associated verdict, or null
	 */
	Verdict getVerdict();
	
}
