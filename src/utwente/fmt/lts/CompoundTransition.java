package utwente.fmt.lts;

import java.util.Iterator;

public interface CompoundTransition<S extends CompoundState> extends Transition<State> {
	S getSource();
	S getDestination();
	Iterator<? extends Transition<? extends State>> getSubTransitions();
	CompoundTransition<S> instantiate(Instantiation i);
}
