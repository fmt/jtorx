package utwente.fmt.lts;

public interface Constraint {
	String getString();
}
