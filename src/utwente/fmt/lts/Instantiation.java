package utwente.fmt.lts;

public interface Instantiation {
	Label getLabel();
	Term getTerm();
}
