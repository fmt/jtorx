package utwente.fmt.lts;

import java.util.Collection;

public interface LTS {
	java.util.Iterator<?extends State> init() throws LTSException;
	void done();
	long getNrOfStates();
	long getNrOfTransitions();
	boolean hasPosition();
	Position getRootPosition(); // to be used to generate visualization

	void cleanupPreserving(Collection<? extends State> s);

	public static class LTSException extends Exception {

		private static final long serialVersionUID = 426331731670428166L;

		public LTSException(String text) {
			super(text);
		}
	}
}
