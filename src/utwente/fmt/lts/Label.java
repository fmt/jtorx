package utwente.fmt.lts;

public interface Label extends SymbolicLabel {
	Action getAction();
	Boolean isObservable();
	String getString();
	Boolean eq(Label l);
}
