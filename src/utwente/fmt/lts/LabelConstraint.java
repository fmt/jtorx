package utwente.fmt.lts;

public interface LabelConstraint {    
    Boolean isSatisfiedBy(Label l);
}
