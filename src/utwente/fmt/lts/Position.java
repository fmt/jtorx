package utwente.fmt.lts;

public interface Position {
	String getX();
	String getY();
}
