package utwente.fmt.lts;

public interface State {
	java.util.Iterator<?extends Transition<?extends State>> menu(LabelConstraint l );
	LTS getLTS();
	String getID();    // to be used to compare for equality
	String getLabel(boolean quote); // to be shown in visualizations
	Position getPosition(); // to be used to generate visualization
	String getNodeName(); // to be used to identify nodes in animation of prefab dot files
	State getCanonical(); // to obtain canonical representative of state
}
