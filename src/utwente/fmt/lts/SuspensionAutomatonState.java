package utwente.fmt.lts;

import java.util.Iterator;

// refine CompoundState by partitioning subStates into
//  - directSubStates: states directly reached by an observable transition
//  - indirectSubStates: states reached via observable transitions + one or more tau-transitions
public interface SuspensionAutomatonState extends CompoundState {
	Iterator<? extends State> getDirectSubStates();
	Iterator<? extends State> getIndirectSubStates();
	SuspensionAutomatonState getCanonical();
}
