package utwente.fmt.lts;

import java.util.Iterator;

public interface SymbolicLabel {
	// Iterator<String> getVariableNames();
	// Iterator<String> getVariableTypes();
	Iterator<Variable> getVariables();
	Constraint getConstraint();
	// Boolean eq(SymbolicLabel l);
}
