package utwente.fmt.lts;

public interface SymbolicState  {
	java.util.Iterator<?extends SymbolicTransition<?extends SymbolicState>> menu(LabelConstraint l );
	SymbolicLTS getLTS();
}
