package utwente.fmt.lts;

import java.util.Iterator;

public interface SymbolicTransition<T extends SymbolicState> {
	SymbolicLabel getLabel();
	SymbolicTransition<T> instantiate(Instantiation i);
	Label getSolution();
}
