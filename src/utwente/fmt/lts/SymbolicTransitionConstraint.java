package utwente.fmt.lts;

import java.util.Iterator;

public interface SymbolicTransitionConstraint {
	String toString();
	Iterator<String> getFreeVariableNames();
}
