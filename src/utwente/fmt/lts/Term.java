package utwente.fmt.lts;

import java.util.ArrayList;

// a Term is either:
//  - a variable
//  - an operator name with a list of sub-terms

// if there are no variables in the Term we call it closed,
// else we call it open.
// does the interface have to give access to the variables?
// does the interface have to indicate whether a term is open or closed?
// in principle a term may be typed, but we do not deal with that.

public interface Term {
	public boolean isVariable();
	public Variable getVariable();
	public String getOperator();
	public ArrayList<Term> subTerms();
	public UnificationResult unify(Term t, ArrayList<UnificationBinding> bl);
	public UnificationResult unify(Term t);
	public String unparse();
}
