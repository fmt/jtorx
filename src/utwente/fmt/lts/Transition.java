package utwente.fmt.lts;

import java.util.Vector;

public interface Transition<T extends State> extends SymbolicTransition<SymbolicState> {
	T getSource();
	Label getLabel();
	T getDestination();
	
	Vector<Position> getPosition();
	Position getLabelPosition();
	String getEdgeName(); // to be used to identify edges in animation of prefab dot files
	
	Transition<T> instantiate(Instantiation i);
}
