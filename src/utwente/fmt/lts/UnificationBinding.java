package utwente.fmt.lts;

public interface UnificationBinding {
	public Variable getVariable();
	public Term getTerm();
}
