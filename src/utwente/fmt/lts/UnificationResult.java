package utwente.fmt.lts;

import java.util.ArrayList;

public interface UnificationResult {
	public boolean isUnifyable();
	public ArrayList<UnificationBinding> getBindings();
}
