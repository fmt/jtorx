package utwente.fmt.lts;

public interface Variable {
	String getName();
	String getType();
	String getOriginalName();
}
