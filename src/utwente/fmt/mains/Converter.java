package utwente.fmt.mains;

import java.io.IOException;


import utwente.fmt.Version;
import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.reporting.StderrErrorReporter;
import utwente.fmt.jtorx.testgui.RunItemData;
import utwente.fmt.jtorx.writers.AnidotWriter;
import utwente.fmt.jtorx.writers.AutWriter;
import utwente.fmt.jtorx.writers.DotWriter;
import utwente.fmt.jtorx.writers.LTSWriter;
import utwente.fmt.jtorx.writers.TraceWriter;
import utwente.fmt.lts.LTS;

public class Converter {
	
	public enum OutputType {
		UNKNOWN,
		ANIDOT,
		AUT,
		DOT,
		TRACE,
	}
	
	public void convert(OutputType type, String inputFile) {
		RunItemData r = new RunItemData();
		
		LTS l = r.readModelFile(inputFile, new StderrErrorReporter(), new NullProgressReporter());
		
		LTSWriter w = null;
		
		switch(type) {
		case ANIDOT:
			w = new AnidotWriter(l, new AnyInterpretation(),  System.out);
			break;
		case AUT:
			w = new AutWriter(l, new AnyInterpretation(),  System.out);
			break;
		case DOT:
			w = new DotWriter(l, new AnyInterpretation(),  System.out);
			break;
		case TRACE:
			w = new TraceWriter(l, new AnyInterpretation(),  System.out);
		    break;
		}
		
		if (w==null) {
			System.err.println("jtorxio: unsupported result type: "+type.name());
			System.exit(1);
		}
		
		try {
			w.write();
		} catch (IOException e) {
			System.err.println("jtorxio: write error: "+e.toString());
		}
	
	}
	public Converter() {
	}
	
	public static void main(String[] args) {
		if (args.length > 0 && args[0].equals("-V")) {
			System.err.println("JTorX version "+Version.getVersionAndDate());
			System.exit(0);
		}

		if (args.length != 2) {
			System.err.println("usage: jtorxio <type> <inputfile>");
			System.err.println("where <type> is one of:");
		    System.err.println("\t-anidot");
			System.err.println("\t-aut");
			System.err.println("\t-dot or  -gv");
			System.err.println("\t-trace");
			System.err.println("usage: jtorxio -V");
			System.exit(1);
		}
		
		OutputType type = OutputType.UNKNOWN;
		
		if(args[0].equals("-anidot"))
			type = OutputType.ANIDOT;
		else if(args[0].equals("-aut"))
			type = OutputType.AUT;
		else if (args[0].equals("-dot") || args[0].equals("-gv"))
			type = OutputType.DOT;
		else if (args[0].equals("-trace"))
			type = OutputType.TRACE;
		
		if (type == OutputType.UNKNOWN) {
			System.err.println("jtorxio: unknown type: "+args[0]);
			System.exit(1);
		} else {
			Converter c = new Converter();
			c.convert(type, args[1]);
		}
	}

}
