package utwente.fmt.mains;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import utwente.fmt.Version;
import utwente.fmt.interpretation.IOLTSInterpretation;
import utwente.fmt.interpretation.InterpKind;
import utwente.fmt.interpretation.TraceKind;
import utwente.fmt.jtorx.interpretation.ActionListInterpretation;
import utwente.fmt.jtorx.interpretation.AnyInterpretation;
import utwente.fmt.jtorx.interpretation.PostfixDeltaInterpretation;
import utwente.fmt.jtorx.interpretation.PostfixInterpretation;
import utwente.fmt.jtorx.interpretation.PrefixDeltaInterpretation;
import utwente.fmt.jtorx.interpretation.PrefixInterpretation;
import utwente.fmt.jtorx.lazyotf.LazyOTFConfig;
import utwente.fmt.jtorx.logger.coverage.CoverageData;
import utwente.fmt.jtorx.reporting.NullProgressReporter;
import utwente.fmt.jtorx.testgui.Config;
import utwente.fmt.jtorx.testgui.QuiescenceInterpretation;
import utwente.fmt.jtorx.testgui.RollCall;
import utwente.fmt.jtorx.testgui.RunItemData;
import utwente.fmt.jtorx.testgui.RunItemData.AdapterKind;
import utwente.fmt.jtorx.testgui.RunSessionData;
import utwente.fmt.jtorx.torx.explorer.guide.GuidanceLTS;
import utwente.fmt.jtorx.torx.guided.GuidedLTS;
import utwente.fmt.jtorx.torx.primer.Primer;
import utwente.fmt.jtorx.ui.ErrorReporter;
import utwente.fmt.jtorx.ui.ProgressReporter;
import utwente.fmt.jtorx.writers.AniDotLTSWriter;
import utwente.fmt.jtorx.writers.AnidotWriter;
import utwente.fmt.jtorx.writers.AutWriter;
import utwente.fmt.jtorx.writers.DotWriter;
import utwente.fmt.jtorx.writers.LTSWriter;
import utwente.fmt.jtorx.writers.TraceWriter;
import utwente.fmt.lts.CompoundLTS;
import utwente.fmt.lts.LTS;
import utwente.fmt.test.Verdict;

public class SuspensionAutomaton {
	
	private RollCall missing = new RollCall();
	private IOLTSInterpretation quiescenceInterp =  null;
	
	private ErrorReporter err = new MyErrorReporter();
	private ProgressReporter progress = new NullProgressReporter();

	
	public static String titleOf(File f) {
		return f.getName().split("\\.",2)[0];
	}

	
	public enum OutputType {
		UNKNOWN,
		ANIDOT,
		AUT,
		DOT,
		TRACE,
	}
	
	class Testrun implements utwente.fmt.engine.Testrun, utwente.fmt.configuration.Testrun {

		private void loadConfig(RunSessionData d, String fname, boolean loadingLogForReplay) {
			if (fname==null || fname.trim().equals(""))
				return;
			if (d==null)
				return;

			FileInputStream fin;
			try {
				fin = new FileInputStream (fname);
				BufferedReader r = new BufferedReader(new InputStreamReader(fin));
				cfg.read(r);
				fin.close();
				if (loadingLogForReplay) { // tweak log settings to use log as SUT
					cfg.add("ADAPTERKIND", AdapterKind.LOGADAPTER);
					cfg.add("IMPLTEXT", fname);
					// should we disable things like impl instantiator?
				}
				addStatusMsg("read configuration from: "+fname);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				addStatusMsg("cannot read configuration: "+e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				addStatusMsg("cannot read configuration: "+e.getMessage());
				e.printStackTrace();
			}
		}
		
		// ====================================
		// To implement interface engine.Testrun
		
		public void addStatusMsg(String s) {
			System.err.println("Info: "+s);
		}
		public ProgressReporter getStateSpaceProgressReporter() {
			return null;
		}
		public void runSetup() {
		}
		public long parseSeed(String s, String err) {
			long n  = 0;
			if (!s.trim().equals("")) {
				try {
					n = Long.parseLong(s);
				}
				catch(NumberFormatException nfe) {
					addStatusMsg(err+": "+s);
				}
			}
			return n;
		}

		public long initSeed(String s) {
			long n  = 0;
			if (!s.trim().equals("")) {
				try {
					n = Long.parseLong(s);
				}
				catch(NumberFormatException nfe) {
					addStatusMsg("seed is not a number: "+s);
				}
			}
			if (n == 0) {
				Random rand = new Random();
				n = rand.nextInt();
				if (n < 0)
					n = -n;
			}
			return n;
		}
		public long initSimSeed(String s, long rs) {
			long n  = 0;
			if (!s.trim().equals("")) {
				try {
					n = Long.parseLong(s);
				}
				catch(NumberFormatException nfe) {
					addStatusMsg("sim seed is not a number: "+s);
				}
			}
			if (n == 0) {
				n = rs;
			}
			return n;
		}
		public void prepareLogMuxForSession() {
		}
		public void startLogMuxSession(RunSessionData rsd) {
		}
		public void abortTestrun() {
			System.err.println("== ABORTING TESTRUN ==");
		}
	
		// called from non-gui thread
		
		public void runBegins() {
			System.err.println("== TESTRUN STARTS { ++");
		}
		
		public void stopTestrun() {
			
			System.err.println("== TESTRUN ENDS } ++");
		}
		public void addLogger(final boolean inWindow, final String t) {
		}
		public void addCoverageLogger(CoverageData coverage) {
		}
		public void showMenu() {
		}
		public void showVerdict(final Verdict verdict) {
			System.err.println("Verdict: "+verdict.getString());
		}
		public void showCoverage(final CoverageData c) {
			System.err.println("Coverage: "+c.getReport());
		}


		// To implement interface configuration.Testrun
		public IOLTSInterpretation getInterpretation() {
			InterpKind k = getInterpKind();
			if (getReconDelta()) {
				if (k==InterpKind.POSTFIX)
					return new PostfixDeltaInterpretation();
				else if (k==InterpKind.PREFIX)
					return new PrefixDeltaInterpretation();
				else if (k==InterpKind.ACTIONLIST)
					return new ActionListInterpretation(getInterpInputs(), getInterpOutputs());
				else
					return null;			
			} else {
				if (k==InterpKind.POSTFIX)
					return new PostfixInterpretation();
				else if (k==InterpKind.PREFIX)
					return new PrefixInterpretation();
				else if (k==InterpKind.ACTIONLIST)
					return new ActionListInterpretation(getInterpInputs(), getInterpOutputs());
				else
					return null;
			}
		}
		public boolean useGuide() {
			return cfg.getBool("USEGUIDE");
		}
		public boolean useCoverage() {
			return cfg.getBool("USECOVERAGE");
		}
		public boolean useModelInstantiator() {
			return cfg.getBool("USEMODELINST");
		}
		public String getInstModelFilename() {
			return cfg.getString("INSTMODELTEXT");
		}
		public boolean useImplInstantiator() {
			return cfg.getBool("USEIMPLINST");
		}
		public String getInstImplFilename() {
			return cfg.getString("INSTIMPLTEXT");
		}
		public String getModelFilename() {
			return cfg.getString("MODELTEXT");
		}
		public String getGuideFilename() {
			return cfg.getString("GUIDETEXT");
		}
		public String getImplFilename() {
			return cfg.getString("IMPLTEXT");
		}
		public RunItemData.LTSKind getGuideKind() {
			return cfg.getLTSKind("GUIDEKIND");
		}
		public RunItemData.AdapterKind getImplKind() {
			return cfg.getAdapterKind("ADAPTERKIND");
		}
		public TraceKind getTraceKind() {
			return cfg.getTraceKind("TRACEKIND");
		}
		public int getTimeoutValue() {
			return cfg.getInt("TIMEOUTVAL");
		}
		public TimeUnit getTimeoutUnit() {
			return cfg.getTimeUnit("TIMEOUTUNIT");
		}
		public LTS getSelectedLTS() {
			return null;
		}
		public LTS getCombinedLTS() {
			return null;
		}
		public boolean useImplRealTime() {
			return cfg.getBool("USEREALTIME");
		}
		public boolean getStripAddLabelAnno() {
			return cfg.getBool("ADDRMLABELANNO");
		}
		public InterpKind getInterpKind() {
			return cfg.getInterpKind("INTERPKIND");
		}
		public boolean getReconDelta() {
			return cfg.getBool("RECONDELTA");
		}
		public boolean getSynthDelta() {
			return cfg.getBool("SYNTHDELTA");
		}
		public boolean getAngelicCompletion() {
			return cfg.getBool("ANGELCOMPL");
		}
		public boolean getDivergence() {
			return cfg.getBool("DIVERGENCE");
		}
		public String getInterpInputs() {
			return cfg.getString("INTERPINPUTS");
		}
		public String getInterpOutputs() {
			return cfg.getString("INTERPOUTPUTS");
		}
		public String getSeed() {
			return cfg.getString("SEED");
		}
		public void setSeed(String s) {
			cfg.add("SEED", s);
		}
		public String getSimSeed() {
			return cfg.getString("SIMSEED");
		}
		public void setSimSeed(String s) {
			cfg.add("SIMSEED", s);
		}
		public boolean vizModel() {
			return false;
		}
		public boolean vizMsc() {
			return false;
		}
		public boolean vizRun() {
			return false;
		}
		public boolean vizLogPane() {
			return false;
		}
		public boolean vizSuspAutom() {
			return false;
		}
		public boolean vizGuide() {
			return false;
		}
		public boolean vizCoverage() { // TODO: is this what we really want???
			return cfg.getBool("VIZCOVERAGE");
		}
		public boolean vizImpl() {
			return false;
		}
		
		//========= constructor and private data
		public Testrun() {
		}
		
		private Config cfg = new Config();

		@Override
		public boolean isLazyOTFEnabled() {
			return false;
		}

		@Override
		public LazyOTFConfig getLazyOTFConfig() {
			return null;
		}
	}
	class MyErrorReporter implements ErrorReporter {
		
		public void report(final String s) {
			System.err.println("errreporter: "+s);
		}
		
		//TODO stop the test run
		public void endOfTest(final String s) {
			System.err.println("errreporter: eot: "+s);
			// stop the test run
		}

		public void log(long t, final String src, final String s) {
			System.err.println("errreporter: ("+src+") "+fmt.format(t)+" "+s);
			//logMux.addLogMessage(t, src, s);
		}
		
		private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
}

	
	public void convert(OutputType type) {
		final utwente.fmt.configuration.Testrun cfg = cfgRun;
		final utwente.fmt.engine.Testrun run = cfgRun;
 		
        final RunItemData modelData = new RunItemData();
        final RunItemData guideData = new RunItemData();
        final RunItemData guidePrimerData = new RunItemData();
        final RunItemData combinatorData = new RunItemData();
		
		final IOLTSInterpretation interp = cfg.getInterpretation();
		final boolean useGuide = cfg.useGuide();
		final boolean useCoverage = cfg.useCoverage();
		final boolean useModelInstantiator = cfg.useModelInstantiator();
		final String cfgInstModelFileName = cfg.getInstModelFilename();

		final String cfgModelFilename = cfg.getModelFilename();
		final String cfgGuideFilename = cfg.getGuideFilename();
		final RunItemData.LTSKind guideKind = cfg.getGuideKind();
		LTS tmpIocoCheckerTestPurpose = null;
		final TraceKind traceKind = cfg.getTraceKind();
		
		missing.check(cfgModelFilename.equals(""), "model");
		if (cfg.useGuide() && (guideKind==RunItemData.LTSKind.MODEL || guideKind==RunItemData.LTSKind.TRACE))
			missing.check(cfgGuideFilename.equals(""), "guide");
		if (cfg.useGuide() && guideKind == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE) {
			tmpIocoCheckerTestPurpose = cfg.getSelectedLTS();
			missing.check(tmpIocoCheckerTestPurpose==null, "iocoChecker-trace-as-guide");
		}
		if (cfg.useGuide() && guideKind == RunItemData.LTSKind.IOCOCHECKERMULTITRACE) {
			tmpIocoCheckerTestPurpose = cfg.getCombinedLTS();
			missing.check(tmpIocoCheckerTestPurpose==null, "iocoChecker-traces-as-guide");
		}
		final LTS iocoCheckerTestPurpose = tmpIocoCheckerTestPurpose;

		if (!missing.getMissing().equals("")) {
			run.addStatusMsg("specify"+missing.getMissing());
			run.abortTestrun();
			return;
		}
		
		run.addStatusMsg("");

		runData.reset();
		runData.setUseGuide(useGuide);
		runData.setUseModelInstantiator(useModelInstantiator);
		runData.setModelData(modelData);
		runData.setGuideData(guideData);
		runData.setGuidePrimerData(guidePrimerData);
		runData.setCombinatorData(combinatorData);

		runData.setInterpKind(cfg.getInterpKind());
		runData.setReconDelta(cfg.getReconDelta());
		runData.setSynthDelta(cfg.getSynthDelta());
		runData.setAngelicCompletion(cfg.getAngelicCompletion());
		runData.setDivergence(cfg.getDivergence());
		runData.setInterpInputs(cfg.getInterpInputs());
		runData.setInterpOutputs(cfg.getInterpOutputs());
		runData.setTraceKind(traceKind);
		

		// How to deal with special case where particular TorX-explorer has Instantiator built-in?
		// always allow user to override and user external Instantiator
		// TODO extend torx-explorer protocol to allow 'user' to ask for properties
		//  - finite/infinite ?
		//  - known-to-be-symbolic ?
		//  - has built-in instantiator ?
		// e.g. by having comamand: p
		// and answer  P list of property names that apply

		String instModelFileName = "";
		if (useModelInstantiator && cfgInstModelFileName!=null)
			instModelFileName = cfgInstModelFileName;
		runData.setModelInstantiator(instModelFileName);

		modelData.endPreviousIfDifferent(cfgModelFilename, instModelFileName);
		modelData.setFilename(cfgModelFilename, instModelFileName);
		modelData.setLTSKind(RunItemData.LTSKind.MODEL);
	
		// TODO this needs more thought: should endPreviousIfDifferent
		// look not only at filename but also at kind???

		guideData.setLTSKind(guideKind);
		if (useGuide && (guideKind==RunItemData.LTSKind.MODEL || guideKind==RunItemData.LTSKind.TRACE)) {
			guideData.endPreviousIfDifferent(cfgGuideFilename, "");
			guideData.setFilename(cfgGuideFilename, "");
		} else if (useGuide && 
				(guideKind == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE ||
						guideKind == RunItemData.LTSKind.IOCOCHECKERMULTITRACE)) {
			guideData.setFilename("", "");
		} else {
			guideData.setFilename("", "");
		}

		if (! modelData.startInstantiatorIfNeeded(err, progress)) {
			run.abortTestrun();
			return;
		}
		if (! modelData.readLTSFileIfNeeded(err, progress)) {
			run.abortTestrun();
			return;
		}
		if (useGuide && (guideKind==RunItemData.LTSKind.MODEL) &&
				! guideData.readLTSFileIfNeeded(err, progress)) {
			run.abortTestrun();
			return;
		}
		if (useGuide && (guideKind==RunItemData.LTSKind.TRACE) &&
				! guideData.readLTSFileIfNeeded(err, progress)) {
			run.abortTestrun();
			return;
		}
		if (useGuide &&
				(guideKind == RunItemData.LTSKind.IOCOCHECKERSINGLETRACE ||
						guideKind == RunItemData.LTSKind.IOCOCHECKERMULTITRACE))
			guideData.setLTS(iocoCheckerTestPurpose);
		if (useGuide && !guideData.containsEpsilon())
			guideData.setLTS(new GuidanceLTS(guideData.getLTS()));

		final boolean cacheAllModelStates = runData.getVizSuspAut();
		final boolean synthDelta = runData.getSynthDelta();
		final boolean angelicCompletion = runData.getAngelicCompletion();
		final boolean divergence = runData.getDivergence();


				CompoundLTS myPrimer = new Primer(modelData.getLTS(), interp, !synthDelta /*tracesOnly*/, cacheAllModelStates, traceKind, angelicCompletion, divergence, err, progress);
				runData.setModelLTS(myPrimer);
				runData.setModelWriter(new AniDotLTSWriter(modelData.getLTS(),  interp, run.getStateSpaceProgressReporter()));

                quiescenceInterp = new QuiescenceInterpretation(interp);
 
              Set<CompoundLTS> guides = new HashSet<CompoundLTS>();
              LTS l = null;
                
    			if (useGuide) {
					// TODO make sure choice for always STRACES here is correct
					CompoundLTS myGuide = new Primer(guideData.getLTS(), quiescenceInterp, true /*tracesOnly*/, cacheAllModelStates, TraceKind.STRACES, false  /*no AngelicCompletion*/, false /*no divergence*/, err, progress);
					Set<CompoundLTS> specs = new HashSet<CompoundLTS>();
					specs.add(myPrimer);
					guides.add(myGuide);
					if (guideKind==RunItemData.LTSKind.MODEL || guideKind==RunItemData.LTSKind.TRACE) {
						File guideFile = new File(guideData.getModelFilename());
						runData.setGuideTitle(titleOf(guideFile));
					} else
						runData.setGuideTitle("IocoCheckerTrace");
					runData.setGuideLTS(myGuide);
					runData.setGuideWriter(new AniDotLTSWriter(guideData.getLTS(),  quiescenceInterp, run.getStateSpaceProgressReporter()));
				}

				if (guides.size() > 0) {
					Set<CompoundLTS> specs = new HashSet<CompoundLTS>();
					specs.add(myPrimer);
					CompoundLTS myGuidedPrimer = new GuidedLTS(specs, guides, interp, /*enableCache:*/ !useCoverage, err);
					l = myGuidedPrimer;
				} else {
					l = myPrimer;
				}

		
		LTSWriter w = null;
		
		switch(type) {
		case ANIDOT:
			w = new AnidotWriter(l, new AnyInterpretation(),  System.out);
			break;
		case AUT:
			w = new AutWriter(l, new AnyInterpretation(),  System.out);
			break;
		case DOT:
			w = new DotWriter(l, new AnyInterpretation(),  System.out);
			break;
		case TRACE:
			w = new TraceWriter(l, new AnyInterpretation(),  System.out);
		    break;
		}
		
		if (w==null) {
			System.err.println("jtorxio: unsupported result type: "+type.name());
			System.exit(1);
		}
		
		try {
			w.write();
		} catch (IOException e) {
			System.err.println("jtorxio: write error: "+e.toString());
		}
	
	}
	public SuspensionAutomaton(String configFile) {
		cfgRun =  new Testrun();
		cfgRun.loadConfig(runData, configFile, false);


	}
	
	public static void main(String[] args) {
		String cfgFile = null;

		if (args.length > 0 && args[0].equals("-V")) {
			System.err.println("JTorX version "+Version.getVersionAndDate());
			System.exit(0);
		}

		if (args.length != 2) {
			System.err.println("usage: jtorxsa type configFile");
			System.err.println("where <type> is one of:");
		    System.err.println("\t-anidot");
			System.err.println("\t-aut");
			System.err.println("\t-dot or  -gv");
			System.err.println("usage: jtorxsa -V");
			System.exit(1);
		}
		
		OutputType type = OutputType.UNKNOWN;
		
		if(args[0].equals("-anidot"))
			type = OutputType.ANIDOT;
		else if(args[0].equals("-aut"))
			type = OutputType.AUT;
		else if (args[0].equals("-dot") || args[0].equals("-gv"))
			type = OutputType.DOT;
		
		cfgFile = args[1];
		
		if (type == OutputType.UNKNOWN) {
			System.err.println("jtorxsa: unknown type: "+args[0]);
			System.exit(1);
		} else {
			SuspensionAutomaton c = new SuspensionAutomaton(cfgFile);
			c.convert(type);
		}
	}
	
	private Testrun cfgRun;
	private RunSessionData runData = new RunSessionData();
}
