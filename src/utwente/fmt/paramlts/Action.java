package utwente.fmt.paramlts;

public interface Action {
	String getString();
}
