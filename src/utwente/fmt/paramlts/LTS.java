package utwente.fmt.paramlts;

public interface LTS<T> {
	StateIterator<T> init();
	void done();
}
