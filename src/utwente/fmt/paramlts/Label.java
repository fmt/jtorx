package utwente.fmt.paramlts;

public interface Label {
	Action getAction();
	Boolean isObservable();
	String getString();
	Boolean eq(Label l);
}
