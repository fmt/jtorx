package utwente.fmt.paramlts;

public interface LabelConstraint {
 //   void isInput();
  //  void isOutput();
  //  void isObservable();
 //   void isInternal();
  //  void hasAction(Action a);
    
    Boolean holds(Label l);
}
