package utwente.fmt.paramlts;

public interface State<T> {
	TransitionIterator<T> menu(LabelConstraint l );
	LTS<T> getLTS();
	String getID();
}
