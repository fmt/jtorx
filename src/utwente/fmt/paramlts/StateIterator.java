package utwente.fmt.paramlts;

public interface StateIterator<T> {
	Boolean hasNext();
	T next();
}
