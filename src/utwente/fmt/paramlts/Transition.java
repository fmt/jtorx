package utwente.fmt.paramlts;

public interface Transition<T> {
	State<T> getSource();
	Label getLabel();
	State<T> getDestination();
}
