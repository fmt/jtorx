package utwente.fmt.paramlts;

public interface TransitionIterator<T> {
	Boolean hasNext();
	Transition<T> next();
}
