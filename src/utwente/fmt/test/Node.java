package utwente.fmt.test;

import utwente.fmt.lts.CompoundState;
import utwente.fmt.lts.Transition;

public interface Node extends CompoundState {
	CompoundState getState();
	void extend(Testcase t);
	Boolean isLeaf();
	Boolean isOpen();
	Boolean isReachedByQuiescence();
	Verdict getVerdict();
	
	void add(Transition<? extends Node> t);
}
