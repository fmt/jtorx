package utwente.fmt.test;

import utwente.fmt.lts.LTS;
//import utwente.fmt.lts.Label;
//import utwente.fmt.lts.State;

// Still have to decide whether this is test case
// or test graph or ...
public interface Testcase {

		LTS getLTS();
		java.util.Iterator<?extends Node> getOpenLeaves();
		
		// Node mkNode(State s);
		// Node mkTransition(State s, Label l, State d);

}
