package utwente.fmt.test;

public interface Verdict {
	String getString();
	Boolean isFail();
	Boolean isError();
}
