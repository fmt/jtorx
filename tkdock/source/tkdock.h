/*tkdock: implements a simple Cocoa-based mechanism for changing dock icon in a Tk application on OS X. (c) 2009 WordTech Communications LLC. License: standard Tcl license,  http://www.tcl.tk/software/tcltk/license.html.*/
/* Original version by Kevin Walzer, who released it as tkdock-1.0;
   this version has significantly been rewritten by Axel.Belinfante@cs.utwente.nl
 */


#import <tcl.h>
#import <Cocoa/Cocoa.h>

int SwitchIcon (ClientData cd, Tcl_Interp *ip, int objc, Tcl_Obj *CONST objv[]);
int OrigIcon (ClientData cd, Tcl_Interp *ip, int objc, Tcl_Obj *CONST objv[]);

int Tkdock_Init (Tcl_Interp *ip);
int Tkdock_SafeInit(Tcl_Interp *ip);
