/*tkdock: implements a simple Cocoa-based mechanism for changing dock icon in a Tk application on OS X. (c) 2009 WordTech Communications LLC. License: standard Tcl license,  http://www.tcl.tk/software/tcltk/license.html.*/
/* Original version by Kevin Walzer, who released it as tkdock-1.0;
   this version has significantly been rewritten by Axel.Belinfante@cs.utwente.nl
 */

#import "tkdock.h"

// Tcl command to set the dock icon
int SwitchIcon (ClientData cd, Tcl_Interp *ip, int objc, Tcl_Obj *CONST objv[]) {
	
	//set up memory pool
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	//need proper number of args	
	if(objc!=2 && objc!=3) {
		Tcl_WrongNumArgs(ip, 1, objv, "?type? arg");
		goto error;
	}

	Tcl_Obj *tclType = 0;
	Tcl_Obj *tclArg = objv[1];
	if(objc == 3) {
		tclType = objv[1];
		tclArg = objv[2];
		if (strcmp(Tcl_GetString(tclType), "-file") != 0 &&
		     strcmp(Tcl_GetString(tclType), "-data") != 0 ) {
			Tcl_AppendResult(ip, "bad option \"", Tcl_GetString(tclType),
				"\": must be -file, or -data", NULL);
			goto error;
		}
	}
 
   	NSImage *newIcon;
	if (objc==2 || (objc==3 && strcmp(Tcl_GetString(tclType), "-file") == 0)) {
		NSString *path  = [NSString stringWithUTF8String:Tcl_GetString(tclArg)];
		newIcon = [[NSImage alloc] initWithContentsOfFile: path];
	} else if (objc==3 && strcmp(Tcl_GetString(tclType), "-data") == 0) {
		int length;
		unsigned char *bytes = Tcl_GetByteArrayFromObj(tclArg, &length);
		NSData* data = [NSData dataWithBytes:bytes length:length];
		newIcon = [[NSImage alloc] initWithData: data];
	}
	
	if (newIcon == nil) {
		Tcl_AppendResult(ip, "failed to create icon", NULL);
		goto error;
	}
	
	//set up autorelease, so it gets cleaned up into an AutoreleasePool
	//when it is no longer the ApplicationIconImage.
	//(without this we would leak memory)
	//(NOTE: we do NOT do this when we alloc/init, above, because
	//then we get 'double release' errors (or segfault) if init fails.
	[newIcon autorelease];
	
	//set the icon
	[NSApp setApplicationIconImage: newIcon];
	
	//release the memory
	[pool release];
	return TCL_OK;
   
error:
	//release the memory
	[pool release];
	return TCL_ERROR;
}

//Tcl command to restore the original dock icon
int OrigIcon (ClientData cd, Tcl_Interp *ip, int objc, Tcl_Obj *CONST objv[])  {
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	//restore the icon
	[NSApp setApplicationIconImage: nil];
		
	//release memory
	[pool release];
		
	return 0;
}

//initalize the package in the tcl interpreter, create tcl commands
int Tkdock_Init (Tcl_Interp *ip) {
	
	NSApplicationLoad();
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	if (Tcl_InitStubs(ip, "8.4", 0) == NULL)
		return TCL_ERROR;

	Tcl_CreateObjCommand(ip, "::tkdock::switchIcon", SwitchIcon,(ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
	Tcl_CreateObjCommand(ip, "::tkdock::origIcon", OrigIcon, (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL); 

	if (Tcl_PkgProvide(ip, "tkdock", "1.0") != TCL_OK)
		return TCL_ERROR;

	//release memory
	[pool release];
	return TCL_OK;
}

int Tkdock_SafeInit(Tcl_Interp *ip) {
	return Tkdock_Init(ip);
}
